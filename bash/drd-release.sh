if [ "v$V" == "v" ]; then
V=`svn info | grep Revision | cut -f2 -d' '`
fi

echo '***' release stone libs
STONEDIR=/drd/software/ext/stone/lin64/$V
mkdir $STONEDIR
rsync -ac /usr/local/lib/libass* $STONEDIR/
rsync -ac /usr/local/lib/libabsubmit* $STONEDIR/
rsync -ac /usr/local/lib/libstone* $STONEDIR/
rsync -ac /usr/local/lib/libclasses* $STONEDIR/
rsync -ac /usr/local/lib/libbrainiac* $STONEDIR/

echo '***' release python libs
rsync -ac --exclude=wx* /usr/lib/python2.5/site-packages/ /drd/software/ext/python/lin64/2.5/$V/

echo '***' release applications
DIR=/drd/software/ext/ab/lin64/$V
mkdir $DIR
rsync -c cpp/apps/assburner_1_3/assburner $DIR/ab
rsync -c cpp/apps/assburner_1_3/assburner.ini $DIR/ab.ini
rsync -ac --exclude=.svn cpp/apps/assburner_1_3/plugins/ $DIR/plugins/
rsync -c cpp/apps/assfreezer/assfreezer $DIR/af
rsync -c cpp/apps/assfreezer/assfreezer.ini $DIR/assfreezer.ini

rsync -c cpp/apps/absubmit/absubmit $DIR/
rsync -c cpp/apps/absubmit/py2ab.py $DIR/
rsync -c cpp/apps/absubmit/maya/*py $DIR/
rsync -c cpp/apps/absubmit/maya/*ui $DIR/

rsync -c python/scripts/manager.py $DIR/
rsync -c python/scripts/reaper.py $DIR/
rsync -c python/scripts/initab.py $DIR/

ln -s /drd/software/ext/ab/images $DIR/images

echo '***' released $V
