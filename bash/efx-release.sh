#!/bin/bash

ARCH=`arch`
echo '***' release stone libs
STONEDIR=/mnt/x5/Global/infrastructure/ab/$ARCH/lib
mkdir -p $STONEDIR
rsync -ac /usr/local/lib/libass* $STONEDIR/
rsync -ac /usr/local/lib/libabsubmit* $STONEDIR/
rsync -ac /usr/local/lib/libstone* $STONEDIR/
rsync -ac /usr/local/lib/libclasses* $STONEDIR/

echo '***' release python libs
PYSITEPATH=`python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()"`
rsync -ac --exclude=wx* $PYSITEPATH /mnt/x5/Global/infrastructure/ab/$ARCH/python/

echo '***' release applications
DIR=/mnt/x5/Global/infrastructure/ab/$ARCH
mkdir -p $DIR
rsync -ac cpp/apps/assburner_1_3/assburner $DIR/assburner/
rsync -ac cpp/apps/assfreezer/assfreezer $DIR/assfreezer/

rsync -ac cpp/apps/absubmit/absubmit $DIR/absbumit/

echo '***' released 
