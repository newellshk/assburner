
import os
from blur.build import *

path = os.path.dirname(os.path.abspath(__file__))
rev_path = os.path.join(path,'../..')

ini = IniConfigTarget("absubmitini",path,'absubmit.ini.template','absubmit.ini')

svn = WCRevTarget("absubmitsvnrev",path,rev_path,"src/svnrev-template.h","src/svnrev.h")
svnnsi = WCRevTarget("absubmitsvnrevnsi",path,rev_path,"absubmit-svnrev-template.nsi","absubmit-svnrev.nsi")
svntxt = WCRevTarget("absubmitsvnrevtxt",path,rev_path,"absubmit_version_template.txt","absubmit_version.txt")
nsi = NSISTarget("absubmit_installer",path,"absubmit.nsi",[svnnsi])
QMakeTarget("absubmit",path,"absubmit.pro",
    ["stone","classes","libabsubmit",svnnsi,svntxt,ini,svn,svntxt],[nsi])

rpm = RPMTarget('absubmitrpm','absubmit',path,'../../../rpm/spec/absubmit.spec.template','1.0')
rpm.pre_deps = ['libabsubmitrpm']

if __name__ == "__main__":
	build()
