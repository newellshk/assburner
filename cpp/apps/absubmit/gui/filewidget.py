
import sip
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.uic import *
from blur.Stone import *
from blur.Classes import *
from blur.Stonegui import *
from blur.defaultdict import DefaultDict

class FileWidget(QWidget):
	def __init__(self,parent,submitCollection):
		QWidget.__init__(self,parent)
		loadUi('ui/filewidgetui.ui',self)
		self.SubmitCollection = submitCollection
		self.mAddFilesButton.clicked.connect( self.addFiles )
		self.mRemoveFilesButton.clicked.connect( self.removeFiles )
		self.mClearFilesButton.clicked.connect( self.clearFiles )
		self.SelectedJobs = JobList()
	
	def addFileItem(self,filePath):
		fi = QFileInfo(filePath)
		self.mFileTree.addTopLevelItem( QTreeWidgetItem( self.mFileTree, [fi.fileName(),fi.path()] ) )
		
	def setSelectedJobs(self,jobs):
		self.SelectedJobs = jobs
		self.mFileTree.clear()
		if self.SelectedJobs.size() == 1:
			self.setEnabled(True)
			for file in self.SubmitCollection.files(self.SelectedJobs[0]):
				self.addFileItem(file)
		else:
			self.setEnabled(False)
			
	def files(self):
		files = []
		for i in range(self.mFileTree.topLevelItemCount()):
			item = self.mFileTree.topLevelItem(i)
			files.append( item.text(1) + QDir.separator() + item.text(0) )
		return files
	
	def updateCollection(self):
		files = self.files()
		for job in self.SelectedJobs:
			self.SubmitCollection.setFiles(job,files)
	
	def addFiles(self):
		files = QFileDialog.getOpenFileNames( self, "Select Batch Job Files" )
		for file in files:
			self.addFileItem(file)
		self.updateCollection()
		
	def removeFiles(self):
		sel = self.mFileTree.selectedItems()
		for item in sel:
			sip.delete(item)
		self.updateCollection()
		
	def clearFiles(self):
		self.mFileTree.clear()
		self.updateCollection()

