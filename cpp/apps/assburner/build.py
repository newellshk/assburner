
from blur.build import *
import os, sys

path = os.path.dirname(os.path.abspath(__file__))
sip_path = os.path.join(path,'sipAssburner')
rev_path = os.path.join(path,'../..')

try:
    os.mkdir(sip_path)
except: pass

ini = IniConfigTarget("assburner_ini",path,'assburner.ini.template','assburner.ini')
svn = WCRevTarget("assburner_svnrev",path,rev_path,"include/svnrev-template.h","include/svnrev.h")
svnnsi = WCRevTarget("assburner_svnrevnsi",path,rev_path,"assburner-svnrev-template.nsi","assburner-svnrev.nsi")
svntxt = WCRevTarget("assburner_svnrevtxt",path,rev_path,"assburner_version_template.txt","assburner_version.txt")

toRelPath = lambda pathList: map( lambda x:'../../../lib/' + x, pathList )
toUsrInc = lambda pathList: map( lambda x:'/usr/include/' + (x.endswith('include') and x[:-7] or x), pathList )
incs = ['classes','classes/autocore','stone/include','stonegui/include','qjson']
sipIncludes = toRelPath(incs) + toUsrInc(incs)
sipLibDirs = toRelPath(['stone','stonegui','classes','qjson'])

# Python module target
st = SipTarget2("pyassburner",path,'assburner',True)
st.ExtraIncludes += sipIncludes
st.ExtraLibDirs += sipLibDirs
st.ExtraLibs += ['stonegui','classes']

# Use Static python modules on windows
deps = None
if sys.platform == 'win32':
	deps = ["sipstatic","pystonestatic","pyclassesstatic","classes",st,svn,ini]
else:
	deps = ["sipstatic","pystone","pyclasses","classes",st,svn,ini]


abpsmon = QMakeTarget("abpsmon",os.path.join(path,'psmon'), "psmon.pro", ["stonegui","classes",svn,ini])

assburner = QMakeTarget("assburner",path, "assburner.pro", deps, ['assburner_installer'])

nsi = NSISTarget("assburner_installer",path,"assburner.nsi",[assburner,abpsmon,svnnsi,svntxt])

rpm = RPMTarget('assburnerrpm','assburner',path,'../../../rpm/spec/assburner.spec.template','1.0',excludeSipFiles=False)
rpm.pre_deps = ['stoneguirpm','classesrpm','stonerpm']

if __name__ == "__main__":
	build()
