
#ifndef APPLICATION_H
#define APPLICATION_H

#include <qapplication.h>

class Application : public QApplication
{
public:
	Application ( int & argc, char ** argv );
	bool notify( QObject * receiver, QEvent * event );
};

class CoreApplication : public QCoreApplication
{
public:
	CoreApplication ( int & argc, char ** argv );
	bool notify( QObject * receiver, QEvent * event );
};

#endif // APPLICATION_H
