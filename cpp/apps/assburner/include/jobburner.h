
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of RenderLine.
 *
 * RenderLine is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * RenderLine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RenderLine; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef JOB_BURNER_H
#define JOB_BURNER_H

#include <qobject.h>
#include <qpointer.h>
#include <qstring.h>
#include <qstringlist.h>
#include <qmap.h>
#include <qprocess.h>

#include "blurqt.h"
#include "process.h"

#include "job.h"
#include "jobassignment.h"
#include "jobtask.h"
#include "jobtaskassignment.h"
#include "jobcommandhistory.h"
#include "service.h"
#include "software.h"

#include "spooler.h"
#include "win32sharemanager.h"

class Slave;
class QTimer;
class QFile;

/// \ingroup Assburner
/// @{

class JobBurner : public QObject
{
Q_OBJECT
public:
	/// created by the Slave
	JobBurner( const JobAssignment & jobAssignment, Slave * slave, int options = OptionMergeStdError );
	virtual ~JobBurner();

	enum State {
		StateNew,
		StateStarted,
		StateDone,
		StateError,
		StateCancelled
	};

	enum Options {
		/* wtf is this, not used */
		OptionCheckDone = 1, 
		
		OptionIgnoreStdError = 2,
	
		/* Delivers stderr output to slotReadStdOut, merged with stdout use QProcess::MergedChannels.
		This should be avoid and use slotProcessOutput instead, since it can differenciate between stdout/stderr if needed */
		OptionMergeStdError = 4,
		
		/// If this option is set, all data is read and delivered to slotProcessOutput
		/// otherwise data is read and delivered line by line.
		OptionProcessIOReadAll = 8,
		
		/* Indicates that stdout/stderr is encoded as utf16.  Otherwise utf8 is used */
		OptionOutputEncodingUTF16 = 16,
		
		/* Normally if a task is no longer assigned taskStart will not mark the task as busy
		 * nor will taskDone mark it as done.  With this option set updates to the task will
		 * work as normal.  This is used for software that can't re-render random frames and
		 * need to accurately record all progress that has occurred */
		OptionAllowUnassignedTaskUpdates = 32,
		
		/* With this option unset, the result of executable() is the executable to run,
		 * and any arguments are the result of buildCmdArgs(). If this option is set then
		 * the result of executable() is the entire command string, space separated.
		 * With this option set the executable name must be quoted if it contains spaces. */
		OptionExecutableIsCommandString = 64,
		
	};

	// Cleanup is done in stages, so that each stage can be customized, and delays can be inserted between each stage
	// by setting mCleanupDelay to a non-zero value.  After each delay mCleanupDelay is automatically reset to 0.
	enum CleanupStage
	{
		CleanupStart,
		CleanupCopy,
		CleanupStopProcess,
		CleanupKillProcess,
		CleanupCheckupTimers,
		CleanupOutput,
		CleanupDone
	};

	State state() const { return mState; }
	
	Options options() const { return mOptions; }
	void setOption( int option, bool set = true );
	
	CleanupStage cleanupState() { return mCleanupStage; }

    /// the Assignment from the Slave
	JobAssignment jobAssignment() const;

	Job job() const;

	Slave * slave() const;

	/// frame numbers of the current assignment
	QString assignedTasks() const;
	/// JobTask list of the current assignment
	JobTaskList assignedTaskList() const;
	/// JobTaskAssignment list of the current assignment
	JobTaskAssignmentList assignedTaskAssignments();
	
	/// Returns an null record if the job is still loading(ie. no task has started)
	JobTaskList currentTasks() const;
	JobTaskAssignmentList currentTaskAssignments() const;
	
	JobCommandHistory jobCommandHistory() const;
	
	/// Returns true if a task has started, false if the job is still loading
	bool taskStarted() const;
	/// Returns true if a task has taken longer then it's allowed
	bool exceededMaxTime();

	/// what time was the burn started
	QDateTime startTime() const;
	/// what time was the burn for the current task started
	QDateTime taskStartTime() const;

	/// Adds a regular expression that will be matched against process output
	/// to determine if the line should be ignored.  Matched against both
	/// stdout and stderr.  If the line matches, it will not be logged, or
	/// passed to slotProcessOutput(Line)
	void addIgnoreRegEx( const QString & regEx );
	
	/// what processes will the burner spawn?
	virtual QStringList processNames() const;

	virtual Service primaryService();
	virtual Software software();
	
	/// Returns the process executable
	virtual QString executable();

	/// The processes working directory is set to the returned string unless it is empty or null
	/// Default implementation returns an empty string
	virtual QString workingDirectory();

	/// Returns arguments to start the process
	virtual QStringList buildCmdArgs();

	/// Splits an env string(semi-colon separated) into a list of key=val strings
	static QStringList splitEnvironment(const QString & env);
	/// Splits a list of key=val strings into a key/val dict
	static QMap<QString,QString> splitEnvironmentKeyVal(QStringList envList);
	/// Merges an environment list into another, with %KEY% replaced by the existing
	/// value if it occurs in a value.
	static QStringList mergeEnvironment(QStringList existing, QStringList toMerge);
	
	/// Returns any additional environment variables that need to be merged with 
	/// the default system environment. Default implementation returns:
	/// splitEnvironment(mJob.environment());
	virtual QStringList customEnvironment();
	
	/// Needs to return either an empty list in which case the default system environment will
	/// be used, or return a full environment.  Default implementation does:
	/// mergeEnvironment(QProcess::systemEnvironment(), customEnvironment());
	/// Override this if you need to do any custom processing of the default system environment,
	/// such as removing keys or altering values.
	virtual QStringList environment();

	QProcess * process() const { return mCmd; }

	// Returns Stone::processId(process()), can be overridden to get working memory stats etc. if
	// the burner subclass isn't using QProcess
	virtual qint32 processId();
	
	QString burnFile() const { return mBurnFile; }

	QString burnDir() const;

	static JobError logError( const Job & j, const QString & msg, const JobTaskList & jtl = JobTaskList(), bool timeout=false, JobCommandHistory jch = JobCommandHistory() );

	/// to be called by a burner if an error is raised
	/// Can be called multiple times for a single burner, but best to include all error information in a single call if possible
	/// is called automatically by slotProcessExited, and slotProcessError under some conditions, see those functions for details
	void jobErrored( const QString &, bool timeout=false );

	static QString stringForProcessError( QProcess::ProcessError error );
	
	virtual void getCurrentInfo( QList<QPair<QString,QString> > & tableData );
	
protected:
	/// to be called by a burner when the job has successfully finished
	virtual void jobFinished();
	
	/// to be called by a burner when a task starts
	/// Returns true if the task status changed to 'busy', returns false if the task is not found or no longer assigned
	/// startTime is applied to the jobtask record(s) using setColumnLiteral, it can be used to pass an absolute timestamp
	/// or something in relation to sql now().  An empty/null startTime will result in the time being sql now()
	bool taskStart( int frame, const QString & outputName = QString(), const QString & startTime = QString() );
	bool taskStart( int frame, const QString & outputName, const QDateTime & startTime );
	bool taskStart( int frame, const QString & outputName, int secondsSinceStarted );
	
	/// to be called by a burner when a task is done
	/// endTime is applied to the jobtask record(s) using setColumnLiteral, it can be used to pass an absolute timestamp
	/// or something in relation to sql now().  An empty/null startTime will result in the time being sql now()
	void taskDone( int frame, const QString & endTime = QString() );
	void taskDone( int frame, const QDateTime & endTime );
	
	
	QTimer * checkupTimer() { return mCheckupTimer; }

	void logMessage( const QString &, QProcess::ProcessChannel = QProcess::StandardOutput );

	/// Connects process to slots
	/// slotReadStdOut
	/// slotReadStdError
	/// slotProcessExited
	/// slotProcessError
	virtual void connectProcess( QProcess * );

	/// Responsibilities
	///  1) Create mCmd
	///  2) Connect mCmd's signals
	///  3) Set mCommandHistory.command
	///  4) Start mCmd
	
	/// Default implementation does the following
	///  1) Creates mCmd
	///  2) Calls connectProcess on mCmd
	///  3) Gets cmd via executable and buildCmdArgs()
	///  4) Sets mCommandHistory.commad to cmd + " " + args.join(" ")
	///  5) Starts mCmd with start( cmd, args )
	virtual void startProcess();

	/// Copies 'frame' (path generated using mJob.outputPath and makeFramePath function),
	/// to all subsequent frames in the frameNth range, up to frameRangeEnd
	void copyFrameNth( int frame, int frameNth, int frameRangeEnd, bool missingOnly );

	void updatePriority( const QString & priorityName );
	
public slots:

	virtual void start();
	/// Called by slave to start the job
	/// Shouldn't need to override this, override startProcess instead.
	/// This calls startProcess, sets up JobCommandHistory, starts
	/// the output timer and the memory timer
	virtual void startBurn();
	
	virtual void startCopy();

	virtual void cancel();

	virtual void saveScreenShot();

signals:
	/// emitted so the Slave knows the Burn is done
	void finished();
	/// emitted so the Slave knows the Burn errored out
	void errored( const QString & );
	void cancelled();

	/// emitted whenever a file is generated by a Burner that should have perms set,
	/// copied somewhere, etc.
	void fileGenerated( const QString & );

	/// emitted when all cleanup is done.  There should be no processes left, no files open,
	/// and all win32 share references released
	void cleaned();
	
protected slots:
	/// Default implementation checks to make sure maximum load time 
	/// and maximum task time haven't been exceeded.
	virtual bool checkup();

	/// Process slots
	virtual void slotReadStdOut();
	virtual void slotReadStdError();

	/// If the job has not completed or errored already this function will call jobErrored with "process exited unexpectedly"
	virtual void slotProcessExited();
	
	/// Default implementation does nothing
	virtual void slotProcessStarted();

	/// Calls jobErrored with the error code and error description
	virtual void slotProcessError( QProcess::ProcessError );

	/// Default imp calls slotProcessOutputLine for each line
	virtual void slotProcessOutput( const QByteArray & output, QProcess::ProcessChannel );

	/// Default imp does nothing
	virtual void slotProcessOutputLine( const QString & output, QProcess::ProcessChannel );

	void syncFile( const QString & );

	/// pushes changes to mStdOut and mStdErr to the database
	virtual void updateOutput( bool overrideLogging = false );

	/// Calls checkMemory and checkCpuTime, and updates mLastStatsTime
	virtual void checkStats();
	
	/// see how much memory the spawned process tree is using, makes sure it dosn't go over the allowed amount.
	virtual void checkMemory();
	virtual void checkCpuTime();
	
	void slotCopyFinished();
	void slotCopyError( const QString & text, int );

	virtual void cleanup();
protected:
	/// Return value specifies the delay in ms before the next cleanup stage
	/// ignored for the last stage(CleanupDone)
	virtual int cleanupStage( CleanupStage mode );

	void deliverOutput( QProcess::ProcessChannel );
	bool checkIgnoreLine( const QString & );
	void openLogFiles();

	/// This is only filled in after JobBurner;:cleanup has been called.
	/// Used to kill any lingering processes after they have reasonable time
	/// to exit on their own 
	QList<qint32> processIds() const;

	Slave * mSlave;
	QProcess * mCmd;
	Job mJob;
	JobAssignment mJobAssignment;
	JobTaskAssignmentList mTaskAssignments;
	JobTaskList mTasks;
	
	Service mPrimaryService;
	
	//JobTaskList mCurrentTasks;
	struct RunningTask {
		int taskNumber;
		QDateTime started;
		Interval startedCpuTime;
		JobTaskList tasks;
		JobTaskAssignmentList taskAssignments;
		JobOutput output;
	};

	typedef QList<RunningTask> RunningTaskList;
	RunningTaskList mRunningTasksList, mDoneTasksList;

	RunningTaskList::iterator findRunningTaskIt( int taskNumber, bool searchDoneTasks = false );

	bool mLoaded, mTaskTimeWarned;
	QDateTime mStart, mLastTaskEnded, mLastStatsTime, mLastStatsTime2;
	QString mBurnFile, mTaskList;
	JobCommandHistory mJobCommandHistory;
	QTimer * mOutputTimer, * mStatsTimer, * mCheckupTimer;
	QString mStdOut, mStdErr;
	State mState;
	Options mOptions;
	QList<QRegExp> mIgnoreREs, mErrorREs;
	SpoolRef * mCurrentCopy;
	Win32ShareManager::MappingRef mMappingRef;
	int mMappingDelayCount;
	bool mLogFilesReady;
	QFile * mStdOutFile, * mStdErrFile;
	// Store the process id so we can kill the child processes during cleanup
	qint32 mProcessId;
	QList<qint32> mProcessIds;
	ProcessMemInfo mLastMemoryInfo;
	Interval mLastCpuTime, mLastCpuTime2;
	bool mMemoryPriorityLowered;
	
	// If we detect low cpu usage and the job is over it's task time limit,
	// we record the total cpu time and point when it was first noticed.
	// Then we can wait a certain amount of time for the task to finish or the
	// cpu usage to ramp back up, before deciding to kill the job
	QDateTime mLowCpuStart, mLowCpuDecide;
	Interval mLowCpuTime;

	CleanupStage mCleanupStage;
	
	friend class Slave;
};

/// @}

#endif // JOB_BURNER_H
