
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of RenderLine.
 *
 * RenderLine is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * RenderLine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RenderLine; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifdef COMPILE_MAX_BURNER

#ifndef MAX_BURNER_H
#define MAX_BURNER_H

#include <service.h>

#include "jobburner.h"

/*
 * This class is used to start,
 * and moniter 3dsmax using 3dsmaxcmd.exe
 *
 * Requires MAX_XXXX services with linked software entries
 */

class QTimer;

class MaxBurnerBase : public JobBurner
{
public:
	MaxBurnerBase( const JobAssignment & jobAssignment, Slave * slave, int options = OptionMergeStdError );
	~MaxBurnerBase();
	
protected:
	int maxVersion();
	QString maxDir();
	bool fixPermissions();
	bool cleanUserMacroTempFiles();

	// Number of MaxBurner instances currently instanciated, to prevent
	// certain cleanup functionality when another copy of max is running.
	static int sInstanceCount;
};
/// \ingroup Assburner
/// @{

class MaxBurner : public MaxBurnerBase
{
Q_OBJECT
public:
	MaxBurner( const JobAssignment & jobAssignment, Slave * slave );

	QStringList processNames() const;

	bool checkup();

public slots:
	void startProcess();

public slots:
	void slotProcessOutputLine( const QString & line, QProcess::ProcessChannel );

protected:
	int cleanupStage(CleanupStage);

	QStringList buildCmdArgs();
	QString executable();
	QString startupScriptDest();
	QString workingDirectory();

	// Used to support dynamic plugin.ini's, because 3dsmaxcmd.exe doesn't support -p option
	QString pluginIniPath();
	bool modifyPluginIni();
	bool restorePluginIni(bool useDefault);
	bool verifyPluginIni();

	// Currently used to setup extra exr channels and attributes
	bool generateStartupScript();
	void toggleMentalRayLogging( bool restore );
	void disableMacroRecorder();
	
	void cleanupTempFiles();

	QStringList mWindowTitleMatches;
	
	int mFrame;
	QString mCurrentOutput;
	QString mStartupScriptPath;

	QString mRestorePluginIniPath;
	bool mOriginalPluginIniIsDefault;

	// Variables to restore mentalray logging settings
	int mMRVerbosityInfo, mMRVerbosityProgress, mMRVerbosityDebug;
};

/// @}

#endif // MAX_BURNER_H

#endif
