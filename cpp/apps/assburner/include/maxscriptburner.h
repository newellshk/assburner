
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of RenderLine.
 *
 * RenderLine is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * RenderLine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RenderLine; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifdef COMPILE_MAXSCRIPT_BURNER

#ifndef MAX_SCRIPT_BURNER_H
#define MAX_SCRIPT_BURNER_H


#include "jobburner.h"
#include "maxburner.h"

class QProcess;

/// \ingroup Assburner
/// @{

/**
 * This burner is used to run
 * 3dsmax.exe with a maxscript.
 * It communicates via file.
 * Tested with Max7 and Max8
 */
class MaxScriptBurner : public MaxBurnerBase
{
Q_OBJECT
public:
	MaxScriptBurner( const JobAssignment & jobAssignment, Slave * slave, int options = OptionMergeStdError  );
	~MaxScriptBurner();

	virtual QStringList processNames() const;
	virtual bool checkup();
	virtual QString runScriptSource();
	// This is used to process each line coming from maxscript via
	// the communication file that is setup.  This is here so that
	// burners overriding the MaxScriptBurner class can customize
	// the behavior
	// Returns true for success, false for error
	virtual bool processMaxScriptMessage( const QString & messageLine );
	
	QStringList customEnvironment();
	
public slots:
	void startBurn();
	QString executable();
	QStringList buildCmdArgs();

protected:
	int cleanupStage(CleanupStage);
	QString runScriptDest();
	QString framesFile();
	bool ensureStartupScript();
	
	QStringList mWindowTitleMatches;
	int mStatusLine;
	QString mStatusFile;
	bool mDebugMode;
};

/// @}

#endif // MAX_SCRIPT_BURNER_H

#endif

