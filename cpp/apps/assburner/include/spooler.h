
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of RenderLine.
 *
 * RenderLine is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * RenderLine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RenderLine; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef SPOOLER_H
#define SPOOLER_H

#include <qobject.h>
#include <qpointer.h>

#include <string>

#include "abstractdownload.h"
#include "job.h"

class QProcess;
class QTimer;
class QFtp;

class Spooler;

/// \ingroup Assburner
/// @{

class SpoolRef;

/**
 * SpoolItem - Manages the files for a single job
 *  Has one or more references each with their own directory
 *  that has a copy of any files associated with this item
 */
class SpoolItem : public QObject
{
Q_OBJECT
public:
	SpoolItem( Spooler * spooler, const Job & job );
	virtual ~SpoolItem();

	void ref();

	void startCopy();

	enum State {
		New,
		Copy,
		Done,
		Errored,
		Cancelled
	};

	State state() const { return mState; }

	bool isCopied() { return mCopied; }
	QString destFile() const { return mDestFile; }

	bool isZip();

	AbstractDownload * download();

	Spooler * spooler() const { return mSpooler; }
signals:
	// Gui copy progress
	void copyProgressChange( int );

	void errored( const QString & msg, int state );
	void stateChanged( int oldState, int newState );
	void cancelled();
	void completed();

public slots:
	void downloadProgress( qint64 done, qint64 total );
	void downloadStateChanged( int state );

protected:
	void cleanupDownload();
	// Called by deref when ref count reaches 0
	void cancelCopy();

	bool checkMd5();

	void _finished();
	void _error( const QString & msg );
	void _changeState( State state );

	State mState;
	int mRefCount;
	
	void deref();
	
	Job mJob;
	QString mSrcFile, mDestFile;
	quint64 mSizeUsed;
	
	Spooler * mSpooler;

	AbstractDownload * mDownload;

	bool mCopied, mUnzipped;
	// To guarantee a unique directory for each burner
	int mNextDirNumber;

	friend class SpoolRef;
	friend class Spooler;
};

class SpoolRef : public QObject
{
Q_OBJECT
public:
	SpoolRef( SpoolItem * item );
	~SpoolRef();

	enum State {
		New,
		Copy,
		Unzip,
		Done,
		Errored,
		Cleaned
	};

	void start();

	SpoolItem * spoolItem() const { return mSpoolItem; }
	QString directory() const;
	QString burnFile();
	bool isUnzipped() const;

signals:
	// Gui copy progress
	void copyProgressChange( int );

	void errored( const QString & msg, int state );
	void stateChanged( int oldState, int newState );
	void cancelled();
	void completed();

public slots:
	void cancel();

protected slots:
	void slotCopyCompleted();
	void slotCopyErrored( const QString & msg, int state );

	void unzipExited();
	void unzipReadyRead();
	void unzipTimedOut();

protected:
	void cleanupDirectory();
	void _changeState( State state );
	void _error( const QString & msg );
	void _finished();

	bool unzip();
	void checkUnzip();

	int mState;
	SpoolItem * mSpoolItem;
	QTimer * mCheckupTimer;
	QProcess * mUnzipProcess;
	bool mUnzipped;
	int mUnzipFailureCount;
	int mUnzipTimeout;
	QString mDirectory, mBurnFile;
	friend class SpoolItem;
};

typedef QList<SpoolItem*> SpoolList;
typedef QList<SpoolItem*>::Iterator SpoolIter;

class Spooler : public QObject
{
Q_OBJECT
public:
	// If allowCleanup is false then we are not allowed to clean
	// files from the spool directory
	Spooler( QObject * parent, bool allowCleanup = true );
	~Spooler();

	SpoolItem * startCopy( const Job & job );

	bool ensureDriveSpace( quint64 );

	bool checkDriveSpace( quint64 );

	// Returns the SpoolItem * associated witth he arg, or 0 if not found
	SpoolItem * itemFromJob( const Job & );

	QString spoolDir() const;

public slots:
	void cleanup(quint64 cleanupIdelBytes=0);

	// This function is called when disk space is
	// needed.  It deletes old unneeded files
	// in assburnerSpoolDir.
	// Returns the number of bytes freed
	int fileCleanup();

protected:
	void addDownloadStat( const QString &, const Job &, int, int );

	void removeSpoolItem( SpoolItem * );

	QString mSpoolDir;
	SpoolList mSpool;
	
	bool mRecordDownloadStats, mAllowCleanup;

	QTimer * mCheckTimer;

	friend class SpoolItem;
};

std::string add_suffix(float val);

/// @}

#endif // SPOOLER_H

