
from optparse import OptionParser
import subprocess, sys

parser = OptionParser(usage="usage: %prog [options] mantra_path ifd_path start_frame end_frame", version="%prog 1.0")
options, args = parser.parse_args()

if len(args) < 4:
	parser.error('At least one of the mandatory options are missing.')
elif len(args) > 4:
	parser.error('Too many arguments provided.')
else:
	mantraPath = args[0]
	ifdPath = args[1]
	startFrame = args[2]
	endFrame = args[3]

def renderMantra(mantraPath, ifdPath, startFrame, endFrame):
	for f in range(int(startFrame), (int(endFrame) + 1)):
		print 'Starting Frame {num}.'.format(num=str(f))
		sys.stdout.flush()
		try:
			output = subprocess.check_output(
				[mantraPath, '-V5', '-f', ifdPath.replace('$F4','%04i' % f)]
			)
		except subprocess.CalledProcessError as e:
			print e.output
			print 'Error: mantra returned nonzero return code'
			sys.exit(e.returncode)
		print 'Frame {num} complete.'.format(num=str(f))
		sys.stdout.flush()

renderMantra(mantraPath, ifdPath, startFrame, endFrame)


