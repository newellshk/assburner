
import sys, traceback
from blur.Stone import Interval
from blur.Classes import Notification, Host
from blur.logredirect import RedirectOutputToLog

RedirectOutputToLog()

def do_imports(imports):
	# Split lines, remove whitespace
	lines = (line.strip() for line in imports.splitlines())
	# and skip empties
	lines = (line for line in lines if line)
	for line in lines:
		try:
			exec(line)
		except Exception as e:
			Notification.periodic( 'Assburner','Plugin Import Failure', Interval('1 day'), \
				'Assburner plugin failure on %s:\n%s' % (Host.currentHostName(), traceback.format_exc()) )

imports = """
import aftereffects
import auto_reboot
import cinema4d
import clientupdate
import fusion
import hardware_info
import houdini
import mantra
import nuke
import realflow
import tempfile_cleaner
import vray_spawner
import xsi
"""

do_imports( imports )


win_imports = """
import fumefx_sim
"""

if sys.platform=='win32':
	do_imports( win_imports )
