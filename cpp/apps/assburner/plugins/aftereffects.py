
from blur.Stone import *
from blur.Classes import *
from blur.Assburner import *
from PyQt4.QtCore import *
from PyQt4.QtSql import *
import re, traceback, os, time

#from subprocess import *
import subprocess,platform

qs = QString

class AfterEffectsBurner(JobBurner):
	def __init__(self,jobAssignment,slave):
		JobBurner.__init__(self,jobAssignment,slave)
		self.Job = jobAssignment.job()
		self.Slave = slave
		
		self.CurrentFrame = None
		self.LastFrame = None
		
		self.ExpectingFramePath = None

		self.frameDone = QRegExp("PROGRESS:\\s+\\(?(Skipping)?\\s*[\\d;+]*\\)?\\s*\\((\\d+)\\):")
	#        self.frameStart = QRegExp("M2AB: starting frame")
		self.jobDone = QRegExp("Total Time Elapsed")

		self.errors = []
		self.errors.append(QRegExp("Error: "))
		self.errors.append(QRegExp("ERROR: "))
		self.errors.append(QRegExp("The directory originally specified by an output module for this render item no longer exists."))

		if hasattr(self.Job,'ignoreMissingEffects') and not self.Job.ignoreMissingEffects():
			self.errors.append(QRegExp("INFO:This project contains \\d+ reference"))

	## Names of processes to kill after burn is finished
	def processNames(self):
		return QStringList("aerender") << "aerender.exe"

	def buildCmdArgs(self):
		job = self.Job
		args = QStringList()

		frameStart = int(self.assignedTasks().section("-",0,0))
		frameEnd = self.assignedTasks().section("-",1,1)

		args << "-project" <<  self.burnFile()

		if job.comp():
			args << "-comp" << job.comp()

		args << "-s" << qs.number(frameStart)

		if not frameEnd or frameEnd < 0:
			args << "-e" <<  qs.number( frameStart )
			self.LastFrame = frameStart
		else:
			args << "-e" << qs.number( int(frameEnd) )
			self.LastFrame = int(frameEnd)
		
		self.CurrentFrame = frameStart
		return args

	def executable(self):
		return os.path.join(unicode(self.software().installedPath()), 'aerender.exe')

	def startProcess(self):
		outputDir = os.path.dirname(str(self.job().outputPath()))
		if not os.path.exists(outputDir):
			self.logMessage( "AE::startBurn() creating output dir: %s" % (outputDir) )
			os.mkdir(outputDir)
			os.chmod(outputDir,0777)

		JobBurner.startProcess(self)
		self.taskStart(self.CurrentFrame)

	def slotProcessOutputLine(self,line,channel):
		# Error checking
		for errorx in self.errors:
			if errorx.indexIn( line ) >= 0:
				self.jobErrored(line)
				return

		if self.jobDone.indexIn( line ) >= 0:
			self.jobFinished()
		elif self.CurrentFrame is not None and self.frameDone.indexIn( line ) >= 0:
			self.taskDone( self.CurrentFrame )
			self.CurrentFrame += 1
			if self.CurrentFrame > self.LastFrame or not self.taskStart(self.CurrentFrame):
				self.CurrentFrame = None
			#self.ExpectingFramePath = makeFramePath( self.Job.outputPath(), self.CurrentFrame)
			
	#def checkup(self):
		#JobBurner.checkup(self)
		#if self.ExpectingFramePath:
			#self.logMessage( "AE::slotProcessOutputLine() checking for frame at %s" % self.ExpectingFramePath)
			#if os.path.exists(self.ExpectingFramePath) and os.path.getsize(self.ExpectingFramePath) > 129:
				#self.taskDone( self.CurrentFrame )
				##self.fileGenerated( framePath )

				#frameNth = self.Job.frameNth()
				#frameEnd = self.Job.maxTaskNumber()
				#if frameNth > 0 and self.CurrentFrame < frameEnd:
					#self.copyFrameNth( self.CurrentFrame, frameNth, frameEnd, self.Job.frameNthMode() == 1 )

				#self.CurrentFrame += 1
				#if self.CurrentFrame > self.LastFrame or not self.taskStart(self.CurrentFrame):
					#self.CurrentFrame = None
				
				#self.ExpectingFramePath = None
				
	#def slotProcessExited(self):
		#self.checkup()
		#if self.ExpectingFramePath:
			#self.jobErrored( framePath + " does not exist or is empty frame" )


class AfterEffectsBurnerPlugin(JobBurnerPlugin):
	def jobTypes(self):
		return QStringList('AfterEffects8') << 'AfterEffects9' << 'AfterEffects10'

	def createBurner(self,jobAssignment,slave):
		Log( "AEBurnerPlugin::createBurner() called, Creating AfterEffectsBurner" )
		return AfterEffectsBurner(jobAssignment,slave)

JobBurnerFactory.registerPlugin( AfterEffectsBurnerPlugin(), True )

