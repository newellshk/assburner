from blur.Stone import *
from blur.Classes import *
from blur.Assburner import *
from PyQt4.QtCore import *
from PyQt4.QtSql import *
import re, traceback, sys

class Cinema4DBurner(JobBurner):
	def __init__(self,jobAssignment,slave):
		JobBurner.__init__(self,jobAssignment,slave)
		self.CurrentFrame = None
		self.PreR13 = False
		self.NeedCleanupDelay = True
		
		# Pre R13 variables needed for detecting frames
		self.FileServerClockSkew = 0
		self.RenderStarted = False
		self.JobDone = False
		
		# Some jobs spew tons of these
		self.addIgnoreRegEx( "Node is already in TexData list" )
		
	def buildCmdArgs(self):
		args = QStringList()

		if self.software().version() < 13:
			self.PreR13 = True
		
			# Hack for how c4d handles output paths, S:/frame_02.tif will output S:/frame_02_0001.tif, S:/frame_02_0002.tif, etc
			opfi = QFileInfo(self.job().outputPath())
			op = opfi.completeBaseName()
			if op.size() and op.at(op.size()-1).isDigit():
				op += '_'
			self.OutputPath = opfi.path() + QDir.separator() + op + "." + opfi.suffix()
			self.calculateClockSkew()
		
		args << '-nogui'
		args << '-render'
		args << self.burnFile().replace('/','\\')
		args << '-frame'
		if self.job().videoMode():
			args << str(self.job().videoStartFrame()) << str(self.job().videoEndFrame())
			self.StartFrame = self.job().videoStartFrame()
			self.FrameCount = self.job().videoEndFrame() - self.StartFrame + 1
		else:
			try:
				frameList = expandNumberList( self.assignedTasks() )[0]
				self.StartFrame = frameList[0]
				self.EndFrame = frameList[-1]
				args << str(self.StartFrame) << str(self.EndFrame)
			except:
				self.jobErrored('Unable to parse frame list: ' + str(self.assignedTasks()) )
		
		return args
	
	def cleanupStage(self,stage):
		if stage == JobBurner.CleanupCopy and self.NeedCleanupDelay:
			self.NeedCleanupDelay = False
			delay = '10 seconds'
			self.logMessage( "Cinema4DBurner.cleaupStage() waiting " + delay )
			return Interval.fromString(delay)[0].asOrder(Interval.Milliseconds)
		return JobBurner.cleanupStage(self,stage) 
		
	def checkup(self):
		if self.PreR13:
			self.preR13Checkup()
		return JobBurner.checkup(self)
	
	def slotProcessStarted(self):
		if self.PreR13:
			self.startFrame( self.StartFrame )
	
	def slotProcessExited(self):
		if self.PreR13:
			if not self.JobDone:
				self.checkup()
			if self.JobDone:
				self.jobFinished()
				return
		JobBurner.slotProcessExited(self)

	def slotProcessOutputLine(self,line,channel):
		if re.search( 'Enter Registration Data', line ):
			self.jobErrored( 'Cinema4d Spewing registration requests, needs registered on this host' )
			return
			
		mo = re.search( 'Rendering frame ([-\\d]+) at <(.+)>', line )
		if mo:
			frame = int(mo.group(1))
			startTimeString = mo.group(2)
			startTime = QDateTime.fromString(startTimeString, 'ddd MMM dd hh:mm:ss yyyy')
			if self.job().videoMode():
				if self.taskStarted():
					self.CurrentFrame = frame
					progress = (self.CurrentFrame - self.StartFrame) / self.FrameCount
					self.currentTasks().setProgresses( int( 100.0 * progress ) ).commit()
				else:
					self.taskStart( 1 )
			else:
				if self.CurrentFrame is not None:
					self.taskDone( self.CurrentFrame, startTime )
				self.CurrentFrame = frame
				self.taskStart( self.CurrentFrame, QString(), startTime )

		mo = re.search( 'Rendering successful', line )
		if mo:
			if self.job().videoMode():
				self.currentTasks().setProgresses(100).commit()
				self.taskDone(1)
			else:
				self.taskDone(self.CurrentFrame)
			self.jobFinished()
		
		JobBurner.slotProcessOutputLine(self,line,channel)

	def calculateClockSkew(self):
		path = os.path.join(unicode(self.OutputPath), 'clock_skew_test.txt')
		csf = QFile(path)
		if not csf.open( QIODevice.WriteOnly ):
			self.jobErrored('Unable to open clock skew test file at %s' % path)
		csf.write('clock skew test')
		csf.close()
		self.FileServerClockSkew = QDateTime.currentDateTime().secsTo( QFileInfo(path).lastModified() )
		self.logMessage('File server clock skew set to %i' % self.FileServerClockSkew)
		QFile.remove(path)

	def startTimeUnskewed(self):
		return self.startTime().addSecs( self.FileServerClockSkew )

	def taskPath(self,task):
		path = None
		if task.jobOutput().isRecord() and RangeFileTracker(task.jobOutput().fileTracker()).isRecord():
			path = task.jobOutput().fileTracker().filePath(task.frameNumber())
		else:
			path = makeFramePath( self.OutputPath, task.frameNumber() )
		return path

	def startFrame(self,frameNumber):
		if not self.taskStart(frameNumber):
			return False
		self.CurrentFrame = frameNumber
		self.OutputPaths = {}
		for task in self.currentTasks():
			# RangeFileTracker::filePath( int frameNumber ), returns full frame path
			path = self.taskPath(task)
			if path is not None:
				self.logMessage( 'Adding path to check for current frame output ' + path )
				self.OutputPaths[path] = False
			else:
				self.jobErrored( 'Unable to get valid output path for frame %i, ID: %i' % (task.frameNumber(), task.key()) )
				return False
		return True

	# Pre R13 Code for detecting frame output files
	def preR13Checkup(self):
		if self.JobDone: return True
		while self.CurrentFrame is not None:
			allFramesDone = True
			for path, checked in self.OutputPaths.iteritems():
				if not checked:
					self.logMessage( "Checking Path: " + path )
					allFramesDone = False
					fi = QFileInfo( path )
					if not fi.exists():
						self.logMessage( "File not found" )
						break
					if not fi.isFile():
						self.logMessage( "Path does not point to a file" )
						break
					if not Path.checkFileFree( path ):
						self.logMessage( "File is not free" )
						break
					if not (fi.lastModified() > self.startTimeUnskewed()):
						self.logMessage( "File is old, last modified: %s" % fi.lastModified().toString() )
						break
					# Mark this frame as finished
					allFramesDone = True
					self.OutputPaths[path] = True
					self.logMessage( 'Frame Output Finished: ' + path )
			
			if allFramesDone:
				self.logMessage( 'All outputs finished for frame: ' + str(self.CurrentFrame) )
				self.taskDone( self.CurrentFrame )
				if self.CurrentFrame == self.EndFrame:
					# Give 5 seconds for the object buffer passes to be written since we dont
					# know about them or check for them yet
					self.JobDone = True
					QTimer.singleShot( 30000, self, SLOT('jobFinished()') )
					return True
				if not self.startFrame( self.CurrentFrame + 1 ):
					return False
			else:
				break
		return None

class Cinema4DBurnerPlugin(JobBurnerPlugin):
	def jobTypes(self):
		return QStringList('Cinema4D') << 'Cinema4D R11'

	def createBurner(self,jobAssignment,slave):
		disableWindowsErrorReporting( 'cinema4d.exe' )
		return Cinema4DBurner(jobAssignment,slave)

JobBurnerFactory.registerPlugin( Cinema4DBurnerPlugin() )
