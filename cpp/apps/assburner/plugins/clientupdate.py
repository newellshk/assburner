
from blur.Stone import *
from blur.Classes import *
from blur.Assburner import *
from PyQt4.QtCore import *

# This class starts a batch file that reinstalls assburner and other libraries and applications
# In order to update assburner, qt, etc. All software that is using it must be terminated
# including assburner itself.  So this class starts the batch file, then terminates assburner.
# The batch file will report success or failure through the clientupdateutil python module
# If the host is left in a state where the clientupdateutil cannot update the database
# and assburner cannot restart, then the reclaim_tasks daemon will insert a generic job error.
class ClientUpdateBurner(JobBurner):
	def burnDir(self):
		return "c:/blur/"
	
	def generatedScriptDest(self):
		return self.burnDir() + 'client_update.bat'
		
	def generateBatchScript(self):
		# Reload the config entry just in case it has changed
		#c = Config.recordByName( "assburnerClientUpdatePath" )
		#c.reload()
		#clientUpdateBatchPath = c.value()
		clientUpdateBatchPath = self.job().cmd()
		ourTasks = JobTask.select( (JobTask.c.Job == self.job()) & (JobTask.c.Host == Host.currentHost()) )
		if len(ourTasks) != 1:
			self.jobErrored( "There should be a single task assigned to this host, got: " + ourTasks.debug() )
			return
		taskKey = ourTasks[0].key()
		batchSrc = readFullFile( ":/runClientUpdate.bat" )[0]
		ret = batchSrc.replace( "%{clientUpdateBatchPath}",clientUpdateBatchPath ).replace( "%{taskKey}", str(taskKey) )
		return ret

	def writeBatchFile(self):
		return writeFullFile( self.generatedScriptDest(), self.generateBatchScript() )
	
	# By starting the process with QProcess.startDetached, we avoid letting the JobBurner class know
	# about the process, therefore it won't be able to clean up/kill the process when assburner
	# terminates
	def startProcess(self):
		self.writeBatchFile()
		if not QProcess.startDetached( 'cmd.exe', ['/c',self.generatedScriptDest()], self.burnDir() ):
			self.jobErrored( "Failed to start client update command: " + cmd )
			return
		self.logMessage( 'Started batch file at ' + self.generatedScriptDest() )
		for taskNumber in expandNumberList(self.assignedTasks())[0]:
			self.taskStart( taskNumber )
		self.logMessage( 'Tasks set to busy, about to exit' )
		# Terminate assburner without job cleanup
		self.slave().clientUpdate()
		
class ClientUpdateBurnerPlugin(JobBurnerPlugin):
	def jobTypes(self):
		return QStringList('ClientUpdate')

	def createBurner(self,jobAssignment,slave):
		return ClientUpdateBurner(jobAssignment,slave)

JobBurnerFactory.registerPlugin( ClientUpdateBurnerPlugin() )
