
from blur.Stone import *
from blur.Classes import *
from blur.Assburner import *
from PyQt4.QtCore import *
from PyQt4.QtSql import *
import traceback, os

class DelightBurner(JobBurner):
	def __init__(self,jobAssignment,slave):
		JobBurner.__init__(self,jobAssignment,slave)
		self.CurrentFrame = None
		self.frameList = []
		self.StartFrame = None
		self.EndFrame = None

		# Rendering /drd/jobs/hf2/global/rnd/surfdev/artur.vill/empland/houdini/ribs/empland.1191.rib
		if self.burnFile().endsWith("..rib"):
			self.frameStart = QRegExp("^# Rendering.*\.(\d+)\.rib")
		else:
			self.frameStart = QRegExp("^# Rendering.*\.rib")

		# Statistics for frame 1049
		self.frameDone = QRegExp("Statistics for frame (\d+)")

		self.jobDone = QRegExp("^baztime:")

		self.errors = []
		self.errors.append(QRegExp("cannot open output file"))
		#self.errors.append(QRegExp("^3DL SEVERE ERROR"))

	def environment(self):
		env = self.job().environment()
		Log( "DelightBurner::environment(): %s" % env )
		return env.split("\n")

	def buildCmdArgs(self):
		self.frameList = expandNumberList( self.assignedTasks() )[0]
		self.StartFrame = self.frameList[0]
		self.EndFrame = self.frameList[-1]
		args = QStringList()
		args << "-nd"
		args << "-Progress"
		args << "-t"
		args << str(self.job().threads())

		if self.burnFile().endsWith("..rib"):
			for n in self.frameList:
				args << self.burnFile().replace("..rib", (".%04d.rib" % n))
		else:
			args << "-frames"
			args << str(self.StartFrame) << str(self.EndFrame)
			args << self.burnFile()
		return args

	def executable(self):
		timeCmd = "/usr/bin/time --format=baztime:real:%e:user:%U:sys:%S:iowait:%w ";
		cmd = timeCmd + "/bin/su %s -c \"renderdl " % self.job().user().name()
		cmd = cmd + self.buildCmdArgs.join(" ") + "\""
		return cmd

	def slotProcessOutputLine(self,line,channel):
		JobBurner.slotProcessOutputLine(self,line,channel)

		# Frame status
		if self.frameDone.indexIn(line) >= 0:
			frame = int(self.frameDone.cap(1))
			self.taskDone(frame)
		elif self.frameStart.indexIn(line) >= 0:
			#if self.burnFile().endsWith("..rib"):
			#	frame = int(self.frameStart.cap(1))

			if self.CurrentFrame is None:
				self.CurrentFrame = self.StartFrame
			else:
				self.taskDone(self.CurrentFrame)
				self.CurrentFrame = self.CurrentFrame + 1

			if self.CurrentFrame <= self.EndFrame:
				self.taskStart(self.CurrentFrame)
		elif line.contains(self.jobDone):
			#jobStats = line.split(":")
		  # baztime:real:%e:user:%U:sys:%S:iowait:%w
			#jch = self.jobCommandHistory()
			#jch.setRealtime( jobStats[2] )
			#jch.setUsertime( jobStats[4] )
			#jch.setSystime( jobStats[6] )
			#jch.setIowait( jobStats[8] )
			#jch.commit()
			self.taskDone(self.CurrentFrame)
			self.jobFinished()
		else:
			for e in self.errors:
				if line.contains(e):
					self.jobErrored( line )

class DelightBurnerPlugin(JobBurnerPlugin):
	def jobTypes(self):
		return QStringList('3Delight')

	def createBurner(self,jobAssignment,slave):
		return DelightBurner(jobAssignment,slave)

JobBurnerFactory.registerPlugin( DelightBurnerPlugin() )

