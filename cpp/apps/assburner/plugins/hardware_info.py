
from PyQt4.QtCore import Qt, QDate
from blur.Classes import Host
import sys

def updateHardwareInfo():
	if sys.platform == 'win32':
		import wmi
		host = Host.currentHost()
		c = wmi.WMI()
		vcl = c.Win32_VideoController()
		for vc in vcl:
			if 'hook' in vc.Caption:
				continue
			dateStr = vc.DriverDate
			date = QDate( int(dateStr[0:4]), int(dateStr[4:6]), int(dateStr[6:8]) )
			host.setVideoCard( vc.Caption )
			# If it fails above this then let it fail, but if we can at least get the card type then we'll commit that
			try:
				host.setVideoCardDriver( vc.DriverVersion + ' ' + date.toString(Qt.ISODate) )
				mb = vc.AdapterRAM / (1024 * 1024)
				if mb < 0:
					mb = -mb
				host.setVideoMemory( mb )
			except:
				pass
			break

		try:
			mbi = c.Win32_ComputerSystem()[0]
			host.setModel( mbi.Model )
			host.setManufacturer( mbi.Manufacturer )
		except: pass
		
		host.commit()

		# Disk free space per drive
		try:
			from win32api import GetLogicalDriveStrings, GetDiskFreeSpace
			from win32file import DRIVE_FIXED, GetDriveType
			driveSpaceStrings = []
			for driveString in GetLogicalDriveStrings().split('\x00'):
				if GetDriveType(driveString) == DRIVE_FIXED:
					sectorsPerCluster, bytesPerSector, freeClusters, totalClusters = GetDiskFreeSpace(driveString)
					mbPerCluster = sectorsPerCluster * bytesPerSector / (1024 * 1024.)
					driveSpaceStrings.append( "%s,%i,%i" % (driveString[0], totalClusters * mbPerCluster, freeClusters * mbPerCluster) )
			host.hostStatus().setDriveSpace(';'.join(driveSpaceStrings)).commit()
		except:
			import traceback
			traceback.print_exc()

if __name__=="__main__" and hasattr(sys,'argv'):
	from blur.quickinit import *

updateHardwareInfo()