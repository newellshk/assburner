
from blur.Stone import *
from blur.Classes import *
from blur.Assburner import *
from blur.defaultdict import *
from PyQt4.QtCore import *
from PyQt4.QtSql import *
import re, traceback, os

class HoudiniBurner(JobBurner):
	def __init__(self,jobAssignment,slave):
		JobBurner.__init__(self,jobAssignment,slave)
		self.RenderStarted = False
		self.HoudiniDir = None
		self.CurrentFrame = None
		self.EndFrame = None
		self.ErrorTexts = [
			'No licenses could be found to run this application',
			'Please check for a valid license server host',
			'Error Loading:' ]
		self.Traceback = None
		
	# Names of processes to kill after burn is finished
	def processNames(self):
		return QStringList() << "hbatch.exe" << "hython.exe"
	
	def executable(self):
		return os.path.join(unicode(self.software().installedPath()), 'bin/hython.exe')
		
	def buildCmdArgs(self):
		args = QStringList()
		args << 'houdini_ifd_gen.py'
		args << self.burnFile()
		args << self.job().renderNodeName()
		args << self.job().outputPath()
		tasks = self.assignedTasks()
		if '-' in tasks:
			(startFrame,endFrame) = str(self.assignedTasks()).split('-')
			args << startFrame << endFrame
			self.EndFrame = int(endFrame)
		else:
			# Single frame
			self.EndFrame = int(tasks)
			args << tasks << tasks
		return args

	def slotProcessOutputLine(self,line,channel):
		line = unicode(line)
		
		# We are collecting all the lines of a traceback before recording an error
		if self.Traceback is not None:
			self.Traceback.append(line)
			# If not indented(more than the usual one space after the time), then it's the last line
			if not re.match( r'^\s{2,}', line ):
				self.jobErrored( ','.join(self.Traceback) )
			return
		
		if 'Traceback (most recent call last):' in line:
			self.Traceback = [line]
			return
		
		for errorText in self.ErrorTexts:
			if errorText in line:
				self.jobErrored(line)
				return
		
		startMatch = re.match( r'Starting Frame (\d+).', line )
		doneMatch = re.match( r'Frame (\d+) complete.', line )
		if startMatch:
			self.CurrentFrame = int(startMatch.group(1))
			self.taskStart(self.CurrentFrame)
			
		if doneMatch:
			frame = int(doneMatch.group(1))
			if frame != self.CurrentFrame:
				self.jobErrored('Got frame done message for frame {frameDone} when expecting {frameCurrent}'.format( frameDone=frame, frameCurrent=self.CurrentFrame ))
				return
			self.taskDone(frame)
		
	def slotProcessExited(self):
		if self.CurrentFrame == self.EndFrame:
			self.jobFinished()
		else:
			self.jobErrored('Houdini exited unexpectedly')

class HoudiniScriptBurner(HoudiniBurner):
	def buildCmdArgs(self):
		args = QStringList()
		args << self.burnDir() + 'houdini.hip'
		args << self.burnDir() + 'houdini.py'
		tasks = self.assignedTasks()
		
		if '-' in tasks:
			(startFrame,endFrame) = str(self.assignedTasks()).split('-')
			args << startFrame << endFrame
			self.EndFrame = int(endFrame)
		else:
			# Single frame
			self.EndFrame = int(tasks)
			args << tasks << tasks
		return args

class HoudiniBurnerPlugin(JobBurnerPlugin):
	def jobTypes(self):
		return ['Houdini','HoudiniScript']

	def createBurner(self,jobAssignment,slave):
		if jobAssignment.job().jobType().name() == 'HoudiniScript':
			return HoudiniScriptBurner(jobAssignment,slave)
		return HoudiniBurner(jobAssignment,slave)

JobBurnerFactory.registerPlugin( HoudiniBurnerPlugin() )
