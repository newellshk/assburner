
from blur.Stone import *
from blur.Classes import *
from blur.Assburner import *
from blur.defaultdict import *
from PyQt4.QtCore import *
from PyQt4.QtSql import *
import re, traceback, os

class MantraBurner(JobBurner):
	def __init__(self,jobAssignment,slave):
		JobBurner.__init__(self,jobAssignment,slave)
		
	def executable(self):
		return 'python.exe'
	
	def buildCmdArgs(self):
		args = QStringList()
		args << 'mantra_render.py'
		args << os.path.join(unicode(self.software().installedPath()), 'bin', 'mantra.exe')
		args << self.job().fileName()
		tasks = self.assignedTasks()
		if '-' in tasks:
			(startFrame,endFrame) = str(self.assignedTasks()).split('-')
			args << startFrame << endFrame
			self.EndFrame = int(endFrame)
		else:
			# Single frame
			self.EndFrame = int(tasks)
			args << tasks << tasks
		return args
	
	def slotProcessOutputLine(self,line,channel):
		startMatch = re.match( r'Starting Frame (\d+).', line )
		doneMatch = re.match( r'Frame (\d+) complete.', line )
		errorMatch = re.match( r'Error: (.+)$', line )
		if startMatch:
			self.CurrentFrame = int(startMatch.group(1))
			self.taskStart(self.CurrentFrame)
			
		if doneMatch:
			frame = int(doneMatch.group(1))
			if frame != self.CurrentFrame:
				self.jobErrored('Got frame done message for frame {frameDone} when expecting {frameCurrent}'.format( frameDone=frame, frameCurrent=self.CurrentFrame ))
				return
			self.taskDone(frame)

		if errorMatch:
			self.jobErrored(line)
		
	def slotProcessExited(self):
		if self.CurrentFrame == self.EndFrame:
			self.jobFinished()
		else:
			self.jobErrored('Mantra exited unexpectedly')

class MantraBurnerPlugin(JobBurnerPlugin):
	def jobTypes(self):
		return ['Mantra']

	def createBurner(self,jobAssignment,slave):
		return MantraBurner(jobAssignment,slave)

JobBurnerFactory.registerPlugin( MantraBurnerPlugin() )

