
from blur.Stone import *
from blur.Classes import *
from blur.Assburner import *
from PyQt4.QtCore import *
from PyQt4.QtSql import *
import re, traceback, os, time, shutil, stat

#from subprocess import *
import subprocess

qs = QString
qr = QRegExp

_tempPath = '/Local/temp_maya'

class MayaBurner(JobBurner):
    def __init__(self,jobAssignment,slave):
        JobBurner.__init__(self,jobAssignment,slave)
        self.Job = job
        self.Slave = slave

        self.CurrentFrame = None

        self.frameDone = QRegExp("M2AB: completed frame")
        self.frameStart = QRegExp("M2AB: starting frame")
        self.jobDone = QRegExp("M2AB: completed job")
        self.errors = []
        self.errors.append(QRegExp("^Error:.*"))
        self.errors.append(QRegExp("Memory exception"))
        self.errors.append(QRegExp("could not get a license"))
        self.errors.append(QRegExp("Rendering aborted."))
        self.errors.append(QRegExp("maya encountered a fatal error"))
        self.errors.append(QRegExp("Warning: This scene does not have any \
renderable cameras"))
        self.errors.append(QRegExp("Error:.+Plug-in.+was not found"))
        self.errors.append(QRegExp("glibc detected"))
        self.errors.append(QRegExp("cannot open shared object file"))

        self.ignores = []
        self.ignores.append(QRegExp("^Error.*ignored"))
      
        if str(job.jobType().name()).find("MentalRay") >= 0:
            self.ignores.append(QRegExp("Prog: .Mayatomr.Scene. : DAG node:"))
            self.ignores.append(QRegExp("shader .* called mi_sample_light with \
unknown light tag"))

        self.ErrorOutput = ""
             


    def cleanup(self):
        Log( "MayaBurner::cleanup() called" )
        if self.process():
            mProcessId = self.process().pid()
            Log( "MayaBurner::cleanup() Getting pid: %s" % mProcessId )

            # Need to find the correct PID space ..
            if mProcessId > 10:
                descendants = processChildrenIds( mProcessId, True )
                for processId in descendants:
                    Log( "MayaBurner::cleanup() Killing child pid: %s" % processId )
                    killProcess( processId )
                    killProcess( mProcessId )

        # Delete temp files
        try:
            call = subprocess.call
            call("rm -f /usr/tmp/fb*",shell=True)
            call("rm -f /usr/tmp/*.ma",shell=True)
            call("rm -f /usr/tmp/*.mel",shell=True)
            call("rm -f /usr/tmp/*.iff",shell=True)
            call("rm -rf /usr/tmp/mayaDiskCache*",shell=True)

            call("rm -f %s/fb*" % (_tempPath) ,shell=True)
            call("rm -f %s/*.ma" % (_tempPath) ,shell=True)
            call("rm -f %s/*.mel" % (_tempPath) ,shell=True)
            call("rm -f %s/*.iff" % (_tempPath) ,shell=True)
            call("rm -rf %s/mayaDiskCache*" % (_tempPath) ,shell=True)
            call("find %s/img/ -mmin +300 -delete" % (_tempPath) ,shell=True)
        except OSError, e:
            Log( "MayaBurner::cleanup() failed in deleting temp files" )

        JobBurner.cleanup(self)
        Log( "MayaBurner::cleanup() done" )

    # Names of processes to kill after burn is finished
    def processNames(self):
        return QStringList()

    def slotProcessExited(self):
        if self.CurrentFrame:
#            self.taskDone(self.CurrentFrame)
            self.jobFinished()
        else:
            self.jobErrored("process exited before frame was complete")

    def customEnvironment(self):
        env = JobBurner.customEnvironment(self)
        if self.rendererFlag(self.Job.renderer()) == "hw":
            env += ["DISPLAY=:0"]
        return env

    def buildCmdMaya(self):
       
        job = self.Job
 
        self.CurrentFrame = int( self.assignedTasks().section("-",0,0) )
        cmd = " -s " + str( self.CurrentFrame )
        
        frameEnd = self.assignedTasks().section("-",1,1) 
        if frameEnd.isEmpty():
            cmd += " -e " + str( self.CurrentFrame )
        else:
            cmd += " -e " + frameEnd

        if job.width() > 0:
            cmd += " -x " + str( job.width() )
        
        if job.height() > 0:
            cmd += " -y " + str( job.height() )

#        cmd += " -rd " + os.path.dirname(str(job.outputPath()))+"/"
        cmd += " -rd " + os.path.join(_tempPath,"img")

        if not job.projectPath().isEmpty() and  \
           not str(job.append()).find("-proj") >= 0:
            cmd += " -proj " + job.projectPath()
        if not job.camera().isEmpty():
            cmd += " -cam " + job.camera()
        if not job.append().isEmpty():
            cmd += " " + job.append()

        cmd += " -renderer " + self.rendererFlag(job.renderer())

        if not job.append().isEmpty() and \
            str(job.append()).find("defaultRenderLayer") >= 0  :
            cmd += " -im %s%.4n%.e"
        else:
            cmd += " -im %s%_l%.4n%.e"

        cmd += " " + job.fileName()
        
        return cmd

    def rendererFlag(self, renderer):
        renderer = str(renderer)
        if renderer.lower() == "maya":
            return "sw"
        elif renderer.lower() == "mentalray":
            return "mr"
        elif renderer.lower() == "mayahardware":
            return "hw"
        else:
            return "file"

    def executable(self):
        jt = self.Job.jobType().name()
        user = self.Job.user().name()
        if jt == "Maya85" or jt=="MentalRay85":
            cmd = "su - %s -c \"%s " % (user,"/usr/aw/maya8.5/bin/Render")
        elif jt == "Maya2009":
            cmd = "su - %s -c \"%s " % (user,
                            "/usr/autodesk/maya2009-x64/bin/Render")
        elif jt == "Maya2011":
            cmd = "su - %s -c \"%s " % (user,
                            "/usr/autodesk/maya2011-x64/bin/Render")

        cmd += self.buildCmdMaya()
        cmd += "\""

        return cmd

    def startProcess(self):
        Log( "MayaBurner::startBurn() called" )
        tmpDir = os.path.join(_tempPath,"img")
        if not os.path.exists(tmpDir):
            try:
                os.mkdir(tmpDir)
            except:
                self.jobErrored("Could not create temp directory")

        subprocess.call("chgrp -R staff %s" % (tmpDir),shell=True)
        subprocess.call("chmod -R g+rwX %s" % (tmpDir),shell=True)

        outDir = os.path.dirname(os.path.normpath(str(self.Job.outputPath())))
        user = str(self.Job.user().name())

        if not os.path.exists(outDir):
            try:
                if not outDir.startswith("/mnt/x5/Shows"):
                    self.jobErrored("Invalid output directory") 
                    return
                Log( "MayaBurner::startBurn() creating output dir %s" % (outDir))
                if not self.Job.append().isEmpty() and \
                    str(self.Job.append()).find("defaultRenderLayer") >= 0  :
                    os.mkdir(outDir)
                else:
                    os.makedirs(outDir)
            except Exception,e:
                self.jobErrored("Could not create output directory" + str(e))

        if not self.Job.append().isEmpty() and \
            str(self.Job.append()).find("defaultRenderLayer") >= 0  :
            subprocess.call("chown -R %s:staff %s" % (user,outDir),shell=True)
            subprocess.call("chmod -R g+rwX %s" % (outDir),shell=True)
        else:
            subprocess.call("chown -R %s:staff %s" % (user,os.path.dirname(outDir)),shell=True)
            subprocess.call("chmod -R g+rwX %s" % (os.path.dirname(outDir)),shell=True)

        JobBurner.startProcess(self)
#        self.taskStart(self.CurrentFrame)
        Log( "MayaBurner::startBurn() done" )

    def slotProcessOutputLine(self,line,channel):
        #Log( "MayaBurner::slotReadOutput() called, ready to read output" )
        
        # Error checking
        frameError=False
        for errorx in self.errors:
            if errorx.indexIn( line ) >= 0:
                frameError=True

        for ignorex in self.ignores:
            if ignorex.indexIn( line ) >= 0:
                frameError=False

        if frameError:
            self.ErrorOutput += line
            self.jobErrored( self.ErrorOutput )
        elif self.frameStart.indexIn( line ) >= 0:
            self.taskStart( self.CurrentFrame )
        elif self.jobDone.indexIn( line ) >= 0:
            Log( "MayaBurner::slotProcessOutputLine() jobFinished()" )
            # sleep here cause Maya likes to dump memoryreports and we don't want to force cleanup too early
            time.sleep(2)
            self.jobFinished()
        elif self.frameDone.indexIn( line ) >= 0:
            framePath = makeFramePath( self.Job.outputPath(), \
                        self.CurrentFrame)
            frameTmpPath = os.path.join(_tempPath, "img", os.path.basename(str(framePath)))
            Log( "MayaBurner::slotProcessOutputLine() checking for frame at %s" % frameTmpPath)
            if os.path.exists(frameTmpPath) and os.path.getsize(frameTmpPath) > 129:
                try:
                    Log( "MayaBurner::slotProcessOutputLine() copying frame to %s" % framePath)
                    shutil.copy2(frameTmpPath,str(framePath))
                except:
                    self.jobErrored( "Error copying rendered file." ) 
                self.taskDone( self.CurrentFrame )
#                self.fileGenerated( framePath )            
                Log( "MayaBurner::slotProcessOutputLine() Completed frame: %d" % self.CurrentFrame )

                if self.Job.project().name() == "smallville":
                    self.syncFile( framePath )
        
                frameNth = self.Job.frameNth()
                frameEnd = self.Job.getValue("frameEnd").toInt()
                if frameNth > 0 and self.CurrentFrame < frameEnd:
                    self.copyFrameNth( self.CurrentFrame, frameNth, frameEnd, \
                                self.Job.frameNthMode() == 1 )

                self.CurrentFrame += 1
            else:
                self.ErrorOutput += framePath+" does not exist or is empty frame"
                self.jobErrored( self.ErrorOutput )

    def syncFile(self, path):
        curHost = Host.currentHost()
        if curHost.location().name() == "VCO":
            destPath = "rsync://pollux/root"
        elif curHost.location().name() == "SMO":
            if self.CurrentFrame % 2 == 0:
                destPath = "rsync://arnold/root"
            else:
                destPath = "rsync://northlic01/root"
        cmd = "rsync -aR --numeric-ids %s %s" % (path, destPath)
        Log( "syncFile(): "+cmd )
        proc = subprocess.Popen(cmd.split() ,stdout=subprocess.PIPE,stderr=subprocess.PIPE, shell=False)
            

class MayaBurnerPlugin(JobBurnerPlugin):
    def __init__(self):
        JobBurnerPlugin.__init__(self)

    def jobTypes(self):
        return QStringList('Maya') << 'Maya85' << 'MentalRay85' << 'Maya2009' \
<< 'Maya2011'

    def createBurner(self,jobAssignment,slave):
        Log( "MayaBurnerPlugin::createBurner() called, Creating MayaBurner" )
        return MayaBurner(jobAssignment,slave)

JobBurnerFactory.registerPlugin( MayaBurnerPlugin(), True )

