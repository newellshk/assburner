from blur.Stone import *
from blur.Classes import *
from blur.Assburner import *
from PyQt4.QtCore import *
from PyQt4.QtSql import *
import re, traceback

class VDubConsts:
	ScriptDir = "C:\\Documents and Settings\\All Users\\Application Data\\Sony\\Vegas Pro\\8.0\\Script Menu\\VDub Scripts\\"
	Scripts = { 'lossyImg' : ScriptDir + "lossyRenderScriptFromImages.syl",
				'losslessImg' : ScriptDir + "losslessRenderScriptFromImages.syl",
				'lossy' : ScriptDir + "lossyRenderScript.syl",
				'lossless' : ScriptDir + "losslessRenderScript.syl" }
	Codecs = { 'lossy' : "C:\\Program Files (x86)\\Blackmagic Design\\Blackmagic DeckLink\\BMDCodecMJPG.dll",
				'lossless' : "C:\\Program Files (x86)\\Blackmagic Design\\Blackmagic DeckLink\\BMDCodecLib.dll" }

class ProxyBurner(JobBurner):
	def __init__(self,jobAssignment,slave):
		JobBurner.__init__(self,jobAssignment,slave)
		self.VDDir = None
		
	# Names of processes to kill after burn is finished
	def processNames(self):
		return QStringList() << "vdub.exe"
	
	def isInputFromFrames(self):
		seqExt = os.path.splitext(str(self.job().imageSequence().toLower()))[1]
		return not (seqExt == "avi" or seqExt == "mov")

	def vdubScript( self ):
		proxyType = 'lossy'
		if self.job().lossless():
			proxyType = 'lossless'
		if self.isInputFromFrames():
			proxyType += "Img"
		script = VDubConsts.Scripts[proxyType]
		return script
	
	def checkCodecs( self ):
		for rt in ['lossy','lossless']:
			if not os.path.exists(VDubConsts.Codecs[rt]):
				self.jobErrored( 'Codec missing: ' + VDubConsts.Codecs[rt] )
				return False
		return True
		
	def buildCmdArgs(self):
		args = QStringList()
		if not self.checkCodecs():
			return args
		outputPath = str(self.job().outputPath())
		outputDir = os.path.dirname(outputPath)
		if not os.path.exists(outputDir):
			try:
				os.makedirs(outputDir)
			except:
				self.jobErrored( "Error creating output path: " + outputDir + " error was: " + os.error )
				return QStringList()
		
		args << "/i"
		args << self.vdubScript()
		args << self.job().imageSequence()
		args << outputPath
		if self.isInputFromFrames():
			fps = self.job().fps()
			if fps == 0:
				fps = 24
			args << str(fps)
			#args << str(self.job().frameStart()) << str(self.job().frameEnd())
		return args
	
	def slotProcessOutputLine(self,line,channel):
		# Job started
		if re.match( 'Beginning dub operation.', line ):
			self.RenderStarted = True
			self.taskStart(1)
		
		# Job completion
		if re.match( '^Ending operation.', line ):
			self.taskDone(1)
			self.jobFinished()

class ProxyBurnerPlugin(JobBurnerPlugin):
	def jobTypes(self):
		return QStringList('Proxy')

	def createBurner(self,jobAssignment,slave):
		return ProxyBurner(jobAssignment,slave)

JobBurnerFactory.registerPlugin( ProxyBurnerPlugin() )
