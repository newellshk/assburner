
from blur.Stone import *
from blur.Classes import *
from blur.Assburner import *
from PyQt4.QtCore import *
import re, traceback, sys, os

# Returns a string list of args
def vrimgConvertArgs( inputFile, frameNumber, outputDescJson ):
	for v in outputDescJson.array():
		channelDesc = v.toObject()
		outputPath = channelDesc['path'].toString()
		bitDepth = channelDesc['bitDepth'].toString()
		channel = channelDesc['channel'].toString()

		args = []
		args.append( inputFile )
		args.append( makeFramePath(outputPath,frameNumber) )
		args += ['-channel',channel]
		if bitDepth == '16':
			args.append('-half')
		
		p = QProcess

class VrayBurner(JobBurner):
	def __init__(self,jobAssignment,slave):
		JobBurner.__init__(self,jobAssignment,slave)
		self.vrimgOutputDesc = jobAssignment.job().vrimgOutputDesc()
		self.checkupTimer.setInterval(1000) # Every second
	
        # Names of processes to kill after burn is finished
	def processNames(self):
		return QStringList() << "vray.exe"
	
	def buildCmdArgs(self):
		args = QStringList()
		burnFile = self.burnFile()

		if os.system.startswith('linux'):
			# Until we have JobFile and can correctly do this once per job...
			transDir = os.path.join(self.burnDir(),'vrscene_translated')
			try:
				os.mkdir(transDir)
			except:
				self.jobErrored('Unable to create directory for translating the vray scene')
				return ''
			
			if not Mapping.translateVrayScene(burnFile, transDir):
				self.jobErrored('Error translating the vray scene from windows to linux')
				return ''
			burnFile = os.path.join(transDir,os.path.basename(burnFile))
		
		job = self.job()
		args << "-display=0"
		args << "-sceneFile=" + burnFile
		args << "-autoClose=1"
		args << "-rtEngine=%i" % job.renderEngine()
		args << "-frames=" + self.assignedTasks.replace(',',';')
		args << "-imgFile=" + Mapping.osPath(job.outputPath())
		return args
	
	def slotProcessOutputLine(self,line,channel):
		JobBurner.slotProcessOutputLine(self,line,channel)
		
		if 'error: Cannot create output image file' in line:
			self.jobErrored(line)
			return

		m = re.search(r'Starting frame (\d+)',line)
		if m:
			self.CurrentFrame = int(m.group(1))
			self.taskStart(self.CurrentFrame)
		
		m = re.search(r'Frame took')
		if m:
			self.taskDone(self.CurrentFrame)
			if self.vrimgOutputDesc:
				vrimgConvert(outputFrame, self.vrimgOutputDesc)
		
		if False:
			self.jobFinished()
			

class VrayBurnerPlugin(JobBurnerPlugin):
	def jobTypes(self):
		return QStringList('Vray')

	def createBurner(self,jobAssignment,slave):
		return VrayBurner(jobAssignment,slave)

JobBurnerFactory.registerPlugin( VrayBurnerPlugin() )
