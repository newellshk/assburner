
from blur.Stone import *
from blur.Classes import *
from blur.Assburner import *
from PyQt4.QtCore import *
import sys
import sip
class VraySpawnerBurner(MaxBurnerBase):
	def __init__(self,jobAssignment,slave):
		MaxBurnerBase.__init__(self,jobAssignment,slave)
		self.checkupTimer().setInterval(15000) # 15 second interval
		# Check if it's a max job using vray distributed nodes
		# otherwise all the job is doing is starting vray spawner
		# for user's to take advantage of with their local vray/3dsmax
		job = jobAssignment.job()
		self.IsMaxJob = isinstance(job,JobMax)
		self.PrimaryTaskCount = None
		tasks = self.assignedTaskList()
		assert(tasks.size() == 1)
		self.FrameNumber = tasks[0].frameNumber()
		self.LastCpuCheckTime = None
		self.LastCpuTime = None
		self.CpuBelowCutoffCount = 0
		
	def executable(self):
		try:
			return self.maxDir() + QDir.separator() + "vrayspawner%i.exe" % self.maxVersion()
		except:
			import traceback
			traceback.print_exc()
			self.jobErrored( traceback.format_exc() )
		return ''

	def startProcess(self):
		MaxBurnerBase.startProcess(self)
		self.taskStart(self.FrameNumber)

	def checkup(self):
		# Intentionally omit calling JobBurner::checkup, because it only
		# checks for max task time, which we don't want to support
		if self.IsMaxJob:
			# Get a count of how many images need rendered before we can stop vray spawner
			if self.PrimaryTaskCount is None:
				q = Database.current().exec_("SELECT count(*) FROM JobTask WHERE fkeyJob=%i AND (label IS NULL OR left(label,10) != 'vray_dist_')" % self.job().key())
				assert(q.next())
				self.PrimaryTaskCount = q.value(0).toInt()[0]

			# Get an up-to-date count of finished + cancelled tasks to see if we are done
			jobStatus = self.job().jobStatus(Index.UseSelect)
			if jobStatus.tasksDone() + jobStatus.tasksCancelled() == self.PrimaryTaskCount:
				self.taskDone(self.FrameNumber)
		else:
			done = False
			job = self.job()
			dur = job.autoFinishAfterDuration()
			cdt = QDateTime.currentDateTime()
			pastTimeCutoff = dur.isNull() or Interval(self.startTime(),cdt) > dur
			if pastTimeCutoff and job.autoFinishOnIdle():
				cpuTime = processCpuInfo( self.processId(), True ).totalTime()
				if self.LastCpuCheckTime is not None:
					cpuUse = (cpuTime - self.LastCpuTime) / Interval(self.LastCpuCheckTime,cdt)
					# Using at least 50% of one core, pretty low bar
					if cpuUse < 0.5:
						self.CpuBelowCutoffCount += 1
						if self.CpuBelowCutoffCount >= 4:
							done = True
					else:
						self.CpuBelowCutoffCount = 0
				self.LastCpuCheckTime = cdt
				self.LastCpuTime = cpuTime
			elif pastTimeCutoff:
				done = True
			if done:
				self.taskDone(self.FrameNumber)
				self.jobFinished()
		return True

class VraySpawnerTestBurner(VraySpawnerBurner):
	def executable(self):
		return '/usr/bin/python'
	
	def buildCmdArgs(self):
		return ['null_tester.py']
	

class Test(object):
	def run(self,slave):
		pass
	def test(self,slave):
		try:
			return self.run(slave)
		except:
			import traceback
			traceback.print_exc()
			return False

class VSTest(Test):
	def createJob(self,jobType):
		self.Job = Job.schema().schema().tableByName( 'Job' + jobType ).table().newObject()
		self.Job.setJobType( JobType(jobType) ).commit()
		self.JobService = JobService().setJob(self.Job).setService(Service('MAX2014_64')).commit()
		self.JobTask = JobTask().setJob(self.Job).setFrameNumber(1).setStatus('assigned').setHost(Host.currentHost()).commit()
		
	def assignJob(self):
		status = JobAssignmentStatus('ready')
		self.JobAssignment = JobAssignment().setJob(self.Job).setJobAssignmentStatus(status).commit()
		self.JobTaskAssignment = JobTaskAssignment().setJobAssignment(self.JobAssignment).setJobTask(self.JobTask).setJobAssignmentStatus(status).commit()
		
	def setup(self,slave,jobType):
		self.CS = ChangeSet.create()
		self.CSE = ChangeSetEnabler(self.CS)
		self.createJob(jobType)
		self.assignJob()
		self.Burner = VraySpawnerTestBurner(self.JobAssignment,slave)
		sip.transferto(self.Burner,None)
		#self.Burner, errMessage = JobBurnerFactory.createBurner(self.JobAssignment,slave)
		#if errMessage:
		#	print errMessage

	def startup(self,*args,**kwargs):
		self.setup(*args,**kwargs)
		self.Burner.startBurn()
	
	def teardown(self):
		del self.CSE
		del self.CS
		del self.Job
		del self.JobAssignment
		try:
			self.Burner.cancel()
		except:
			pass
		del self.Burner
		# Allow burner cleanup to complete
		QCoreApplication.instance().processEvents(QEventLoop.AllEvents,50)
	
class VRTest(VSTest):
	def __init__(self,autoFinishDuration=None,autoFinishCpu=False):
		self.AutoFinishDuration = autoFinishDuration
		self.AutoFinishCpu = autoFinishCpu
	
	def createJob(self,jobType):
		VSTest.createJob(self,jobType)
		if self.AutoFinishDuration is not None:
			self.Job.setAutoFinishAfterDuration(self.AutoFinishDuration).commit()
	
	def run(self,slave):
		self.startup(slave,jobType='VraySpawner')
		assert(self.JobTask.status() == 'busy')
		endTime = (self.AutoFinishDuration + Interval(20)).adjust(QDateTime.currentDateTime())
		if self.AutoFinishDuration:
			while QDateTime.currentDateTime() < endTime:
				QCoreApplication.instance().processEvents(QEventLoop.AllEvents,50)
		assert(self.JobTask.status() == 'done')
		self.teardown()
		return True

class MaxTest(VSTest):
	def run(self,slave):
		self.startup(slave,jobType='Max')
		self.teardown()
		return True

class VraySpawnerTester(JobBurnerTester):
	def __init__(self):
		JobBurnerTester.__init__(self)
		print "Created VraySpawnerTester"
	
	def tests(self):
		return [
			lambda: VRTest(autoFinishDuration=Interval(15)),
			lambda: MaxTest()]
	
	def test(self,slave):
		print "VraySpawnerTester"
		success, fail = 0, 0
		for t in self.tests():
			test = t()
			if test.test(slave):
				success+=1
			else:
				fail+=1
		return (success,fail)
	
class VraySpawnerBurnerPlugin(JobBurnerPlugin):
	def __init__(self):
		JobBurnerPlugin.__init__(self)
		self.MaxBurnerPlugin = JobBurnerFactory.plugin('Max')
	
	def jobTypes(self):
		return QStringList('Max') << 'VraySpawner'

	def createBurner(self,jobAssignment,slave):
		job = jobAssignment.job()
		# Check to see if this is a vray spawner task
		if isinstance(job,JobMax) and jobMax.vrayDistributedCount() > 0:
			jtal = jobAssignment.jobTaskAssignments();
			jtl = jtal.jobTaskAssignments()
			if jtl.size() == 1 and jtl[0].label().startsWith( "vray_dist_" ):
				Log( "VraySpawnerBurnerPlugin::createBurner() called, Creating VraySpawnerBurner" )
				return VraySpawnerBurner(jobAssignment,slave)
		elif isinstance(job,JobVraySpawner):
			return VraySpawnerBurner(jobAssignment,slave)
		return self.MaxBurnerPlugin.createBurner(jobAssignment,slave)
	
	def createTester(self,jobTypeName):
		return VraySpawnerTester()

JobBurnerFactory.registerPlugin( VraySpawnerBurnerPlugin() )

