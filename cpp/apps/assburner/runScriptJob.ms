
-- This is used by submitElementsTemplate.ms
global scriptArchiveRoot = "%BURN_DIR%"
global frameListString = "%FRAME_LIST%"
global assburnerStatusFH = undefined
global assburnerStatusFilename = "%STATUS_FILE%"
global runPythonScript = %RUN_PYTHON_SCRIPT%
global runScriptStartupSuccess = false
global pyMain = undefined

function checkForValidSpoolDir =
(
	return (doesFileExist (scriptArchiveRoot + "\\runscriptframes.txt"))
)

function reportInitError errorMessage =
(
	setquietmode true
	local fh = openFile "%STATUS_FILE%" mode:"w"
	errorMessage = _blurString.findReplaceString errorMessage "\n" "\\n"
	format "MaxScript Exception: %" errorMessage to:fh
	stack to:fh
	close fh
	quitMax #noPrompt
)

if checkForValidSpoolDir() then
(
	try (
		-- This will load blurLibrary from the current treeGrunt location
		if ( _blurLibrary == undefined ) then
			filein "$max/stdplugs/stdscripts/baselib/blurStartupMaxLib.ms"

		_blurLibrary.Load "blurFile"
		_blurLibrary.Load "blurString"

		-- loads the script from the maxscript job
		-- This script must contain a runScript function
		-- that takes a task(frame) number
		if( runPythonScript ) then (
			pymax.run "%BURN_DIR%\\script.py"
			pyMain = pymax.import "__main__"
		) else (
			fileIn ("%BURN_DIR%\\script.ms")
		)
	) catch (
		local ce = getCurrentException()
		reportInitError ce
	)
	runScriptStartupSuccess = true
)

function runScriptJob =
(
	setquietmode true
	local fh = openFile "%STATUS_FILE%" mode:"w"
	assburnerStatusFH = fh
	try
	(
		local framesFile = scriptArchiveRoot + "\\runscriptframes.txt"
		
		if( doesFileExist framesFile ) then
		(
			local framearray = #()
			local frameList = _blurFile.readFullFile framesFile
			local framestringarray = filterString frameList ","

			for idx = 1 to framestringarray.count do 
				append framearray (framestringarray[idx] as Integer)

			deleteFile framesFile

			loadMaxFile "%BURN_DIR%/maxhold.mx" quiet:true useFileUnits:true

			for idx = 1 to framearray.count do 
			(
				local frame = framearray[idx]
				format "starting %\n" frame to:fh
				flush fh
				if( runPythonScript ) then (
					local retValue = ((pyMain.__dict__["runScript"]) frame)
					if ( retValue != undefined ) then throw(retValue)
				) else (
					runScript frame
				)
				format "finished %\n" frame to:fh
				flush fh
			)
		) 
		else 
		(
			format "%\n" "Frames File does not exist" to:fh
			flush fh
			return true
		)
	) 
	catch
	(
		local ce = getCurrentException()
		ce = _blurString.findReplaceString ce "\n" "\\n"
		format "MaxScript Exception: %" ce to:fh
		stack to:fh
		close fh
		quitMax #noPrompt
		return false
	)
	
	format "%" "success\n" to:fh
	close fh
	assburnerStatusFH = undefined
	quitMax #noPrompt
)

if runScriptStartupSuccess then
(
	runScriptJob()
)
