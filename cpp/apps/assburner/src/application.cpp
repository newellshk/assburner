
#include "application.h"
#include "connection.h"

Application::Application( int & argc, char ** argv )
: QApplication( argc, argv )
{}

bool Application::notify( QObject * receiver, QEvent * event )
{
	try {
		return QApplication::notify(receiver, event);
	}
	catch( SqlException & se ) {
		LOG_1( "Sql Exception, restarting: " + se.error() + "\n" + se.sql() );
		exit( 666 );
	}
	catch( LostConnectionException & lce ) {
		LOG_1( "Lost Connection Exception, restarting" );
		exit( 666 );
	}
	catch( std::exception & se ) {
		LOG_1( "std::exception, restarting: " + QString(se.what()) );
		exit( 666 );
	}
	return false;
}

CoreApplication::CoreApplication( int & argc, char ** argv )
: QCoreApplication( argc, argv )
{}

bool CoreApplication::notify( QObject * receiver, QEvent * event )
{
	try {
		return QCoreApplication::notify(receiver, event);
	}
	catch( SqlException & se ) {
		LOG_1( "Sql Exception, restarting: " + se.error() + "\n" + se.sql() );
		exit( 666 );
	}
	catch( LostConnectionException & lce ) {
		LOG_1( "Lost Connection Exception, restarting" );
		exit( 666 );
	}
	catch( std::exception & se ) {
		LOG_1( "std::exception, restarting: " + QString(se.what()) );
		exit( 666 );
	}
	return false;
}
