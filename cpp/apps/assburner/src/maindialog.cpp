
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of RenderLine.
 *
 * RenderLine is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * RenderLine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RenderLine; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qprocess.h>
#include <qapplication.h>
#include <qdir.h>
#include <qprogressbar.h>
#include <qpushbutton.h>
#include <qglobal.h>
#include <qlabel.h>
#include <qmessagebox.h>
#include <qgroupbox.h>
#include <qmenu.h>
#include <qevent.h>
#include <qfile.h>
#include <qfileinfo.h>
#include <qtextdocument.h>
#include <qtexttable.h>

#include "blurqt.h"

#include "config.h"

#include "jobburner.h"
#include "maindialog.h"
#include "settingsdialog.h"


const char * start = "Start The Burn";
const char * stop = "Stop The Burn";

MainDialog::MainDialog(Slave * s, QWidget * parent)
: QDialog( parent, Qt::WindowMinimizeButtonHint )
, mSlave( s )
, mTrayIcon( nullptr )
, mTrayMenu( nullptr )
, mTrayMenuToggleAction( nullptr )
, mBringToTop( true )
, mInfoUpdateTimer( nullptr )
{
	setupUi(this);
	
	mInfoUpdateTimer = new QTimer(this);
	connect( mInfoUpdateTimer, SIGNAL( timeout() ), SLOT( slotUpdateInfo() ) );

	readConfig();

	mCopyProgress->setRange( 0, 100 );
	mCopyProgress->setValue( 0 );
	
	connect( mSlave, SIGNAL( statusChange( const QString & ) ), SLOT( setStatus( const QString & ) ) );
	connect( mSlave, SIGNAL( copyProgressChange( int ) ), SLOT( setCopyProgress( int ) ) );

	mCurrentJobGroup->hide();

	QString trayIconPath = ":/images/"+cAppName+"Icon.png";
	QImage trayImage(trayIconPath);
	
	if (trayImage.isNull()) {
		LOG_1( "Unable to load system tray icon from resource path: " + trayIconPath );
	
	} else {
		mTrayMenu = new QMenu( this );
		mTrayMenuToggleAction = mTrayMenu->addAction( QString( start ), this, SLOT( slotDisablePressed() ) );
		mTrayMenu->addAction( "Exit "+cAppName, qApp, SLOT( quit() ) );

		QIcon trayIcon( QPixmap::fromImage( trayImage.scaled( 20, 20, Qt::KeepAspectRatio, Qt::SmoothTransformation ) ) );
		mTrayIcon = new QSystemTrayIcon( trayIcon, this );
		mTrayIcon->setContextMenu( mTrayMenu );
		connect( mTrayIcon, SIGNAL( activated( QSystemTrayIcon::ActivationReason ) ),
			SLOT( slotTrayIconActivated( QSystemTrayIcon::ActivationReason ) ) );
		mTrayIcon->show();
	}
	
	QString logoImage( ":/images/"+cAppName+"Logo.png" );
	mImageLabel->setPixmap(QPixmap(logoImage));

	connect( mAFButton, SIGNAL( clicked() ), SLOT( showAssfreezer() ) );
	connect( OptionsButton, SIGNAL( clicked() ), SLOT( showOptions() ) );
	connect( ClientLogButton, SIGNAL( clicked() ), SLOT( showClientLog() ) );
	connect( mShowSeedsButton, SIGNAL( toggled( bool ) ), SLOT( slotShowSeeds( bool ) ) );
	connect( DisableButton, SIGNAL( clicked() ), SLOT( slotDisablePressed() ) );
	
	setAttribute( Qt::WA_QuitOnClose, true );

	//layout()->setSizeConstraint(QLayout::SetFixedSize);
	slotShowSeeds( false );
	setStatus(mSlave->status());
	updateSize();
}

MainDialog::~MainDialog()
{
	delete mSlave;
	mSlave = 0;
}

void MainDialog::setDisplay( const QString & button, const QString & status, const QString & color )
{
	QString statusText = QString("<p align=\"center\">Status [ <font size=\"+1\" color=\"%1\">%2</font> ]</p>").arg( color ).arg( status );
	mStatusLabel->setText( statusText );
	setWindowTitle(QString(cAppName+" Client [ %1 ] - Version %2, build %3").arg(status).arg(VERSION).arg(SVN_REVSTR) );
	DisableButton->setText( button );
}

void MainDialog::updateTray()
{
	bool isOffline = mSlave->status() == "offline";
	mTrayMenuToggleAction->setText( QString( isOffline ? start : stop ) );
}

void MainDialog::keyPressEvent( QKeyEvent * ke )
{
	if (ke->key() == Qt::Key_Escape) {
		// go offline, minimize to the system try
		if( !checkLocked() ) {
			mSlave->offline();
			hide();
		}
	} else {
		QDialog::keyPressEvent( ke );
	}
}


typedef struct {
	const char * status, * button_text, * color;
	bool bring_to_front;
} display;

display disps [] =
{
	{ "Offline", start, "#000000", false },
	{ "Ready", stop, "#008800", true },
	{ "Copy", stop, "#DD8822", true },
	{ "Assigned", stop, "#DD8822", true },
	{ "Busy", stop, "#DD8822", true },
	{ "Error", start, "#000000", true },
	{ 0, 0, 0, false }
};

void MainDialog::setStatus( const QString & status )
{
	updateTray();
	LOG_5( "Status: " + status );

	QString st = status;
	st[0] = st[0].toUpper();

	// Find the status display entry
	display * d = disps;
	for( ; d && d->status; d++ )
		if( st == QString( d->status ) )
			break;

	// If not found, move back to "Error"
	if( !d->status ) d--;

	if( d && d->status ) {
		setDisplay( d->button_text, d->status, d->color );
		if( mBringToTop || ( d->bring_to_front && isHidden() ) )
			showAndActivate();
	}

	if( status == "ready" || status == "offline" )
		setCopyProgress(0);

	mBringToTop = false;
}


void MainDialog::showAndActivate()
{
	show();
	raise();
	activateWindow();
}

void MainDialog::showOptions()
{
	LOG_5("Showing settings dialog");
	SettingsDialog * settings = new SettingsDialog(this);
	// Exec for a modal dialog
	int result = settings->exec();
	
	if( result == QDialog::Accepted ){
		LOG_5("Applying new settings");
		settings->applyChanges();	// Apply the changes to 'config'
		readConfig();				// Read the values from 'config'
	}

	delete settings;
}

void MainDialog::showClientLog()
{
	QProcess::startDetached(cLogCommand.arg(cClientLogFile));
}

void MainDialog::slotDisablePressed()
{
	bool stopping = mSlave->status() != "offline";
	if( stopping && checkLocked() ) return;
	mBringToTop = true;
	mSlave->toggle();
	if( stopping )
		hide();
}

void MainDialog::setCopyProgress( int cp )
{
	if( mSlave->activeAssignments().isEmpty() ) {
		mCurrentJobGroup->setEnabled( false );
		mCopyProgress->setValue( 0 );
	} else {
		mCurrentJobGroup->setEnabled( true );
		mCopyProgress->setValue( cp == -1 ? 100 : cp );
	}
}

static void updateTable( QTextTable * table, QList<QPair<QString,QString> > data )
{
	table->resize(data.size(), 2);
	
	QTextCursor tc(table);
	typedef QPair<QString,QString> TextPair;
	foreach( TextPair dp, data ) {
		QTextTableCell cell = table->cellAt( tc );
		if( dp.second == "-" && cell.columnSpan() != 2 )
			table->mergeCells( cell.row(), cell.column(), 1, 2 );
		if( dp.second != "-" && cell.columnSpan() == 2 )
			table->splitCell( cell.row(), cell.column(), 1, 2 );
		tc.movePosition( QTextCursor::EndOfBlock, QTextCursor::KeepAnchor );
		tc.removeSelectedText();
		tc.insertText( dp.first );
		if( dp.second != "-" ) {
			tc.movePosition( QTextCursor::NextCell );
			tc.movePosition( QTextCursor::EndOfBlock, QTextCursor::KeepAnchor );
			tc.removeSelectedText();
			tc.insertText( dp.second );
		}
		tc.movePosition( QTextCursor::NextRow );
	}
}

void MainDialog::slotUpdateInfo()
{
	QList<JobBurner*> active = mSlave->activeBurners();
	if( active.size() > 0 ) {
		QMap<JobBurner*,QTextTable*> newMap;
		foreach( JobBurner * burner, active ) {
			QTextTable * table = 0;
			if( mInfoTableMap.contains(burner) )
				table = mInfoTableMap[burner];
			else {
				QTextCursor tc(mJobInfoEdit->document());
				tc.movePosition(QTextCursor::End);
				QTextTableFormat tableFormat;
				tableFormat.setBorderStyle( QTextFrameFormat::BorderStyle_Solid );
				tableFormat.setBorder(1);
				tableFormat.setCellSpacing(-1);
				tableFormat.setCellPadding(3);
				table = tc.insertTable(1,1,tableFormat);
			}
			
			QList<QPair<QString,QString> > data;
			data += qMakePair<QString,QString>( "Burner 0x" + QString::number( (qint64)burner, 16 ).toUpper(), "-" );
			burner->getCurrentInfo(data);
			updateTable( table, data );
			newMap[burner] = table;
		}
		mInfoTableMap = newMap;
	} else {
		mJobInfoEdit->clear();
		mInfoTableMap.clear();
	}
}

void MainDialog::readConfig()
{
	IniConfig & c( config() );
	c.pushSection( "Assburner" );
	cClientLogFile = c.readString("ClientLogFile", CLIENT_LOG );
	LOG_3( "setting cClientLogFile to " + cClientLogFile );
	cLogCommand = c.readString("LogCommand", LOG_COMMAND );
	LOG_3( "setting cLogCommand to " + cLogCommand );
	cAFPath = c.readString("AFPath", AF_COMMAND);
	LOG_3( "setting cAFPath to " + cAFPath );
	cAppName = c.readString("ApplicationName", "Assburner");
	cWindowStaysOnTop = c.readBool("WindowStaysOnTop", true);
	setWindowStaysOnTop( cWindowStaysOnTop );
	c.popSection();
}

void MainDialog::showAssfreezer()
{
	if( !QFile::exists( cAFPath ) ) {
		QMessageBox::warning( this, "Assfreezer Missing", "Assfreezer could not be found at: " + cAFPath );
		return;
	}
	QProcess * p = new QProcess(this);
	// Delete Automatically when assfreezer exits
	connect( p, SIGNAL( finished ( int, QProcess::ExitStatus ) ), p, SLOT(deleteLater()));
	p->setWorkingDirectory( QFileInfo(cAFPath).path() );
	p->start( cAFPath, QStringList() << "-current-render" );
}

void MainDialog::slotShowSeeds( bool ss )
{
	if( ss ) {
		//layout()->setSizeConstraint(QLayout::SetMinimumSize);
		mCurrentJobGroup->show();
		mShowSeedsButton->setText( "-" );
		mInfoUpdateTimer->start( 1000 );
		slotUpdateInfo();
	} else {
		mCurrentJobGroup->hide();
		mShowSeedsButton->setText( "+" );
		QTimer::singleShot( 0, this, SLOT( updateSize() ) );
		//layout()->setSizeConstraint(QLayout::SetFixedSize);
		layout()->update();
		mInfoUpdateTimer->stop();
	}
}

void MainDialog::setWindowStaysOnTop( bool newVal ){
	Qt::WindowFlags flags = windowFlags();

	if( newVal ){
		flags |= Qt::WindowStaysOnTopHint;
	} else {
		flags &= ~(Qt::WindowStaysOnTopHint);
	}

	setWindowFlags( flags );
	show();
}

void MainDialog::updateSize()
{
	layout()->update();
	resize( sizeHint() );
}

void MainDialog::closeEvent( QCloseEvent * ce )
{
	if( ce->spontaneous() ) {
		LOG_5( "Ignoring spontaneous closeevent." );
		ce->ignore();
		hide();
		return;
	}
}

void MainDialog::slotTrayIconActivated( QSystemTrayIcon::ActivationReason reason )
{
	if( reason == QSystemTrayIcon::Trigger ) {
		if( isVisible() && isActiveWindow() ) {
			LOG_3( "Going offline" );
			if( !checkLocked() ) {
				mSlave->offline();
				hide();
			}
		} else if( isVisible() ) {
			LOG_3( "Raising window" );
			raise();
			activateWindow();
		} else {
			LOG_3( "Going online" );
			// This will raise the window and set it active
			mBringToTop = true;
			mSlave->online();
		}
	}
}

bool MainDialog::checkLocked()
{
	Job::HostCancelModeEnum cancelMode = mSlave->hostCancelMode();
	if( cancelMode == Job::CancelAllowedWithWarning ) {
		return
			QMessageBox::warning( this, "Confirm stopping the burn", "Stopping the burn at this time will halt an important job, please wait if possible.\n  Are you sure you want to stop the burn?", QMessageBox::Yes | QMessageBox::No, QMessageBox::No )
			== QMessageBox::No;
	} else if ( cancelMode == Job::CancelNotAllowed ) {
		QMessageBox::critical( this, "Cannot stop the burn until job finishes", "Assburner is running a critical job and cannot allow you to stop the burn at this time." );
		return true;
	}
	return false;
}

