
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of RenderLine.
 *
 * RenderLine is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * RenderLine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RenderLine; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifdef COMPILE_MAX_BURNER

#include <qbuffer.h>
#include <qdir.h>
#include <qprocess.h>
#include <qregexp.h>
#include <qtimer.h>
#include <qfile.h>
#include <qfileinfo.h>

#include "iniconfig.h"
#include "path.h"
#include "process.h"

#include "host.h"
#include "joberror.h"
#include "jobmax.h"
#include "jobtype.h"
#include "jobservice.h"
#include "location.h"
#include "service.h"
#include "software.h"

#include "config.h"
#include "slave.h"
#include "maxburner.h"
#include "maxscriptburner.h"

#ifdef Q_OS_WIN
#include "windows.h"
#endif // Q_OS_WIN

int MaxBurnerBase::sInstanceCount = 0;

MaxBurnerBase::MaxBurnerBase( const JobAssignment & jobAssignment, Slave * slave, int options )
: JobBurner( jobAssignment, slave, options )
{
	sInstanceCount++;
}

MaxBurnerBase::~MaxBurnerBase()
{
	sInstanceCount--;
}

int MaxBurnerBase::maxVersion()
{
	return software().version();
}

QString MaxBurnerBase::maxDir()
{
	return software().installedPath();
}

bool MaxBurnerBase::fixPermissions()
{
#ifdef Q_OS_WIN
	QString cmd = "Max_fix_perms.bat";
	int ec = 0;
	if( Host::currentHost().location().name().startsWith("gce") )
	{
/*		QProcess process;
		process.start(cmd, QStringList() << maxDir());
		process.waitForFinished();
		ec = process.exitCode();
*/
	} else {
		Win32Process process;
		process.setLogon( "Administrator", "blur.core5", "" );
		if( !process.start( cmd, QStringList() << maxDir() ) ) {
			jobErrored("Unable to start permission fix as admin: ");
			return false;
		}
		process.waitForFinished();
		ec = process.exitCode();
	}
	if( ec != 0 && ec != 1 ) { // Seems to return 1 for some reason, but works fine.
		logError(job(),"Permission fix failed with return code: " + QString::number(ec));
		return false;
	}
	logMessage("Permission fix succeeded");
#endif
	return true;
}

bool MaxBurnerBase::cleanUserMacroTempFiles()
{
	bool success = true;
	if( sInstanceCount == 1 ) {
		QDir dir(maxDir() + QDir::separator() + "usermacros");
		foreach( QString fileName, dir.entryList( QStringList() << "__temp*.mcr", QDir::Files ) ) {
			if( !QFile::remove( dir.filePath(fileName) ) ) {
				jobErrored( "Unable to remove " + dir.filePath(fileName) + " prior to starting max." );
				success = false;
			}
		}
	}
	return success;
}

MaxBurner::MaxBurner( const JobAssignment & jobAssignment, Slave * slave )
: MaxBurnerBase( jobAssignment, slave )
, mFrame( 0 )
, mOriginalPluginIniIsDefault( false )
{
	mErrorREs << QRegExp("Obselete File Format") << QRegExp( "Error" )
	 << QRegExp( "\\*\\*\\*WARNING\\*\\*\\* The Object XRef file" );
	mIgnoreREs += QRegExp( "MENTAL\\s+RAY\\s+LOG:.+progr:" );
	mIgnoreREs += QRegExp( "MENTAL\\s+RAY\\s+LOG:.+warn" );
	QString defaultWindowTitles = (QStringList()
	<< "Microsoft Visual C++ Runtime Library"
	<< "DLL"
	<< "DbxHost Message"
	<< "Fatal Error"
	<< "3ds Max Error Report"
	<< "glu3D"
	<< "PNG Plugin"
	<< "Brazil r/s Error"
	<< "command line renderer"
	<< "Preferences Error"
	<< "License Alert"
	<< "Licensing Error"
	<< "LMU.exe"
	<< "COM Error"
	).join("\n");
	mWindowTitleMatches = Config::getString( "assburnerMaxWindowTitleDetectionList", defaultWindowTitles ).split("\n");
	LOG_5( "Window Title Detection List: \n" + mWindowTitleMatches.join("\n") );
}

/*
 *  plugin.ini logic
 *
 *  Startup Logic
 *
 *  If JobMax.pluginIniPath is empty
 *    1. Verify that the existing plugin.ini is acceptable
 *      1a. If default_plugin.ini exists, ensure that plugin.ini is an empty ini that includes default_plugin.ini, backup and restore if needed
 *      2a. If default_plugin.ini doesn't exist, use whatever is there (this allows proper functionality for installs that aren't setup to support dynamic plugin.ini)
 *  If JobMax.pluginIniPath is valid
 *    If existing plugin.ini is empty and points to default_plugin.ini, then set mOriginalPluginIniIsDefault=true
 *    else backup existing plugin.ini
 *    Write out new plugin.ini pointing to JobMax.pluginIniPath 
 *
 *  Cleanup Logic
 *
 *  If mOriginalPluginIniIsDefault
 *     write out empty ini that points to default_plugin.ini
 *  else if mRestorePluginIniPath is valid
 *     move it back to plugin.ini
 */

const char * defaultPluginIniContents = 
"[Include]\r\n"
"=%1\r\n";
const char * defaultPluginIni = "default_plugin.ini";

bool MaxBurner::verifyPluginIni()
{
	bool defaultIniExists = Path(Path(pluginIniPath()).dirPath() + defaultPluginIni).fileExists();
	bool needCustomIni = !JobMax(mJob).pluginIniPath().isEmpty();

	// This would be the default case for a setup that doesn't have empty(forwarding) plugin.ini
	if( !defaultIniExists && !needCustomIni )
		return true;

	QString contents = readFullFile(pluginIniPath());
	mOriginalPluginIniIsDefault = QRegExp(QString(defaultPluginIniContents).arg("[^\\n\\r]*")).exactMatch( contents );
	
	if( mOriginalPluginIniIsDefault ) {
		logMessage( "Original plugin.ini is default, no need to backup" );
		return true;
	}

	// Current plugin.ini not generated by us, so lets back it up
	mRestorePluginIniPath = pluginIniPath() + ".backup";
	if( !Path::copy( pluginIniPath(), mRestorePluginIniPath ) ) {
		jobErrored( "Unable to backup existing plugin.ini file" );
		return false;
	}

	logMessage( "plugin.ini backed up to " + mRestorePluginIniPath );

	// If we don't need a custom ini then we'll write out the default
	if( !needCustomIni && !restorePluginIni(true) ) {
		jobErrored( QString("Unable to modify plugin.ini to include ") + defaultPluginIni );
		return false;
	}

	return true;
}

bool MaxBurner::restorePluginIni( bool useDefault )
{
	QString contents;
	if( useDefault ) {
		contents = QString(defaultPluginIniContents).arg(defaultPluginIni);
		if( !writeFullFile( pluginIniPath(), contents ) ) {
			LOG_1( "Error restoring default plugin.ini contents" );
			return false;
		}
		logMessage( "Wrote out default ini" );
	} else if( !mRestorePluginIniPath.isEmpty() ) {
		if( !Path::copy( mRestorePluginIniPath, pluginIniPath() ) ) {
			LOG_1( "Error restoring plugin.ini, backup at " + mRestorePluginIniPath );
			return false;
		}
		QFile::remove(mRestorePluginIniPath);
		logMessage( "Restored plugin.ini from " + mRestorePluginIniPath );
	}
	return true;
}

QString MaxBurner::pluginIniPath()
{
	return maxDir() + "plugin.ini";
}

bool MaxBurner::modifyPluginIni()
{
	JobMax jm(mJob);
	if( !verifyPluginIni() )
		return false;
	if( jm.pluginIniPath().isEmpty() )
		return true;
	QString iniName = QFileInfo(jm.pluginIniPath()).fileName();
	QString destName("assburner_temp_plugin.ini");
	QString destFilePath(maxDir() + QDir::separator() + destName );
	if( !Path::copy( jm.pluginIniPath(), destFilePath ) ) {
		LOG_1( "Error copying " + jm.pluginIniPath() + " to " + destFilePath + " for custom plugin.ini" );
		return false;
	}
	if( !writeFullFile( pluginIniPath(), QString(defaultPluginIniContents).arg(destName) ) ) {
		jobErrored( "Unable to write custom plugin.ini to " + pluginIniPath() );
		return false;
	}
	logMessage( "Wrote custom plugin.ini with path pointing to " + JobMax(mJob).pluginIniPath().replace("/","\\") );
	return true;
}

template<typename TYPE> static QString nts( TYPE n ) { return n ? "1" : "0"; }

QStringList MaxBurner::buildCmdArgs()
{
	if( !modifyPluginIni() ) return QStringList();

	JobMax jm( mJob );
	QStringList args;
	args << "-frames:" + assignedTasks().replace( ' ', "" );
	// Should probably warn for this somehow, but it is possible to render fine without this
	// the default output path for the file will be used, and i guess the file could theoretically
	// be setup to not output frames at all
	if( !jm.outputPath().isEmpty() && jm.passOutputPath() )
		args << "-outputName:" + jm.outputPath().replace( "/", "\\\\" );
	args << "-height:" + QString::number(jm.flag_h());
	args << "-width:" + QString::number(jm.flag_w());
	args << "-v:5";
	args << "-rfw:1";
	args << "-videoColorCheck:" +	nts( jm.flag_xv() );
	args << "-force2sided:" 	+	nts( jm.flag_x2() );
	args << "-renderHidden:" 	+	nts( jm.flag_xh() );
	args << "-atmospherics:" 	+	nts( !jm.flag_xa() );
	args << "-superBlack:" 		+	nts( jm.flag_xk() );
	args << "-renderFields:" 	+	nts( jm.flag_xf() );
	args << "-fieldOrder:" 	+	QString( jm.flag_xo() ? "even" : "odd");
	args << "-displacements:" 	+	nts( !jm.flag_xd() );
	args << "-effects:" 		+	nts( !jm.flag_xe() );
	args << "-ditherPaletted:" +	nts( jm.flag_xp() );
	args << "-ditherTrueColor:"+	nts( jm.flag_xc() );
	args << "-gammaCorrection:"+	nts( jm.gammaCorrection() );
	if( !jm.getValue("verbosity").isNull() )
		args << "-v:" + QString::number(jm.verbosity());
	
	args << mBurnFile.replace( "/", "\\\\" );

	// If frames list is too long, it will cause cmd line corruption and 3dsmaxcmd will give a strange error,
	// usually something about the camera.  I'm not sure what the actual cutoff is.
	if( args.join(" ").size() > 512 ) {
		QString cmdFilePath = burnDir() + "cmd_file.txt";
		{
			QFile cmdFile(cmdFilePath);
			if( !cmdFile.open( QIODevice::WriteOnly ) ) {
				jobErrored( "Unable to write to cmd file at: " + cmdFilePath );
				return args;
			}
		
			QTextStream ts(&cmdFile);
			ts << args.join("\n");
		}
		logMessage( "Arguments written to file " + cmdFilePath + ":\n" + args.join("\n") );
		args = (QStringList() << "@" + cmdFilePath);
	}

	return args;
}

QStringList MaxBurner::processNames() const
{
    return QStringList() << "3dsmax.exe" << "3dsmaxcmd.exe";
}

QString MaxBurner::executable()
{
	return maxDir() + "\\3dsmaxcmd.exe";
}

QString MaxBurner::startupScriptDest()
{
	return maxDir() + "/scripts/startup/assburnerStartupScript.ms";
}

QString MaxBurner::workingDirectory()
{
//	if( mJob.jobType().name() == "Max" )
//		return maxDir();
	return QString();
}

void MaxBurner::startProcess()
{
	// Make sure there are no other 3dsmaxcmd.exe or 3dsmax.exe running
/*	if( !killAll( "3dsmaxcmd.exe", 10 ) || !killAll( "3dsmax.exe", 10 ) ) {
		jobErrored( "M7B: Couldn't kill existing 3dsmax processes" );
		return;
	} */

	// Make sure there are no '3ds Max Error Report' dialogs up
	if( !killAll( "SendDmp.exe", 10) ) {
		jobErrored( "M7B: Couldn't kill '3ds Max Error Report' dialog" );
		return;
	}

	generateStartupScript();
	toggleMentalRayLogging(/*restore=*/false);
	disableMacroRecorder();
	
	int ver = maxVersion();
	if( ver >= 2014 )
		fixPermissions();
	if( ver >= 2016 )
		setOption( OptionOutputEncodingUTF16 );
	
	cleanUserMacroTempFiles();

	MaxBurnerBase::startProcess();
}

void MaxBurner::slotProcessOutputLine( const QString & line, QProcess::ProcessChannel channel )
{
	QRegExp complete( "Job Complete" ), assigned( "Frame (\\d+) assigned" ), framecomplete( "Frame (\\d+) completed" );
	JobMax j( mJob );

	bool ass = assigned.indexIn(line) >= 0;
	bool com = complete.indexIn(line) >= 0;
	bool framecom = framecomplete.indexIn(line) >= 0;

	// Certain versions of max will not report finished frames,
	// they will just report that the next is started, so we
	// assume the current frame has finished when we get
	// frame finished, frame started, or job complete
	if( ass || com || framecom ) {
		if( taskStarted() ) {
			taskDone( mFrame );

			// Nth Frame Copy
			if( j.frameNth() > 0 && mFrame < (int)j.frameEnd() && j.frameNthMode() > 0 )
				copyFrameNth( mFrame, j.frameNth(), (int)j.frameEnd(), j.frameNthMode() == 1 );
		}
		if( ass ) {
			mFrame = assigned.cap( 1 ).toInt();
			taskStart( mFrame );
		}
		if( com )
			jobFinished();
		return;
	}

	JobBurner::slotProcessOutputLine( line, channel );
}

bool MaxBurner::checkup()
{
	if( !JobBurner::checkup() )
		return false;

	// Kill 3dsmax.exe if these windows popup
	QString windowFound;
	if( findMatchingWindow( qprocessId(mCmd), mWindowTitleMatches, /*matchProcessChildren=*/true, /*caseSensitive=*/false, &windowFound ) )
		jobErrored( "Found window: " + windowFound + ", cancelling the burn");
	return true;
}

void MaxBurner::cleanupTempFiles()
{
	QRegExp tempFileDelMatch( "(\\.tmp|\\.ac\\$|^cr(\\d+)|^st(\\d+)|^mzptmp(\\d+))$" );
	QFileInfoList fil = QDir( "C:/Documents And Settings/" ).entryInfoList( QDir::Dirs | QDir::NoDotAndDotDot );
	foreach( QFileInfo fi, fil ) {
		LOG_5( "Searching dir " + fi.path() + "/Temp/ for temp files to delete" );
		QDir tempDir( fi.path() + "/Temp/" );
		QFileInfoList tempFileList = tempDir.entryInfoList( QDir::Files );
		foreach( QFileInfo tempFile, tempFileList ) {
			if( tempFile.fileName().contains( tempFileDelMatch ) ) {
				LOG_5( "Deleting temp file: " + tempFile.filePath() );
				QFile::remove( tempFile.filePath() );
			}
		}
	}
}

int MaxBurner::cleanupStage(CleanupStage stage)
{
	if( stage == CleanupDone ) {
		restorePluginIni(mOriginalPluginIniIsDefault);
		toggleMentalRayLogging(/*restore=*/true);

		if( !mStartupScriptPath.isEmpty() )
			QFile::remove( mStartupScriptPath );

		cleanupTempFiles();
	}
	return JobBurner::cleanupStage(stage);
}

void MaxBurner::toggleMentalRayLogging( bool restore )
{
	IniConfig ini(maxDir() + "\\plugcfg\\mentalray_max.ini");
	ini.pushSection( "Preferences" );
	if( !restore ) {
		mMRVerbosityInfo = ini.readInt( "MsgVerbosityInfo", 0 );
		mMRVerbosityProgress = ini.readInt( "MsgVerbosityProgress", 0 );
		mMRVerbosityDebug = ini.readInt( "MsgVerbosityDebug", 0 );
	}
	ini.writeInt( "MsgVerbosityInfo", restore ? mMRVerbosityInfo : 0 );
	ini.writeInt( "MsgVerbosityProgress", restore ? mMRVerbosityProgress : 0 );
	ini.writeInt( "MsgVerbosityDebug", restore ? mMRVerbosityDebug : 0 );
	ini.writeToFile();
}

void MaxBurner::disableMacroRecorder()
{
	QString path = maxDir() + "/3dsmax.ini";
	QString iniStr = readFullFile( path );
	QRegExp re( "EnableMacroRecorder\\s*=\\s*(\\w+)" );
	if( iniStr.contains( re ) ) {
		if( re.cap(1) != "0" ) {
			iniStr.replace( re.pos(1), re.cap(1).size(), "0" );
			writeFullFile( path, iniStr );
		}
	}
}

bool MaxBurner::generateStartupScript()
{
	JobMax jm(mJob);
	QString path = maxDir() + "/scripts/startup/assburnerStartupScript.ms";
	QBuffer buffer;
	buffer.open( QIODevice::WriteOnly );

	{
		// Scrope for startupScript for proper flushing
		QTextStream startupScript( &buffer );
	
		if( mJob.fileName().right(4) == ".zip" ) {
			// Set sysinfo.currentdir for renderballs
			// so that the point cache files can be found
			startupScript << ("sysinfo.currentdir = \"" + path.replace("/","\\\\") + "\"\n");
			mBurnFile += "/maxhold.mx";
		}
		
		// If exrVersion == 1, we leave everything alone.
		if( jm.outputPath().endsWith( "exr", Qt::CaseInsensitive )  && (jm.exrVersion() != 1) ) {

			startupScript << "function restoreExrSettings =\n(\n\n";

			if( jm.exrVersion() == 0 ) {
				startupScript << "\tMaxOpenExr.SetSaveUseDefaults false\n\n";

				if( !jm.exrChannels().isEmpty() ) {
					startupScript << "\tMaxOpenEXR.RemoveAllChannels()\n";
					int channel = 1;
					foreach( QString exrChannel, jm.exrChannels().split(";") ) {
						QStringList parts = exrChannel.split(",");
						if( parts.size() == 4 ) {
							startupScript << QString( "\tMaxOpenEXR.AddChannel %1\n" ).arg(parts[0]);
							startupScript << QString( "\tMaxOpenEXR.SetChannelActive %1 %2\n" ).arg(channel).arg(parts[1]);
							startupScript << QString( "\tMaxOpenEXR.SetChannelFileTag %1 \"%2\"\n" ).arg(channel).arg(parts[2]);
							startupScript << QString( "\tMaxOpenEXR.SetChannelDataType %1 %2\n" ).arg(channel).arg(parts[3]);
							channel++;
						}
					}
					startupScript << "\n";
				}
			
				if( !jm.exrAttributes().isEmpty() ) {
					startupScript << "\tMaxOpenEXR.RemoveAllAttribs()\n";
					int channel = 1;
					foreach( QString exrAttrib, jm.exrAttributes().split(";") ) {
						QStringList parts = exrAttrib.split(",");
						if( parts.size() == 4 ) {
							startupScript << QString( "\tMaxOpenEXR.AddAttrib %1\n" ).arg(parts[0]);
							startupScript << QString( "\tMaxOpenEXR.SetAttribActive %1 %2\n" ).arg(channel).arg(parts[1]);
							startupScript << QString( "\tMaxOpenEXR.SetAttribFileTag %1 \"%2\"\n" ).arg(channel).arg(parts[2]);
							startupScript << QString( "\tMaxOpenEXR.SetAttribContent %1 \"%2\"\n" ).arg(channel).arg(parts[3]);
							channel++;
						}
					}
					startupScript << "\n";
				}

				startupScript << "\tMaxOpenExr.SetSaveBitDepth " << jm.exrSaveBitDepth() << "\n";
			
			} else if( jm.exrVersion() == 2 ) { //Explicit loading of the new interface settings
				startupScript << "\tfOpenExr.setCompression " << jm.exrCompression() << "\n";
				
				startupScript << "\tfOpenExr.setSaveScanline " << (jm.exrSaveScanline() ? "true" : "false") << "\n";
				
				startupScript << "\tfOpenExr.setTileSize " << jm.exrTileSize() << "\n";
				
				startupScript << "\tfOpenExr.setSaveRegion " << (jm.exrSaveRegion() ? "true" : "false") << "\n";
				
				QStringList gbufferLayerNames;
				gbufferLayerNames << "Z Depth" << "Material ID" << "Object ID" << "UV Coordinates" << "Normal" << "Non-Clamped Color"
					<< "Coverage" << "Background" << "Node Render ID" << "Color" << "Transparency" << "Velocity" << "Sub-Pixel Weight" << "Sub-Pixel Mask";
					
				QStringList exrLayers = jm.exrChannels().split(";");
				foreach( QString exrLayer, exrLayers ) {
					QStringList layerParts = exrLayer.split(",");
					if( layerParts.size() == 5 ) {
						int layerType = layerParts[0].toInt();
						QString layerName = layerParts[1], layerOutputName = layerParts[2], layerOutputType = layerParts[3], layerOutputFormat = layerParts[4];
						if( layerType == 1 ) { 			// Render element layer
							startupScript << "\tfOpenExr.addRenderElementLayer " << layerName << " " << layerOutputName << " " << layerOutputType << " " << layerOutputFormat << "\n";
						} else if( layerType == 2 ) {	// G-Buffer layer
							startupScript << "\tfOpenExr.addGBufferChannelLayer " << gbufferLayerNames.indexOf( layerName ) << " " << layerOutputName << " " << layerType << " " << layerOutputFormat << "\n";
						}
					}
				}
				
			}

			// This applies the exr settings changes
			startupScript << "\trof = rendOutputFilename\n";
			startupScript << "\tprint (\"Reloading Output Settings: \" + rof)\n";
			startupScript << "\trendOutputFilename = rof\n)\n\n";
			startupScript << "callbacks.addScript #filePostOpen \"restoreExrSettings()\"\n";
		}

		startupScript << jm.startupScript();
	}

	if( buffer.size() ) {
		logMessage( "Writing startup script to " + path );
		logMessage( "Contents:\n" + QString::fromUtf8(buffer.data()) + "\n\n" );
		QFile preRenderScriptFile( path );
		if( !preRenderScriptFile.open( QIODevice::WriteOnly ) ) {
			jobErrored( "Unable to write startup script to " + path );
			return false;
		}
		preRenderScriptFile.write( buffer.data() );
		preRenderScriptFile.close();
		mStartupScriptPath = path;
		return true;
	}

	return false;
}

#endif
