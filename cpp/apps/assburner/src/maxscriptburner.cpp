
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of RenderLine.
 *
 * RenderLine is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * RenderLine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RenderLine; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifdef COMPILE_MAXSCRIPT_BURNER

#include <qdir.h>
#include <qprocess.h>
#include <qtimer.h>
#include <qregexp.h>
#include <qfile.h>
#include <qfileinfo.h>

#include "path.h"
#include "process.h"

#include "jobmaxscript.h"
#include "jobservice.h"
#include "jobtype.h"
#include "software.h"

#include "config.h"
#include "maxscriptburner.h"
#include "slave.h"


MaxScriptBurner::MaxScriptBurner( const JobAssignment & jobAssignment, Slave * slave, int options )
: MaxBurnerBase( jobAssignment, slave, options )
, mStatusLine( 0 )
, mDebugMode(JobMaxScript(jobAssignment.job()).debugMode())
{
	// Kill 3dsmax.exe if these windows popup
	QStringList defaultWindowTitles;
	defaultWindowTitles += "Microsoft Visual C++ Runtime Library";
	defaultWindowTitles += "DLL";
	defaultWindowTitles += "DbxHost Message";
	defaultWindowTitles += "Fatal Error";
	defaultWindowTitles += "3ds Max Error Report";
	defaultWindowTitles += "glu3D";
	defaultWindowTitles += "License Alert";
	defaultWindowTitles += "Licensing Error";
	defaultWindowTitles += "Application Error";
	defaultWindowTitles += "Frost License";
	defaultWindowTitles += "Xmesh Saver License";
	defaultWindowTitles += "LMU.exe";
	defaultWindowTitles += "Loading of custom driver failed.Forcing null driver mode.";
	defaultWindowTitles += "COM Error";
	defaultWindowTitles += "Image I/O Error";
	mWindowTitleMatches = Config::getString( "assburnerMaxScriptWindowTitleDetectionList", defaultWindowTitles.join("\n") ).split("\n");
	LOG_5( "Window Title Detection List: \n" + mWindowTitleMatches.join("\n") );
	
}

MaxScriptBurner::~MaxScriptBurner()
{
}

QStringList MaxScriptBurner::processNames() const
{
    return QStringList() << "3dsmax.exe";
}

QString MaxScriptBurner::runScriptDest()
{
	return burnDir() + "runScriptJob.ms";
}

bool MaxScriptBurner::ensureStartupScript()
{
	QString startupScript(
	"sysEnv = dotNetClass \"System.Environment\"\n"
	"assburnerStartupScript = sysEnv.GetEnvironmentVariable \"ASSBURNER_STARTUP_SCRIPT\"\n"
	"if (assburnerStartupScript != undefined and assburnerStartupScript.count > 0) then\n"
	"	fileIn assburnerStartupScript\n");
	
	QString oldStartupScriptPath = maxDir() + "/scripts/startup/assburner_maxscript_job_hook.ms";
	if( QFile::exists(oldStartupScriptPath) )
		QFile::remove(oldStartupScriptPath);
	
	QString startupScriptPath = maxDir() + "/scripts/startup/zzz_assburner_maxscript_job_hook.ms";
	if( !QFile::exists(startupScriptPath) || readFullFile(startupScriptPath) != startupScript ) {
		if( !writeFullFile(startupScriptPath, startupScript) ) {
			jobErrored( "Unable to write the startup script at: " + startupScriptPath );
			return false;
		}
	}
	return true;
}

int MaxScriptBurner::cleanupStage(CleanupStage stage)
{
	if( stage == CleanupKillProcess ) {
		if( mJob.exclusiveAssignment() ) {
			LOG_5( "Killing all 3dsmax.exe and 3dsmaxcmd.exe processes" );
			killAll("3dsmax.exe", 10);
			killAll("3dsmaxcmd.exe", 10);
		}
	}
	return JobBurner::cleanupStage(stage);
}

QString MaxScriptBurner::runScriptSource()
{
	if( mDebugMode )
		return "runScriptJobDebug.ms";
	return "runScriptJob.ms";
}

static QString escapedWinPath(const QString & path)
{
	QString tmp = path;
	tmp.replace( "/", "\\" );
	int len = 0;
	do {
		len = tmp.length();
		tmp.replace( "\\\\", "\\" );
	} while( tmp.length() != len );
	return tmp.replace( "\\", "\\\\" );
}

void MaxScriptBurner::startBurn()
{
	JobMaxScript jms( mJob );

	mStatusFile = burnDir() + "scriptstatus.txt";

	if( maxVersion() >= 2014 )
		fixPermissions();
	
	cleanUserMacroTempFiles();

	// Check for error after calling burnDir, in case the services aren't setup properly
	if( state() == StateError ) return;
	
	if( !ensureStartupScript() )
		return;
	
	QString runScriptSrc = runScriptSource();

	if( !QFile::exists( runScriptSrc ) ) {
		jobErrored( "MSB: Missing file required for maxscript jobs: " + runScriptSrc );
		return;
	}
	
	QString runScriptDest = burnDir() + "runScriptJob.ms";

	QString frameList;
	{
		bool validNumberList = true;
		QList<int> frames = expandNumberList( assignedTasks(), &validNumberList );

		if( !validNumberList ) {
			jobErrored( "MSB: Invalid number list: " + assignedTasks() );
			return;
		}

		QStringList sl;
		foreach( int frame, frames )
			sl += QString::number( frame );

		frameList = sl.join(",");
	}

	{
		bool error = false;
		QString txt = readFullFile( runScriptSrc, &error );
		if( error ) {
			jobErrored( "MSB: Couldnt open " + runScriptSrc + " for reading" );
			return;
		}
		txt.replace( "%BURN_DIR%", escapedWinPath( burnDir() + "/" ) );
		txt.replace( "%STATUS_FILE%", escapedWinPath( mStatusFile ) );
		txt.replace( "%JOB_PARENT_ID%", QString::number( jms.key() ) );
		txt.replace( "%FRAME_LIST%", frameList );
		txt.replace( "%RUN_PYTHON_SCRIPT%", jms.runPythonScript() ? "true" : "false" );
		if( !writeFullFile( runScriptDest, txt ) ) {
			jobErrored( "MSB: Couldnt open " + runScriptDest + " for writing" );
			return;
		}
	}

	QString framesFile = burnDir() + "runscriptframes.txt";
	
	// Write frame list file for maxscript to read
	if( !writeFullFile( framesFile, frameList ) ) {
		jobErrored( "MSB: Unabled to write to frames file at: " + framesFile );
		return;
	}
	
	QFile::remove( mStatusFile );
	
	if( mJob.exclusiveAssignment() ) {
		// Make sure there are no other 3dsmaxcmd.exe or 3dsmax.exe running
		if( !killAll( "3dsmaxcmd.exe", 10 ) || !killAll( "3dsmax.exe", 10 ) ) {
			jobErrored( "MSB: Couldn't kill existing 3dsmax processes" );
			return;
		}

		// Make sure there are no '3ds Max Error Report' dialogs up
		if( !killAll( "SendDmp.exe", 10) ) {
			jobErrored( "M7B: Couldn't kill '3ds Max Error Report' dialog" );
			return;
		}
	}

	JobBurner::startBurn();
	
	mCheckupTimer->start( 500 );
}

QString MaxScriptBurner::executable()
{
	JobMaxScript jms( mJob );
	QString cmd = maxDir();
	if( jms.use3dsMaxCmd() )
		cmd += "\\3dsmaxcmd.exe";
	else
		cmd += "\\3dsmax.exe";
	
	if( !QFile::exists( cmd ) ) {
		jobErrored( "MSB: Couldn't find executable: " + cmd );
		return QString();
	}
	
	return cmd;
}

QStringList MaxScriptBurner::buildCmdArgs()
{
	JobMaxScript jms( mJob );
	QStringList args;
	if( jms.use3dsMaxCmd() )
		args << (burnDir() + QDir::separator() + "maxhold.mx");
	else if( jms.silent() )
		args << "-silent";
	return args;
}

QStringList MaxScriptBurner::customEnvironment()
{
	return MaxBurnerBase::customEnvironment() << ("ASSBURNER_STARTUP_SCRIPT=" + runScriptDest());
}

// Returns true if the job is done
bool MaxScriptBurner::checkup()
{
	// Ignore timeouts and dialogs in debug mode, let the user see them
	if( !mDebugMode ) {
		
		// Return if JobBurner::checkup finds a problem, ie. exceeded max load or max task time
		if( !JobBurner::checkup() )
			return false;

		QString windowFound;
		if( findMatchingWindow( qprocessId(mCmd), mWindowTitleMatches, /*matchProcessChildren=*/true, /*caseSensitive=*/false, &windowFound ) ) {
			killAll( "ad32lw.exe" );
			jobErrored( "Found window: " + windowFound + ", cancelling the burn");
			return false;
		}
		
	}

	// Nothing to do if maxscript hasn't yet written out a status file
	// We wait until we get a status file, an error dialog, or time out
	if( !QFile::exists( mStatusFile ) )
		return true;

	QFile status( mStatusFile );
	if( !status.open( QIODevice::ReadOnly ) ) {
		jobErrored( "Couldn't open status file for reading: " + mStatusFile );
		return false;
	}

	QString line;
	int lineNumber = 0;

	QTextStream inStream( &status );
	
	while( !inStream.atEnd() ) {
		line = inStream.readLine();

		// Keep track of where we're at
		// TODO: This is quite inefficient, we should seek to the last byte offset
		if( lineNumber++ < mStatusLine )
			continue;
		mStatusLine = lineNumber;
		
		logMessage( "MSB: Read new line: " + line );
		if( !processMaxScriptMessage(line) && !line.isEmpty() ) {
			QString error = line + "\n" + inStream.readAll().replace( "\\n", "\n" );
			// Don't record an error in debug mode or if processMaxScriptMessage already recorded one
			if( mDebugMode || state() == StateError )
				logMessage( error );
			else
				jobErrored( error );
			break;
		}
	}

	return true;
}

// Returns true for success, false for error
bool MaxScriptBurner::processMaxScriptMessage( const QString & line )
{
	QString errorString;
	QRegExp starting( "^starting (\\d+)" ), finished( "^finished (\\d+)" ), success( "^success" ), log("^log:");
	if( starting.indexIn(line) >= 0 ) {
		int frame = starting.cap(1).toInt();
		taskStart( frame );
	} else if( finished.indexIn(line) >= 0 ) {
		taskDone( finished.cap(1).toInt() );
	} else if( line.contains( success ) ) {
		// Never call job finished in debug mode, so that max stays up and can be examined
		if( !mDebugMode )
			jobFinished();
	} else if( line.contains( log ) ) {
		logMessage( line.mid(4) );
	} else {
		return false;
	}
	return true;
}

#endif
