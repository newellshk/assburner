
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of RenderLine.
 *
 * RenderLine is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * RenderLine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RenderLine; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qdir.h>
#include <qfileinfo.h>
#include <qregexp.h>
#include <qtimer.h>
#include <qthread.h>
#include <qprocess.h>

#include <math.h>

#ifdef Q_OS_LINUX
#include <sys/vfs.h>
#endif

#include "abdownloadstat.h"
#include "common.h"
#include "config.h"
#include "job.h"
#include "jobtype.h"
#include "path.h"
#include "maindialog.h"
#include "process.h"
#include "syslog.h"
#include "process.h"
#include "service.h"
#include "blurqt.h"
#include "md5.h"

#include "spooler.h"

SpoolItem::SpoolItem( Spooler * spooler, const Job & job )
: mState( New )
, mRefCount( 0 )
, mJob( job )
, mSizeUsed( 0 )
, mSpooler( spooler )
, mDownload( 0 )
, mCopied( false )
, mNextDirNumber(0)
{
	mSrcFile = job.getValue( "fileName" ).toString();
	if( !mSrcFile.isEmpty() ) {
		if( mJob.uploadedFile() ) {
			QFileInfo srcInfo( mSrcFile );
			QFileInfo destInfo( mSpooler->spoolDir() + "/" + srcInfo.fileName() );
			mDestFile = destInfo.filePath();
		} else
			mDestFile = mSrcFile;
	}
}

SpoolItem::~SpoolItem()
{
	delete mDownload;
}

bool SpoolItem::isZip()
{
	return QFileInfo( mDestFile ).suffix() == "zip";
}

void SpoolItem::_error( const QString & msg )
{
	int oldState = mState;
	_changeState( Errored );
	emit errored( msg, oldState );
}

void SpoolItem::_changeState( State state )
{
	int oldState = mState;
	mState = state;
	emit stateChanged( oldState, state );
}

void SpoolItem::_finished()
{
	_changeState( Done );
	emit completed();
}

void SpoolItem::startCopy()
{
	LOG_TRACE;

	// It's possible for a job to get unassigned, then reassigned while the copy is still in progress
	// because we don't stop/delete a copy just because the host is no longer assigned, only when the job
	// is no longer valid, or we need extra disk space
	if( mState == Copy ) return;
	
	// No file for this job
	if( mSrcFile.isEmpty() || !mJob.uploadedFile() ) {
		_finished();
		return;
	}

	bool oldFileIsBad = false;
	QFileInfo destInfo( mDestFile );

	// File already here, check if it's valid
	if( destInfo.exists() ) {
		// If it's not valid then delete it and start over
		if( destInfo.size() != mJob.fileSize() ) {
			LOG_3( "Existing file " + destInfo.filePath() + " doesn't match filesize in database, deleting." );
			oldFileIsBad = true;
		} else if( !checkMd5() ) {
			LOG_3( "Existing file " + destInfo.filePath() + " doesn't match md5sum in database, deleting." );
			oldFileIsBad = true;
		} else {
			LOG_3( "File already in place and matches md5sum" );
			_finished();
			return;
		}
	}

	if( oldFileIsBad && !QFile::remove( destInfo.filePath() ) ) {
		_error( "Couldn't delete existing invalid jobfile: " + destInfo.filePath() );
		return;
	}

	// We currently require 250 megs of free space on top of the size required for
	// the job file. TODO This should be configurable per jobtype(per job?), and should
	// also take into account the size need once the archive is unzipped when the
	// file is an archive.
	qint64 fsRequired = 1024*1024*500; // 500 megs
	// Since we always keep a pristine copy, we need space for twice the file size
	if( !destInfo.exists() )
		fsRequired += mJob.fileSize() * 2.0;

	// Ensure enough space
	if( !mSpooler->ensureDriveSpace( fsRequired ) ) {
		_error( "Insufficient drive space for job" );
		host_error( "Insufficient drive space for job", "startCopy", mJob.jobType().service() );
		return;
	}

	// Ensure an existing download is cleaned up, because overwriting the pointer with a new one
	cleanupDownload();
	
	mDownload = new MultiDownload( this );
	connect( mDownload, SIGNAL( progress( qint64, qint64 ) ), SLOT( downloadProgress( qint64, qint64 ) ) );
	connect( mDownload, SIGNAL( stateChanged( int ) ), SLOT( downloadStateChanged( int ) ) );

	mDownload->setup( mSrcFile, mDestFile );
	mDownload->start();
	_changeState( Copy );
}

void SpoolItem::cleanupDownload()
{
	if( mDownload ) {
		mDownload->disconnect( this );
		mDownload->cancel();
		mDownload->deleteLater();
		mDownload = 0;
	}
}

void SpoolItem::cancelCopy()
{
	LOG_TRACE
	cleanupDownload();

	_changeState( Cancelled );
	emit cancelled();

	mSpooler->removeSpoolItem( this );

	// Only remove the file if we copied it local
	if( mJob.uploadedFile() )
		QFile::remove( mDestFile );

	mSizeUsed = 0;
	
	//LOG_TRACE
}

bool SpoolItem::checkMd5()
{
	// if md5sum in DB, create hash of local jobfile, compare with DB
	QString dbMd5sum = mJob.fileMd5sum();
	LOG_3("mJob has fileMd5sum in DB of " + dbMd5sum );
	if ( !dbMd5sum.isEmpty() ) {
		Md5 md5;
		QString localMd5sum;
		localMd5sum = md5.calcMd5( mDestFile );
		LOG_3("Md5::calcMd5 of " + mDestFile + " gave md5sum of:" + localMd5sum );
		if (localMd5sum != dbMd5sum) { // hashes don't match, error out
			_error( QString("Jobfile corruption detected! Job %1, local file %2. Database MD5 hash is %3, local hash is %4")
			       .arg( mJob.key() )
				   .arg( mDestFile )
				   .arg( dbMd5sum )
				   .arg( localMd5sum ) );
			return false;
		}
	}
	return true;
}

void SpoolItem::downloadProgress( qint64 done, qint64 total )
{
	LOG_5( QString::number( done / 1024 ) + " of " + QString::number( total / 1024 ) + " kbytes downloaded" );
	emit copyProgressChange( (100*done) / total );
}

void SpoolItem::downloadStateChanged( int state )
{
	if( state == AbstractDownload::Error ) {
		_error( mDownload->errorMsg() );
		return;
	} else if( state == AbstractDownload::Finished ) {
		int mseconds = qMax( mDownload->elapsed(), 1 );
		mSizeUsed = mDownload->size();
	
		LOG_3( QString(mDownload->typeName()) + " finished " + mJob.name()
				+ " size " + QString::number( mSizeUsed )
				+ QString(" in %1:%2:%3").arg( mseconds / 3600000 ).arg( (mseconds / 60000) % 60 ).arg( (mseconds/1000) % 60 )
				+ " at " + add_suffix( mSizeUsed / float(mseconds/1000.0) ).c_str() + "/S" );
	
		mSpooler->addDownloadStat( mDownload->typeName(), mJob, mSizeUsed, mseconds/1000 );

		emit copyProgressChange( 100 );

		cleanupDownload();

		_finished();

		return;
	}
}

void SpoolItem::ref()
{
	mRefCount++;
}

void SpoolItem::deref()
{
	mRefCount--;
	if( mRefCount == 0 ) {
		cleanupDownload();
		deleteLater();
	}
}

SpoolRef::SpoolRef( SpoolItem * item )
: mState( New )
, mSpoolItem( item )
, mCheckupTimer( 0 )
, mUnzipProcess( 0 )
, mUnzipped( false )
, mUnzipFailureCount( 0 )
, mUnzipTimeout( 300000 )
{
	item->ref();
	mDirectory = item->spooler()->spoolDir() + QDir::separator() + QString::number(item->mJob.key()) + "_" + QString::number(item->mNextDirNumber++) + QDir::separator();
	connect( item, SIGNAL( copyProgressChange( int ) ), SIGNAL( copyProgressChange( int ) ) );
	connect( item, SIGNAL( errored( const QString &, int ) ), SLOT( slotCopyErrored( const QString &, int ) ) );
//	connect( item, SIGNAL( stateChanged( int, int ) ), SLOT( slotCopyStateChanged( int, int ) ) );
	connect( item, SIGNAL( completed() ), SLOT( slotCopyCompleted() ) );
	connect( item, SIGNAL( cancelled() ), SLOT( cancel() ) );
}


SpoolRef::~SpoolRef()
{
	cancel();
	cleanupDirectory();
}

void SpoolRef::_error( const QString & msg )
{
	int oldState = mState;
	_changeState( Errored );
	emit errored( msg, oldState );
}

void SpoolRef::_changeState( State state )
{
	int oldState = mState;
	mState = state;
	emit stateChanged( oldState, state );
}

void SpoolRef::_finished()
{
	emit completed();
	_changeState( Done );
}

void SpoolRef::start()
{
	if( mState != New ) return;

	Path dir(mDirectory);
	if( !dir.dirExists() ) {
		if( !dir.mkdir() ) {
			_error( "Failed to create directory for burner at: " + mDirectory );
			return;
		}
	}
	if( mSpoolItem->state() == SpoolItem::Done ) {
		slotCopyCompleted();
		return;
	}
	_changeState( Copy );
	mSpoolItem->startCopy();
	return;
}

void SpoolRef::cancel()
{
	if( mState != Cleaned ) {
		if( mSpoolItem ) {
			mSpoolItem->deref();
			mSpoolItem = 0;
		}
		if( mUnzipProcess ) {
			mUnzipProcess->disconnect( this );
			mUnzipProcess->kill();
			mUnzipProcess->deleteLater();
			mUnzipProcess = 0;
		}
		if( mCheckupTimer ) {
			mCheckupTimer->disconnect( this );
			mCheckupTimer->deleteLater();
			mCheckupTimer = 0;
		}
		mState = Cleaned;
	}
}

void SpoolRef::slotCopyCompleted()
{
	if( !mSpoolItem ) return;

	if( mSpoolItem->isZip() ) {
		checkUnzip();
		return;
	}
	// Ensure file is copied to the burners spool directory now
	burnFile();
	if( mState != Errored )
		_finished();
}

void SpoolRef::slotCopyErrored( const QString & msg, int  )
{
	_error(msg);
}

bool SpoolRef::isUnzipped() const
{
	return mUnzipped;
}

void SpoolRef::cleanupDirectory()
{
	QString err;
	if( !Path::remove( directory(), true, &err ) ) {
		//_error( "Failed to remove folder completely: " + directory() + " error was: " + err );
	}
	mUnzipped = false;
}

QString SpoolRef::directory() const
{
	return mDirectory;
}

QString SpoolRef::burnFile()
{
	if( mBurnFile.isEmpty() && mSpoolItem ) {
		if( !mSpoolItem->mJob.uploadedFile() )
			mBurnFile = mSpoolItem->destFile();
		else if( !mSpoolItem->isZip() && !mSpoolItem->destFile().isEmpty() ) {
			if( !mSpoolItem->mSpooler->ensureDriveSpace(mSpoolItem->mSizeUsed) ) {
				_error( "Insufficient drive space for job" );
				host_error( "Insufficient drive space for job", "startCopy", mSpoolItem->mJob.jobType().service() );
				return QString();
			}
			Path srcPath(mSpoolItem->destFile());
			mBurnFile = directory() + QDir::separator() + srcPath.fileName();
			if( !Path::copy( srcPath.path(), mBurnFile ) ) {
				_error( "Failed to copy job file from " + mSpoolItem->destFile() + " to " + directory() );
				mBurnFile = QString();
			}
		}
	}
	return mBurnFile;
}

void SpoolRef::checkUnzip()
{
	if( !mUnzipped && mSpoolItem && mSpoolItem->isZip() ) {
		if( !mSpoolItem->mSpooler->ensureDriveSpace( mSpoolItem->mJob.fileSize() * 1.5 ) ) {
			_error( "Insufficient drive space for job" );
			host_error( "Insufficient drive space for job", "startCopy", mSpoolItem->mJob.jobType().service() );
			return;
		}
		unzip();
	}
}

bool SpoolRef::unzip()
{
	if( mState == Unzip ) return false;

	if( !Path::checkFileFree( mSpoolItem->mDestFile ) ) {
		_error( "SpoolItem::checkUnzip: Couldn't open the file" );
		return false;
	}

	if( !mSpoolItem->checkMd5() )
		return false;

	_changeState( Unzip );
	QString dest( directory() );
	IniConfig cfg(config());
	cfg.pushSection( "Assburner" );
	QString unzip( cfg.readString( "ZipCmd", "c:\\blur\\assburner\\unzip.exe") );
	cfg.popSection();

	QString cmd = unzip + " " + mSpoolItem->mDestFile + " -d " + dest;
	LOG_3( "Running unzip cmd: " + cmd );
	
	QString error;
	if( !Path::remove( dest, true, &error ) ) {
		error = "SpoolItem::checkUnzip: couldn't remove existing render directory at: " + dest + error;
		_error( error );
		host_error( error, "checkUnzip", mSpoolItem->mJob.jobType().service() );
		return false;
	}

	mUnzipProcess = new QProcess();
	connect( mUnzipProcess, SIGNAL( readyReadStandardOutput() ), SLOT( unzipReadyRead() ) );
	connect( mUnzipProcess, SIGNAL( finished( int ) ), SLOT( unzipExited() ) );
	connect( mUnzipProcess, SIGNAL( error( QProcess::ProcessError ) ),SLOT( unzipExited() ) );
	
	mUnzipProcess->start(cmd);

	// Give the unzip 5 minutes to complete, then kill it
	mCheckupTimer = new QTimer( this );
	connect( mCheckupTimer, SIGNAL( timout() ), SLOT( unzipTimedOut() ) );
	mCheckupTimer->start( mUnzipTimeout );
	return true;
}

void SpoolRef::unzipExited()
{
	if( mUnzipProcess->exitCode() != 0 )
	{
		_error( "SpoolItem::unzipExited: Error unzipping archive" );
		return;
	}

	mUnzipProcess->disconnect( this );
	mUnzipProcess->deleteLater();
	mUnzipProcess = 0;

	mCheckupTimer->disconnect( this );
	mCheckupTimer->deleteLater();
	mCheckupTimer = 0;

	mUnzipped = true;
	_finished();
}

void SpoolRef::unzipReadyRead()
{
	if( !mUnzipProcess ) {
		LOG_1( "called while mUnzipProcess=0" );
		return;
	}
	mUnzipProcess->setReadChannel( QProcess::StandardOutput );
	while( mUnzipProcess->canReadLine() ) {
		LOG_5( "Unzip stdout: " + QString::fromLatin1(mUnzipProcess->readLine()) );
	}
}

void SpoolRef::unzipTimedOut()
{
	if( mUnzipProcess ) {
		LOG_1( "Unzip Timed Out" );
		mUnzipProcess->disconnect( this );
		mUnzipProcess->kill();
		mUnzipProcess->deleteLater();
		mUnzipProcess = 0;

		mCheckupTimer->disconnect( this );
		mCheckupTimer->deleteLater();
		mCheckupTimer = 0;

		mUnzipFailureCount++;

		if( mUnzipFailureCount > 1 ) { // We'll try twice
			_error( "The Unzip Timed Out After " + QString::number( mUnzipTimeout ) + " seconds" );
			return;
		}

		// Try again, double the timeout time
		mUnzipTimeout *= 2;
		checkUnzip();
	} else {
		LOG_1( "called when mUnzipProcess is 0" );
	}
}

Spooler::Spooler( QObject * parent, bool allowCleanup )
: QObject( parent )
, mRecordDownloadStats( false )
, mAllowCleanup( allowCleanup )
, mCheckTimer( 0 )
{
	IniConfig c(config());
	c.pushSection( "Assburner" );
#ifdef Q_OS_WIN
	mSpoolDir = c.readString( "SpoolDirectory", "c:/blur/assburner/spool" );
#else
	mSpoolDir = c.readString( "SpoolDirectory", "/tmp/" );
#endif
	c.popSection();
	mRecordDownloadStats = Config::getBool( "assburnerRecordDownloadStats", false );

	if( !QDir( mSpoolDir ).exists() ){
		LOG_5( "Creating Spool Dir" );
		QDir().mkdir( mSpoolDir );
	}

	// Cleanup all the leftover files right when we start
	fileCleanup();

	// Check the spool items every 30 seconds to make sure they 
	// are valid
	mCheckTimer = new QTimer( this );
	connect( mCheckTimer, SIGNAL( timeout() ), SLOT( cleanup() ) );

	// Every 5 minutes, check for cleanable jobs
	mCheckTimer->start( 5 * 60 *1000 );
}

Spooler::~Spooler()
{
	SpoolList spoolCopy(mSpool);
	for( SpoolIter it = spoolCopy.begin(); it != spoolCopy.end(); ++it )
		(*it)->cancelCopy();
}

SpoolItem * Spooler::startCopy( const Job & job )
{
	SpoolItem * si = itemFromJob( job );
	if( !si ) {
		si = new SpoolItem( this, job );
		si->ref();
		mSpool += si;
	}
	return si;
}

void Spooler::removeSpoolItem( SpoolItem * item )
{
	if( mSpool.contains( item ) ) {
		mSpool.removeAll(item);
		item->deref();
	} else {
		LOG_1( "ERROR: item not found in spool" );
	}
}

QString Spooler::spoolDir() const
{
	return mSpoolDir;
}

void Spooler::cleanup(quint64 cleanupIdleBytes)
{
	quint64 cleaned = 0;
	
	foreach( SpoolItem * si, mSpool )
	{
		Job j = si->mJob;
		j.reload();
		if( !j.isRecord() || j.status() == "done" || j.status() == "deleted" || j.status() == "suspended" ) {
			LOG_3( "Job has status " + (j.isRecord() ? j.status() : QString("deleted")) + ", Deleting Spool Item" );
			cleaned += si->mSizeUsed;
			si->cancelCopy();
		}
	}
	if( cleanupIdleBytes > 0 && cleaned < cleanupIdleBytes )
	{
		foreach( SpoolItem * si, mSpool )
		{
			// If only the spooler is holding a reference, then there are no active users
			if( si->mRefCount == 1 ) {
				cleaned += si->mSizeUsed;
				si->cancelCopy();
			}
			if( cleaned > cleanupIdleBytes )
				break;
		}
	}
}

bool Spooler::checkDriveSpace( quint64 required )
{
	return availableDriveSpace(mSpoolDir) > required;
}

bool Spooler::ensureDriveSpace( quint64 required )
{
	if( !checkDriveSpace( required ) ) {
		fileCleanup();
		cleanup();
		quint64 avail = availableDriveSpace(mSpoolDir);
		if( avail < required ) {
			cleanup( required - avail );
			return checkDriveSpace(required);
		}
	}
	return true;
}

int Spooler::fileCleanup()
{
	QDir dir( mSpoolDir );
	QFileInfoList fil = dir.entryInfoList( QDir::Files );
	int spaceSaved = 0;

	if( !mAllowCleanup ) return spaceSaved;

	for( int i=0; i<fil.size(); i++ ) {
		QFileInfo fi( fil.at(i) );
		QString ext = fi.suffix();
		
		// Currently we'll just clean these file types
		if( ext != "max" && ext != "zip" && ext != "xml" && ext != "mx" )
			continue;

		// Find the job id. ex job_is_name_bla_blaXXXXXX.max
		int id_start, id_end;
		QString name = fi.fileName();

		//  First find the last X
		for( id_end = name.length()-1; id_end >= 0 && !name[id_end].isNumber(); id_end-- );

		// No digits in the filename, well, delete the fucker anyway
		if( id_end >= 0 ) {
			// Then find the first X
			for( id_start = id_end; id_start > 0 && name[id_start-1].isNumber(); id_start-- );
			
			int id = name.mid( id_start, id_end-id_start ).toInt();
			
			if( id > 0 ) {
				Job j( id );
				if( j.isRecord() && j.status() != "done" && j.status() != "deleted" )
					continue;
			}
		}

		int space = fi.size();
		if( QFile::remove( fi.filePath() ) ) {
			LOG_5( "Removed File: " + name );
			spaceSaved += space;
		}
	}

	fil = dir.entryInfoList( QDir::Dirs );
	for( int i=0; i<fil.size(); i++ ) {
		QFileInfo fi( fil.at(i) );
		QRegExp burnDirRegEx("^\\d+_\\d+$" );
		if( burnDirRegEx.exactMatch(fi.fileName()) ) {
			Job j( burnDirRegEx.cap(1).toInt() );
			if( !j.isRecord() || !itemFromJob(j) ) {
				QFileInfo pidFileInfo(fi.filePath() + QDir::separator() + "pid.txt");
				if( pidFileInfo.exists() ) {
					LOG_3( "Found pid file, checking for valid pid: " + pidFileInfo.filePath() );
					int pid = pidFromFile(pidFileInfo.filePath());
					if( pid ) {
						QDateTime startTime = processStartTime(pid);
						LOG_3( QString("Found valid pid %1, started at time %2, pid file creation time %3").arg(pid).arg(startTime.toString()).arg(pidFileInfo.created().toString()) );
						if( startTime.isValid() && startTime < pidFileInfo.created() ) {
							LOG_1( "Killing Leftover process, job: " + j.name() + ", pid: " + QString::number(pid) );
							killProcess( pid );
						}
					}
				}
				LOG_5( "Deleting Spool Directory: " + fi.fileName() );
				QString error;
				long long size = Path::dirSize( fi.filePath() );
				if( !Path::remove( fi.filePath(), true, &error ) )
					LOG_3( "Failed to remove directory, reason was: " + error );
				else
					spaceSaved += size;
			}
		}
	}
	
	QDateTime cdt = QDateTime::currentDateTime();
	Interval ageLimit = Interval(0,14,0); // 14 days
	foreach( QFileInfo fi, QDir::current().entryInfoList( QDir::Files ) )
	{
		// Clean old assburner_burn_ logs, and old crash dumps
		// Check filename
		bool remove = 
			(fi.suffix() == "log" && fi.fileName().startsWith("assburner_burn_"))
			||
			fi.suffix() == "dmp";
		
		// Check age
		remove = remove && Interval(fi.created(),cdt) > ageLimit;
		
		if( remove && QFile::remove(fi.filePath()) )
			spaceSaved += fi.size();
	}
	
	LOG_5( "Freed " + QString::number( spaceSaved ) + " bytes" );
	return spaceSaved;
}

void Spooler::addDownloadStat( const QString & type, const Job & job, int size, int time )
{
	if( mRecordDownloadStats ) {
		AbDownloadStat stat;
		stat.setHost( Host::currentHost() );
		stat.setType( type );
		stat.setSize( size );
		stat.setTime( time );
		stat.setColumnLiteral( "finished", "NOW()" );
		stat.setAbrev( QString(SVN_REVSTR).toInt() );
		stat.setJob( job );
		stat.commit();
	}
}

SpoolItem * Spooler::itemFromJob( const Job & job )
{
	for( SpoolIter it = mSpool.begin(); it != mSpool.end(); ++it )
	{
		if( (*it)->mJob == job )
			return *it;
	}
	return 0;
}

std::string to_string(float v, int width, int precision = 3)
{
	std::stringstream s;
	s.precision(precision);
	s.flags(std::ios_base::right);
	s.width(width);
	s.fill(' ');
	s << v;
	return s.str();
}

std::string add_suffix(float val)
{
	const char* prefix[] = {"B", "kB", "MB", "GB", "TB"};
	const int num_prefix = sizeof(prefix) / sizeof(const char*);
	for (int i = 0; i < num_prefix; ++i)
	{
		if (fabs(val) < 1000.f)
			return to_string(val, i==0?5:4) + prefix[i];
		val /= 1000.f;
	}
	return to_string(val, 6) + "PB";
};


