
TARGET = assfreezer

include($$(PRI_SHARED)/common.pri)
include($$(PRI_SHARED)/python.pri)
include($$(PRI_SHARED)/assfreezer.pri)
include($$(PRI_SHARED)/absubmit.pri)
include($$(PRI_SHARED)/classesui.pri)
include($$(PRI_SHARED)/classes.pri)
include($$(PRI_SHARED)/stonegui.pri)
include($$(PRI_SHARED)/stone.pri)

INCLUDEPATH += .out include

win32:QMAKE_LFLAGS+=/FORCE:MULTIPLE

SOURCES+= \
	src/main.cpp

win32{
	LIBS += -lpsapi -lMpr -lws2_32 -lgdi32 -lopengl32 -lWer
	INCLUDEPATH+=c:\nvidia\cg\include
}

# Python modules
debug:win32 {
	LIBS+=-L../../lib/assfreezer/sipAssfreezer -lpyAssfreezer_d
	LIBS+=-L../../lib/classes/sipClasses -lpyClassesui_d
	LIBS+=-L../../lib/stone/sipStone -lpyStoneui_d
	LIBS+=-L../../lib/classes/sipClasses -lpyClasses_d
	LIBS+=-L../../lib/stone/sipStone -lpyStone_d
	LIBS+=-L../../lib/sip/siplib -lpysip_d
} else {
	
	win32 {
		LIBS+=-L../../lib/assfreezer/sipAssfreezer -lpyAssfreezer
		LIBS+=-L../../lib/classesui/sipClassesui -lpy_Classesui
		LIBS+=-L../../lib/stonegui/sipStonegui -lpy_Stonegui
		LIBS+=-L../../lib/classes/sipClasses -lpy_Classes
		LIBS+=-L../../lib/stone/sipStone -lpy_Stone
		LIBS+=-L../../lib/sip/siplib/ -lpysip
	}
}

RESOURCES+=assfreezer.qrc

CONFIG += qt thread warn_on opengl
QT+=opengl xml sql network widgets
RC_FILE = assfreezer.rc

target.path=$$BIN_PREFIX
target.files=assfreezer

INSTALLS += target

unix {
	config.path = /etc/
	config.files = assfreezer.ini
	INSTALLS += config
}
