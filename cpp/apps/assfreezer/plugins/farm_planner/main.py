
import bisect

import sip
sip.setapi('QDate',2)

from PyQt4.QtCore import *
from PyQt4.QtGui import QColor, QBrush, QPixmap, QPainter
from blur.Classes import *
from blur.defaultdict import *
from blur.Stone import *
from blur.Stonegui import calculateRenderUsageWorker, calculateProjectRenderUsageWorker
import series

# Arbitrary function that normalizes render power of a machine
# to be linearly scalable to the appoximate average render power
# that was available for past projects.  Need to start keeping
# render power data along with stats...
def normalizedRenderPower( mhz, cores ):
	return cores * mhz / float(72000) + .3

def profile(sort='cumulative', lines=300, strip_dirs=False, fileName=r'/tmp/profile.txt', acceptArgs=True):
	import cProfile
	import pstats
	"""
	A decorator which profiles a callable using cProfile
		
	:param sort: sort by this column. defaults to cumulative.
	:param lines: limit output to this number of lines. Defaults to 50.
	:param strip_dirs: If False the full path to files will be shown. Defaults to False.
	:param fileName: The name of the temporary file used to store the output for sorting.
	:param acceptArgs: Does the decorated function take args or kwargs? Mainly
					used to fix issues with PyQt signals.
	
	:type sort: str
	:type lines: int
	:type strip_dirs: bool
	:type fileName: str
	:type acceptArgs: bool
	"""
	def outer(fun):
		def inner(*args, **kwargs):
			prof = cProfile.Profile()
			if acceptArgs:
				ret = prof.runcall(fun, *args, **kwargs)
			else:
				ret = prof.runcall(fun, args[0])

			prof.dump_stats(fileName)
			stats = pstats.Stats(fileName)
			if strip_dirs:
				stats.strip_dirs()
			if isinstance(sort, (tuple, list)):
				stats.sort_stats(*sort)
			else:
				stats.sort_stats(sort)
			stats.print_stats(lines)
			return ret
		return inner

	# in case this is defined as "@profile" instead of "@profile()"
	if hasattr(sort, '__call__'):
		fun = sort
		sort = 'cumulative'
		outer = outer(fun)
	return outer

class FarmResource(object):
	# Renderpower is the cpu power * the number of cores
	# If renderPower is none it is computed automatically
	def __init__(self, hostStatus):
		self.HostSchedule = hostStatus
		self.Enabled = True
	
	@property
	def Modified(self):
		return self.HostSchedule.isUpdated()
	
	@property
	def RenderPower(self):
		hs = self.HostSchedule
		return normalizedRenderPower( hs.mhz(), hs.cores() )
	
	@property
	def Name(self):
		return self.HostSchedule.name()
	
	@Name.setter
	def Name(self,val):
		self.HostSchedule.setName(val)
	
	@property
	def CpusPerHost(self):
		return self.HostSchedule.cores()
	
	@CpusPerHost.setter
	def CpusPerHost(self,val):
		self.HostSchedule.setCores(val)
	
	@property
	def Mhz(self):
		return self.HostSchedule.mhz()
	
	@Mhz.setter
	def Mhz(self,val):
		self.HostSchedule.setMhz(val)
		
	@property
	def OnlineDate(self):
		return self.HostSchedule.onlineDate()
	
	@OnlineDate.setter
	def OnlineDate(self,val):
		self.HostSchedule.setOnlineDate(val)
		
	@property
	def OfflineDate(self):
		return self.HostSchedule.offlineDate()
	
	@OfflineDate.setter
	def OfflineDate(self,val):
		self.HostSchedule.setOfflineDate(val)
	
	@property
	def DailyAvgRenderTime(self):
		return self.HostSchedule.avgDailyRenderTime()
	
	@DailyAvgRenderTime.setter
	def DailyAvgRenderTime(self,val):
		self.HostSchedule.setAvgDailyRenderTime(val)

	@property
	def Count(self):
		return self.HostSchedule.count()
	
	@Count.setter
	def Count(self,val):
		self.HostSchedule.setCount(val)
	
	def setEnabled(self,enabled):
		self.Enabled = enabled

	# If adjusted is true, the rendertime is multiplied by the render power
	def renderTime(self,date = None, adjusted=True):
		if not self.Enabled:
			return Interval()
		if date is not None and ((date < self.OnlineDate) or (self.OfflineDate.isValid() and date > self.OfflineDate)):
			return Interval()
		if adjusted:
			return self.DailyAvgRenderTime * self.RenderPower * self.Count
		return self.DailyAvgRenderTime * self.Count
	
	# Returns a DaySeries filled with floats representing hours of render time
	def renderTimeSeries(self, renderPowerAdjusted=True):
		if self.Enabled:
			daily = self.DailyAvgRenderTime * self.Count
			if renderPowerAdjusted:
				daily *= self.RenderPower
			offlineDate = self.OfflineDate
			if offlineDate.isNull():
				offlineDate = QDate.currentDate().addYears(5)
			print self.OnlineDate, offlineDate
			ret = series.DaySeries(self.OnlineDate,offlineDate,constant=daily.asOrder(Interval.Seconds) / 3600.0)
			return ret
		return series.DaySeries.null()
	
	@staticmethod
	def fromHosts(name,hosts):
		avgTimeByHost = DefaultDict(Interval)
		q = Database.current().exec_( "SELECT fkeyhost, SUM(readytime+assignedtime+copytime+loadtime+busytime) / 14.0 from hostdailystat where fkeyhost IN (%s) and (now()::date - date) < 14 group by fkeyhost" % hosts.keyString() )
		while q.next():
			avgTimeByHost[Host(q.value(0).toInt()[0])] = Interval.fromString(q.value(1).toString())[0]
		renderTime = Interval()
		cpuCount = 0
		mhz = 0
		for host in hosts:
			rt = avgTimeByHost[host]
			renderTime += rt
			cpuCount += host.cpus()
			mhz += host.mhz()

		count = len(hosts)
		hs = HostSchedule()
		hs.setName( name )
		hs.setCount( count )
		hs.setCores( cpus / count )
		hs.setMhz( mhz / count )
		hs.setAvgDailyRenderTime( renderTime / count )
		hs.setOnlineDate( onlineDate )
		hs.setOfflineDate( offlineDate )

		#print "Adding Farm Resources", name, renderPower
		return FarmResource(hs)
		
class ResourcePool(QObject):
	ResourceAdded = pyqtSignal(object)
	ResourceRemoved = pyqtSignal(object)
	ResourceChanged = pyqtSignal(object)
	
	def __init__(self,parent = None):
		QObject.__init__(self,parent)
		self.Resources = []
		self.loadCurrentHosts()
	
	def addResource(self, resource):
		self.Resources.append(resource)
		self.ResourceAdded.emit(resource)
		
	def removeResource(self,resource):
		self.Resource.remove(resource)
		self.ResourceRemoved.emit(resource)
	
	#@profile
	def resourceChanged(self,resource):
		self.ResourceChanged.emit(resource)
		
	def loadCurrentHosts(self):
		for hs in HostSchedule.select():
			self.Resources.append( FarmResource(hs) )
		#hostsByNamePrefix = DefaultDict(HostList)
		#hosts = Host.select("WHERE keyhost IN (SELECT fkeyhost from HostService WHERE fkeyservice IN (SELECT keyservice from service WHERE service~'^MAX' AND enabled=true) AND enabled=true) and online=1")
		#nameReg = QRegExp( "^(\D+)\d+" )
		#for host in hosts:
			#if nameReg.exactMatch(host.name()):
				#hostsByNamePrefix[nameReg.cap(1)] += host
			#else:
				#self.Resources.append( FarmResource.fromHosts(host.name(),HostList(host)) )
		#for hostNamePrefix, hosts in hostsByNamePrefix.iteritems():
			#self.Resources.append( FarmResource.fromHosts(hostNamePrefix,hosts) )
		#FarmResource('rentals', Interval.fromString('24 hours'), (8 * 24567) / 2000, QDate( 2011, 3, 28 ), QDate())

	def renderTime(self,date = None, adjusted = True):
		rt = Interval()
		for resource in self.Resources:
			rt += resource.renderTime(date, adjusted)
		return rt

	def renderTimeSeries(self,renderPowerAdjusted = True):
		return series.sum( [resource.renderTimeSeries(renderPowerAdjusted) for resource in self.Resources] )
	
class ProjectWeightContainer(object):
	def __init__(self,project,schedules=None):
		self.Project = project
		self.setupSchedules(schedules)
	
	def reset(self):
		self.setupSchedules(self.Schedules)
		
	def setupSchedules(self,schedules):
		if schedules is None:
			schedules = ProjectWeightScheduleList()
		self.Schedules = schedules
		self.ByDate = {s.dateTime():s for s in schedules}
		self.Dates = self.ByDate.keys()
		self.Dates.sort()
	
	def addSchedule(self,schedule):
		if schedule not in self.Schedules:
			self.setupSchedules(self.Schedules + schedule)
	
	def removeSchedule(self,schedule):
		self.setupSchedules(self.Schedules - schedule)
	
	def weightAndNextChange(self,date):
		if isinstance(date,QDate):
			date = QDateTime(date)
		i = bisect.bisect_right(self.Dates,date)
		if i:
			nextChange = (i < len(self.Dates)) and self.Dates[i] or None
			return (self.ByDate[self.Dates[i-1]].weight(),nextChange)
		return (0.0,None)
	
	def schedule(self,date):
		if isinstance(date,QDate):
			date = QDateTime(date)
		i = bisect.bisect_right(self.Dates,date)
		if i:
			return self.ByDate[self.Dates[i-1]]
		return ProjectWeightSchedule()

	def weight(self,date):
		if isinstance(date,QDate):
			date = QDateTime(date)
		i = bisect.bisect_right(self.Dates,date)
		if i:
			return self.ByDate[self.Dates[i-1]].weight()
		return 0.0
	
	def setWeight(self,weight,sched):
		if sched is not None:
			sched = sched.setWeight(weight)
			self.Schedules.update(sched)
			return sched
		self.Project = self.Project.setAssburnerWeight(weight)
		return self.Project
	
	def weightSeries(self):
		if not self.Dates:
			return series.DaySeries.null()
		l = []
		lastDate = self.Dates[0].date()
		lastValue = self.ByDate[self.Dates[0]].weight()
		for dateTime in self.Dates[1:]:
			date = dateTime.date()
			l.append( series.DaySeries(lastDate,date,constant=lastValue) )
			lastDate = date
			lastValue = self.ByDate[dateTime].weight()
		l.append( series.DaySeries(lastDate,lastDate.addYears(10),constant=lastValue) )
		return series.sum( l )

	def dump(self):
		print "Project:", self.Project.name()
		if self.Project.assburnerWeight():
			print "\tCurrent:", self.Project.assburnerWeight()
		for date in self.Dates:
			sched = self.ByDate[date]
			print "\t%s: %f" % (date.toString(), sched.weight())

class ProjectWeightManager(QObject):
	weightChange = pyqtSignal()
	projectAdded = pyqtSignal(Project)
	
	def __init__(self):
		QObject.__init__(self)
		self.ProjectContainers = DefaultDict(ProjectWeightContainer,passKey=True)
		self.RemovedSchedules = ProjectWeightScheduleList()
		self.clearCache()
	
	def clearCache(self):
		self.TotalsByDateCache = {}
		
	def commitChanges(self):
		self.RemovedSchedules.remove()
		toCommit = ProjectWeightScheduleList()
		projects = ProjectList()
		for project,container in self.ProjectContainers.iteritems():
			for sched in container.Schedules:
				if not sched.isRecord() and sched.dateTime().date() == QDate.currentDate():
					# Disregard the uncommitted schedule representing the change between the past and future
					# if there is no change indicated
					if sched.weight() == project.assburnerWeight():
						continue
					# Set the time to the time of commit, so we get an accurate history of when the weight changed.
					sched.setDateTime( QDateTime.currentDateTime() )
					project.setAssburnerWeight(sched.weight())
					projects.append(project)
				toCommit += sched
		toCommit.commit()
		projects.commit()
	
	def loadFromDatabase(self):
		schedules = Expression().orderBy(ProjectWeightSchedule.c.DateTime,Expression.Ascending).select()
		projects = ((Project.c.AssburnerWeight > 0) | Project.c.Key.in_(schedules)).select()
		schedulesByProject = DefaultDict(ProjectWeightScheduleList,initialDict=schedules.groupedByForeignKey('fkeyproject'))
		for project in projects:
			self.addProject(project,schedulesByProject[project])
	
	def addSchedule(self,sched):
		self.clearCache()
		proj = sched.project()
		self.addProject(proj)
		self.ProjectContainers[proj].addSchedule(sched)
		self.RemovedSchedules.remove(sched)

	def removeSchedule(self,sched):
		self.clearCache()
		self.ProjectContainers[sched.project()].removeSchedule(sched)
		self.RemovedSchedules.append(sched)
		
	def addProject(self,proj,schedules=None):
		if not proj in self.ProjectContainers:
			self.clearCache()
			if schedules is None:
				schedules = ProjectWeightSchedule.recordsByProject(proj)
			self.ProjectContainers[proj] = ProjectWeightContainer(proj,schedules)
			self.projectAdded.emit(proj)
	
	def schedule(self,project,date):
		return self.ProjectContainers[project].schedule(date)
	
	# Replace the unmodified schedule or project record with the now modified one
	def setWeight(self,project,weight,sched = None):
		self.clearCache()
		return self.ProjectContainers[project].setWeight(weight,sched)
		
	def weight(self,project,date):
		sched = self.schedule(project,date)
		if sched is not None:
			return sched.weight()
		if date < QDate.currentDate():
			return 0.0
		return project.assburnerWeight()
	
	def weightSeries(self,project):
		return self.ProjectContainers[project].weightSeries()
	
	def totalWeightSeries(self):
		return series.sum( [pc.weightSeries() for pc in self.ProjectContainers.itervalues()] )

	def totalWeight(self,date):
		try:
			return self.TotalsByDateCache[date]
		except: pass
		ret = 0.0
		nextChange = None
		for pc in self.ProjectContainers.itervalues():
			weight, projectNextChange = pc.weightAndNextChange(date)
			nextChange = (projectNextChange is None) and nextChange or projectNextChange
			ret += weight
		if nextChange is None:
			nextChange = date.addDays(50)
		while date < nextChange:
			self.TotalsByDateCache[date] = ret
			date = date.addDays(1)
		return ret
	
	def nextWeightChange(self,project,dt):
		closest = None
		for ws in self.schedules(project):
			if ws.dateTime() > dt and (closest is None or ws.dateTime() < closest.dateTime()):
				closest = ws
		if closest is not None:
			return closest.dateTime()
		return None
		
	def projects(self):
		return self.ProjectContainers.keys()
		
	def schedules(self,project):
		return self.ProjectContainers[project].Schedules
	
	def resetProject(self,project):
		self.ProjectContainers[project].reset()
		self.weightChange.emit()
		
	def dump(self,project=None):
		for project in (project and [project] or self.Projects):
			self.ProjectContainers[project].dump()

def nextRandomColor():
	ret = QColor(Qt.GlobalColor(nextRandomColor.color))
	nextRandomColor.color += 1
	return ret
nextRandomColor.color = Qt.red

def brushStyle(i):
	#return Qt.SolidPattern
	l = [Qt.BDiagPattern, Qt.FDiagPattern, Qt.DiagCrossPattern, Qt.Dense4Pattern, Qt.Dense5Pattern, Qt.Dense6Pattern]
	#l = [Qt.Dense1Pattern, Qt.Dense2Pattern, Qt.Dense3Pattern, Qt.Dense4Pattern, Qt.Dense5Pattern, Qt.Dense6Pattern]
	return l[i % len(l)]

def patternBrush(color,brushStyle,altColor=Qt.white):
	px = QPixmap(40,40)
	px.fill(color)
	p = QPainter(px)
	p.fillRect(0,0,40,40,QBrush(altColor,brushStyle))
	del p
	return QBrush(px)

class Workload(object):
	def __init__(self,projectResolution,brushStyle):
		self.Enabled = True
		self.Project = projectResolution.project()
		self.ProjectResolution = projectResolution
		self.BrushStyle = brushStyle
		self.Color = None
		self.Brush = None
		self.clearCache()
		if self.ProjectResolution.pixelRenderTimeEstimate() == Interval():
			self.ProjectResolution.setPixelRenderTimeEstimate( Interval.fromString('.035 seconds')[0] )

	def __repr__(self):
		return "<Workload %s %s>" % (self.Project.name(), self.ProjectResolution.name())
	
	def color(self):
		if self.Color is None:
			self.Color = workpool().projectColor(self.Project)
		return self.Color
	
	def brush(self):
		if self.Brush is None:
			self.Brush = patternBrush(self.color(),self.BrushStyle)
		return self.Brush
	
	def selectedBrush(self):
		return patternBrush(self.color(), self.BrushStyle, Qt.red)
	
	def clearCache(self):
		self.DailyRenderTimeSubmitted = None
		self.Start = self.ProjectResolution.renderCrunchStart()
		self.End = self.ProjectResolution.renderCrunchEnd()
		if self.Start.isNull() or self.End.isNull():
			print self.ProjectResolution, self.Start, self.End
		
	def lifespan(self):
		return (self.ProjectResolution.renderCrunchStart(),self.ProjectResolution.renderCruncEnd())

	def totalRenderTime(self):
		return pr.runLength().seconds() * pr.width() * pr.height() * pr.fps() * pr.pixelRenderTimeEstimate()
		
	def dailyRenderTimeSubmitted(self):
		if self.DailyRenderTimeSubmitted is None:
			pr = self.ProjectResolution
			#print "Found matching project resoultion for project", self.Project.name()
			pixels = (pr.runLength().seconds() * pr.width() * pr.height() * pr.fps()) / float(self.Start.daysTo(self.End)+1)
			self.DailyRenderTimeSubmitted = pr.pixelRenderTimeEstimate() * pixels
		return self.DailyRenderTimeSubmitted
	
	# Interval
	def renderTimeSubmitted(self,date):
		if self.Enabled and date >= self.Start and date <= self.End:
			return self.dailyRenderTimeSubmitted()
		return Interval()
		
	def renderTimeSubmittedSeries(self):
		if self.Enabled:
			ret = series.DaySeries(self.Start,self.End,constant=self.dailyRenderTimeSubmitted().asOrder(Interval.Seconds) / 3600.0)
			return ret
		return series.DaySeries.null()
	
	@staticmethod
	def fromProjectResolutions(projectResolutions):
		ret = []
		for i, pr in enumerate(projectResolutions):
			ret.append( Workload(pr,brushStyle(i)) )
		return ret
	
	@staticmethod
	def fromProjects(projects):
		PRC = ProjectResolution.c
		ret = []
		resByProject = (PRC.Project.in_(projects) & (PRC.PrimaryOutput==True) & PRC.RenderCrunchStart.isNotNull()).select().groupedByForeignKey('fkeyproject')
		for proj, resolutions in resByProject.iteritems():
			ret += Workload.fromProjectResolutions(resolutions)
		return ret

class RenderStatus(object):
	def __init__(self,startDate,endDate):
		self.RenderUsage = series.DaySeries(startDate,endDate)
		self.RenderUsage.fillZeros()
		self.RenderBacklog = series.DaySeries(startDate,endDate)
		self.RenderBacklog.fillZeros()

	def __str__(self):
		return "%s %s %s" % (object.__str__(self), str(self.RenderUsage), str(self.RenderBacklog))
	
class WorkPool(QObject):
	WorkloadAdded = pyqtSignal(object)
	WorkloadRemoved = pyqtSignal(object)
	WorkloadChanged = pyqtSignal(object)
	CalculationsChanged = pyqtSignal()
	
	def __init__(self):
		QObject.__init__(self)
		self.Workloads = []
		self.Projects = ProjectList()
		self.RenderStatus = None
		self.RenderStatusByProject = {}
		self.RenderStatusByWorkload = {}
		self.LastDateRange = None
		self.ProjectColors = {}
		self.PRLoaded = ProjectResolutionList()
		rs = resourcepool()
		rs.ResourceAdded.connect( self.recalculateAll )
		rs.ResourceRemoved.connect( self.recalculateAll )
		rs.ResourceChanged.connect( self.recalculateAll )
		wm = weightManager()
		wm.weightChange.connect( self.recalculateAll )
		
		prlTable = ProjectResolution.table()
		prlTable.added.connect(self.projectResolutionsAdded)
		prlTable.removed.connect(self.projectResolutionsRemoved)
	
	def projectResolutionFilter(self):
		PR = ProjectResolution.c
		startDate, endDate = self.LastDateRange
		projectStatuses = Project.c.ProjectStatus.in_(ProjectStatus.c.ProjectStatus.in_(['Bid','Pre-Production','Production']))
		return (PR.RenderCrunchStart.isNotNull() & (PR.RenderCrunchEnd > startDate) \
			& (PR.RenderCrunchStart < endDate) & (PR.RenderCrunchStart <= PR.RenderCrunchEnd) \
			& PR.Project.in_(projectStatuses))
	
	def projectResolutionsAdded(self,projectResolutions):
		self.addProjectResolutions( projectResolutions.filter(self.projectResolutionFilter()) )

	def projectResolutionsRemoved(self,projectResolutions):
		self.PRLoaded -= projectResolutions
		for wl in self.Workloads:
			if wl.ProjectResolution in projectResolutions:
				self.WorkloadRemoved.emit(wl)
				self.Workloads.remove(wl)
		self.recalculateAll()
	
	def recalculateAll(self):
#		import traceback
#		traceback.print_stack()
		self.recalculate( *self.LastDateRange )
		self.CalculationsChanged.emit()
	
	def addWorkload(self,workload):
		self.Workloads.append( workload )
		self.WorkloadAdded.emit( workload )

	def loadProjectColors(self,projects):
		elementColors = ElementColor.c.Element.in_(projects).select()
		for ec in elementColors:
			self.ProjectColors[ec.element()] = ec.color()
		for project in (projects - elementColors.elements()):
			self.ProjectColors[project] = Qt.yellow
		
	def projectColor(self,project):
		if not project in self.ProjectColors:
			colors = ElementColor.recordsByElement(project)
			self.ProjectColors[project] = colors and colors[0].color() or nextRandomColor()
		return self.ProjectColors[project]
	
	def addProjects(self,projects):
		for wl in Workload.fromProjects(projects):
			if not wl.Project in self.Projects:
				self.Projects.append(wl.Project)
			self.addWorkload(wl)
	
	def workloadChanged(self,workload):
		self.WorkloadChanged.emit(workload)
		self.recalculateAll()
	
	def projectWorkloads(self,project):
		if not project in self.Projects:
			self.addProject(project)
		ret = []
		for wl in self.Workloads:
			if wl.Project == project:
				ret.append(wl)
		return ret
	
	def renderTimeSubmittedSeries(self,project=None):
		workloads = project is None and self.Workloads or self.projectWorkloads(project)
		return series.sum( [wl.renderTimeSubmittedSeries() for wl in workloads] )

	def addProjectResolutions( self, projectResolutions ):
		projects = projectResolutions.projects().unique()
		for p in [p for p in projects if p not in self.Projects]:
			self.Projects.append(p)
		
		prToWl = {}
		
		for p, prl in projectResolutions.groupedByForeignKey(ProjectResolution.c.Project).iteritems():
			if not p in self.Projects:
				self.Project.append(p)
			for wl in Workload.fromProjectResolutions(prl):
				prToWl[wl.ProjectResolution.pristine()] = wl
		
		loadedCount = 0
		# Done in a way to keep order of the first rendercrunchstart of each project
		for pr in projectResolutions:
			if pr in self.PRLoaded: continue
			self.Workloads.append(prToWl[pr])
			self.PRLoaded.append(pr)
			loadedCount += 1
		
		if loadedCount:
			self.recalculate( *self.LastDateRange )
		
	def loadDateRange( self, startDate, endDate ):
		#if startDate < QDate.currentDate():
		#	startDate = QDate.currentDate()
		dateRange = (startDate,endDate)
		if dateRange != self.LastDateRange:
			self.LastDateRange = dateRange
			self.addProjectResolutions( self.projectResolutionFilter().orderBy(ProjectResolution.c.RenderCrunchStart,Expression.Ascending).select() )
	
	def recalculate( self, startDate, endDate ):
		self.calculateRenderUsage(endDate)
		self.calculateProjectRenderUsage(endDate)
	
	def currentRenderBacklog(self):
		return 0.0
	
	@staticmethod
	def calculateRenderUsageWorker(count, leftover, resourceSeriesData, submitSeriesData, renderUsageData, renderBacklogData):
		for i in xrange(count):
			rt = resourceSeriesData[i]
			ru = leftover + submitSeriesData[i]
			if ru > rt:
				leftover = ru - rt
				ru = rt
			else:
				leftover = 0.0
			renderUsageData[i] = ru
			renderBacklogData[i] = leftover
		
	def calculateRenderUsage(self, endDate):
		leftover = self.currentRenderBacklog()
		curDate = QDate.currentDate()
		self.RenderStatus = RenderStatus(curDate,endDate)
		print curDate, endDate
		resourceSeries = resourcepool().renderTimeSeries().fit(curDate,endDate)
		submitSeries = self.renderTimeSubmittedSeries().fit(curDate,endDate)
		count = curDate.daysTo(endDate)
		calculateRenderUsageWorker(count,leftover, resourceSeries.Data, submitSeries.Data, self.RenderStatus.RenderUsage.Data, self.RenderStatus.RenderBacklog.Data)
		
	def currentRenderBacklogByProject(self):
		return DefaultDict(0.0)
		
	@staticmethod
	def calculateProjectRenderUsageWorker( count, projects, resourceSeries ):

		tiny = 0.000001

		def findLeastRenderTime():
			least = None
			cnt = 0
			for pc in projects:
				rt = pc.leftover
				if rt < tiny:
					continue
				if least is None:
					least = rt
				else:
					least = min(least,rt)
				cnt += 1
			return (least, cnt)

		for i in xrange(count):
			# Get render time available for the date
			rt = rto = resourceSeries[i]

			# Add submission time to current backlogs for projects and workloads
			for pc in projects:
				projectSub = 0.0
				for wlc in pc.workloads:
					if i >= wlc.submitOffset:
						sub = wlc.submitSeries[i-wlc.submitOffset]
						projectSub += sub
						wlc.leftover += sub
				pc.leftover += projectSub
				
				# Apply weighted render time to the project and it's workloads
				if pc.weightSeries is not None and i >= pc.weightOffset:
					weight = pc.weightSeries[i - pc.weightOffset]
					weightedRenderTime = min(pc.leftover, rto * weight)
					if weightedRenderTime > tiny:
						rt -= weightedRenderTime
						pc.leftover -= weightedRenderTime
						pc.renderUsage[i] += weightedRenderTime
						for wl in pc.workloads:
							wlr = min(weightedRenderTime,wl.leftover)
							wl.leftover -= wlr
							wl.renderUsage[i] += wlr
							weightedRenderTime -= wlr

			# Split up existing rendertime evenly between projects
			while rt > tiny:
				(least, cnt) = findLeastRenderTime()
				if least is None or cnt == 0:
					break
				if least * cnt > rt:
					least = rt / float(cnt)
				if cnt == 0 or least < tiny:
					break
				for pc in projects:
					if pc.leftover > tiny:
						rtu = min(least,pc.leftover)
						pc.leftover -= rtu
						pc.renderUsage[i] += rtu
						rt -= rtu
						for wl in pc.workloads:
							wlru = min(rtu,wl.leftover)
							wl.leftover -= wlru
							wl.renderUsage[i] += wlru
							rtu -= wlru
			
			for pc in projects:
				if pc.leftover > tiny:
					pc.renderBacklog[i] = pc.leftover
				for wl in pc.workloads:
					if wl.leftover > tiny:
						wl.renderBacklog[i] = wl.leftover
	
	#@profile
	def calculateProjectRenderUsage(self,endDate):
		
		class ProjectContainer(object):
			pass
		class WorkloadContainer(object):
			pass
		
		leftoverByProject = self.currentRenderBacklogByProject()
		curDate = QDate.currentDate()
		
		projects = []
		
		# In order of RenderCrunchStart
		workloadsByProject = DefaultDict(list)
		self.RenderStatusByProject = {}
		self.RenderStatusByWorkload = {}
		
		for wl in self.Workloads:
			s = wl.renderTimeSubmittedSeries().trim(curDate,endDate)
			if s and not s.isEmpty():
				wlc = WorkloadContainer()
				wlc.leftover = 0.0
				rs = self.RenderStatusByWorkload[wl] = RenderStatus(curDate,endDate)
				wlc.renderBacklog = rs.RenderBacklog
				wlc.renderUsage = rs.RenderUsage
				wlc.submitSeries = s
				wlc.submitOffset = curDate.daysTo(s.Start)
				workloadsByProject[wl.Project].append(wlc)
		
		for pr in self.Projects:
			leftover = leftoverByProject[pr]
			workloads = workloadsByProject[pr]
			if workloads or leftover:
				pc = ProjectContainer()
				pc.leftover = leftover
				rs = self.RenderStatusByProject[pr] = RenderStatus(curDate,endDate)
				pc.renderUsage = rs.RenderUsage
				pc.renderBacklog = rs.RenderBacklog
				pc.workloads = workloads
				ws = weightManager().weightSeries(pr).trim(curDate,endDate)
				if not ws.isEmpty():
					pc.weightSeries = ws
					pc.weightOffset = curDate.daysTo(ws.Start)
				else:
					pc.weightSeries = None
					pc.weightOffset = 0
				projects.append(pc)
		
		resourceSeries = resourcepool().renderTimeSeries().fit(curDate,endDate).Data
		count = curDate.daysTo(endDate) + 1
		
		calculateProjectRenderUsageWorker(count, projects, resourceSeries)
		
	def renderBacklogSeries(self, projectOrWorkload = None):
		if projectOrWorkload is not None:
			if isinstance(projectOrWorkload,Workload):
				return self.RenderStatusByWorkload[projectOrWorkload].RenderBacklog
			return self.RenderStatusByProject[projectOrWorkload].RenderBacklog
		return self.RenderStatus.RenderBacklog
	
	def renderUsageSeries(self, projectOrWorkload = None):
		if projectOrWorkload is not None:
			try:
				if isinstance(projectOrWorkload,Workload):
					return self.RenderStatusByWorkload[projectOrWorkload].RenderUsage
				return self.RenderStatusByProject[projectOrWorkload].RenderUsage
			except:
				return series.DaySeries.null()
		return self.RenderStatus.RenderUsage

	def renderFinishDate(self, projectOrWorkload):
		rsd = (isinstance(projectOrWorkload,Workload) and self.RenderStatusByWorkload or self.RenderStatusByProject)
		try:
			return rsd[projectOrWorkload].RenderUsage.lastNonZero()
		except KeyError:
			return None
		
class HistorySegment(object):
	def __init__(self,project=None):
		self.Project = project
		self.LoadedStart = None
		self.LoadedEnd = None

	def loadEmpty(self,startDate,endDate):
		self.Submitted = series.DaySeries(startDate,endDate,constant=0.0)
		self.Rendered = series.DaySeries(startDate,endDate,constant=0.0)
		self.LoadedStart = startDate
		self.LoadedEnd = endDate
		
	def loadHistory(self,startDate,endDate):
		size = startDate.daysTo(endDate)+1
		sub = series.DaySeries(startDate,endDate)
		ren = series.DaySeries(startDate,endDate)
		sub.fillZeros()
		ren.fillZeros()
		if self.Project:
			q = Database.current().exec_( "SELECT day, totaltimestarted / '1 hour'::interval, totaltimesubmitted / '1 hour'::interval from jobstat_daily_summary WHERE fkeyproject=? AND day >= ? AND day <= ? order by day asc",  [self.Project.key(),startDate,endDate] )
		else:
			q = Database.current().exec_( "SELECT day, sum(totaltimestarted) / '1 hour'::interval, sum(totaltimesubmitted) / '1 hour'::interval from jobstat_daily_summary WHERE day >= ? AND day <= ? group by day order by day asc",  [startDate,endDate] )

		while q.next():
			date = q.value(0).toDate()
			ren[date] = q.value(1).toDouble()[0]
			sub[date] = q.value(2).toDouble()[0]
		return (sub,ren)
	
	def loadHistoryFromQuery(self,startDate,endDate,q):
		size = startDate.daysTo(endDate)+1
		self.Submitted = sub = series.DaySeries(startDate,endDate)
		self.Rendered = ren = series.DaySeries(startDate,endDate)
		sub.fillZeros()
		ren.fillZeros()
		self.LoadedStart = startDate
		self.LoadedEnd = endDate
		while True:
			fkeyproject = q.value(0).toInt()[0]
			if fkeyproject != self.Project.key():
				break
			date = q.value(1).toDate()
			ren[date] = q.value(2).toDouble()[0]
			sub[date] = q.value(3).toDouble()[0]
			if not q.next():
				return False
		return True
	
	def loadDateRange(self,startDate,endDate,q = None):
		if self.LoadedStart is None or (startDate < self.LoadedStart and endDate > self.LoadedEnd):
			self.Submitted, self.Rendered = self.loadHistory(startDate,endDate)
			self.LoadedStart = startDate
			self.LoadedEnd = endDate
		elif startDate < self.LoadedStart:
			self.Submitted, self.Rendered = map( series.sum, zip( (self.Submitted, self.Rendered), self.loadHistory(startDate,self.LoadedStart) ) )
			self.LoadedStart = startDate
		elif endDate > self.LoadedEnd:
			self.Submitted, self.Rendered = map( series.sum, zip( (self.Submitted, self.Rendered), self.loadHistory(self.LoadedEnd,endDate) ) )
			self.LoadedEnd = endDate
	
	def submitted(self,startDate,endDate):
		self.loadDateRange(startDate,endDate)
		return self.Submitted.fit(startDate,endDate)
		
	def rendered(self,startDate,endDate):
		self.loadDateRange(startDate,endDate)
		return self.Rendered.fit(startDate,endDate)
	
	def backlog(self,startDate,endDate):
		return series.DaySeries.null()

class HistoryManager(object):
	def __init__(self):
		self.HistoryByProject = DefaultDict(HistorySegment,passKey=True)
	
	def loadHistory(self,startDate,endDate,projects):
		q = Database.current().exec_( "SELECT fkeyproject, day, totaltimestarted / '1 hour'::interval, totaltimesubmitted / '1 hour'::interval from jobstat_daily_summary WHERE fkeyproject IN (%s) AND day >= ? AND day <= ? order by fkeyproject, day asc" % ProjectList(projects).keyString(), [startDate,endDate] )
		cont = q.next()
		loaded = ProjectList()
		while cont:
			project = Project(q.value(0).toInt()[0])
			loaded.append(project)
			cont = self.HistoryByProject[project].loadHistoryFromQuery(startDate,endDate,q)
		for p in (projects - loaded):
			self.HistoryByProject[p].loadEmpty(startDate,endDate)
	
	def submitted(self,startDate,endDate,project=None):
		return self.HistoryByProject[project].submitted(startDate,endDate)

	def rendered(self,startDate,endDate,project=None):
		return self.HistoryByProject[project].rendered(startDate,endDate)

	def backlog(self,startDate,endDate,project=None):
		return self.HistoryByProject[project].backlog(startDate,endDate)
	
	def availableProjects(self):
		pass
	
_workpool = None
def workpool():
	global _workpool
	if _workpool is None:
		_workpool = WorkPool()
	return _workpool
	
_resourcepool = None
def resourcepool():
	global _resourcepool
	if _resourcepool is None:
		_resourcepool = ResourcePool()
	return _resourcepool

_weightManager = None
def weightManager():
	global _weightManager
	if _weightManager is None:
		_weightManager = ProjectWeightManager()
		_weightManager.loadFromDatabase()
	return _weightManager

_historyManager = None
def historyManager():
	global _historyManager
	if _historyManager is None:
		_historyManager = HistoryManager()
	return _historyManager

if __name__=="__main__":
	from blur.quickinit import *
	rp = ResourcePool()
	rp.loadCurrentHosts()
	wp = WorkPool()
	wp.loadDateRange(QDate.currentDate(), QDate.currentDate().addYears(1))
	pwm = ProjectWeightManager()
	pwm.dump()
