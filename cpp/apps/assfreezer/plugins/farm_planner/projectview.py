
import os

from PyQt4.QtCore import Qt, QDate, pyqtSignal
from PyQt4.QtGui import QAction, QDialog, QMenu, QColor, QBrush
from PyQt4.uic import *

from blur.Stone import Interval, RecordList
from blur.Stonegui import VirtualRecordItem, RecordTreeView, RecordSuperModel, ModelIter, PyRecordDataTranslator
from blur.Classes import ProjectResolution, Project, ProjectList, ProjectResolutionList
from blur.Classesui import ProjectResolutionDialog

from main import workpool

class WorkloadItem(VirtualRecordItem):
	Columns = [ 'Enabled', 'Project', 'Name', 'Width', 'Height', 'FPS', 'Run Length', 'Est. Seconds Per Pixel', 'Render Start', 'Render End', 'Modified', 'Render Finish Date' ]
	
	def __init__(self, workload):
		VirtualRecordItem.__init__(self)
		self.Workload = workload
		self.ProjectResolution = workload.ProjectResolution
		self.RenderFinishDate = None
	
	def renderFinishDate(self):
		if self.RenderFinishDate is None:
			self.RenderFinishDate = workpool().renderFinishDate(self.Workload)
		return self.RenderFinishDate
	
	def getRecord(self):
		return self.ProjectResolution

	def clearCache(self):
		self.RenderFinishDate = None
	
	# Copy-on-write symantics of records require that we get the modified record
	# back when the RecordDataTranslator handles edits
	def recordChanged(self,record):
		print record.dump()
		self.ProjectResolution = record
		self.Workload.ProjectResolution = record
		self.Workload.clearCache()
		self.clearCache()
		workpool().workloadChanged(self.Workload)

	def modelFlags(self, idx):
		flags = VirtualRecordItem.modelFlags(self,idx)
		col = idx.column()
		if col == 0:
			flags |= Qt.ItemIsUserCheckable
		elif col not in (1,10,11):
			flags = flags | Qt.ItemIsEditable
		return flags | Qt.ItemIsEnabled | Qt.ItemIsSelectable
	
	def setModelData(self, idx, val, role):
		if role == Qt.CheckStateRole and idx.column() == 0:
			self.Workload.Enabled = val.toBool()
			workpool().workloadChanged(self.Workload)
			return True
		if role == Qt.EditRole:
			if idx.column() in (6,7):
				iv, s = Interval.fromString(val.toString())
				if idx.column() == 6:
					self.Workload.ProjectResolution.setRunLength(iv)
				elif idx.column() == 7:
					self.Workload.ProjectResolution.setPixelRenderTimeEstimate(iv)
				self.Workload.clearCache()
				workpool().workloadChanged(self.Workload)
				return True
			elif idx.column() in (8,9):
				date = val.toDate()
				print date.toString()
				if idx.column() == 8:
					self.Workload.ProjectResolution.setRenderCrunchStart(date)
				elif idx.column() == 9:
					self.Workload.ProjectResolution.setRenderCrunchEnd(date)
				self.Workload.clearCache()
				workpool().workloadChanged(self.Workload)
				return True
		return VirtualRecordItem.setModelData(self,idx,val,role)
	
	def modelData(self,idx,role):
		col = idx.column()
		if col == 0 and role == Qt.CheckStateRole:
			return self.Workload.Enabled and Qt.Checked or Qt.Unchecked
		if role in (Qt.DisplayRole,Qt.EditRole):
			if col == 0:
				return ''
			elif col == 1:
				return self.ProjectResolution.project().name()
			elif col == 10:
				return self.ProjectResolution.isUpdated()
			elif col == 11:
				return self.renderFinishDate()
		if role == Qt.BackgroundRole:
			if col == 0:
				return self.Workload.brush()
			if col == 11:
				if self.renderFinishDate() > self.Workload.ProjectResolution.renderCrunchEnd():
					return QBrush(Qt.red)
			elif col not in (1,10):
				field = idx.model().translator(idx).recordColumnField(col)
				if self.Workload.ProjectResolution.isUpdated(field):
					return QBrush(Qt.yellow)
		return VirtualRecordItem.modelData(self,idx,role)

class WorkloadTree(RecordTreeView):
	selectedProjectsChanged = pyqtSignal(object)
	selectedWorkloadsChanged = pyqtSignal(object)
	
	def __init__(self,workPool,parent=None):
		RecordTreeView.__init__(self,parent)
		
		self.Model = RecordSuperModel(self)
		self.Model.setHeaderLabels( WorkloadItem.Columns )
		
		self.Trans = PyRecordDataTranslator(self.Model.treeBuilder(),WorkloadItem)
		PR = ProjectResolution.c
		self.Trans.setRecordColumnList( [None, PR.Name, PR.Name, PR.Width, PR.Height, PR.Fps, PR.RunLength, PR.PixelRenderTimeEstimate, PR.RenderCrunchStart, PR.RenderCrunchEnd, None,None], True )
		self.setModel(self.Model)

		self.WorkPool = workPool
		self.WorkPool.WorkloadAdded.connect( self.workloadAdded )
		self.WorkPool.WorkloadRemoved.connect( self.workloadRemoved )
		self.WorkPool.WorkloadChanged.connect( self.workloadChanged )
		self.WorkPool.CalculationsChanged.connect( self.clearCaches )
		
		self.setContextMenuPolicy( Qt.CustomContextMenu )
		self.customContextMenuRequested.connect( self.showMenu )
		self.NewWorkloadAction = QAction("Add Project Resolution", self)
		self.NewWorkloadAction.triggered.connect( self.newWorkload )

		self.EditWorkloadAction = QAction("Edit Project Resolution", self)
		self.EditWorkloadAction.triggered.connect( self.editWorkload )
		
		self.RemoveWorkloadAction = QAction("Remove Project Resolution(s)", self)
		self.RemoveWorkloadAction.triggered.connect( self.removeWorkload )
		
		self.CommitWorkloadsAction = QAction("Commit Changes To Selected", self)
		self.CommitWorkloadsAction.triggered.connect( self.commitWorkload )
		
		self.AddProjectWeightRowAction = QAction("Add Project Weight Row", self)
		self.AddProjectRenderRowAction = QAction("Add Project Render Row", self)
		
		self.selectionChanged.connect( self.slotSelectionChanged )

		for workload in self.WorkPool.Workloads:
			self.workloadAdded(workload)

	def clearCaches( self ):
		for idx in ModelIter.collect(self.Model):
			self.Trans.item(idx).clearCache()
			self.Model.updateIndex(idx)
	
	def slotSelectionChanged( self, s ):
		self.selectedProjectsChanged.emit( ProjectResolutionList(self.selection()).projects().unique() )
		self.selectedWorkloadsChanged.emit( [self.Trans.item(idx).Workload for idx in self.selectedRows()] )
		
	def workloadAdded(self, workload):
		self.Trans.append( WorkloadItem(workload) )
	
	def workloadRemoved(self, workload):
		self.Model.remove( RecordList(workload.ProjectResolution) )
	
	def workloadChanged(self, workload):
		item = self.Trans.item(self.Model.findIndex(workload.ProjectResolution))
		item.clearCache()
		self.Model.updated( workload.ProjectResolution )

	def newWorkload(self):
		ProjectResolutionDialog.create(Project(),self)

	def editWorkload(self):
		prl = ProjectResolutionList(self.selection())
		parent = self.window()
		for pr in prl:
			ProjectResolutionDialog.edit(pr,parent)
	
	def removeWorkload(self):
		prl = ProjectResolutionList(self.selection())
		prl.remove()
		
	def commitWorkload(self):
		self.selection().commit()
			
	def showMenu(self, pos):
		selSize = self.selection().size()
		mnu = QMenu(self)
		mnu.addAction( self.NewWorkloadAction )
		mnu.addAction( self.EditWorkloadAction )
		mnu.addAction( self.RemoveWorkloadAction )
		self.EditWorkloadAction.setEnabled( selSize >= 1 )
		mnu.addAction( self.CommitWorkloadsAction )
		mnu.addSeparator()
		mnu.addAction( self.AddProjectWeightRowAction )
		mnu.addAction( self.AddProjectRenderRowAction )
		mnu.exec_(self.mapToGlobal(pos))
	
