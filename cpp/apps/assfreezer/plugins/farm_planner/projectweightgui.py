
import os
import itertools
from math import ceil
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from PyQt4.QtSql import QSqlQuery
from PyQt4.uic import *
from blur.defaultdict import DefaultDict
from blur.Classes import ProjectWeightSchedule, ProjectStatusList, ProjectStatus, ProjectList
from blur.Stone import Interval, Database
from blur.Stonegui import pathFromNumpyArray
from main import ProjectWeightManager, workpool, resourcepool, weightManager, historyManager
from timeline import View, Scene, GraphRow, RowUser, Scale, RatioScale, colors
import series

clamp = lambda x, l, u: min(u,max(l,x))

# Returns two date ranges, the range from start up until the date before split
# And the range from split until endDate.  If startDate-endDate falls outside
# then one of the returned values is None
def splitDateRange(startDate,endDate,split):
	if startDate < split and endDate >= split:
		return (startDate,split.addDays(-1)), (split,endDate)
	elif startDate < split:
		return (startDate,endDate), None
	return None, (startDate,endDate)
	
class WeightMixin(object):
	def __init__(self,projectWeightManager = None):
		super(WeightMixin,self).__init__()
		self.MaxWeight = 1.0
		self.WeightManager = projectWeightManager
	
	def weightToY(self,weight):
		return self.Height - (weight / self.MaxWeight) * self.Height
	
	def yToWeight(self,y):
		return ((self.Height - y) / float(self.Height)) * self.MaxWeight

class TotalWeightRow(WeightMixin,GraphRow):
	def __init__(self,projectWeightManager = None,startDate=None,parentItem = None):
		WeightMixin.__init__(self,projectWeightManager)
		GraphRow.__init__(self,startDate,parentItem)
		self.MaxWeight = 2.0
		self.Rects = []
		self.createLabel( "Project Weight Totals" )
		self.OverWeight = False
		self.WeightManager.weightChange.connect( self.reset )
		
	def colorForWeight(self,weight):
		if weight > 1.001:
			return colors.ProjectWeightOverage
		return colors.ProjectWeight
		
	def createRect(self,dateStart,dateEnd,weight):
		rectItem = QGraphicsRectItem(self)
		y = self.weightToY( weight )
		height = self.Height - y
		rectItem.setRect( QRectF( 0, 0, dateStart.daysTo(dateEnd) * self.DayWidth, height ) )
		rectItem.setPos( QPointF( self.dateToX( dateStart ), y ) )
		rectItem.setPen( QPen() )
		rectItem.setBrush( self.colorForWeight(weight) )
		#weightLabel = QGraphicsTextItem(rectItem)
		#weightTextHeight = QFontMetrics(weightLabel.font()).height()
		#if weight > .55:
			#weightLabel.setPos( QPointF( 0, 0 ) )
		#else:
			#weightLabel.setPos( QPointF( 0, - weightTextHeight - 5 ) )
		#weightLabel.setPlainText( QString.number(int(weight * 100.0 + .01)) + "%" )
		self.Rects.append(rectItem)
	
	def reset(self,resetAllUsers=False):
		for rect in self.Rects:
			self.scene().removeItem(rect)
		self.Rects = []
		lastWeight = None
		lastDate = None
		curDate = self.DateStart
		self.OverWeight = False
		while curDate <= self.DateEnd:
			weight = self.WeightManager.totalWeight(curDate)
			if lastWeight is None:
				lastWeight = weight
				lastDate = curDate
			if lastWeight != weight or curDate == self.DateEnd:
				if curDate == self.DateEnd and lastWeight == weight:
					curDate = curDate.addDays(1)
				self.createRect(lastDate,curDate,lastWeight)
				if weight > 1.001 and curDate > QDate.currentDate():
					self.OverWeight = True
				lastWeight = weight
				lastDate = curDate
			curDate = curDate.addDays(1)

class ProjectWeightRow(WeightMixin,GraphRow):
	def __init__(self,projectWeightManager = None,startDate=None,project = None,parentItem = None,height=120):
		WeightMixin.__init__(self,projectWeightManager)
		GraphRow.__init__(self,startDate,parentItem,height)
		self.Project = project
		self.createLabel( project.name() )
		self.PenWidth = 3
		self.CurrentDateTime = None
		self.CDTSched = None
		self.reset()
	
	def lineColor(self,readOnly):
		if readOnly:
			return colors.ProjectWeightROLine
		return colors.ProjectWeightLine
		
	def linePen(self,readOnly):
		pen = QPen( self.lineColor(readOnly) )
		#pen.setCapStyle( Qt.RoundCap )
		pen.setWidth( self.PenWidth )
		return pen
	
	def lineLength(self, dateTime):
		lastDate = None
		if self.WeightManager:
			lastDateTime = self.WeightManager.nextWeightChange(self.Project,dateTime)
		if lastDateTime is None or lastDateTime > QDateTime(self.DateEnd):
			if lastDateTime is None and dateTime < self.CurrentDateTime:
				lastDateTime = self.CurrentDateTime
			else:
				lastDateTime = QDateTime(self.DateEnd.addDays(1))
		return self.dateTimeToX(lastDateTime) - self.dateTimeToX(dateTime)
	
	def splitSchedule(self,scheduleItem,x):
		splitDate = self.closestDate(x)
		
		pws = ProjectWeightSchedule()
		pws.setProject(self.Project)
		pws.setWeight(scheduleItem.weight())
			
		if scheduleItem.Schedule is not None:
			pws.setDateTime(scheduleItem.Schedule.dateTime())
			scheduleItem.Schedule.setDateTime( QDateTime(splitDate) )
			self.WeightManager.addSchedule(pws)
			gwsi = ScheduleItem(self,pws)
		else:
			pws.setDateTime(QDateTime(splitDate))
			self.WeightManager.addSchedule(pws)
			scheduleItem.Schedule = pws
			scheduleItem.setupMoveDateRect()
			gwsi = ScheduleItem(self,project=self.Project,startDate=scheduleItem.StartDate)
		
		if scheduleItem.Schedule == self.CDTSched:
			self.CDTSched = pws
		
		self.scheduleChanged()
	
	def scheduleWeightChanged(self,scheduleItem):
		# If the new value matches the previous schedule change, then we can delete this schedule as a unique entity
		# Dont combine the readonly to readwrite cutoff that happens on the current date
		if scheduleItem.Schedule is not None and self.WeightManager.weight(self.Project,scheduleItem.dateTime().addSecs(-1)) == scheduleItem.weight() and scheduleItem.dateTime() != self.CurrentDateTime:
			print "Removing Schedule Item", scheduleItem.Schedule.dump()
			self.removeScheduleItem(scheduleItem)
			
		# If the new value matches the next schedule change, then we can delete the next schedule change
		nextChangeDate = self.WeightManager.nextWeightChange(self.Project,scheduleItem.dateTime())
		if nextChangeDate is not None and self.WeightManager.weight(self.Project,nextChangeDate) == scheduleItem.weight():
			scheduleItem = self.scheduleItemForSchedule(self.WeightManager.schedule(self.Project,nextChangeDate))
			if scheduleItem:
				self.removeScheduleItem(scheduleItem)
	
		self.WeightManager.weightChange.emit()
		
	def scheduleItems(self):
		ret = []
		for item in self.childItems():
			if isinstance(item, ScheduleItem):
				ret.append(item)
		return ret
		
	def scheduleItemForSchedule(self, schedule):
		for item in self.scheduleItems():
			if item.Schedule == schedule:
				return item
		return None
	
	def scheduleItemsBetweenDates(self,first,second):
		start = first
		end = second
		if start > end:
			end = first
			start = second
		ret = []
		for item in self.scheduleItems():
			if item.date() >= start and item.date() <= end:
				ret.append(item)
		return ret
	
	def resetPreviousItem(self,item):
		closest = None
		for prev in self.scheduleItems():
			if prev != item and prev.date() < item.date() and (closest is None or closest.date() < prev.date()):
				closest = prev
		if closest is not None:
			closest.reset()
		
	def scheduleChanged(self):
		#self.WeightManager.weightChange.emit()
		for item in self.childItems():
			if isinstance(item, ScheduleItem):
				item.reset()
	
	def dateMoved(self,scheduleItem):
		self.WeightManager.resetProject(scheduleItem.Schedule.project())
		self.resetPreviousItem(scheduleItem)

	def addScheduleItem(self,scheduleItem):
		self.scene().addItem(scheduleItem)
		scheduleItem.setParentItem(self)
		self.WeightManager.addSchedule(scheduleItem.Schedule)
		self.scheduleChanged()
		
	def removeScheduleItem(self,scheduleItem):
		self.WeightManager.removeSchedule(scheduleItem.Schedule)
		self.scene().removeItem(scheduleItem)
		self.scheduleChanged()
		
	def reset(self,resetAllUsers=False):
		#print "Resetting project weight row for project", self.Project.name()
		self.WeightManager.dump(self.Project)
		self.CurrentDateTime = QDateTime.currentDateTime()
		cdt = self.CurrentDateTime
		for item in self.childItems():
			if isinstance(item,ScheduleItem):
				self.scene().removeItem(item)
		hasStartDateSched = False
		hasCDSched = False
		for sched in self.WeightManager.schedules(self.Project):
			if sched == self.CDTSched:
				sched.setDateTime( self.CurrentDateTime )
				hasCDSched = True
			dateTime = sched.dateTime()
			nextChange = self.WeightManager.nextWeightChange(self.Project,dateTime)
			print dateTime.toString(), nextChange and nextChange.toString()
			if nextChange is not None and nextChange < QDateTime(self.DateStart):
				continue
			if dateTime > QDateTime(self.DateEnd.addDays(1)):
				continue
			if dateTime <= QDateTime(self.DateStart):
				hasStartDateSched = True
			ScheduleItem(self,sched)
			if dateTime < cdt and (nextChange > cdt or nextChange is None):
				sched = ProjectWeightSchedule()
				sched.setProject(self.Project)
				sched.setWeight(self.Project.assburnerWeight())
				sched.setDateTime( cdt )
				self.CDTSched = sched
				self.WeightManager.addSchedule(sched)
				item = ScheduleItem(self,sched)
				hasCDSched = True
				#print "Split schedule at current date time", item
		if not hasCDSched and QDateTime(self.DateEnd) > cdt and QDateTime(self.DateStart) < cdt:
			sched = ProjectWeightSchedule()
			sched.setProject(self.Project)
			sched.setWeight(self.Project.assburnerWeight())
			sched.setDateTime( cdt )
			self.CDTSched = sched
			self.WeightManager.addSchedule(sched)
			item = ScheduleItem(self,sched)
			#print "Created new schedule at current date time", item
		if not hasStartDateSched and self.DateStart!=cdt.date():
			ScheduleItem(self,project=self.Project,startDate=self.DateStart)

class GraphDataSource(object):
	def __init__(self,parent=None):
		self.Parent = parent
		self.LoadedStart = None
		self.LoadedEnd = None
		self.Data = None
		self.NotifyChange = []
		self.Selected = False
		
	def name(self):
		return 'Name Me!'
	
	def pen(self):
		return QPen()
	
	def brush(self):
		return QBrush()
	
	def setSelected(self,sel):
		if isinstance(sel,bool) and sel != self.Selected:
			self.Selected = sel
			self.displayChanged()
	
	# Call this to notify any attached graphs that we need pen/brush
	# updated
	def displayChanged(self,dataSource=None):
		if self.Parent:
			self.Parent.displayChanged(self)
			
	def dataChanged(self,dataSource=None):
		self.LoadedStart = self.LoadedEnd = None
		self.Data = None
		if self.Parent:
			self.Parent.dataChanged(dataSource or self)
		
	def maxValue(self,startDate,endDate):
		self.checkLoad(startDate,endDate)
		if startDate == endDate:
			return self.Data[startDate]
		offset = self.LoadedStart.daysTo(startDate)
		cnt = startDate.daysTo(endDate) + 1
		cur = self.Data[offset:offset + cnt]
		if cur.size:
			return cur.max()
		return 0.0

	# Returns (Name,Pen,Brush,Value)
	def value(self,date):
		val = None
		if self.LoadedStart is not None and date >= self.LoadedStart and date <= self.LoadedEnd:
			val = self.Data[self.LoadedStart.daysTo(date)]
			print val
		else:
			print self.LoadedStart, self.LoadedEnd, date
		return (self.name(),self.pen(),self.brush(),val)

	# Simplest implementations just need to implement this
	def load(self,startDate,endDate):
		return None

	def checkLoad(self,startDate,endDate):
		if self.LoadedStart is None or (startDate < self.LoadedStart and endDate > self.LoadedEnd):
			self.Data = self.load(startDate,endDate)
			self.LoadedStart = startDate
			self.LoadedEnd = endDate
		elif startDate < self.LoadedStart:
			self.Data = self.load(startDate,self.LoadedStart) + self.Data
			self.LoadedStart = startDate
		elif endDate > self.LoadedEnd:
			self.Data = self.Data + self.load(self.LoadedEnd, endDate)
			self.LoadedEnd = endDate

class HistoryProjectionDataSource(GraphDataSource):
	def __init__(self,parent,historyDataSource=None,projectionDataSource=None):
		GraphDataSource.__init__(self,parent)
		self.CurrentDate = QDate.currentDate()
		self.HistoryDataSource = historyDataSource
		if self.HistoryDataSource:
			self.HistoryDataSource.Parent = self
		self.ProjectionDataSource = projectionDataSource
		if self.ProjectionDataSource:
			self.ProjectionDataSource.Parent = self
	
	# Returns (Name,Value)
	def value(self,date):
		if self.ProjectionDataSource and date >= self.CurrentDate:
			return self.ProjectionDataSource.value(date)
		elif self.HistoryDataSource and date < self.CurrentDate:
			return self.HistoryDataSource.value(date)
		return GraphDataSource.value(self,date)

	def loadHistory(self,startDate,endDate):
		if self.HistoryDataSource:
			return self.HistoryDataSource.load(startDate,endDate)
		return None
	
	def loadProjections(self,startDate,endDate):
		if self.ProjectionDataSource:
			return self.ProjectionDataSource.load(startDate,endDate)
		return None

	def reloadProjections(self):
		historyDateRange, projectionDateRange = splitDateRange(self.LoadedStart,self.LoadedEnd,self.CurrentDate)
		if projectionDateRange:
			data = self.loadProjections(*projectionDateRange)
			self.Data = series.sum( (self.Data.trim(*historyDateRange), data) )
			self.ParentRow.reset()

	def checkLoadHistory(self,startDate,endDate):
		if self.LoadedStart is None or (startDate < self.LoadedStart and endDate > self.LoadedEnd):
			self.Data = self.loadHistory(startDate,endDate)
			self.LoadedStart = startDate
			self.LoadedEnd = endDate
		elif startDate < self.LoadedStart:
			self.Data = self.loadHistory(startDate,self.LoadedStart) + self.Data
			self.LoadedStart = startDate
		elif endDate > self.LoadedEnd:
			self.Data = self.Data + self.loadHistory(startDate,self.LoadedStart)
			self.LoadedEnd = endDate
	
	def checkLoadProjections(self,startDate,endDate):
		if self.LoadedStart is None or (startDate < self.LoadedStart and endDate > self.LoadedEnd):
			self.Data = self.loadProjections(startDate,endDate)
			self.LoadedStart = startDate
			self.LoadedEnd = endDate
		elif startDate < self.LoadedStart:
			self.Data = self.loadProjections(startDate,self.LoadedStart) + self.Data
			self.LoadedStart = startDate
		elif endDate > self.LoadedEnd:
			self.Data = self.Data + self.loadProjections(self.LoadedEnd, endDate)
			self.LoadedEnd = endDate
	
	def checkLoad(self,startDate,endDate):
		historyRange, projectionRange = splitDateRange(startDate,endDate,self.CurrentDate)
		if historyRange:
			self.checkLoadHistory(*historyRange)
		if projectionRange:
			self.checkLoadProjections(*projectionRange)

class FixedPathItem(QGraphicsPathItem):
	def paint(self,painter,option,widget):
		painter.setBackground( QBrush(Qt.white) )
		QGraphicsPathItem.paint(self,painter,option,widget)

class BaseGraph(RowUser):
	def __init__(self,parentRow,scale):
		RowUser.__init__(self,parentRow)
		self.DataSources = []
		self.Scale = scale
		self.Paths = []
		self.PathsByDataSource = {}
		
	def dataChanged(self,dataSource):
		self.scheduleReset()
		#self.ParentRow.reset()
		#self.Scale.recalculate()
		#self.Scale.reset()
		#self.reset()
	
	def displayChanged(self,dataSource):
		pathItem = self.PathsByDataSource.get(dataSource,None)
		if pathItem:
			self.setPathDisplay(dataSource,pathItem)
	
	def maxValue(self):
		self.checkLoad()
		ret = 0.0
		for ds in self.DataSources:
			ret = max(ret,ds.maxValue(self.DateStart,self.DateEnd))
		return ret
	
	def values(self,date):
		ret = []
		for ds in self.DataSources:
			ret.append(ds.value(date))
		return ret
	
	@staticmethod
	def createPathWorker(data, offset, count, dayWidth, height, scaleMax):
		return pathFromNumpyArray(data.Data,offset,count,dayWidth,height,scaleMax)
		path = QPainterPath()
		mul = height / float(scaleMax)
		path.moveTo(0.0,data[offset] * mul)
		cur = 1
		lastVal = 0.0
		while cur <= count:
			path.lineTo(cur * dayWidth,lastVal)
			val = data[cur + offset] * mul
			path.lineTo(cur * dayWidth,val)
			lastVal = val
			cur += 1
		return path

	def createPath(self,data,offset):
		path = self.createPathWorker(data, offset, self.dayCount(), self.DayWidth, -self.Height, self.Scale.ScaleMax)
		#path.setFillRule(Qt.WindingFill)
		
		transform = QTransform()
		transform.translate(0,self.Height)
		#transform.scale(1.0,-1.0)
		
		pathItem = FixedPathItem(path,self)
		pathItem.setTransform(transform)
		self.Paths.append( pathItem )
		return pathItem

	def setPathDisplay(self,dataSource,pathItem):
		pathItem.setPen( dataSource.pen() )
		brush = dataSource.brush()
		if brush:
			pathItem.setBrush( brush )

	def addPath(self,dataSource,pathItem):
		self.setPathDisplay(dataSource,pathItem)
		self.PathsByDataSource[dataSource] = pathItem
	
	def createPaths(self):
		for ds in self.DataSources:
			self.addPath( ds, self.createPath(ds.Data,ds.LoadedStart.daysTo(self.DateStart)) )

	def checkLoad(self):
		startDate, endDate = self.timelineBrowser().timelineRange()
		for ds in self.DataSources:
			ds.checkLoad(startDate,endDate)
	
	def reset(self,resetAllUsers=False):
		self.checkLoad()
		for path in self.Paths:
			self.scene().removeItem(path)
		self.Paths = []
		self.PathsByDataSource = {}
		self.createPaths()

class StackedGraph(BaseGraph):
	def maxValue(self):
		self.checkLoad()
		data = None
		for ds in self.DataSources:
			if data is None:
				data = ds.Data
			else:
				data = ds.Data + data
		ret = 0.0
		for d in data.Data:
			ret = max(ret,d)
		return ret

	def createPaths(self):
		data = None
		lastPath = None
		for ds in self.DataSources:
			if data is None:
				data = ds.Data
			else:
				data = ds.Data + data
			path = self.createPath(data, data.Start.daysTo(self.DateStart))
			self.addPath(ds,path)
			if lastPath:
				path.stackBefore(lastPath)
			lastPath = path

class RenderDataSourceMixin(object):
	def name(self):
		if self.Project:
			return "%s Render" % self.Project.name()
		return "Total Render"
	
	def pen(self):
		return QPen(Qt.blue,1.0)
	
	def brush(self):
		if hasattr(self,'Workload') and self.Workload is not None:
			if self.Selected:
				return self.Workload.selectedBrush()
			return self.Workload.brush()
		if self.Project is not None:
			col = QColor(workpool().projectColor(self.Project))
			if self.Alpha:
				col.setAlpha(self.Alpha)
			if self.Selected:
				return patternBrush(col,Qt.BDiagPattern,Qt.red)
			return QBrush(col)
		return QBrush()
		
class RenderHistoryDataSource(RenderDataSourceMixin,GraphDataSource):
	def __init__(self,parent,project=None,alpha=None):
		GraphDataSource.__init__(self,parent)
		self.Project = project
		self.Alpha = alpha
		workpool().CalculationsChanged.connect(self.dataChanged)
	
	def load(self,startDate,endDate):
		ret = historyManager().rendered(startDate,endDate,self.Project).movingAverage(2)
		ret[QDate.currentDate()] = 0
		return ret

class RenderProjectionDataSource(RenderDataSourceMixin,GraphDataSource):
	def __init__(self,parent,project=None,workload=None):
		GraphDataSource.__init__(self,parent)
		self.Project = project
		self.Workload = workload
		workpool().CalculationsChanged.connect(self.dataChanged)

	def load(self,startDate,endDate):
		print startDate, endDate
		wp = workpool()
		wp.loadDateRange(startDate,endDate)
		return wp.renderUsageSeries(self.Workload or self.Project).fit(startDate,endDate)

class RenderDataSource(RenderDataSourceMixin,HistoryProjectionDataSource):
	def __init__(self,parent,project=None):
		HistoryProjectionDataSource.__init__(self,parent,RenderHistoryDataSource(self,project),RenderProjectionDataSource(self,project))
		self.Project = project

class RenderGraph(BaseGraph):
	def __init__(self,parentRow,scale,project=None):
		BaseGraph.__init__(self,parentRow,scale)
		self.DataSources.append(RenderDataSource(self,project))

class SubmissionHistoryDataSource(GraphDataSource):
	def __init__(self,parent,project=None):
		GraphDataSource.__init__(self,parent)
		self.Project = project
	def name(self):
		return '%s Submission History' % (self.Project and self.Project.name() or 'Total')
	def load(self,startDate,endDate):
		return historyManager().submitted(startDate,endDate,self.Project).movingAverage(3)

class SubmissionProjectionDataSource(GraphDataSource):
	def __init__(self,parent,project=None,workload=None):
		GraphDataSource.__init__(self,parent)
		self.Project = project
		self.Workload = workload
		workpool().CalculationsChanged.connect(self.dataChanged)

	def name(self):
		if self.Workload:
			name = "%s %s" % (self.Workload.Project.name(), self.Workload.ProjectResolution.name())
		elif self.Project:
			name = self.Project.name()
		else:
			name = 'Total'
		return '%s Submission Projection' % name
		
	def load(self,startDate,endDate):
		wp = workpool()
		wp.loadDateRange(startDate,endDate)
		if self.Workload:
			ret = self.Workload.renderTimeSubmittedSeries().fit(startDate,endDate)
			return ret
		return wp.renderTimeSubmittedSeries(self.Project).fit(startDate,endDate)

	def brush(self):
		if self.Workload:
			if self.Selected:
				return self.Workload.selectedBrush()
			return self.Workload.brush()
		return QBrush()
	
class SubmissionDataSource(HistoryProjectionDataSource):
	def __init__(self,parent,project=None):
		HistoryProjectionDataSource.__init__(self,parent,SubmissionHistoryDataSource(self,project),SubmissionProjectionDataSource(self,project))

	def pen(self):
		return QPen(Qt.black,1.0)

	def brush(self):
		return QBrush()
		
class SubmissionGraph(BaseGraph):
	def __init__(self,parentRow,scale,project=None):
		BaseGraph.__init__(self,parentRow,scale)
		if project is None:
			self.DataSources.append(SubmissionDataSource(self))
		else:
			for wl in workpool().projectWorkloads(project):
				self.DataSources.append(SubmissionProjectionDataSource(self,workload=wl))
			self.DataSources.append(SubmissionHistoryDataSource(self,project))

class BacklogDataSource(HistoryProjectionDataSource):
	def __init__(self,parent,project=None):
		HistoryProjectionDataSource.__init__(self,parent)
		self.Project = project
		workpool().CalculationsChanged.connect(self.dataChanged)
		
	def name(self):
		return '%s Backlog' % (self.Project and self.Project.name() or 'Total')

	def brush(self):
		if self.Project is not None:
			col = workpool().projectColor(self.Project)
			#col.setAlpha(80)
			return QBrush(col)
		color = QColor(Qt.red)
		color.setAlpha(128)
		return QBrush(color)
		
	def loadHistory(self,startDate,endDate):
		return historyManager().backlog(startDate,endDate,self.Project).fit(startDate,endDate)
	
	def loadProjections(self,startDate,endDate):
		wp = workpool()
		wp.loadDateRange(startDate,endDate)
		rp = resourcepool()
		return wp.renderBacklogSeries(self.Project).fit(startDate,endDate) / rp.renderTimeSeries().fit(startDate,endDate)
		
class BacklogGraph(BaseGraph):
	def __init__(self,parentRow,scale,project=None):
		BaseGraph.__init__(self,parentRow,scale)
		self.DataSources.append(BacklogDataSource(self,project))

class AvailableRenderDataSource(GraphDataSource):
	def __init__(self,parent):
		GraphDataSource.__init__(self,parent)
		resourcepool().ResourceChanged.connect(self.dataChanged)
		resourcepool().ResourceAdded.connect(self.dataChanged)
		resourcepool().ResourceRemoved.connect(self.dataChanged)

	def name(self):
		return 'Total Available Rendering'

	def pen(self):
		return QPen(Qt.green, 1.0)
	
	def load(self,startDate,endDate):
		return resourcepool().renderTimeSeries().fit(startDate,endDate)
	
class AvailableRenderGraph(BaseGraph):
	def __init__(self,parentRow,scale):
		BaseGraph.__init__(self,parentRow,scale)
		self.DataSources.append(AvailableRenderDataSource(self))

	def pen(self):
		return QPen(Qt.green, 1.0)
	
	def load(self,startDate,endDate):
		return None
	
class ScheduleItem(QGraphicsItemGroup):
	MoveWeight = 1
	MoveDate = 2
	
	def __init__(self,row,projectWeightSchedule=None,project=None,startDate=None):
		QGraphicsItemGroup.__init__(self,row)
		self.Row = row
		self.Project = project
		self.Schedule = projectWeightSchedule
		self.StartDate = startDate
		self.MoveType = None
		self.Line = QGraphicsLineItem(self)
		self.Line.setPen(self.Row.linePen(self.readOnly()))
		self.setFlags( self.ItemIsMovable | self.ItemIsSelectable | self.ItemIsFocusable )
		if not self.readOnly():
			self.Line.setCursor( QCursor( Qt.SizeVerCursor ) )
		self.MoveDateRect = None
		self.WeightLabel = QGraphicsTextItem(self)
		self.WeightTextHeight = QFontMetrics(self.WeightLabel.font()).height()
		# Date at the start of a date change
		self.InitialDate = None
		# This holds any ScheduleItems that are temporarily removed from an ongoing date change of this item
		self.DateChangeDiscards = []
		self.reset()
		
	def setupMoveDateRect(self):
		if self.Schedule is not None and self.dateTime() > QDateTime.currentDateTime() and self.dateTime() >= self.Row.DateStart:
			self.MoveDateRect = QGraphicsRectItem(QRectF(-3,-3,6,6),self)
			self.MoveDateRect.setCursor( QCursor( Qt.SizeHorCursor ) )
			self.MoveDateRect.setBrush( self.Row.lineColor(self.readOnly()) )
#			self.MoveDateRect.setPen( self.Timeline.lineColor(self.readOnly() ) )
		elif self.MoveDateRect:
			self.scene().removeItem(self.MoveDateRect)
			self.MoveDateRect = None
		
	def hoverEnterEvent(self,evt):
		self.setSelected(True)
		
	def itemChange(self,change,value):
		if change == QGraphicsItem.ItemSelectedChange:
			print "Item selected is", value.toBool()
		return QGraphicsItemGroup.itemChange(self,change,value)
	
	def dateTime(self):
		if self.Schedule is not None:
			return self.Schedule.dateTime()
		return QDateTime(self.StartDate)
		
	def date(self):
		return self.dateTime().date()
	
	def readOnly(self):
		if self.dateTime() < self.Row.CurrentDateTime:
			return True
		return False
	
	def setDate(self,date):
		if self.Schedule is not None:
			self.Schedule.setDateTime( QDateTime(date) )

	def weight(self):
		if self.Schedule is not None:
			return self.Schedule.weight()
		return self.Project.assburnerWeight()
	
	def setWeight(self,weight):
		if self.Schedule is not None:
			self.Schedule = self.Row.WeightManager.setWeight(self.Schedule.project(),weight,self.Schedule)
		# If we dont have a schedule and aren't readonly, then we are setting the current weighting without a schedule
		elif not self.readOnly():
			self.Row.WeightManager.addSchedule(self.Schedule)
#			self.Project = self.Timeline.WeightManager.setWeight(self.Project,weight)
	
	def reset(self,resetAllUsers=False):
		self.resetPos()
		self.setupMoveDateRect()
		self.Line.setLine( QLineF( self.Row.PenWidth / 2.0, 0, self.Row.lineLength(self.dateTime()) - self.Row.PenWidth / 2.0, 0 ) )
		
	def resetPos(self):
		if self.weight() > .55:
			self.WeightLabel.setPos( QPointF( 0, 0 ) )
		else:
			self.WeightLabel.setPos( QPointF( 0, - self.WeightTextHeight - 5 ) )
		self.WeightLabel.setPlainText( QString.number(int(self.weight() * 100.0 + .01)) + "%" )
		self.setPos( QPointF( self.Row.dateTimeToX(self.dateTime()), self.Row.weightToY(self.weight()) ) )
		
	def changeDate(self,date):
		discards = self.Row.scheduleItemsBetweenDates(self.InitialDate,date)
		try:
			discards.remove(self)
		except: pass
		# Check to see if any should be readded
		for item in self.DateChangeDiscards:
			if (item.date() > date and item.date() > self.InitialDate) or (item.date() < date and item.date() < self.InitialDate):
				self.Row.addScheduleItem(item)
				self.DateChangeDiscards.remove(item)
		for item in discards:
			self.Row.removeScheduleItem(item)
		self.DateChangeDiscards += discards
		self.setDate( date )
		self.Row.dateMoved(self)
		self.reset()
		
	def revertDateChange(self):
		for item in self.DateChangeDiscards:
			self.Row.addScheduleItem(item)
		self.setDate( self.InitialDate)
		self.Row.scheduleChanged()
		
	def applyDateChange(self):
		for item in self.DateChangeDiscards:
			self.Row.removeScheduleItem(item)
		
	def mousePressEvent(self,evt):
		if not self.readOnly():
			bt = evt.button(); mod = evt.modifiers()
			isSplit = (bt == Qt.LeftButton and mod & Qt.ControlModifier) or bt == Qt.MidButton
			if (not isSplit) and bt == Qt.LeftButton:
				if self.MoveDateRect and self.MoveDateRect.contains( evt.pos() ):
					self.MoveType = self.MoveDate
					self.InitialDate = self.date()
				else:
					self.MoveType = self.MoveWeight
			elif isSplit:
				print "Splitting!"
				self.Row.splitSchedule(self,self.Row.mapFromScene(evt.scenePos()).x())
				self.MoveType = self.MoveWeight
		QGraphicsItemGroup.mousePressEvent(self,evt)
	
	def mouseMoveEvent(self,evt):
		print "Move"
		if self.MoveType == self.MoveDate:
			self.changeDate( self.Row.closestDate( evt.scenePos().x() ) )
		elif self.MoveType == self.MoveWeight:
			weight = self.Row.yToWeight(self.Row.mapFromScene(evt.scenePos()).y())
			print "Moving weight to ", weight
			self.setWeight( clamp(weight,0.0,1.0) )
			self.resetPos()
			#self.Row.WeightManager.weightChange.emit()
			
	def mouseReleaseEvent(self,evt):
		print "Release"
		if self.MoveType == self.MoveWeight:
			self.Row.scheduleWeightChanged(self)
		elif self.MoveType == self.MoveDate:
			self.applyDateChange()
		self.MoveType = 0

	def contextMenuEvent(self,evt):
		if self.dateTime() > self.Row.CurrentDateTime:
			menu = QMenu(evt.widget())
			remove = menu.addAction( "Remove this scheduled weight change" )
			result = menu.exec_(evt.screenPos())
			if result == remove:
				self.Row.removeScheduleItem(self)

# Move to common or split off common functionality
class ScheduleScene(Scene):
	def __init__(self):
		Scene.__init__(self)
		self.WeightManager = weightManager()
		self.WeightManager.projectAdded.connect( self.addProjectSlot )
		self.ResourcePool = resourcepool()
		self.ResourcePool.ResourceAdded.connect( self.reset )
		self.ResourcePool.ResourceRemoved.connect( self.reset )
		self.ResourcePool.ResourceChanged.connect( self.reset )
		
		#self.TotalWeightRow = TotalWeightRow(self.WeightManager,self.StartDate)
		#self.addRow(self.TotalWeightRow)

		workpool().loadDateRange( QDate.currentDate().addDays(-30), self.TimelineBrowser.timelineRange()[1] )
		
		self.AllProjectsSubmitGraph = None
		self.AllProjectsRenderGraph = None
		self.AvailableRenderGraph = None
		self.ProjectWeightRowsByProject = {}
		
		separateSubmitRow = False
		backlogRow = True
		stackedRenderHistory = True
		
		if stackedRenderHistory:
			historyProjects = ProjectList(self.WeightManager.projects())
		else:
			historyProjects = ProjectList()

		workpool().loadProjectColors( historyProjects + ProjectList([wl.Project for wl in workpool().Workloads]) )
		
		backlogRowHeight = 80
		height = min((450 - (backlogRow and backlogRowHeight or 0)) / (1 + (separateSubmitRow and 1 or 0)), 200)
		
		# Render and submission in one row
		self.RenderRow = GraphRow(self.StartDate,height = height)
		self.RenderScale = Scale(self.RenderRow, 'Host Days', divider=24.0)

		#self.RenderGraph = RenderGraph(self.RenderRow,self.RenderScale)
		#self.RenderScale.ScaleMakers.append( self.RenderGraph )

		self.addRow(self.RenderRow)
		
		self.AvailableRenderGraph = AvailableRenderGraph(self.RenderRow,self.RenderScale)
		self.RenderScale.ScaleMakers.append( self.AvailableRenderGraph )

		self.AllProjectsRenderGraph = StackedGraph(self.RenderRow,self.RenderScale)
		if stackedRenderHistory:
			historyManager().loadHistory( *(list(self.TimelineBrowser.timelineRange()) + [historyProjects]) )
			for proj in historyProjects:
				self.AllProjectsRenderGraph.DataSources.append(RenderHistoryDataSource(self.AllProjectsRenderGraph,proj))
		
		for wl in workpool().Workloads:
			self.AllProjectsRenderGraph.DataSources.append(RenderProjectionDataSource(self.AllProjectsRenderGraph,wl.Project,wl))
		
		if separateSubmitRow:
			self.SubmitRow = GraphRow(self.StartDate,height=height)
			self.SubmitScale = Scale(self.SubmitRow, 'Host Days', divider=24.0)
			self.addRow(self.SubmitRow)
		else:
			self.SubmitRow = self.RenderRow
			self.SubmitScale = self.RenderScale

		if separateSubmitRow:
			self.AllProjectsSubmitGraph = StackedGraph(self.SubmitRow,self.SubmitScale)
			for workload in workpool().Workloads:
				self.AllProjectsSubmitGraph.DataSources.append(SubmissionProjectionDataSource(self.AllProjectsSubmitGraph,workload=workload))

			self.SubmissionGraph = SubmissionGraph(self.SubmitRow,self.SubmitScale)
			self.SubmitScale.ScaleMakers.append(self.SubmissionGraph)
			
		# Backlog in another
		if backlogRow:
			self.BacklogRow = GraphRow(self.StartDate,height=backlogRowHeight)
			self.BacklogScale = Scale(self.BacklogRow,'Days')
			self.BacklogGraph = BacklogGraph(self.BacklogRow,self.BacklogScale)
			self.BacklogScale.ScaleMakers.append(self.BacklogGraph)
			self.addRow(self.BacklogRow)
		else:
			self.BacklogRow = None
		
		self.GraphsByProject = DefaultDict(list)
		self.ProjectsShown = ProjectList()
		self.SelectedDataSources = []
		self.SelectedWorkloads = set()
		self.SelectedWorkloadDataSources = []
		#self.addAllProjects()

	def findRenderDataSourcesByWorkload(self,workload):
		if self.AllProjectsRenderGraph:
			for ds in self.AllProjectsRenderGraph.DataSources:
				if hasattr(ds,'Workload') and ds.Workload == workload:
					yield ds
		if self.AllProjectsSubmitGraph:
			for ds in self.AllProjectsSubmitGraph.DataSources:
				if hasattr(ds,'Workload') and ds.Workload == workload:
					yield ds
	
	def setSelectedWorkloads(self,workloads):
		workloads = set(list(workloads))
		toAdd = workloads - self.SelectedWorkloads
		toRemove = self.SelectedWorkloads - workloads
		self.SelectedWorkloads = workloads
		for ds in self.SelectedWorkloadDataSources[:]:
			if ds.Workload in toRemove:
				ds.setSelected(False)
				self.SelectedWorkloadDataSources.remove(ds)
		for wl in toAdd:
			for ds in self.findRenderDataSourcesByWorkload(wl):
				ds.setSelected(True)
				self.SelectedWorkloadDataSources.append(ds)
	
	def addProjectWeightRow(self,project):
		pwr = ProjectWeightRow( weightManager(), self.TimelineBrowser.currentRange()[0], project )
		self.addRow(pwr)
		self.ProjectWeightRowsByProject[project] = pwr
	
	def addProjectRenderRow(self,project):
		prr = GraphRow(self.StartDate,height=100,name=project.name())
		scale = Scale(prr, 'Host Days', divider=24.0)
		self.addRow(prr)
		stacked = StackedGraph(prr, scale)
		for wl in workpool().projectWorkloads(project):
			print "Adding workload", wl
			stacked.DataSources.append(SubmissionProjectionDataSource(stacked,workload=wl))
		renderGraph = BaseGraph( prr, scale )
		renderGraph.DataSources.append(RenderHistoryDataSource(renderGraph,project,alpha=120))
		scale.ScaleMakers.append(stacked)
		scale.ScaleMakers.append(renderGraph)

	def setProjectsShown(self,projects):
		toAdd = projects - self.ProjectsShown
		toRemove = self.ProjectsShown - projects
		self.ProjectsShown = projects
		for project in toAdd:
			print "Adding project", project.displayName()
			self.addProject(project)
		for project in toRemove:
			self.removeProject(project)
		
	def addAllProjects(self):
		for proj in self.WeightManager.projects():
			self.addProject(proj)

	def findRenderDataSources(self,project):
		for ds in self.AllProjectsRenderGraph.DataSources:
			if ds.Project == project:
				yield ds
		for ds in self.AllProjectsSubmitGraph.DataSources:
			if ds.Project == project:
				yield ds
	
	def addProject(self,project):
		print "Adding project", project
		addProjectWeightRow(project)
		if False:
			self.GraphsByProject[project] += [
				RenderGraph( self.RenderRow, self.RenderScale, project ),
				SubmissionGraph( self.SubmitRow, self.SubmitScale, project ) ]
			if self.BacklogRow:
				self.GraphsByProject[project].append( BacklogGraph( self.BacklogRow, self.BacklogScale, project ) )
		
	def removeProject(self,project):
		print "Removing project", project
		pwr = self.ProjectWeightRowsByProject.pop(project,None)
		if pwr:
			self.removeRow( pwr )
		return
		for item in self.GraphsByProject[project]:
			self.removeItem(item)
		del self.GraphsByProject[project]
	
	def addProjectSlot(self,project):
		self.addProject(project)
		self.resize()
	
# Move to common
class ScheduleView(View):
	def __init__(self,parentWidget = None):
		self.Scene = ScheduleScene()
		View.__init__(self,self.Scene,parentWidget)
		#self.setHorizontalScrollBarPolicy( Qt.ScrollBarAlwaysOff )

def createTestProjectTimeline(projectName,pwm):
	project = Project()
	project.setName( projectName )
	pws = ProjectWeightSchedule()
	pws.setDateTime( QDateTime(QDate.currentDate().addDays(-20)) )
	pws.setWeight( .5 )
	pws.setProject(project)
	pws2 = ProjectWeightSchedule()
	pws2.setDateTime( QDateTime(QDate.currentDate().addDays(-10)) )
	pws2.setWeight( .7 )
	pws2.setProject(project)
	for s in (pws,pws2):
		pwm.insertSchedule(s)
	gti = ProjectWeightRow(pwm,project)
	return gti
	

