
from PyQt4.QtCore import Qt, QDate, QModelIndex
from PyQt4.QtGui import QAction, QDialog, QMenu
from PyQt4.uic import *

from blur.Stone import Interval
from blur.Stonegui import VirtualRecordItem, RecordTreeView, RecordSuperModel, ModelIter, PyRecordDataTranslator
from blur.Classes import HostSchedule, HostScheduleList

from main import resourcepool, FarmResource

class ResourceItem(VirtualRecordItem):
	Columns = [ 'Enabled', 'Name', 'Count', 'Cores/Host', 'Mhz', 'DailyAvgRenderTime', 'RenderPower', 'OnlineDate', 'OfflineDate', 'Modified' ]
	
	def __init__(self, resource):
		VirtualRecordItem.__init__(self)
		self.Resource = resource

	def getRecord(self):
		return self.Resource.HostSchedule

	# Copy-on-write symantics of records require that we get the modified record
	# back when the RecordDataTranslator handles edits
	def recordChanged(self,record):
		self.Resource.HostSchedule = record
		resourcepool().resourceChanged(self.Resource)
	
	def modelFlags(self, idx):
		flags = VirtualRecordItem.modelFlags(self,idx)
		if idx.column() == 0:
			return Qt.ItemIsEnabled | Qt.ItemIsEditable | Qt.ItemIsUserCheckable
		return flags
		
	def setModelData(self, idx, val, role):
		col = idx.column()
		if role == Qt.CheckStateRole and col == 0:
			self.Resource.Enabled = val.toBool()
			resourcepool().resourceChanged(self.Resource)
			return True
		import traceback
		traceback.print_stack()
		if col in (7,8) and val.type() == QVariant.DateTime:
			val = QVariant(val.toDateTime().date())
			print val
		return VirtualRecordItem.setModelData(self,idx,val,role)
	
	def modelData(self,idx,role):
		if idx.column() == 0 and role == Qt.CheckStateRole:
			return self.Resource.Enabled and Qt.Checked or Qt.Unchecked
		if role in (Qt.DisplayRole,Qt.EditRole):
			if idx.column() == 9:
				return self.Resource.Modified
			elif idx.column() == 6:
				return self.Resource.RenderPower
			elif idx.column() == 0:
				return ''
		return VirtualRecordItem.modelData(self,idx,role)

class ResourceTree(RecordTreeView):
	def __init__(self,resourcePool,parent=None):
		RecordTreeView.__init__(self,parent)
		
		self.Model = RecordSuperModel(self)
		self.Model.setHeaderLabels( ResourceItem.Columns )
		
		self.Trans = PyRecordDataTranslator(self.Model.treeBuilder(),ResourceItem)
		HS = HostSchedule.c
		self.Trans.setRecordColumnList( [HS.Name, HS.Name, HS.Count, HS.Cores, HS.Mhz, HS.AvgDailyRenderTime, HS.Name, HS.OnlineDate, HS.OfflineDate, HS.Name], True )
		self.setModel(self.Model)
		
		self.ResourcePool = resourcePool
		self.ResourcePool.ResourceAdded.connect( self.resourceAdded )
		self.ResourcePool.ResourceRemoved.connect( self.resourceRemoved )
		self.ResourcePool.ResourceChanged.connect( self.resourceChanged )

		self.setContextMenuPolicy( Qt.CustomContextMenu )
		self.customContextMenuRequested.connect( self.showMenu )

		self.NewResourceAction = QAction("Add Resource", self)
		self.NewResourceAction.triggered.connect( self.newResource )

		self.CommitResourcesAction = QAction("Commit Changes To Selected", self)
		self.CommitResourcesAction.triggered.connect( self.commitResources )

		for resource in self.ResourcePool.Resources:
			self.Trans.append( ResourceItem(resource) )

	def resourceAdded(self, resource):
		self.Trans.append( ResourceItem(resource) )
	
	def resourceRemoved(self, resource):
		self.Model.remove( resource.HostSchedule )

	def resourceChanged(self, resource):
		self.Model.updated( resource.HostSchedule )
		
	def newResource(self):
		rd = ResourceDialog(self)
		rd.show()

	def commitResources(self):
		self.selection().commit()

	def showMenu(self, pos):
		mnu = QMenu(self)
		mnu.addAction( self.NewResourceAction )
		mnu.addAction( self.CommitResourcesAction )
		mnu.exec_(self.mapToGlobal(pos))
		
class ResourceDialog( QDialog ):
	def __init__(self,parent=None):
		QDialog.__init__(self,parent)
		loadUi( "resourcedialogui.ui", self )
		self.mOnlineDate.setDate( QDate.currentDate() )
		self.mOfflineDate.setDate( QDate.currentDate().addYears(5) )
	
	def toResource(self):
		hs = HostSchedule()
		hs.setName(self.mNameEdit.text())
		hs.setCount(self.mCountSpin.value())
		hs.setCores(self.mCoresSpin.value())
		hs.setMhz(self.mMhzSpin.value())
		hs.setAvgDailyRenderTime(Interval.fromString( '1 hour' )[0] * self.mRenderTimeSpin.value())
		hs.setOnlineDate(self.mOnlineDate.date())
		hs.setOfflineDate(self.mOfflineDate.date())
		ret = FarmResource(hs)
		return ret
	
	def accept(self):
		resourcepool().addResource( self.toResource() )
		QDialog.accept(self)
		
