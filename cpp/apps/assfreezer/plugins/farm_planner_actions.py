

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from blur.Assfreezer import *
import sip

class ProjectWeightScheduleAction( QAction ):
	def __init__(self, parent, parentWindow):
		QAction.__init__(self,"Project Weight Scheduler...",parent)
		self.ParentWindow = parentWindow
		self.triggered.connect( self.showProjectWeightScheduleDialog )
		
	def showProjectWeightScheduleDialog(self):
		from farm_planner.mainwindow import FarmPlannerWindow
		fpw = FarmPlannerWindow()
		sip.transferto(fpw,None)
		fpw.show()

class FarmPlannerMenuPlugin( AssfreezerMenuPlugin ):
	def __init__(self):
		AssfreezerMenuPlugin.__init__(self)
		
	def executeMenuPlugin(self, menu):
		# Ensure we only add one action, this could be called more than once
		for action in menu.actions():
			if isinstance(action,ProjectWeightScheduleAction):
				return
		self.Action = ProjectWeightScheduleAction(menu,menu.parent().window())
		menu.addAction( self.Action )

plugin = FarmPlannerMenuPlugin()
AssfreezerMenuFactory.instance().registerMenuPlugin( plugin, "Assfreezer_Tools_Menu" )

