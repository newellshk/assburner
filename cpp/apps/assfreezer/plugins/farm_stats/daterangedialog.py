
import os
from PyQt4.QtCore import QDateTime
from PyQt4.QtGui import QDialog
from PyQt4.uic import loadUi


class DateRangeDialog( QDialog ):
	def __init__(self,parent=None):
		QDialog.__init__(self,parent)
		loadUi(os.path.join(os.path.dirname(__file__),"daterangedialog.ui"),self)
		dt = QDateTime.currentDateTime()
		self.mStart.setDateTime( dt.addDays(-7) )
		self.mEnd.setDateTime( dt )
		
	def accept(self):
		QDialog.accept(self)
		self.StartDateTime = self.mStart.dateTime()
		self.EndDateTime = self.mEnd.dateTime()
