
import os
from blur.Stone import *
from blur.Stonegui import *
from PyQt4.QtCore import *
from PyQt4.QtGui import QBrush
from blur.defaultdict import *

from daterangedialog import *

class IntervalHoursModel(RecordListModel):
	def __init__(self,parent = None):
		RecordListModel.__init__(self,parent)
	
	def recordData( self, record, role, col ):
		if record.getValue(col).typeName() == 'Interval':
			if role == Qt.DisplayRole:
				interval = record.getValue(col).toPyObject()
				#if interval > Interval.fromString( '10 hours' )[0]:
				#	return QVariant(interval.asOrder(Interval.Hours))
				return QVariant(interval.toString(Interval.Hours))
			if role == Qt.TextAlignmentRole:
				return Qt.AlignRight
		return RecordListModel.recordData(self,record,role,col)

class FarmReport:
	def __init__(self,name):
		self.name = name
		self.records = RecordList()
		self.columns = []
		self.headerLabels = []
		self.model = None
		
	def generate(self):
		pass

	def createModel( self, parent = None ):
		ret = IntervalHoursModel(parent)
		self.setupModel(ret)
		return ret
	
	def setupModel( self, model ):
		model.setRootList( self.records )
		def getColumnVal( c, pos ):
			if isinstance(c, tuple):
				return c[pos]
			return c
		model.setColumns( map( lambda x: getColumnVal(x,0), self.Columns ) )
		model.setHeaderLabels( map( lambda x: getColumnVal(x,1), self.Columns ) )
	
user_slave_summary_table = None
user_render_summary_table = None
project_per_pixel_summary_table = None
user_job_count_table = None
hosthistory_status_percentages = None

def loadTables():
	global user_slave_summary_table
	global user_render_summary_table
	global user_job_count_table
	global project_per_pixel_summary_table
	global hosthistory_status_percentages
	global shotstatus_render_table
	
	# Create the table for this function
	if not user_slave_summary_table:
		db = Database.current()
		db.schema().mergeXmlSchema( os.path.join( os.path.dirname(__file__), 'stats_schema.xml' ) )
		tbn = db.tableByName
		user_slave_summary_table = tbn( 'user_slave_summary' )
		user_render_summary_table = tbn( 'user_render_summary' )
		user_job_count_table = tbn( 'user_job_counts' )
		project_per_pixel_summary_table = tbn( 'project_per_pixel_summary' )
		hosthistory_status_percentages = tbn( 'hosthistory_status_percentages' )
		
class UserHostSlaveReportModel(RecordListModel):
	def __init__(self,parent = None):
		RecordListModel.__init__(self,parent)
		
	def compare( self, r1, c1, r2, c2 ):
		if c1.toLower() == 'hours':
			return qVariantCmp(r1.getValue(c1), r2.getValue(c2))
		return RecordListModel.compare(self,r1,c1,r2,c2)

class UserHostSlaveReport(FarmReport):
	def __init__(self):
		FarmReport.__init__(self,'User Host Slave Summary')
		self.Columns = ['User', 'Host', 'Hours']
		
	def generate(self):
		FarmReport.generate(self)
		loadTables()
		drd = DateRangeDialog()
		if drd.exec_() != QDialog.Accepted:
			return
		self.records = Database.current().exec_( "SELECT * FROM hosthistory_user_slave_summary( ?, ? );", user_slave_summary_table, [QVariant(drd.StartDateTime),QVariant(drd.EndDateTime)] )

	def createModel( self, parent = None ):
		ret = UserHostSlaveReportModel(parent)
		self.setupModel(ret)
		return ret

			
class UserRenderReport(FarmReport):
	def __init__(self):
		FarmReport.__init__(self,'User Render Summary')
		self.Columns = ['User', ('TotalRenderTime','Total Render Time'), ('TotalErrorTime','Total Error Time'), ('ErrorTimePercent','Error Time Percent')]
		
	def generate(self):
		FarmReport.generate(self)
		loadTables()
		drd = DateRangeDialog()
		if drd.exec_() != QDialog.Accepted:
			return
		self.records = Database.current().connection().executeQuery( "select usr.name, sum(coalesce(totaltasktime,'0'::interval)) as totalrendertime, sum(coalesce(totalerrortime,'0'::interval)) as totalerrortime, sum(coalesce(totalerrortime,'0'::interval))/sum(coalesce(totaltasktime,'0'::interval)+coalesce(totalerrortime,'0'::interval)) as errortimeperc from jobstat, usr where started > ? and ended is not null and ended < ? AND fkeyusr=keyelement group by usr.name order by errortimeperc desc;", user_render_summary_table, [QVariant(drd.StartDateTime),QVariant(drd.EndDateTime)] )
	
class UserJobCountReport(FarmReport):
	def __init__(self):
		FarmReport.__init__(self,'User Job Counts')
		self.Columns = ['User','Ready','Started','Done','Suspended','Holding']
		
	def generate(self):
		FarmReport.generate(self)
		loadTables()
		per_user = {}
		stats = Database.current().exec_("select name, job.status, count(*) from job, usr where fkeyusr=keyelement and job.status IN ('ready','started','done','suspended','holding') group by keyelement, job.status, name order by job.status, count desc;")
		while stats.next():
			user = stats.value(0).toString()
			if not user in per_user:
				per_user[user] = Record( user_job_count_table )
				per_user[user].setValue( "user", QVariant(user) )
			per_user[user].setValue( stats.value(1).toString(), QVariant(stats.value(2).toInt()[0]) )
		for rec in per_user.values():
			self.records.append( rec )

class ProjectPerPixelReport(FarmReport):
	def __init__(self):
		FarmReport.__init__(self,'Project Per-Pixel Report')
		self.Columns = [
			('project', 'Project'),
			('job_count', 'Job Count'),
			('frame_count', 'Frame Count'),
			('render_time', 'Render Hours'),
			('avg_job_time', 'Average Job Hours' ),
			('run_length', 'Run Length'),
			('resolution', 'Resolution'),
			('render_time_per_frame', 'Render Hours Per Finished Frame'),
			('render_time_per_pixel', 'Render Seconds Per Finished Pixel'),
			('rendered_frames_per_output_frame', 'Rendered Frames Per Output Frame'),]
		
	def generate(self):
		FarmReport.generate(self)
		loadTables()
		self.records = Database.current().connection().executeQuery( """
	SELECT 	project.name as project_name,
			interval_normalize(sum(totaltasktime + totalloadtime + totalerrortime + totalcopytime)) as total_time,
			pr.runLength,
			pr.width::text || 'x' || pr.height::text as resolution,
			interval_normalize(sum(totaltasktime + totalloadtime + totalerrortime + totalcopytime) / (interval_to_seconds(pr.runLength)::float8 * pr.fps)) as time_per_frame,
			sum(totaltasktime + totalloadtime + totalerrortime + totalcopytime) / (interval_to_seconds(pr.runLength)::float8 * pr.fps * pr.width * pr.height) as time_per_pixel,
			sum(taskscompleted) / (interval_to_seconds(pr.runLength) * pr.fps) as frames_per_output_frame,
			count(*) as total_jobs,
			sum(totaltasktime + totalloadtime + totalerrortime + totalcopytime) / count(*) as avg_job_time,
			sum(taskscompleted) as total_frames
			from jobstat inner join project on jobstat.fkeyproject=project.keyelement INNER JOIN projectresolution as pr ON pr.fkeyproject=project.keyelement AND pr.primaryoutput=true AND coalesce(pr.runlength,'0'::interval) > '0'::interval WHERE not jobstat.name ~'PointCache' and pr.runlength is not null AND pr.fps > 0 AND project.created > now() - '3 years'::interval
			GROUP BY project.name, pr.fps, pr.width, pr.height, pr.runlength
		""",
		project_per_pixel_summary_table )

class RenderfarmSummaryReport(FarmReport):
	def __init__(self):
		FarmReport.__init__(self,'Renderfarm Summary Report')
	
class StatusDurationPercentagesModel(RecordListModel):
	def __init__(self,parent = None):
		RecordListModel.__init__(self,parent)
	
	def getColor( self, record ):
		if record.getValue("status").toString() == 'busy':
			if record.getValue("wasted").toBool() == False:
				return QBrush(Qt.green)
			return QBrush(Qt.red)
		return QVariant()
		
	def recordData( self, record, role, col ):
		if role == Qt.DisplayRole and col.endsWith("perc"):
			val = record.getValue(col)
			if not val.isNull():
				return QVariant( '%0.3f %%' % (val.toDouble()[0] * 100.0) )
		if role == Qt.BackgroundRole:
			return QVariant(self.getColor(record))
		return RecordListModel.recordData(self,record,role,col)

	def compare( self, r1, c1, r2, c2 ):
		if c1.endsWith("perc") and c2.endsWith("perc"):
			return qVariantCmp(r1.getValue(c1), r2.getValue(c2))
		return RecordListModel.compare(self,r1,c1,r2,c2)

class StatusDurationPercentages(FarmReport):
	def __init__(self):
		FarmReport.__init__(self,'Renderfarm Status Duration and Percentages')
		self.Columns = [
			'status',
			'loading',
			'wasted',
			('total_time','Time'),
			('total_perc','Time %'),
			('non_idle_perc','Non-Idle %'),
			('non_idle_non_wasted_perc','Non-Idle, Non-Wasted %') ]
	
	def generate(self):
		FarmReport.generate(self)
		loadTables()
		drd = DateRangeDialog()
		if drd.exec_() != QDialog.Accepted:
			return
		self.records = Database.current().exec_( "select * from hosthistory_status_percentages_duration_adjusted( ?, ? )", hosthistory_status_percentages, [QVariant(drd.StartDateTime),QVariant(drd.EndDateTime)] )
		
	def createModel( self, parent = None ):
		ret = StatusDurationPercentagesModel(parent)
		self.setupModel(ret)
		return ret

class ShotStatusRenderReport(FarmReport):
	SQL = \
r"""
select iq2.scene, iq2.shot, iq2.date_due, iq2.fullname, iq2.framecount, iq2.status, interval_normalize(iq1.totaltime) as "totalTime", iq3.job_count, iq1.totaltime / (iq2.framecount * .035 * 1600 * 681) / '1 second'::interval as percent_done from (

	select substring(outputpath from $$\w:\\Groundhog\\(?:Render|Comp|Cache_FumeFX)\\(Sc\w+)$$) as scene, substring(outputpath from $$\w:\\Groundhog\\Render\\Sc\w+\\(S[\d.]+)$$) as shot, sum(coalesce(totaltasktime,'0'::interval)+coalesce(totalloadtime,'0'::interval)+coalesce(totalerrortime,'0'::interval)) as totaltime from jobstat where fkeyproject in (select keyelement from project where name='Groundhog') group by scene, shot

) as iq1 right join (

	select s.name as shot, sc.name as scene, s.order as wave, s.rangeend - s.rangestart as framecount, ds.name as status, d.datecomplete as date_due, e.namefirst || ' ' || e.namelast as fullname from shot s inner join element sc on s.fkeyelement=sc.keyelement inner join delivery d on s.keyelement=d.fkeyelement and d.name='Lighting' inner join deliverystatus ds on ds.keydeliverystatus=d.fkeydeliverystatus left join employee e on d.fkeyusr_assignedto=e.keyelement where s.fkeyproject=(select keyelement from project where name='Groundhog') and s.enabled=true

) as iq2 on iq1.scene=iq2.scene and iq1.shot=iq2.shot left join (
	select count(*) as job_count, substring(outputpath from $$\w:\\Groundhog\\(?:Render|Comp|Cache_FumeFX)\\(Sc\w+)$$) as scene, substring(outputpath from $$\w:\\Groundhog\\Render\\Sc\w+\\(S[\d.]+)$$) as shot
	from job where status in ('new','ready','started') and fkeyproject=(select keyelement from project where name='Groundhog') group by scene, shot
) as iq3 on iq1.scene=iq3.scene and iq1.shot=iq3.shot
order by iq2.status, iq2.date_due, iq2.scene, iq2.shot;

"""
	def __init__(self):
		FarmReport.__init__(self,'Shot Status Render Report')
	
	def generate(self):
		self.records = Database.current().connection().executeQuery( self.SQL )
		
	def setupModel(self,model):
		if self.records:
			self.Columns = { str(f.name()) :str( f.displayName()) for f in self.records[0].table().schema().fields() }
		else:
			self.Columns = {}
		FarmReport.setupModel(self,model)
	
class FarmReportType:
	def __init__(self, reportName, reportClass):
		self.Name = reportName
		self.Class = reportClass

Types = [
	FarmReportType("User's Host Slave Report",UserHostSlaveReport),
	FarmReportType("User Render Report",UserRenderReport),
	FarmReportType("User Job Count Report", UserJobCountReport),
	FarmReportType("Project Per-Pixel Report",ProjectPerPixelReport),
	FarmReportType('Renderfarm Status Duration and Percentages',StatusDurationPercentages),
	FarmReportType('Shot Status Render Report', ShotStatusRenderReport) ]

