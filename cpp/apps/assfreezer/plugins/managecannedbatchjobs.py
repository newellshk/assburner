
import sys

from PyQt4.QtCore import Qt
from PyQt4.QtGui import *
from PyQt4.uic import loadUi

from blur.Stone import *
from blur.Classes import JobCannedBatch, JobCannedBatchList
from blur.Stonegui import *
try:
	from blur.Assfreezer import *
except:
	pass

import os

menuEntryText = 'Manage Canned Batch Jobs...'


class CannedBatchTranslator(RecordDataTranslator):
	def __init__(self,treeBuilder):
		RecordDataTranslator.__init__(self,treeBuilder)
		self.setRecordColumnList( ['group'] )
		#self.setColumnEditable( 0, True )
		
	def recordData(self,can,index,role):
		if role == Qt.DisplayRole and index.column() == 0:
			if can.isRecord():
				return can.name()
			return can.group()
		return RecordDataTranslator.recordData(self,can,index,role)

	def recordFlags(self,can,index):
		flags = RecordDataTranslator.recordFlags(self,can,index)
		return flags

class CannedBatchManagerModel( RecordSuperModel ):
	def __init__(self,parent=None):
		RecordSuperModel.__init__(self,parent)
		self.Trans = CannedBatchTranslator(self.treeBuilder())
		self.setHeaderLabels( ['Group / Name'] )
		canned = JobCannedBatch.select()
		cannedGroups = JobCannedBatchList()
		byGroup = canned.groupedBy( 'group' )
		for group in byGroup.keys():
			can = JobCannedBatch()
			can.setGroup(group)
			cannedGroups.append(can)
		self.setRootList( cannedGroups )
		self.setSortOrder( Qt.AscendingOrder )
		self.setAutoSort(True)
		for cannedGroup in cannedGroups:
			self.append( byGroup[cannedGroup.group()], self.findIndex( cannedGroup ) )
		
		tbl = JobCannedBatch.table()
		tbl.added.connect( self.cansAdded )
		tbl.removed.connect( self.cansRemoved )
		tbl.updated.connect( self.canUpdated )
		
	def canUpdated(self,can,old):
		# Signals don't automatically convert them from Record to JobCannedBatch
		can = sipWrapRecord(can)
		old = sipWrapRecord(old)
		if can.group() != old.group():
			self.removeCan(can)
			self.insertCan(can)
		else:
			self.update(can)
		
	def cansRemoved(self,cans):
		for can in cans:
			self.removeCan(can)
	
	def removeCan(self,can):
		idx = self.findIndex(can)
		parent = idx.parent()
		self.remove( idx )
		if self.rowCount(parent) == 0:
			self.remove(parent)
	
	def findGroupIndex(self,group,createIfMissing=True):
		mi = ModelIter(self)
		while mi.isValid():
			if( mi.current().data( Qt.DisplayRole ).toString() == group ):
				return mi.current()
			mi.next()
		if createIfMissing:
			return self.addGroup(group)
		return QModelIndex()
	
	def addGroup(self,group):
		can = JobCannedBatch()
		can.setGroup(group)
		return self.append( can )

	def insertCan(self,can):
		self.append( can, self.findGroupIndex(can.group()) )
	
	def cansAdded(self,cans):
		for can in cans:
			self.insertCan(can)
		
class CannedBatchManagerDialog( QDialog ):
	def __init__(self,parent=None):
		QDialog.__init__(self,parent)
		loadUi(os.path.join(os.path.dirname(__file__),"cannedbatchmanagerdialogui.ui"),self)
		self.CannedBatchManagerModel = CannedBatchManagerModel(self)
		self.mCannedBatchTree.setModel(self.CannedBatchManagerModel)
		self.mCannedBatchTree.setColumnAutoResize( 0, True )
		self.mCannedBatchTree.setRootIsDecorated( True )
		self.mCannedBatchTree.selectionChanged.connect( self.refreshDetails )
		self.mCannedBatchTree.showMenu.connect( self.showMenu )
		self.mApplyButton.clicked.connect( self.applyDetails )
		self.mResetButton.clicked.connect( self.refreshDetails )
		self.mCommandEdit.textEdited.connect( self.setChangesTrue )
		self.mNameEdit.textEdited.connect( self.setChangesTrue )
		self.mGroupCombo.editTextChanged.connect( self.setChangesTrue )
		
		self.refreshGroupCombo()
		self.refreshDetails()
		
	def refreshGroupCombo(self):
		self.mGroupCombo.clear()
		mi = ModelIter(self.mCannedBatchTree.model())
		while mi.isValid():
			self.mGroupCombo.addItem( mi.current().data( Qt.DisplayRole ).toString() )
			mi.next()
			
	def refreshDetails(self):
		sel = self.mCannedBatchTree.selection()
		self.mDetailsGroup.setEnabled( bool(sel.size()) )
		isMul = sel.size() > 1
		self.mNameEdit.setEnabled( not isMul )
		self.mCommandEdit.setEnabled( not isMul )
		if isMul or sel.isEmpty():
			self.mNameEdit.setText('')
			self.mCommandEdit.setText('')
		else:
			can = sel[0]
			self.mNameEdit.setText( can.name() )
			self.mCommandEdit.setText( can.cmd() )
			
		if sel.isEmpty() or len(sel.groupedBy("group").keys()) != 1:
			self.mGroupCombo.setEditText( '' )
		else:
			can = sel[0]
			self.mGroupCombo.setEditText( can.group() )
		self.setChanges( False )
		
	def applyDetails(self):
		sel = self.mCannedBatchTree.selection()
		if sel.isEmpty(): return
		if sel.size() == 1:
			can = sel[0]
			if not can.isRecord():
				# Dummy record for the group
				# Get index for the dummy
				mi = self.CannedBatchManagerModel.findIndex( can, False )
				if mi.isValid():
					# Collect the records of the child indexes
					cans = self.CannedBatchManagerModel.getRecords( ModelIter.collect( mi.child(0,0) ) )
					cans.setGroups( self.mGroupCombo.currentText() )
					cans.commit()
			else:
				# Changing a single normal item
				can.setCmd( self.mCommandEdit.text() )
				can.setName( self.mNameEdit.text() )
				can.setGroup( self.mGroupCombo.currentText() )
				can.commit()
		else:
			sel.setGroup( self.mGroupCombo.currentText() )
			sel.commit()
		self.setChanges( False )

	def setChanges(self,changes):
		self.mApplyButton.setEnabled( changes )
		self.mResetButton.setEnabled( changes )

	def setChangesTrue(self):
		self.setChanges(True)
	
	def showMenu(self,pos):
		sel = self.mCannedBatchTree.selection()
		if sel.isEmpty(): return
		menu = QMenu(self)
		rem = menu.addAction( "Remove" )
		res = menu.exec_(pos)
		if rem == res:
			toRemove = sel
			for can in sel:
				if not can.isRecord():
					mi = self.CannedBatchManagerModel.findIndex( can, False )
					if mi.isValid():
						toRemove += self.CannedBatchManagerModel.getRecords( ModelIter.collect( mi.child(0,0) ) )
			toRemove.remove()
		
class JobCannedBatchMenuPlugin( AssfreezerMenuPlugin ):
	def executeMenuPlugin(self, menu):
		for action in menu.actions():
			if action.text() == menuEntryText:
				return
		menu.addAction( menuEntryText, self.manageCannedBatchJobs )
		self.Parent = menu.parent()
		
	def manageCannedBatchJobs(self):
		cbm = CannedBatchManagerDialog(self.Parent)
		cbm.show()

try:
	AssfreezerMenuFactory.instance().registerMenuPlugin( JobCannedBatchMenuPlugin(), "Assfreezer_Tools_Menu" )
except: pass

if __name__=="__main__":
	app = QApplication(sys.argv)
	import blur.quickinit
	cbm = CannedBatchManagerDialog()
	cbm.show()
	QApplication.instance().exec_()
	
