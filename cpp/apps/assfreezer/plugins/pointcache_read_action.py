
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from blur.Classes import *
from blur.Assfreezer import *

class PCRAction( QAction ):
	def __init__(self, parent):
		QAction.__init__(self,"Explore PointCache Read Folder",parent)
		self.Menu = parent
		self.triggered.connect( self.slotTriggered )
		
	def slotTriggered(self):
		jobs = self.Menu.mJobList.jobTree().selection()
		for job in jobs:
			wipDir = QFileInfo(job.fileOriginal()).dir()
			print wipDir.path()
			if wipDir.cd( "../point_cache/read" ):
				exploreFile( wipDir.path() )
			else:
				wipDir.cdUp()
				QMessageBox.critical( self.Menu, 'Unable to find point cache read directory', 'PointCache read directory not found at ' + wipDir.path() + '\\point_cache\\read\\' )

class PCRMenuPlugin( AssfreezerMenuPlugin ):
	def executeMenuPlugin(self, menu):
		# Ensure we only add one action, this could be called more than once
		pcpService = Service.recordByName( 'PCPreview' )
		jobs = menu.mJobList.jobTree().selection()
		if jobs.isEmpty():
			return
		for job in jobs:
			if not job.jobServices().services().contains( pcpService ):
				return
		for action in menu.actions():
			if isinstance(action,PCRAction):
				return
		menu.addAction( PCRAction(menu) )

AssfreezerMenuFactory.instance().registerMenuPlugin( PCRMenuPlugin(), "AssfreezerJobMenu" )

