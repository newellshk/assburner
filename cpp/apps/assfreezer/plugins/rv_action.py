import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from blur.Assfreezer import *
from blur.Stone import makeFramePath

class RVAction( QAction ):
	def __init__(self, jobMenu):
		QAction.__init__(self, 'Open in RV', jobMenu)
		
		self.jobList = jobMenu.mJobList
		self.triggered.connect( self.openRV )
		
	def openRV(self):
		job = self.jobList.currentJob()
		cur = self.jobList.frameTree().current()
		if cur.isRecord():
			output = cur.jobOutput()
			path = output.fileTracker().filePath() if output.isRecord() else job.outputPath()
		else:
			path = job.outputPath()
		frameNumber = cur.frameNumber() if cur.isRecord() else job.minTaskNumber()
		path = makeFramePath(path, frameNumber, 4, not job.jobType().name().startsWith("Max") );
		# TODO: replace the frameNumber with # so rv opens the file sequence.
		# r'/blur/u/FSB/Comp/Sc001/S0040.00/comp/SC001_S0040_comp_v029/exr/SC001_S0040_comp_v029.#.exr'
		if sys.platform == 'win32':
			from _winreg import ConnectRegistry, OpenKey, HKEY_CLASSES_ROOT, QueryValueEx
			aReg = ConnectRegistry(None, HKEY_CLASSES_ROOT)
			aKey = OpenKey(aReg, r'rvlink\shell\open\command')
			value, typ = QueryValueEx(aKey, '')
			print value.replace('%1', path)
			QProcess.startDetached(value.replace('%1', path))
		else:
			QProcess.startDetached('rv',[path])
		
class RVMenuPlugin( AssfreezerMenuPlugin ):
	def executeMenuPlugin(self, menu):
		# Ensure we only add one action, this could be called more than once
		for action in menu.actions():
			if isinstance(action,RVAction):
				return
		self.appendAction(menu, 'Show Output in Explorer', RVAction(menu) )

plug = RVMenuPlugin()
AssfreezerMenuFactory.instance().registerMenuPlugin( plug, "AssfreezerJobMenu" )
AssfreezerMenuFactory.instance().registerMenuPlugin( plug, "AssfreezerTaskMenu" )
