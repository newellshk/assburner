import os
import re

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from blur.Classes import *
from blur.Assfreezer import *

class VersionCreatorAction( QAction ):
	def __init__(self, parent):
		QAction.__init__(self, "Create Versions for Jobs...", parent)
		self.Menu = parent
		self.triggered.connect(self.slotTriggered)

	def _buildNukeVersion(self, element, firstFrame, job):
		
		kwargs = {}
		kwargs['Path'] = self.buildVersionPath(firstFrame)
		
		if isinstance(element, Shot):

			# TODO: These are ballsy assumptions. This information should be on the job.
			kwargs['Department'] = 'Compositing'
			kwargs['Type'] = 'Composite'

		elif isinstance(element, Asset):

			# TODO: These are ballsy assumptions. This information should be on the job.
			kwargs['Department'] = 'FX'
			kwargs['Type'] = 'Sample'

		# Retrieve the source from the job environment.
		env = str(job.environment())
		match = re.search("NUKE_SOURCE_FILE=(.+)", env)
		if match:
			kwargs['Source'] = match.groups()[0]

		return kwargs


	def buildVersionPath(self, firstFrame):
		""" Builds the version path based on single/sequence."""
		import blurdev.media
		path = ""
		sequence = blurdev.media.imageSequenceFromFileName(firstFrame)
		# If we're dealing with a single file, we'll need to manually format it as a sequence Path/Sequence[0-0].abc
		# instead of accepting the filename without sequence formatting (as imageSequenceRepr returns.)
		path = blurdev.media.imageSequenceRepr(sequence, forceRepr=True)
		return path

	def getVFBFirstFrame(self, fileTracker, frame):
		"""If the VRay frame buffer is used, then a dot is automatically
		appended.  Account for this and generate a path for this.
		"""
		from blur.Stone import makeFramePath
		vfbBaseName, vfbExt = os.path.splitext(
			str(fileTracker.fileNameTemplate())
		)
		vfbFileNameTemplate = vfbBaseName + "." + vfbExt
		firstFrame = unicode(
			fileTracker.path() + makeFramePath(
				vfbFileNameTemplate,
				frame
			)
		)
		return firstFrame

	def slotTriggered(self):
		jobs = self.Menu.mJobList.jobTree().selection()

		items = []
		failures = []
		for output in jobs.jobOutputs():
			job = output.job()
			element = job.element()
			user = job.user()
			jobFormat = dict(name=job.name(), key=job.key(), user=user.name())

			if not element.isRecord():
				failures.append('Element is not set for Job: {name}({key})'.format(**jobFormat))
				continue

			# TODO: pass this as part of the Submit process?
			# I am getting the department from the Employee record, which wont work for User records.
			if not isinstance(user, Employee):
				failures.append('{user} is not a Employee for Job: {name}({key})'.format(**jobFormat))
				continue


			# VRay adds a dot as a frame delimiter if the VRay Frame Buffer is
			# being used.  In order to handle this, check if the path exists,
			# If it doesn't, then attempt to recreate the path generation
			# with an added dot delimiter
			outputFileTracker = output.fileTracker()
			if not outputFileTracker.isRecord():
				failures.append('Job Output not set for Job: {name}({key})'.format(**jobFormat))
				continue
			minFrameNumber = job.minTaskNumber()
			firstFrame = unicode(Mapping.osPath(outputFileTracker.filePath(minFrameNumber)))
			if not os.path.exists(firstFrame):
				firstFrame = self.getVFBFirstFrame(
					outputFileTracker,
					minFrameNumber
				)
			kwargs = {}
			# For now, base this off the job type.  We will be able to use
			# the internal job dictionary to store this later.
			nukeJobType = JobType("Nuke")

			publishInfo = job.publishInfo()
			if 'Type' in publishInfo:
				kwargs = publishInfo
				# Don't pass the version value to the publish method.
				try:
					kwargs.pop('version')
				except KeyError:
					pass
				kwargs['Path'] = self.buildVersionPath(firstFrame)
			#--------------------------------------------------------------------------------
			# TODO: Remove this code once the submit tools set job.publishInfo.
			elif job.jobType() == nukeJobType:
				kwargs = self._buildNukeVersion(element, str(firstFrame), job)
			else:
				kwargs['Path'] = self.buildVersionPath(firstFrame)
				kwargs['Department'] = 'Scene Assembly'
				kwargs['Type'] = 'Pass'
				path = unicode(job.elementFile())
				kwargs['Variation'] = os.path.basename(os.path.splitext(os.path.basename(path))[0])
				# Remove the shot number and MultiShot from the variation if its set
				kwargs['Variation'] = re.sub(r'S\d{4}.\d{2}_MultiShot_', r'', kwargs['Variation'], count=1)
				# Remove the shot number from the variation if its set
				kwargs['Variation'] = re.sub(r'S\d{4}.\d{2}_', r'', kwargs['Variation'], count=1)
				kwargs['Source'] = job.fileOriginal()
			#--------------------------------------------------------------------------------

			kwargs['Job ID'] = str(job.key())
			items.append((element, kwargs))

		if failures:
			print 'failures', '*'*50
			print '\n'.join(failures)
			print 'End', '*'*55

			msg = 'Unable to publish these jobs.\n\n{}\n\n'.format('\n'.join(failures))
			QMessageBox.information(self.Menu.parent(), 'Unable to publish these jobs.', msg)

		if items:
			try:
				# Importing takes a while, give the user some indication that something is happening
				QApplication.setOverrideCursor(Qt.WaitCursor)
				parent = self.Menu.parent()

				# NOTE: I am intentionally importing here so the checks happen quickly
				# and the import delay is only hit if its needed.
				import blurdev

				# The VersionCreatorDialog creates a logger and is closed causing the logger to close,
				# this causes the python error detection code to error out. To prevent this we make
				# sure the python logger is parented to Assfreezer instead.
				blurdev.core.logger(parent)

				from trax.api import data
				from blur3d.pipe.cinematic.gui.dialog.versioncreatordialog import VersionCreatorDialog, VersionTreeWidgetItem
				def toTrax(element):
					# This seems to be the only way to convert a blur.Classes object fully into a
					# a trax.api.data object.
					return data.__dict__[unicode(element.elementType().name())](element.key())

				# recasting element to trax Element to ensure trax's isinstance checks work
				items = [VersionTreeWidgetItem(None, toTrax(element), **kwargs) for element, kwargs in items]
				blurdev.launch(VersionCreatorDialog, kwargs={'parent': parent, 'items': items})
				blurdev.tools.logUsage({'name': 'outputpublisher'})
			finally:
				QApplication.restoreOverrideCursor()

class VersionCreatorMenuPlugin( AssfreezerMenuPlugin ):
	def executeMenuPlugin(self, menu):
		# Ensure we only add one action, this could be called more than once
		for action in menu.actions():
			if isinstance(action,VersionCreatorAction):
				return
		menu.addAction(VersionCreatorAction(menu))

menuPlugin = VersionCreatorMenuPlugin()
AssfreezerMenuFactory.instance().registerMenuPlugin( menuPlugin, "AssfreezerJobMenu" )
