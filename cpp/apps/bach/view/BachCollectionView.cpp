//-------------------------------------------------------------------------------------------------
/*
 * BachCollectionView.cpp
 *
 *  Created on: Jun 18, 2009
 *      Author: david.morris
 */

#include "BachCollectionView.h"
#include <assert.h>

#include <qfile.h>
#include <qurl.h>
#include <qpixmap.h>
#include <qpainter.h>
#include <qdatetime.h>
#include <qheaderview.h>
#include <qtextdocument.h>
#include <qtextedit.h>
#include <qapplication.h>
#include <recorddrag.h>


#include "bachasset.h"
#include "bachbucket.h"
#include "bachbucketmap.h"
#include "bachbucketlist.h"
#include "utils.h"

//-------------------------------------------------------------------------------------------------
BachCollectionView::BachCollectionView( QWidget * parent )
:	RecordListView( parent )
{
	mCollectionsModel = new RecordSuperModel( this );
	new BachBucketTranslator( mCollectionsModel->treeBuilder() );
	mCollectionsModel->setHeaderLabels(QStringList() << "name");
	setModel( mCollectionsModel );
}

//-------------------------------------------------------------------------------------------------
void BachCollectionView::refresh()
{
	RecordList currentSelected = selection();
	BachBucketList bbl = BachBucket::select();
	model()->setRootList( bbl );
	setSelection( currentSelected );
}

//-------------------------------------------------------------------------------------------------
void BachCollectionView::dragEnterEvent( QDragEnterEvent * event )
{
	qWarning("BachCollectionView::dragEnterEvent procedure triggered");
    if (event->source() == this)
    {
        event->setDropAction(Qt::MoveAction);
        event->accept();
    }
    else
    {
        event->acceptProposedAction();
    }
}

//-------------------------------------------------------------------------------------------------
void BachCollectionView::dragLeaveEvent( QDragEnterEvent * /*event*/ )
{
	qWarning("BachCollectionView::dragLeaveEvent procedure triggered");
	mCurrentDropTarget = BachBucket();
}

//-------------------------------------------------------------------------------------------------
void BachCollectionView::dragMoveEvent( QDragMoveEvent * event )
{
    if (event->source() == this)
    {
		event->setDropAction(Qt::MoveAction);
		event->accept();
	}
	else
	{
		QModelIndex midx = indexAt( event->pos() );
		mCurrentDropTarget = mCollectionsModel->getRecord( midx );
		setSelection( RecordList( mCurrentDropTarget ) );
		event->acceptProposedAction();
	}
 }

//-------------------------------------------------------------------------------------------------
void BachCollectionView::dropEvent( QDropEvent * event )
{
    if (event->source() == this)
    {
        event->setDropAction(Qt::MoveAction);
        event->accept();
    }
    else if ( mCurrentDropTarget.isValid() )
    {
    	if ( !RecordDrag::canDecode( event->mimeData() ) )
    		return;

    	BachBucketMapList bbml = mCurrentDropTarget.bachBucketMaps();
		DBG( "Have:"+QString::number(bbml.size()) );

    	BachBucketMapList toAdd;

    	RecordList dropped;
    	RecordDrag::decode( event->mimeData(), &dropped );
    	for ( RecordIter it = dropped.begin() ; it != dropped.end() ; ++it )
    	{
    		BachAsset ba = *it;
    		BachBucketMap bbm;
    		bbm.setBachBucket( mCurrentDropTarget );
    		bbm.setBachAsset( ba );
    		DBG( "Dropped:"+ba.path() );

    		// TODO: make it all unique, somehow...
//    		BachBucketMapList fbbml = BachBucketMap::recordByBucketAndAssets( mCurrentDropTarget, ba );
//    		DBG( "Dropped:"+ba.path()+", and found: "+QString::number(fbbml.count()) );
//    		foreach ( BachBucketMap bbm1, fbbml )
//    		{
//        		DBG( "Path:"+bbm1.bachAsset().path()+":"+bbm1.bachBucket().name() );
//    		}
//    		if ( fbbml.count() == 0 )

    		BachBucketMap fbbm = BachBucketMap::recordByBucketAndAsset( mCurrentDropTarget, ba );
    		if ( !fbbm.isRecord() )
    			toAdd.append( bbm );
    	}

		DBG( "To Add:"+QString::number(toAdd.size()) );
    	toAdd.commit();
        event->acceptProposedAction();
        refresh();
    }
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void BachBucketItem::setup( const Record & r, const QModelIndex & /*i*/ )
{
	mBachBucket = r;
	mMappingsCount = mBachBucket.bachBucketMaps().count();
}

//-------------------------------------------------------------------------------------------------
QVariant BachBucketItem::modelData( const QModelIndex & /*i*/, int role ) const
{
	switch ( role )
	{
		case Qt::DisplayRole:
		{
			return mBachBucket.name()+" ("+QString::number( mMappingsCount )+")";
		} break;

		case Qt::SizeHintRole:
		{
			return QSize( 50, 25 );
		} break;

		default: return QVariant();
	}
}

//-------------------------------------------------------------------------------------------------
QString BachBucketItem::sortKey( const QModelIndex & i ) const
{
	return modelData(i,Qt::DisplayRole).toString().toUpper();
}

//-------------------------------------------------------------------------------------------------
int BachBucketItem::compare( const QModelIndex & a, const QModelIndex & b, int,  bool )
{
	QString ska = sortKey( a ), skb = BachBucketTranslator::data(b).sortKey( b );
	return ska == skb ? 0 : ( ska > skb ? 1 : -1 );
}

//-------------------------------------------------------------------------------------------------
Qt::ItemFlags BachBucketItem::modelFlags( const QModelIndex & i )
{
	int col = i.column();
	if( col == 1 || col == 2 )
	{
		return Qt::ItemFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable );
	}
	else
	{
		return Qt::ItemFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled );
	}
}

//-------------------------------------------------------------------------------------------------
Record BachBucketItem::getRecord()
{
	return mBachBucket;
}
