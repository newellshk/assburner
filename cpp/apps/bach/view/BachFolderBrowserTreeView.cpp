/*
 * BachFolderBrowserTreeView.cpp
 *
 *  Created on: Jun 24, 2009
 *      Author: david.morris
 */

#include "BachFolderBrowserTreeView.h"
#include <QDirModel>

using namespace Qt;

//-------------------------------------------------------------------------------------------------
BachFolderBrowserTreeView::BachFolderBrowserTreeView( QWidget * a_Parent )
:	QTreeView( a_Parent )
{

	m_Model = new QDirModel( QStringList(), QDir::Dirs|QDir::NoDotAndDotDot, QDir::Name, this );
	setModel( m_Model );
	setRootIndex( m_Model->index( "/drd/jobs" ) );

	QHeaderView * header = new QHeaderView( Qt::Horizontal );
	setHeader( header );

	setColumnWidth( 0, 250 );
	hideColumn( 1 ); // size
	hideColumn( 2 ); // type
	hideColumn( 3 ); // date

	connect( this, SIGNAL( collapsed(const QModelIndex &) ), SLOT(onCollapsed(const QModelIndex &)));
	connect( this, SIGNAL( expanded(const QModelIndex &) ), SLOT(onExpanded(const QModelIndex &)));

}

//-------------------------------------------------------------------------------------------------
void BachFolderBrowserTreeView::onCollapsed( const QModelIndex & /*index*/ )
{
	resizeColumnToContents( 0 );
}

//-------------------------------------------------------------------------------------------------
void BachFolderBrowserTreeView::onExpanded( const QModelIndex & /*index*/ )
{
	resizeColumnToContents( 0 );
}




