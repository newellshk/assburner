
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Bach.
 *
 * Bach is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Bach is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bach; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qaction.h>
#include <qcombobox.h>
#include <qimage.h>
#include <qlabel.h>
#include <qmessagebox.h>
#include <qmenu.h>
#include <qpushbutton.h>
#include <qprintdialog.h>
#include <qprinter.h>
#include <qregexp.h>
#include <qsplitter.h>
#include <qstatusbar.h>
#include <qtabbar.h>
#include <qtabwidget.h>
#include <qmenubar.h>
#include <qtoolbar.h>
#include <qurl.h>
#include <qfontdialog.h>
#include <qscrollbar.h>
#include <qclipboard.h>
#include <qprocess.h>
#include <qprogressdialog.h>
#include <qpixmapcache.h>
#include <qinputdialog.h>
#include <qfile.h>
#include <qsize.h>

#include "blurqt.h"
#include "database.h"
#include "path.h"
#include "iniconfig.h"
#include "process.h"

#include "bachasset.h"

#include "svnrev.h"
#include "bachmainwindow.h"
#include "bachitems.h"
#include "bachbucket.h"
#include "bachbucketmap.h"
#include "bachbucketmaplist.h"
#include "bachthumbnailloader.h"
#include "utils.h"

static QString sCacheRoot;
static QSize sTnSize;

BachMainWindow::BachMainWindow( QWidget * parent )
: QMainWindow( parent )
,mResults(0)
,mResultsShown(0)
,mMissing(0)
,mSearchInProgress(false)
,mSearchUpdateInProgress(false)
,mSearchCanceled(false)
{
	setupUi( this );
	setWindowIcon( QIcon("images/bachicon.jpg") );

	// Caption
	setWindowTitle( QString("Bach 1.1") + ", build "+ SVN_REVSTR );

	 // actions
	mFontAction = new QAction( "Change &Font...", this );
	connect( mFontAction, SIGNAL( triggered() ), SLOT(changeFont()) );

	mGenThumbsAction = new QAction( "Generate Thumbnails", this );
	connect( mGenThumbsAction, SIGNAL( triggered() ), SLOT(generateAllThumbs()) );

	mPopCacheAction = new QAction( "Populate Cache", this );
	connect( mPopCacheAction, SIGNAL( triggered() ), SLOT(populateCache()) );

	mPrintAction = new QAction( "&Print...", this );
	//connect( mPrintAction, SIGNAL( triggered() ), SLOT(print()) );
	mQuitAction = new QAction( "&Quit", this );
	connect( mQuitAction, SIGNAL( triggered() ), qApp, SLOT(quit()) );

	mOpenAction = new QAction( "Open", this );
	connect( mOpenAction, SIGNAL( triggered() ), SLOT(openFiles()) );
	mShowAction = new QAction( "Show folder", this );
	connect( mShowAction, SIGNAL( triggered() ), SLOT(showFiles()) );
	mCopyPathAction = new QAction( "Copy path", this );
	connect( mCopyPathAction, SIGNAL( triggered() ), SLOT(copyPaths()) );
	mCopyImageAction = new QAction( "Copy image", this );
	connect( mCopyImageAction, SIGNAL( triggered() ), SLOT(copyImage()) );

	mRemoveFromBucketAction = new QAction( "Remove from collection", this );
	connect( mRemoveFromBucketAction, SIGNAL( triggered() ), SLOT(removeFromBucket()) );

	void ();

	mEditAction = new QAction( "Edit Tags", this );
	connect( mEditAction, SIGNAL( triggered() ), SLOT(editTags()) );

	mDblClickToPlayAction = new QAction( "Double click to play movies", this );
	mDblClickToPlayAction->setCheckable(true);
	mDblClickToPlayAction->setChecked(true);

	mDblClickToEditAction = new QAction( "Double click to edit tags", this );
	mDblClickToEditAction->setCheckable(true);
	mDblClickToEditAction->setChecked(false);

	populateMenus();

	IniConfig & config = userConfig();
	config.pushSection("Bach_Display_Prefs");

 	qApp->setFont( config.readFont("ShotFont") );

	initThumbSlider();
	{
		mThumbnailModel = new RecordSuperModel( mThumbDetailsView );
		new BachAssetTranslator( mThumbnailModel->treeBuilder() );
		mThumbnailModel->setAutoSort(false);

		mThumbDetailsView->setModel( mThumbnailModel );
		WrapTextDelegate * wtd = new WrapTextDelegate;
		mThumbDetailsView->setItemDelegate( wtd );
		setupAssetTree( mThumbDetailsView );

		mThumbsView->setModel( mThumbnailModel );
		mThumbsView->setViewMode( QListView::IconMode );
		ThumbDelegate * thumbGate = new ThumbDelegate;
		mThumbsView->setItemDelegate( thumbGate );
		mThumbsView->setModelColumn(1);
	}
	connect( mSearchEdit, SIGNAL(returnPressed()), SLOT(doTagSearch()));
	connect( mSearchButton, SIGNAL(pressed()), SLOT(searchButtonPressed()));

	connect( mThumbDetailsView, SIGNAL( selectionChanged(RecordList) ), SLOT( selectionChanged() ) );
	connect( mThumbsView, SIGNAL( selectionChanged(RecordList) ), SLOT( selectionChanged() ) );
	connect( mCollectionView, SIGNAL( selectionChanged(RecordList) ), SLOT( collectionViewSelectionChanged() ) );

	connect( mThumbDetailsView, SIGNAL( doubleClicked(const QModelIndex &) ), SLOT(treeEditItem(const QModelIndex &)));
	connect( mThumbsView, SIGNAL( doubleClicked(const QModelIndex &) ), SLOT(treeEditItem(const QModelIndex &)));
	connect( mCollectionView, SIGNAL( doubleClicked(const QModelIndex &) ), SLOT(collectionViewDoubleClicked()));
	connect( mFoldersView, SIGNAL( doubleClicked(const QModelIndex &) ), SLOT(foldersViewDoubleClicked(const QModelIndex &)));

	connect( mRightTabWidget, SIGNAL( currentChanged(int) ), SLOT( tabChanged(int) ) );

	connect( mCollectionAddBtn,     SIGNAL( clicked(bool) ), SLOT( collectionAddBtnPressed(bool) ) );
	connect( mCollectionNewBtn,     SIGNAL( clicked(bool) ), SLOT( collectionNewBtnPressed(bool) ) );
	connect( mCollectionDelBtn,     SIGNAL( clicked(bool) ), SLOT( collectionDelBtnPressed(bool) ) );
	connect( mCollectionRefreshBtn, SIGNAL( clicked(bool) ), SLOT( collectionRefreshBtnPressed(bool) ) );

	mCollectionView->refresh();

	mSearchEdit->setFocus( Qt::OtherFocusReason );
	QPixmapCache::setCacheLimit( 1024 * 2000 ); // 2 GB

	sCacheRoot = _cacheRoot();

}

BachMainWindow::~BachMainWindow()
{
}

void BachMainWindow::closeEvent( QCloseEvent * ce )
{
	saveAssetTree(mThumbDetailsView);
	IniConfig & config = userConfig();
	config.pushSection("Bach_Display_Prefs");
	config.writeString( "FrameGeometry",
	  QString("%1,%2,%3,%4").arg( pos().x() ).arg( pos().y() ).arg( size().width() ).arg( size().height() )
	);
	config.writeFont( "ShotFont", qApp->font() );
	config.popSection();
	QMainWindow::closeEvent(ce);
}

void BachMainWindow::changeFont() {
	qApp->setFont( QFontDialog::getFont(0, mThumbDetailsView->font()) );
}

void BachMainWindow::populateMenus()
{
	QMenuBar * mb = menuBar();
	mb->clear();
	mFileMenu = mb->addMenu( "&File" );
	//mFileMenu->addAction( mPrintAction );
	mFileMenu->addAction( mGenThumbsAction );
	mFileMenu->addAction( mPopCacheAction );
	mFileMenu->addAction( mFontAction );
	mFileMenu->addAction( mQuitAction );

	mViewMenu = mb->addMenu( "&View" );
	mViewMenu->addAction( mDblClickToPlayAction );
	mViewMenu->addAction( mDblClickToEditAction );

	mTreeMenu = new QMenu;
	populateTreeMenu();
	connect( mThumbDetailsView, SIGNAL( customContextMenuRequested(const QPoint &) ), SLOT(showTreeMenu( const QPoint & )));
	connect( mThumbsView, SIGNAL( customContextMenuRequested(const QPoint &) ), SLOT(showTreeMenu( const QPoint & )));

	mSearchTimer = new QTimer(this);
	connect( mSearchTimer, SIGNAL( timeout() ), SLOT( updateSearchResults() ) );
}

void BachMainWindow::populateTreeMenu()
{
	mTreeMenu->clear();
 	mTreeMenu->addAction(mOpenAction);
 	mTreeMenu->addAction(mEditAction);
 	mTreeMenu->addAction(mShowAction);
 	mTreeMenu->addAction(mCopyPathAction);
 	mTreeMenu->addAction(mCopyImageAction);
 	mTreeMenu->addAction(mRemoveFromBucketAction);
}

void BachMainWindow::showTreeMenu( const QPoint & )
{
	QAction * result = mTreeMenu->exec(QCursor::pos());
	if( !result )
		return;
}

BachAssetList BachMainWindow::selection() const
{
	if( mRightTabWidget->currentIndex() == 0 )
		return mThumbDetailsView->selection();
	else if( mRightTabWidget->currentIndex() == 1 )
		return mThumbsView->selection();
	return BachAssetList();
}

void BachMainWindow::openFiles() const
{
	BachAssetList selected = selection();

	foreach( BachAsset ba, selected ) {
#ifdef Q_OS_LINUX
		QProcess::startDetached("Thunar", QStringList() << ba.path());
#endif
#ifdef Q_OS_MAC
		QProcess::startDetached("open", QStringList() << ba.path());
#endif
#ifdef Q_OS_WIN
		QString path = Path("T:"+ba.path()).path();
		openURL( path );
#endif
	}
}

void BachMainWindow::showFiles() const
{
	BachAssetList selected = selection();
	foreach( BachAsset ba, selected ) {
#ifdef Q_OS_LINUX
		QProcess::startDetached("Thunar", QStringList() << Path(ba.path()).dirPath());
#endif
#ifdef Q_OS_MAC
		QProcess::startDetached("open", QStringList() << Path(ba.path()).dirPath());
#endif
#ifdef Q_OS_WIN
		QString path = Path("T:"+ba.path()).dirPath();
		openExplorer( path );
#endif
	}
}

//-------------------------------------------------------------------------------------------------
void BachMainWindow::copyPaths()
{
	QStringList paths;
	BachAssetList selected = selection();
	foreach( BachAsset ba, selected ) {
#ifdef Q_OS_WIN
		QString path = Path("T:"+ba.path()).path();
		paths << path;
#else
		paths << ba.path();
#endif
	}
	QClipboard * cb = QApplication::clipboard();
	cb->setText( paths.join("\n") );
}

//-------------------------------------------------------------------------------------------------
void BachMainWindow::copyImage()
{
	QStringList paths;
	BachAssetList selected = selection();
	if( selected.size() == 0 )
		return;

#ifdef Q_OS_WIN
	QString path = Path("T:"+selected[0].path()).path();
	QImage img = ThumbnailLoader::loadFromDisk( path );
#else
	QImage img = ThumbnailLoader::loadFromDisk( selected[0].path() );
#endif
	QClipboard * cb = QApplication::clipboard();
	cb->setImage( img );
}


//-------------------------------------------------------------------------------------------------
void BachMainWindow::removeFromBucket()
{
	BachBucketList bbl = mCollectionView->selection();
	if ( bbl.count() == 0 )
		return;

	BachAssetList bal = selection();
	if( bal.count() == 0 )
		return;

	int removed = 0;
	foreach( BachAsset ba, bal )
	{
		BachBucketMap bbm = BachBucketMap::recordByBucketAndAsset( bbl[0], ba );
		removed += bbm.remove()==1?1:0;
	}

	DBG( "Removed from bucket: "+QString::number( removed ) );
	mCollectionView->refresh();
}

//-------------------------------------------------------------------------------------------------
void BachMainWindow::refreshView()
{
	int currentSlidePos = mThumbDetailsView->verticalScrollBar()->sliderPosition();

 //	mThumbDetailsView->refresh();

	mThumbDetailsView->verticalScrollBar()->setSliderPosition(currentSlidePos);
}

//-------------------------------------------------------------------------------------------------
void BachMainWindow::updateSearchResults()
{
	if( mSearchCanceled ) {
		searchComplete();
		return;
	}
	if( mSearchUpdateInProgress )
		return;
	mSearchUpdateInProgress = true;

	//QTime updateTime = QTime::currentTime();

	int toUpdate = qMin((uint)75, mFound.size()-mResultsShown);
	int results = 0;
	BachAssetList toShow;
	while( mUpdateIter != mFound.end() && results++ <= toUpdate ) {
		toShow += *mUpdateIter;
		++mUpdateIter;
	}
	mThumbnailModel->append( toShow );
	//LOG_3("msecs to update:"+QString::number(updateTime.msecsTo(QTime::currentTime())));

	mResultsShown += toUpdate;
	if( mUpdateIter == mFound.end() )
		searchComplete();
	mSearchUpdateInProgress = false;
}

//-------------------------------------------------------------------------------------------------
void BachMainWindow::searchButtonPressed()
{
	if( mSearchInProgress )
		cancelSearch();
	else
		doTagSearch();
}

//-------------------------------------------------------------------------------------------------
void BachMainWindow::doTagSearch()
{
	if ( !doPreSearch() )
		return;
	QString searchText = mSearchEdit->text();
	mFound = BachAsset::select("filetype=2 AND fti_tags @@ to_tsquery(?)", VarList() << searchText.replace(" ","&"));
	doPostSearch();
}

//-------------------------------------------------------------------------------------------------
void BachMainWindow::doBucketSearch( const BachBucket & bb )
{
	if ( !doPreSearch() )
		return;

	mFound = bb.bachBucketMaps().bachAssets();
	doPostSearch();
}

//-------------------------------------------------------------------------------------------------
void BachMainWindow::doDirectorySearch( const QString & a_Directory )
{
	if ( !doPreSearch() )
		return;

	mFound = BachAsset::select("directory=(?)", VarList() << a_Directory );
	doPostSearch();
}

//-------------------------------------------------------------------------------------------------
bool BachMainWindow::doPreSearch()
{
	if( mSearchInProgress )
		return false;

	mSearchInProgress = true;
	mSearchButton->setText("Cancel");
	statusBar()->showMessage("Searching... ");
	qApp->setOverrideCursor(QCursor(Qt::WaitCursor));
	mSearchTime = QTime::currentTime();

	return true;
}

//-------------------------------------------------------------------------------------------------
void BachMainWindow::doPostSearch()
{
	mUpdateIter = BachAssetIter(mFound);
	mResults = mFound.size();
	LOG_3("msecs to load from db:"+QString::number(mSearchTime.msecsTo(QTime::currentTime())));
	LOG_3("Searching... "+QString::number(mResults)+" records found");
	mSearchTime = QTime::currentTime();

	mResultsShown = 0;
	mThumbnailModel->updateRecords( BachAssetList() );
	statusBar()->showMessage("Searching... "+QString::number(mResults)+" records found");
	mSearchTimer->start(50);
}

//-------------------------------------------------------------------------------------------------
void BachMainWindow::cancelSearch()
{
	mSearchCanceled = true;
}

//-------------------------------------------------------------------------------------------------
void BachMainWindow::searchComplete()
{
	mSearchTimer->stop();
	qApp->restoreOverrideCursor();

	int msecs = mSearchTime.msecsTo(QTime::currentTime());
	LOG_3("msecs to update views:"+QString::number(msecs));
	statusBar()->showMessage("Showing "+QString::number(mResultsShown)+" out of "+QString::number(mResults)+" valid records found. Search took "+QString::number(msecs)+" msecs");
	mSearchButton->setText("Search");
	mSearchInProgress = false;
	mSearchCanceled = false;
}

//-------------------------------------------------------------------------------------------------
void BachMainWindow::selectionChanged()
{
	BachAssetList selected = selection();
	statusBar()->showMessage(QString::number(selected.size())+" of "+QString::number(mResults) +" records selected");

	DBG( "BachAssets" );
	for ( BachAssetIter it = selected.begin() ; it != selected.end() ; ++it )
	{
		const BachAsset & ba = *it;
		DBG( "\tBachAsset: " + ba.path() );
	}
}

//-------------------------------------------------------------------------------------------------
void BachMainWindow::collectionViewSelectionChanged()
{
	BachBucketList selected = mCollectionView->selection();
	DBG( "BachAssets" );
	for ( BachBucketIter it = selected.begin() ; it != selected.end() ; ++it )
	{
		const BachBucket & bb = *it;
		DBG( "\tBachAsset: " + bb.name() );
	}
}

//-------------------------------------------------------------------------------------------------
void BachMainWindow::collectionViewDoubleClicked()
{
	// borg borg borg
	BachBucketList bbl = mCollectionView->selection();
	if ( bbl.count() == 0 )
	{
		return;
	}
	BachBucket bb = *bbl.at( 0 );
	doBucketSearch( bb );
}

//-------------------------------------------------------------------------------------------------
void BachMainWindow::foldersViewDoubleClicked( const QModelIndex & a_Idx )
{
	QString filePath = mFoldersView->getDirModel()->filePath( a_Idx );
	if ( !filePath.endsWith( '/' ) )
		filePath.append( '/' );
	DBG( "Double clicked on: "+filePath );
	doDirectorySearch( filePath );
}

//-------------------------------------------------------------------------------------------------
void BachMainWindow::doubleClicked()
{

}

void BachMainWindow::treeEditItem(const QModelIndex & index)
{
	if( (index.column() == 2 && mDblClickToEditAction->isChecked()) ||
			(index.column() == 1 && mDblClickToPlayAction->isChecked()) ) {
	qWarning( "=== enable editing for row " + QString::number(index.row()).toAscii() + ", col " + QString::number(index.column()).toAscii() );
		mThumbDetailsView->setCurrentIndex(index);
		mThumbDetailsView->edit(index);
	} else
		openFiles();
}

void BachMainWindow::generateAllThumbs()
{
	// this will create thumbnails where needed, and populate all caches
	qWarning("generating thumbs");
	BachAssetList all = BachAsset::select("filetype=2");
	QProgressDialog * progress = new QProgressDialog("Generating thumbnails...", "Abort", 0, all.size());
	progress->setWindowModality(Qt::WindowModal);

	QSize tnSize(240,180);
	foreach( BachAsset ba, all ) {
		qApp->processEvents();
		if( progress->wasCanceled() )
			break;

		QString cachePath = ThumbnailLoader::thumbnailCachePath( sCacheRoot, ba.path(), tnSize );
		if( !QFile::exists(cachePath) )
			ThumbnailLoader::generate( ba.path(), cachePath, tnSize, false );
		progress->setValue(progress->value()+1);
	}
	progress->setValue(progress->maximum());
}

void BachMainWindow::populateCache()
{
	// this will create thumbnails where needed, and populate all caches
	qWarning("generating thumbs");
	BachAssetList all = BachAsset::select();
	QProgressDialog * progress = new QProgressDialog("Pre-Loading cache...", "Abort", 0, all.size());
	progress->setWindowModality(Qt::WindowModal);

	QSize tnSize(240,180);
	foreach( BachAsset ba, all ) {
		qApp->processEvents();
		if( progress->wasCanceled() )
			break;

		ThumbnailLoader::load( sCacheRoot, ba.path(), tnSize, false );
		progress->setValue(progress->value()+1);
	}
	progress->setValue(progress->maximum());
}

void BachMainWindow::tabChanged(int tab)
{
	if( tab == 1 )
		mThumbsView->setGridSize(mThumbsView->gridSize());
}

void BachMainWindow::editTags()
{
	BachAssetList selected = selection();
	QString tags;
	if( selected.size() > 0 )
		tags = selected[0].tags();

	bool ok;
	QString text = QInputDialog::getText(this, tr("Edit Tags"),
										tr("Tags:"),
										QLineEdit::Normal,
										tags,
										&ok);
	if (ok && !text.isEmpty()) {
		selected.setTags(text);
		selected.commit();
	}
}

QString BachMainWindow::cacheRoot()
{
	return sCacheRoot;
}

QString BachMainWindow::_cacheRoot()
{
#ifdef Q_OS_WIN
	QString temp( "T:/reference/.thumbnails/" );
#else
	QString temp( "/drd/reference/.thumbnails/" );
#endif // Q_OS_WIN

	QStringList environment =  QProcess::systemEnvironment();
	foreach( QString key, environment )
		if( key.startsWith("BACHCACHEPATH=") )
			temp = key.replace("BACHCACHEPATH=","");

	// Make sure the thumbnail cache directory exists
	if( !Path( temp ).mkdir() ) {
		LOG_3( "Couldn't create thumbnailcache directory: " + temp );
	}
	return temp;
}

//-------------------------------------------------------------------------------------------------
void BachMainWindow::collapseToSequence()
{
	BachAssetList selected = selection();
}

//-------------------------------------------------------------------------------------------------
void
BachMainWindow::collectionAddBtnPressed(bool)
{
	BachBucketList bbl = mCollectionView->selection();
	if ( bbl.count() == 0 )
	{
		return;
	}

	BachBucket bb = *bbl.at( 0 );
	BachAssetList selected = selection();
	BachBucketMapList bbml = bb.bachBucketMaps();

	BachBucketMapList toAdd;

	for ( RecordIter it = selected.begin() ; it != selected.end() ; ++it )
	{
		BachAsset ba = *it;
		DBG( "Adding:"+ba.path() );

		BachBucketMap bbm;
		bbm.setBachBucket( bb );
		bbm.setBachAsset( ba );
		toAdd.append( bbm );
	}

	bbml |= toAdd;
	bbml.commit();

	mCollectionView->refresh();
}

//-------------------------------------------------------------------------------------------------
void
BachMainWindow::collectionNewBtnPressed(bool)
{
	QString name;
	bool ok = false;
	QString text = QInputDialog::getText(this, tr("New Collection"),
										tr("Name:"),
										QLineEdit::Normal,
										name,
										&ok);
	if ( ok && !text.isEmpty() )
	{
		BachBucket bb;
		bb.setName( text );
		bb.commit();
	}
	mCollectionView->refresh();
}

//-------------------------------------------------------------------------------------------------
void
BachMainWindow::collectionDelBtnPressed(bool)
{
	BachBucketList bbl = mCollectionView->selection();
	if ( bbl.count() == 0 )
	{
		return;
	}

	BachBucket bb = bbl[ 0 ];

	QMessageBox msgBox;
	msgBox.setWindowTitle( "Delete bucket \""+bb.name()+"\"?" );
	msgBox.setText( "Area you sure you want to delete the selected bucket?" );
	msgBox.setInformativeText( "This will delete the collection and all the mappings, \nbut the images will remain in the pool.\n\n"
			"Note: this action can NOT be undone" );
	msgBox.setStandardButtons( QMessageBox::Ok | QMessageBox::Cancel );
	msgBox.setDefaultButton( QMessageBox::Ok );
	int ret = msgBox.exec();
	if ( ret == QMessageBox::Cancel )
		return;

	BachBucketMapList bbml = BachBucketMap::recordsByBachBucket( bb );
	foreach ( BachBucketMap bbm, bbml )
		bbm.remove();
	bb.remove();

	mCollectionView->refresh();
}

//-------------------------------------------------------------------------------------------------
void
BachMainWindow::collectionRefreshBtnPressed(bool)
{
	mCollectionView->refresh();
	collectionViewDoubleClicked();
}

void BachMainWindow::initThumbSlider() {
	mThumbSlider = new QSlider(this);
	mThumbSlider->setRange(1,24);
	mThumbSlider->setSingleStep(1);
	mThumbSlider->setGeometry(0,0,200,20);
	mThumbSlider->setOrientation(Qt::Horizontal);
	statusBar()->addPermanentWidget(mThumbSlider);

	connect( mThumbSlider, SIGNAL(valueChanged(int)), SLOT(calculateTnSize(int)));
	mThumbSlider->setValue(8);
	//calculateTnSize( mThumbSlider->value() );
}

void BachMainWindow::calculateTnSize(int imagesToShow)
{
    // mThumbSlider holds the value of how many images to show horizontally
    //int imagesToShow = mThumbSlider->value();

    // widget of mRightTabWidget is amount of space we have in total
    int amountOfSpace = mRightTabWidget->width()-8;

    int pixelsPerImage = (int)((float)amountOfSpace / (float)imagesToShow) - 12;
    sTnSize = QSize(pixelsPerImage, pixelsPerImage);
    mThumbsView->setIconSize(sTnSize);
		//LOG_3("scroll by:" + QString::number(mThumbsView->horizontalScrollBar()->singleStep()));
		mThumbsView->horizontalScrollBar()->setSingleStep(5);
}

QSize BachMainWindow::tnSize()
{
    return sTnSize;
}

