
TARGET=classmaker

include($$(PRI_SHARED)/common.pri)
include($$(PRI_SHARED)/stone.pri)
include($$(PRI_SHARED)/stonegui.pri)

HEADERS += \
	mainwindow.h \
	sourcegen.h \
	tabledialog.h \
	fielddialog.h \
	indexdialog.h \
	createdatabasedialog.h \

SOURCES += \
	mainwindow.cpp \
	sourcegen.cpp \
	main.cpp \
	tabledialog.cpp \
	fielddialog.cpp \
	indexdialog.cpp \
	createdatabasedialog.cpp \

FORMS += \
	mainwindowui.ui \
	tabledialogui.ui \
	fielddialogui.ui \
	indexdialogui.ui \
	createdatabasedialogui.ui \
	docsdialogui.ui
	
TEMPLATE=app

CONFIG+=qt thread
QT+=sql xml gui network widgets

RC_FILE=classmaker.rc

INSTALLS += target
