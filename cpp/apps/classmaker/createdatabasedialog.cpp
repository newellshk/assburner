
#include <qpushbutton.h>
#include <qtablewidget.h>
#include <qlabel.h>
#include <qstringlist.h>
#include <qregexp.h>

#include "blurqt.h"
#include "createdatabasedialog.h"
#include "connection.h"
#include "database.h"
#include "freezercore.h"
#include "iniconfig.h"
#include "logindialog.h"

CreateDatabaseDialog::CreateDatabaseDialog( Schema * schema, QWidget * parent )
: QDialog( parent )
, mSchema( schema )
, mDatabase( new Database(schema) )
, mTableSchema( 0 )
{
	mUI.setupUi( this );

	connect( mUI.mVerifyButton, SIGNAL( clicked() ), SLOT( verify() ) );
	connect( mUI.mCreateButton, SIGNAL( clicked() ), SLOT( create() ) );
	connect( mUI.mCloseButton, SIGNAL( clicked() ), SLOT( reject() ) );
	connect( mUI.mEditConnectionButton, SIGNAL( clicked() ), SLOT( editConnection() ) );

	updateConnectionLabel();
}

void CreateDatabaseDialog::setTableSchema( TableSchema * tableSchema )
{
	mTableSchema = tableSchema;
}

void CreateDatabaseDialog::editConnection()
{
	Connection * c = mDatabase->connection();
	LoginDialog login(this,c);
	if( login.exec() == QDialog::Accepted ) {
		c->reconnect();
		updateConnectionLabel();
	}
}

void CreateDatabaseDialog::verify()
{
	QString output;
	try {
		if( mTableSchema ) {
			Table * table = mDatabase->tableFromSchema( mTableSchema );
			table->verifyTable( false, &output );
		} else {
			mDatabase->verifyTables( &output );
		}
	} catch (const SqlException & se) {
		output = se.error();
	}
	mUI.mHistoryEdit->setText( output );
}

void CreateDatabaseDialog::create()
{
	QString output;
	try {
		if( mTableSchema ) {
			Table * table = mDatabase->tableFromSchema( mTableSchema );
			if( table->exists() )
				table->verifyTable( true, &output );
			else
				table->createTable( &output );
		} else {
			mDatabase->createTables( &output );
		}
	} catch (const SqlException & se) {
		output = se.error();
	}
	mUI.mHistoryEdit->setText( output );
}

void CreateDatabaseDialog::updateConnectionLabel()
{
	Connection * c = mDatabase->connection();
	mUI.mConnectionStatus->setText(
		(c->checkConnection() ? "Connected: " : "Connection Failed: ") +
		c->databaseName() + " on " + c->userName() + ":" +
		c->password().replace( QRegExp("."), "x" ) + "@" + c->host() );
}
