
#include <qstring.h>

static const char * string_data = 
<%STRING_DATA%>;

static int string_count = <%STRING_COUNT%>;

static int string_offsets [<%STRING_COUNT%>] = {
<%STRING_OFFSETS%> };

static int string_lengths [<%STRING_COUNT%>] = {
<%STRING_LENGTHS%> };

QString * qstrings = 0;
static bool stringsBuilt = false;

void buildStrings()
{
	qstrings = new QString[string_count];
	for( int i=string_count-1; i >= 0; --i ) {
		qstrings[i] = QString::fromUtf8(&string_data[string_offsets[i]],string_lengths[i]);
	}
	stringsBuilt = true;
}

void freeStrings()
{
	delete [] qstrings;
	qstrings = 0;
	stringsBuilt = false;
}
