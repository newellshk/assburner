/*
 *
 * Copyright 2009 Dr.D Studios
 *
 * This file is part of the RenderLine software package.
 *
 * AB is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Assburner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Resin; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 
#include <qaction.h>
#include <qfileinfo.h>
#include <qtoolbar.h>
#include <qtreewidget.h>
#include <qmenu.h>
#include <qmessagebox.h>
#include <QCloseEvent>
#include <QXmlStreamReader>
#include <QProcess>
#include <QSettings>
#include <QInputDialog>

#include "mainwindow.h"

MainWindow::MainWindow(const QString & xmlRoot)
: QMainWindow( 0 )
,mRoot(xmlRoot)
,mCmdLine("")
,mCmdArgs()
{
	setupUi( this );

	setWindowIcon( QIcon( xmlRoot+"/logo-epa-color.png" ) );
	mTree->setContextMenuPolicy( Qt::CustomContextMenu );
	mTree->setIconSize(QSize(24,24));
	connect( mTree, SIGNAL( customContextMenuRequested( const QPoint & ) ), SLOT( showContextMenu( const QPoint & ) ) );
	connect( mTree, SIGNAL( itemDoubleClicked( QTreeWidgetItem *, int ) ), SLOT( slotDoubleClicked( QTreeWidgetItem *, int ) ) );

	connect( mProjectCombo, SIGNAL( currentIndexChanged( const QString & ) ), SLOT( populateDepartments( const QString & ) ) );
	connect( mDeptCombo, SIGNAL( currentIndexChanged( const QString & ) ), SLOT( populateSoftware( const QString & ) ) );
	// Load the job and project pulldowns
	loadEpaXml();
	//loadSettings();
}

void MainWindow::closeEvent(QCloseEvent * event)
{
  LOG("save settings");
	saveSettings();
	event->accept();
}

void MainWindow::loadSettings()
{
	QSettings settings( QSettings::IniFormat, QSettings::UserScope, "DRD", "EPA");

	settings.beginGroup("mainwindow");
	QSize size = settings.value("size", QSize(220,400)).toSize();
	resize(size);
	QPoint pos = settings.value("pos", QPoint(1780,800)).toPoint();
	move(pos);

	setProject( settings.value("project").toString() );
	setDept( settings.value("dept").toString() );
	settings.endGroup();
}

void MainWindow::saveSettings()
{
        QSettings settings( QSettings::IniFormat, QSettings::UserScope, "DRD", "EPA");

	settings.beginGroup("mainwindow");
	settings.setValue("size", this->size());
	settings.setValue("pos", this->pos());
	settings.setValue("project", mCurrentProject);
	settings.setValue("dept", mCurrentDept);
	settings.endGroup();
}

void MainWindow::showContextMenu( const QPoint & p)
{
	QTreeWidgetItem * currentItem = mTree->itemAt(p);
	//LOG( currentItem->text(0) );
	QMenu * m = new QMenu( this );
	if( mTree->indexOfTopLevelItem(currentItem) > -1 ) {
		m->addAction("Open");
		m->addAction("Command line");
		m->addAction("Add variable");
		m->addSeparator();
		m->addAction("Create Shortcut");
	} else if (currentItem->childCount() == 0) {
		m->addAction("Remove variable");
	} else {
		delete m;
		return;
	}
	QAction * res = m->exec( QCursor::pos() );
	if( !res ) {
		delete m;
		return;
	}

	if( res->text() == "Open" )
		launchSoftware(currentItem->text(0));
	if( res->text() == "Command line" )
		shellSoftware(currentItem->text(0));
	if( res->text() == "Add variable" ) {
		bool ok;
		QString text = QInputDialog::getText(this, tr("Add Variable name"),
				                                       tr("Variable name:"), QLineEdit::Normal,
																							"", &ok);
		if (ok && !text.isEmpty()) {
			QTreeWidgetItem * newItem = new QTreeWidgetItem( currentItem, QStringList() << text << "" );
			newItem->setData(0, Qt::UserRole, currentItem->data(0, Qt::UserRole));
			newItem->setIcon(0, QIcon(mRoot+"icons/dep.png"));
			newItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled);

		}
	}
	if( res->text() == "Remove variable" )
		delete currentItem;

	delete m;
}

void MainWindow::loadEpaXml()
{
	QFile xmlFile(mRoot+"/epa.xml");
	if (!xmlFile.open(QIODevice::ReadOnly | QIODevice::Text))
		return;

	QXmlStreamReader xml;
	xml.addData( xmlFile.readAll() );

	while( !xml.atEnd() ) {
		xml.readNext();
		if( xml.isStartElement() ) {
			if( xml.name() == "project" ) {
				QString projectName = xml.attributes().value("name").toString();
				mCurrentProject = projectName;
			}
			if( xml.name() == "department" ) {
				QString deptName = xml.attributes().value("name").toString();
				mCurrentDept = deptName;
			}
			if( xml.name() == "software" ) {
				QString name = xml.attributes().value("name").toString();
				QString version = xml.attributes().value("version").toString();
				mProjectData[mCurrentProject][mCurrentDept] += name; // + "__" + version;
			}
                }
	}
}

void MainWindow::populateProjects()
{
	mProjectCombo->clear();
	mProjectCombo->addItems( mProjectData.keys() );
}

void MainWindow::populateDepartments(const QString & project)
{
	mCurrentProject = project;
	mDeptCombo->clear();
	mDeptCombo->addItems( mProjectData[project].keys() );
}

void MainWindow::populateSoftware(const QString & dept)
{
	mCurrentDept = dept;
	mTree->clear();
	foreach( QString software, mProjectData[mCurrentProject][dept] ) {
		QTreeWidgetItem * softwareItem = new QTreeWidgetItem( QStringList() << software );
		mTree->addTopLevelItem(softwareItem);
		EpaLoader loader(mCurrentProject+"_"+mCurrentDept+"_"+software, true /* useSystemEnv */);
		loader.setRoot(mRoot);
		loader.setArch(mArch);
		loader.setSep(mSep);
		loader.setTreeWidgetItem( softwareItem );
		loader.mergeEnvironment(QStringList() << mCurrentProject << mCurrentDept << software);
		if( loader.environment().contains("EPA_ICON") ) {
			QIcon icon(loader.environment().value("EPA_ICON"));
			softwareItem->setIcon(0, icon);
		}

	}
	mTree->resizeColumnToContents(0);
	mTree->resizeColumnToContents(1);
}

QStringList MainWindow::buildEnvironment(const QString & env)
{
	QString presetLabel = mCurrentProject + "_" + mCurrentDept + "_" + env;
	LOG("buildEnvironment for: "+env);
	QMap<QString, QString> envMap;
	QList<QTreeWidgetItem *> items = mTree->findItems(env, Qt::MatchExactly);
	foreach( QTreeWidgetItem * item, items ) {
		if( item->text(0) == env && mTree->indexOfTopLevelItem(item) > -1 ) {
			// traverse from the top down
			QTreeWidgetItemIterator it( item, QTreeWidgetItemIterator::NoChildren );
			while (*it) {
				//LOG( "set: "+(*it)->text(0)+" = "+(*it)->text(1)+" data="+(*it)->data(0, Qt::UserRole).toString());
				if( (*it)->data(0, Qt::UserRole).toString() == presetLabel ) {
					envMap[(*it)->text(0)] = (*it)->text(1);
					if( (*it)->text(0) == "EPA_CMDLINE" )
						mCmdLine = (*it)->text(1);
					if( (*it)->text(0) == "EPA_WORKINGDIR" )
						mWorkingDir = (*it)->text(1);
					//LOG( "set: "+(*it)->text(0)+" = "+(*it)->text(1));
				}
				++it;
			}
		}
	}
	QStringList ret;
	foreach( QString key, envMap.keys() )
		ret << key + "=" + envMap[key];
	return ret;
}

void MainWindow::launchSoftware(const QString & software)
{
	QStringList env = buildEnvironment(software);

	QProcess  * proc = new QProcess();
	proc->setEnvironment(env);
	LOG("chdir: "+mWorkingDir);
	if( !mWorkingDir.isEmpty() )
		proc->setWorkingDirectory(mWorkingDir);
	LOG("cmdline: "+mCmdLine);
	LOG("cmdargs: "+mCmdArgs.join(" "));
	proc->setProcessChannelMode(QProcess::ForwardedChannels);
	if(mCmdArgs.size() > 0)
		mCmdLine += " "+ mCmdArgs.join(" ");
	proc->start(mCmdLine);

	mCmdLine.clear();
	mCmdArgs.clear();
	mWorkingDir.clear();
}

void MainWindow::shellSoftware(const QString & software)
{
	QStringList env = buildEnvironment(software);

	QProcess  * proc = new QProcess();
	proc->setEnvironment(env);
	if( !mWorkingDir.isEmpty() )
		proc->setWorkingDirectory(mWorkingDir);
	proc->setProcessChannelMode(QProcess::ForwardedChannels);
	proc->start("terminal");

	mCmdLine.clear();
	mWorkingDir.clear();
}

void MainWindow::setProject( const QString & project )
{
	mCurrentProject = project;
	mProjectCombo->setCurrentIndex( mProjectCombo->findText(project) );
}

void MainWindow::setDept( const QString & dept )
{
	mCurrentDept = dept;
	mDeptCombo->setCurrentIndex( mDeptCombo->findText(dept) );
}

void MainWindow::setRoot( const QString & root )
{
	mRoot = root;
}

void MainWindow::setArch( const QString & arch )
{
	mArch = arch;
}

void MainWindow::setSep( const QString & sep )
{
	mSep = sep;
}

void MainWindow::setOverride( const QMap<QString, QString> & over )
{
	mOverride = over;
}

void MainWindow::slotDoubleClicked( QTreeWidgetItem * item, int column )
{
	if( mTree->indexOfTopLevelItem(item) > -1 )
		launchSoftware(item->text(0));
}

void MainWindow::dragEnterEvent( QDragEnterEvent * event )
{
	QTreeWidgetItem * dropItem = mTree->itemAt( mTree->mapFromGlobal(QCursor::pos()) );
	if( dropItem ) {
		event->acceptProposedAction();
	}
}

void MainWindow::dragMoveEvent( QDragMoveEvent * event )
{
	QTreeWidgetItem * dropItem = mTree->itemAt( mTree->mapFromGlobal(QCursor::pos()) );
	if( dropItem && !dropItem->isSelected() && mTree->indexOfTopLevelItem(dropItem) > -1 ) {
		mTree->clearSelection();
		dropItem->setSelected(true);
		event->acceptProposedAction();
	}
}

void MainWindow::dropEvent( QDropEvent * event )
{
	QTreeWidgetItem * dropItem = mTree->itemAt( mTree->mapFromGlobal(QCursor::pos()) );
	if( event->mimeData()->hasUrls() && dropItem && mTree->indexOfTopLevelItem(dropItem) > -1 ) {
		QString path = event->mimeData()->urls().first().toLocalFile();
		LOG("launch: "+path);
		mCmdArgs << path;
		launchSoftware(dropItem->text(0));
		event->acceptProposedAction();
	}
}
