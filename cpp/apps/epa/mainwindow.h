
/*
 *
 * Copyright 2003, 2004 Blur Studio Inc.
 *
 * This file is part of the Resin software package.
 *
 * Assburner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Assburner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Resin; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 
#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <qmap.h>
#include <qpoint.h>
#include <qmainwindow.h>
#include <QDropEvent>
#include <QMimeData>
#include <QUrl>

#include "epaloader.h"

#include "ui_epalaunchui.h"

class MainWindow : public QMainWindow, public Ui::EpaWindowUI
{
	Q_OBJECT
public:
	MainWindow(const QString & xmlRoot);

public slots:

	void populateProjects();
	void populateDepartments(const QString &);
	void populateSoftware(const QString &);

	void loadEpaXml();

	void showContextMenu(const QPoint &);

	void slotDoubleClicked( QTreeWidgetItem *, int );
	void launchSoftware( const QString & );
	void shellSoftware( const QString & );
	QStringList buildEnvironment( const QString & );

	void setOverride( const QMap<QString, QString> & );
	void setProject( const QString & );
	void setDept( const QString & );
	void setRoot( const QString & );
	void setArch( const QString & );
	void setSep( const QString & );

	void loadSettings();

protected:
	void closeEvent(QCloseEvent *);
	void saveSettings();

// drag and drop start of apps
  void dragEnterEvent( QDragEnterEvent * event );
	void dragMoveEvent( QDragMoveEvent * event );
	void dropEvent( QDropEvent * event );

private:
	QMap< QString, QMap<QString, QStringList> > mProjectData;
	QString mCurrentProject, mCurrentDept, mWorkingDir, mCmdLine;
	
	QMap<QString, QString> mOverride;
	QMap<QString, QMap<QString, QString> > mSoftwareEnv;

	QStringList mOverrideList;
	QStringList mCmdArgs;

	QString mRoot, mArch, mSep;
};

#endif // MAIN_WINDOW_H

