// -*- C++;indent-tabs-mode: t; tab-width: 4; c-basic-offset: 4; -*-

#include <half.h>
#include <ImathBox.h>
#include <ImfInputFile.h>
#include <ImfChannelList.h>
#include <ImfChannelListAttribute.h>
#include <ImfPixelType.h>
#include <ImfRgba.h>
#include <ImfOutputFile.h>

#include <iostream>
#include <vector>
#include <iterator>
#include <thread>


void usage()
{
	std::cout << "exruntile - Converts tiled exrs to normal scanline format" << std::endl;
	std::cout << "usage exruntile inFileName outFileName" << std::endl;
}

template< class InputIt >
int distance( InputIt i, InputIt e )
{
	int cnt = 0;
	for( ; i != e; ++i, ++cnt );
	return cnt;
}

int main(int argc, char * argv[] )
{
	if( argc < 3 ) {
		usage();
		return 1;
	}
	
	std::string inFileName(argv[1]);
	std::string outFileName(argv[2]);
	
	try {
		/* Runs a lot faster utilizing all cores */
		unsigned int threads = std::thread::hardware_concurrency();
		if (threads) {
			std::cout << "Using " << threads << " work threads" << std::endl;
			Imf::setGlobalThreadCount(threads);
		}
		
		/* Read header */
		Imf::InputFile inFile (inFileName.c_str());
		Imf::Header hdr = inFile.header();
		Imath::Box2i dw = hdr.dataWindow();
		int dataWidth = dw.max.x - dw.min.x + 1;
		int dataHeight = dw.max.y - dw.min.y + 1;
		
		/* Copy all non-tile attributes to new header */
		Imf::Header outHeader;
		for( auto it = hdr.begin(), end = hdr.end(); it != end; ++it ) {
			std::string name = it.name();
			if( name == "tiles" ) continue;
			outHeader.insert(name.c_str(), it.attribute());
		}
		
		/* Prepare a framebuffer object for reading/writing data */
		Imf::FrameBuffer fb;
		std::vector<std::vector<half> > scanlines;
		
		Imf::ChannelList channels = hdr.channels();
		
		/* Collect the standar (non-layer) channels */
		{
			std::vector<const char *> standardChannels;
			for( auto it = channels.begin(), end = channels.end(); it != end; ++it ) {
				const char * name = it.name();
				if (strstr(name,".")) continue;
				standardChannels.push_back(name);
			}
			
			scanlines.emplace_back( dataWidth * standardChannels.size() * dataHeight );
			std::vector<half> & scanline = scanlines.back();
			int xStride = standardChannels.size() * sizeof(half);
			int yStride = xStride * dataWidth;
			int idx = 0;
			for( auto channelName : standardChannels ) {
				fb.insert( channelName, Imf::Slice( Imf::HALF, reinterpret_cast<char*>(&scanline[idx++]), xStride, yStride ) );
				std::cout << "Adding channel: " << channelName << std::endl;
			}
		}

		/* Collect extra layers */
		std::set<std::string> layers;
		channels.layers(layers);
		for( auto layerName : layers ) {
			Imf::ChannelList::Iterator it, end;
			channels.channelsInLayer(layerName,it,end);
			int channelCount = distance(it,end);
			
			scanlines.emplace_back( dataWidth * channelCount * dataHeight );
			std::vector<half> & scanline = scanlines.back();
			
			int xStride = channelCount * sizeof(half);
			int yStride = xStride * dataWidth;
			for( int idx = 0; it != end; ++it, ++idx ) {
				fb.insert( it.name(), Imf::Slice( Imf::HALF, reinterpret_cast<char*>(&scanline[idx]), xStride, yStride ) );
				std::cout << "Adding channel: " << it.name() << std::endl;
			}
		}

		Imf::OutputFile outFile( outFileName.c_str(), outHeader, Imf::WRITE_RGBA );
		inFile.setFrameBuffer( fb );
		outFile.setFrameBuffer( fb );
		inFile.readPixels( dw.min.y, dw.max.y );
		outFile.writePixels( dataHeight );
	}
	catch (const std::exception &e)
	{
		std::cerr << e.what() << std::endl;
		return 1;
	}
	
	return 0;
}


