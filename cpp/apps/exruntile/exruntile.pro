
TARGET=exruntile

include($$(PRI_SHARED)/common.pri)

SOURCES += exruntile.cpp

unix {
	LIBS+=-lIex -lImath -lHalf -lIlmImf
	INCLUDEPATH+=/usr/include/OpenEXR/
} else {
	DEFINES+=OPENEXR_DLL
	CONFIG+=exceptions
	OPENEXR=E:/openexr_$${COMPILER}/
	ILMBASE=E:/ilmbase_$${COMPILER}/
	INCLUDEPATH+=$${OPENEXR}/Include/OpenExr
	INCLUDEPATH+=$${ILMBASE}/Include/OpenExr
	LIBS+=-L$${OPENEXR}\Lib -L$${ILMBASE}\Lib
	LIBS+=-lIex-2_2 -lImath-2_2 -lHalf -lIlmImf-2_2 -lIlmThread-2_2
}

CONFIG += thread warn_on rtti exceptions console
INSTALLS += target
 
