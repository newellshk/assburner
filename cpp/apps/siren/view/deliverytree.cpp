
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include "modeliter.h"

#include "deliverytree.h"

#include "element.h"
#include "delivery.h"
#include "deliveryelement.h"


struct DeliveryCache : public CacheBase
{
	Delivery mDelivery;
	Qt::CheckState mChecked;

	void setup( const Record & r, const QModelIndex & = QModelIndex() ) {
		mDelivery = r;
		mChecked = Qt::Unchecked;
	}

	Record getRecord() { return mDelivery; }

	QVariant data( const QModelIndex & i, int role ) {
		if( i.column() == 0 ) {
			if( role == Qt::CheckStateRole )
				return mChecked;
			else if( role == Qt::DisplayRole )
				return mDelivery.name();
		}
		return QVariant();
	}

	Qt::ItemFlags flags( const QModelIndex & ) {
		return Qt::ItemFlags( Qt::ItemIsUserCheckable | Qt::ItemIsEnabled );
	}

	bool setData( const QModelIndex & i, const QVariant & data, int role ) {
		if( i.column() == 0 && role == Qt::CheckStateRole ) {
			mChecked = Qt::CheckState(data.toInt());
			return true;
		}
		return false;
	}

	int cmp( const DeliveryCache & other, const QModelIndex &, const QModelIndex &, bool ) const {
		return compareRetI(mDelivery.name(), other.mDelivery.name());
	}
};

typedef RecordModelImp<TreeNodeT<DeliveryCache> > DeliveryModel;

DeliveryTree::DeliveryTree( QWidget * parent )
: RecordTreeView( parent )
, mReadOnly( false )
{

	setSelectionMode( QAbstractItemView::ExtendedSelection );
	Table * et = Delivery::table();
	connect( et, SIGNAL( added( RecordList ) ), SLOT( slotElementsAdded( RecordList ) ) );
	connect( et, SIGNAL( removed( RecordList ) ), SLOT( slotElementsRemoved( RecordList ) ) );
	connect( et, SIGNAL( updated( Record, Record ) ), SLOT( slotElementUpdated( Record, Record ) ) );
	setColumnAutoResize( 0, true );
}

void DeliveryTree::setRootElement(const Project & p)
{
	QStringList headerLabels;
	headerLabels << "Name";
	DeliveryModel * dm = new DeliveryModel( this );
	dm->setHeaderLabels( headerLabels );

	mDeliveries = Delivery::recordsByProject(mProject);
	dm->setRootList( mDeliveries );
	setModel( dm );

	model()->setRootList( mDeliveries );
	expandRecursive();
}

DeliveryList DeliveryTree::elementsByState( Qt::CheckState state )
{
	return model()->getRecords( ModelIter::collect( model(), ModelIter::fromCheckState(state) ) );
}

DeliveryList DeliveryTree::checkedElements()
{
	return elementsByState( Qt::Checked );
}

DeliveryList DeliveryTree::noChangeElements()
{
	return elementsByState( Qt::PartiallyChecked );
}

void DeliveryTree::setChecked( DeliveryList els )
{
	RecordModel * m = model();
	for( ModelIter it(m,ModelIter::Recursive); it.isValid(); ++it )
		if( els.contains( m->getRecord(*it) ) )
			m->setData(*it,Qt::Checked,Qt::CheckStateRole);
}

void DeliveryTree::setNoChange( DeliveryList els )
{
	RecordModel * m = model();
	for( ModelIter it(m,ModelIter::Recursive); it.isValid(); ++it )
		if( els.contains( m->getRecord(*it) ) )
			m->setData(*it,Qt::PartiallyChecked,Qt::CheckStateRole);
}

void DeliveryTree::slotElementsAdded( RecordList  )
{
}

void DeliveryTree::slotElementUpdated( Record , Record )
{
}

void DeliveryTree::slotElementsRemoved( RecordList  )
{
}

