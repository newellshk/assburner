
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef DELIVERY_TREE_H
#define DELIVERY_TREE_H

#include <qtreeview.h>

#include "recordtreeview.h"

#include "delivery.h"
#include "deliveryelement.h"
#include "project.h"

class DeliveryTree : public RecordTreeView
{
Q_OBJECT
public:
	DeliveryTree( QWidget * parent );

	void setRootElement(const Project &);
	DeliveryList checkedElements();
	DeliveryList noChangeElements();

	void setChecked( DeliveryList els );
	void setNoChange( DeliveryList els );

	void setReadOnly( bool );
	
public slots:
	void slotElementsAdded( RecordList );
	void slotElementsRemoved( RecordList );
	void slotElementUpdated( Record, Record );

protected:
	Project mProject;
	bool mReadOnly;
	DeliveryList mDeliveries;

	DeliveryList elementsByState( Qt::CheckState );
};

#endif // DELIVERY_TREE_H

