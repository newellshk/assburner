
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include "fastmodel.h"
#include "modeliter.h"

#include "elementchecktree.h"

#include "asset.h"
#include "shot.h"
#include "shotgroup.h"
#include "project.h"

struct ElementCheckCache : public CacheBase
{
	Element e;
	Qt::CheckState checked;
	void setup( const Record & r, const QModelIndex & = QModelIndex() ) {
		e = r;
		checked = Qt::Unchecked;
	}
	Record getRecord() { return e; }
	QVariant data( const QModelIndex & i, int role ) {
		if( i.column() == 0 ) {
			if( role == Qt::CheckStateRole )
				return checked;
			else if( role == Qt::DisplayRole )
				return e.displayName();
		}
		return QVariant();
	}
	Qt::ItemFlags flags( const QModelIndex & ) {
		return Qt::ItemFlags( Qt::ItemIsUserCheckable | Qt::ItemIsEnabled );
	}
	bool setData( const QModelIndex & i, const QVariant & data, int role ) {
		if( i.column() == 0 && role == Qt::CheckStateRole ) {
			checked = Qt::CheckState(data.toInt());
			return true;
		}
		return false;
	}
	RecordList children( const QModelIndex & ) {
		return e.children();
	}
	int cmp( const ElementCheckCache & other, const QModelIndex &, const QModelIndex &, bool ) const {
		return compareRetI(e.displayName(), other.e.displayName());
	}
};

typedef RecordModelImp<TreeNodeT<ElementCheckCache> > ElementCheckModel;

ElementCheckTree::ElementCheckTree( QWidget * parent )
: RecordTreeView( parent )
, mReadOnly( false )
{
	QStringList headerLabels;
	headerLabels << "Name" << "Type";
	ElementCheckModel * ecm = new ElementCheckModel( this );
	ecm->setHeaderLabels( headerLabels );
	setModel( ecm );
	setSelectionMode( QAbstractItemView::ExtendedSelection );
	Table * et = Element::table();
	connect( et, SIGNAL( added( RecordList ) ), SLOT( slotElementsAdded( RecordList ) ) );
	connect( et, SIGNAL( removed( RecordList ) ), SLOT( slotElementsRemoved( RecordList ) ) );
	connect( et, SIGNAL( updated( Record, Record ) ), SLOT( slotElementUpdated( Record, Record ) ) );
	setColumnAutoResize( 0, true );
}

void ElementCheckTree::setRootElement( const Element & root )
{
	model()->setRootList( root );
	expandRecursive();
}

void ElementCheckTree::setElementTypeList( ElementTypeList elementTypes )
{
	mElementTypes = elementTypes;
}

ElementList ElementCheckTree::elementsByState( const ElementType & , Qt::CheckState state )
{
	return model()->getRecords( ModelIter::collect( model(), ModelIter::fromCheckState(state) ) );
}

ElementList ElementCheckTree::checkedElements( const ElementType & returnType )
{
	return elementsByState( returnType, Qt::Checked );
}

ElementList ElementCheckTree::noChangeElements( const ElementType & returnType )
{
	return elementsByState( returnType, Qt::PartiallyChecked );
}

void ElementCheckTree::setChecked( ElementList els )
{
	RecordModel * m = model();
	for( ModelIter it(m,ModelIter::Recursive); it.isValid(); ++it )
		if( els.contains( m->getRecord(*it) ) )
			m->setData(*it,Qt::Checked,Qt::CheckStateRole);
}

void ElementCheckTree::setNoChange( ElementList els )
{
	RecordModel * m = model();
	for( ModelIter it(m,ModelIter::Recursive); it.isValid(); ++it )
		if( els.contains( m->getRecord(*it) ) )
			m->setData(*it,Qt::PartiallyChecked,Qt::CheckStateRole);
}

void ElementCheckTree::slotElementsAdded( RecordList  )
{
}

void ElementCheckTree::slotElementUpdated( Record , Record )
{
}

void ElementCheckTree::slotElementsRemoved( RecordList  )
{
}

void ElementCheckTree::buildDependencies( ElementList selected  )
{
	if( selected.size() )
		setRootElement( selected[0].project() );
	else
		return;

	QMap<uint, uint> depMap;
	foreach( Element e, selected )
	{
		ElementList deps = e.dependencies();
		foreach( Element dep, deps )
		{
			uint key = dep.key();
			if( !depMap.contains( key ) )
				depMap[key] = 1;
			else
				depMap[key]++;
		}
	}

	ElementList checked, tri;
	for( QMap<uint, uint>::Iterator it = depMap.begin(); it != depMap.end(); ++it ){
		if( it.value() == selected.size() )
			checked += Element( it.key() );
		else
			tri += Element( it.key() );
	}

	setChecked( checked );
	setNoChange( tri );
}

void ElementCheckTree::saveDependencies( ElementList selected )
{
	if( selected.size() ){
		ElementList on = checkedElements( Asset::type() ), 
								nc = noChangeElements( Asset::type() );
		foreach( Element e, selected )
		{
			ElementList deps = e.dependencies();
			for( ElementIter dep = deps.begin(); dep != deps.end(); )
			{
				if( !on.contains( *dep ) && !nc.contains( *dep ) )
					dep = deps.remove( dep );
				else
					++dep;
			}
			foreach( Element e2, on )
				if( !deps.contains( e2 ) )
					deps += e2;
			e.setDependencies( deps );
		}
	}
}


