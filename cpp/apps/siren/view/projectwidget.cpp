
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qcheckbox.h>
#include <qcombobox.h>
#include <qdatetimeedit.h>
#include <qgroupbox.h>
#include <qlineedit.h>
#include <qlistwidget.h>
#include <qpushbutton.h>
#include <qvalidator.h>

#include "config.h"
#include "projectwidget.h"
#include "record.h"
#include "resinerror.h"
#include "resolutiondialog.h"
#include "database.h"
#include "fieldlineedit.h"

#include "projectstatus.h"

ProjectWidget::ProjectWidget( QWidget * parent )
: IntroBase( parent )
{
	setupUi( this );
	connect( mAddResolutionButton, 		SIGNAL( clicked() ), SLOT( addResolution() ) );
	connect( mEditResolutionButton, 	SIGNAL( clicked() ), SLOT( editResolution() ) );
	connect( mRemoveResolutionButton, SIGNAL( clicked() ), SLOT( removeResolution() ) );
	
	connect( Resolution::table(), SIGNAL( added( RecordList ) ), SLOT( resolutionsAdded( RecordList ) ) );
	connect( Resolution::table(), SIGNAL( removed( RecordList ) ), SLOT( resolutionsDeleted( RecordList ) ) );
	connect( Resolution::table(), SIGNAL( updated( Record, Record ) ), SLOT( resolutionUpdated( Record ) ) );
}

void ProjectWidget::setElementList( ElementList list )
{
	Element element = list[0];

	bool hasEditPerms = User::hasPerms( "Project", true );
	mAddResolutionButton->setEnabled( hasEditPerms );
	mEditResolutionButton->setText( hasEditPerms ? "Edit" : "View" );
	
	if( mProject.isRecord() ){
		mProject.setStartDate( mStartDate->date() );
		mProject.setDueDate( mDueDate->date() );
		mProject.setDayRate( mDayRate->text().toFloat() );
		mProject.commit();
	}

	if( element.elementType() != Project::type() )
		return;

	mProject = Project( element );
	mStartDate->setDate( mProject.startDate() );
	mDueDate->setDate( mProject.dueDate() );
	mDayRate->setText( QString::number( mProject.dayRate() ) );
	
	mResolutionListBox->clear();

	mResolutions = Resolution::recordsByProject( mProject );
	foreach( Resolution r, mResolutions )
		mResolutionListBox->addItem( r.displayString() );
	bool ee = bool(mResolutions.size());
	mEditResolutionButton->setEnabled( ee );
	mRemoveResolutionButton->setEnabled( ee && hasEditPerms );
}

void ProjectWidget::resolutionsAdded( RecordList recs )
{
	ResolutionList list( recs );
	foreach( Resolution res, list )
	{
		if( res.project() != mProject )
			continue;

		mResolutionListBox->addItem( res.displayString() );
		mResolutions += res;
		mEditResolutionButton->setEnabled( true );
		mRemoveResolutionButton->setEnabled( true );
	}
}

void ProjectWidget::resolutionsDeleted( RecordList recs )
{
	ResolutionList list( recs );
	foreach( Resolution res, list )
	{
		if( res.project() != mProject )
			continue;

		int i = 0;
		foreach( Resolution r2, mResolutions ) {
			if( r2 == res ) {
				mResolutions.remove( r2 );
				delete mResolutionListBox->item( i );
				break;
			}
			i++;
		}
		bool ee = bool(mResolutions.size());
		mEditResolutionButton->setEnabled( ee );
		mRemoveResolutionButton->setEnabled( ee );
	}
}

void ProjectWidget::resolutionUpdated( Record rec )
{
	Resolution res( rec );
	int idx = mResolutions.findIndex( res );
	if( idx >= 0 )
		mResolutionListBox->item( idx )->setText( res.displayString() );
}

void ProjectWidget::addResolution()
{
	ResolutionDialog rd( this );
	if( rd.exec() == QDialog::Accepted ){
		Database::current()->beginTransaction( "Add Resolution" );
		Resolution res = rd.resolution();
		res.setProject( mProject );
		res.commit();
		Database::current()->commitTransaction();
	}
}

void ProjectWidget::editResolution()
{
	ResolutionDialog rd( this );
	QListWidgetItem * it = mResolutionListBox->currentItem();
	if( !it ) return;
	Resolution res = mResolutions[mResolutionListBox->row(it)];
	rd.setResolution( res );
	if( rd.exec() == QDialog::Accepted ){
		Database::current()->beginTransaction( "Edit Resolution" );
		res.commit();
		Database::current()->commitTransaction();
	}
}

void ProjectWidget::removeResolution()
{
	QListWidgetItem * cur = mResolutionListBox->currentItem();
	if( !cur ) return;

	if( ResinError::deleteConfirmation( this ) )
	{
		Database::current()->beginTransaction( "Remove Resolution" );
		
		ResolutionIter it = mResolutions.at( mResolutionListBox->row( cur ) );
		if( it != mResolutions.end() )
			(*it).remove();
		Database::current()->commitTransaction();
	}
}

void ProjectWidget::addDelivery()
{
	ResolutionDialog rd( this );
	if( rd.exec() == QDialog::Accepted ){
		Database::current()->beginTransaction( "Add Resolution" );
		Resolution res = rd.resolution();
		res.setProject( mProject );
		res.commit();
		Database::current()->commitTransaction();
	}
}

void ProjectWidget::editDelivery()
{
	ResolutionDialog rd( this );
	QListWidgetItem * it = mResolutionListBox->currentItem();
	if( !it ) return;
	Resolution res = mResolutions[mResolutionListBox->row(it)];
	rd.setResolution( res );
	if( rd.exec() == QDialog::Accepted ){
		Database::current()->beginTransaction( "Edit Resolution" );
		res.commit();
		Database::current()->commitTransaction();
	}
}

void ProjectWidget::removeDelivery()
{
	QListWidgetItem * cur = mResolutionListBox->currentItem();
	if( !cur ) return;

	if( ResinError::deleteConfirmation( this ) )
	{
		Database::current()->beginTransaction( "Remove Resolution" );
		
		ResolutionIter it = mResolutions.at( mResolutionListBox->row( cur ) );
		if( it != mResolutions.end() )
			(*it).remove();
		Database::current()->commitTransaction();
	}
}

