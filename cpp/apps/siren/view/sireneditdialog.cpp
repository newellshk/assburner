
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qcheckbox.h>
#include <qcombobox.h>
#include <qdatetime.h>
#include <qdatetimeedit.h>
#include <qgroupbox.h>
#include <qinputdialog.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qstringlist.h>
#include <qstackedwidget.h>
#include <qmessagebox.h>

#include "blurqt.h"
#include "modeliter.h"
#include "shotitems.h"
#include "sirenmainwindow.h"
#include "sirenshotwidget.h"
#include "assettype.h"
#include "elementstatus.h"

#include "resinerror.h"

#include "sireneditdialog.h"

SirenEditDialog::SirenEditDialog( QWidget * parent )
: QDialog( parent )
{
	setupUi( this );
	
	commitList.clear();

	connect( mPrevButton, SIGNAL( pressed() ), SLOT( prevShot() ) );
	connect( mNextButton, SIGNAL( pressed() ), SLOT( nextShot() ) );
	connect( mNewShotButton, SIGNAL( pressed() ), SLOT( newShot() ) );
	connect( mDupShotButton, SIGNAL( pressed() ), SLOT( dupShot() ) );
	connect( new QShortcut( QKeySequence::New, this ), SIGNAL( activated() ), SLOT( newShot() ) );
}

void SirenEditDialog::setElementList( ElementList el )
{
	commitList.commit();
	commitList.clear();
	mIntroView->setElementList(el);
	
	if( el[0].elementType() != Shot::type() ) {
		mPrevButton->hide();
		mNextButton->hide();
		mNewShotButton->hide();
		mDupShotButton->hide();
	}
	currentElement = el[0];
}

Element SirenEditDialog::current()
{
	return currentElement;
}

void SirenEditDialog::accept()
{
	setElementList( ElementList() );
	QDialog::accept();
}

void SirenEditDialog::nextShot()
{
	SirenShotWidget * tree = ((SirenMainWindow*)parent())->mTree;
	ElementList selected = tree->selectedElements();
	Element lastEl = selected[selected.size()-1];

	QModelIndex last = ((RecordModel*)tree->model())->findIndex( lastEl );
	
	QModelIndex next = last.sibling( last.row()+1, 0 );
	Record nextShot;
	if( next.isValid() )
		nextShot = ((ShotModel*)tree->model())->getRecord( next );
	
	if( nextShot.isRecord() ) {
		setElementList( ElementList( Shot(nextShot) ) );
		tree->setSelection( ElementList( Shot(nextShot) ) );
	}
}

void SirenEditDialog::prevShot()
{
	SirenShotWidget * tree = ((SirenMainWindow*)parent())->mTree;
	ElementList selected = tree->selectedElements();
	Element firstEl = selected[0];

	QModelIndex first = ((RecordModel*)tree->model())->findIndex( firstEl );
	QModelIndex prev = first.sibling( first.row()-1, 0 );
	Record prevShot;
	if( prev.isValid() )
		prevShot = ((ShotModel*)tree->model())->getRecord( prev );
	
	if( prevShot.isRecord() ) {
		setElementList( ElementList( Shot(prevShot) ) );
		tree->setSelection( ElementList( Shot(prevShot) ) );
	}
}

void SirenEditDialog::dupShot()
{
	QMessageBox error( QMessageBox::Warning, "disable", "Duplicate Shot not currently enabled", QMessageBox::Ok, this);
	error.setModal(true);
	error.exec();
	
	Shot c = current();
	qWarning("shot is " + c.name().toAscii());
	
/*	QVariant tempName = mIntroView->mIntroView->mNameEdit;
	tempStatus = mIntroView->mStatusCombo;
	tempStartDate = mIntroView->mDateStart;
	tempEndDate = mIntroView->mDateComplete;
	tempDaysBid = mIntroView->mDaysBidSpin;
	tempDaysEstimate = mIntroView->mDaysEstSpin;
	tempDescription = mIntroView->mDescription;

	tempFrameStart = mIntroView->mDetailStack->mFrameStartSpin;
	tempFrameEnd = mIntroView->mDetailStack->mFrameEndSpin;
	tempScriptPage = mIntroView->mDetailStack->mScriptPageEdit;
	tempCamInfo = mIntroView->mDetailStack->mCameraInfoEdit;
	tempDialog = mIntroView->mDetailStack->mDialogEdit;

	newShot();

	mIntroView->mNameEdit = tempName;
	mIntroView->mStatusCombo = tempStatus;
	mIntroView->mDateStart = tempStartDate;
	mIntroView->mDateComplete = tempEndDate;
	mIntroView->mDaysBidSpin = tempDaysBid;
	mIntroView->mDaysEstSpin = tempDaysEstimate;
	mIntroView->mDescription = tempDescription;

	mIntroView->mDetailStack->mFrameStartSpin = tempFrameStart;
	mIntroView->mDetailStack->mFrameEndSpin = tempFrameEnd;
	mIntroView->mDetailStack->mScriptPageEdit = tempScriptPage;
	mIntroView->mDetailStack->mCameraInfoEdit = tempCamInfo;
	mIntroView->mDetailStack->mDialogEdit = tempDialog;*/
}

void SirenEditDialog::newShot(const Project & project, const ShotGroup & scene )
{
	qWarning("this dialog should be directed to a new shot");
	
	SirenShotWidget * tree = ((SirenMainWindow*)parent())->mTree;
	ShotList sl = tree->allElements();
	Shot blankShot = AssetType::recordByName( "Shot" ).construct();
	
	LOG_5( "Shot's assettype is: " + blankShot.assetType().name() );
	blankShot.setFrameStart( 0 );
	blankShot.setFrameEnd( 0 );
	blankShot.setName( "" );
	blankShot.setParent( scene );
	blankShot.setProject( project );
	blankShot.setElementStatus( ElementStatus::recordByName( "New" ) );
	blankShot.setElementType( Shot::type() );
	
	double highestNum = 0.0;
	foreach(double shotNum, sl.shotNumbers())
		highestNum = qMax( shotNum, highestNum );
	blankShot.setShotNumber( (int)highestNum + 1 );

	setElementList( blankShot );
	commitList += (Element)blankShot;
	
	TaskList to_add;
	Task task2d;
	task2d.setAssetType( AssetType::recordByName("Compositing") );
	task2d.setParent(blankShot);
	task2d.setProject(blankShot.parent());
	task2d.setName("Compositing");
	task2d.setElementType(Task::type());
	commitList += (Element)task2d;

	Task task3d;
	task3d.setAssetType( AssetType::recordByName("FX") );
	task3d.setParent(blankShot);
	task3d.setProject(blankShot.parent());
	task3d.setName("VFX");
	task3d.setElementType(Task::type());
	commitList += (Element)task3d;

	Task taskRoto;
	taskRoto.setAssetType( AssetType::recordByName("Roto") );
	taskRoto.setParent(blankShot);
	taskRoto.setProject(blankShot.parent());
	taskRoto.setName("Roto");
	taskRoto.setElementType(Task::type());
	commitList += (Element)taskRoto;
}

