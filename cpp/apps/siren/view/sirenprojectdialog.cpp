
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qcheckbox.h>
#include <qcombobox.h>
#include <qdatetime.h>
#include <qdatetimeedit.h>
#include <qgroupbox.h>
#include <qinputdialog.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qstringlist.h>
#include <qstackedwidget.h>

#include "blurqt.h"
#include "config.h"

#include "assettype.h"
#include "group.h"
#include "elementstatus.h"
#include "project.h"
#include "projectstatus.h"
#include "resinerror.h"
#include "shotgroup.h"

#include "sirenprojectdialog.h"

SirenProjectDialog::SirenProjectDialog( QWidget * parent )
: QDialog( parent )
{
	setupUi( this );
	mDueDate->setDate( QDate::currentDate() );
	mStartDate->setDate( QDate::currentDate() );
}

void SirenProjectDialog::accept()
{
	if( mProjectNameEdit->text().isEmpty() ){
		ResinError::nameEmpty( this, "Project Name" );
		return;
	}
	if( Project::recordByName( mProjectNameEdit->text() ).isRecord() ){
		ResinError::nameTaken( this, mProjectNameEdit->text() );
		return;
	}
	if( mAbbrEdit->text().isEmpty() ){ 
		ResinError::nameEmpty( this, "Abbreviation" );
		return;
	}

	Project p = Project();
	p.setElementStatus( ElementStatus::recordByName( "New" ) );
	p.setProjectStatus( ProjectStatus::recordByName( "Production" ) );
	p.setElementType( Project::type() );
	p.setName( mProjectNameEdit->text() );
	p.setStartDate( mStartDate->date() );
	p.setDueDate( mDueDate->date() );
	p.setShortName( mAbbrEdit->text() );
	p.commit();

	// need keyElement before we can set our own fkeyelement to ourself
	p.setProject( p );
	p.commit();

	mProject = p;
	
	QDialog::accept();
}

Project SirenProjectDialog::project()
{
	return mProject;
}

