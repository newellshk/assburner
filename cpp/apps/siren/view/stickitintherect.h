
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef STICK_IT_IN_THE_RECT_H
#define STICK_IT_IN_THE_RECT_H

#include <qevent.h>
#include <qimage.h>
#include <qpainter.h>
#include <qpixmap.h>
#include <qwidget.h>

class StickItInTheRect : public QWidget
{
Q_OBJECT
public:
	StickItInTheRect( QWidget * parent );
	virtual void paintEvent( QPaintEvent * pe );
	virtual void mousePressEvent( QMouseEvent * me );
	virtual void mouseMoveEvent( QMouseEvent * me );
	virtual void resizeEvent( QResizeEvent * re );
	
	void redrawRect( QPainter * p, const QRect & rect = QRect() );
	void setRect( const QRect & rect );
	void setImage( QImage i );
	QRect grabRect();
	QPixmap pixmap();

signals:
	void rectChange( QRect );

public slots:
	void setSquareAspect( bool );

protected:
	double mScale;
	QImage mOrig;
	QPixmap mBuffer;
	bool showRect;
	QRect mRect;
	bool mSquareAspect;
	bool mLockRegion;

};

#endif // STICK_IT_IN_THE_RECT_H

