
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qfile.h>
#include <qpixmap.h>
#include <qbuffer.h>
#include <qwhatsthis.h>
#include <qurl.h>
#include <qmessagebox.h>

#include "blurqt.h"
#include "thumbnailloader.h"

#include "element.h"
#include "elementui.h"
#include "thumbnail.h"
#include "thumbnailui.h"
#include "user.h"

#include "thumbnailbutton.h"
#include "thumbnailchooser.h"

ThumbnailButton::ThumbnailButton ( QWidget * parent )
: QPushButton( parent )
, mDefault( false )
, mValid( false )
{
	setText("");
	connect( this, SIGNAL( clicked() ), SLOT( chooseFile() ) );
	setFixedSize(120,120);
	setIcon( QIcon("images/assfreezer_icon.png") );
	setAcceptDrops( true );
	setWhatsThis( "<b>Thumbnail Button</b><br>"
					"Click this button to set the thumbnail for the selected element(s)." );
}

void ThumbnailButton::dropEvent(QDropEvent* event)
{
	QStringList files;
	QList<QUrl> urls = event->mimeData()->urls();
	// We'll just take the first image url
	foreach( QUrl url, urls ) {
		QString path = url.toLocalFile();
		qWarning("file dropped on ThumbnailButton: "+path.toAscii());
		if( QFile::exists( path ) ) {
			chooseFile(path, true);
		}
	}
}

void ThumbnailButton::dragEnterEvent( QDragEnterEvent *e )
{
	if( e->mimeData()->hasUrls() )
		e->accept();
}

bool ThumbnailButton::setThumbnail( const Thumbnail & tn )
{
	mThumbnail = tn;
	qWarning("checking for tn for: "+mThumbnail.originalFile().toAscii() );
	QPixmap	pix = ThumbnailLoader::load( mThumbnail, QSize( 120, 120 ) );
  
	if( pix.isNull() )
		return false;
		
	mDefault = false;
	mValid = true;
	mPixmap = pix;
	setIcon( pix );
	if( pix.width() > 24 )
		setIconSize(size());
	else
		setIconSize(pix.size());
	return true;
}

Thumbnail ThumbnailButton::thumbnail() const
{
	return mThumbnail;
}

void ThumbnailButton::setElementList( ElementList list )
{
	qWarning("==commiting thumbnails==");
	commitList.commit();
	commitList.clear();
	
	mElements = list;
	mValid = false;
	foreach( Element e, list )
	{
		mThumbnail = e.thumbnail();
		
		if( !mThumbnail.isRecord() )
			continue;
		
		if( setThumbnail( mThumbnail ) )
			break;
	}
	if( mValid )
		return;
	mPixmap = ElementUi(list[0]).image( QSize( 120, 120 ) );
	setIcon( mPixmap );
	if( mPixmap.width() > 24 )
		setIconSize(size());
	else
		setIconSize(mPixmap.size());
	mDefault = true;
}

void ThumbnailButton::accept()
{
	setElementList( ElementList() );
}

void ThumbnailButton::updateThumbnail( ElementList list )
{
	if( mDefault || !mValid )
		return;
		
	foreach( Element e, list )
	{
		Thumbnail tn( e.thumbnail() );
		if( !tn.isRecord() ) {
			tn.setElement( e );
			tn.setDate( QDateTime::currentDateTime() );
			tn.setUser( User::currentUser() );
		}

		QByteArray ba;
		QBuffer buffer(&ba);
		buffer.open(QIODevice::WriteOnly);
		mPixmap.save(&buffer, "PNG");
		tn.setImage( ba );

		tn.setOriginalFile( mPath );
		if( mClipRect.isValid() )
			tn.setClipRect( 
				QString("%1,%2,%3,%4")
				.arg( mClipRect.x() )
				.arg( mClipRect.y() )
				.arg( mClipRect.width() )
				.arg( mClipRect.height() )
			);
		tn.commit();
		e.setThumbnail( tn );
		e.commit();

		ThumbnailUi(tn).setImage( mPixmap );
	}
}

void ThumbnailButton::chooseFile()
{
	chooseFile(QString());
}

void ThumbnailButton::chooseFile(const QString & startPath, bool noDialog)
{
	QRect rect;
	QStringList sl = mThumbnail.clipRect().split(',');
	QString path = mThumbnail.filePath();
	if( !startPath.isEmpty() ) {
		path = startPath;
	} else if( sl.size() == 4 && QFile::exists( mThumbnail.originalFile() ) ) {
		rect = QRect( sl[0].toInt(), sl[1].toInt(), sl[2].toInt(), sl[3].toInt() );
		path = mThumbnail.originalFile();
	}

	if( noDialog ) {
		QPixmap image( startPath );
		if( !image.isNull() ) {
			mPath = startPath;
			mClipRect = image.rect();
			mPixmap = image;
			setIcon( image.scaled( QSize( 120,120 ), Qt::KeepAspectRatio ) );
			setIconSize(size());
			mDefault = false;
			mValid = true;
			updateThumbnail( mElements );
		}
		else {
			QMessageBox error( QMessageBox::Warning, "invalid file", "could not load file '" + startPath + "'\ninvalid image format", QMessageBox::Ok, this);
			error.setModal(true);
			error.exec();
		}
	}
	else {
		ThumbnailChooser * tc = new ThumbnailChooser( path, rect, this );
		
		if( tc->exec() == QDialog::Accepted )
		{
			QPixmap pix = tc->pixmap();
			if( !pix.isNull() ) {
				mPath = tc->path();
				mClipRect = tc->rect();
				mPixmap = pix;
				setIcon( pix.scaled( QSize( 120,120 ), Qt::KeepAspectRatio ) );
				setIconSize(size());
				mDefault = false;
				mValid = true;
				updateThumbnail( mElements );
			}
		}
		delete tc;
	}
}

void ThumbnailButton::setDefaultPixmap( QPixmap def )
{
	setIcon( def );
	setIconSize( def.size() );
	mDefault = true;
}

