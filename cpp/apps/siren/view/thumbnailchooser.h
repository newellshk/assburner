
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef THUMBNAIL_CHOOSER_H
#define THUMBNAIL_CHOOSER_H

#include <QDialog>
#include <qimage.h>
#include <qrect.h>

#include "thumbnail.h"
#include "ui_thumbnailchooser.h"

class ThumbnailChooser : public QDialog, public Ui::ThumbnailChooser
{
Q_OBJECT

public:
	ThumbnailChooser( const QString & path, const QRect & rect, QWidget * parent=0 );
	~ThumbnailChooser();
	
	virtual void dropEvent( QDropEvent* event );
   	virtual void dragEnterEvent( QDragEnterEvent *e );
	
	void setPath( const QString & );
	QRect thumbnailRect();

	QPixmap pixmap() const;
	QString path() const;
	QRect rect() const;
	
	int exec();

public slots:
	bool chooseFile();
	void setRegionLocked();
	void setRect( QRect );

protected:
	QRect mRect;
	QString mPath;
	QImage mImage;
};

#endif // THUMBNAIL_CHOOSER_H

