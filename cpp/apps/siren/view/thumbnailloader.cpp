 
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qfile.h>
#include <qsize.h>
#include <qdir.h>

#include "thumbnailloader.h"
#include "blurqt.h"
#include "path.h"

QString ThumbnailLoader::thumbnailCachePath( const QString & path, const QSize & size )
{
#ifdef Q_OS_WIN
	static const QString temp( "C:/temp/" );
#else
	static const QString temp( "/tmp/" );
#endif // Q_OS_WIN

	// Make sure the thumbnail cache directory exists
	if( !Path( temp + "thumbnailcache" ).mkdir() ) {
		LOG_3( "Couldn't create thumbnailcache directory: " + temp + "thumbnailcache" );
		return QString::null;
	}
	
	QString ret( path );
	ret = ret.replace("\\","_");
	ret = ret.replace("/","_");
	ret = ret.replace(":","");
	if( size.isValid() )
		ret += QString("_%1x%2.png").arg( size.width() ).arg( size.height() );
	return temp + "thumbnailcache/" + ret;
}

void ThumbnailLoader::clear( const QString & fileName )
{
	if( mCache )
		mCache->remove( fileName );
	
	QString cacheFile = thumbnailCachePath( fileName );
	Path p( cacheFile );
	QDir d( p.dirPath() );
	QStringList matches = d.entryList( QStringList(p.fileName() + "*") );
	foreach( QString s, matches )
		QFile::remove( p.dirPath() + s );
}

QPixmap ThumbnailLoader::load( const Thumbnail & t, const QSize & returnSize )
{
	if( !mCache )
		mCache = new PixCache;

	QString p = t.originalFile();
	Path path( p );

	ThumbnailSource & ts = (*mCache)[p];
	
	if( ts.contains( returnSize ) )
		return ts.get( returnSize );
	
	QImage img;
	img.loadFromData(t.image());
	if( !img.isNull() ){
		QImage scaled = img.scaled( returnSize, Qt::KeepAspectRatio, Qt::SmoothTransformation );
		QPixmap pix = QPixmap::fromImage( scaled );
		ts.set( returnSize, pix );
		return pix;
	}
	return QPixmap();
}

PixCache * ThumbnailLoader::mCache = 0;

