
import os
from blur.build import *

path = os.path.dirname(os.path.abspath(__file__))
QMakeTarget("libabsubmit",path,"libabsubmit.pro", ["stone","classes"],[])

rpm = RPMTarget('libabsubmitrpm','libabsubmit',path,'../../../rpm/spec/libabsubmit.spec.template','1.0')
rpm.pre_deps = ['classesrpm','stonerpm']

pyrpm = RPMTargetSip('pyabsubmitrpm','pyabsubmit',path,'../../../rpm/spec/pyabsubmit.spec.template','1.0')
pyrpm.pre_deps = ['pystonerpm','pyclassesrpm','libabsubmitrpm']

# Python module target
pc = SipTarget2("pyabsubmit",path,'absubmit')
pc.pre_deps = ["libabsubmit","pyclasses:install"]
pc.ExtraIncludes += [ '../../classes', '../../classes/autocore', '../../stone/include', '../../qjson/','/usr/include/absubmit','/usr/include/classes','/usr/include/stone','/usr/include/qjson' ]
pc.ExtraLibs += ['stone','classes','absubmit','qjson4']
pc.ExtraLibDirs = ['../../stone','../../classes','../../qjson/']

if __name__ == "__main__":
	build()
