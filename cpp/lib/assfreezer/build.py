
import os
from blur.build import *

path = os.path.dirname(os.path.abspath(__file__))
rev_path = os.path.join(path,'../..')

svn = WCRevTarget("libassfreezersvnrev",path,rev_path,"include/svnrev-template.h","include/svnrev.h")

sipIncludes = [
	'../../classes/','/usr/include/classes',
	'../../classes/autocore/','/usr/include/classes/autocore',
	'../../stone/include','/usr/include/stone',
	'../../stonegui/include','/usr/include/stonegui',
	'../../classesui/include','/usr/include/classesui',
	'../../classesui/.out/',
	'../../qjson/', '/usr/include/qjson/',
	'/usr/include/assfreezer']

sipLibDirs = ['../../stone','../../stonegui','../../classes','../../classesui','../../qjson/']

# Python module target
pc = SipTarget2("pyassfreezer",path,'assfreezer')
pc.pre_deps = ["libassfreezer","pyclasses:install"]
pc.ExtraIncludes += sipIncludes
pc.ExtraLibDirs += sipLibDirs
pc.ExtraLibs += ['stone','qjson4','stonegui','classes','classesui','assfreezer']

pcs = SipTarget2("pyassfreezerstatic",path,'assfreezer',True)
pcs.ExtraIncludes += sipIncludes

QMakeTarget("libassfreezer",path,"libassfreezer.pro",['qgv',"classes","classesui","libabsubmit",svn])
#QMakeTarget("libassfreezerstatic",path,"libassfreezer.pro",["stonestatic","stoneguistatic","classesuistatic","libabsubmit"],[],True)

rpm = RPMTarget('libassfreezerrpm','libassfreezer',path,'../../../rpm/spec/libassfreezer.spec.template','1.0')
rpm.pre_deps = ["libabsubmitrpm"]

pyrpm = RPMTargetSip('pyassfreezerrpm','pyassfreezer',path,'../../../rpm/spec/pyassfreezer.spec.template','1.0')


if __name__ == "__main__":
	build()
