
#ifndef ASSFREEZER_MENUS_H
#define ASSFREEZER_MENUS_H

#include <qmenu.h>

#include "jobdep.h"
#include "jobtask.h"
#include "joberror.h"
#include "service.h"

#include "afcommon.h"

class QAction;

class JobListWidget;
class HostListWidget;
class ErrorListWidget;
class JobViewerPlugin;
class HostViewerPlugin;
class AssfreezerView;

class ASSFREEZER_EXPORT AssfreezerMenuPlugin
{
public:
	virtual ~AssfreezerMenuPlugin(){}
	
	virtual void executeMenuPlugin( QMenu * )=0;
	
	// Name can be the name of a separator which is treated as a section, or a single action
	static void prependAction( QMenu * menu, const QString & name, QAction * action );
	static void appendAction( QMenu * menu, const QString & name, QAction * action );
};

class ASSFREEZER_EXPORT AssfreezerMenuFactory
{
public:
	static AssfreezerMenuFactory * instance();
	
	void registerMenuPlugin( AssfreezerMenuPlugin * plugin, const QString & menuName );
	QList<QAction*> aboutToShow( QMenu * menu, bool addPreSep = false, bool addPostSep = false );
	
	QMap<QString,QList<AssfreezerMenuPlugin*> > mPlugins;
};

class ASSFREEZER_EXPORT AssfreezerMenu : public QMenu
{
Q_OBJECT
public:
	AssfreezerMenu( QWidget * parent, const QString & title = QString() );

public slots:
	virtual void slotCurrentViewChanged( AssfreezerView * );
	virtual void slotAboutToShow() = 0;
	virtual void slotActionTriggered( QAction * ) {}
};

template<class BASE=AssfreezerMenu> class ASSFREEZER_EXPORT JobListMenu : public BASE
{
public:
	JobListMenu(JobListWidget * jobList, const QString & title = QString())
	: BASE(jobList, title)
	, mJobList(jobList)
	{}

	JobListWidget * mJobList;
protected:
	void slotCurrentViewChanged( AssfreezerView * );
};

template<class BASE=AssfreezerMenu> class ASSFREEZER_EXPORT HostListMenu : public BASE
{
public:
	HostListMenu(HostListWidget * hostList, const QString & title = QString())
	: BASE(hostList, title)
	, mHostList(hostList)
	{}
	
	HostListWidget * mHostList;
protected:
	void slotCurrentViewChanged( AssfreezerView * );
};

template<class BASE=AssfreezerMenu> class ASSFREEZER_EXPORT ErrorListMenu : public BASE
{
public:
	ErrorListMenu(ErrorListWidget * errorList, const QString & title = QString())
	: BASE(errorList, title)
	, mErrorList(errorList)
	{}
	
	ErrorListWidget * mErrorList;
protected:
	void slotCurrentViewChanged( AssfreezerView * );
};

class ASSFREEZER_EXPORT StatusFilterMenu : public JobListMenu<>
{
Q_OBJECT
public:
	StatusFilterMenu(JobListWidget *);

	void slotAboutToShow();
	void slotActionTriggered(QAction*);
	void updateActionStates();

protected:
	bool mStatusActionsCreated;
	QAction * mStatusShowAll, * mStatusShowNone;
	QList<QAction*> mStatusActions;
};

class ASSFREEZER_EXPORT ProjectFilterMenu : public JobListMenu<>
{
Q_OBJECT
public:
	ProjectFilterMenu(JobListWidget *);

	void slotAboutToShow();
	void slotActionTriggered(QAction*);
	void updateActionStates();

protected:
	QAction * mProjectShowAll, * mProjectShowNone, * mProjectShowNonProject;
	QList<QAction*> mProjectActions;
	bool mProjectActionsCreated;
};

class ASSFREEZER_EXPORT JobTypeFilterMenu : public AssfreezerMenu
{
Q_OBJECT
public:
	JobTypeFilterMenu(AssfreezerView *, const QString & title = QString());

	void slotAboutToShow();
	void slotActionTriggered(QAction*);
	void updateActionStates();

	virtual JobTypeList jobTypeFilter() const = 0;
	virtual void setJobTypeFilter( JobTypeList ) = 0;
	virtual JobTypeList activeJobTypes() const = 0;
protected:
	bool mJobTypeActionsCreated;
	QAction * mJobTypeShowAll, * mJobTypeShowNone;
	QList<QAction*> mJobTypeActions;
};

class ASSFREEZER_EXPORT JobListJobTypeFilterMenu : public JobListMenu<JobTypeFilterMenu>
{
Q_OBJECT
public:
	JobListJobTypeFilterMenu(JobListWidget * jobList);
	
	virtual JobTypeList jobTypeFilter() const;
	virtual void setJobTypeFilter( JobTypeList );
	virtual JobTypeList activeJobTypes() const;
};

class ASSFREEZER_EXPORT ErrorListJobTypeFilterMenu : public ErrorListMenu<JobTypeFilterMenu>
{
Q_OBJECT
public:
	ErrorListJobTypeFilterMenu(ErrorListWidget * errorList);
	
	virtual JobTypeList jobTypeFilter() const;
	virtual void setJobTypeFilter( JobTypeList );
	virtual JobTypeList activeJobTypes() const;
};

class ASSFREEZER_EXPORT ServiceFilterMenu : public AssfreezerMenu
{
Q_OBJECT
public:
	ServiceFilterMenu(AssfreezerView * view, const QString & title);
	
	void slotAboutToShow();
	void slotActionTriggered(QAction*);
	void updateActionStates();

	virtual ServiceList serviceFilter() const = 0;
	virtual void setServiceFilter( ServiceList ) = 0;
	virtual ServiceList activeServices() const = 0;
protected:
	bool mJobServiceActionsCreated;
	QAction * mJobServiceShowAll, * mJobServiceShowNone;
	QList<QAction*> mJobServiceActions;
};

class ASSFREEZER_EXPORT JobServiceFilterMenu : public JobListMenu<ServiceFilterMenu>
{
Q_OBJECT
public:
	JobServiceFilterMenu(JobListWidget * jobList);
	
	ServiceList serviceFilter() const;
	ServiceList activeServices() const;
	void setServiceFilter( ServiceList );
};

class ASSFREEZER_EXPORT HostServiceFilterMenu : public HostListMenu<ServiceFilterMenu>
{
Q_OBJECT
public:
	HostServiceFilterMenu(HostListWidget * hostList);

	ServiceList serviceFilter() const;
	ServiceList activeServices() const;
	void setServiceFilter( ServiceList );
};

class ASSFREEZER_EXPORT ErrorServiceFilterMenu : public ErrorListMenu<ServiceFilterMenu>
{
Q_OBJECT
public:
	ErrorServiceFilterMenu(ErrorListWidget * errorList);

	ServiceList serviceFilter() const;
	ServiceList activeServices() const;
	void setServiceFilter( ServiceList );
};

class ASSFREEZER_EXPORT AssfreezerJobMenu : public JobListMenu<>
{
Q_OBJECT
public:
	AssfreezerJobMenu(JobListWidget *);

	void slotAboutToShow();
	void slotActionTriggered( QAction * );

public slots:
	void slotAboutToShowLocationsMenu();
	void slotLocationActionToggled(bool checked);
	
protected:
	QAction 
		* mRemoveAllDependenciesAction,
		* mRemoveDependencyAction,
		* mDepTypePerJobAction,
		* mDepTypePerTaskAction,
		* mShowHistoryAction,
		* mSubmitVideoMakerAction,
		* mSetJobKeyListAction,
		* mClearJobKeyListAction,
		* mModifyFrameRangeAction,
		* mVersionedFilterDisableAction,
		* mVersionedFilterTrueAction,
		* mVersionedFilterFalseAction;
	
	QMenu * mLocationsMenu;
	
	JobDepList mJobDeps, mJobIsDeps;
};

class ASSFREEZER_EXPORT CannedBatchJobMenu : public HostListMenu<>
{
Q_OBJECT
public:
	CannedBatchJobMenu(HostListWidget * hostList);

	void slotAboutToShow();
	void slotActionTriggered(QAction*);
};

class ASSFREEZER_EXPORT TailServiceLogMenu : public HostListMenu<>
{
Q_OBJECT
public:
	TailServiceLogMenu(HostListWidget * hostList);

	void slotAboutToShow();
	void slotActionTriggered(QAction*);

protected:
};

class ASSFREEZER_EXPORT AssfreezerHostMenu : public HostListMenu<>
{
Q_OBJECT
public:
	AssfreezerHostMenu(HostListWidget * hostList);

	void slotAboutToShow();
	void slotActionTriggered(QAction*);
public slots:
	void slotShowAssignServiceMenu();
	void slotAddServiceActionTriggered(QAction*);
protected:
	QAction * mShowOfflineHostsAction, * mShowHistoryAction, * mNewHostAction,
		* mEditHostsAction, * mRemoveHostsAction, * mCancelBatchJobTasksAction,
		* mExploreDriveAction, * mCreateHostListAction;
	QMenu * mAssignServiceMenu;
};

class ASSFREEZER_EXPORT AssfreezerTaskMenu : public JobListMenu<>
{
Q_OBJECT
public:
	AssfreezerTaskMenu(JobListWidget *);

	void slotAboutToShow();
	void slotActionTriggered(QAction*);
protected:
	QAction * mInfoAction, * mRerenderFramesAction, * mSuspendFramesAction, 
	        * mCancelFramesAction, * mShowLogAction, * mCopyCommandAction,
			* mSelectHostsAction, * mVncHostsAction, * mShowHistoryAction;
	JobTaskList mTasks;
	QMap<QAction *, JobViewerPlugin *> mJobViewerActions;
	QMap<QAction *, HostViewerPlugin *> mHostViewerActions;
};

/* This menu is used by the errors tab in the Job List View(JobListWidget), and the main
 * list view in the Error List View(ErrorListWidget).  It customizes itself depending
 * on the context by checking the type of it's parent widget.
 */
class ASSFREEZER_EXPORT AssfreezerErrorMenu : public AssfreezerMenu
{
Q_OBJECT
public:
	AssfreezerErrorMenu(QWidget *, JobErrorList selection, JobErrorList all);

	void setErrors( JobErrorList selection, JobErrorList allErrors );
	void slotAboutToShow();
	void slotActionTriggered(QAction*);

protected:
	JobErrorList mSelection, mAll;
	QAction 
	 * mClearSelected,
	 * mClearAll,
	 * mViewSimilar,
	 * mCopyText,
	 * mShowLog,
	 * mSelectHosts,
	 * mRemoveHosts,
	 * mClearHostErrorsAndOffline,
	 * mClearHostErrors,
	 * mVncHostsAction,
	 * mShowErrorInfo;
	
	// Error view specific menus and actions
	QAction
	 * mSetJobKeyListAction,
	 * mClearJobKeyListAction,
	 * mSetLimitAction;

	ErrorServiceFilterMenu * mServiceFilterMenu;
	ErrorListJobTypeFilterMenu * mJobTypeFilterMenu;
	
	QMap<QAction *, HostViewerPlugin *> mHostViewerActions;
};

#endif // ASSFREEZER_MENUS_H
