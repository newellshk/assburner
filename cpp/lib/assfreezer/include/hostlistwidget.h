
#ifndef HOST_LIST_WIDGET_H
#define HOST_LIST_WIDGET_H

#include <memory>
#if defined(_MSC_VER) && (_MSC_VER < 1600)
using namespace std::tr1;
#else
using namespace std;
#endif

#include <qstring.h>

#include "service.h"

#include "assfreezerview.h"

class QAction;
class QMenu;

class FilterEdit;
class RecordTreeView;

class AssFreezerWidget;

void clearHostErrorsAndSetOffline( HostList hosts, bool offline);
QString verifyKeyList( const QString & list, Table * table );
QList<uint> verifyKeyList( const QList<uint> & list, Table * table );

class ASSFREEZER_EXPORT HostListWidget : public AssfreezerView
{
Q_OBJECT
public:
	HostListWidget(QWidget * parent);
	~HostListWidget();

	virtual QString viewType() const;

	QAction* RefreshHostsAction;
	QAction* HostOnlineAction;
	QAction* HostOfflineAction;
	QAction* HostRestartAction;
	QAction* HostRestartWhenDoneAction;
	QAction* HostRebootAction;
	QAction* HostRebootWhenDoneAction;

	QAction* RestartAction;
	QAction* VNCHostsAction;
	QAction* ClientUpdateAction;

	QAction* SubmitBatchJobAction;
	QAction* ShowHostInfoAction;
	QAction* ClearHostErrorsSetOfflineAction;
	QAction* ClearHostErrorsAction;
	QAction* ShowHostErrorsAction;
	QAction* ShowJobsAction;

	FilterEdit * mHostFilterEdit;

	virtual QToolBar * toolBar( QMainWindow * );
	virtual void populateViewMenu( QMenu * );

	RecordTreeView * hostTreeView() const;
	
public slots:

	void hostListSelectionChanged();
	
	void setHostsStatus(const QString & status);
	/// selected hosts are told to start burning
	void setHostsOnline();
	/// selected hosts are told to stop burning
	void setHostsOffline();
	/// selected hosts are told to exit, and the process monitor is responsible for restarting Assburner
	void setHostsRestart();
	void setHostsRestartWhenDone();
	void setHostsReboot();
	void setHostsRebootWhenDone();
	/// selected hosts are told to update to the latest Assburner client
	void setHostsClientUpdate();
	/// opens vncviewer sessions to selected hosts
	void vncHosts();

	/// Opens a Job View and selects the jobs currently assigned to the selected hosts
	void showAssignedJobs();

	void showHostPopup(const QPoint &);

	void selectHosts( HostList );

	void applyOptions();

	void slotGroupingChanged(bool);
	
protected:
	/// refreshes the host list from the database
	void doRefresh();

	void save( IniConfig & ini, bool = false );
	void restore( IniConfig & ini, bool = false );

	void initializeViews(IniConfig & ini);
	// When static data is ready this is called once
	void finishInitialization(IniConfig & ini);

	void customEvent( QEvent * evt );

	RecordTreeView * mHostTree;

	bool mViewsInitialized;
	
	bool mStaticDataRetrieved;
	
	struct SharedData {
		SharedData() : mReady(false) {}
		bool mReady;
		// List of services that are used by jobs
		ServiceList mServiceList;
	};

	static weak_ptr<SharedData> mSharedData;
	shared_ptr<SharedData> mStaticData;

	ServiceList mServiceList, mServiceFilter;

	bool mShowOfflineHosts;
	bool mHostTaskRunning;
	bool mQueuedHostRefresh;

	QToolBar * mToolBar;

	HostList mHostsToSelect;
	
public:
	QMenu * mHostMenu,
	 * mTailServiceLogMenu,
	 * mHostServiceFilterMenu,
	 * mCannedBatchJobMenu;

	friend class AssfreezerHostMenu;
	friend class AssFreezerWidget;
	friend class HostServiceFilterMenu;
	friend class CannedBatchJobMenu;
	friend class TailServiceLogMenu;
};

#endif // HOST_LIST_WIDGET_H
