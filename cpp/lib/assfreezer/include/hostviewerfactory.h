
/* $Author: brobison $
 * $LastChangedDate: 2010-02-01 21:49:52 -0800 (Mon, 01 Feb 2010) $
 * $Rev: 9298 $
 * $HeadURL: svn://svn.blur.com/blur/branches/concurrent_burn/cpp/lib/assfreezer/include/jobsettingswidgetplugin.h $
 */

#ifndef HOST_VIEWER_FACTORY_H
#define HOST_VIEWER_FACTORY_H

#include <qmap.h>
#include <qstring.h>

class QWidget;
class HostViewerPlugin;

class HostViewerFactory
{
public:
	static void registerPlugin( HostViewerPlugin * );
	static QMap<QString,HostViewerPlugin*>  mHostViewerPlugins;
	static bool mPluginsLoaded;
};

#endif // HOST_VIEWER_FACTORY_H
