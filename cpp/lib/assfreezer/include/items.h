
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Assburner.
 *
 * Assburner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Assburner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blur; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* $Author$
 * $LastChangedDate$
 * $Rev$
 * $HeadURL$
 */

#ifndef ITEMS_H
#define ITEMS_H

#include <qtreewidget.h>
#include <qdatetime.h>
#include <qitemdelegate.h>
#include <qpainter.h>

#include "blurqt.h"

#include "employee.h"
#include "job.h"
#include "jobdep.h"
#include "jobtask.h"
#include "joberror.h"
#include "jobtype.h"
#include "jobstatus.h"
#include "jobservice.h"
#include "host.h"
#include "hoststatus.h"

#include "hostselector.h"
#include "recordtreeview.h"
#include "recordsupermodel.h"
#include "modelgrouper.h"

#include "afcommon.h"
#include "displayprefsdialog.h"

#include <math.h>

// color if valid
QVariant civ( const QColor & c );

struct ASSFREEZER_EXPORT FrameItem : public RecordItemBase
{
	static QDateTime CurTime;
	JobTask task;
	JobOutput output;
	JobAssignment jobAssignment;
	JobTaskAssignment taskAssignment;
	QString label, hostName, time, memory, stat;
	int frameNumber;
	ColorOption * co;
	char loadedStatus;
	bool labelSort;
	void setup( const Record & r, const QModelIndex & );
	QVariant modelData( const QModelIndex & i, int role ) const;
	char getSortChar() const;
	int compare( const QModelIndex & a, const QModelIndex & b, int, bool ) const;
	Qt::ItemFlags modelFlags( const QModelIndex & );
	Record getRecord();
};

typedef TemplateRecordDataTranslator<FrameItem> FrameTranslator;

struct ASSFREEZER_EXPORT JobErrorItem : public RecordItemBase
{
	static const int GroupRole = Qt::UserRole;
	JobError error;
	QString hostName, cnt, msg;
	bool multiLine;
	ColorOption * co;
	void setup( const JobError & r, const QModelIndex & );
	QVariant modelData( const QModelIndex & i, int role ) const;
	Qt::ItemFlags modelFlags( const QModelIndex & ) const;
	Record getRecord();
	RecordList children( const QModelIndex & );
};

struct ASSFREEZER_EXPORT ErrorItem : public JobErrorItem
{
	Job job;
	JobType jobType;
	QString services;
	void setup( const JobError & r, const QModelIndex & );
	QVariant modelData( const QModelIndex & i, int role ) const;
};

typedef TemplateRecordDataTranslator<JobErrorItem> JobErrorTranslator;
typedef TemplateRecordDataTranslator<ErrorItem> ErrorTranslator;
//typedef RecordModelImp<TreeNodeT<ErrorCache> > ErrorModel;

class ASSFREEZER_EXPORT JobErrorModel : public RecordSuperModel
{
Q_OBJECT
public:
	JobErrorModel( QObject * parent );
};

class ASSFREEZER_EXPORT ErrorModel : public RecordSuperModel
{
Q_OBJECT
public:
	ErrorModel( QObject * parent );

	void setErrors( JobErrorList errors, JobList jobs, JobServiceList services );

	JobList mJobs;
	QMap<Job,JobServiceList> mJobServicesByJob;
};

struct ASSFREEZER_EXPORT JobItem : public RecordItemBase
{
	Job job;
	JobStatus jobStatus;
	mutable QPixmap icon;
	QString done, userName, hostsOnJob, priority, project, submitted, errorCnt, avgTime, type, ended, timeInQueue, services, avgMemory, estMemory, estCompletion;
	QString version;
	bool healthIsNull, estMemoryFromPreviousRender;
	ColorOption * co;
	void setup( const Record & r, const QModelIndex & );
	QVariant modelData( const QModelIndex & i, int role ) const;
	int compare( const QModelIndex & a, const QModelIndex & b, int, bool ) const;
	Qt::ItemFlags modelFlags( const QModelIndex & );
	Record getRecord();
};

typedef TemplateRecordDataTranslator<JobItem> JobTranslator;

struct ASSFREEZER_EXPORT GroupedJobItem : public GroupItem
{
	virtual QString calculateGroupValue( const QModelIndex & self, int column);
	QVariant modelData( const QModelIndex & i, int role ) const;
	Qt::ItemFlags modelFlags( const QModelIndex & );
};

typedef TemplateDataTranslator<GroupedJobItem> GroupedJobTranslator;

class ASSFREEZER_EXPORT JobTreeBuilder : public RecordTreeBuilder
{
public:
	JobTreeBuilder( SuperModel * parent );
	virtual bool hasChildren( const QModelIndex & parentIndex, SuperModel * model );
	virtual void loadChildren( const QModelIndex & parentIndex, SuperModel * model );
protected:
	JobTranslator * mJobTranslator;
};

class ASSFREEZER_EXPORT JobModel : public RecordSuperModel
{
Q_OBJECT
public:
	JobModel( QObject * parent );

	bool dropMimeData ( const QMimeData * data, Qt::DropAction action, int row, int column, const QModelIndex & parent );

	bool isDependencyTreeEnabled() const { return mDependencyTreeEnabled; }
	void setDependencyTreeEnabled( bool );

	QPixmap jobTypeIcon( const JobType & );
public slots:
	void depsAdded(RecordList);
	void depsRemoved(RecordList);

signals:
	void dependencyAdded( const QModelIndex & parent );
	
protected:
	void addRemoveWorker( JobDepList, bool remove );

	bool mDependencyTreeEnabled;
	QMap<JobType,QPixmap> mJobTypeIconMap;
};

ASSFREEZER_EXPORT void setupJobView( RecordTreeView *, IniConfig & );
ASSFREEZER_EXPORT void saveJobView( RecordTreeView *, IniConfig & );
ASSFREEZER_EXPORT void setupHostView( RecordTreeView *, IniConfig & );
ASSFREEZER_EXPORT void saveHostView( RecordTreeView *, IniConfig & );
ASSFREEZER_EXPORT void setupJobErrorView( RecordTreeView *, IniConfig & );
ASSFREEZER_EXPORT void saveJobErrorView( RecordTreeView *, IniConfig & );
ASSFREEZER_EXPORT void setupHostErrorView( RecordTreeView *, IniConfig & );
ASSFREEZER_EXPORT void saveHostErrorView( RecordTreeView *, IniConfig & );
ASSFREEZER_EXPORT void setupErrorView( RecordTreeView *, IniConfig & );
ASSFREEZER_EXPORT void saveErrorView( RecordTreeView *, IniConfig & );
ASSFREEZER_EXPORT void setupFrameView( RecordTreeView *, IniConfig & );
ASSFREEZER_EXPORT void saveFrameView( RecordTreeView *, IniConfig & );

class ASSFREEZER_EXPORT ProgressDelegate : public QItemDelegate
{
Q_OBJECT
public:
	ProgressDelegate( QObject * parent=0 );
	~ProgressDelegate() {}

	virtual void paint ( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const;
protected:
	ColorOption * mStartedColor, * mReadyColor, * mDoneColor, * mSuspendedColor;
};

class ASSFREEZER_EXPORT LoadedDelegate : public QItemDelegate
{
Q_OBJECT
public:
	LoadedDelegate( QObject * parent=0 ) : QItemDelegate( parent ) {}
	~LoadedDelegate() {}

	virtual void paint ( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const;
};

class ASSFREEZER_EXPORT MultiLineDelegate : public QItemDelegate
{
Q_OBJECT
public:
	MultiLineDelegate( QObject * parent=0 ) : QItemDelegate( parent ) {}
	~MultiLineDelegate() {}

	QSize sizeHint(const QStyleOptionViewItem &option,
                              const QModelIndex &index) const;
};

#endif // ITEMS_H

