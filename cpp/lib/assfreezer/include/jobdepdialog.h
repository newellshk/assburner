
#ifndef JOB_DEP_DIALOG_H
#define JOB_DEP_DIALOG_H

#include "job.h"

#include "afcommon.h"
#include "ui_jobdepdialogui.h"

class ASSFREEZER_EXPORT JobDepDialog : public QDialog, public Ui::JobDepDialogUI
{
Q_OBJECT
public:
	JobDepDialog(QWidget * parent = nullptr);
	
	static bool createDeps( const Job & dependent, JobList providers, QWidget * parent=nullptr );
};

#endif // JOB_DEP_DIALOG_H