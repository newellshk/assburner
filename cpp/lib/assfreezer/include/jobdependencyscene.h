
#ifndef JOB_DEPENDENCY_SCENE_H
#define JOB_DEPENDENCY_SCENE_H

#include <qmap.h>

#include <QGVScene.h>

#include "job.h"

class QGVNode;
class JobNode;

class JobDependencyScene : public QGVScene
{
public:
	JobDependencyScene( const QString & name, QObject * parent = 0 );

	void setJobs(const JobList &);

	QGVNode * jobNode(const Job &);

protected:
	JobList mJobList;
	QMap<Job,JobNode*> mJobNodes;
};

#endif // JOB_DEPENDENCY_SCENE_H
