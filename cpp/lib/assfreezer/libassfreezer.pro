
TARGET=assfreezer
TEMPLATE=lib

include($$(PRI_SHARED)/common.pri)
include($$(PRI_SHARED)/python.pri)
include($$(PRI_SHARED)/absubmit.pri)
include($$(PRI_SHARED)/classesui.pri)
include($$(PRI_SHARED)/classes.pri)
include($$(PRI_SHARED)/stonegui.pri)
include($$(PRI_SHARED)/stone.pri)
include($$(PRI_SHARED)/qgv.pri)

DEPENDPATH+=src include ui

#DEFINES += USE_GRAPHVIZ

SOURCES+= \
#	src/abadminplugins.cpp \
	src/afcommon.cpp \
	src/assfreezerview.cpp \
	src/assfreezermenus.cpp \
	src/batchsubmitdialog.cpp \
	src/displayprefsdialog.cpp \
	src/errorlistwidget.cpp \
	src/framenthdialog.cpp \
	src/glwindow.cpp \
	src/graphiteview.cpp \
	src/hosterrorwindow.cpp \
	src/hostlistwidget.cpp \
	src/hostservicematrix.cpp \
	src/imagecache.cpp \
	src/imageview.cpp \
	src/items.cpp \
	src/jobaftereffectssettingswidget.cpp \
	src/jobbatchsettingswidget.cpp \
	src/jobdepdialog.cpp \
	src/jobenvironmentwindow.cpp \
	src/jobfusionsettingswidget.cpp \
	src/jobhistoryview.cpp \
	src/joblistwidget.cpp \
	src/jobmaxsettingswidget.cpp \
	src/jobmaxscriptsettingswidget.cpp \
	src/jobmayasettingswidget.cpp \
	src/jobrealflowsettingswidget.cpp \
	src/jobshakesettingswidget.cpp \
	src/jobsettingswidget.cpp \
	src/jobstatwidget.cpp \
	src/jobsettingswidgetplugin.cpp \
	src/jobxsisettingswidget.cpp \
	src/mainwindow.cpp \
	src/projectweightdialog.cpp \
	src/projectweightview.cpp \
	src/servicestatusview.cpp \
	src/settingsdialog.cpp \
	src/tabtoolbar.cpp \
	src/threadtasks.cpp \
	src/viewmanager.cpp

HEADERS+= \
#	include/abadminplugins.h \
	include/afcommon.h \
	include/assfreezerview.h \
	include/assfreezermenus.h \
	include/batchsubmitdialog.h \
	include/displayprefsdialog.h \
	include/errorlistwidget.h \
	include/framenthdialog.h \
	include/glwindow.h \
	include/graphiteview.h \
	include/hosterrorwindow.h \
	include/hostlistwidget.h \
	include/hostservicematrix.h \
	include/imagecache.h \
	include/imageview.h \
	include/items.h \
	include/jobaftereffectssettingswidget.h \
	include/jobbatchsettingswidget.h \
	include/jobdepdialog.h \
	include/jobenvironmentwindow.h \
	include/jobfusionsettingswidget.h \
	include/jobhistoryview.h \
	include/joblistwidget.h \
	include/jobmaxsettingswidget.h \
	include/jobmaxscriptsettingswidget.h \
	include/jobmayasettingswidget.h \
	include/jobrealflowsettingswidget.h \
	include/jobshakesettingswidget.h \
	include/jobstatwidget.h \
	include/jobsettingswidget.h \
	include/jobsettingswidgetplugin.h \
	include/jobxsisettingswidget.h \
	include/mainwindow.h \
	include/projectweightdialog.h \
	include/projectweightview.h \
	include/servicestatusview.h \
	include/settingsdialog.h \
	include/tabtoolbar.h \
	include/threadtasks.h \
	include/viewmanager.h

contains(DEFINES,USE_GRAPHVIZ) {
	SOURCES += src/jobdependencyscene.cpp
	HEADERS += include/jobdependencyscene.h
}

FORMS += \
	ui/aboutdialog.ui \
	ui/batchsubmitdialogui.ui \
	ui/displayprefsdialogui.ui \
	ui/hostservicematrixwidgetui.ui \
	ui/framenthdialogui.ui \
	ui/jobaftereffectssettingswidgetui.ui \
	ui/jobbatchsettingswidgetui.ui \
	ui/jobdepdialogui.ui \
	ui/jobenvironmentwindowui.ui \
	ui/jobfusionsettingswidgetui.ui \
	ui/jobfusionvideomakersettingswidgetui.ui \
	ui/joblistwidgetui.ui \
	ui/jobmaxscriptsettingswidgetui.ui \
	ui/jobmaxsettingswidgetui.ui \
	ui/jobmayasettingswidgetui.ui \
	ui/jobrealflowsettingswidgetui.ui \
	ui/jobsettingswidgetui.ui \
	ui/jobshakesettingswidgetui.ui \
	ui/jobxsisettingswidgetui.ui \
	ui/projectweightdialogui.ui \
	ui/settingsdialogui.ui

contains(QT_VERSION, ^4\\..*) {
	SOURCES += src/webview.cpp
	HEADERS += include/webview.h
	QT += webkit
}

# In tree include paths
INCLUDEPATH += .out include

win32{
#	INCLUDEPATH+=c:\nvidia\cg\include
#	LIBS+=-Lc:\nvidia\cg\lib -lcgGL -lcg
#	LIBS+=-Lc:\IntelLib
}

macx{
  INCLUDEPATH+=/Developer/SDKs/MacOSX10.4u.sdk/usr/X11R6/include/
}

unix{
#	LIBS+=-lCgGL
	LIBS+=-lGLU
}

DEFINES+=ASSFREEZER_MAKE_DLL
#DEFINES+=USE_IMAGE_MAGICK

contains( DEFINES, USE_IMAGE_MAGICK ) {
	unix:LIBS+=-L/usr/local/ImageMagick/lib -lMagick++ -lMagickWand -lMagickCore 
	unix:INCLUDEPATH+=/usr/local/ImageMagick/include/ImageMagick
	macx:INCLUDEPATH+=/usr/local/include
	macx:LIBS+=-L/usr/local/ImageMagick/lib -lMagick++  -lMagickCore -lMagickWand
}

CONFIG += qt thread opengl
QT+=gui xml sql opengl network widgets

target.path=$$LIB_PREFIX
INSTALLS += target
