
#include "jobdep.h"

#include "jobdepdialog.h"

JobDepDialog::JobDepDialog(QWidget * parent)
: QDialog(parent)
{
	setupUi(this);
	
}

bool JobDepDialog::createDeps( const Job & dependent, JobList providers, QWidget * parent )
{
	JobDepDialog jdd(parent);
	const bool isSuspended = dependent.status() == "suspended";
	jdd.mUnsuspendCheck->setVisible( isSuspended );
	jdd.mUnsuspendCheck->setChecked( isSuspended );
	
	if( jdd.exec() == QDialog::Accepted ) {
		JobDep::Type depType = jdd.mPerJobRadio->isChecked() ? JobDep::PerJob : JobDep::PerTask;
		foreach( Job pdep, providers ) {
			JobDep dep;
			dep.setJob( dependent );
			dep.setDep( pdep );
			dep.setDepType( depType );
			dep.commit();
		}
		if( isSuspended && jdd.mUnsuspendCheck->isChecked() )
			Job::resumeJobs( dependent );
		return true;
	}
	return false;
}

