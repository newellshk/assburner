
#include <qaction.h>
#include <qgraphicsview.h>
#include <qmenu.h>
#include <QGraphicsSceneContextMenuEvent>

#include <QGVNode.h>
#include <QGVEdge.h>

#include "jobdep.h"
#include "jobtype.h"

#include "jobdependencyscene.h"

class JobNode : public QGVNode
{
public:
	JobNode(const Job & job, QGVScene *scene)
	: QGVNode(scene)
	, mJob(job)
	{
		setLabel( mJob.name() );
	}
	
    void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget)
	{
		QGVNode::paint(painter,option,widget);
		painter->save();
		painter->restore();
	}
	
	Job mJob;
};

class JobDepEdge : public QGVEdge
{
public:
	JobDepEdge(const JobDep & jobDep, QGVNode * source, QGVNode * target)
	: QGVEdge(source,target)
	, mJobDep(jobDep)
	{}
	
	void setupAction(QAction * action,JobDep::Type type)
	{
		action->setCheckable(true);
		action->setChecked(type == mJobDep.depType());
		action->setData(type);
	}
	
	virtual void contextMenuEvent(QGraphicsSceneContextMenuEvent * event)
	{
		QMenu menu;
		setupAction(menu.addAction( "Per Job Dependency" ),JobDep::PerJob);
		setupAction(menu.addAction( "Per Task Dependency" ),JobDep::PerTask);
		QAction * ret = menu.exec(event->screenPos());
		if( ret ) {
			mJobDep.setDepType(ret->data().toInt()).commit();
			update();
		}
	}
	
	virtual QPen pen() const
	{
		QPen p(QGVEdge::pen());
		p.setStyle( mJobDep.depType() == 1 ? Qt::SolidLine : Qt::DotLine );
		if( isSelected() )
			p.setColor( Qt::red );
		return p;
	}
	
	JobDep mJobDep;
};

JobDependencyScene::JobDependencyScene( const QString & name, QObject * parent )
: QGVScene( name, parent )
{}

void JobDependencyScene::setJobs(const JobList & jobs)
{
	mJobList = jobs;
	mJobNodes.clear();
	clear();
	
	if( jobs.isEmpty() ) return;
	
	JobDepList deps = JobDep::table()->selectFrom( QString("jobdep_recursive('%1') AS JobDep").arg(jobs.keyString()) );

	setGraphAttribute("label", "DEMO");
	setGraphAttribute("splines", "ortho");
	setGraphAttribute("rankdir", "LR");
	setGraphAttribute("nodesep", "0.4");
	//setGraphAttribute("width", "10");
	//setGraphAttribute("height", "4");
	QList<QGraphicsView*> _views = views();
	QGraphicsView * view = _views.size() ? _views[0] : 0;
	if( view )
		setGraphAttribute("ratio", QString::number(view->height() / float(view->width())).toUtf8().constData());
	
	setNodeAttribute("shape", "box");
	setNodeAttribute("style", "filled");
	setNodeAttribute("fillcolor", "white");
	setNodeAttribute("height", "1.2");
	setEdgeAttribute("minlen", "3");

	// Ensure all selected jobs have a node, even if they have no dependencies
	foreach( Job j, jobs )
		jobNode(j);
	
	foreach( JobDep dep, deps ) {
		JobDepEdge * edge = new JobDepEdge(dep, jobNode(dep.job()), jobNode(dep.dep()) );
		if( dep.depType() == 2 )
			edge->setAttribute("style", "dashed" );
	}

	applyLayout();
}

QGVNode * JobDependencyScene::jobNode(const Job & job)
{
	// Find and return
	QMap<Job,JobNode*>::iterator it = mJobNodes.find(job);
	if( it != mJobNodes.end() ) return it.value();
	
	// Create new node
	JobNode * node = new JobNode(job, this);
	node->setIcon( job.jobType().icon() );
	
	// Distinguish selected jobs
	if( mJobList.contains(job) ) {
		node->setAttribute("fillcolor", "#CCCCCC");
		node->setAttribute("peripheries", "1");
	}
	
	mJobNodes[job] = node;
	return node;
}

