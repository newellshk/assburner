
/* $Author$
 * $LastChangedDate$
 * $Rev$
 * $HeadURL$
 */

#include <qsqlquery.h>
#include <qsqlerror.h>

#include "database.h"

#include "employee.h"
#include "group.h"
#include "hostservice.h"
#include "jobassignmentstatus.h"
#include "jobstatus.h"
#include "permission.h"
#include "usergroup.h"

#include "threadtasks.h"
#include "hostselector.h"

#define CHECK_CANCEL if( mCancel ) return;

static bool staticJobListDataRetrieved = false;

StaticJobListDataTask::StaticJobListDataTask( QObject * rec )
: ThreadTask( STATIC_JOB_LIST_DATA, rec )
, mHasData( false )
{}

void StaticJobListDataTask::run()
{
	if( !staticJobListDataRetrieved ) {
		mJobTypes = JobType::select();
		CHECK_CANCEL
		mProjects = Project::select( "WHERE lastjobsubmitted > now() - '180 days'::interval" );
		CHECK_CANCEL
		// Ensure host table is preloaded
		Host::select();
		CHECK_CANCEL
		mServices = Service::select( "WHERE keyService IN (SELECT fkeyService FROM JobService)" );
		mHasData = staticJobListDataRetrieved = true;
		// Cache information needed for permission checks
		// Permission checks will happen when building menus
		Employee::select();
		Group::select();
		Permission::select();
		UserGroup::recordsByUser( User::currentUser() );
	}
}

StaticHostListDataTask::StaticHostListDataTask( QObject * rec )
: ThreadTask( STATIC_HOST_LIST_DATA, rec )
{}

void StaticHostListDataTask::run()
{
	mServices = Service::select( "WHERE keyService IN (SELECT fkeyService FROM JobService)" );
	Employee::select();
}

JobListTask::JobListTask( QObject * rec, const JobFilter & jf, const JobList & jobList, ProjectList projects, ServiceList activeServices, bool fetchJobServices, bool needDeps )
: ThreadTask( JOB_LIST, rec )
, mJobFilter( jf )
, mJobList( jobList )
, mProjects( projects )
, mFetchJobServices( fetchJobServices )
, mFetchJobDeps( needDeps )
, mActiveServices( activeServices )
{
	
}

void JobListTask::run()
{
//	User::select(); // Make sure usr is loaded
//	CHECK_CANCEL

	bool projectFilterShowingNone = (!mJobFilter.allProjectsShown && mJobFilter.visibleProjects.isEmpty() && !mJobFilter.showNonProjectJobs);

	if( mJobList.size() ) {
		mReturn = mJobList.reloaded();
	} else
	// If we have a project list, and all projects are hidden, then there will be no items, so just clear the list
	if( !(projectFilterShowingNone || mJobFilter.typesToShow.isEmpty() || mJobFilter.statusToShow.isEmpty() || mJobFilter.servicesToShow.isEmpty()) )
	{
		Expression e;
		
		if( mJobFilter.statusToShow.size() > 0 && mJobFilter.statusToShow.size() != 9 )
			e = Job::c.Status.in(mJobFilter.statusToShow);

		if( mJobFilter.userList.size() > 0 )
			e &= Job::c.User.in(mJobFilter.userList);
		
		if( (uint)mJobFilter.servicesToShow.size() < mActiveServices.size() )
			e &= Job::c.Key.in( JobService::c.Service.in(mJobFilter.servicesToShow) );

		if( mJobFilter.allProjectsShown && !mJobFilter.showNonProjectJobs )
			e &= Job::c.Project.isNotNull() & (Job::c.Project != ev_(0));
		
		else if( !mJobFilter.allProjectsShown ) {
			QList<uint> projectKeys = mJobFilter.visibleProjects;
			Expression sub;
			if( mJobFilter.showNonProjectJobs ) {
				projectKeys.append( 0 );
				sub = Job::c.Project.isNull();
			}
			if( projectKeys.size() )
				e &= (Job::c.Project.in(projectKeys) || sub);
		}
		
		if( !mJobFilter.versionedFilter.isNull() )
			// ((length(publishInfo -> 'version) > 0) == boolean)
			e &= (Job::c.PublishInfo[ev_("version")].length() > ev_(0)).coalesce(ev_(false)) == mJobFilter.versionedFilter.toBool();
		
		if( mJobFilter.elementList.size() > 0 )
			e &= Job::c.Element.in(mJobFilter.elementList);

		if( mJobFilter.mExtraFilters.isValid() )
			e &= mJobFilter.mExtraFilters;

		e = e.orderBy( Job::c.Key, Expression::Descending ).limit(mJobFilter.mLimit);

		bool supportsMultiSelect = Database::current()->connection()->capabilities() & Connection::Cap_MultiTableSelect;
		TableList tables;

		CHECK_CANCEL
		foreach( uint type, mJobFilter.typesToShow ) {
			JobType jt( type );
			
			/* Find the actual storage table */
			while( jt.useParentTable() )
				jt = jt.parentJobType();
			
			if( !jt.isRecord() ) {
				LOG_5( "JobType filter was not a valid record: " + QString::number(type) );
				continue;
			}
			
			Table * tbl = Database::current()->tableByName( "job" + jt.name() );
			if( !tbl ) {
				LOG_5( "Couldn't find table for jobtype: " + jt.name() );
				continue;
			}
			
			if( tables.contains(tbl) )
				continue;
			tables += tbl;
			
			if( !supportsMultiSelect )
				mReturn += tbl->selectOnly( e );
			CHECK_CANCEL
		}
		if( supportsMultiSelect )
			mReturn = Job::table()->selectMulti( tables, e );
		CHECK_CANCEL
	}

	JobList jobsNeedingRefresh;
	if( mFetchJobDeps && mReturn.size() ) {
		JobList jobsNeedingDeps = mReturn;
		Index * idx = JobDep::table()->indexFromField( JobDep::c.Job );
		mJobDeps = JobDep::table()->selectFrom( "jobdep_recursive('" + jobsNeedingDeps.keyString() + "') AS JobDep" );
		// Because the function is recursive, we need to clear all existing entries for both jobsNeedingDeps
		// and any other jobs pointed to by the returned JobDep.fkeyJob
		idx->setEntries( (jobsNeedingDeps | mJobDeps.jobs()).getValue( Job::c.Key ), mJobDeps );
		mDependentJobs = mJobDeps.deps();
		jobsNeedingRefresh = (mDependentJobs - mReturn);
		jobsNeedingRefresh.reload();
		CHECK_CANCEL
	}

	JobList allJobs = mReturn + jobsNeedingRefresh;
	if( allJobs.size() ) {
		allJobs.jobStatuses(Index::UseSelect); // Always does a full select
		CHECK_CANCEL

		if( mFetchJobServices )
			mJobServices = allJobs.jobServices(Index::UseSelect);
	}
}

HostListTask::HostListTask( QObject * rec, ServiceList serviceFilter, ServiceList activeServices, const Expression & extraFilters, bool onlineOnly, bool loadHostServices, bool loadHostInterfaces )
: ThreadTask( HOST_LIST, rec )
, mServiceFilter( serviceFilter )
, mActiveServices( activeServices )
, mExtraFilters( extraFilters )
, mOnlineOnly( onlineOnly )
, mLoadHostServices( loadHostServices )
, mLoadHostInterfaces( loadHostInterfaces )
{
}


void HostListTask::run()
{
	static JobAssignmentStatusList jobAssignmentStatuses = JobAssignmentStatus::c.Status.in( QStringList() << "ready" << "copy" << "busy" ).select();
	if( !mServiceFilter.isEmpty() ) {
		Expression e;
		if( mServiceFilter.size() < mActiveServices.size() )
			e = Host::c.Key.in( Query( HostService::c.Host, HostService::c.Service.in(mServiceFilter) ) );
		if( mOnlineOnly )
			e &= Host::c.Online == ev_(1);
		if( mExtraFilters.isValid() )
			e &= mExtraFilters;
		mReturn = Host::select(e);
		if( mReturn.size() ) {
			CHECK_CANCEL
			mHostStatuses = mReturn.hostStatuses(Index::UseSelect);
			CHECK_CANCEL
			
			mJobAssignments = jobAssignmentsByHostExp()(QVariant::fromValue<RecordList>(mReturn),Index::UseSelect);
			jobTaskAssignmentsByJobAssignments(mJobAssignments,Index::UseSelect);
			CHECK_CANCEL
			// We only display the job name in the host view, so cached is fine since job names are generally immutable
			mJobs = mJobAssignments.jobs();
			// A jobs project is also generally immutable
			mProjects = mJobs.projects();
			// Usernames are also immutable
			mUsers = mReturn.users();
		}
	}
	if( mLoadHostServices ) {
		CHECK_CANCEL
		// mReturn.hostServices();
		HostService::select();
	}
	if( mLoadHostInterfaces ) {
		CHECK_CANCEL
		mHostInterfaces = mReturn.hostInterfaces();
	}
}

FrameListTask::FrameListTask( QObject * rec, const Job & job )
: ThreadTask( FRAME_LIST, rec )
, mJob( job )
{}

void FrameListTask::run()
{
	mReturn = mJob.jobTasks(Index::UseSelect);
	CHECK_CANCEL
	mTaskAssignments = mReturn.jobTaskAssignments();
	mJobAssignments = mTaskAssignments.jobAssignments();
	//CHECK_CANCEL
	mOutputs = mJob.jobOutputs(Index::UseSelect);
	CHECK_CANCEL
	mFileTrackers = mOutputs.fileTrackers(Index::UseSelect);
	CHECK_CANCEL
	QSqlQuery q = Database::current()->exec( "SELECT now();" );
	if( q.next() )
		mCurTime = q.value( 0 ).toDateTime();
}

PartialFrameListTask::PartialFrameListTask( QObject * rec, const JobTaskList & jtl )
: ThreadTask( PARTIAL_FRAME_LIST, rec )
, mJtl( jtl )
{}

void PartialFrameListTask::run()
{
	if( mJtl.size() == 0 )
		return;
	mReturn = mJtl.reloaded();
	CHECK_CANCEL
	QSqlQuery q = Database::current()->exec( "SELECT now();" );
	if( q.next() )
		mCurTime = q.value( 0 ).toDateTime();
}

ErrorListTask::ErrorListTask( QObject * rec, const Job & job, bool fetchClearedErrors )
: ThreadTask( ERROR_LIST, rec )
, mFetchJobs(false)
, mFetchServices(false)
, mFetchClearedErrors( fetchClearedErrors )
, mLimit( 0 )
, mJobFilter(job)
{}

ErrorListTask::ErrorListTask( QObject * rec, const Host & host, int limit, bool showCleared )
: ThreadTask( ERROR_LIST, rec )
, mFetchJobs(true)
, mFetchServices(true)
, mFetchClearedErrors( showCleared )
, mLimit( limit )
, mHostFilter(host)
{}

ErrorListTask::ErrorListTask( QObject * rec, JobList jobFilter, HostList hostFilter, ServiceList serviceFilter, JobTypeList jobTypeFilter, const QString & messageFilter, bool showServices, int limit, const Expression & extraFilters )
: ThreadTask( ERROR_LIST, rec )
, mFetchJobs(true)
, mFetchServices(showServices)
, mFetchClearedErrors(true)
, mLimit( limit )
, mHostFilter(hostFilter)
, mJobFilter(jobFilter)
, mServiceFilter(serviceFilter)
, mJobTypeFilter(jobTypeFilter)
, mMessageFilter(messageFilter)
, mExtraFilters(extraFilters)
{}

void ErrorListTask::run()
{
	Expression e;
	if( mJobFilter.size() )
		e &= JobError::c.Job.in(mJobFilter); // fkeyJob IN (*mJobFilter*)
	if( mHostFilter.size() )
		e &= JobError::c.Host.in(mHostFilter); // fkeyHost IN (*mHostFilter*)
	if( !mFetchClearedErrors )
		e &= (JobError::c.Cleared == ev_(false)) || JobError::c.Cleared.isNull(); // (cleared = false OR cleared IS NULL)
	if( mServiceFilter.size() )
		e &= JobError::c.Job.in( Query( JobService::c.Job, JobService::c.Service.in(mServiceFilter) ) ); // fkeyJob IN (SELECT fkeyjob FROM JobService WHERE fkeyService IN (*mServiceFilter*))
	if( mJobTypeFilter.size() )
		e &= JobError::c.Job.in( Query( Job::c.Key, Job::c.JobType.in(mJobTypeFilter) ) ); // fkeyJob IN (SELECT keyJob FROM Job WHERE fkeyJobType IN (*jobTypeFilter*))
	if( mExtraFilters.isValid() )
		e &= mExtraFilters;
	if( mLimit ) {
		// If there's a limit fetch the most recent errors
		e = e.orderBy(JobError::c.LastOccurrence);
		e = e.limit(mLimit);
	}
	mReturn = JobError::select(e);
	if( mFetchJobs )
		mJobs = mReturn.jobs(Index::UseSelect);
	if( mFetchServices )
		mJobServices = JobService::c.Job.in(mReturn).select();
}

CounterTask::CounterTask( QObject * rec )
: ThreadTask( COUNTER, rec )
{}

void CounterTask::run()
{
	QSqlQuery query = Database::current()->exec("SELECT hostsTotal, hostsActive, hostsReady, jobsTotal, jobsActive, jobsDone FROM getcounterstate();");
	if( query.next() ) {
		mReturn.hostsTotal = query.value(0).toInt();
		mReturn.hostsActive = query.value(1).toInt();
		mReturn.hostsReady = query.value(2).toInt();
		mReturn.jobsTotal = query.value(3).toInt();
		mReturn.jobsActive = query.value(4).toInt();
		mReturn.jobsDone = query.value(5).toInt();
	}
	CHECK_CANCEL
	mManagerService = Service("AB_manager");
}


JobHistoryListTask::JobHistoryListTask( QObject * rec, JobList jobs )
: ThreadTask( JOB_HISTORY_LIST, rec )
, mJobs( jobs )
{
}

void JobHistoryListTask::run()
{
	mReturn = mJobs.jobHistories();
}
