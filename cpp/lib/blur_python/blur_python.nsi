

; DESCRIPTION: Blur Python Modules installer script
; (C) Blur Studio 2005-2011

!include blur_python-svnrev.nsi
!define MUI_PRODUCT "Blur Python Modules"
!define MUI_VERSION "v1.0.${MUI_SVNREV}"

Name "${MUI_PRODUCT} ${MUI_VERSION} ${PLATFORM}"

!include "MUI.nsh"
!include FileFunc.nsh	; Provide GetOptions and GetParameters
!include "..\..\..\nsis\ReplaceSubStr.nsh"
!include "..\..\..\nsis\GetParent.nsh"
!include "..\..\..\nsis\LastInstall.nsh"
!include "..\..\..\nsis\SoftwareINI.nsh"

!if ${PLATFORM} == "win32-msvc2005_64"
	!define _64_BIT
!endif
!if ${PLATFORM} == "win32-msvc2008_64"
	!define _64_BIT
!endif
!if ${PLATFORM} == "win32-msvc2010_64"
	!define _64_BIT
!endif
!if ${PLATFORM} == "win32-msvc2012_64"
	!define _64_BIT
!endif

; Define the bit name used for software.ini
!ifdef _64_BIT
	!define BIT_NAME ' x64'
!endif
!ifndef _64_BIT
	!define BIT_NAME ''
!endif

!define PYQT_PYTHON_HKLM    "Software\Python\PythonCore\${PYTHON_VERSION}\InstallPath"

; Name of resulting executable installer
!define OUT_FILE "blur_python_install_${MUI_SVNREV}_python${PYTHON_VERSION}_${PLATFORM}.exe"
OutFile "${OUT_FILE}"
!define ORIG_INSTALL_DIR "${PYTHON_PATH}\Lib\site-packages"
InstallDir "${PYTHON_PATH}\Lib\site-packages"

!define MUI_FINISHPAGE
!define MUI_FINISHPAGE_NOREBOOTSUPPORT
!define MUI_HEADERBITMAP "assfreezer.bmp"

!define MUI_FINISHPAGE_TITLE "Blur Python Installation Complete"
!define MUI_FINISHPAGE_TEXT "\n\r\nClick Finish now to close the installation program."

#SilentInstall silent

!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES ; File installation page
#!insertmacro MUI_PAGE_FINISH

ShowInstDetails show

Function .onInit

!ifdef _64_BIT
	SetRegView 64
!endif

	# If $INSTDIR has been overridden at the cmd line with /D=, then skip setting it to the python install path
	StrCmpS "${ORIG_INSTALL_DIR}" $INSTDIR +2 0
	Return

	ReadRegStr $0 HKLM "${PYQT_PYTHON_HKLM}" ""
	${If} $0 != ""
		# This registry key has a extra slash
		StrCpy $INSTDIR "$0Lib\site-packages\"
    ${Endif}
	
FunctionEnd

Section "install"
	# nuke old assfreezer installs, if any
	SetOutPath $INSTDIR\blur

	# Libs
	File ..\stone\stone.dll
	File ..\classes\classes.dll
	File ..\stonegui\stonegui.dll
	File ..\classesui\classesui.dll
	File ..\assfreezer\assfreezer.dll
	File ..\absubmit\absubmit.dll
	File ..\ctrax\ctrax.dll
	File ..\qjson\qjson4.dll
	File blur_python_version.txt

	# Python Modules
	File ..\stone\sipStone\_Stone.pyd
	File ..\classes\sipClasses\_Classes.pyd
	File ..\stonegui\sipStonegui\_Stonegui.pyd
	File ..\classesui\sipClassesui\_Classesui.pyd
	File ..\absubmit\sipAbsubmit\_absubmit.pyd
	File ..\assfreezer\sipAssfreezer\Assfreezer.pyd
	File ..\ctrax\sipCtrax\Ctrax.pyd

	Delete Stone.pyd
	Delete Classes.pyd
	Delete Stonegui.pyd
	Delete Classesui.pyd
	Delete absubmit.pyd

	SetOutPath $INSTDIR\PyQt4
	File ..\qjson\sipQtJson\QtJson.pyd

	SetOutPath $INSTDIR
	File /r "..\..\..\python\blur"

	SetOutPath $INSTDIR\blur\Classes
	File ..\classes\ng_schema.xml

	# Register the uninstaler only if a cmd line arg was not passed in
	ClearErrors
	${GetParameters} $R0
	${GetOptions} $R0 "/noUninstallerReg" $R1
	
	IfErrors 0 makeInstaller
		!ifdef _64_BIT
			SetRegView 64
		!endif
		# Only need to show the Python folder not \Lib\site-packages
		Push $INSTDIR
		Call GetParent
		Call GetParent
		Pop $R9
		# Can't store \'s as a registry key so replace with /
		!insertmacro ReplaceSubStr $R9 "\" "/"
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}$MODIFIED_STR" "DisplayName" "${MUI_PRODUCT} $R9 (remove only)"
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}$MODIFIED_STR" "UninstallString" "$INSTDIR\blur\pyqt_uninstall.exe"
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}$MODIFIED_STR" "Publisher" "Blur Studio"
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}$MODIFIED_STR" "DisplayVersion" "${MUI_VERSION}"
	makeInstaller:
		WriteUninstaller "$INSTDIR\blur\pyqt_uninstall.exe"
		!insertmacro SetLastInstall "$INSTDIR\blur\pyqt_uninstall.exe"
	# Update software.ini so we can track what software is installed on each system
	!insertmacro UpdateSettingsINI ${BLUR_SOFTWARE_INI} "Blur Python${PYTHON_VERSION}${BIT_NAME}" "${MUI_VERSION}" "${OUT_FILE}"
SectionEnd

Section "Uninstall"
	;Delete Files and directory
	RMDir /r $INSTDIR

	;Delete Uninstaller And Unistall Registry Entries
	Push $INSTDIR
	Call un.GetParent
	Call un.GetParent
	Call un.GetParent
	Pop $R9
	# Can't store \'s as a registry key so replace with /
	!insertmacro un.ReplaceSubStr $R9 "\" "/"
	!ifdef _64_BIT
		SetRegView 64
	!endif
	DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\${MUI_PRODUCT}"
	DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}$MODIFIED_STR"  
SectionEnd
