
#ifdef HEADER_FILES
#endif

#ifdef CLASS_FUNCTIONS
	// Returns a ContractList containing all contracts for a user
	// If you pass True to isSigned, it will filter the list to only
	// contracts the user has signed. If you pass False, it will filter
	// the list to only contracts you haven't signed. If you pass nothing
	// it will return all contracts for you.
	static ContractList forUser( User & user, QVariant isSigned=QVariant() );
#endif

