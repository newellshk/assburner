
#ifndef COMMIT_CODE
#include "delivery.h"
#include "deliverybreakdown.h"
#include <qdebug.h>

DeliveryProblemList Delivery::problems(bool includeBreakdowns) const {
	uint out = problemFlags();
	if (includeBreakdowns) {
		foreach( DeliveryBreakdown bdown, deliveryBreakdowns() ) {
			out |= bdown.problemFlags();
		}
	}
	return DeliveryProblem::recordsByIndex( out  );
}

void Delivery::setProblems(DeliveryProblemList records) {
	uint out = 0;
	foreach(DeliveryProblem dp, records)
		out |= dp.key();
	setProblemFlags( out );
}

#endif