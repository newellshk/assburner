
#ifndef COMMIT_CODE
#include "deliverybreakdown.h"
#include <qdebug.h>

DeliveryProblemList DeliveryBreakdown::problems() const {
	return DeliveryProblem::recordsByIndex(problemFlags());
}

void DeliveryBreakdown::setProblems(DeliveryProblemList records) {
	uint out = 0;
	foreach(DeliveryProblem dp, records)
		out |= dp.key();
	setProblemFlags( out );
}

#endif