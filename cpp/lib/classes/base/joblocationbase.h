
#ifdef CLASS_FUNCTIONS
	enum SyncState {
		SyncNotStarted = 0,
		SyncStarted = 1,
		SyncCompleted = 2,
		SyncErrored = 3,
		SyncDeleted = 4,
		  // Transition status until the daemon deletes the files and sets it to SyncDeleted
		SyncDiscarded = 5
	};
#endif
 
