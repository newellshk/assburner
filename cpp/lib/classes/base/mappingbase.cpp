/*
 *
 * Copyright 2003, 2004 Blur Studio Inc.
 *
 * This file is part of the Resin software package.
 *
 * Resin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Resin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Resin; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifndef COMMIT_CODE

#include "pyembed.h"

#include <qfileinfo.h>
#include <qfile.h>
#include <qdir.h>
#include <qprocess.h>
#include <qsqlquery.h>
#include <limits>

#include "blurqt.h"
#include "database.h"
#include "connection.h"

#include "host.h"
#include "hostload.h"
#include "hostmapping.h"
#include "jobtype.h"
#include "jobtypemapping.h"
#include "location.h"
#include "mapping.h"
#include "mappingtype.h"
#include "path.h"

bool Mapping::map( bool forceUnmount, QString * errMsg, QString * infoOut )
{
	bool isLoadBalanced = false;
	Host h = host();
	HostMapping hm;

	if( h.isRecord() ) {
		// Reload host to verify it is online/offline.  If we are not connected to the database
		// then we will skip this check.
		if( Database::current()->connection()->isConnected() )
			h.reload();
	}
	// If there is a group of hosts, pick the one with the lowest load
	else { // !h.isRecord()
		isLoadBalanced = true;
		Location myLocation = Host::currentHost().location();
		Expression hostMappings = HostMapping::c.Mapping == *this, online = Host::c.Online == 1;
		
		HostList hl;
		// If we have a location first check for matching mappings
		if( myLocation.isRecord() )
			hl = Host::select( online && Host::c.Key.in(hostMappings && HostMapping::c.Location == myLocation) );
		// otherwise use any mappings without a location
		if( hl.isEmpty() )
			hl = Host::select( online && Host::c.Key.in(hostMappings && HostMapping::c.Location.isNull()) );
		
		LOG_5( "Got " + QString::number( hl.size() ) + " hosts for this mapping" );
		double bestLoad = 0.0;
		foreach( Host hit, hl ) {
			HostLoad hl = hit.hostLoad();
			double computedAvg = hl.loadAvg() + hl.loadAvgAdjust();
			LOG_5( "Checking host " + hit.name() + "  Load: " + QString::number( computedAvg ) + " Online: " + QString::number(hit.online()) );
			if( !h.isRecord() || computedAvg < bestLoad ) {
				bestLoad = computedAvg;
				h = hit;
			}
		}
		hm = HostMapping::select( HostMapping::c.Host == h && HostMapping::c.Mapping == *this )[0];
	}

	if( !h.isRecord() || !h.online() ) {
		if( errMsg ) *errMsg = "Unable to find online host for mapping.  keyMapping: " + QString::number( key() );
		return false;
	}

	bool ret = false;
#ifdef Q_OS_WIN

	if( mappingType().name() == "smb" ) {
		QString unc, err, userName, password;
		unc += "\\\\";
		unc += h.name();
		unc += "\\";
		unc += share();
		userName = hm.userName();
		if( !userName.isEmpty() ) {
			if( !hm.domain().isEmpty() )
				userName = hm.domain() + "\\" + userName;
			password = hm.password();
		}
		ret = mapDriveAsUser( mount()[0].toLatin1(), unc, userName, password, forceUnmount, &err );
		if( infoOut ) {
			QString info = "#" + QString::number(key()) + " " + unc + " on " + mount();
			if( hm.isRecord() )
				info += " HostMapping:" + QString::number(hm.key());
			if( !userName.isEmpty() )
				info += " User: " + userName;
			*infoOut = info;
		}
		if( !ret && !forceUnmount && err.contains("The local device name has a remembered connection to another network resource.") )
			ret = mapDriveAsUser( mount()[0].toLatin1(), unc, userName, password, true, &err );
		if( !ret && errMsg )
			*errMsg = "Unable to map share: " + unc + " to drive: " + mount() + " Error was:" + err;
		else
			LOG_3( "Smb share mapped: " + unc + " to drive: " + mount() );
	}
#else
	Q_UNUSED(infoOut);
	Q_UNUSED(forceUnmount);

	if( mappingType().name() == "nfs" ) {
		QString cmd;
		cmd += "mount -t nfs ";
		cmd += h.name();
		cmd += ":/";
		cmd += share() + " ";
		cmd += mount();
		QProcess p;
		p.start( cmd );
		ret = p.exitCode() == 0;
	}

#endif // Q_OS_WIN

	// Update loadavgadjust, for better load balancing
	if( ret && isLoadBalanced )
		Database::current()->exec("SELECT * FROM increment_loadavgadjust(" + QString::number(h.key()) + ")");

	return ret;
}

bool Mapping::isMirrorSyncPath( const QString & fullPath )
{
	// Keep this in python so we can modify it quickly without recompile
	return runPythonFunction( "blur.mappingutil", "isMirrorSyncPath", VarList() << QVariant(fullPath) ).toBool();
}

bool Mapping::isMappedPath( const JobType & jobType, const QString & path )
{
	QString drive = path[0].toLower();
	MappingList mappings = jobType.jobTypeMappings().mappings();
	foreach( Mapping mapping, mappings ) {
		if( mapping.mount() == drive ) {
			if( !mapping.host().isRecord() )
				return isMirrorSyncPath( path );
			return true;
		}
	}
	return false;
}

static QPair<QString,QString> parseUnc(const QString path, int * retUncEnd=0)
{
	const int pathLen = path.size();
	int uncMid = path.indexOf("\\",3); // Start looking after \\ and first host name char
	
	if (uncMid < 0 || uncMid > pathLen-2) {
		LOG_3( "Invalid UNC Path: " + path );
		return qMakePair<QString,QString>(QString(),QString());
	}
	
	int uncShareStart = uncMid+1;
	// Advance past all \'s
	while (uncShareStart+1 < pathLen && path[uncShareStart+1] == '\\')
		uncShareStart++;
	
	// Need to have found a middle \ and for there to be chars left after all \'s
	if (uncShareStart > pathLen-1) {
		LOG_3( "Invalid UNC Path: " + path );
		return qMakePair<QString,QString>(QString(),QString());
	}
	
	int uncEnd = path.indexOf("\\",uncShareStart+1); // Start looking after first share name char
	if (uncEnd < 0)
		uncEnd = pathLen;
	if (retUncEnd)
		*retUncEnd = uncEnd;
	
	QString hostName = path.mid(2,uncMid-2);
	QString shareName = path.mid(uncShareStart,uncEnd-uncShareStart);
	return qMakePair<QString,QString>(hostName,shareName);
}

QString Mapping::osPath( const QString & path, OSPathType type )
{
	const int pathLen = path.size();
	const bool isUnixPath = path.startsWith("/");
	const bool isWinDrivePath = pathLen >= 2 && path[1] == ':';
	// Shortest UNC path would be \\a\b, 5 chars
	const bool isUncPath = pathLen >= 5 && path.left(2) == "\\\\" && path[3] != '\\';
	
	if( type == OSPath_CurrentOS ) {
#ifdef Q_OS_WIN
		type = OSPath_Windows;
#else
		type = OSPath_Unix;
#endif
	}
	
	// Check if we can avoid the mapping lookup and conversion
	if( 	(isUnixPath 	&& type == OSPath_Unix)
		||  (isWinDrivePath && (type == OSPath_Windows || type == OSPath_DriveLetter))
		||  (isUncPath 		&& (type == OSPath_Windows || type == OSPath_UNC)) )
		return path;
	
	MappingList ml;
	Mapping m;
	QString relPath;
	
	if( isUnixPath ) {
		const bool isBlurSymLink = path.startsWith("/blur/");
		if (isBlurSymLink) {
			QChar driveLetter = path[6].toLower();
			ml = Mapping::select().filter(Mapping::c.Mount == driveLetter);
			// skip /blur/x
			relPath = path.mid(8);
		} else {
			QString spath(path);
			spath.replace("//","/");
			ml = Mapping::select().filter(ev_(spath).ilike(Mapping::c.MountPath + "%"));
		}
		
		if( ml.size() > 1 ) {
			MappingList ml2 = ml.filter( Mapping::c.UserDefault == true );
			if( ml2.isEmpty() || ml2.size() > 1 ) {
				LOG_1( "Unable to determine which mapping to use for path: " + path );
				LOG_1( "Options are: " + ml.debug() );
				return path;
			}
			m = ml2[0];
		} else if( ml.isEmpty() ) {
			LOG_1( "No mapping entry found for path: " + path );
			return path;
		}
		
		m = ml[0];
		if (!isBlurSymLink)
			relPath = path.mid(m.mountPath().size());
	}
	else if( isWinDrivePath ) {
		ml = Mapping::select().filter(Mapping::c.Mount == path[0].toLower() && Mapping::c.UserDefault);
		if( ml.size() == 0 ) {
			LOG_3( "No mapping entry found for path: " + path );
			return path;
		}
		if( ml.size() > 1 )
			LOG_3( "Multiple mapping entries found for path: " + path );
		m = ml[0];
		relPath = path.mid(3);
	}
	else if( isUncPath ) {
		int uncEnd;
		QPair<QString,QString> uncPair = parseUnc(path,&uncEnd);
		QString hostName = uncPair.first;
		QString shareName = uncPair.second;
		if( hostName.isNull() )
			return path;

		relPath = path.mid(uncEnd);
		Host h = Host::recordByName(hostName);
		
		// First try to match host/share directly
		if( h.isRecord() )
			ml = Mapping::select().filter(Mapping::c.Host == h && Mapping::c.Share == shareName);
		
		// Otherwise just try to match share name
		if( ml.size() == 0 ) {
			LOG_5( "Unable to find direct match for unc path: " + path );
			ml = Mapping::select().filter(Mapping::c.Share == shareName);
		}

		if( ml.size() > 1 ) {
			MappingList ml2 = ml.filter( Mapping::c.UserDefault == true );
			if( ml2.size() == 0 || ml2.size() > 1 ) {
				LOG_1( "Unable to determine which mapping to use for path: " + path );
				LOG_1( "Options are: " + ml.debug() );
				return path;
			}
			m = ml2[0];
		}
		else if( ml.size() )
			m = ml[0];
		else {
			LOG_1( "Unable to find mapping entry for unc path: " + path );
			return path;
		}
	}
	
	if( type == OSPath_Windows || type == OSPath_DriveLetter ) {
		// Lookup current unc path for the driveletter
		QString curDriveMapping = driveMapping(m.mount()[0].toLatin1());
		if( !curDriveMapping.isNull() || type == OSPath_DriveLetter ) {
			// TODO: Check share name and maybe host, add options to this function
			return m.mount() + ":\\" + relPath.replace("/","\\");
		}
	}
	
	if( type == OSPath_Windows || type == OSPath_UNC )
		return "\\\\" + m.host().name() + "\\" + m.share() + "\\" + relPath.replace("/","\\");

	// Unix Output follows
	
	// Use /blur/DRIVELETTER if available
	QString driveLetterMount = "/blur/" + m.mount().toLower();
	if (QDir(driveLetterMount).exists())
		return driveLetterMount + "/" + relPath.replace("\\","/");
	
	// Else use standard /mnt/SHARENAME
	return m.mountPath() + "/" + relPath.replace("\\","/");
}

bool Mapping::translateVrayScene( const QString & vrsceneInput, const QString & vrsceneOutputDir, QString * error )
{
	const char * sFile="file=\"", * sInclude="#include \"";
	
	QTime t;
	t.start();
	
	QFileInfo inputFileInfo(vrsceneInput);
	QString outputFilePath = vrsceneOutputDir + QDir::separator() + inputFileInfo.fileName();
	QFile input(vrsceneInput), output(outputFilePath);
	QStringList includes;
	
	if (!input.open(QIODevice::ReadOnly)) {
		if (error) *error = "Unable to open input file for reading: " + vrsceneInput;
		return false;
	}
	
	if (!output.open(QIODevice::WriteOnly)) {
		if (error) *error = "Unable to open output file for writing: " + outputFilePath;
		return false;
	}
	
	int lineNo=0;
	while (!input.atEnd()) {
		lineNo++;
		QByteArray line = input.readLine();
		int idx = -1, pathStart = -1;
		bool isInclude = false;
		
		if( (idx = line.indexOf(sFile)) >= 0 )
			pathStart = idx + strlen(sFile);
		else if( (idx = line.indexOf(sInclude)) >= 0 ) {
			isInclude = true;
			pathStart = idx + strlen(sInclude);
		}
		
		if( idx >= 0 ) {
			int pathEnd = pathStart;
			// Find second quote
			while( pathEnd < line.size() && line.at(pathEnd) != '"' )
				pathEnd++;
			
			if( pathEnd == line.size() ) {
				if (error) *error = QString("Unterminated quoted string at line %1 in file %2").arg(lineNo).arg(vrsceneInput);
				return false;
			}
			
			QString path = QString::fromUtf8(line.data() + pathStart, pathEnd - pathStart);
			if (!path.isEmpty()) {
				QString translated = Mapping::osPath(path);
				if( isInclude ) {
					includes += translated;
					translated = vrsceneOutputDir + QDir::separator() + QFileInfo(translated).fileName();
				} else {
					if (!QFile::exists(translated)) {
						QString icase = findFileICase(translated);
						if (icase.isEmpty())
							LOG_1("WARNING: Path not found: " + translated);
						else {
							translated = icase;
							LOG_3("Found matching path using case insensitive lookup: " + translated);
						}
					}
				}
				QByteArray newLine = line.left(pathStart) + translated.toUtf8() + line.mid(pathEnd);
				line = newLine;
			}
		}
		
		output.write(line);
	}
	output.close();
	int elapsed = t.elapsed();
	LOG_3( QString("Processed %0 MB file %1 in %2 ms, %3 MB/s").arg(inputFileInfo.size() / (1024.0 * 1024)).arg(vrsceneInput).arg(elapsed).arg((inputFileInfo.size() * 1000.0) / (1024.0 * 1024.0 * elapsed)) );
	
	foreach( QString include, includes ) {
		if( !translateVrayScene( include, vrsceneOutputDir, error ) )
			return false;
	}
	
	return true;
}

#endif // CLASS_FUNCTIONS

