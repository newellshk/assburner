
#include <qapplication.h>
#include <qdatetime.h>
#include <qthread.h>
#include <QtTest/QtTest>

#include "blurqt.h"
#include "database.h"
#include "field.h"
#include "freezercore.h"
#include "record.h"
#include "schema.h"

#include "assettype.h"
#include "config.h"
#include "job.h"
#include "host.h"
#include "jobtask.h"
#include "filetracker.h"
#include "pathtemplate.h"
#include "project.h"
#include "versionfiletracker.h"

class ClassesTest : public QObject
{
	Q_OBJECT
private slots:
	void initTestCase();
	void databaseConnection();
	void mergeXmlSchema();
	void jobTaskInsert();
	void jobTaskValues();
	void recordRefCounts();
	void checkForUpdate();
	void fieldListCreation();
	void recordList();
	void threads();
};

void ClassesTest::initTestCase()
{
	//QApplication a( argc, argv );
	initConfig( "test.ini" );
	blurqt_loader();
//	shutdown();
}

void ClassesTest::databaseConnection()
{
	Connection * c = Database::current()->connection();
	QVERIFY(c->checkConnection());
}

Database * d = 0;
Table * t = 0;
void ClassesTest::mergeXmlSchema()
{
	d = Database::current();
	d->schema()->mergeXmlSchema( "../schema.xml" );
	t = d->schema()->tableByName( "JobTask" )->table();
	QVERIFY(t);
}

Record r;
void ClassesTest::jobTaskInsert()
{
	r = t->load();
	r.setValue( "jobTask", 666 );
	r.setValue( "fkeyHost", 777 );
	r.setValue( "fkeyJob", 111 );
	r.commit();
	QVERIFY( r.key() != 0 );
}

void ClassesTest::jobTaskValues()
{
	QCOMPARE( 666, r.getValue( "jobTask" ).toInt() );
	QCOMPARE( 777, r.getValue( "fkeyHost" ).toInt() );
	QCOMPARE( 111, r.getValue( "fkeyJob" ).toInt() );
}

void ClassesTest::recordRefCounts()
{
	QCOMPARE( 1, r.imp()->refCount() );
	Record mod = r;
	QCOMPARE( 2, r.imp()->refCount() );
	mod.setValue( "fkeyJob", 112 );
	QCOMPARE( 1, mod.imp()->refCount() );
	QCOMPARE( 2, r.imp()->refCount() );
	mod.commit();
	QCOMPARE( 112, r.getValue( "fkeyJob" ).toInt() );
	mod = t->record( r.key(), true, false );
	QCOMPARE( mod.imp(), r.imp() );
}

void ClassesTest::checkForUpdate()
{
	Record same = t->record( r.key(), true, false );
	QCOMPARE( r.imp(), same.imp() );
	int key = r.key();
	same = Record();
	r = Record();
	same = t->record( key, false, true );
	QCOMPARE( (int)same.isRecord(), 0 );
}

void ClassesTest::fieldListCreation()
{
	FieldList fl = FieldList() << JobTask::c.Key << JobTask::c.FrameNumber << JobTask::c.Host;
	QCOMPARE( 3, fl.size() );
}

void ClassesTest::recordList()
{
	RecordList rl;
	rl += Job();
	rl.append(Host());
	QCOMPARE((int)rl.size(),2);
	QCOMPARE((int)JobList(rl).size(),1);
	QCOMPARE((int)HostList(rl).size(),1);
	JobList jl = rl;
	QCOMPARE( (int)jl.size(), 1 );
	QCOMPARE( (int)(HostList() += rl).size(), 1 );
}

/*
bool test7()
{
	QString test( "PathTemplate methods" );

	PathTemplate project_template;
	project_template.setName( "test_project_template" );
	project_template.setPathTemplate( "[Name]/" );
	project_template.commit();

	Project p = AssetType::recordByName( "Project" ).construct();
	p.setName( "A_Project" );
	p.setWipDrive( "G:" );
	p.setPathTemplate( project_template );
	p.commit();
	RETFAIL( testString( "G:/A_Project/", p.path(), test + ": project", true ) );

	PathTemplate asset_template;
	asset_template.setName( "test_asset_template" );
	asset_template.setPathTemplate( "[Parent.Path][Name:/ /_/]/" );
	asset_template.commit();

	Element ag = AssetType::recordByName( "Asset Group" ).construct();
	ag.setName( "An Asset Group" );
	ag.setParent( p );
	ag.setProject( p );
	ag.setPathTemplate( asset_template );
	ag.commit();
	RETFAIL( testString( "G:/A_Project/An_Asset_Group/", ag.path(), test + ": asset group", true ) );

	Element c = AssetType::recordByName( "Character" ).construct();
	c.setName( "A Character" );
	c.setParent( ag );
	c.setPathTemplate( asset_template );
	c.commit();
	RETFAIL( testString( "G:/A_Project/An_Asset_Group/A_Character/", c.path(), test + ": character", true ) );

	Element t = AssetType::recordByName( "Modeling" ).construct();
	t.setName( "Modeling" );
	t.setParent( c );
	t.setPathTemplate( asset_template );
	t.commit();
	RETFAIL( testString( "G:/A_Project/An_Asset_Group/A_Character/Modeling/", t.path(), test + ": modeling", true ) );

	PathTemplate ft_template;
	ft_template.setName( "test_filetracker_template" );
	ft_template.setPathTemplate( "[Path]" );
	ft_template.setFileNameTemplate( "[climb_to_type(Character).Name:/ /_/]_Mesh.max" );
	ft_template.commit();

	FileTracker ft;
	ft.setName( "modeling_final" );
	ft.setPathTemplate( ft_template );
	ft.setElement( t );
	ft.commit();
	RETFAIL( testString( "G:/A_Project/An_Asset_Group/A_Character/Modeling/A_Character_Mesh.max", ft.filePath(), test + ": modeling_final", true ) );

	PathTemplate ft2_template;
	ft2_template.setName( "test_filetracker2_template" );
	ft2_template.setPathTemplate( "[Path]" );
	ft2_template.setFileNameTemplate( "[climb_to_istask(false).Name:/ /_/]_Rig.max" );
	ft2_template.commit();

	FileTracker ft2;
	ft2.setName( "rigging_final" );
	ft2.setPathTemplate( ft2_template );
	ft2.setElement( t );
	ft2.commit();
	RETFAIL( testString( "G:/A_Project/An_Asset_Group/A_Character/Modeling/A_Character_Rig.max", ft2.filePath(), test + ": rigging_final", true ) );

	PathTemplate version_template;
	version_template.setName( "test_versionfiletracker_template" );
	version_template.setPathTemplate( "[Path]" );
	version_template.setFileNameTemplate( "[climb_to_istask(false).Name:/ /_/]_v[Version]_[Iteration].max" );
	version_template.commit();

	VersionFileTracker ft_wip;
	ft_wip.setName( "max_modeling_wip" );
	ft_wip.setPathTemplate( version_template );
	ft_wip.setElement( t );
	ft_wip.setVersion( 1 );
	ft_wip.setIteration( 2 );
	ft_wip.commit();
	RETFAIL( testString( "G:/A_Project/An_Asset_Group/A_Character/Modeling/A_Character_v1_2.max", ft_wip.filePath(), test + ": rigging_final", true ) );
	
	return true;
}*/

class ThreadTest : public QThread
{
public:
	ThreadTest(Database * db, ConfigList records, QWaitCondition * wait) : QThread(), Db(db), Records(records), Wait(wait)
	{
		
	}
	
	void run() {
		// Create a new connection to the database for our thread(thread local)
		Connection * conn = Connection::createFromIni( config(), "Database" );
		Db->setConnection( conn );
		conn->reconnect();
		// Share the same db with the rest of the process
		Database::setCurrent( Db );
		//ConfigList mods;
		
		const int loops = 5;
		for( int i = 0; i < loops; ++i ) {
			foreach( Config c, Config::select( Expression().limit(1000) ) ) {
				QString s = c.dump();
				//c.setConfig("test");
				//mods.append(c);
			}
			foreach( Config c, Records ) {
				c.isValid();
				//QString s = c.dump();
			}
		}
		Wait->wakeOne();
	}

	Database * Db;
	ConfigList Records;
	QWaitCondition * Wait;
};

#include <iostream>
void ClassesTest::threads()
{
	const int threadCounts [] = { 1, 2, 3, 4, 5 , 6, 7, 8, 9, 10, 12, 20, 50, 100, 500 };
	for( int threadCount=0, end=sizeof(threadCounts) / sizeof(threadCounts[0]); threadCount<end; ++threadCount ) {
		QTime time;
		QMutex mutex;
		QWaitCondition wait;
		time.start();
		ConfigList cl = Config::select( Expression().limit(1000) );
		QList<ThreadTest*> threads;
		for( int i=0; i < threadCount; ++i ) {
			ThreadTest * tt = new ThreadTest(Database::current(),cl,&wait);
			tt->start();
			threads.append(tt);
		}
		while( threads.size() ) {
			mutex.lock();
			wait.wait(&mutex,5);
			mutex.unlock();
			for( int i=0; i<threads.size(); ++i ) {
				if( threads[i]->isFinished() ) {
					delete threads[i];
					threads.removeAt(i);
					break;
				}
			}
		}
		int elapsed = time.elapsed();
		std::cout << threadCount << "\t" << elapsed << "\t" << (elapsed/double(threadCount)) << std::endl;
	}
}

QTEST_MAIN(ClassesTest)
#include "test.moc"
