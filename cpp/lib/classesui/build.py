
from blur.build import *

path = os.path.dirname(os.path.abspath(__file__))
sippath = os.path.join(path,'sipClassesui')

post_deps = []

# Install the libraries
if sys.platform != 'win32':
	LibInstallTarget("classesuiinstall",path,"classesui","/usr/lib/")

try:
	os.mkdir(sippath)
except: pass

sipIncludes = [
	'../../classes/','/usr/include/classes',
	'../../classes/autocore/','/usr/include/classes/autocore',
	'../../stone/include','/usr/include/stone',
	'../../stonegui/include','/usr/include/stonegui',
	'../../qjson/','/usr/include/qjson',
	'/usr/include/classesui']

sipLibDirs = ['../../stone','../../stonegui','../../classes','../../qjson/']

# Python module targets, both depend on classes
pc = SipTarget2("pyclassesui",path,'Classesui')
pc.pre_deps = ["classesui","pyclasses:install","pystonegui:install"]
pc.ExtraIncludes += sipIncludes
pc.ExtraLibDirs += sipLibDirs
pc.ExtraLibs += ['stone','stonegui','classes','classesui','qjson4']

sst = SipTarget2("pyclassesuistatic",path,'Classesui',True)
sst.pre_deps = ["classesui"]
sst.ExtraIncludes += sipIncludes

Target = QMakeTarget("classesui",path,"classesui.pro",["stonegui","classes"],post_deps)
StaticTarget = QMakeTarget("classesuistatic",path,"classesui.pro",["classesui"],[],True)

rpm = RPMTarget('classesuirpm','libclassesui',path,'../../../rpm/spec/classesui.spec.template','1.0')

pyrpm = RPMTargetSip('pyclassesuirpm','pyclassesui',path,'../../../rpm/spec/pyclassesui.spec.template','1.0')

if __name__ == "__main__":
	build()
