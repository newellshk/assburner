
#ifndef HOST_LIST_SAVE_DIALOG_H
#define HOST_LIST_SAVE_DIALOG_H

#include <qdialog.h>

#include "classesui.h"
#include "ui_savelistdialogui.h"

#include "host.h"
#include "hostgroup.h"

class CLASSESUI_EXPORT HostListSaveDialog : public QDialog, Ui::SaveListDialogUI
{
Q_OBJECT
public:
	HostListSaveDialog( QWidget * parent, HostList hosts, HostGroup existing = HostGroup() );
	
	void accept();
	
	static bool saveList( QWidget * parent, HostList hosts, HostGroup existing = HostGroup() );
protected:
	HostList mHosts;
};

#endif // HOST_LIST_SAVE_DIALOG_H

