
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Assburner.
 *
 * Assburner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Assburner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blur; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* $Author$
 * $LastChangedDate$
 * $Rev$
 * $HeadURL$
 */

#ifndef HOST_SELECTOR_H
#define HOST_SELECTOR_H

#include <qstring.h>

#include "host.h"
#include "job.h"
#include "hoststatus.h"
#include "project.h"
#include "service.h"
#include "jobassignment.h"
#include "jobtaskassignment.h"

#include "classesui.h"
#include "recordsupermodel.h"
#include "ui_hostselectorbase.h"

#include "viewcolors.h"

class RecordModel;

struct ColorOption;

CLASSESUI_EXPORT HostList hostListFromString( const QString & );

CLASSESUI_EXPORT Expression jobAssignmentsByHostExp();
CLASSESUI_EXPORT Expression jobTaskAssignmentByJobAssignmentExp();
CLASSESUI_EXPORT JobAssignmentList jobAssignmentsByHost( const Host & );
CLASSESUI_EXPORT JobTaskAssignmentList jobTaskAssignmentsByJobAssignments( JobAssignmentList, int lookupMode=Index::UseCache|Index::UseSelect );

class CLASSESUI_EXPORT HostItem : public RecordItemBase
{
public:
	HostItem();
	mutable bool jobLoaded:1, ipLoaded:1, driveSpaceLoaded:1;
	Host host;
	HostStatus status;
	mutable Project project;
	mutable QString activeJobNames;
	mutable QString name, ver, mem, availMem, mhz, user, pulse, services, ip, uptime, tasktime, drvSpace;
	mutable Interval uptimeInterval, tasktimeInterval;
	ColorOption * co;
	QString jobName() const;
	QString ipAddress() const;
	QString driveSpace() const;
	void setup( const Record & r, const QModelIndex &, bool loadJob = true );
	QVariant modelData( const QModelIndex & i, int role ) const;
	char getSortChar() const;
	int compare( const QModelIndex & a, const QModelIndex & b, int, bool ) const;
	Qt::ItemFlags modelFlags( const QModelIndex & );
	Record getRecord();

	static const ColumnStruct host_columns [];
	static ViewColors * HostColors;
};

typedef TemplateRecordDataTranslator<HostItem> HostTranslator;

class CLASSESUI_EXPORT HostSelector : public QDialog, public Ui::HostSelectorBase
{
Q_OBJECT
public:
	HostSelector(QWidget * parent=0);

	/// Checks the hosts according to the list
	void setHostList( const RecordList & );
	/// Checks the hosts according to a comma separated list of host names
	void setHostList( const QString & hostList );

	/// Returns a comma separated list of host names for each checked host in the list
	QString hostStringList() const;

	/// Returns the list of hosts that are checked
	HostList hostList() const;

	/// Returns the list of services required for each displayed host
	ServiceList serviceFilter() const;
	
	bool showingMyGroupsOnly() const;
public slots:

	void selectAll();

	void checkSelected();

	void uncheckSelected();

	void setShowMyGroupsOnly( bool );
	void refreshHostGroups();

	void hostGroupChanged( const Record & hg );

	/// Saves the current hostList() as a host group, prompts the user for info
	void saveHostGroup();

	/// Opens the Host Lists Dialog
	void manageHostLists();

	/// Sets the list of services required for each displayed host and calls refresh
	void setServiceFilter( ServiceList );

	/// Schedules a refresh of the list of hosts according to the current service filter
	void refresh();

	void showOptionsMenu();
	
	void filter( const Expression & );
	
protected slots:
	/// Performs the actual refresh
	void performRefresh();
	void performHostGroupRefresh();
	void updateCheckCount();
	
protected:
	void setSelected(bool check);

	void updateList( HostList checked );


	bool mRefreshPending, mHostGroupRefreshPending;
	HostList mNeedsSelected;
	ServiceList mServiceFilter;
	RecordSuperModel * mModel;
	QAction * mShowMyGroupsOnlyAction, * mManageGroupsAction, * mSaveGroupAction;
};

#endif // HOST_SELECTOR_H

