
#ifndef JOB_ASSIGNMENT_WINDOW_H
#define JOB_ASSIGNMENT_WINDOW_H

#include <qmainwindow.h>

#include "jobassignment.h"
#include "jobcommandhistory.h"

#include "classesui.h"

#include "ui_jobassignmentwindowui.h"

class CLASSESUI_EXPORT JobAssignmentWindow : public QMainWindow, public Ui::JobAssignmentWindowUI
{
Q_OBJECT
public:
	JobAssignmentWindow( QWidget * parent=0 );
	~JobAssignmentWindow();

	void setJobAssignment(const JobAssignment &);
	JobAssignment jobAssignment();

	void setJobCommandHistory(const JobCommandHistory &);
	JobCommandHistory jobCommandHistory();

public slots:
	void refresh();

protected slots:
	void doRefresh();

protected:
	bool mRefreshRequested;
	JobAssignment mJobAssignment;
	JobCommandHistory mJobCommandHistory;
	QString mLogRoot;
};

#endif // JOB_ASSIGNMENT_WINDOW_H

