
#ifndef PROJECT_RESOLUTION_H
#define PROJECT_RESOLUTION_H

#include <qwidget.h>
#include <qdialog.h>

#include "projectresolution.h"

#include "classesui.h"

#include "ui_projectresolutionwidgetui.h"

class QDialogButtonBox;

class CLASSESUI_EXPORT ProjectResolutionWidget : public QWidget, public Ui::ProjectResolutionWidgetUI
{
Q_OBJECT
public:
	ProjectResolutionWidget(QWidget * parent = nullptr);

	void setProjectResolution( ProjectResolution pr );
	ProjectResolution projectResolution() const { return mProjectResolution; }

public slots:
	void apply( bool commit = true );
	void reset();
	
protected slots:
	void resolutionsFutureReady(ExpressionFuture future);

protected:
	ProjectResolution mProjectResolution;
};

class CLASSESUI_EXPORT ProjectResolutionDialog : public QDialog
{
Q_OBJECT
public:
	ProjectResolutionDialog( QWidget * parent = nullptr );
	ProjectResolutionDialog( const ProjectResolution &, QWidget * parent = nullptr );
	
	ProjectResolutionWidget * projectResolutionWidget() const { return mProjectResolutionWidget; }
	
	static ProjectResolution create( const Project &, QWidget * parent = nullptr );
	static ProjectResolution edit( const ProjectResolution &, QWidget * parent = nullptr );

protected slots:
	void slotCheckCanSave();

protected:
	void accept();
	void init();
	
	ProjectResolutionWidget * mProjectResolutionWidget;
	QDialogButtonBox * mDialogButtonBox;
};

#endif // PROJECT_RESOLUTION_H
