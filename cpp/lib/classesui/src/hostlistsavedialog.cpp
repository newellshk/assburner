
#include <qmessagebox.h>

#include "hostlistsavedialog.h"

#include "user.h"

HostListSaveDialog::HostListSaveDialog( QWidget * parent, HostList hosts, HostGroup existing )
: QDialog( parent )
, mHosts( hosts )
{
	setupUi(this);
	mNameCombo->addItems( HostGroup::recordsByUser( User::currentUser() ).filter( "name", QRegExp( "^.+$" ) ).sorted( "name" ).names() );
	mNameCombo->addItems( HostGroup::recordsByUser( User() ).filter( "name", QRegExp( "^.+$" ) ).sorted( "name" ).names() );

	if( existing.isRecord() ) {
		int idx = mNameCombo->findText( existing.name() );
		if( idx >= 0 )
			mNameCombo->setCurrentIndex( idx );

		if( !existing.user().isRecord() )
			mGlobalCheck->setChecked( true );
	}
}

void HostListSaveDialog::accept()
{
	HostGroup hg = HostGroup::recordByNameAndUser( mNameCombo->currentText(), mGlobalCheck->isChecked() ? User() : User::currentUser() );
	if( hg.isRecord() &&
		QMessageBox::question( this, "Overwrite Host List?"
			,"This will overwrite the existing list: " + hg.name() + "\nAre you sure you want to continue?"
			, QMessageBox::Yes, QMessageBox::No ) != QMessageBox::Yes
		)
		return;

	//
	// Commit the host group
	hg.setName( mNameCombo->currentText() );
	hg.setUser( User::currentUser() );
	hg.setPrivate_( !mGlobalCheck->isChecked() );
	hg.commit();

	// Gather existing hostgroup items
	QMap<Host,HostGroupItem> exist;
	HostGroupItemList com;

	HostGroupItemList hgl = HostGroupItem::recordsByHostGroup( hg );
	foreach( HostGroupItem hgi, hgl )
		exist[hgi.host()] = hgi;

	// Commit the items
	foreach( Host h, mHosts ) {
		HostGroupItem hgi;
		if( exist.contains( h ) ) {
			hgi = exist[h];
			exist.remove( h );
		}
		hgi.setHostGroup(hg);
		hgi.setHost(h);
		com += hgi;
	}
	com.commit();
	
	// Delete the old ones
	HostGroupItemList toRemove;
	for( QMap<Host,HostGroupItem>::Iterator it = exist.begin(); it != exist.end(); ++it )
		toRemove += it.value();
	toRemove.remove();
	
	QDialog::accept();
}

bool HostListSaveDialog::saveList( QWidget * parent, HostList hosts, HostGroup existing )
{
	HostListSaveDialog sd(parent,hosts,existing);
	return sd.exec() == QDialog::Accepted;
}

