
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Assburner.
 *
 * Assburner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Assburner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Blur; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* $Author$
 * $LastChangedDate$
 * $Rev$
 * $HeadURL$
 */

#include <qcheckbox.h>
#include <qcombobox.h>
#include <qmenu.h>
#include <qmessagebox.h>
#include <qpushbutton.h>
#include <qregexp.h>
#include <qtimer.h>
#include <qtreewidget.h>

#include "iniconfig.h"

#include "modeliter.h"

#include "department.h"
#include "dynamichostgroup.h"
#include "employee.h"
#include "host.h"
#include "hostinterface.h"
#include "hostservice.h"
#include "hoststatus.h"
#include "hostgroup.h"
#include "hostgroupitem.h"
#include "hostlistsavedialog.h"
#include "hostlistsdialog.h"
#include "user.h"

#include "hostselector.h"

const ColumnStruct HostItem::host_columns [] =
{
	{ "Host", "HostColumn", 100, 0, false },
	{ "Current Job", "CurrentJobColumn", 200, 1, false },
	{ "Status", "StatusColumn", 65, 2, false },
	{ "Frames", "FramesColumn", 50, 3, false },
	{ "OS", "OSColumn", 120, 4, false },
	{ "Memory", "MemoryColumn", 100, 5, false },
	{ "Mhz", "MhzColumn", 75, 6, false },
	{ "User", "UserColumn", 70, 7, false },
	{ "Packet Weight", "PacketWeightColumn", 40, 8, true },
	{ "Description", "DescriptionColumn", 200, 9, false },
	{ "Pulse", "PulseColumn", 130, 10, false },
	{ "Key", "KeyColumn", 0, 11, true },
	{ "OS Version", "OsVersionColumn", 100, 12, false },
	{ "CPU Name", "CpuNameColumn", 40, 14, false },
	{ "Arch", "ArchColumn", 65, 15, false },
	{ "Services", "ServicesColumn", 100, 16, true },
	{ "Avail. Mem", "AvailMemColumn", 80, 17, false },
	{ "IP Address", "IPAddressColumn", 45, 18, true },
	{ "Domain", "DomainColumn", 45, 19, true },
	{ "Department", "DepartmentColumn", 60, 20, true },
	{ "System Uptime", "SystemUptimeColumn", 120, 21, false },
	{ "Elapsed Task Time", "ElapsedTaskTimeColumn", 60, 22, false },
	{ "OS Service Pack", "OSServicePackColumn", 60, 13, true },
	{ "OS Build Number", "OSBuildNumberColumn", 60, 23, true },
	{ "Video Card", "VideoCardColumn", 100, 24, true },
	{ "Video Card Driver", "VideoCardDriverColumn", 120, 25, true },
	{ "Video Memory", "VideoMemoryColumn", 100, 26, true },
	{ "Project (Current Job)", "CurrentJobProjectColumn", 100, 27, true },
	{ "Manufacturer", "ManufacturerColumn", 100, 28, true },
	{ "Model", "ModelColumn", 100, 29, true },
	{ "Drive Space", "DriveSpaceColumn", 100, 30, true },
	{ 0, 0, 0, 0, false }
};

ViewColors * HostItem::HostColors = 0;

HostItem::HostItem()
: jobLoaded(false)
, ipLoaded(false)
, driveSpaceLoaded(false)
{}

Expression jobAssignmentsByHostExp()
{
	static Expression ret;
	if( !ret.isValid() ) {
		ret = (JobAssignment::c.Host == PlaceHolder("host")
				&& JobAssignment::c.JobAssignmentStatus.between(ev_(1),ev_(3))).cache();
	}
	return ret;
}

JobAssignmentList jobAssignmentsByHost( const Host & h )
{
	return jobAssignmentsByHostExp()(QVariant::fromValue<Record>(h));
}

Expression jobTaskAssignmentByJobAssignmentExp()
{
	static Expression ret;
	if( !ret.isValid() ) {
		ret = (JobTaskAssignment::c.JobAssignment==PlaceHolder("jobAssignment")
				&& JobTaskAssignment::c.JobAssignmentStatus == ev_(3)).cache();
	}
	return ret;
}

JobTaskAssignmentList jobTaskAssignmentsByJobAssignments( JobAssignmentList jobAssignments, int lookupMode )
{
	return jobTaskAssignmentByJobAssignmentExp()(QVariant::fromValue<RecordList>(jobAssignments),lookupMode);
}

void HostItem::setup( const Record & r, const QModelIndex &, bool loadJob )
{
	static const QString s_mb(" Mb"), s_mhz(" Mzh ("), s_rparen(")"), s_space(" ");
	host = r;
	name = host.name();
	status = host.hostStatus();
	ver = host.os() + s_space + host.abVersion();
	mem = QString::number(host.memory()) + s_mb;
	availMem = QString::number(status.availableMemory()) + s_mb;
	mhz = QString::number(host.mhz()) + s_mhz + QString::number(host.cpus()) + s_rparen;
	User u = host.user();
	Employee e(u);
	user = e.isRecord() ? e.name() : u.name();
	pulse = status.slavePulse().toString(Qt::ISODate);
	co = HostColors ? HostColors->getColorOption(status.slaveStatus()) : 0;
	services = QString();
	QDateTime cdt(QDateTime::currentDateTime());
	uptimeInterval = status.systemStartupTimestamp().isNull() ? Interval() : Interval( status.systemStartupTimestamp(), cdt );
	
	tasktimeInterval = status.taskStartTimestamp().isNull() ? Interval() : Interval( status.taskStartTimestamp(), cdt );
	uptime = uptimeInterval == Interval() ? QString() : uptimeInterval.toDisplayString();

	// Ensure to regenerate cached fields
	jobLoaded = ipLoaded = driveSpaceLoaded = false;

	if( loadJob ) jobName();
	tasktime = tasktimeInterval == Interval() ? QString() : tasktimeInterval.toDisplayString();
}

QString HostItem::jobName() const
{
	if( !jobLoaded ) {
		JobAssignmentList jobAssignments = jobAssignmentsByHost(host);
		
		/*
		QStringList jobKeys;
		foreach( JobAssignment ja, jobAssignments )
			jobKeys.append(ja.getValue(JobAssignment::c.Job).toString());
		LOG_1( name + ": " + jobAssignments.keyString() );
		LOG_1( name + ": " + jobKeys.join(",") );
		*/
		
		JobList jobs = jobAssignments.jobs();
		activeJobNames = jobs.names().join(",");
		QDateTime earliestStartTime;
		foreach( JobTaskAssignment jta, jobTaskAssignmentsByJobAssignments(jobAssignments) )
		{
			QDateTime startTime = jta.started();
			if( earliestStartTime.isNull() || earliestStartTime > startTime )
				earliestStartTime = startTime;
		}
		if( !earliestStartTime.isNull() )
			tasktimeInterval = Interval(earliestStartTime,QDateTime::currentDateTime());
		if( jobs.size() )
			project = jobs[0].project();
		jobLoaded = true;
	}
	return activeJobNames;
}

QString HostItem::ipAddress() const
{
	static const QString s_comma(",");
	if( !ipLoaded ) {
		HostInterfaceList hil = host.hostInterfaces();
		ip = hil.ips().join(s_comma);
	}
	return ip;
}

QString HostItem::driveSpace() const
{
	static const QString s_space(" "), s_free("% free "), s_gb("Gb");
	if( !driveSpaceLoaded ) {
		driveSpaceLoaded = true;
		QStringList parts;
		foreach( QString perDrive, status.driveSpace().split(';') ) {
			QStringList tmp = perDrive.split(',');
			if( tmp.size() != 3 ) continue;
			double gbTotal = tmp[1].toDouble() / 1024., gbFree = tmp[2].toDouble() / 1024.;
			parts.append( tmp[0] + s_space + QString::number(int(100.*gbFree/gbTotal + .5)) + s_free + QString::number(gbTotal) );
		}
		drvSpace = parts.join(",");
	}
	return drvSpace;
}

static QVariant civ( const QColor & c )
{
	if( c.isValid() )
		return QVariant(c);
	return QVariant();
}

QVariant HostItem::modelData( const QModelIndex & i, int role ) const
{
	int col = i.column();
	if( role == Qt::DisplayRole ) {
		switch( col ) {
			case 0: return name;
			case 1: return jobName();
			case 2: return status.slaveStatus();
			case 3: return QVariant();//status.slaveFrames();
			case 4: return ver;
			case 5: return mem;
			case 6: return mhz;
			case 7: return user;
			case 8: return "";
			case 9: return host.description();
			case 10: return pulse;
			case 11: return host.key();
			case 12: return host.osVersion();
			case 13: return host.cpuName();
			case 14: return host.architecture();
			case 15:
			{
				if( services.isNull() ) {
					services = host.hostServices().services().sorted("service").services().join(",");
					if( services.isNull() ) services = QString("");
				}
				return services;
			}
			case 16: return availMem;
			case 17: return ipAddress();
			case 18: return host.windowsDomain();
			case 19: return host.department().name();
			case 20: return uptime;
			case 21: return tasktime;
			case 22: return host.servicePackVersion();
			case 23: return QString::number(host.buildNumber());
			case 24: return host.videoCard();
			case 25: return host.videoCardDriver();
			case 26: return QString("%1 Mb").arg(host.videoMemory());
			case 27: return project.name();
			case 28: return host.manufacturer();
			case 29: return host.model();
			case 30: return driveSpace();
		}
	} else if ( role == Qt::TextColorRole )
		return co ? civ(co->fg) : QVariant();
	else if( role == Qt::BackgroundColorRole )
		return co ? civ(co->bg) : QVariant();
	return QVariant();
}

char HostItem::getSortChar() const {
	QString stat = status.slaveStatus();
	if( stat=="ready" ) return 'a';
	else if( stat=="assigned" ) return 'b';
	else if( stat=="copy" ) return 'c';
	else if( stat=="busy" ) return 'd';
	else if( stat=="starting" ) return 'e';
	else if( stat=="waking" ) return 'f';
	else if( stat=="sleep" ) return 'g';
	else if( stat=="sleeping" ) return 'h';
	else if( stat=="client-update" ) return 'i';
	else if( stat=="restart" ) return 'j';
	else if( stat=="restart-when-done" ) return 'k';
	else if( stat=="reboot" ) return 'l';
	else if( stat=="reboot-when-done" ) return 'm';
	else if( stat=="stopping" ) return 'n';
	else if( stat=="offline" ) return 'o';
	else if( stat=="no-pulse" ) return 'p';
	else if( stat=="no-ping" ) return 'q';
	else return 'z';
}

int HostItem::compare( const QModelIndex & a, const QModelIndex & b, int col, bool asc ) const
{
	HostItem & other = HostTranslator::data(b);
	if( col == 2 ) {
		char sc = getSortChar(), osc = other.getSortChar();
		if( sc == osc ) return 0;
		if( sc > osc ) return 1;
		return -1;
	}
	if( col >= 5 && col <= 6 ) {
		int vala = col == 5 ? host.memory() : host.mhz();
		int valb = col == 5 ? other.host.memory() : other.host.mhz();
		return vala - valb;
	}
	if( col == 20 )
		return Interval::compare(uptimeInterval,other.uptimeInterval);
	if( col == 21 )
		return Interval::compare(tasktimeInterval,other.tasktimeInterval);
	return ItemBase::compare(a,b,col,asc);
}

Qt::ItemFlags HostItem::modelFlags( const QModelIndex & )
{ return Qt::ItemFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled ); }

Record HostItem::getRecord()
{ return host; }

struct HostListItem : public HostItem {
	bool checked;
	void setup( const Record & r, const QModelIndex & i ) {
		HostItem::setup(r,i, false /*loadJob*/);
		checked = false;
	}
	QVariant modelData( const QModelIndex & i, int role ) const {
		int col = i.column();
		if( role == Qt::DisplayRole ) {
			switch( col ) {
				case 0: return name;
				case 1: return status.slaveStatus();
				case 2: return ver;
				case 3: return mem;
				case 4: return mhz;
				case 5: return user;
				case 6: return host.description();
			}
		} else if ( role == Qt::TextColorRole )
			return co ? civ(co->fg) : QVariant();
		else if( role == Qt::CheckStateRole && col == 0 )
			return checked ? Qt::Checked : Qt::Unchecked;
		return QVariant();
	}

	bool setModelData( const QModelIndex & i, const QVariant & v, int role ) {
		if( role == Qt::CheckStateRole && i.column() == 0 ) {
			checked = v.toInt() == Qt::Checked;
			return true;
		}
		return false;
	}

	int compare( const QModelIndex & a, const QModelIndex & b, int col, bool asc );

	Qt::ItemFlags modelFlags( const QModelIndex & i )
	{
		if( i.column() == 0 )
			return Qt::ItemFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable );
		return Qt::ItemFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled );
	}
};

typedef TemplateRecordDataTranslator<HostListItem> HostListTranslator;

int HostListItem::compare( const QModelIndex & a, const QModelIndex & b, int col, bool asc )
{
	HostListItem & other = HostListTranslator::data(b);
	if( col == 1 ) {
		char sc = getSortChar(), osc = other.getSortChar();
		if( sc == osc ) return 0;
		if( sc > osc ) return 1;
		return -1;
	}
	if( col >= 3 && col <= 4 ) {
		int vala = col == 3 ? host.memory() : host.mhz();
		int valb = col == 3 ? other.host.memory() : other.host.mhz();
		return vala - valb;
	}
	return ItemBase::compare(a,b,col,asc);
}

HostSelector::HostSelector( QWidget * parent )
: QDialog( parent )
, mRefreshPending( false )
, mHostGroupRefreshPending( false )
{
	HostSelectorBase::setupUi( this );

	mModel = new RecordSuperModel( mHostTree );
	new HostListTranslator(mModel->treeBuilder());
	mHostTree->setModel( mModel );
	mModel->setHeaderLabels( QStringList() << "Host" << "Status" << "Version" << "Memory" << "Mhz" << "User" << "Description" );

	mModel->setAutoSort(true);
	mModel->sort(0,Qt::AscendingOrder);
	connect( mModel, SIGNAL(dataChanged(const QModelIndex&,const QModelIndex&)), SLOT(updateCheckCount()) );
	
	connect( SelectAll, SIGNAL( clicked() ), SLOT( selectAll() ) );
	connect( CheckSelected, SIGNAL( clicked() ), SLOT( checkSelected() ) );
	connect( UnCheckSelected, SIGNAL( clicked() ), SLOT( uncheckSelected() ) );

	mShowMyGroupsOnlyAction = new QAction( "Show My Lists Only", this );
	mShowMyGroupsOnlyAction->setCheckable( true );
	IniConfig cfg(userConfig());
	cfg.pushSection( "HostSelector" );
	mShowMyGroupsOnlyAction->setChecked( cfg.readBool( "ShowMyGroupsOnly", false ) );
	cfg.popSection();

	mManageGroupsAction = new QAction( "Manage My Lists", this );
	mSaveGroupAction = new QAction( "Save Current List", this );
	
	connect( mHostGroupCombo,SIGNAL( currentChanged( const Record & ) ), SLOT( hostGroupChanged( const Record & ) ) );
	connect( mSaveGroupAction, SIGNAL( triggered() ), SLOT( saveHostGroup() ) );
	connect( mManageGroupsAction, SIGNAL( triggered() ), SLOT( manageHostLists() ) );
	connect( mShowMyGroupsOnlyAction, SIGNAL( toggled(bool) ), SLOT( setShowMyGroupsOnly(bool) ) );
	connect( mOptionsButton, SIGNAL( clicked() ), SLOT( showOptionsMenu() ) );

	mHostGroupCombo->setColumn( "name" );

	mHostFilterEdit->label()->setText( "Host Name Filter:" );
	mHostFilterEdit->setMatchField( Host::c.Name );
	mHostFilterEdit->setUpdateMode(FilterEdit::UpdateOnEdit);
	connect( mHostFilterEdit, SIGNAL(filterChanged(const Expression &)), SLOT(filter(const Expression &)) );
	
	refresh();
	refreshHostGroups();
}

void HostSelector::showOptionsMenu()
{
	QMenu * menu = new QMenu(this);
	menu->addAction( mShowMyGroupsOnlyAction );
	menu->addSeparator();
	menu->addAction( mSaveGroupAction );
	menu->addAction( mManageGroupsAction );
	menu->exec( QCursor::pos() );
	delete menu;
}

HostList hostListFromString( const QString & hostList )
{
	HostList hosts;
	QStringList sl = hostList.split(',');
	foreach( QString hn, sl ) {
		Host h(hn);
		LOG_3( QString(h.isRecord() ? "Found host " : "Unknown host ") + hn );
		if( h.isRecord() ) hosts += h;
	}
	return hosts;
}

void HostSelector::refresh()
{
	if( !mRefreshPending ) {
		QTimer::singleShot( 0, this, SLOT( performRefresh() ) );
		mRefreshPending = true;
	}
}

void HostSelector::performRefresh()
{
	Expression e;
	if( mServiceFilter.size() )
		e = Host::c.Key.in( Expression::sql( "(SELECT fkeyhost FROM "
			"(SELECT count(*), fkeyhost FROM HostService WHERE HostService.fkeyservice IN (" + mServiceFilter.keyString() + ") AND HostService.enabled=true GROUP BY fkeyhost) AS iq "
			"WHERE count=" + QString::number(mServiceFilter.size()) + ")" ) ) && Host::c.Online == 1;
	HostList hl = Host::select(e);
	HostStatusList hsl = hl.hostStatuses();
	if( mNeedsSelected.isEmpty() )
		mNeedsSelected = mModel->getRecords( ModelIter::collect(mModel,ModelIter::Checked) );
	mModel->updateRecords( hl );
	mRefreshPending = false;

	if( !mNeedsSelected.isEmpty() ) {
		updateList( mNeedsSelected );
		mNeedsSelected.clear();
	}
}

void HostSelector::filter( const Expression & exp )
{
	for( ModelIter it(mModel); it.isValid(); ++it )
		mHostTree->setRowHidden( (*it).row(), (*it).parent(), (exp.isValid() && !exp.matches(mModel->getRecord(*it))) );
	updateCheckCount();
}

void HostSelector::updateCheckCount()
{
	int checked = 0, checkedHidden = 0;
	for( ModelIter it(mModel); it.isValid(); ++it )
		if( mModel->data( *it, Qt::CheckStateRole ).toInt() == Qt::Checked ) {
			checked++;
			if( mHostTree->isRowHidden( (*it).row(), (*it).parent() ) )
				checkedHidden++;
		}
	QString rt = "Checked Hosts: " + QString::number(checked);
	if( checkedHidden )
		rt += " <b style=\"color:red\">Checked Hidden: " + QString::number(checkedHidden) + "</b>";
	mCheckedStatusLabel->setText(rt);
}

void HostSelector::setHostList( const QString & hostList )
{
	updateList(hostListFromString(hostList));
}

void HostSelector::setHostList( const RecordList & hrl )
{
	updateList(hrl);
}

void HostSelector::updateList( HostList checked )
{
	if( mRefreshPending ) {
		mNeedsSelected = checked;
		return;
	}

	int rows = mModel->rowCount();
	for( int r=0; r<rows; r++ ) {
		QModelIndex i = mModel->index(r,0);
		mModel->setData( i, QVariant(checked.contains(mModel->getRecord(i)) ? Qt::Checked : Qt::Unchecked), Qt::CheckStateRole );
	}
}

void HostSelector::refreshHostGroups()
{
	if( !mHostGroupRefreshPending ) {
		QTimer::singleShot( 0, this, SLOT( performHostGroupRefresh() ) );
		mHostGroupRefreshPending = true;
	}
}

void HostSelector::performHostGroupRefresh()
{
	mHostGroupRefreshPending = false;
	HostGroup hg;
	hg.setName( "Custom List" );
	HostGroupList hgl = HostGroup::select( "fkeyusr=? or private is null or private=false", VarList() << User::currentUser().key() ).sorted( "name" );
	if( mShowMyGroupsOnlyAction->isChecked() )
		hgl = hgl.filter( "fkeyusr", User::currentUser().key() );
	hgl.insert(hgl.begin(),hg);
	mHostGroupCombo->setItems( hgl );
}

void HostSelector::setShowMyGroupsOnly( bool showMyGroupsOnly )
{
	IniConfig cfg(userConfig());
	cfg.pushSection( "HostSelector" );
	cfg.writeBool( "ShowMyGroupsOnly", showMyGroupsOnly );
	cfg.popSection();
	mShowMyGroupsOnlyAction->setChecked( showMyGroupsOnly );
	refreshHostGroups();
}

bool HostSelector::showingMyGroupsOnly() const
{
	return mShowMyGroupsOnlyAction->isChecked();
}

void HostSelector::hostGroupChanged( const Record & hgr )
{
	if( hgr.isRecord() )
		updateList(HostGroup(hgr).hosts());
}

void HostSelector::saveHostGroup()
{
	HostListSaveDialog::saveList( this, hostList(), mHostGroupCombo->current() );
	refreshHostGroups();
}

void HostSelector::manageHostLists()
{
	HostListsDialog * hld = new HostListsDialog( this );
	hld->show();
	hld->exec();
	delete hld;
}

HostList HostSelector::hostList() const
{
	HostList ret = mModel->getRecords( ModelIter::collect( mModel, ModelIter::Checked ) );
	LOG_5( "Found " + QString::number( ret.size() ) + " checked hosts" );
	return ret;
}

QString HostSelector::hostStringList() const
{
	QString ret = hostList().names().join(",");
	LOG_5( ret );
	return ret;
}

void HostSelector::setSelected( bool check )
{
	QItemSelection sel = mHostTree->selectionModel()->selection();
	foreach( QItemSelectionRange r, sel ) {
		if( r.left() > 0 ) continue;
		for( int i = r.top(); i<= r.bottom(); i++ )
			mModel->setData( mModel->index( i, 0, r.parent() ), QVariant(check ? Qt::Checked : Qt::Unchecked), Qt::CheckStateRole );
	}
}

void HostSelector::selectAll()
{
	mHostTree->selectionModel()->select(
		  QItemSelection(
			  mModel->index(0,0)
			, mModel->index(mModel->rowCount()-1,mModel->columnCount()-1)
		  )
		, QItemSelectionModel::ClearAndSelect );
}

void HostSelector::checkSelected()
{
	setSelected( true );
}

void HostSelector::uncheckSelected()
{
	setSelected( false );
}

void HostSelector::setServiceFilter( ServiceList sl )
{
	mServiceFilter = sl;
	refresh();
}

ServiceList HostSelector::serviceFilter() const
{
	return mServiceFilter;
}

