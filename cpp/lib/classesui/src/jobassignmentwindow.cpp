
#include <qtimer.h>
#include <qfile.h>

#include "config.h"
#include "jobassignmentwindow.h"

JobAssignmentWindow::JobAssignmentWindow( QWidget * parent )
: QMainWindow( parent )
, mRefreshRequested( false )
{
	setupUi( this );

	setAttribute( Qt::WA_DeleteOnClose, true );

	connect(mRefreshButton, SIGNAL(pressed()), SLOT(refresh()));
	resize(600, 800);
	mLogRoot = Config::getString("assburnerLogRootDir", "");
}

JobAssignmentWindow::~JobAssignmentWindow()
{}

JobAssignment JobAssignmentWindow::jobAssignment()
{
	return mJobAssignment;
}

void JobAssignmentWindow::setJobAssignment(const JobAssignment & ja)
{
	mJobAssignment = ja;
	mJobCommandHistory = ja.jobCommandHistory();
	refresh();
}

void JobAssignmentWindow::setJobCommandHistory(const JobCommandHistory & jch)
{
	mJobCommandHistory = jch;
	refresh();
}

JobCommandHistory JobAssignmentWindow::jobCommandHistory()
{
	return mJobCommandHistory;
}

void JobAssignmentWindow::refresh()
{
	if( !mRefreshRequested ) {
		mRefreshRequested = true;
		QTimer::singleShot( 0, this, SLOT( doRefresh() ) );
	}
}

void JobAssignmentWindow::doRefresh()
{
	mRefreshRequested = false;
	mJobAssignment.reload();
	mJobCommandHistory.reload();
	mCommandEdit->setText( mJobAssignment.command() );
	if( mLogRoot.isEmpty() ) {
		mPathEdit->hide();
		mLogText->document()->setPlainText( mJobCommandHistory.stdOut() );
	} else {
		QFile read(mJobCommandHistory.stdOut());
		if( !read.open(QIODevice::ReadOnly | QIODevice::Text) )
			return;
		mLogText->document()->setPlainText( read.readAll() );
		mPathEdit->setText( mJobCommandHistory.stdOut() );
	}
	mLogText->moveCursor(QTextCursor::End);
}

