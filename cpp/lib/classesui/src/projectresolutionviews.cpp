
#include <qdialogbuttonbox.h>
#include <qmessagebox.h>
#include <qpushbutton.h>
#include <qregexp.h>

#include "projectresolutionviews.h"


ProjectResolutionWidget::ProjectResolutionWidget(QWidget * parent)
: QWidget( parent )
{
	setupUi(this);

	mProjectCombo->setSpecialItemText( "Select Project" );
	
	Expression resString = ProjectResolution::c.Width.cast(QVariant::String) + "x" + ProjectResolution::c.Height.cast(QVariant::String);
	Expression::sql("select width::text || 'x' || height::text from "
	" (SELECT width, height, count(*) FROM projectresolution WHERE coalesce(width,0) > 0 and coalesce(height,0) > 0 GROUP BY width, height )"
	" as iq where iq.count > 2 order by width asc").future()
		.then(this,SLOT(resolutionsFutureReady(ExpressionFuture)));
}

void ProjectResolutionWidget::setProjectResolution( ProjectResolution pr )
{
	mProjectResolution = pr;
	reset();
}

void ProjectResolutionWidget::reset()
{
	const ProjectResolution & pr(mProjectResolution);
	
	const bool valid = pr.isValid();

	mProjectCombo->setProject(pr.project());
	mProjectCombo->setEnabled(!pr.isRecord());

	if( pr.width() > 0 && pr.height() > 0 ) {
		QString res("%1x%2");
		res = res.arg(pr.width()).arg(pr.height());
		int resIdx = mResolutionCombo->findText(res);
		if( resIdx < 0 )
			mResolutionCombo->insertItem(resIdx=0, res);
		mResolutionCombo->setCurrentIndex(resIdx);
	} else
		mResolutionCombo->setCurrentIndex(mResolutionCombo->findText("1280x720"));
		
	mNameEdit->setText(pr.name());
	mPrimaryOutputCheck->setChecked(valid ? pr.primaryOutput() : true);
		
	if(valid && pr.fps() > 0){
		QString fps = QString::number(pr.fps());
		int fpsIdx = mFramesPerSecondCombo->findText(fps);
		if( fpsIdx < 0 )
			mFramesPerSecondCombo->insertItem(fpsIdx=0,fps);
		mFramesPerSecondCombo->setCurrentIndex(fpsIdx);
	} else
		mFramesPerSecondCombo->setCurrentIndex(mFramesPerSecondCombo->findText("30"));
	
	mRunLengthEdit->setInterval(pr.runLength() > 0 ? pr.runLength() : Interval::fromString("0:01:00"));
	Interval spp = pr.pixelRenderTimeEstimate();
	mRenderTimeEstimateSpin->setValue(spp > 0 ? (spp.microseconds() / 1000000. + spp.seconds()) : 0.035);
	
	QDate start = pr.renderCrunchStart();
	if( start.isNull() ) start = QDate::currentDate();
	mRenderCrunchStart->setDate(start);
	QDate end = pr.renderCrunchEnd();
	if( end.isNull() || end < start ) end = start.addDays(14);
	mRenderCrunchEnd->setDate(end);
	mNotesEdit->setPlainText(pr.hardwareRequirements());
}

void ProjectResolutionWidget::apply( bool commit )
{
	QRegExp resExp("(\\d+)x(\\d+)");
	if( resExp.exactMatch(mResolutionCombo->currentText()) ) {
		mProjectResolution
			.setWidth( resExp.cap(1).toInt() )
			.setHeight( resExp.cap(2).toInt() );
	}
		
	mProjectResolution
		.setProject( mProjectCombo->project() )
		.setName( mNameEdit->text() )
		.setPrimaryOutput( mPrimaryOutputCheck->isChecked() )
		.setFps( mFramesPerSecondCombo->currentText().toInt() )
		.setRunLength( mRunLengthEdit->interval() )
		.setPixelRenderTimeEstimate( Interval(1) * mRenderTimeEstimateSpin->value() )
		.setRenderCrunchStart( mRenderCrunchStart->date() )
		.setRenderCrunchEnd( mRenderCrunchEnd->date() )
		.setHardwareRequirements( mNotesEdit->toPlainText() );
		
	if( commit )
		mProjectResolution.commit();
}

void ProjectResolutionWidget::resolutionsFutureReady(ExpressionFuture future)
{
	QString cur = mResolutionCombo->currentText();
	mResolutionCombo->clear();
	foreach( Record r, future.get() )
		mResolutionCombo->addItem( r.getValue(0).toString() );
	int idx = mResolutionCombo->findText(cur.size() ? cur : "1280x720");
	if( idx < 0 )
		mResolutionCombo->insertItem(idx=0, cur);
	mResolutionCombo->setCurrentIndex(idx);
}

ProjectResolutionDialog::ProjectResolutionDialog( QWidget * parent )
: QDialog( parent )
, mProjectResolutionWidget( nullptr )
{
	init();
}

ProjectResolutionDialog::ProjectResolutionDialog( const ProjectResolution & pr, QWidget * parent )
: QDialog( parent )
, mProjectResolutionWidget( nullptr )
, mDialogButtonBox( nullptr )
{
	init();
	mProjectResolutionWidget->setProjectResolution( pr );
}

void ProjectResolutionDialog::init()
{
	QLayout * layout = new QVBoxLayout(this);
	
	layout->addWidget( mProjectResolutionWidget = new ProjectResolutionWidget(this) );
	connect( mProjectResolutionWidget->mProjectCombo, SIGNAL(projectChanged(const Project&)), SLOT(slotCheckCanSave()) );
	connect( mProjectResolutionWidget->mRunLengthEdit, SIGNAL(validityChanged(bool)), SLOT(slotCheckCanSave()) );
	connect( mProjectResolutionWidget->mNameEdit, SIGNAL(textChanged(const QString&)), SLOT(slotCheckCanSave()) );
	mDialogButtonBox = new QDialogButtonBox( QDialogButtonBox::Save | QDialogButtonBox::Cancel, Qt::Horizontal, this );
	layout->addWidget( mDialogButtonBox );
	connect( mDialogButtonBox, SIGNAL( accepted() ), SLOT( accept() ) );
	connect( mDialogButtonBox, SIGNAL( rejected() ), SLOT( reject() ) );
}

void ProjectResolutionDialog::slotCheckCanSave()
{
	mDialogButtonBox->button( QDialogButtonBox::Save )->setEnabled(
		mProjectResolutionWidget->mProjectCombo->project().isRecord()
		&& mProjectResolutionWidget->mRunLengthEdit->valid()
		&& mProjectResolutionWidget->mNameEdit->text().size() );
}

void ProjectResolutionDialog::accept()
{
	// Check for a unique name
	mProjectResolutionWidget->apply(/*commit=*/false);
	ProjectResolution pr = mProjectResolutionWidget->projectResolution();
	ProjectResolution::_c & PR = ProjectResolution::c;
	if( pr.isUpdated(PR.Name) && (PR.Project == pr.project() && PR.Name == pr.name()).select().size() )
	{
		if( QMessageBox::question( this, "Duplicate Project Resolution Name", "There is already a project resolution named \"" + pr.name() + "\" for this project\n"
			"Do you wish to create a duplicate?", QMessageBox::Yes | QMessageBox::No ) == QMessageBox::No )
			return;
	}
	pr.commit();
	QDialog::accept();
}

ProjectResolution ProjectResolutionDialog::create( const Project & project, QWidget * parent )
{
	ProjectResolutionDialog prd( ProjectResolution().setProject(project), parent );
	prd.exec();
	return prd.projectResolutionWidget()->projectResolution();
}

ProjectResolution ProjectResolutionDialog::edit( const ProjectResolution & projectResolution, QWidget * parent )
{
	ProjectResolutionDialog prd( projectResolution, parent );
	prd.exec();
	return prd.projectResolutionWidget()->projectResolution();
}

