
import sys
from blur.build import *

path = os.path.dirname(os.path.abspath(__file__))
sippath = os.path.join(path,'sipQtWinMigrate')

try:
    os.mkdir(sippath)
except: pass

# Python module targets
SipTarget("pyqtwinmigrate",path,False,None,["sip:install","pyqt:install","qtwinmigrate"])

if __name__ == "__main__":
	build()
