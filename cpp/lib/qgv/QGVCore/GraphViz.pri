
#GraphViz librairie
DEFINES += WITH_CGRAPH
INCLUDEPATH += private .
QMAKE_CXXFLAGS += -DQGVCORE_LIB

unix {
# CONFIG += link_pkgconfig
# PKGCONFIG += libcdt libgvc libcgraph libgraph
	INCLUDEPATH+=/usr/include/graphviz
	LIBS+=-lcgraph -lgvc
}
win32 {
 #Configure Windows GraphViz path here :
 GRAPHVIZ_PATH = "E:/source/graphviz"
 DEFINES += WIN32_DLL
 DEFINES += GVDLL
 INCLUDEPATH += $$GRAPHVIZ_PATH/include/graphviz
 LIBS += -L$$GRAPHVIZ_PATH/lib/release/lib -lgvc -lcgraph -lgraph -lcdt
}
