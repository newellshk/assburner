/***************************************************************
QGVCore
Copyright (c) 2014, Bergont Nicolas, All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library.
***************************************************************/

#include <QVarLengthArray>
#include <QPainter>

#include "QGVScene.h"
// The following include allows the automoc to detect, that it must moc this class
#include "moc_QGVScene.cpp"
#include <QDebug>

#include <QGVNode.h>
#include <QGVEdge.h>
#include <QGVSubGraph.h>

#include <QGVCore.h>
#include <QGVGraphPrivate.h>
#include <QGVGvcPrivate.h>
#include <QGVEdgePrivate.h>
#include <QGVNodePrivate.h>

QGVScene::QGVScene(const QString &name, QObject *parent)
: QGraphicsScene(parent)
, mGridSize(0)
{
	_context = new QGVGvcPrivate(gvContext());
	_graph = new QGVGraphPrivate(agopen(name.toLocal8Bit().data(), Agdirected, NULL));
	//setGraphAttribute("fontname", QFont().family());
}

QGVScene::~QGVScene()
{
	clear();
	gvFreeLayout(_context->context(), _graph->graph());
	agclose(_graph->graph());
	gvFreeContext(_context->context());
	delete _graph;
	delete _context;
}

void QGVScene::setGraphAttribute(const QString &name, const QString &value)
{
	agattr(_graph->graph(), AGRAPH, name.toLocal8Bit().data(), value.toLocal8Bit().data());
}

void QGVScene::setNodeAttribute(const QString &name, const QString &value)
{
	agattr(_graph->graph(), AGNODE, name.toLocal8Bit().data(), value.toLocal8Bit().data());
}

void QGVScene::setEdgeAttribute(const QString &name, const QString &value)
{
	agattr(_graph->graph(), AGEDGE, name.toLocal8Bit().data(), value.toLocal8Bit().data());
}

QGVNode *QGVScene::addNode(const QString &label)
{
	QGVNode * ret = new QGVNode(this);
	ret->setLabel(label);
	return ret;
}

void QGVScene::addNode(QGVNode * node, QGVSubGraph *subgraph)
{
	Agnode_t * anode = agnode(subgraph ? subgraph->_sgraph->graph() : _graph->graph(), NULL, TRUE);
	node->_node = new QGVNodePrivate(anode);
	addItem(node);
	_nodes.append(node);
	if( subgraph ) {
		agsubnode(subgraph->_sgraph->graph(), anode, TRUE);
		subgraph->_nodes.append(node);
	}
}

void QGVScene::addEdge(QGVEdge * edge, QGVNode * source, QGVNode * target)
{
	Agedge_t* aedge = agedge(_graph->graph(), source->_node->node(), target->_node->node(), NULL, TRUE);
	if(aedge == NULL)
	{
		qWarning()<<"Invalid edge :";//<<label;
		return;
	}
	edge->_edge = new QGVEdgePrivate(aedge);
	_edges.append(edge);
	addItem(edge);
}

void QGVScene::addSubGraph(QGVSubGraph *subgraph, const QString & name, bool cluster, QGVSubGraph *parent)
{
    Agraph_t* sgraph;
	Agraph_t* spar = parent ? parent->_sgraph->graph() : _graph->graph();
    if(cluster)
		sgraph = agsubg(spar, ("cluster_" + name).toLocal8Bit().data(), TRUE);
    else
		sgraph = agsubg(spar, name.toLocal8Bit().data(), TRUE);

    if(sgraph == NULL)
    {
        qWarning()<<"Invalid subGraph :"<<name;
        return;
    }

	subgraph->_sgraph = new QGVGraphPrivate(sgraph);
	addItem(subgraph);
	_subGraphs.append(subgraph);
}

QGVEdge *QGVScene::addEdge(QGVNode *source, QGVNode *target, const QString &label)
{
	QGVEdge * edge = new QGVEdge(source,target);
	edge->setLabel(label);
	return edge;
}

QGVSubGraph *QGVScene::addSubGraph(const QString &name, bool cluster)
{
    Agraph_t* sgraph;
    if(cluster)
				sgraph = agsubg(_graph->graph(), ("cluster_" + name).toLocal8Bit().data(), TRUE);
    else
				sgraph = agsubg(_graph->graph(), name.toLocal8Bit().data(), TRUE);

    if(sgraph == NULL)
    {
        qWarning()<<"Invalid subGraph :"<<name;
        return 0;
    }

		QGVSubGraph *item = new QGVSubGraph(new QGVGraphPrivate(sgraph), this);
    addItem(item);
    _subGraphs.append(item);
    return item;
}

void QGVScene::setRootNode(QGVNode *node)
{
    Q_ASSERT(_nodes.contains(node));
		agset(_graph->graph(), "root", node->label().toLocal8Bit().data());
}

void QGVScene::loadLayout(const QString &text)
{
	_graph->setGraph(QGVCore::agmemread2(text.toLocal8Bit().constData()));

	if(gvLayout(_context->context(), _graph->graph(), "dot") != 0)
    {
        qCritical()<<"Layout render error"<<agerrors()<<QString::fromLocal8Bit(aglasterr());
        return;
    }

    //Debug output
		//gvRenderFilename(_context->context(), _graph->graph(), "png", "debug.png");

    //Read nodes and edges
	for (Agnode_t* node = agfstnode(_graph->graph()); node != NULL; node = agnxtnode(_graph->graph(), node))
    {
		QGVNode *inode = new QGVNode(new QGVNodePrivate(node), this);
		inode->updateLayout();
		addItem(inode);
		for (Agedge_t* edge = agfstout(_graph->graph(), node); edge != NULL; edge = agnxtout(_graph->graph(), edge))
        {
			QGVEdge *iedge = new QGVEdge(new QGVEdgePrivate(edge), this);
			iedge->updateLayout();
			addItem(iedge);
		}

	}
	update();
}

void QGVScene::applyLayout()
{
		if(gvLayout(_context->context(), _graph->graph(), "dot") != 0)
    {
        /*
         * Si plantage ici :
         *  - Verifier que les dll sont dans le repertoire d'execution
         *  - Verifie que le fichier "configN" est dans le repertoire d'execution !
         */
        qCritical()<<"Layout render error"<<agerrors()<<QString::fromLocal8Bit(aglasterr());
        return;
    }

    //Debug output
		//gvRenderFilename(_context->context(), _graph->graph(), "canon", "debug.dot");
		//gvRenderFilename(_context->context(), _graph->graph(), "png", "debug.png");

    //Update items layout
    foreach(QGVNode* node, _nodes)
        node->updateLayout();

    foreach(QGVEdge* edge, _edges)
        edge->updateLayout();

    foreach(QGVSubGraph* sgraph, _subGraphs)
        sgraph->updateLayout();
/*
    //Graph label
		textlabel_t *xlabel = GD_label(_graph->graph());
    if(xlabel)
    {
        QGraphicsTextItem *item = addText(xlabel->text);
				item->setPos(QGVCore::centerToOrigin(QGVCore::toPoint(xlabel->pos, QGVCore::graphHeight(_graph->graph())), xlabel->dimen.x, -4));
    }
*/
    update();
}

void QGVScene::clear()
{
	gvFreeLayout(_context->context(), _graph->graph());
    _nodes.clear();
    _edges.clear();
    _subGraphs.clear();
    QGraphicsScene::clear();
}

#include <QGraphicsSceneContextMenuEvent>
void QGVScene::contextMenuEvent(QGraphicsSceneContextMenuEvent *contextMenuEvent)
{
    QGraphicsItem *item = itemAt(contextMenuEvent->scenePos(), QTransform());
    if(item)
    {
        item->setSelected(true);
        if(item->type() == QGVNode::Type)
            emit nodeContextMenu(qgraphicsitem_cast<QGVNode*>(item));
        else if(item->type() == QGVEdge::Type)
            emit edgeContextMenu(qgraphicsitem_cast<QGVEdge*>(item));
        else if(item->type() == QGVSubGraph::Type)
            emit subGraphContextMenu(qgraphicsitem_cast<QGVSubGraph*>(item));
        else
            emit graphContextMenuEvent();
    }
    QGraphicsScene::contextMenuEvent(contextMenuEvent);
}

void QGVScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    QGraphicsItem *item = itemAt(mouseEvent->scenePos(), QTransform());
    if(item)
    {
        if(item->type() == QGVNode::Type)
            emit nodeDoubleClick(qgraphicsitem_cast<QGVNode*>(item));
        else if(item->type() == QGVEdge::Type)
            emit edgeDoubleClick(qgraphicsitem_cast<QGVEdge*>(item));
        else if(item->type() == QGVSubGraph::Type)
            emit subGraphDoubleClick(qgraphicsitem_cast<QGVSubGraph*>(item));
    }
    QGraphicsScene::mouseDoubleClickEvent(mouseEvent);
}

void QGVScene::setGridSize( int gridSize )
{
	if( mGridSize == gridSize ) return;
	
	mGridSize = gridSize;
	update();
}

int QGVScene::gridSize() const
{
	return mGridSize;
}

void QGVScene::drawBackground(QPainter * painter, const QRectF & rect)
{
	if( mGridSize <= 0 ) return;
	
    const qreal left = int(rect.left()) - (int(rect.left()) % mGridSize);
    const qreal top = int(rect.top()) - (int(rect.top()) % mGridSize);

    QVarLengthArray<QLineF, 100> lines;

    for (qreal x = left; x < rect.right(); x += mGridSize)
        lines.append(QLineF(x, rect.top(), x, rect.bottom()));
    for (qreal y = top; y < rect.bottom(); y += mGridSize)
        lines.append(QLineF(rect.left(), y, rect.right(), y));

    painter->setRenderHint(QPainter::Antialiasing, false);

    painter->setPen(QColor(Qt::lightGray).lighter(110));
    painter->drawLines(lines.data(), lines.size());
    painter->setPen(Qt::black);
}
