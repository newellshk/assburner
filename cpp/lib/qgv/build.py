
from blur.build import *
import platform

path = os.path.dirname(os.path.abspath(__file__))
qgv = QMakeTarget( 'qgv', path, 'QGraphViz.pro' )
arch = platform.architecture()
if arch[0] == '64bit' and arch[1].startswith('Windows'):
    qgv.build = lambda: None

rpm = RPMTarget('qgvrpm','libqgraphviz',path,'../../../rpm/spec/qgv.spec.template','1.0')
rpm.pre_deps = ["blurpythonbuildrpm"]

if __name__ == "__main__":
	build()
