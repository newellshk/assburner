// -*- C++;indent-tabs-mode: t; tab-width: 4; c-basic-offset: 4; -*-

/**
* KImageIO Routines to read (and perhaps in the future, write) images
* in the high dynamic range EXR format.
* Copyright (c) 2003, Brad Hards <bradh@frogmouth.net>
*
* This library is distributed under the conditions of the GNU LGPL.
*/

#include <half.h>
#include <ImfRgbaFile.h>
#include <ImfStandardAttributes.h>
#include <ImathBox.h>
#include <ImfInputFile.h>
#include <ImfBoxAttribute.h>
#include <ImfChannelList.h>
#include <ImfChannelListAttribute.h>
#include <ImfCompressionAttribute.h>
#include <ImfFloatAttribute.h>
#include <ImfIntAttribute.h>
#include <ImfLineOrderAttribute.h>
#include <ImfPixelType.h>
#include <ImfRgba.h>
#include <ImfOutputFile.h>
#include <ImfStringAttribute.h>
#include <ImfVecAttribute.h>
#include <ImfArray.h>
#include <ImfConvert.h>
#include <ImfVersion.h>
#include <IexThrowErrnoExc.h>

#include <iostream>

#include <QImage>
#include <qcolor.h>
#include <QDataStream>
#include <QImageIOPlugin>
#include <qvariant.h>
#include <qdebug.h>

#include "exr.h"

class K_IStream: public Imf::IStream
{
public:
	K_IStream( QIODevice *dev, QByteArray fileName ):
		IStream( fileName.data() ), m_dev ( dev )
	{}

	virtual bool  read( char c[], int n );
	virtual Imf::Int64 tellg( );
	virtual void seekg( Imf::Int64 pos );
	virtual void clear( );

private:
	QIODevice *m_dev;
};

bool K_IStream::read( char c[], int n )
{
	qint64 result = m_dev->read( c, n );
	if ( result > 0 ) {
		return true;
	} else if ( result == 0 ) {
		throw Iex::InputExc( "Unexpected end of file" );
	} else // negative value {
		Iex::throwErrnoExc( "Error in read", result );
	return false;
}

Imf::Int64 K_IStream::tellg( )
{
	return m_dev->pos();
}

void K_IStream::seekg( Imf::Int64 pos )
{
	m_dev->seek( pos );
}

void K_IStream::clear( )
{
	// TODO
}

class K_OStream : public Imf::OStream
{
public:
	K_OStream( QIODevice *dev, QByteArray fileName )
	: OStream( fileName.data() ), m_dev( dev )
	{}
	
	virtual void write(const char c[], int n)
	{
		m_dev->write(c,n);
	}
	
	virtual Imf::Int64 tellp()
	{
		return m_dev->pos();
	}
	
	virtual void seekp(Imf::Int64 pos)
	{
		m_dev->seek(pos);
	}
	
protected:
	QIODevice *m_dev;
};

/* this does a conversion from the ILM Half (equal to Nvidia Half)
 * format into the normal 32 bit pixel format. Process is from the
 * ILM code.
 */
QRgb RgbaToQrgba(struct Imf::Rgba imagePixel, bool applyGamma)
{
	if( !applyGamma ) {
		return qRgba( char (Imath::clamp ( imagePixel.r * 255.0f, 0.f, 255.f ) + .5f ),
				  char (Imath::clamp ( imagePixel.g * 255.0f, 0.f, 255.f ) + .5f ),
				  char (Imath::clamp ( imagePixel.b * 255.0f, 0.f, 255.f ) + .5f ),
				  char (Imath::clamp ( imagePixel.a * 255.0f, 0.f, 255.f ) + .5f ) );
	}

	float r,g,b;
	//  1) Compensate for fogging by subtracting defog
	//     from the raw pixel values.
	// Response: We work with defog of 0.0, so this is a no-op

	//  2) Multiply the defogged pixel values by
	//     2^(exposure + 2.47393).
	// Response: We work with exposure of 0.0.
	// (2^2.47393) is 5.55555
	#define EXR_EXPOSURE 5.55555
	//#define EXR_EXPOSURE 11.1111f
	r = imagePixel.r * EXR_EXPOSURE;
	g = imagePixel.g * EXR_EXPOSURE;
	b = imagePixel.b * EXR_EXPOSURE;

	//  3) Values, which are now 1.0, are called "middle gray".
	//     If defog and exposure are both set to 0.0, then
	//     middle gray corresponds to a raw pixel value of 0.18.
	//     In step 6, middle gray values will be mapped to an
	//     intensity 3.5 f-stops below the display's maximum
	//     intensity.
	// Response: no apparent content.

	//  4) Apply a knee function.  The knee function has two
	//     parameters, kneeLow and kneeHigh.  Pixel values
	//     below 2^kneeLow are not changed by the knee
	//     function.  Pixel values above kneeLow are lowered
	//     according to a logarithmic curve, such that the
	//     value 2^kneeHigh is mapped to 2^3.5 (in step 6,
	//     this value will be mapped to the the display's
	//     maximum intensity).
	// Response: kneeLow = 0.0 (2^0.0 => 1); kneeHigh = 5.0 (2^5 =>32)
    if (r > 1.0)
		r = 1.0 + Imath::Math<float>::log ((r-1.0) * 0.184874 + 1) / 0.184874;
    if (g > 1.0)
		g = 1.0 + Imath::Math<float>::log ((g-1.0) * 0.184874 + 1) / 0.184874;
    if (b > 1.0)
		b = 1.0 + Imath::Math<float>::log ((b-1.0) * 0.184874 + 1) / 0.184874;
//
//  5) Gamma-correct the pixel values, assuming that the
//     screen's gamma is 0.4545 (or 1/2.2).
	r = Imath::Math<float>::pow (r, 0.4545);
	g = Imath::Math<float>::pow (g, 0.4545);
	b = Imath::Math<float>::pow (b, 0.4545);

//  6) Scale the values such that pixels middle gray
//     pixels are mapped to 84.66 (or 3.5 f-stops below
//     the display's maximum intensity).
//
//  7) Clamp the values to [0, 255].
	return qRgba( char (Imath::clamp ( r * 84.66f, 0.f, 255.f ) + .5f ),
				  char (Imath::clamp ( g * 84.66f, 0.f, 255.f ) + .5f ),
				  char (Imath::clamp ( b * 84.66f, 0.f, 255.f ) + .5f ),
				  char (Imath::clamp ( imagePixel.a * 255.f, 0.f, 255.f ) + .5f ) );
}

EXRHandler::EXRHandler( bool applyGammaCorrection )
: mApplyGammaCorrection( applyGammaCorrection )
{
}

bool EXRHandler::canRead() const
{
	return canRead( device() );
}

QByteArray EXRHandler::name() const
{
	// TODO
	return QByteArray("exr");
}

bool EXRHandler::read( QImage *outImage )
{
    try
    {
		int dataWidth, dataHeight, dataPosX, dataPosY;
		
		K_IStream istr( device(), QByteArray() );
		Imf::RgbaInputFile file( istr );
		Imath::Box2i dw = file.dataWindow();

		dataWidth = dw.max.x - dw.min.x + 1;
		dataHeight = dw.max.y - dw.min.y + 1;
		dataPosX = dw.min.x;
		dataPosY = dw.min.y;

		Imf::Array2D<Imf::Rgba> pixels;
		pixels.resizeErase(dataHeight, dataWidth);

		file.setFrameBuffer(&pixels[0][0] - dataPosX - dataPosY * dataWidth, 1, dataWidth);
		file.readPixels(dataPosY, dw.max.y);

		int displayWidth, displayHeight, displayPosX, displayPosY;
		
		Imath::Box2i displayWindow = file.displayWindow();
		displayWidth = displayWindow.max.x - displayWindow.min.x + 1;
		displayHeight = displayWindow.max.y - displayWindow.min.y + 1;
		displayPosX = displayWindow.min.x;
		displayPosY = displayWindow.min.y;
		
		//qDebug() << "Data window ((" << dw.min.x << "," << dw.min.y << "),(" << dw.max.x << "," << dw.max.y << "))" << endl;
		//qDebug() << "Display window ((" << displayWindow.min.x << "," << displayWindow.min.y << "),(" << displayWindow.max.x << "," << displayWindow.max.y << "))" << endl;
		
		QImage image(displayWidth, displayHeight, QImage::Format_ARGB32);
		if( image.isNull())
			return false;

		if( displayWindow != dw )
			image.fill( QColor(0,0,0,0) );
		
		// somehow copy pixels into image
		for ( int y=0; y < displayHeight; y++ ) {
			if( y < dataPosY - displayPosY ) continue;
			if( y > dw.max.y - displayPosY ) break;
			for ( int x=0; x < displayWidth; x++ ) {
				if( x < dataPosX - displayPosX ) continue;
				if( x > dw.max.x - displayPosX ) break;
				// copy pixels(x,y) into image(x,y)
				image.setPixel( x, y, RgbaToQrgba( pixels[y-dataPosY][x-dataPosX], mApplyGammaCorrection ) );
			}
		}

		*outImage = image;

		return true;
    }
    catch (const std::exception &exc)
    {
		qWarning() << exc.what() << endl;
        return false;
    }
}


static Imf::Rgba QRgbaToRgba(QRgb qrgb, bool applyGamma = false)
{
	return Imf::Rgba( qRed(qrgb) / 255.0, qGreen(qrgb) / 255.0, qBlue(qrgb) / 255.0,  qAlpha(qrgb) / 255.0 );
}


bool EXRHandler::write( const QImage &image )
{
	try {
		qDebug() << "Writing exr!!\n";
		Imf::Header hdr(image.width(), image.height()
			); /*, float pixelAspectRatio = 1,
			const IMATH_NAMESPACE::V2f &screenWindowCenter = IMATH_NAMESPACE::V2f (0, 0),
			float screenWindowWidth = 1,
			LineOrder lineOrder = INCREASING_Y,
			Compression = ZIP_COMPRESSION); */
		hdr.channels().insert("R", Imf::Channel(Imf::HALF));
		hdr.channels().insert("G", Imf::Channel(Imf::HALF));
		hdr.channels().insert("B", Imf::Channel(Imf::HALF));
		hdr.channels().insert("A", Imf::Channel(Imf::HALF));
		
		K_OStream ostr( device(), QByteArray() );
		Imf::Array2D<Imf::Rgba> pixels;
		pixels.resizeErase(image.height(), image.width());
		for( int y=0, yend=image.height(); y < yend; y++ ) {
			const QRgb * scanline = (const QRgb*)image.constScanLine(y);
			for( int x=0, xend=image.width(); x < xend; x++ )
				pixels[y][x] = QRgbaToRgba(scanline[x]);
		}
		Imf::RgbaOutputFile file( ostr, hdr, Imf::WRITE_RGBA );
		file.setFrameBuffer(&pixels[0][0],1,image.width());
		file.writePixels(image.height());

	} catch ( std::exception & e ) {
		qDebug() << "Got exception writing exr " << e.what() << "\n";
		return false;
	}
	return true;
}


bool EXRHandler::canRead(QIODevice *device)
{
    if (!device) {
        qWarning("EXRHandler::canRead() called with no device");
        return false;
    }

    QByteArray head = device->peek(4);
    //qDebug() << "Exr header" << head << "Size: " << head.size();
	
    return Imf::isImfMagic( head.data() );
}


QVariant EXRHandler::option(ImageOption option) const
{
	if( option == QImageIOHandler::Gamma )
		return mApplyGammaCorrection;
	return QVariant();
}

void EXRHandler::setOption(ImageOption option, const QVariant &value)
{
	if( option == QImageIOHandler::Gamma && value.type() == QVariant::Bool )
		mApplyGammaCorrection = value.toBool();
}

bool EXRHandler::supportsOption(ImageOption option) const
{
	if( option == QImageIOHandler::Gamma )
		return true;
	return false;
}

/* --- Plugin --- */

QStringList EXRPlugin::keys() const
{
	return QStringList() << "exr" << "EXR" << "exr_nogamma";
}

QImageIOPlugin::Capabilities EXRPlugin::capabilities(QIODevice *device, const QByteArray &format) const
{
    if ( format == "exr" || format == "EXR" || format == "exr_nogamma" )
		return Capabilities(CanRead|CanWrite);
    if ( !format.isEmpty() )
        return 0;
    if ( !device->isOpen() )
        return 0;

    Capabilities cap;
    if (device->isReadable() && EXRHandler::canRead(device))
        cap |= CanRead;
	if (device->isWritable())
		cap |= CanWrite;
    return cap;
}

QImageIOHandler *EXRPlugin::create(QIODevice *device, const QByteArray &format) const
{
    QImageIOHandler *handler = new EXRHandler(format != "exr_nogamma");
    handler->setDevice(device);
    handler->setFormat(format);
    return handler;
}

Q_EXPORT_STATIC_PLUGIN( EXRPlugin )
Q_EXPORT_PLUGIN2( exr, EXRPlugin )
