
from PyQt4.QtGui import *
import sys

app = QApplication([])
QApplication.addLibraryPath("c:/blur/common/")

imgTestPath = "test.exr"
#imgTestPath ='/mnt/morlock006/assfreezer_Display/E_Key_0443.exr'
#if sys.platform=='win32':
#	imgTestPath='c:/assfreezer_Display/E_Key_0443.exr'
	
def readWithBlackBG( format = '', fileName=imgTestPath ):
	imgReader = QImageReader(fileName,format)
	img = imgReader.read()
	imgRet = QImage(img.width(),img.height(),QImage.Format_ARGB32)
	p = QPainter(imgRet)
	p.fillRect(0,0,imgRet.width(),imgRet.height(),QColor(0,0,0))
	p.drawImage(0,0,img)
	p.end()
	return QPixmap.fromImage(imgRet)
	
def rewrite():
	imgReader = QImageReader(imgTestPath,'exr_nogamma')
	img = imgReader.read()
	imgWriter = QImageWriter('test2.exr','exr')
	imgWriter.write(img)
	
lbl = QLabel(None)
lbl.setWindowTitle( 'EXR gamma adjusted' )
lbl.setPixmap( readWithBlackBG() )
lbl.show()

lbl2 = QLabel(None)
lbl2.setWindowTitle( 'EXR not gamma adjusted' )
lbl2.setPixmap( readWithBlackBG('exr_nogamma') )
lbl2.show()

rewrite()

lbl3 = QLabel(None)
lbl3.setWindowTitle( 'EXR after save' )
lbl3.setPixmap( readWithBlackBG(fileName='test2.exr') )
lbl3.show()

app.exec_()
