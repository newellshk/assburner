

; DESCRIPTION: Qt installer script
; (C) Blur Studio 2005-2012

!define MUI_VERSION "4.8.6"
!define MUI_ITERATION "8"
!define MUI_PRODUCT "Blur Qt ${MUI_VERSION}-${MUI_ITERATION} DLLs"
!define UNINSTALL_PATH "C:\blur\uninstall"

!if ${PLATFORM} == "win32-msvc2005_64"
	!define _64_BIT
!endif
!if ${PLATFORM} == "win32-msvc2008_64"
	!define _64_BIT
!endif
!if ${PLATFORM} == "win32-msvc2010_64"
	!define _64_BIT
!endif
!if ${PLATFORM} == "win32-msvc2012_64"
	!define _64_BIT
!endif

!define ILMBASE_PATH "e:\ilmbase_${COMPILER}\lib\"
!define OPENEXR_PATH "e:\openexr_${COMPILER}\lib\"
!define ZLIB_PATH "e:\zlib_${COMPILER}\bin\"

!ifdef _64_BIT
	!define INSTALL_PATH 'C:\windows\system32\blur64'
	!define QTDIR "$%QTDIR%"

	;!define QTDIR "C:\Qt\${MUI_VERSION}_64"
	!define MUI_UNINSTALL_NAME "Blur Qt ${MUI_VERSION}-${MUI_ITERATION} 64Bit DLLs"
	!define UNINSTALL_FILE "qt_uninstall_64.exe"
	!define BIT_NAME 'x86_64'
	!define PG_BIN_PATH "E:\Postgresql-9.4\bin\"
	!define OPENSSL_PATH "e:\openssl-Win64\"

!else
	!define INSTALL_PATH 'C:\blur\common'
	!define QTDIR "$%QTDIR%"

	;!define QTDIR "C:\Qt\${MUI_VERSION}"
	!define MUI_UNINSTALL_NAME "Blur Qt ${MUI_VERSION}-${MUI_ITERATION} 32Bit DLLs"
	!define UNINSTALL_FILE "qt_uninstall.exe"
	!define BIT_NAME 'x86'
	!define PG_BIN_PATH "E:\postgresql-9.4_win32\bin\"
	!define OPENSSL_PATH "e:\openssl-Win32\"
!endif

InstallDir ${INSTALL_PATH}

Name "${MUI_PRODUCT} ${MUI_VERSION} ${PLATFORM}"

!include "MUI.nsh"
!include "x64.nsh"
!include FileFunc.nsh	; Provide GetOptions and GetParameters
!include "..\..\..\nsis\LastInstall.nsh"
!include "..\..\..\nsis\FileSystem.nsh"
!include "..\..\..\nsis\SoftwareINI.nsh"
!include "..\..\..\nsis\StrCount.nsh"
!include "..\..\..\nsis\ReplaceSubStr.nsh"
!include "EnvVarUpdate.nsh"

; Name of resulting executable installer
!define OUT_FILE "qt_install_${MUI_VERSION}-${MUI_ITERATION}_${PLATFORM}.exe"
OutFile "${OUT_FILE}"

!define MUI_FINISHPAGE
!define MUI_FINISHPAGE_NOREBOOTSUPPORT
!define MUI_HEADERBITMAP "assfreezer.bmp"

; The icon for the installer title bar and .exe file
;!define MUI_ICON "myapp.ico"
; I want to use my product logo in the installer
;!define MUI_HEADERIMAGE
; Here is my product logo
;!define MUI_HEADERIMAGE_BITMAP "myapp.bmp"
; I want the logo to be on the right side of the installer, not the left
;!define MUI_HEADERIMAGE_RIGHT
; I've written a function that I want to be called when the user cancels the installation
;!define MUI_CUSTOMFUNCTION_ABORT myOnAbort
; Override the text on the Finish Page
!define MUI_FINISHPAGE_TITLE "${MUI_PRODUCT} Installation Complete"
!define MUI_FINISHPAGE_TEXT "\n\r\nClick Finish now to close the installation program."

#SilentInstall silent

!insertmacro MUI_PAGE_LICENSE '${QTDIR}\.LICENSE-DESKTOP-US'
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES ; File installation page

Section "install"
!ifdef _64_BIT
   ${DisableX64FSRedirection}
#	Delete "${INSTDIR}\QtAssistantClient4.dll"
	Delete "${INSTDIR}\QtCLucene4.dll"
	Delete "${INSTDIR}\QtCore4.dll"
	Delete "${INSTDIR}\QtDeclarative4.dll"
	Delete "${INSTDIR}\QtDesigner4.dll"
	Delete "${INSTDIR}\QtDesignerComponents4.dll"
	Delete "${INSTDIR}\QtGui4.dll"
	Delete "${INSTDIR}\QtHelp4.dll"
	Delete "${INSTDIR}\QtMultimedia4.dll"
	Delete "${INSTDIR}\QtNetwork4.dll"
	Delete "${INSTDIR}\QtOpenGL4.dll"
	Delete "${INSTDIR}\QtScript4.dll"
	Delete "${INSTDIR}\QtScriptTools4.dll"
	Delete "${INSTDIR}\QtSql4.dll"
	Delete "${INSTDIR}\QtSvg4.dll"
	Delete "${INSTDIR}\QtTest4.dll"
	Delete "${INSTDIR}\QtWebKit4.dll"
	Delete "${INSTDIR}\QtXml4.dll"
#	Delete "${INSTDIR}\QtXmlPatterns4.dll"
	Delete "${INSTDIR}\QtTest4.dll"
	Delete "${INSTDIR}\phonon4.dll"
	Delete "${INSTDIR}\qscintilla2.dll"
	RMDir /r "${INSTDIR}\sqldrivers"
	RMDir /r "${INSTDIR}\imageformats"
!endif

	SetOutPath $INSTDIR
#	File "${QTDIR}\bin\QtAssistantClient4.dll"
	File "${QTDIR}\bin\QtCLucene4.dll"
	File "${QTDIR}\bin\QtCore4.dll"
	File "${QTDIR}\bin\QtDeclarative4.dll"
	File "${QTDIR}\bin\QtDesigner4.dll"
	File "${QTDIR}\bin\QtDesignerComponents4.dll"
	File "${QTDIR}\bin\QtGui4.dll"
	File "${QTDIR}\bin\QtHelp4.dll"
	File "${QTDIR}\bin\QtMultimedia4.dll"
	File "${QTDIR}\bin\QtNetwork4.dll"
	File "${QTDIR}\bin\QtOpenGL4.dll"
	File "${QTDIR}\bin\QtScript4.dll"
	File "${QTDIR}\bin\QtScriptTools4.dll"
	File "${QTDIR}\bin\QtSql4.dll"
	File "${QTDIR}\bin\QtSvg4.dll"
	File "${QTDIR}\bin\QtTest4.dll"
	File "${QTDIR}\bin\QtWebKit4.dll"
	File "${QTDIR}\bin\QtXml4.dll"
#	File "${QTDIR}\bin\QtXmlPatterns4.dll"
	File "${QTDIR}\bin\QtTest4.dll"
	File "${QTDIR}\bin\phonon4.dll"
	File ..\qscintilla\Qt4\qscintilla2.dll
	File ..\qtwinmigrate\lib\QtSolutions_MFCMigrationFramework-2.8.dll

;	File ..\..\..\binaries\krb5_32.dll
	File "${ILMBASE_PATH}\Half.dll"
	File "${ILMBASE_PATH}\IlmThread-2_2.dll"
	File "${ILMBASE_PATH}\Iex-2_2.dll"
	File "${ILMBASE_PATH}\Imath-2_2.dll"
	File "${ZLIB_PATH}\zlib.dll"
	;File "${ILMBASE_PATH}\zlibwapi.dll"
	File "${OPENEXR_PATH}\IlmImf-2_2.dll"
;	File ..\..\..\binaries\comerr32.dll
;	File ..\..\..\binaries\libtiff3.dll
;	File ..\..\..\binaries\libimage.dll
;	File ..\..\..\binaries\libpng13.dll
;	File ..\..\..\binaries\libiconv-2.dll

;;	Needed for qt with -openssl-linked, provides https support
	File "${OPENSSL_PATH}\libeay32.dll"
	File "${OPENSSL_PATH}\ssleay32.dll"

	RMDir /r $INSTDIR\sqldrivers
	SetOutPath $INSTDIR\sqldrivers

	File "${QTDIR}\plugins\sqldrivers\qsqlpsql4.dll"
	File "${QTDIR}\plugins\sqldrivers\qsqlite4.dll"

;; Needed for libpq.dll
	SetOutPath $INSTDIR
	File "${PG_BIN_PATH}\libpq.dll"
!ifdef _64_BIT
	File "${PG_BIN_PATH}\iconv.dll"
	File "${PG_BIN_PATH}\libintl-8.dll"
!else
	File "${PG_BIN_PATH}\libiconv.dll"
	File "${PG_BIN_PATH}\intl.dll"
!endif

	SetOutPath $INSTDIR\phonon_backend
	File /r "${QTDIR}\plugins\phonon_backend\*.dll"

	SetOutPath $INSTDIR\imageformats
	Delete $INSTDIR\imageformats\qjpeg1.dll
	Delete $INSTDIR\imageformats\tif.dll
	File "${QTDIR}\plugins\imageformats\qjpeg4.dll"
	File "${QTDIR}\plugins\imageformats\qgif4.dll"
	File "${QTDIR}\plugins\imageformats\qico4.dll"
	File "${QTDIR}\plugins\imageformats\qmng4.dll"
	File "${QTDIR}\plugins\imageformats\qsvg4.dll"
	File "${QTDIR}\plugins\imageformats\qtiff4.dll"

	File ..\qimageio\exr\exr.dll
	File ..\qimageio\tga\tga.dll
	File ..\qimageio\sgi\sgi.dll
	File ..\qimageio\rla\rla.dll

	# Register the uninstaller only if a cmd line arg was not passed in
	ClearErrors
	${GetParameters} $R0
	${GetOptions} $R0 "/noRegisterPath" $R1
	IfErrors 0 finishRegisterPath
		${EnvVarUpdate} $R0 "PATH" "P" "HKLM" $INSTDIR
		${If} $R0 == ''
			${If} ${Silent}
			${Else}
				MessageBox MB_OK "Unable to update the path environment variable automatically.$\r$\nYou will need to add $INSTDIR; at the START path environment variable. Including the semi-colon.$\r$\n$\r$\nYou can copy this message using Ctrl+C."
			${EndIf}
			# This way we can check the error level to see if a problem occured when installing silently
			# We let the installer finish installing because this error can be fixed by the user.
			SetErrorLevel 4
		${Else}
			DetailPrint "Path Updated: $R0"
		${EndIf}
	finishRegisterPath:
	
	# Calculate the uninstaller path
	!insertmacro StrCount $INSTDIR ":\windows\" ""
	Pop $1
	${If} $1 == 1
		# Installing in the windows directory, Nsis doesn't allow making .exe's in the windows dir so install it here.
		Push ""
		Push "${UNINSTALL_PATH}\${UNINSTALL_FILE}"
	${Else}
		# Installing outside the windows directory, so create the installer inside the installed directory
		Push "$INSTDIR"
		Push "$INSTDIR\${UNINSTALL_FILE}"
	${EndIf}
	Pop $1
	Pop $2
	
	# Register the uninstaler only if a cmd line arg was not passed in
	ClearErrors
	${GetParameters} $R0
	${GetOptions} $R0 "/noUninstallerReg" $R1
	IfErrors 0 makeInstaller
		!ifdef _64_BIT
			SetRegView 64
		!endif
		# Can't store \'s as a registry key so replace with /
		!insertmacro ReplaceSubStr $INSTDIR "\" "/"
		
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_UNINSTALL_NAME}$MODIFIED_STR" "DisplayName" "${MUI_UNINSTALL_NAME} $2 (remove only)"
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_UNINSTALL_NAME}$MODIFIED_STR" "UninstallString" $1
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_UNINSTALL_NAME}$MODIFIED_STR" "Publisher" "Blur Studio"
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_UNINSTALL_NAME}$MODIFIED_STR" "DisplayVersion" "${MUI_VERSION}-${MUI_ITERATION}"
	makeInstaller:
		CreateDirectory ${UNINSTALL_PATH}
		WriteUninstaller $1
		!insertmacro SetLastInstall $1
	
	# Update software.ini so we can track what software is installed on each system
	!insertmacro UpdateSettingsINI ${BLUR_SOFTWARE_INI} "Qt4 ${BIT_NAME}" "${MUI_VERSION}" "${OUT_FILE}"
SectionEnd

Section "Uninstall"
	!ifdef _64_BIT
		SetRegView 64
		${DisableX64FSRedirection}
	!endif
	
	# Delete the uninstaller
	Delete "$INSTDIR\${UNINSTALL_FILE}"
	
	# Calculate the uninstaller path
	${If} $INSTDIR == ${UNINSTALL_PATH}
		Push ${INSTALL_PATH}
	${Else}
		Push $INSTDIR
	${EndIf}
	# $1 is the directory Qt was installed to
	Pop $1
	
	#Delete Files and directory
	Delete $1\QtCore4.dll
	Delete $1\QtDeclarative4.dll
	Delete $1\QtGui4.dll
	Delete $1\QtOpenGL4.dll
	Delete $1\QtNetwork4.dll
	Delete $1\QtSql4.dll
	Delete $1\QtHelp4.dll
	Delete $1\QtScript4.dll
	Delete $1\QtScriptTools4.dll
	Delete $1\QtSvg4.dll
	Delete $1\QtTest4.dll
	Delete $1\QtWebKit4.dll
	Delete $1\QtXml4.dll
#	Delete $1\QtXmlPatterns4.dll
	Delete $1\QtCLucene4.dll
	Delete $1\QtAssistantClient4.dll
	Delete $1\QtDesigner4.dll
	Delete $1\QtDesignerComponents4.dll
	Delete $1\QtTest4.dll
	Delete $1\QtMultimedia4.dll
	Delete $1\phonon4.dll
	Delete $1\qscintilla2.dll
	Delete $1\QtSolutions_MFCMigrationFramework-2.8.dll
	Delete $1\half.dll
	Delete $1\IlmThread-2_2.dll
	Delete $1\Iex-2_2.dll
	Delete $1\IlmImf-2_2.dll
	Delete $1\Imath-2_2.dll
	Delete $1\zlibwapi.dll
	Delete $1\libpq.dll
	Delete $1\libeay32.dll
	Delete $1\ssleay32.dll

	Delete $1\iconv.dll
	Delete $1\libintl-8.dll

	Delete $1\phonon_backend\phonon_ds94.dll
	Delete $1\phonon_backend\phonon_ds9d4.dll
	
	RMDir /r $1\sqldrivers
	RMDir /r $1\imageformats
	# delete phonon_backend if empty
	RMDir $1\phonon_backend
	# delete install directory if empty
	RMDir $1

	# Can't store \'s as a registry key so replace with /
	!insertmacro un.ReplaceSubStr $1 "\" "/"
	#Delete Unistall Registry Entries
	DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\${MUI_PRODUCT}"
	DeleteRegKey HKEY_LOCAL_MACHINE "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_UNINSTALL_NAME}$MODIFIED_STR"
	
	${un.EnvVarUpdate} $0 "PATH" "R" "HKLM" $1

SectionEnd

