
import sys, glob, shutil
from blur.build import *

path = os.path.dirname(os.path.abspath(__file__))
sippath = os.path.join(path,'sipStone')

try:
    os.mkdir(sippath)
except: pass

# Install the libraries
if sys.platform != 'win32':
	LibInstallTarget("stoneinstall",path,"stone","/usr/lib/")

# Python module targets
pc = SipTarget2("pystone",path,'Stone',False,None,["stone",'pyqjson'])
sst = SipTarget2("pystonestatic",path,'Stone',True,None,["stone"])
pc.ExtraIncludes = ['/usr/include/stone/']
pc.ExtraLibs += ['stone']

#if sys.platform=='win32':
	#lib = 'pyStone.lib'
	#srclib = 'Stone.lib'
	#if sys.argv.count("debug"):
		#lib = 'pyStone_d.lib'
		#srclib = 'Stone_d.lib'
	#sst.post_deps = [CopyTarget("pystonecopy",sippath,srclib,lib)]
#else:
	#sst.post_deps = [CopyTarget("pystonecopy",sippath,"libStone.a","libpyStone.a")]

# Create the main qmake target
stone = QMakeTarget("stone",path,"stone.pro")

if Target.qt_major() == 4:
	stone.pre_deps.append( 'qjson' )

# Create versioned dll and lib file
svnpri = WCRevTarget("stonelibsvnrevpri",path,"../..","svnrev-template.pri","svnrev.pri")
#post_deps.append(LibVersionTarget("stonelibversion","lib/stone","../..","stone"))

#sv = QMakeTarget("stoneversioned",path,"stone.pro",[],[svnpri])
#sv.Defines = ["versioned"]

#
#  RPMS
#
# We have to install our shared .pri files so that each rpm source package doesn't
# need it's own copies.  Therefore we'll use the libstone-dev package to install them
# so we need to copy them into the libstone source tarball
rpm = RPMTarget('stonerpm','libstone',path,'../../../rpm/spec/stone.spec.template','1.0')
rpm.pre_deps=['qjsonrpm']
rpm.Files.append('../../qmake_shared')

pyrpm = RPMTargetSip('pystonerpm','pystone',path,'../../../rpm/spec/pystone.spec.template','1.0')
pyrpm.pre_deps=['pyqjsonrpm']

if __name__ == "__main__":
	build()
