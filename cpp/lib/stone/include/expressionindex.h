
#ifndef EXPRESSION_INDEX_H
#define EXPRESSION_INDEX_H

#include "index.h"
#include "expression.h"

namespace Stone {

	/**
 * Uses an expression and it's placeholders to calculate index
 * values.
 */
class STONE_EXPORT ExpressionIndex : public HashIndex
{
public:
	ExpressionIndex( Table * parent, const Expression & exp );
	~ExpressionIndex();

	//QList<QVariant> toCacheOrder( const QList<QVariant> & ) const;

	virtual RecordList select( const QList<QVariant> & vars, int lookupMode );
	virtual QList<QVariant> extractValues( const Record & record ) const;

	virtual void recordAdded( const Record & );
	virtual void recordRemoved( const Record & );
	virtual void recordUpdated( const Record &, const Record & );
	virtual void recordsIncoming( const RecordList &, bool ci = false, bool needLock = true );

protected:
	virtual bool checkMatch( const Record & record, const QList<QVariant> & args, int entryCount );
	RecordList selectHelper( const QList<QVariant> & args );
	
	Expression mExpression, mCacheExpression;
	int mCacheAllCount;
	bool mNeedIncomingFilter;
	// AutoFill placeholders must be positioned after non-autofill, this allows quick reordering of the lookup values
	//QList<int> mCacheOrder;
	friend class Expression;
};

}

#endif // EXPRESSION_INDEX_H