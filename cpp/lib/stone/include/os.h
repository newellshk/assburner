
#ifndef OS_H
#define OS_H

#include "blurqt.h"

enum OperatingSystem
{
	Invalid = 0,
	Linux = 1,
	OSX	 = 2,
	Windows = 4
};

STONE_EXPORT OperatingSystem currentOS();

#endif // OS_H
