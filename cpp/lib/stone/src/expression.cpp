
#include <algorithm>
#include <functional>
#include <vector>

#include <qdatetime.h>
#include <qwaitcondition.h>

#include "changeset.h"
#include "connection.h"
#include "database.h"
#include "expression.h"
#include "expressionindex.h"
#include "freezercore.h"
#include "interval.h"
#include "intlist.h"
#include "record.h"
#include "schema.h"
#include "stringmap.h"
#include "table.h"
#include "tableschema.h"

namespace Stone {

class _PlaceHolderExpression;
class _SetPlaceHolderExpression;
class _CacheExpression;

static ExpressionPrivate * getRecordLink( const Expression & base, const Record & r, bool nullExprForNullRecord=true );
static QList<Table*> findTables( ExpressionPrivate * p );

struct Context
{
	Context() : exp(0), par(0) {}
	Context(const ExpressionPrivate * _exp, const Context & _parent) : exp(_exp), par(&_parent) {}
	const ExpressionPrivate * exp;
	const Context * par;
};

struct ToStrContext : public Context
{
	ToStrContext(Expression::StringType st,Connection * _conn) : Context(), stringType(st), conn(_conn), needArray(true) {}
	ToStrContext(const ExpressionPrivate * _exp, const ToStrContext & _parent) : Context(_exp,_parent), stringType(_parent.stringType), conn(_parent.conn), needArray(true) {}
	Expression::StringType stringType;
	Connection * conn;
	bool needArray;
};

struct PrepStruct
{
	PrepStruct() : hasSubQuery(false), positionalArgsUsed(0), constantNoResults(false) {}
	ExpressionList orderByList, groupByList;
	Expression limit;
	bool hasSubQuery;
	int positionalArgsUsed;
	QList<QVariant> positionalArgs;
	QMap<QString,QVariant> namedArgs;
	bool removeCacheAllExpressions;
	bool constantNoResults;
};

struct PrepContext : public Context
{
	PrepContext(PrepStruct * ps) : Context(), prepStruct(ps) {}
	PrepContext(const ExpressionPrivate * _exp, const PrepContext & _parent) : Context(_exp,_parent), prepStruct(_parent.prepStruct) {}
	PrepStruct * prepStruct;
};

#define TS_CTX ToStrContext c(this,pc)
#define PREP_CTX PrepContext c(this,pc)

struct GatherPlaceHolders
{
	GatherPlaceHolders() : canExtractValues(true), insideSubQuery(0) {}
	QList<_SetPlaceHolderExpression*> setters;
	QList<_PlaceHolderExpression*> placeHolders;
	QList<ExpressionPrivate*> parents;
	bool canExtractValues;
	int insideSubQuery;
};

typedef std::vector<ExpressionPrivate*> ExpPrivVec;

int operatorPrecedenceMap [] =
{
	0, // Invalid_Expression,
	0, // Field_Expression,
	0, // Value_Expression,
	0, // RecordValue_Expression,
	10, // And_Expression,
	0, // Or_Expression,
	30, // Equals_Expression,
	20, // NEquals_Expression,
	20, // Not_Expression,
	40, // Larger_Expression,
	40, // LargerOrEquals_Expression,
	40, // Less_Expression,
	40, // LessOrEquals_Expression,
	65, // BitAnd_Expression,
	65, // BitOr_Expression,
	65, // BitXor_Expression,
	90, // Plus_Expression,
	90, // Minus_Expression,
	//65, // Concat_Expression
	80, // IsNull_Expression,
	70, // IsNotNull_Expression,
	60, // In_Expression,
	60, // NotIn_Expression,
	50, // Like_Expression,
	50, // ILike_Expression,
	0, // OrderBy_Expression,
	65, // RegEx_Expression,
	0, // Limit_Expression,
	0, // Query_Expression,
	0, // Table_Expression,
	// _CombinationExpression depends on these being in this order(starting index doesn't matter)
	0, // Union_Expression,
	0, // UnionAll_Expression,
	0, // Intersect_Expression,
	0, // IntersectAll_Expression,
	0, // Except_Expression,
	0, // ExceptAll_Expression,

	0, // Alias_Expression,
	0, // Values_Expression,
	0, // Null_Expression,
	0, // TableOid_Expression,
	0, // Sql_Expression,
	0, // Cast_Expression,
	0, // PlaceHolder_Expression,
	0, // SetPlaceHolder_Expression,
	0, // Cache_Expression,
	0, // GroupBy_Expression,
	0, // Coalesce_Expression
	0, // TempTable_Expression,
	0, // Future_Expression,

	80, // Is_Expression
	80, // IsNot_Expression
	80, // IsDistinctFrom_Expression
	80, // IsNotDistinctFrom_Expression

	0, // Between_Expression
	0, // Overlaps_Expression
	0, // OverlapsOpen_Expression

	0, // Replace_Expression,
	0, // Lower_Expression,
	0, // Upper_Expression,
	0, // Position_Expression,
	0, // Length_Expression,
	0, // Left_Expression,
	0, // Right_Expression,

	65, // GetValue_Expression
	0 // Contains_Expression
};
                                                                                                     
ExpressionPrivate * ExpressionPrivate::prepareForExec(const PrepContext & pc, Connection * conn)
{
	PREP_CTX;
	ref();
	QList<ExpressionPrivate*> ch = children();
	bool hasChildChange = false;
	for( int i = 0, end = ch.size(); i < end; ++i ) {
		ExpressionPrivate * child = const_cast<ExpressionPrivate*>(ch.at(i));
		ExpressionPrivate * nc = child->prepareForExec(c,conn);
		if( child != nc ) {
			if( nc )
				nc->ref();
			ch[i] = nc;
			hasChildChange = true;
			child->deref();
		}
	}
	if( hasChildChange )
		setChildren(ch);
	deref();
	return this;
}

bool ExpressionPrivate::needParens(const ToStrContext & pc) const
{
	return (pc.exp ? pc.exp->operatorPrecedence() : 0) >= operatorPrecedence();
}

int ExpressionPrivate::operatorPrecedence() const
{
	return operatorPrecedenceMap[type];
}

_CacheExpression * ExpressionPrivate::findParentCache(int & placeHoldersOnLeft,int & placeHolderCount, bool inValidNode)
{
	foreach( ExpressionPrivate * child, children() ) {
		if( inValidNode && child->type == Expression::Cache_Expression ) {
			ExpressionPrivate * ce = child;
			GatherPlaceHolders gph;
			ce->gatherPlaceHolders(&gph);
			placeHolderCount = gph.placeHolders.size();
			return (_CacheExpression*)ce;
		}
		if( child->type == Expression::PlaceHolder_Expression ) {
			placeHoldersOnLeft++;
		}
		// We don't currently combine multiple cache nodes, so we can't use cache results from inside an OR expression
		_CacheExpression * ce = child->findParentCache(placeHoldersOnLeft, placeHolderCount, inValidNode && (child->type != Expression::Or_Expression));
		if( ce )
			return ce;
	}
	return 0;
}
	
bool ExpressionPrivate::isOnlySupplemental() const
{
	foreach( ExpressionPrivate * child, children() )
		if( !child->isOnlySupplemental() )
			return false;
	return false;
}

bool ExpressionPrivate::containsWhereClause()
{
	if( type == Expression::Limit_Expression || type == Expression::OrderBy_Expression ||
		type == Expression::Field_Expression || type == Expression::Table_Expression ||
		type == Expression::Query_Expression || (type >= Expression::Union_Expression && type <= Expression::ExceptAll_Expression) || type == Expression::Value_Expression )
		return false;
	return true;
}

bool ExpressionPrivate::_load( QDataStream & ds )
{
	load(ds);
	int childCount, childType;
	ds >> childCount;
	QList<ExpressionPrivate*> children;
	for( int i=0; i < childCount; ++i ) {
		ds >> childType;
		ExpressionPrivate * child = Expression::_create((Expression::Type)childType);
		if( child ) {
			child->_load(ds);
			child->ref();
			children.append(child);
		} else
			return false;
	}
	setChildren(children);
	return true;
}

void ExpressionPrivate::_save( QDataStream & ds )
{
	QList<ExpressionPrivate*> cl = children();
	ds << type;
	save(ds);
	ds << cl.size();
	foreach( ExpressionPrivate *p, cl )
		p->_save(ds);
}

	/*
	void visit_worker(std::function<bool(ExpressionPrivate*,const ExpPrivVec &)> visitor, QList<Expression::Type> * typesToVisit, std::vector<ExpressionPrivate*> parentStack)
	{
		if( !typesToVisit || typesToVisit->contains(type) ) {
			if( !visitor(this,parentStack) )
				return;
		}
		parentStack.push_back(this);
		foreach( ExpressionPrivate * c, children() )
			c->visit_worker(visitor,typesToVisit,parentStack);
		parentStack.pop_back();
	}
	void visit(std::function<bool(ExpressionPrivate*,const ExpPrivVec &)> visitor, QList<Expression::Type> * typesToVisit=0)
	{
		std::vector<ExpressionPrivate*> parents;
		visit_worker(visitor,typesToVisit,parents);
	}
	QList<ExpressionPrivate*> collect(QList<Expression::Type> types)
	{
		QList<ExpressionPrivate*> ret;
		visit( [&] (ExpressionPrivate * p,const ExpPrivVec &) -> bool { ret.append(p); return true; }, &types );
		return ret;
	}
	*/
	
bool ExpressionPrivate::isMe( ExpressionPrivate * other, quint64 * myHashPtr )
{
	if( this == other ) return true;
	quint64 myHash = myHashPtr ? *myHashPtr : hash();
	if( myHash != other->hash() ) return false;
	QList<ExpressionPrivate*> kids = children();
	// Only a few OneArg expressions don't modify the hash
	if( kids.size() == 1 )
		return isMe(kids[0],&myHash);
	return false;
}

inline quint64 hash_combine(quint64 seed, quint64 hash)
{
	const quint64 k = 0x9ddfea08eb382d69ULL;
	quint64 a = k * (seed ^ hash);
	a ^= (a >> 47);
	a = k * (seed ^ a);
	a ^= (a >> 47);
	return a * k;
}

class _CompoundExpression : public ExpressionPrivate {
public:
	_CompoundExpression( Expression::Type type ) : ExpressionPrivate( type ) {}
	_CompoundExpression( const _CompoundExpression & other ) : ExpressionPrivate(other)
	{
		expressions.reserve(other.expressions.size());
		foreach( ExpressionPrivate * p, other.expressions )
			append(p->copy());
	}
	
	~_CompoundExpression() {
		foreach( ExpressionPrivate * p, expressions )
			p->deref();
	}
	
	ExpressionPrivate * copy() const { return new _CompoundExpression(*this); }
	
	quint64 hash() const
	{
		if( expressions.isEmpty() ) return type;
		QVector<quint64> childHashes;
		childHashes.reserve(expressions.size());
		foreach( ExpressionPrivate * c, expressions )
			childHashes.push_back(c->hash());
		qSort(childHashes);
		quint64 hash = childHashes[0];
		for( int i=1, end=childHashes.size(); i < end; ++i )
			hash = hash_combine(hash,childHashes[i]);
		return hash_combine(hash, type);
	}

	void append( ExpressionPrivate * p ) {
		if( p ) {
			p->ref();
			expressions.append(p);
		}
	}
	
	static QString joinStrings(const ToStrContext & c, QList<ExpressionPrivate*> kids, const QString & sep, bool needParens=false)
	{
		QStringList parts;
		foreach( ExpressionPrivate * child, kids )
			parts.append( child->toString(c) );
		QString ret = parts.join(sep);
		if( needParens )
			ret = "(" + ret + ")";
		return ret;
	}
	
	virtual QString toString(const ToStrContext & pc) const {
		TS_CTX;
		return joinStrings( c, expressions, separator(), needParens(pc) );
	}
	
	virtual QList<ExpressionPrivate*> children() const
	{ return expressions; }

	virtual void setChildren(const QList<ExpressionPrivate*> & children)
	{
		expressions = children;
		expressions.removeAll(0);
	}

	virtual bool isConstant() {
		foreach( ExpressionPrivate * c, children() )
			if( !c->isConstant() )
				return false;
		return true;
	}
	
	QList<ExpressionPrivate*> expressions;
};

class _OneArgExpression : public ExpressionPrivate
{
public:
	_OneArgExpression( Expression::Type type, ExpressionPrivate * _child = 0 )
	: ExpressionPrivate(type)
	, child(_child)
	{
		if( child )
			child->ref();
	}
	_OneArgExpression( const _OneArgExpression & other )
	: ExpressionPrivate(other)
	, child(0)
	{
		if( other.child ) {
			child = other.child->copy();
			if( child )
				child->ref();
		}
	}
	~_OneArgExpression()
	{
		if( child )
			child->deref();
	}
	ExpressionPrivate * copy() const { return new _OneArgExpression(*this); }
	virtual quint64 hash() const { return child ? child->hash() : 0; }
	quint64 type_hash() const { return hash_combine(_OneArgExpression::hash(),type); }
	
	virtual void append( ExpressionPrivate * ep ) {
		if( !child && ep ) {
			child = ep;
			child->ref();
		}
	}
	virtual QList<ExpressionPrivate*> children() const
	{
		QList<ExpressionPrivate*> ret;
		if( child ) ret << child;
		return ret;
	}
	virtual void setChildren(const QList<ExpressionPrivate*> & children)
	{
		assert(children.size() <= 1);
		if( children.size() )
			child = children[0];
	}
	virtual QVariant value( const Record & record ) const
	{ return child ? child->value(record) : QVariant(); }
	
	virtual bool isConstant() {
		return child ? child->isConstant() : true;
	}
	ExpressionPrivate * child;
};

class _TwoArgExpression : public ExpressionPrivate
{
public:
	_TwoArgExpression( Expression::Type type, ExpressionPrivate * _left = 0, ExpressionPrivate * _right = 0 )
	: ExpressionPrivate(type)
	, left(_left)
	, right(_right)
	{
		if( left )
			left->ref();
		if( right )
			right->ref();
	}
	_TwoArgExpression( const _TwoArgExpression & other )
	: ExpressionPrivate(other)
	, left(0)
	, right(0)
	{
		if( other.left ) {
			left = other.left->copy();
			if( left )
				left->ref();
		}
		if( other.right ) {
			right = other.right->copy();
			if( right )
				right->ref();
		}
	}
	~_TwoArgExpression()
	{
		if( left )
			left->deref();
		if( right )
			right->deref();
	}
	virtual QString toString(const ToStrContext & pc) const {
		TS_CTX;
		QString ret;
		if( left )
			ret = left->toString(c);
		if( left && right )
			ret += separator();
		if( right )
			ret += right->toString(c);
		if( needParens(pc) )
			ret = "(" + ret + ")";
		return ret;
	}
	ExpressionPrivate * copy() const { return new _TwoArgExpression(*this); }
	quint64 hash() const {
		quint64 leftHash = left ? left->hash() : 0;
		quint64 rightHash = right ? right->hash() : 0;
		if( leftHash < rightHash )
			qSwap(leftHash,rightHash);
		return hash_combine(hash_combine(leftHash,rightHash), type);
	}
	
	virtual void append( ExpressionPrivate * ep ) {
		if( !left && ep ) {
			left = ep;
			left->ref();
		}
		else if( !right && ep ) {
			right = ep;
			right->ref();
		}
	}
	virtual QList<ExpressionPrivate*> children() const
	{ 
		QList<ExpressionPrivate*> ret;
		if( left ) ret << left;
		if( right ) ret << right;
		return ret;
	}
	virtual void setChildren(const QList<ExpressionPrivate*> & children)
	{
		assert(children.size() == 2);
		left = children[0];
		right = children[1];
	}
	virtual bool isConstant() {
		return (left ? left->isConstant() : true) && (right ? right->isConstant() : true);
	}

	ExpressionPrivate * left, * right;
};

class _ThreeArgExpression : public ExpressionPrivate
{
public:
	_ThreeArgExpression( Expression::Type type, ExpressionPrivate * _one = 0, ExpressionPrivate * _two = 0, ExpressionPrivate * _three = 0 )
	: ExpressionPrivate(type)
	, one(_one)
	, two(_two)
	, three(_three)
	{
		if( one )
			one->ref();
		if( two )
			two->ref();
		if( three )
			three->ref();
	}
	_ThreeArgExpression( const _ThreeArgExpression & other )
	: ExpressionPrivate(other)
	, one(0)
	, two(0)
	, three(0)
	{
		if( other.one ) {
			one = other.one->copy();
			if( one )
				one->ref();
		}
		if( other.two ) {
			two = other.two->copy();
			if( two )
				two->ref();
		}
		if( other.three ) {
			three = other.three->copy();
			if( three )
				three->ref();
		}
	}
	~_ThreeArgExpression()
	{
		if( one )
			one->deref();
		if( two )
			two->deref();
		if( three )
			three->deref();
	}
	ExpressionPrivate * copy() const { return new _ThreeArgExpression(*this); }
	quint64 hash() const {
		quint64 oneHash = one ? one->hash() : 0;
		quint64 twoHash = two ? two->hash() : 0;
		quint64 threeHash = three ? three->hash() : 0;
		return hash_combine(hash_combine(oneHash,hash_combine(twoHash,threeHash)), type);
	}
	
	virtual void append( ExpressionPrivate * ep ) {
		if( !ep ) return;
		if( !one ) (one = ep)->ref();
		else if( !two ) (two = ep)->ref();
		else if( !three ) (three = ep)->ref();
	}
	virtual QList<ExpressionPrivate*> children() const
	{ 
		QList<ExpressionPrivate*> ret;
		if( one ) ret << one;
		if( two ) ret << two;
		if( two ) ret << three;
		return ret;
	}
	virtual void setChildren(const QList<ExpressionPrivate*> & children)
	{
		assert(children.size() == 3);
		one = children[0];
		two = children[1];
		three = children[2];
	}
	virtual bool isConstant() {
		return (one ? one->isConstant() : true) && (two ? two->isConstant() : true) && (three ? three->isConstant() : true);
	}

	ExpressionPrivate * one, * two, * three;
};


class _AliasExpression : public _OneArgExpression
{
public:
	_AliasExpression( ExpressionPrivate * child = 0, const QString & _alias = QString() )
	: _OneArgExpression( Expression::Alias_Expression, child )
	, alias(_alias)
	{}
	_AliasExpression( const _AliasExpression & other )
	: _OneArgExpression( other )
	, alias(other.alias)
	, tableAlias(other.tableAlias)
	{}
	ExpressionPrivate * copy() const { return new _AliasExpression(*this); }
	// An alias should have no affect on what data is selected, therefore doesn't need to modify the hash
	//virtual quint64 hash() const { return 0; }
	QString toString(const ToStrContext & pc) const
	{
		TS_CTX;
		QString ret = child->toString(c);
		if( !alias.isEmpty() )
			ret += " AS " + alias;
		return ret;
	}
	void save( QDataStream & ds )
	{
		ds << QVariant(alias) << QVariant(tableAlias);
	}
	void load( QDataStream & ds )
	{
		QVariant v;
		ds >> v; alias = v.toString();
		ds >> v; tableAlias = v.toString();
	}
	
	QString alias, tableAlias;
};

class _CastExpression : public _OneArgExpression
{
public:
	_CastExpression( ExpressionPrivate * child = 0, QVariant::Type _type = QVariant::Invalid )
	: _OneArgExpression( Expression::Cast_Expression, child )
	, variantType(_type)
	{}
	
	_CastExpression( const _CastExpression & other )
	: _OneArgExpression(other)
	, variantType(other.variantType)
	{}
	ExpressionPrivate * copy() const { return new _CastExpression(*this); }
	QString toString(const ToStrContext & pc) const
	{
		TS_CTX;
		QString ret = child->toString(c);
		ret += "::" + Field::dbTypeString(Field::variantTypeToFieldType(variantType));
		return ret;
	}
	virtual quint64 hash() const { return type_hash(); }
	QVariant::Type valueType() const {
		return variantType;
	}
	QVariant value( const Record & record ) const {
		QVariant v = child->value(record);
		if( v.canConvert(variantType) )
			v.convert(variantType);
		else
			return convert(v);
		return v;
	}
	QVariant convert( QVariant v ) const
	{
		return v;
	}
	void save( QDataStream & ds )
	{
		ds << QVariant((int)variantType);
	}
	void load( QDataStream & ds )
	{
		QVariant v;
		ds >> v; variantType = (QVariant::Type)v.toInt();
	}
	
	QVariant::Type variantType;
};


class _AndExpression : public _CompoundExpression
{
public:
	_AndExpression() : _CompoundExpression( Expression::And_Expression ) {}
	_AndExpression( const _AndExpression & other )
	: _CompoundExpression( other ) {}
	
	ExpressionPrivate * copy() const { return new _AndExpression(*this); }
	QVariant::Type valueType() const {
		return QVariant::Bool;
	}
	QVariant value( const Record & record ) const {
		bool hasNull = false;
		foreach( ExpressionPrivate * c, expressions ) {
			QVariant v = c->value(record);
			if( !v.isNull() ) {
				bool b = v.toBool();
				if( !b ) return false;
				continue;
			}
			hasNull = true;
		}
		if( hasNull )
			return QVariant(QVariant::Bool);
		return true;
	}
	virtual bool isConstant() {
		foreach( ExpressionPrivate * child, expressions ) {
			if( child->isConstant() && !child->value(Record()).toBool() )
				return true;
		}
		return _CompoundExpression::isConstant();
	}
	virtual QString toString(const ToStrContext & pc) const {
		int constTrueCount = 0;
		foreach( ExpressionPrivate * child, expressions ) {
			if( child->isConstant() ) {
				QVariant v = child->value(Record());
				if( v.isNull() )
					return "NULL";
				else if( !v.toBool() )
					return "FALSE";
				constTrueCount++;
			}
		}
		if( constTrueCount == expressions.size() )
			return "TRUE";
		return _CompoundExpression::toString(pc);
	}
	virtual QString separator() const { return " AND "; }
};

class _OrExpression : public _CompoundExpression
{
public:
	_OrExpression() : _CompoundExpression( Expression::Or_Expression ) {}
	_OrExpression( const _OrExpression & other ) : _CompoundExpression(other) {}
	ExpressionPrivate * copy() const { return new _OrExpression(*this); }
	QVariant::Type valueType() const {
		return QVariant::Bool;
	}
	QVariant value( const Record & record ) const {
		foreach( ExpressionPrivate * c, expressions )
			if( c->matches(record) )
				return true;
		return false;
	}
	virtual bool isConstant() {
		foreach( ExpressionPrivate * child, expressions ) {
			if( child->isConstant() && child->value(Record()).toBool() )
				return true;
		}
		return _CompoundExpression::isConstant();
	}
	virtual QString toString(const ToStrContext & pc) const {
		QList<ExpressionPrivate*> kids;
		// Any constant true child results in the entire OR statement being replaced with TRUE
		// If not only non-constant children are put into the expression, because any constants
		// if not true are false or NULL
		foreach( ExpressionPrivate * child, expressions ) {
			if( child->isConstant() ) {
				if( child->value(Record()).toBool() )
					return "TRUE";
			} else
				kids.append(child);
		}
		TS_CTX;
		return joinStrings( c, kids, separator(), needParens(pc) );
	}
	virtual QString separator() const { return " OR "; }
};

class _IsNullExpression: public _OneArgExpression
{
public:
	_IsNullExpression( Expression::Type type, ExpressionPrivate * _child = 0 )
	: _OneArgExpression( type, _child ) {}
	_IsNullExpression( const _IsNullExpression & other ) : _OneArgExpression(other) {}
	ExpressionPrivate * copy() const { return new _IsNullExpression(*this); }
	virtual quint64 hash() const { return type_hash(); }
	QVariant::Type valueType() const {
		return QVariant::Bool;
	}
	QVariant value( const Record & record ) const {
		QVariant v = child->value(record);
		bool isNull = true;
		if( v.userType() == qMetaTypeId<Record>() )
			isNull = !v.value<Record>().isValid();
		else
			isNull = v.isNull();
		return type == Expression::IsNotNull_Expression ? !isNull : isNull;
	}
	QString toString(const ToStrContext & pc) const
	{
		TS_CTX;
		return child->toString(c) + (type == Expression::IsNotNull_Expression ? " IS NOT NULL" : " IS NULL");
	}
};

class _IsExpression: public _TwoArgExpression
{
public:
	_IsExpression( Expression::Type type, ExpressionPrivate * _left = 0, ExpressionPrivate * _right = 0 )
	: _TwoArgExpression( type, _left, _right ) {}
	_IsExpression( const _IsExpression & other ) : _TwoArgExpression(other) {}
	ExpressionPrivate * copy() const { return new _IsExpression(*this); }
	QVariant::Type valueType() const {
		return QVariant::Bool;
	}
	bool isNull( ExpressionPrivate * p, const Record & record ) const
	{
		QVariant v = p->value(record);
		return (v.userType() == qMetaTypeId<Record>()) ? !v.value<Record>().isValid() : v.isNull();
	}
	QVariant value( const Record & record ) const {
		bool leftNull = isNull(left,record), rightNull = isNull(right,record);
		return (leftNull == rightNull) == (type == Expression::Is_Expression);
	}
	QString toString(const ToStrContext & pc) const 
	{
		TS_CTX;
		return left->toString(c) + (type == Expression::IsNot_Expression ? " IS NOT " : " IS ") + right->toString(c);
	}
};

class _CoalesceExpression : public _CompoundExpression
{
public:
	_CoalesceExpression( ExpressionPrivate * c1 = 0, ExpressionPrivate * c2 = 0 )
	: _CompoundExpression( Expression::Coalesce_Expression )
	{
		append(c1);
		append(c2);
	}
	_CoalesceExpression( const _CoalesceExpression & other ) : _CompoundExpression(other) {}
	ExpressionPrivate * copy() const { return new _CoalesceExpression(*this); }
	QVariant::Type valueType() const {
		foreach( ExpressionPrivate * c, expressions ) {
			QVariant::Type t = c->valueType();
			if( t != QVariant::Invalid )
				return t;
		}
		return QVariant::Invalid;
	}
	QVariant value( const Record & record ) const {
		foreach( ExpressionPrivate * c, expressions ) {
			QVariant val = c->value(record);
			if( !val.isNull() )
				return val;
		}
		return QVariant();
	}
	virtual bool isConstant() {
		foreach( ExpressionPrivate * child, expressions ) {
			if( !child->isConstant() )
				return false;
			
			if( !child->value(Record()).isNull() )
				return true;
		}
		return true;
	}
	QString toString(const ToStrContext & pc) const
	{
		TS_CTX;
		return QLatin1String("COALESCE") + joinStrings( c, expressions, ",", true );
	}
};

bool eq( const QVariant & v1, const QVariant & v2 )
{
	QVariant cvt1(v1), cvt2(v2);
	QVariant::Type t1 = (QVariant::Type)v1.userType(), t2 = (QVariant::Type)v2.userType();
	const QVariant::Type rt = (QVariant::Type)qMetaTypeId<Record>();
	if( t1 == rt && t2 == rt )
		return v1.value<Record>() == v2.value<Record>();
	if( t1 == rt )
		cvt1 = QVariant(v1.value<Record>().key());
	else if( t2 == rt )
		cvt2 = QVariant(v2.value<Record>().key());
	return cvt1 == cvt2;
}

class _InExpression : public _CompoundExpression
{
public:
	_InExpression( ExpressionPrivate * leftExp = 0 ) : _CompoundExpression( Expression::In_Expression )
	{
		append(leftExp);
	}
	_InExpression( const _InExpression & other ) : _CompoundExpression(other) {}
	ExpressionPrivate * copy() const { return new _InExpression(*this); }
	QVariant::Type valueType() const {
		return QVariant::Bool;
	}
	
	void addRecords(const Expression & parent, RecordList records)
	{
		expressions.reserve(records.size()+expressions.size());
		foreach( const Record & r, records )
			append(getRecordLink(parent, r, false));
	}
	
	QVariant value( const Record & record ) const {
		if( expressions.size() < 2 ) return (type == Expression::NotIn_Expression);
		bool foundMatch = false;
		QVariant left = expressions[0]->value(record);
		if( left.isNull() ) return left;
		for( int i = 1; i < expressions.size(); ++i ) {
			QVariant right = expressions[i]->value(record);
			if( right.isNull() ) continue;
			if( eq(right, left) ) {
				foundMatch = true;
				break;
			}
		}
		return type == Expression::In_Expression ? foundMatch : !foundMatch;
	}
	
	virtual bool isConstant() {
		if( expressions.size() < 2 ) return true;
		if( type == Expression::In_Expression && expressions[0]->isConstant() ) {
			QVariant left = expressions[0]->value(Record());
			for( int i = 1; i < expressions.size(); ++i ) {
				if( expressions[i]->isConstant() && eq(expressions[i]->value(Record()), left) )
					return true;
			}
		}
		return _CompoundExpression::isConstant();
	}

	QString toString(const ToStrContext & pc) const
	{
		if( expressions.size() < 2 ) {
			if( pc.conn )
				return pc.conn->prepareValueString( QVariant(type==Expression::NotIn_Expression) );
			return type==Expression::NotIn_Expression ? "TRUE" : "FALSE";
		}
		TS_CTX;
		c.needArray = false;
		QStringList parts;
		for( int i = 1; i < expressions.size(); ++i )
			parts.append( expressions[i]->toString(c) );
		return expressions[0]->toString(c) + QString(type==Expression::NotIn_Expression ? " NOT" : "") + " IN (" + parts.join(",") + ")";
	}
	
	ExpressionPrivate * prepareForExec(const PrepContext & pc, Connection * conn);

};

class _LikeExpression : public _TwoArgExpression
{
public:
	_LikeExpression( ExpressionPrivate * _left = 0, ExpressionPrivate * _right = 0, bool caseSensitive = false )
	: _TwoArgExpression( caseSensitive ? Expression::Like_Expression : Expression::ILike_Expression, _left, _right )
	{}
	_LikeExpression( const _LikeExpression & other ) : _TwoArgExpression(other) {}
	ExpressionPrivate * copy() const { return new _LikeExpression(*this); }
	QVariant::Type valueType() const {
		return QVariant::Bool;
	}
	static QString likeToRegEx( const QString & like )
	{
		QString ret;
		ret.reserve(like.size() + 20);
		for( int i = 0; i < like.size(); ++i ) {
			QChar c = like.at(i);
			if( c == '%' ) {
				ret.append( ".*" );
				continue;
			}
			if( c == '_' ) {
				ret.append( "." );
				continue;
			}
			if( c == '(' || c == ')' || c == '.' || c == '*' || c == '{' || c == '}' || c == '[' || c == ']' || c == '\\' || c == '^' || c == '$' || c == '|' || c == '+' || c == '?' )
				ret.append( '\\' );
			ret.append( c );
		}
		ret.squeeze();
		return ret;
	}
	QVariant value( const Record & record ) const
	{
		QVariant leftv = left->value(record);
		if( leftv.isNull() ) return leftv;
		QVariant rightv = right->value(record);
		if( rightv.isNull() ) return rightv;
		return QRegExp(likeToRegEx(rightv.toString()), type == Expression::Like_Expression ? Qt::CaseSensitive : Qt::CaseInsensitive).exactMatch(leftv.toString());
	}
	QString toString(const ToStrContext & pc) const
	{
		TS_CTX;
		return left->toString(c) + (type == Expression::Like_Expression ? " LIKE " : " ILIKE ") + right->toString(c);
	}
};

class _ReplaceExpression : public _ThreeArgExpression
{
public:
	_ReplaceExpression( ExpressionPrivate * _source = 0, ExpressionPrivate * _search = 0, ExpressionPrivate * _replace = 0 )
	: _ThreeArgExpression( Expression::Replace_Expression, _source, _search, _replace )
	{}
	_ReplaceExpression( const _ReplaceExpression & other ) : _ThreeArgExpression(other) {}
	ExpressionPrivate * copy() const { return new _ReplaceExpression(*this); }
	QVariant::Type valueType() const { return QVariant::String; }
	QVariant value( const Record & record ) const
	{
		// TODO: Type checking
		QString source = one->value(record).toString(), search = two->value(record).toString(), replace = three->value(record).toString();
		qDebug() << source << search << replace << endl;
		source.replace(search,replace);
		return source;
	}
	QString toString(const ToStrContext & pc) const
	{
		TS_CTX;
		return "replace(" + one->toString(c) + "," + two->toString(c) + "," + three->toString(c) + ")";
	}
};

class _CaseChangeExpression : public _OneArgExpression
{
public:
	_CaseChangeExpression( Expression::Type type = Expression::Upper_Expression, ExpressionPrivate * _source = 0 )
	: _OneArgExpression( type, _source ) {}
	_CaseChangeExpression( const _CaseChangeExpression & other ) : _OneArgExpression(other) {}
	ExpressionPrivate * copy() const { return new _CaseChangeExpression(*this); }
	QVariant::Type valueType() const { return QVariant::String; }
	QVariant value( const Record & record ) const
	{
		// TODO: Type checking
		QString source = child->value(record).toString();
		return type == Expression::Upper_Expression ? source.toUpper() : source.toLower();
	}
	QString toString(const ToStrContext & pc) const
	{
		static const QString s_upper("upper("), s_lower("lower(");
		TS_CTX;
		return (type == Expression::Upper_Expression ? s_upper : s_lower) + child->toString(c) + ")";
	}
};

class _StringLenExpression : public _OneArgExpression
{
public:
	_StringLenExpression( ExpressionPrivate * _source = 0 )
	: _OneArgExpression( Expression::Length_Expression, _source ) {}
	_StringLenExpression( const _StringLenExpression & other ) : _OneArgExpression(other) {}
	ExpressionPrivate * copy() const { return new _StringLenExpression(*this); }
	QVariant::Type valueType() const { return QVariant::Int; }
	QVariant value( const Record & record ) const
	{
		// TODO: Type checking
		QString source = child->value(record).toString();
		return source.length();
	}
	QString toString(const ToStrContext & pc) const
	{
		static const QString s_length("length(");
		TS_CTX;
		return s_length + child->toString(c) + ")";
	}
};

class _StringRightLeftExpression : public _TwoArgExpression
{
public:
	_StringRightLeftExpression( Expression::Type type = Expression::Left_Expression, ExpressionPrivate * _source = 0, ExpressionPrivate * _cnt = 0 )
	: _TwoArgExpression( type, _source, _cnt ) {}
	_StringRightLeftExpression( const _StringRightLeftExpression & other ) : _TwoArgExpression(other) {}
	ExpressionPrivate * copy() const { return new _StringRightLeftExpression(*this); }
	QVariant::Type valueType() const { return QVariant::String; }
	QVariant value( const Record & record ) const
	{
		// TODO: Type checking
		QString source = left->value(record).toString();
		int len = right->value(record).toInt();
		return type == Expression::Left_Expression ? source.left(len) : source.right(len);
	}
	QString toString(const ToStrContext & pc) const
	{
		static const QString s_left("left("), s_right("right(");
		TS_CTX;
		return (type == Expression::Left_Expression ? s_left : s_right) + left->toString(c) + "," + right->toString(c) + ")";
	}
};

class _StringPositionExpression : public _TwoArgExpression
{
public:
	_StringPositionExpression( ExpressionPrivate * _source = 0, ExpressionPrivate * _substring = 0 )
	: _TwoArgExpression( Expression::Position_Expression, _source, _substring ) {}
	_StringPositionExpression( const _StringPositionExpression & other ) : _TwoArgExpression(other) {}
	ExpressionPrivate * copy() const { return new _StringPositionExpression(*this); }
	QVariant::Type valueType() const { return QVariant::Int; }
	QVariant value( const Record & record ) const
	{
		// TODO: Type checking
		QString source = left->value(record).toString(), substring = right->value(record).toString();
		return source.indexOf(substring) + 1;
	}
	QString toString(const ToStrContext & pc) const
	{
		static const QString s_position("position(");
		TS_CTX;
		return s_position + right->toString(c) + " IN " + left->toString(c) + ")";
	}
};

class _GetValueExpression : public _TwoArgExpression
{
public:
	_GetValueExpression( ExpressionPrivate * _source=0, ExpressionPrivate * key=0 )
	: _TwoArgExpression( Expression::GetValue_Expression, _source, key ) {}
	_GetValueExpression( const _GetValueExpression & other ) : _TwoArgExpression(other) {}
	ExpressionPrivate * copy() const { return new _GetValueExpression(*this); }
	QVariant::Type valueType() const { 
		int lvt = left->valueType();
		if( lvt == qMetaTypeId<StringMap>() || lvt == QVariant::StringList )
			return QVariant::String;
		if( lvt == qMetaTypeId<IntList>() )
			return QVariant::Int;
		return QVariant::Invalid;
	}
	QVariant value( const Record & record ) const
	{
		QVariant l = left->value(record);
		QVariant r = right->value(record);
		if( l.userType() == qMetaTypeId<StringMap>() && r.type() == QVariant::String ) {
			QString key = r.toString();
			if( !key.isNull() ) {
				StringMap sm = qvariant_cast<StringMap>(l);
				StringMap::const_iterator it = sm.constFind(key);
				return it == sm.constEnd() ? QString() : it.value();
			}
			return QString();
		}
		else if( l.type() == QVariant::StringList && r.canConvert(QVariant::Int) ) {
			int idx = r.toInt();
			QStringList lsl = l.toStringList();
			if( idx >= 0 && idx < lsl.size() )
				return lsl[idx];
			return QString();
		}
		else if( l.userType() == qMetaTypeId<IntList>() && r.canConvert(QVariant::Int) ) {
			int idx = r.toInt();
			IntList lil = l.value<IntList>();
			if( idx >= 0 && idx < lil.size() )
				return lil[idx];
			return QVariant(QVariant::Int);
		}
		return QVariant();
	}
	// Only used for StringMap case
	virtual QString separator() const { static const QString s_op("->"); return s_op; }
	virtual QString toString(const ToStrContext & pc) const
	{
		if( int(left->valueType()) == qMetaTypeId<StringMap>() )
			return _TwoArgExpression::toString(pc);
		TS_CTX;
		return left->toString(c) + "[" + right->toString(c) + "]";
	}
};

class _ContainsExpression : public _TwoArgExpression
{
public:
	_ContainsExpression( ExpressionPrivate * _source=0, ExpressionPrivate * key=0 )
	: _TwoArgExpression( Expression::Contains_Expression, _source, key ) {}
	_ContainsExpression( const _ContainsExpression & other ) : _TwoArgExpression(other) {}
	ExpressionPrivate * copy() const { return new _ContainsExpression(*this); }
	QVariant::Type valueType() const { return QVariant::Bool; }
	QVariant value( const Record & record ) const
	{
		QVariant r = right->value(record);
		if( r.isNull() ) return r;
		
		QVariant l = left->value(record);
		int ltype = l.userType();
		if( ltype == qMetaTypeId<StringMap>() ) {
			StringMap sm = qvariant_cast<StringMap>(l);
			if( r.type() == QVariant::String ) {
				QString rs = r.toString();
				StringMap::const_iterator it = sm.constFind(rs);
				return it != sm.constEnd();
			} else if( r.type() == QVariant::StringList ) {
				QStringList rsl = r.toStringList();
				foreach( QString rs, rsl ) 
					if( !sm.contains(rs) )
						return false;
				return true;
			} else if( r.userType() == qMetaTypeId<StringMap>() ) {
				StringMap rsm = qvariant_cast<StringMap>(r);
				StringMap::const_iterator lend = sm.constEnd();
				bool ret = true;
				for( StringMap::const_iterator it = rsm.constBegin(), end = rsm.constEnd(); it != end; ++it ) {
					StringMap::const_iterator f = sm.constFind(it.key());
					if( f == lend || it.value() != f.value() ) {
						ret = false;
						break;
					}
				}
				return ret;
			}
		}
		else if( ltype == QVariant::StringList ) {
			QStringList lsl = l.toStringList();
			if( r.type() == QVariant::String ) {
				QString rs = r.toString();
				return bool(lsl.contains(rs));
			}
			else if( r.type() == QVariant::StringList ) {
				QStringList rsl = r.toStringList();
				bool ret = true;
				foreach( QString rs, rsl )
					if( !lsl.contains(rs) ) {
						ret = false;
						break;
					}
				return ret;
			}
			else if( r.isNull() )
				return bool(lsl.contains( QString() ));
		}
		else if( ltype == QVariant::String && r.type() == QVariant::String ) {
			QString ls = l.toString();
			QString rs = r.toString();
			return QVariant(ls.contains(rs));
		}
		else if( ltype == qMetaTypeId<IntList>() ) {
			IntList lil = l.value<IntList>();
			if( r.canConvert(QVariant::Int) )
				return bool(lil.contains(r.toInt()));
			else if( int(r.type()) == qMetaTypeId<IntList>() ) {
				IntList ril = r.value<IntList>();
				bool ret = true;
				foreach( int i, ril )
					if( !lil.contains(i) ) {
						ret = false;
						break;
					}
				return ret;
			}
			else if( r.isNull() )
				return bool(lil.contains(IntList::Null));
		}
		return false;
	}
	QString toString(const ToStrContext & pc) const
	{
		TS_CTX;
		static const QString s_contains_op(" @> ");
		int rtype = right->valueType();
		int ltype = left->valueType();
		if( rtype == int(QVariant::Invalid) )
			return "NULL";
		if( ltype == qMetaTypeId<StringMap>() && (rtype == QVariant::String || rtype == QVariant::StringList || rtype == qMetaTypeId<StringMap>()) ) {
			QString ret;
			static const QString s_hstore_contains_key(" ? "), s_hstore_contains_keylist(" ?& ");
			const QString & s_op = rtype == QVariant::String ? s_hstore_contains_key : (rtype == QVariant::StringList ? s_hstore_contains_keylist : s_contains_op);
			ret = left->toString(c) + s_op + right->toString(c);
			if( pc.exp->operatorPrecedence() >= 65 )
				ret = "(" + ret + ")";
			return ret;
		}
		else if( (ltype == QVariant::StringList && rtype == QVariant::String) || (ltype == qMetaTypeId<IntList>() && rtype == QVariant::Int) )
		{
			return left->toString(c) + s_contains_op + "ARRAY[" + right->toString(c) + "]";
		}
		else if( 
			(ltype == QVariant::StringList && rtype == QVariant::StringList)
			|| (ltype == qMetaTypeId<IntList>() && rtype == qMetaTypeId<IntList>()) )
		{
			return left->toString(c) + s_contains_op + right->toString(c);
		}
		else if( (ltype == qMetaTypeId<IntList>() && rtype == QVariant::Int) || (ltype == QVariant::StringList && rtype == QVariant::String) ) {
			QString ret = right->toString(c) + "=ANY(";
			ret += left->toString(c) + ")";
			return ret;
		}
		static const QString s_position("strpos("), s_mid(", "), s_end(") > 0");
		return s_position + left->toString(c) + s_mid + right->toString(c) + s_end;
	}
};

static QString quoteAndEscapeString( const QString & str, bool safeOnly )
{
	QString out;
	out.reserve(str.size() * 1.1 + 2);
	out.append("\'");
	bool escapedString = false;
	for( int i = 0; i < str.size(); ++i ) {
		QChar c = str[i];
		if( safeOnly ) {
			if( c == '\\' )
				escapedString = true;
			if( c == '\'' ) {
				// Already escaped by double ''
				if( i + 1 < str.size() && str[i+1] == '\'' ) {
					out.append("''");
					i++;
					continue;
				}
				if( i > 0 && str[i-1] == '\\' ) {
					out.append(c);
					continue;
				}
				out.append("\\'");
				escapedString = true;
				continue;
			}
			out.append(c);
		} else {
			if( c == '\\' ) {
				out.append("\\\\");
				escapedString = true;
			} else if( c == '\b' ) {
				out.append("\\b");
				escapedString = true;
			} else if( c == '\f' ) {
				out.append("\\f");
				escapedString = true;
			} else if( c == '\n' ) {
				out.append("\\n");
				escapedString = true;
			} else if( c == '\t' ) {
				out.append("\\t");
				escapedString = true;
			} else if( c == '\'' )
				out.append("''");
			else
				out.append(c);
		}
	}
	out.append("\'");
	if( escapedString )
		out = "E" + out;
	out.squeeze();
	return out;
}

class _RegexExpression : public _TwoArgExpression
{
public:
	_RegexExpression( ExpressionPrivate * p = 0, ExpressionPrivate * p2 = 0, Qt::CaseSensitivity _cs = Qt::CaseInsensitive )
	: _TwoArgExpression( Expression::RegEx_Expression, p, p2 )
	, cs(_cs)
	{}
	_RegexExpression( const _RegexExpression & other ) : _TwoArgExpression(other), cs(other.cs) {}
	ExpressionPrivate * copy() const { return new _RegexExpression(*this); }
	virtual quint64 hash() const {
		return hash_combine(_TwoArgExpression::hash(),cs);
	}

	QVariant::Type valueType() const {
		return QVariant::Bool;
	}

	QString toString(const ToStrContext & pc) const {
		TS_CTX;
		return left->toString(c) + " " + (cs == Qt::CaseSensitive ? "~ " : "~* ") + right->toString(c);
	}
	
	QVariant value( const Record & record ) const
	{
		return QRegExp(right->value(record).toString(),cs).indexIn(left->value(record).toString()) >= 0;
	}
	
	void save( QDataStream & ds )
	{
		ds << QVariant((int)cs);
	}
	void load( QDataStream & ds )
	{
		QVariant v;
		ds >> v; cs = (Qt::CaseSensitivity)v.toInt();
	}
	Qt::CaseSensitivity cs;
};

bool gt( const QVariant & v1, const QVariant & v2 )
{
	QVariant::Type t1 = (QVariant::Type)v1.userType(), t2 = (QVariant::Type)v2.userType();
	if( t1 != t2 ) {
		if( !v2.canConvert(t2) )
			return false;
		// Always cast QDate to QDateTime and not the other way around
		if( t1 == QVariant::Date && t2 == QVariant::DateTime )
			t1 = t2;
		if( t2 == QVariant::Double )
			t1 = t2;
	}
	switch( t1 ) {
		case QVariant::Bool:
			return v1.toBool() && !v2.toBool();
		case QVariant::Char:
			return v1.toChar() > v2.toChar();
		case QVariant::Date:
			return v1.toDate() > v2.toDate();
		case QVariant::DateTime:
			return v1.toDateTime() > v2.toDateTime();
		case QVariant::Double:
			return v1.toDouble() > v2.toDouble();
		case QVariant::Int:
			return v1.toInt() > v2.toInt();
		case QVariant::LongLong:
			return v1.toLongLong() > v2.toLongLong();
		case QVariant::String:
			return v1.toString() > v2.toString();
		case QVariant::Time:
			return v1.toTime() > v2.toTime();
		case QVariant::UInt:
			return v1.toUInt() > v2.toUInt();
		case QVariant::ULongLong:
			return v1.toULongLong() > v2.toULongLong();
		default:
			if( t1 == (QVariant::Type)qMetaTypeId< ::Interval>() )
				return v1.value<Interval>() > v2.value<Interval>();
	}
	return false;
}

bool gte( const QVariant & v1, const QVariant & v2 )
{
	if( v1 == v2 ) return true;
	return gt(v1,v2);
}

bool lt( const QVariant & v1, const QVariant & v2 )
{
	return !gte(v1,v2);
}

bool lte( const QVariant & v1, const QVariant & v2 )
{
	return !gt(v1,v2);
}

class _ComparisonExpression : public _TwoArgExpression
{
public:
	_ComparisonExpression( Expression::Type type ) : _TwoArgExpression( type ) {}
	_ComparisonExpression( const _ComparisonExpression & other ) : _TwoArgExpression(other) {}
	ExpressionPrivate * copy() const { return new _ComparisonExpression(*this); }

	QVariant::Type valueType() const {
		switch( type ) {
			case Expression::Equals_Expression:
			case Expression::NEquals_Expression:
			case Expression::Larger_Expression:
			case Expression::LargerOrEquals_Expression:
			case Expression::Less_Expression:
			case Expression::LessOrEquals_Expression:
			case Expression::IsDistinctFrom_Expression:
			case Expression::IsNotDistinctFrom_Expression:
				return QVariant::Bool;
			case Expression::BitAnd_Expression:
			case Expression::BitOr_Expression:
			case Expression::BitXor_Expression:
				return QVariant::Int;
			case Expression::Plus_Expression:
			case Expression::Minus_Expression:
			{
				if( left && right && type == Expression::Plus_Expression ) {
					QVariant::Type vt1 = left->valueType(), vt2 = right->valueType();
					if( vt1 == QVariant::String && vt2 == QVariant::String )
						return vt1;
				}
				return QVariant::Int;
			}
			default:
				break;
		}
		return QVariant::Bool;
	}

	ExpressionPrivate * prepareForExec( const PrepContext & pc, Connection * conn );
	
	QVariant value( const Record & record ) const
	{
		if( !left || !right ) return false;
		QVariant v = left->value(record);
		QVariant v2 = right->value(record);
		if( type != Expression::IsDistinctFrom_Expression && type != Expression::IsNotDistinctFrom_Expression 
			&& (v.isNull() || v2.isNull()) )
			return QVariant(QVariant::Type(v.userType()));
			
		switch( type ) {
			case Expression::Equals_Expression:
			case Expression::IsNotDistinctFrom_Expression:
				return eq(v,v2);
			case Expression::NEquals_Expression:
			case Expression::IsDistinctFrom_Expression:
				return !eq(v,v2);
			case Expression::Larger_Expression:
				return gt(v,v2);
			case Expression::LargerOrEquals_Expression:
				return gte(v,v2);
			case Expression::Less_Expression:
				return lt(v,v2);
			case Expression::LessOrEquals_Expression:
				return lte(v,v2);
			case Expression::BitAnd_Expression:
				return v.toULongLong() & v2.toULongLong();
			case Expression::BitOr_Expression:
				return v.toULongLong() | v2.toULongLong();
			case Expression::BitXor_Expression:
				return v.toULongLong() ^ v2.toULongLong();
			case Expression::Plus_Expression:
			case Expression::Minus_Expression:
			{
				const bool plus = type == Expression::Plus_Expression;
				QVariant::Type t1 = (QVariant::Type)v.userType(), t2 = (QVariant::Type)v2.userType(),
				intervalType = (QVariant::Type)qMetaTypeId< ::Interval>();
				if( t1 == QVariant::String && t2 == QVariant::String ) {
					if( type == Expression::Plus_Expression )
						return QString(v.toString() + v2.toString());
					return QString();
				}
				if( t1 == QVariant::Double || t2 == QVariant::Double ) {
					double d1(v.toDouble()), d2(v2.toDouble());
					return plus ? d1 + d2 : d1 - d2;
				}
				if( t1 == intervalType ) {
					Interval i = v.value<Interval>();
					if( t2 == QVariant::Date )
						return plus ? i.adjust(QDateTime(v2.toDate())) : QDateTime();
					if( t2 == QVariant::DateTime )
						return plus ? i.adjust(v2.toDateTime()) : QDateTime();
					if( t2 == intervalType ) {
						Interval i2 = v2.value<Interval>();
						return QVariant::fromValue<Interval>(plus ? i + i2 : i - i2);
					}
				}
				if( t2 == intervalType ) {
					Interval i = v2.value<Interval>();
					if( !plus )
						i = i * -1.0;
					if( t1 == QVariant::Date )
						return i.adjust(QDateTime(v.toDate()));
					if( t1 == QVariant::DateTime )
						return i.adjust(v.toDateTime());
					if( t1 == intervalType )
						return QVariant::fromValue<Interval>(i + v.value<Interval>());
				}
				qlonglong ll1(v.toLongLong()), ll2(v2.toLongLong());
				return plus ? ll1 + ll2 : ll1 - ll2;
			}
			default:
				break;
		}
		return QVariant();
	}
	virtual QString separator() const {
		switch( type ) {
			case Expression::Equals_Expression:
				return " = ";
			case Expression::NEquals_Expression:
				return " != ";
			case Expression::Larger_Expression:
				return " > ";
			case Expression::LargerOrEquals_Expression:
				return " >= ";
			case Expression::Less_Expression:
				return " < ";
			case Expression::LessOrEquals_Expression:
				return " <= ";
			case Expression::BitAnd_Expression:
				return " & ";
			case Expression::BitOr_Expression:
				return " | ";
			case Expression::BitXor_Expression:
				return " ^ ";
			case Expression::Plus_Expression:
			{
				if( left && right ) {
					QVariant::Type vt1 = left->valueType(), vt2 = right->valueType();
					if( vt1 == QVariant::String && vt2 == QVariant::String )
						return " || ";
				}
				return " + ";
			}
			case Expression::Minus_Expression:
				return " - ";
			case Expression::IsDistinctFrom_Expression:
				return " IS DISTINCT FROM ";
			case Expression::IsNotDistinctFrom_Expression:
				return " IS NOT DISTINCT FROM ";
			default:
				break;
		}
		return QString();
	}
	int operatorPrecedence() const {
		if( type == Expression::Plus_Expression && left && right && left->valueType() == right->valueType() && left->valueType() == QVariant::String )
			return 65;
		return _TwoArgExpression::operatorPrecedence();
	}
	
	virtual bool isConstant() {
		bool ret = _TwoArgExpression::isConstant();
		// All support expression types except for is(Not)DistinctFrom will return null if either side of the expression is null
		// We can take advantage of that and propogate the constant null which will avoid a round-trip query in many cases
		if( !ret && type != Expression::IsDistinctFrom_Expression && type != Expression::IsNotDistinctFrom_Expression )
			ret = (left && left->isConstant() && left->value(Record()).isNull()) || (right && right->isConstant() && right->value(Record()).isNull());
		return ret;
	}
};

class _BetweenExpression : public _ThreeArgExpression
{
public:
	_BetweenExpression( ExpressionPrivate * cmp=0, ExpressionPrivate * left=0, ExpressionPrivate * right=0 )
	: _ThreeArgExpression( Expression::Between_Expression, cmp, left, right )
	{}
	_BetweenExpression( const _BetweenExpression & other ) : _ThreeArgExpression(other) {}
	ExpressionPrivate * copy() const { return new _BetweenExpression(*this); }
	QVariant::Type valueType() const {
		return QVariant::Bool;
	}
	QVariant value( const Record & record ) const
	{
		if( !one || !two || !three ) return QVariant();
		QVariant cmpVal(one->value(record)), leftVal(two->value(record)), rightVal(three->value(record));
		if( cmpVal.isNull() || leftVal.isNull() || rightVal.isNull() ) return QVariant(cmpVal.userType());
		return gte(cmpVal,leftVal) && lte(cmpVal,rightVal);
	}
	QString toString(const ToStrContext & pc) const {
		if( !one || !two || !three ) return QString();
		TS_CTX;
		QString cmp = one->toString(c), left = two->toString(c), right = three->toString(c);
		cmp = cmp + " BETWEEN " + left + " AND " + right;
		if( needParens(pc) )
			cmp = "(" + cmp + ")";
		return cmp;
	}
};

class _OverlapsExpression : public _CompoundExpression
{
public:
	_OverlapsExpression( Expression::Type type = Expression::Overlaps_Expression, ExpressionPrivate * start1=0,
							ExpressionPrivate * end1=0, ExpressionPrivate * start2=0, ExpressionPrivate * end2=0 )
	: _CompoundExpression( type )
	{
		append(start1); append(end1); append(start2); append(end2);
	}
	_OverlapsExpression( const _OverlapsExpression & other ) : _CompoundExpression(other) {}
	ExpressionPrivate * copy() const { return new _OverlapsExpression(*this); }
	QVariant::Type valueType() const {
		return QVariant::Bool;
	}
	QVariant value( const Record & record ) const
	{
		if( expressions.size() != 4 ) return QVariant();
		
		QVariant start1Val(expressions[0]->value(record)), end1Val(expressions[1]->value(record)),
			start2Val(expressions[2]->value(record)), end2Val(expressions[3]->value(record));
		
		if( start1Val.isNull() || end1Val.isNull() || start2Val.isNull() || end2Val.isNull() ) return QVariant(start1Val.userType());
		
		if( start1Val.userType() == end1Val.userType() && gt(start1Val,end1Val) )
			qSwap(start1Val,end1Val);
		
		if( start2Val.userType() == end2Val.userType() && gt(start2Val,end2Val) )
			qSwap(start2Val,end2Val);
		
		if( type == Expression::Overlaps_Expression ) {
			bool overlaps;
			if (start1Val == end1Val)
				overlaps = !gt(start2Val,end1Val);
			else
				overlaps = !gte(start2Val,end1Val);
			if( !overlaps ) return false;
			if (start2Val == end2Val)
				overlaps = !gt(start1Val,end2Val);
			else
				overlaps = !gte(start1Val,end2Val);
			return overlaps;
		}
		return !( gt(start1Val,end2Val) || gt(start2Val,end1Val) );
	}
	QString toString(const ToStrContext & pc) const {
		if( expressions.size() != 4 ) return QString();
		TS_CTX;
		QString s1(expressions[0]->toString(c)), e1(expressions[1]->toString(c)),
			s2(expressions[2]->toString(c)), e2(expressions[3]->toString(c));
		if( type == Expression::OverlapsOpen_Expression ) {
			e1 += " + interval'1us'";
			e2 += " + interval'1us'";
		}
		s1 = "(" + s1 + "," + e1 + ") OVERLAPS (" + s2 + "," + e2 + ")";
		if( needParens(pc) )
			s1 = "(" + s1 + ")";
		return s1;
	}
};

class _NotExpression : public _OneArgExpression
{
public:
	_NotExpression( ExpressionPrivate * _child = 0 ) : _OneArgExpression( Expression::Not_Expression, _child )
	{}
	_NotExpression( const _NotExpression & other ) : _OneArgExpression(other) {}
	ExpressionPrivate * copy() const { return new _NotExpression(*this); }
	virtual quint64 hash() const { return type_hash(); }
	QVariant::Type valueType() const {
		return QVariant::Bool;
	}
	QVariant value( const Record & record ) const
	{
		return child ? !child->matches(record) : false;
	}
	QString toString(const ToStrContext & pc) const {
		TS_CTX;
		QString cs = child ? child->toString(c) : QString();
		if( cs.size() && !(cs.startsWith('(') && cs.endsWith(')')) )
			return "NOT (" + cs + ")";
		return "NOT " + cs;
	}
};

class _OrderByExpression : public _TwoArgExpression
{
public:
	_OrderByExpression( ExpressionPrivate * e = 0, ExpressionPrivate * _orderField = 0, Expression::OrderByDirection _dir = Expression::Ascending )
	: _TwoArgExpression( Expression::OrderBy_Expression, e, _orderField )
	, dir(_dir)
	{}
	_OrderByExpression( const _OrderByExpression & other ) : _TwoArgExpression(other), dir(other.dir) {}
	ExpressionPrivate * copy() const { return new _OrderByExpression(*this); }
	
	// The order does not ultimately affect which values are returned unless there is a limit
	virtual quint64 hash() const { return left ? left->hash() : 0; }

	virtual bool isOnlySupplemental() const
	{
		return left ? left->isOnlySupplemental() : true;
	}
	
	virtual ExpressionPrivate * prepareForExec(const PrepContext & pc, Connection * conn) {
		_TwoArgExpression::prepareForExec(pc,conn);
		pc.prepStruct->orderByList.append(Expression(this));
		return left;
	}

	QString toString(const ToStrContext & pc) const {
		TS_CTX;
		QString ret = left ? left->toString(c) : QString();
		if( left && left->type == Expression::OrderBy_Expression )
			ret += ", ";
		else
			ret += " ORDER BY ";
		return ret + right->toString(c) + (dir == Expression::Ascending ? " ASC" : " DESC");
	}
	virtual bool isConstant() {
		return left ? left->isConstant() : true;
	}
	void save( QDataStream & ds )
	{
		ds << QVariant((int)dir);
	}
	void load( QDataStream & ds )
	{
		QVariant v;
		ds >> v; dir = (Expression::OrderByDirection)v.toInt();
	}

	Expression::OrderByDirection dir;
};

class _GroupByExpression : public _TwoArgExpression
{
public:
	_GroupByExpression( ExpressionPrivate * e = 0, ExpressionPrivate * _groupByExpr = 0 )
	: _TwoArgExpression( Expression::GroupBy_Expression, e, _groupByExpr )
	{}
	_GroupByExpression( const _GroupByExpression & other ) : _TwoArgExpression(other) {}
	ExpressionPrivate * copy() const { return new _GroupByExpression(*this); }
	
	// The order does not ultimately affect which values are returned unless there is a limit
	virtual quint64 hash() const { return left ? left->hash() : 0; }

	virtual bool isOnlySupplemental() const
	{
		return left ? left->isOnlySupplemental() : true;
	}
	
	virtual ExpressionPrivate * prepareForExec(const PrepContext & pc, Connection * conn) {
		_TwoArgExpression::prepareForExec(pc,conn);
		pc.prepStruct->groupByList.append(Expression(this));
		return left;
	}

	QString toString(const ToStrContext & pc) const {
		TS_CTX;
		const static QString groupBy(" GROUP BY "), sep(", ");
		QString ret = left ? left->toString(c) : QString();
		if( left && left->type == Expression::GroupBy_Expression )
			ret += sep;
		else
			ret += groupBy;
		return ret + right->toString(c);
	}
	virtual bool isConstant() {
		return left ? left->isConstant() : true;
	}
};

class _LimitExpression : public _OneArgExpression
{
public:
	_LimitExpression( ExpressionPrivate * child = 0, int _limit = 1, int _offset = 0 )
	: _OneArgExpression( Expression::Limit_Expression, child )
	, limit( _limit )
	, offset( _offset )
	{}
	_LimitExpression( const _LimitExpression & other ) : _OneArgExpression(other), limit(other.limit), offset(other.offset) {}
	virtual quint64 hash() const { return type_hash(); }
	ExpressionPrivate * copy() const { return new _LimitExpression(*this); }

	virtual bool isOnlySupplemental() const
	{
		return child ? child->isOnlySupplemental() : true;
	}

	virtual ExpressionPrivate * prepareForExec(const PrepContext & pc, Connection * conn) {
		_OneArgExpression::prepareForExec(pc,conn);
		pc.prepStruct->limit = Expression(this);
		return child;
	}

	QString toString(const ToStrContext & pc) const {
		TS_CTX;
		const static QString limitStr(" LIMIT "), offsetStr(" OFFSET ");
		QString ret(limitStr + QString::number(limit));
		if( child )
			ret = child->toString(c) + ret;
		if( offset > 0 )
			ret = ret + offsetStr + QString::number(offset);
		return ret;
	}
	void save( QDataStream & ds )
	{
		ds << limit << offset;
	}
	void load( QDataStream & ds )
	{
		ds >> limit >> offset;
	}

	int limit, offset;
};

// This is meant to be temporarily used in the expression tree
class _RecordValueExpression : public ExpressionPrivate
{
public:
	_RecordValueExpression( const Record & _record )
	: ExpressionPrivate( Expression::RecordValue_Expression )
	, record(_record)
	{}
	Record record;
};

class _ValueExpression : public ExpressionPrivate
{
public:
	_ValueExpression( const QVariant & _v = QVariant(), bool _escaped=false ) : ExpressionPrivate( Expression::Value_Expression ), v(_v), escaped(_escaped) {}
	_ValueExpression( const _ValueExpression & other ) : ExpressionPrivate(other), v(other.v), escaped(other.escaped) {}
	ExpressionPrivate * copy() const { return new _ValueExpression(*this); }
	virtual quint64 hash() const {
		quint64 vh = qHash(v), ut = v.userType();
		return hash_combine((ut<<32) + vh, (vh<<32) + ut);
	}
	
	QVariant::Type valueType() const {
		return (QVariant::Type)v.userType();
	}
	virtual QVariant value( const Record & ) const
	{
		if( v.userType() == qMetaTypeId<Record>() && !v.value<Record>().isRecord() )
			return QVariant(QVariant::Int);
		return v;
	}
	virtual QString toString(const ToStrContext & c) const {
		QString ret, type;
		bool needQuotes = true;
		switch( v.userType() ) {
			case QVariant::Char:
			case QVariant::String:
				ret = quoteAndEscapeString(v.toString(),escaped);
				needQuotes = false;
				break;
			case QVariant::StringList:
			{
				QStringList qae;
				foreach( QString s, v.toStringList() )
					qae.append( quoteAndEscapeString(s,escaped) );
				ret = qae.join(",");
				if( c.needArray ) {
					ret = "ARRAY[" + qae.join(",") + "]";
					type = "text[]";
				}
				needQuotes = false;
				break;
			}
			case QVariant::Time:
			{
				QTime t = v.toTime();
				if( t.isNull() ) {
					ret = "NULL";
					needQuotes = false;
				} else
					ret = t.toString(Qt::ISODate);
				break;
			}
			case QVariant::Date:
			{
				QDate d = v.toDate();
				if( d.isNull() ) {
					ret = "NULL";
					needQuotes = false;
				} else {
					ret = d.toString(Qt::ISODate);
					type = "date";
				}
				break;
			}
			case QVariant::DateTime:
			{
				QDateTime dt = v.toDateTime();
				if( dt.isNull() ) {
					ret = "NULL";
					needQuotes = false;
				} else {
					ret = dt.toString(Qt::ISODate);
					type = "timestamp";
				}
				break;
			}
			default:
				if( v.userType() == qMetaTypeId<Interval>() ) {
					ret = v.value<Interval>().toString();
					type = "interval";
				} else if( v.userType() == qMetaTypeId<StringMap>() ) {
					ret = v.value<StringMap>().toString();
					type = "hstore";
				} else if( v.userType() == qMetaTypeId<IntList>() ) {
					ret = v.value<IntList>().toString();
					if( c.needArray )
						ret = "ARRAY[" + ret + "]";
					type = "int[]";
				} else if( v.userType() == qMetaTypeId<Record>() ) {
					if( c.stringType == Expression::InfoString )
						ret = v.value<Record>().debug();
					else {
						needQuotes = false;
						Record r = v.value<Record>();
						if( r.isValid() && r.key() )
							ret = QString::number(r.key());
						else
							ret = "NULL";
					}
				} else {
					needQuotes = false;
					if( v.isNull() )
						ret = "NULL";
					else
						ret = v.toString();
				}
		}
		if( needQuotes )
			ret = "'" + ret + "'";
		if( type.size() )
			ret += "::" + type;
		return ret;
	}
	virtual bool isConstant() { return true; }
	void save( QDataStream & ds )
	{
		ds << v << escaped;
	}
	void load( QDataStream & ds )
	{
		ds >> v >> escaped;
	}
	QVariant v;
	bool escaped;
};

class _NullExpression : public ExpressionPrivate
{
public:
	_NullExpression( Field::Type _castType = Field::Invalid ) : ExpressionPrivate( Expression::Null_Expression ), castType(_castType) {}
	_NullExpression( const _NullExpression & other ) : ExpressionPrivate(other), castType(other.castType) {}
	ExpressionPrivate * copy() const { return new _NullExpression(*this); }
	
	QVariant::Type valueType() const {
		return QVariant::Type(Field::qvariantType(castType));
	}
	virtual QVariant value( const Record & ) const
	{
		return castType == Field::Invalid ? QVariant() : QVariant(castType);
	}
	virtual QString toString(const ToStrContext &) const {
		return castType == Field::Invalid ? QString("NULL") : QString("NULL::" + Field::dbTypeString(castType));
	}
	virtual bool isConstant() { return true; }
	Field::Type castType; // Invalid for untyped nulls
};

class _ValuesExpression : public ExpressionPrivate
{
public:
	_ValuesExpression( RecordList records, const QString & _alias, FieldList _fields )
	: ExpressionPrivate( Expression::Values_Expression )
	, fields(_fields)
	, alias(_alias)
	{
		foreach( Record r, records ) {
			foreach( Field * f, fields ) {
				if( f )
					values << r.getValue(f);
				else
					values << QVariant();
			}
		}
	}
	_ValuesExpression( const _ValuesExpression & other ) : ExpressionPrivate(other), fields(other.fields), values(other.values), alias(other.alias) {}
	ExpressionPrivate * copy() const { return new _ValuesExpression(*this); }

	virtual QString toString(const ToStrContext & pc) const {
		int nullCount=0;
		QStringList colNames;
		QStringList rowStrings;
		const int cols = fields.size();
		const int rows = values.size() / cols;
		for( int r = 0; r < rows; r++ ) {
			QStringList valueStrings;
			for( int i = 0; i < cols; ++i ) {
				QString vs = _ValueExpression(values[r*cols+i]).toString(pc);
				if( r == 0 ) {
					Field * f = fields[i];
					if( f ) {
						colNames << fields[i]->name().toLower();
						vs += "::" + fields[i]->dbTypeString();
					} else {
						colNames << "_null_" + QString::number(++nullCount);
						vs = "NULL";
						if( i == 0 )
							vs += "::oid";
					}
				}
				valueStrings += vs;
			}
			rowStrings += "(" + valueStrings.join(",") + ")";
		}
		return "(VALUES " + rowStrings.join(",") + ") AS " + alias + "(\"" + colNames.join("\",\"") + "\")";
	}
	virtual bool isConstant() { return true; }

	FieldList fields;
	QList<QVariant> values;
	QString alias;
};

ExpressionPrivate * _FieldExpression::copy() const
{
	return new _FieldExpression(*this);
}

quint64 _FieldExpression::hash() const
{
	quint64 fh = field ? qHash(field->name()) : 0;
	quint64 th = table ? qHash(table->tableName()) : 0;
	return hash_combine( (fh << 32) + th, (th << 32) + fh );
}

QVariant::Type _FieldExpression::valueType() const
{
	return (QVariant::Type)field->qvariantType();
}

QVariant _FieldExpression::value( const Record & record ) const
{
	QVariant ret = record.getValue(field);
	if( ret.userType() == qMetaTypeId<Record>() ) {
		if( !ret.value<Record>().isValid() )
			ret = QVariant(QVariant::Int);
	}
	// A null record will return a null QVariant without the proper
	// type, we change it to the proper type here
	if( ret.isNull() && ret.userType() == QVariant::Invalid )
		return QVariant(QVariant::Type(valueType()));
	return ret;
}

QString _FieldExpression::toString(const ToStrContext & pc) const
{
	const bool isInfo = pc.stringType == Expression::InfoString;
	if( pc.exp && pc.exp->type == Expression::Alias_Expression ) {
		_AliasExpression * ae = (_AliasExpression*)pc.exp;
		if( isInfo )
			return ae->tableAlias + "." + field->name();
		return ae->tableAlias + "." + pc.conn->prepareFieldName(field);
	}
	if( isInfo )
		return field->name();
	Connection * conn = pc.conn;
	if( !conn ) conn = Database::current(field->table()->schema())->connection();
	return conn ? conn->prepareFieldName(field) : field->name();
}

void _FieldExpression::save( QDataStream & ds )
{
	ds << QVariant(table->tableName()) << QVariant(field->name());
}

void _FieldExpression::load( QDataStream & ds )
{
	QVariant v;
	ds >> v;
	table = Database::current()->schema()->tableByName(v.toString());
	ds >> v;
	if( table )
		field = table->field(v.toString());
}

class _TableOidExpression : public ExpressionPrivate
{
public:
	_TableOidExpression() : ExpressionPrivate( Expression::TableOid_Expression ) {}
	_TableOidExpression( const _TableOidExpression & other ) : ExpressionPrivate(other) {}
	ExpressionPrivate * copy() const { return new _TableOidExpression(*this); }
	QVariant::Type valueType() const {
		return QVariant::Int;
	}
	virtual QString toString(const ToStrContext &) const {
		return "tableoid";
	}
};

class _TableExpression : public ExpressionPrivate
{
public:
	_TableExpression( Table * t = 0, bool _only = false ) : ExpressionPrivate( Expression::Table_Expression ), table(t), only(_only) {}
	_TableExpression( const _TableExpression & other ) : ExpressionPrivate(other), table(other.table), only(other.only) {}
	ExpressionPrivate * copy() const { return new _TableExpression(*this); }

	virtual QString toString(const ToStrContext &) const {
		QString ret = "\"" + table->tableName().toLower() + "\"";
		if( only && Database::current(table->schema()->schema())->connection()->capabilities() & Connection::Cap_Inheritance )
			ret = "ONLY " + ret;
		return ret;
	}
	void save( QDataStream & ds )
	{
		ds << QVariant(table->schema()->tableName());
	}
	void load( QDataStream & ds )
	{
		QVariant v;
		ds >> v;
		TableSchema * ts = Database::current()->schema()->tableByName(v.toString());
		table = ts ? ts->table() : 0;
		ds >> only;
	}
	Table * table;
	bool only;
};

class _QueryExpression : public _OneArgExpression
{
public:
	_QueryExpression( ExpressionList _returns = ExpressionList(), ExpressionList _fromList = ExpressionList(), ExpressionPrivate * _child = 0 )
	: _OneArgExpression( Expression::Query_Expression, _child )
	, returns(_returns)
	, fromList(_fromList)
	{
		returnCount = returns.size();
	}
	_QueryExpression( const _QueryExpression & other ) : _OneArgExpression(other), returns(other.returns), fromList(other.fromList), returnCount(other.returnCount) {}
	ExpressionPrivate * copy() const { return new _QueryExpression(*this); }
	
	virtual ExpressionPrivate * prepareForExec(const PrepContext & pc, Connection * conn) {
		PREP_CTX;
		// We don't want to pass outer order by, group by and limits into an inner query
		// So we save and restore them here
		ExpressionList saveOrders = pc.prepStruct->orderByList;
		ExpressionList saveGroups = pc.prepStruct->groupByList;
		pc.prepStruct->orderByList = ExpressionList();
		pc.prepStruct->groupByList = ExpressionList();
		Expression saveLimit = pc.prepStruct->limit;
		pc.prepStruct->limit = Expression();
		// Do the children first, to avoid modifying Table expressions that are
		// part of an already modified expression
		_OneArgExpression::prepareForExec(pc, conn);
		
		pc.prepStruct->constantNoResults = child && child->isConstant() && !child->value(Record()).toBool();

		// If there is a valid changeset enabled, then we must
		// transform the query to reflect updated records for
		// each from table
		if( ChangeSet::current().isValid() ) {
			if( !pc.prepStruct->hasSubQuery ) {
				const Context * p = &pc;
				while( p ) {
					if( p->exp && (p->exp->type == Expression::Query_Expression || p->exp->type == Expression::In_Expression || p->exp->type == Expression::NotIn_Expression) ) {
						c.prepStruct->hasSubQuery = true;
						break;
					}
					p = p->par;
				}
			}
			
			if( !pc.prepStruct->hasSubQuery )
				return this;
			
			for( int i = fromList.size() - 1; i >= 0; --i ) {
				Expression from = fromList[i];
				
				if( from.type() == Expression::Table_Expression ) {
					RecordList added, updated, removed;
					Table * table = ((_TableExpression*)from.p)->table;
					ChangeSet::current().visibleRecordsChanged( &added, &updated, &removed, QList<Table*>() << table );
					if( added.isEmpty() && updated.isEmpty() && removed.isEmpty() )
						continue;
					Field * pkey = table->schema()->field(table->schema()->primaryKeyIndex());
					bool haveExclusions = updated.size() + removed.size() > 0;
					ExpressionList returns;
					FieldList valuesFields;
					// Inheritance selects need tableoid
					if( table->children().size() ) {
						returns.append(Expression::tableOid());
						valuesFields << (Field*)nullptr;
					}
					foreach( Field * f, table->schema()->fields() ) {
						if( f->flag(Field::LocalVariable) ) continue;
						returns.append( Expression::createField(f) );
						valuesFields << f;
					}
					Expression mod = Query( returns, from, haveExclusions ? !Expression::createField(pkey).in(updated+removed) : Expression() );
					if( added.size() + updated.size() > 0 ) {
						// Ensure each record has primary keys
						foreach( Record r, added ) r.key(true);
						foreach( Record r, updated ) r.key(true);
						
						mod = Expression::createCombination( Expression::UnionAll_Expression, ExpressionList() << mod << Query( ExpressionList(), Expression::createValues( added + updated, "v" , &valuesFields), Expression() ) );
					}
					//LOG_1( QString("Replacing expression (%1)" + from.toString() + " with(%2) " + mod.toString()).arg((quint64)from.p,16).arg((quint64)mod.p,16));
					fromList.replace(i, mod);
				}
			}
		}
		
		foreach( Expression exp, pc.prepStruct->orderByList ) {
			_OrderByExpression * o = (_OrderByExpression*)exp.p;
			if( o->left )
				o->left->deref();
			o->left = child;
			o->ref();
			child = o;
		}
		foreach( Expression exp, pc.prepStruct->groupByList ) {
			_OrderByExpression * o = (_OrderByExpression*)exp.p;
			if( o->left )
				o->left->deref();
			o->left = child;
			o->ref();
			child = o;
		}
		if( pc.prepStruct->limit.p ) {
			_LimitExpression * l = (_LimitExpression*)pc.prepStruct->limit.p;
			if( l->child )
				l->child->deref();
			l->child = child;
			l->ref();
			child = l;
		}
		pc.prepStruct->orderByList = saveOrders;
		pc.prepStruct->groupByList = saveGroups;
		pc.prepStruct->limit = saveLimit;
		return this;
	}
	
	// Translates each FROM expression
	void transformToInheritedUnion(TableList tables, QList<RecordReturn> & rrl)
	{
		QMap<Table*,FieldList> fieldsByTable;
		QVector<int> typesByPosition(1);
		typesByPosition.reserve(32);
		QList<QVariant> allArgs;
		
		Expression innerFilter;
		{
			ExpressionPrivate * c = child;
			while( c ) {
				if( c->type == Expression::Limit_Expression )
					c = ((_LimitExpression*)c)->child;
				else if( c->type == Expression::OrderBy_Expression )
					c = ((_OrderByExpression*)c)->left;
				else
					break;
			}
			if( c )
				innerFilter = Expression(c);
		}
		
		// First position is the table oid
		typesByPosition[0] = Field::Int;
		RecordReturn rr;
		rr.tableOidPos = 0;

		foreach( Table * table, tables ) {
			TableSchema * schema = table->schema();
			FieldList fields = schema->fields();
			fieldsByTable[table] = fields;
			QVector<int> positions(fields.size());
			int queryCol = 1, recordCol = 0;
			foreach( Field * f, fields ) {
				if( f->flag( Field::NoDefaultSelect | Field::LocalVariable ) ) {
					positions[recordCol++] = -1;
					continue;
				}
				while( queryCol < typesByPosition.size() && typesByPosition[queryCol] != f->type() )
					queryCol++;
				if( queryCol >= typesByPosition.size() ) {
					typesByPosition.resize(queryCol+1);
					typesByPosition[queryCol] = f->type();
				}
				positions[recordCol++] = queryCol++;
			}
			
			rr.columnPositions[table] = positions;
		}

		int tablePos = 0;
		ExpressionList unionQueries;

		foreach( Table * table, tables ) {
			TableSchema * schema = table->schema();
			FieldList fields = schema->fields();
			fieldsByTable[table] = fields;
			int queryCol = 1;
			ExpressionList returns;
			returns.append( Expression::tableOid() );
			foreach( Field * f, fields ) {
				if( f->flag( Field::NoDefaultSelect | Field::LocalVariable ) )
					continue;
				while( queryCol < typesByPosition.size() && typesByPosition[queryCol] != f->type() ) {
					if( tablePos == 0 )
						returns.append( Expression::null(f->type()).alias(QString("c%1").arg(queryCol)) );
					else
						returns.append( Expression::null() );
					queryCol++;
				}
				Expression fe = Expression::createField(f);
				//if( tablePos == 0 )
				//	fe = fe.alias( QString("c%1").arg(queryCol) );
				returns.append(fe);
				queryCol++;
			}
			while( queryCol < typesByPosition.size() ) {
				if( tablePos == 0 )
					returns.append( Expression::null(Field::Type(typesByPosition[queryCol])).alias(QString("c%1").arg(queryCol)) );
				else
					returns.append( Expression::null() );
				queryCol++;
			}
			unionQueries.append( Query( returns, Expression(table,/*only=*/true), innerFilter ) );
			tablePos++;
		}

		Expression union_ = Expression::createCombination( Expression::UnionAll_Expression, unionQueries );
		fromList = ExpressionList(union_);
		rrl.append(rr);
	}
	
	QVariant::Type valueType() const {
		if( returns.size() == 1 )
			return returns[0].p->valueType();
		return _OneArgExpression::valueType();
	}
	virtual QString toString(const ToStrContext & pc) const
	{
		TS_CTX;
		QString sret, sfrom, schild;

		if( returns.size() ) {
			QStringList retStrings;
			retStrings.reserve(returns.size());
			foreach( Expression ret, returns )
				retStrings += ret.p->toString(c);
			sret = retStrings.join(", ");
		} else
			sret = "*";
		
		if( fromList.size() ) {
			QStringList fromClauses;
			fromClauses.reserve(fromList.size());
			foreach( Expression from, fromList )
				if( from.p ) {
					QString fromStr = from.p->toString(c);
					if( from.p->type == Expression::Query_Expression || (from.p->type >= Expression::Union_Expression && from.p->type <= Expression::ExceptAll_Expression) )
						fromStr = "(" + fromStr + ") AS iq";
					fromClauses += fromStr;
				}
			sfrom = " FROM " + fromClauses.join(",");
		}
		
		if( child )
			schild = QString( !child->isOnlySupplemental() ? " WHERE " : "" ) + child->toString(c);
		
		// If we aren't a top-level query then we surround ourselves with ()
		const bool needParens = pc.exp && (pc.exp->type < Expression::Union_Expression || pc.exp->type > Expression::ExceptAll_Expression);
		if( needParens )
			return "(SELECT " + sret + sfrom + schild + ")";
		
		return "SELECT " + sret + sfrom + schild;
	}
	
	virtual bool isConstant() { return false; }

	virtual void setChildren( const QList<ExpressionPrivate*> & children ) {
		ExpressionList _returns, _fromList;
		_returns.reserve( returnCount );
		_fromList.reserve( children.count() - 1 - returnCount );
		for( int i = 0; i < children.count(); ++i ) {
			if( i < returnCount )
				_returns += Expression(children[i]);
			else if( i < children.count() - 1 )
				_fromList += Expression(children[i]);
			else {
				child = children[i];
			}
		}
		returns = _returns;
		fromList = _fromList;
	}
	virtual QList<ExpressionPrivate*> children() const {
		QList<ExpressionPrivate*> ret;
		ret.reserve(returns.size() + fromList.size() + 1);
		foreach( Expression e, returns )
			ret.append(e.p);
		foreach( Expression e, fromList )
			ret.append(e.p);
		if( child )
			ret.append(child);
		return ret;
	}

	virtual void save( QDataStream & ds )
	{
		ds << returns.size();
	}
	virtual void load( QDataStream & ds )
	{
		ds >> returnCount;
	}
	
	ExpressionList returns, fromList;
	int returnCount;
};

class _CombinationExpression : public _CompoundExpression
{
public:
	_CombinationExpression(Expression::Type type)
	: _CompoundExpression( type )
	{}
	_CombinationExpression( const _CombinationExpression & other ) : _CompoundExpression(other) {}
	ExpressionPrivate * copy() const { return new _CombinationExpression(*this); }

	virtual QString toString(const ToStrContext & pc) const
	{
		TS_CTX;
		const char * combStrings [] = { " UNION ", " INTERSECT ", " EXCEPT " };
		
		int idx = qBound(0, (int)type - (int)Expression::Union_Expression, 5);
		QString comb(combStrings[idx/2]);
		if( idx % 2 )
			comb += "ALL ";
		QStringList subs;
		foreach( ExpressionPrivate * e, expressions )
			subs.append( e->toString(c) );
		return subs.join(comb);
	}
};

class _SqlExpression : public ExpressionPrivate
{
public:
	_SqlExpression(const QString & _sql = QString())
	: ExpressionPrivate( Expression::Sql_Expression )
	, sql(_sql)
	{}
	_SqlExpression( const _SqlExpression & other ) : ExpressionPrivate(other), sql(other.sql) {}
	ExpressionPrivate * copy() const { return new _SqlExpression(*this); }
	
	virtual QString toString(const ToStrContext &) const
	{
		return sql;
	}

	virtual void save( QDataStream & ds )
	{
		ds << QVariant(sql);
	}
	virtual void load( QDataStream & ds )
	{
		QVariant v;
		ds >> v; sql = v.toString();
	}

	QString sql;
};

class _PlaceHolderExpression : public _OneArgExpression
{
public:
	_PlaceHolderExpression(const QString & _name = QString(), const QVariant & _defaultValue = QVariant(), bool _cacheAll = false )
	: _OneArgExpression( Expression::PlaceHolder_Expression, 0 )
	, name(_name)
	, defaultValue(_defaultValue)
	, cacheAll(_cacheAll)
	, isCacheAllReplacement(false)
	{}
	_PlaceHolderExpression( const _PlaceHolderExpression & other ) : _OneArgExpression(other), name(other.name), defaultValue(other.defaultValue), cacheAll(other.cacheAll)
	, isCacheAllReplacement(other.isCacheAllReplacement)
	{
		if( child ) {
			child->deref();
			child = 0;
		}
	}
	ExpressionPrivate * copy() const { return new _PlaceHolderExpression(*this); }

	virtual quint64 hash() const { return type_hash(); }

	virtual ExpressionPrivate * prepareForExec(const PrepContext & pc, Connection * )
	{
		if( child ) return this;

		// We go ahead and eat the arg even if we are caching all
		PrepStruct & ps = *pc.prepStruct;
		QVariant value = defaultValue;
		if( !name.isEmpty() && ps.namedArgs.contains(name) )
			value = ps.namedArgs[name];
		else if( ps.positionalArgs.size() > ps.positionalArgsUsed ) {
			value = ps.positionalArgs[ps.positionalArgsUsed++];
			ps.namedArgs[name] = value;
		}

		// If we are cacheAll and our parent is an equals expression we can simply set isCacheAllReplacement=true
		// and when prepareForExec is called the Equals_Expression will return 0, eliminating itself from the expression tree
		if( cacheAll && pc.prepStruct->removeCacheAllExpressions && pc.exp && pc.exp->type == Expression::Equals_Expression ) {
			isCacheAllReplacement = true;
		}
		else {
			ExpressionPrivate * target = 0;
			value = prepareForCacheAccess( value, const_cast<ExpressionPrivate*>(pc.exp), &target );
			if( value.type() == QVariant::List ) {
				assert(target);
				QList<QVariant> values = value.toList();
				child = new _InExpression(target);
				foreach( QVariant v, values )
					child->append( new _ValueExpression(v) );
			} else
				child = new _ValueExpression(value);
			if( child )
				child->ref();
		}
		return this;
	}

	ExpressionPrivate * findSibling( ExpressionPrivate * parent )
	{
		ExpressionPrivate * ret = 0;
		if( !parent ) return ret;
		QList<ExpressionPrivate*> children = parent->children();
		if( children.size() == 2 ) {
			// Our node could be on either side
			ret = children[0];
			if( ret == this )
				ret = children[1];
		}
		return ret;
	}
	QVariant prepareForCacheAccess( const Record & r, ExpressionPrivate * parent, ExpressionPrivate ** targetOut = 0 )
	{
		ExpressionPrivate * target = findSibling(parent);
		if( target ) {
			if( targetOut )
				*targetOut = target;
			if( target->type == Expression::Field_Expression ) {
				Field * f = ((_FieldExpression*)target)->field;
				TableSchema * fs = ((_FieldExpression*)target)->table;
				TableSchema * rs = r.table()->schema();
				if( f->flag(Field::ForeignKey) ) {
					TableSchema * fkt = f->foreignKeyTable();
					if( fkt->isDescendant(rs) )
						return QVariant(r.key());
					foreach( Field * field, rs->fields() ) {
						if( field->flag(Field::ForeignKey) && fkt->isDescendant(field->foreignKeyTable()) ) {
							return r.getValue(field);
						}
					}
				}
				else if( f->flag(Field::PrimaryKey) ) {
					if( fs->isDescendant(r.table()->schema()) )
						return QVariant(r.key());
					foreach( Field * field, rs->fields() ) {
						if( field->flag(Field::ForeignKey) && fs->isDescendant(field->foreignKeyTable()) ) {
							return r.getValue(field);
						}
					}
				}
			}
		}
		return QVariant(r.key());
	}
	
	QVariant prepareForCacheAccess( const QVariant & value, ExpressionPrivate * parent, ExpressionPrivate ** target = 0 )
	{
		if( value.userType() == qMetaTypeId<Record>() ) {
			return prepareForCacheAccess(qvariant_cast<Record>(value), parent, target);
		} else if( value.userType() == qMetaTypeId<RecordList>() ) {
			RecordList rl = value.value<RecordList>();
			if( rl.isEmpty() ) return QVariant();
			QList<QVariant> ret;
			foreach( Record r, rl )
				ret.append( prepareForCacheAccess(r,parent,target) );
			return ret.size() == 1 ? ret[0] : QVariant(ret);
		} else if( value.type() == QVariant::List ) {
			QList<QVariant> ret, list(value.toList());
			if( list.isEmpty() ) return QVariant();
			foreach( QVariant v, list )
				ret.append( prepareForCacheAccess(v,parent,target) );
			return ret.size() == 1 ? ret[0] : QVariant(ret);
		}
		if( target && !(*target) )
			*target = findSibling(parent);
		return value;
	}
	
	virtual QVariant value( const Record & record ) const
	{ return isCacheAllReplacement ? QVariant(true) : _OneArgExpression::value(record); }

	virtual QVariant::Type valueType() const
	{ return isCacheAllReplacement ? QVariant::Bool : (child ? child->valueType() : _OneArgExpression::valueType()); }

	virtual QString toString(const ToStrContext & pc) const
	{
		if( isCacheAllReplacement ) return "TRUE";
		if( child ) return child->toString(pc);
		if( !name.isEmpty() ) return ":" + name;
		return "?";
	}
	virtual void save( QDataStream & ds )
	{
		ds << QVariant(name) << defaultValue;
	}
	virtual void load( QDataStream & ds )
	{
		QVariant v;
		ds >> v >> defaultValue;
		name = v.toString();
	}
	ExpressionPrivate * equalityExpression(ExpressionPrivate * parent)
	{
		if( parent->type != Expression::Equals_Expression )
			return 0;
		QList<ExpressionPrivate*> sibs = parent->children();
		if( sibs.size() != 2 )
			return 0;
		if( sibs[0] == this )
			return sibs[1];
		return sibs[0];
	}
	bool canExtractValues(ExpressionPrivate * parent) {
		return equalityExpression(parent) != 0;
	}
	QVariant extractValue(ExpressionPrivate*parent, const Record & record) {
		ExpressionPrivate * eq = equalityExpression(parent);
		if( eq )
			return eq->value(record);
		return QVariant();
	}
	QString name;
	QVariant defaultValue;
	bool cacheAll, isCacheAllReplacement;
};

ExpressionPrivate * _ComparisonExpression::prepareForExec( const PrepContext & pc, Connection * conn )
{
	_TwoArgExpression::prepareForExec(pc,conn);
	_PlaceHolderExpression * phe = 0;
	if( left && left->type == Expression::PlaceHolder_Expression )
		phe = (_PlaceHolderExpression*)left;
	else if( right && right->type == Expression::PlaceHolder_Expression )
		phe = (_PlaceHolderExpression*)right;
	if( phe ) {
		if( phe->isCacheAllReplacement )
			return 0;
		// X == PlaceHolder() transformed to X IN (....)
		if( phe->child && phe->child->type == Expression::In_Expression )
			return phe->child;
	}
	return this;
}

class _SetPlaceHolderExpression : public _OneArgExpression
{
public:
	_SetPlaceHolderExpression( ExpressionPrivate * child = 0, const QString & _name = QString(), const QVariant & _value = QVariant() )
	: _OneArgExpression( Expression::SetPlaceHolder_Expression, child )
	, name(_name)
	, value(_value)
	{}
	_SetPlaceHolderExpression( const _SetPlaceHolderExpression & other ) : _OneArgExpression(other), name(other.name), value(other.value)
	{}
	ExpressionPrivate * copy() const { return new _SetPlaceHolderExpression(*this); }
	virtual ExpressionPrivate * prepareForExec(const PrepContext & pc, Connection * conn) {
		PrepStruct & ps = *pc.prepStruct;
		if( !ps.namedArgs.contains(name) ) {
			//LOG_1( "Setting " + name + " to " + value.toString() + " type " + value.typeName() );
			ps.namedArgs[name] = value;
		}
		return _OneArgExpression::prepareForExec(pc, conn);
	}
	virtual QString toString(const ToStrContext & pc) const
	{
		if( child ) return child->toString(pc);
		return QString();
	}
	virtual void save( QDataStream & ds )
	{
		ds << QVariant(name) << value;
	}
	virtual void load( QDataStream & ds )
	{
		QVariant v;
		ds >> v >> value;
		name = v.toString();
	}
	QString name;
	QVariant value;
};

/*static void printArgs(const QList<QVariant> & args)
{
	QStringList toPrint;
	foreach(QVariant v, args)
		toPrint += v.toString();
	LOG_1( toPrint.join(",") );
}*/

/* Returns a new QList with size list.size() * replace.size().  The returned QList contains
 * the elements of list repeated, with index replaced each time with a value from replace */
/*
static QList<QVariant> listMultiplyReplace( const QList<QVariant> & list, const QList<QVariant> replace, int index )
{
	QList<QVariant> ret;
	int lsize = list.size();
	for( int i=0, end=replace.size(); i<end; ++i ) {
		ret.append(list);
		ret.replace(lsize*i+index,replace[i]);
	}
	return ret;
}*/

// Basically a proxy for the child node that acts like it doesn't exist, unless
// it's the root node of the tree, in which case the Expression object will use
// the ExpressionIndex.
class _CacheExpression : public _OneArgExpression
{
public:
	_CacheExpression( ExpressionPrivate * child=0, const Interval & _maxAge = Interval(), bool _useParentCache = true, bool _fillParentCache = false )
	: _OneArgExpression( Expression::Cache_Expression, child )
	, index(0)
	, maxAge(_maxAge)
	, useParentCache(_useParentCache)
	, fillParentCache(_fillParentCache)
	{}
	
	// Might want to think further about possible complications of sharing the index
	// another possibility would be to remove ourselves from the tree during copy
	// by returning a copy of our child from the copy() function below.
	_CacheExpression( const _CacheExpression & other )
	: _OneArgExpression( other )
	, index(other.index)
	, expressionIndex(other.expressionIndex)
	, maxAge( other.maxAge )
	, useParentCache(other.useParentCache)
	, fillParentCache(other.fillParentCache)
	{
	}
	
	~_CacheExpression()
	{}
	
	ExpressionPrivate * copy() const {
		if( !removeMyselfDuringCopy.tryLock() )
			return new _ValueExpression(QVariant(true));
		removeMyselfDuringCopy.unlock();
		return new _CacheExpression(*this);
	}
	
	virtual QString toString(const ToStrContext & pc) const
	{
		if( child ) return child->toString(pc);
		return _OneArgExpression::toString(pc);
	}

	/*
	 * This function will attempt to find a SimpleIndex that matches
	 * the expression.  Only simple Field == PlaceHolder or Field.in(PlaceHolder) expressions
	 * will work.  This way foreign key accessors and reverse accessors
	 * can share the same index as a caching expression.
	 */
	void setupIndex(Table * table)
	{
		if( child->type == Expression::Equals_Expression || child->type == Expression::In_Expression ) {
			_FieldExpression * fieldExpression = 0;
			ExpressionPrivate * placeHolder = 0;
			QList<ExpressionPrivate*> eqKids = child->children();
			if( eqKids.size() == 2 ) {
				foreach( ExpressionPrivate * kid, eqKids ) {
					if( kid->type == Expression::Field_Expression )
						fieldExpression = (_FieldExpression*)kid;
					else if( kid->type == Expression::PlaceHolder_Expression )
						placeHolder = kid;
				}
				if( fieldExpression && placeHolder && fieldExpression->field->index() ) {
					IndexSchema * is = fieldExpression->field->index();
					index = is->table()->table()->indexFromSchema(is);
					return;
				}
			}
		}
		expressionIndex = QSharedPointer<ExpressionIndex>(new ExpressionIndex(table,Expression(child)));
		index = expressionIndex.data();
	}
	
	/*
	 * This function sets up the actual index if needed, and interacts with any parent
	 * caches if necessary.
	 */
	virtual RecordList recordsByIndex( Table * table, const QList<QVariant> & args, int lookupMode, Interval maxAge = Interval() )
	{
		if( !child )
			return RecordList();
		if( !index )
			setupIndex(table);
		if( maxAge == 0 && this->maxAge > 0 )
			maxAge = this->maxAge;
		if( useParentCache ) {
			int placeHoldersOnLeft = 0, placeHolderCount = 0;
			_CacheExpression * parentCache = findParentCache(placeHoldersOnLeft,placeHolderCount);
			if( parentCache && (parentCache->index || fillParentCache) ) {
				RecordList ret;
				QList<QVariant> subArgs = args.mid(placeHoldersOnLeft,placeHolderCount);
				int status = Index::NoInfo;
				if( !fillParentCache ) {
					QDateTime timestamp;
					ret = parentCache->index->records(subArgs,status,&timestamp);
					if( status == Index::RecordsFound && maxAge > 0 && Interval(timestamp,QDateTime::currentDateTime()) > maxAge )
						status = Index::NoInfo;
				} else {
					ret = parentCache->recordsByIndex(table,subArgs,lookupMode,maxAge);
					status = Index::RecordsFound;
				}
				if( status == Index::RecordsFound ) {
					if( placeHolderCount > 0 )
						subArgs = args.mid(0,placeHoldersOnLeft) + args.mid(placeHoldersOnLeft + placeHolderCount);
					else
						subArgs = args;
					parentCache->removeMyselfDuringCopy.lock();
					Expression filterExp = Expression(child).prepareForExec(Database::current()->connection(), subArgs, QMap<QString,QVariant>(), 0, true, false );
					parentCache->removeMyselfDuringCopy.unlock();
					ret = ret.filter(filterExp);
					index->recordsIncoming(ret,true);
					return ret;
				}
			}
		}
		return index->recordsByIndex(args,lookupMode,maxAge);
	}
	
	Index * index;
	QSharedPointer<ExpressionIndex> expressionIndex;
	Interval maxAge;
	bool useParentCache, fillParentCache;
	mutable QMutex removeMyselfDuringCopy;
};

void ExpressionPrivate::gatherPlaceHolders(GatherPlaceHolders * gph) {
	if( type == Expression::SetPlaceHolder_Expression ) {
		_SetPlaceHolderExpression * phe = (_SetPlaceHolderExpression*)this;
		gph->setters.append(phe);
	}
	foreach( ExpressionPrivate * child, children() ) {
		if( child->type == Expression::PlaceHolder_Expression ) {
			_PlaceHolderExpression * phe = (_PlaceHolderExpression*)child;
			if( !gph->placeHolders.contains(phe) ) {
				gph->placeHolders.append(phe);
				gph->parents.append(this);
			}
			if( gph->canExtractValues && (gph->insideSubQuery || !phe->canExtractValues(this)) )
				gph->canExtractValues = false;
				
		}
		bool isSub = (child->type == Expression::Query_Expression);
		if( isSub ) gph->insideSubQuery++;
		child->gatherPlaceHolders(gph);
		if( isSub ) gph->insideSubQuery--;
	}
}

class _TempTableExpression : public _OneArgExpression
{
public:
	static QAtomicInt tempTableNum;
	
	static QString generateTempTableName()
	{
		const static QString temp("temp_");
		int myNum = tempTableNum.fetchAndAddRelaxed(1);
		return temp + QString::number(myNum);
	}
	
	_TempTableExpression( ExpressionPrivate * child, const QString & _tempTableName )
	: _OneArgExpression( Expression::TempTable_Expression, child )
	, tempTableName( _tempTableName )
	{
		if( tempTableName.isEmpty() )
			tempTableName = generateTempTableName();
	}
	
	QString toString( const ToStrContext & pc ) const
	{
		TS_CTX;
		const static QString createTempTable("CREATE TEMP TABLE "), as(" AS ");
		return createTempTable + tempTableName + as + child->toString(c);
	}
	
	void execute( Connection * )
	{
		
	}
	QString tempTableName;
};

QAtomicInt _TempTableExpression::tempTableNum(1);


class _FutureExpression;

FutureSignalObject::FutureSignalObject(QObject * parent)
: QObject(parent)
{}

class ExpressionTask : public ThreadTask
{
public:
	ExpressionTask( _FutureExpression * fe );
	~ExpressionTask();
	void run();
protected:
	_FutureExpression * future;
};

class _FutureExpression : public _OneArgExpression
{
public:
	_FutureExpression( ExpressionPrivate * child, const QList<QVariant> & _positionalArgs, const QMap<QString,QVariant> & _namedArgs, int _lookupMode )
	: _OneArgExpression( Expression::Future_Expression, child )
	, validInt(0)
	, positionalArgs(_positionalArgs)
	, namedArgs(_namedArgs)
	, signalObject(nullptr)
	, lookupMode(_lookupMode)
	, database( Database::current() )
	, connection( database->connection() )
	{
		FreezerCore::addTask( new ExpressionTask(this) );
	}
	
	ExpressionPrivate * copy() const { _FutureExpression * p = const_cast<_FutureExpression*>(this); p->ref(); return p; }

	RecordList get()
	{
		if( !valid() )
			wait();
		return result;
	}
	
	
	bool valid() const
	{
		return qAtomicLoad(validInt) > 0;
	}
	
	void wait()
	{
		if( valid() ) return;
		/* We have to check valid again with the mutex locked,
		 * otherwise we risk a race condition where valid is set
		 * and the wait condition triggered after we check valid
		 * and before we wait */
		mutex.lock();
		if( !valid() )
			waitCond.wait(&mutex);
		mutex.unlock();
	}
	
	void run()
	{
		//Database::setCurrent( database );
		//database->setConnection( connection );
		result = Expression(child).select(positionalArgs,namedArgs,lookupMode,0);
		mutex.lock();
		validInt.fetchAndAddRelaxed(1);
		mutex.unlock();
		waitCond.wakeAll();
		if( signalObject )
			signalObject->then(ExpressionFuture(this));
	}
	
	void then( QObject * receiver, const char * method )
	{
		if( !signalObject ) {
			signalObject = new FutureSignalObject();
			if( !QMetaType::type("ExpressionFuture") )
				qRegisterMetaType<ExpressionFuture>("ExpressionFuture");
		}
		signalObject->connect( signalObject, SIGNAL(then(ExpressionFuture)), receiver, method );
	}

	QAtomicInt validInt;
	QMutex mutex;
	QWaitCondition waitCond;
	RecordList result;
	QList<QVariant> positionalArgs;
	QMap<QString,QVariant> namedArgs;
	FutureSignalObject * signalObject;
	int lookupMode;
	Database * database;
	Connection * connection;
};

ExpressionPrivate * _InExpression::prepareForExec(const PrepContext & pc, Connection * conn)
{
	const int size = expressions.size();
	
	if( size >= 2 ) {
		ExpressionPrivate * left = expressions[0];
		if( left->type == Expression::Field_Expression ) {
			Field * f = ((_FieldExpression*)left)->field;
			TableSchema * ts = f->foreignKeyTable();
			Field * defaultLookup = ts ? ts->defaultLookupField() : 0;
			
			if( defaultLookup ) {
				bool allStringVals = true;
				
				for( int i=1, end=size; i<end; ++i ) {
					ExpressionPrivate * e = expressions[i];
					if( e->type != Expression::Value_Expression || ((_ValueExpression*)e)->v.type() != QVariant::String ) {
						allStringVals = false;
						break;
					}
				}
			
				if( allStringVals ) {
					Index * defaultLookupIndex = Database::current()->tableFromSchema(ts)->indexFromSchema(defaultLookup->index());
					QList<QVariant> vals;
					vals.reserve(expressions.size()-1);
					for( int i=1, end=size; i<end; ++i ) {
						_ValueExpression * ve = (_ValueExpression*)expressions[i];
						vals << ve->v;
					}
					RecordList results = defaultLookupIndex->recordsByIndexMulti(vals);
					const int resultSize = results.size();
					for( int i=1, end=size; i<end; ++i ) {
						ExpressionPrivate * old = expressions[i];
						if( i <= resultSize ) {
							ExpressionPrivate * ve = new _ValueExpression(results[i-1].key());
							expressions[i] = ve;
							ve->ref();
						}
						old->deref();
					}
					for( int i=resultSize+1; i < size; ++i )
						expressions.pop_back();
				}
			}
		}
		
		if( expressions.size() == 2 && expressions[1]->type == Expression::Future_Expression ) {
			_FutureExpression * future = (_FutureExpression*)expressions[1];
			expressions.removeAt(1);
			addRecords(Expression(pc.exp),future->get());
			future->deref();
		}
	}
	return ExpressionPrivate::prepareForExec(pc,conn);
}


ExpressionTask::ExpressionTask( _FutureExpression * fe )
: future( fe )
{
	future->ref();
}

ExpressionTask::~ExpressionTask()
{
	future->deref();
}

void ExpressionTask::run()
{
	future->run();
}

RecordList ExpressionFuture::get()
{
	return p ? ((_FutureExpression*)p)->get() : RecordList();
}

bool ExpressionFuture::valid() const
{
	return p ? ((_FutureExpression*)p)->valid() : false;
}

void ExpressionFuture::wait()
{
	if( p ) ((_FutureExpression*)p)->wait();
}

ExpressionFuture & ExpressionFuture::then(QObject * object, const char * slotName)
{
	if( p ) ((_FutureExpression*)p)->then(object,slotName);
	return *this;
}

TableAlias::TableAlias( Table * table, const QString & alias )
: mTable( table )
, mAlias( alias )
{}
	
Expression TableAlias::operator()(const Expression & e)
{
	if( !e.isValid() ) return e;
	if( e.type() == Expression::Alias_Expression ) {
		((_AliasExpression*)e.p)->tableAlias = mAlias;
		return e;
	}
	_AliasExpression * ae = new _AliasExpression(e.p);
	ae->tableAlias = mAlias;
	return Expression(ae);
}

TableAlias::operator Expression()
{
	return Expression(mTable).alias(mAlias);
}

Expression::Expression( const QVariant & v )
: p( new _ValueExpression( v ) )
{
	p->ref();
}

Expression::Expression( const Record & r )
: p( new _RecordValueExpression( r ) )
{
	p->ref();
}

Expression::Expression( Table * t, bool only )
: p( 0 )
{
	/* 0 ends up in this constructor instead of the QVariant variant :) */
	if( t ) {
		p = new _TableExpression( t, only );
	} else {
		p = new _ValueExpression( QVariant(0) );
	}
	p->ref();
}

Expression & Expression::operator=( const Expression & other )
{
	if( other.p != p ) {
		if( p )
			p->deref();
		p = other.p;
		if( p )
			p->ref();
	}
	return *this;
}

RecordList Expression::operator()(const QList<QVariant> & positionalArgs, const QMap<QString,QVariant> & namedArgs, int lookupMode)
{
	return select(positionalArgs,namedArgs,lookupMode);
}

Expression Expression::operator[](const Expression & e)
{
	return Expression( p && e.p ? new _GetValueExpression(p,e.p) : nullptr );
}

Expression Expression::contains(const Expression & e)
{
	return Expression( p && e.p ? new _ContainsExpression(p,e.p) : nullptr );
}

bool Expression::isConstant() const
{
	return p ? p->isConstant() : true;
}

Expression::Type Expression::type() const
{
	return p ? p->type : Invalid_Expression;
}

#ifdef _HAVE_INIT_LIST
Expression Expression::in( std::initializer_list<Expression> list )
{
	_InExpression * ret = new _InExpression(p);
	for( const Expression * e = list.begin(); e != list.end(); ++e )
		ret->append(e->p);
	return Expression(ret);
}
#endif // _HAVE_INIT_LIST

Expression Expression::cast( QVariant::Type type )
{
	if( !p ) return Expression();
	return Expression(new _CastExpression(p,type));
}

Expression Expression::coalesce( const Expression & exp )
{
	if( !p )
		return exp;
	if( !exp.p )
		return *this;
	return Expression(new _CoalesceExpression(p,exp.p));
}

Expression Expression::isNull()
{
	if( !p ) return Expression();
	return Expression(new _IsNullExpression(Expression::IsNull_Expression,p));
}

Expression Expression::isNotNull()
{
	if( !p ) return Expression();
	return Expression(new _IsNullExpression(Expression::IsNotNull_Expression,p));
}

Expression Expression::is( const Expression & e )
{
	if( !p || !e.p ) return Expression();
	return Expression(new _IsExpression(Expression::Is_Expression,p,e.p));
}

Expression Expression::isNot( const Expression & e )
{
	if( !p || !e.p ) return Expression();
	return Expression(new _IsExpression(Expression::IsNot_Expression,p,e.p));
}

Expression Expression::isDistinctFrom( const Expression & e )
{
	if( !p || !e.p ) return Expression();
	return create(Expression::IsDistinctFrom_Expression,*this,e);
}

Expression Expression::isNotDistinctFrom( const Expression & e )
{
	if( !p || !e.p ) return Expression();
	return create(Expression::IsNotDistinctFrom_Expression,*this,e);
}

static Field * getTableLink( Field * f, TableSchema * ts )
{
	if( f->flag(Field::ForeignKey) ) {
		TableSchema * fkt = f->foreignKeyTable();
		if( fkt->isDescendant(ts) )
			return ts->field(ts->primaryKey());
		foreach( Field * field, ts->fields() ) {
			if( field->flag(Field::ForeignKey) && fkt->isDescendant(field->foreignKeyTable()) ) {
				return field;
			}
		}
	}
	else if( f->flag(Field::PrimaryKey) ) {
		TableSchema * ft = f->table();
		if( ft->isDescendant(ts) )
			return ft->field(ft->primaryKey());
		foreach( Field * field, ts->fields() ) {
			if( field->flag(Field::ForeignKey) && ft->isDescendant(field->foreignKeyTable()) ) {
				return field;
			}
		}
	}
	return 0;
}

static ExpressionPrivate * getRecordLink( const Expression & base, const Record & r, bool nullExprForNullRecord )
{
	// We only support linking from a field expression to a record
	if( base.p && base.p->type == Expression::Field_Expression ) {
		Field * f = ((_FieldExpression*)base.p)->field;
		TableSchema * fs = ((_FieldExpression*)base.p)->table;
		Table * rt = r.table();
		if( !rt ) {
			if( nullExprForNullRecord )
				return new _NullExpression();
			return 0;
		}
		TableSchema * rs = rt->schema();
		if( f->flag(Field::ForeignKey) ) {
			TableSchema * fkt = f->foreignKeyTable();
			if( fkt->isDescendant(rs) )
				return new _ValueExpression(qVariantFromValue<Record>(r));
			foreach( Field * field, rs->fields() ) {
				if( field->flag(Field::ForeignKey) && fkt->isDescendant(field->foreignKeyTable()) ) {
					return new _ValueExpression(r.getValue(field));
				}
			}
		}
		else if( f->flag(Field::PrimaryKey) ) {
			if( fs->isDescendant(r.table()->schema()) )
				return new _ValueExpression(qVariantFromValue<Record>(r));
			foreach( Field * field, rs->fields() ) {
				if( field->flag(Field::ForeignKey) && fs->isDescendant(field->foreignKeyTable()) ) {
					return new _ValueExpression(r.getValue(field));
				}
			}
		}
	}
	return new _ValueExpression(r.key());
}

Expression Expression::in( const ExpressionList & expressions )
{
	if( expressions.size() == 0 ) return Expression();

	_InExpression * ret = new _InExpression(p);
	
	if( p->type == Field_Expression && expressions.size() == 1 ) {
		Field * f = ((_FieldExpression*)p)->field;
		const Expression & e = expressions[0];
		const Expression::Type t = e.p->type;
		if (t != RecordValue_Expression && t != Query_Expression && t != Value_Expression) {
			QList<Table*> tables = findTables(e.p);
			foreach( Table * t, tables ) {
				Field * field = getTableLink(f,t->schema());
				if( field ) {
					ret->append( new _QueryExpression( Expression::createField(field), Expression(t), e.p) );
					return Expression(ret);
				}
			}
		}
	}
	
	ret->expressions.reserve(expressions.size());
	foreach( const Expression & e, expressions ) {
		// Find the link between the tables, and convert to a value expression
		if( e.type() == RecordValue_Expression ) {
			if( p->type != Field_Expression ) continue;
			ret->append(getRecordLink(*this, ((_RecordValueExpression*)e.p)->record,false));
		} else {
			if( e.p->type == Expression::Value_Expression && ((_ValueExpression*)e.p)->v.userType() == qMetaTypeId<RecordList>() )
				ret->addRecords(*this, qvariant_cast<RecordList>(((_ValueExpression*)e.p)->v));
			else
				ret->append(e.p);
		}
	}
	return Expression(ret);
}

Expression Expression::in( const RecordList & records )
{
	_InExpression * ret = new _InExpression(p);
	ret->addRecords(*this,records);
	return Expression(ret);
}

Expression Expression::in( const ExpressionFuture & future )
{
	if( future.p ) {
		_InExpression * ret = new _InExpression(p);
		ret->append(future.p);
		return Expression(ret);
	}
	return Expression();
}

Expression Expression::between( const Expression & left, const Expression & right )
{
	if( !p || !left.p || !right.p ) return Expression();
	return Expression( new _BetweenExpression( p, left.p, right.p ) );
}

Expression Expression::overlaps( const Expression & start1, const Expression & end1, const Expression & start2, const Expression & end2 )
{
	if( !start1.p || !start2.p || !end1.p || !end2.p ) return Expression();
	return Expression( new _OverlapsExpression( Overlaps_Expression, start1.p, end1.p, start2.p, end2.p ) );
}

Expression Expression::overlapsOpen( const Expression & start1, const Expression & end1, const Expression & start2, const Expression & end2 )
{
	if( !start1.p || !start2.p || !end1.p || !end2.p ) return Expression();
	return Expression( new _OverlapsExpression( OverlapsOpen_Expression, start1.p, end1.p, start2.p, end2.p ) );
}

Expression Expression::like( const Expression & e )
{
	return p && e.p ? Expression( new _LikeExpression( p, e.p, true ) ) : Expression();
}

Expression Expression::ilike( const Expression & e )
{
	return p && e.p ? Expression( new _LikeExpression( p, e.p, false ) ) : Expression();
}

Expression Expression::regexSearch( const QString & regex, Qt::CaseSensitivity cs )
{
	return p ? Expression( new _RegexExpression( p, new _ValueExpression(regex), cs ) ) : Expression();
}

Expression Expression::regexSearch( const Expression & regex, Qt::CaseSensitivity cs )
{
	return (p && regex.p) ? Expression( new _RegexExpression( p, regex.p, cs ) ) : Expression();
}

Expression Expression::replace( const Expression & search, const Expression & replacement )
{
	return p && search.p && replacement.p ? Expression( new _ReplaceExpression(p, search.p, replacement.p) ) : Expression();
}

Expression Expression::lower()
{
	return p ? Expression( new _CaseChangeExpression(Expression::Lower_Expression,p) ) : Expression();
}

Expression Expression::upper()
{
	return p ? Expression( new _CaseChangeExpression(Expression::Upper_Expression,p) ) : Expression();
}

Expression Expression::position( const Expression & substring )
{
	return p && substring.p ? Expression( new _StringPositionExpression(p, substring.p) ) : Expression();
}

Expression Expression::length()
{
	return p ? Expression( new _StringLenExpression(p) ) : Expression();
}

Expression Expression::left( const Expression & cnt )
{
	return p && cnt.p ? Expression( new _StringRightLeftExpression(Expression::Left_Expression,p,cnt.p) ) : Expression();
}

Expression Expression::right( const Expression & cnt )
{
	return p && cnt.p ? Expression( new _StringRightLeftExpression(Expression::Right_Expression,p,cnt.p) ) : Expression();
}

Expression Expression::orderBy( const Expression & e, Expression::OrderByDirection dir ) const
{
	return Expression( e.p ? new _OrderByExpression( p, e.p, dir ) : 0 );
}

Expression Expression::groupBy( const Expression & e ) const
{
	return Expression( e.p ? new _GroupByExpression( p, e.p ) : 0 );
}

Expression Expression::limit( int limit, int offset ) const
{
	return Expression( new _LimitExpression( p, limit, offset ) );
}

Expression Expression::contextualAnd( const Expression & other )
{
	if( !p )
		return other;
	if( !other.p )
		return *this;
	QVariant::Type t1 = p->valueType(), t2 = other.p->valueType();
	return create( (t1 == QVariant::Bool || t2 == QVariant::Bool) ? And_Expression : BitAnd_Expression, *this, other );
}

Expression Expression::contextualOr( const Expression & other )
{
	if( !p )
		return other;
	if( !other.p )
		return *this;
	QVariant::Type t1 = p->valueType(), t2 = other.p->valueType();
	return create( (t1 == QVariant::Bool || t2 == QVariant::Bool) ? Or_Expression : BitOr_Expression, *this, other );
}

Expression Expression::alias( const QString & alias ) const
{
	if( !isValid() ) return *this;
	if( type() == Alias_Expression ) {
		((_AliasExpression*)p)->alias = alias;
		return *this;
	}
	return Expression( new _AliasExpression(p, alias) );
}

QString Expression::toString(Expression::StringType stringType,Connection * conn) const
{
	ToStrContext ctx(stringType,conn);
	return p ? p->toString(ctx) : QString();
}

bool Expression::matches( const Record & record ) const
{
	bool ret = p ? p->value(record).toBool() : false;
	//qDebug() << record.debug() << " matches " << toString() << ": " << ret;
	return ret;
}

QVariant Expression::value(const Record & record) const
{
	return p ? p->value(record) : QVariant();
}

QList<Table*> findTables( ExpressionPrivate * p )
{
	QList<Table*> ret;
	if( p->type == Expression::Field_Expression ) {
		Table * t = ((_FieldExpression*)p)->table->table();
		if( !ret.contains(t) )
			ret.append(t);
	}
	foreach( ExpressionPrivate * child, p->children() ) {
		// Don't descend into sub-queries, they don't reflect our return type
		if( child->type == Expression::Query_Expression || child->type == Expression::Future_Expression )
			continue;
		ret += findTables(child);
	}
	return ret;
}

RecordList Expression::select(const QList<QVariant> & positionalArgs, const QMap<QString,QVariant> & namedArgs, int lookupMode, Table ** tableRet, Connection * conn) const
{
	if( !p ) return RecordList();
	_QueryExpression * q = 0;
	QList<Table*> tables;

	
	bool hasNonTableCol = false;
	if( p->type == Expression::Query_Expression )
	{
		q = (_QueryExpression*)p;
		
		// Determine if we are doing a normal table select, a multi-select on an inheritance tree
		// or a select that doesn't fit into any regular TableSchema, instead creating an anonymous
		foreach( Expression e, q->returns ) {
			if( e.type() != Expression::Field_Expression ) {
				hasNonTableCol = true;
			}
		}
		foreach( Expression e, q->fromList ) {
			if( e.type() == Expression::Table_Expression )
				tables.append( ((_TableExpression*)e.p)->table );
		}
	} else if( p->type == Expression::Sql_Expression ) {
		if( !conn ) conn = Database::current()->connection();
		return conn->executeQuery( toString(Expression::QueryString,conn) );
	} else
		tables = findTables(p);
	
	Table * leafTable = 0;
	foreach( Table * t, tables ) {
		if( !leafTable || leafTable->tableTree().contains(t) ) {
			leafTable = t;
			continue;
		}
		// If we have two tables that don't share a common anscestor
		// then we abort since we don't yet support joins
		if( !t->tableTree().contains(leafTable) ) {
			leafTable = 0;
			break;
		}
	}
	
	if( q ) {
		Schema * schema = tables.size() ? tables[0]->schema()->schema() : 0;
		Expression toExec = copy().prepareForExec(conn,positionalArgs,namedArgs);
		if( !conn ) conn = Database::current(schema)->connection();
		RecordList ret = conn->executeQuery( toExec.toString(Expression::QueryString,conn), schema );
		if( ret.isEmpty() ) return ret;
		TableSchema * ts = ret[0].table()->schema();
		FieldList fields = ts->fields();
		assert(fields.size() == q->returns.size());
		for(int i=0; i< fields.size(); ++i) {
			Expression exp = q->returns[i];
			if( exp.type() != Expression::Field_Expression )
				continue;
			Field * anon = fields[i], * orig = ((_FieldExpression*)exp.p)->field;
			if( !orig->foreignKey().isEmpty() ) {
				anon->setForeignKey(orig->foreignKey());
				anon->setFlag(Field::ForeignKey,true);
			} else if( orig->flag( Field::PrimaryKey ) ) {
				anon->setForeignKey( orig->table()->className() );
				anon->setFlag(Field::ForeignKey,true);
			}
		}
		return ret;
	}
	
	if( !leafTable ) {
		QStringList tableNames;
		foreach( Table * t, tables )
			tableNames.append(t->tableName());
		LOG_1( "Unable to determine which table to select from, possible choices are: " + tableNames.join(",") );
		return RecordList();
	}
	if( tableRet )
		*tableRet = leafTable;
	
	ExpressionPrivate * cacheNode = p;
	while( cacheNode->type == SetPlaceHolder_Expression && ((_SetPlaceHolderExpression*)cacheNode)->child )
		cacheNode = ((_SetPlaceHolderExpression*)cacheNode)->child;
	if( cacheNode->type == Cache_Expression )
		return ((_CacheExpression*)cacheNode)->recordsByIndex( leafTable, placeHoldersToPositional(positionalArgs,namedArgs), lookupMode );
	/* else {
		// Try to find a matching caching expression
		quint64 myHash = p->hash();
		std::function<ExpressionIndex*(Table*,ExpressionPrivate*)> findIt = [myHash](Table * table,ExpressionPrivate*self) -> ExpressionIndex * {
			foreach( Index * idx, table->indexes() ) {
				ExpressionIndex * ei = dynamic_cast<ExpressionIndex*>(idx);
				if( !ei || ei->mExpression.p->isMe(self) ) continue;
				if( ei->mExpression.hash() == myHash ) return ei;
			}
			return 0;
		};
		ExpressionIndex * ei = findIt(leafTable,p);
		if( !ei ) {
			foreach( Table * t, leafTable->inherits() ) {
				ei = findIt(t,p);
				if( ei ) break;
			}
		}
		if( ei ) {
			return ei->mExpression.select(positionalArgs,namedArgs,lookupMode,tableRet);
		}
	} */
	return leafTable->select( *this, positionalArgs, namedArgs, true, false, conn );
}

ExpressionFuture Expression::future(const QList<QVariant> & positionalArgs, const QMap<QString,QVariant> & namedArgs, int lookupMode)
{
	if( p )
		return ExpressionFuture( new _FutureExpression( p, positionalArgs, namedArgs, lookupMode ) );
	return ExpressionFuture();
}

Expression Expression::toTempTable( const QString & tempTableName ) const
{
	if( p && p->type == Expression::Query_Expression )
		return Expression(new _TempTableExpression(p,tempTableName));
	return Expression();
}

Expression Expression::createField( Field * field, TableSchema * tableSchema )
{
	if( !field ) return Expression();
	if( !tableSchema ) tableSchema = field->table();
	return Expression( new _FieldExpression(field, tableSchema) );
}

Expression Expression::createValue( const QVariant & value, bool escaped )
{
	return Expression( new _ValueExpression(value,escaped) );
}

Expression Expression::createQuery( const ExpressionList & returns, const ExpressionList & _fromList, const Expression & exp )
{
/*	foreach( Expression e, returns )
		if( e.containsWhereClause() ) {
			qDebug() << "Cannot pass an expression with a where clause as a return field: " << e.toString();
			return Expression();
		} */
	ExpressionList fromList;
	fromList.reserve(std::max(_fromList.size(),2));
	foreach( Expression from, _fromList ) {
		if( from.type() == Alias_Expression )
			from = Expression(((_AliasExpression*)from.p)->child);
		if( from.isValid() && !(from.type() == Query_Expression || from.type() == Table_Expression || from.type() == Values_Expression) ) {
			qDebug() << "The from clause of a query must be another query or a table";
			return Expression();
		}
		if( from.isValid() )
			fromList.append(from);
	}
	if( fromList.isEmpty() ) {
		QList<Table*> tables;
		foreach( Expression e, returns ) {
			foreach( Table * t, findTables(e.p) )
				if( !tables.contains(t) )
					tables.append(t);
		}
		foreach( Table * t, tables )
			fromList.append( Expression(t) );
	}
	return Expression( new _QueryExpression(returns, fromList, exp.p) );
}

Expression Expression::createCombination( Expression::Type type, const ExpressionList & queries )
{
	if( type < Expression::Union_Expression || type > Expression::ExceptAll_Expression ) {
		qDebug() << "Invalid combination type, valid types are Union, UnionAll, Intersect, IntersectAll, Except, and ExceptAll";
		return Expression();
	}
	foreach( Expression e, queries )
		if( !e.isQuery() ) {
			qDebug() << "createCombination only accepts queries";
			return Expression();
		}
	_CombinationExpression * ce = new _CombinationExpression(type);
	foreach( Expression e, queries )
		ce->append(e.p);
	return Expression(ce);
}

Expression Expression::createValues( RecordList records, const QString & alias, FieldList * _fields )
{
	if( records.isEmpty() || alias.isEmpty() ) return Expression();
	FieldList fields;
	if( _fields )
		fields = *_fields;
	else
		fields = records[0].table()->schema()->fields();
	return Expression( new _ValuesExpression( records, alias, fields ) );
}

Expression Expression::createPlaceHolder( const QString & name, const QVariant & defaultValue, bool cacheAll )
{
	return Expression( new _PlaceHolderExpression( name, defaultValue, cacheAll ) );
}

Expression Expression::setPlaceHolder( const QString & name, const QVariant & value )
{
	return Expression( new _SetPlaceHolderExpression( p, name, value ) );
}

Expression Expression::create( Expression::Type type, const Expression & child )
{
	switch( type ) {
		case Not_Expression:
		{
			Expression::Type invertTypes [][2] = {
				{ IsNull_Expression, IsNotNull_Expression },
				{ In_Expression, NotIn_Expression },
				{ Equals_Expression, NEquals_Expression },
				{ Invalid_Expression, Invalid_Expression }
			};
			for( int i = 0; invertTypes[i][0] != Invalid_Expression; ++i ) {
				Expression::Type a = invertTypes[i][0], b = invertTypes[i][1];
				if( child.type() == a ) {
					child.p->type = b;
					return child;
				}
				if( child.type() == b ) {
					child.p->type = a;
					return child;
				}
			}
			return Expression(new _NotExpression( child.p ));
        }
		default:
			break;
	};
	return Expression();
}

Expression Expression::create( Expression::Type type, const Expression & child1, const Expression & child2 )
{
	switch( type ) {
		case Expression::And_Expression:
		{
			if( child1.p && child1.p->type == Expression::And_Expression ) {
				((_AndExpression*)(child1.p))->append(child2.p);
				return child1;
			}
			_AndExpression * ret = new _AndExpression();
			ret->append( child1.p );
			ret->append( child2.p );
			return Expression(ret);
		}
		case Expression::Or_Expression:
		{
			if( child1.p && child1.p->type == Expression::Or_Expression ) {
				((_OrExpression*)(child1.p))->append(child2.p);
				return child1;
			}
			_OrExpression * ret = new _OrExpression();
			ret->append( child1.p );
			ret->append( child2.p );
			return Expression(ret);
		}
		case Expression::Equals_Expression:
		case Expression::NEquals_Expression:
		case Expression::Larger_Expression:
		case Expression::LargerOrEquals_Expression:
		case Expression::Less_Expression:
		case Expression::LessOrEquals_Expression:
		case Expression::BitAnd_Expression:
		case Expression::BitOr_Expression:
		case Expression::BitXor_Expression:
		case Expression::Plus_Expression:
		case Expression::Minus_Expression:
		case Expression::IsDistinctFrom_Expression:
		case Expression::IsNotDistinctFrom_Expression:
		{
			_ComparisonExpression * ret = new _ComparisonExpression( type );
			if( child1.type() == RecordValue_Expression )
				ret->append(getRecordLink(child2,((_RecordValueExpression*)child1.p)->record));
			else
				ret->append( child1.p );
			if( child2.type() == RecordValue_Expression )
				ret->append(getRecordLink(child1,((_RecordValueExpression*)child2.p)->record));
			else
				ret->append( child2.p );
			return Expression(ret);
		}
		default:
			break;
	};
	return Expression();
}

bool Expression::isOnlySupplemental() const
{
	return p ? p->isOnlySupplemental() : true;
}

bool Expression::containsWhereClause() const
{
	return p ? p->containsWhereClause() : false;
}

// Returns true if the expression/query can be executed locally against preloaded values
/*bool Expression::isLocallyExecutable() const
{
	return p ? p->isLocallyExecutable() : true;
}*/

bool Expression::isQuery() const
{
	return p ? p->isQuery() : false;
}

bool Expression::isValidVariantType( QVariant::Type type )
{
	switch( type ) {
		case QVariant::String:
		case QVariant::StringList:
		case QVariant::Time:
		case QVariant::Date:
		case QVariant::DateTime:
		case QVariant::Int:
		case QVariant::UInt:
		case QVariant::Double:
		case QVariant::LongLong:
		case QVariant::ULongLong:
		case QVariant::Bool:
		case QVariant::Char:
			return true;
		default:
			if( type == QVariant::Type(qMetaTypeId<Interval>()) )
				return true;
			return false;
	}
	return false;
}

Expression Expression::copy() const
{
	if( p )
		return Expression(p->copy());
	return Expression();
}

void Expression::save( QDataStream & ds )
{
	if( p )
		p->_save(ds);
}

Expression Expression::load( QDataStream & ds )
{
	int childType;
	ds >> childType;
	ExpressionPrivate * ret = Expression::_create((Expression::Type)childType);
	if( ret ) {
		if( ret->_load(ds) )
			return Expression(ret);
		delete ret;
	}
	return Expression();
}

Expression Expression::prepareForExec( Connection * conn, const QList<QVariant> & positionalArgs, const QMap<QString,QVariant> & namedArgs, bool * reflectsChangeset, bool makeCopy, bool removeCacheAllExpressions, bool * constantNoResults ) const
{
	if( makeCopy ) {
		return copy().prepareForExec(conn,positionalArgs,namedArgs,reflectsChangeset,false,removeCacheAllExpressions, constantNoResults);
	}
	
	if( p ) {
		PrepStruct ps;
		ps.positionalArgs = positionalArgs;
		ps.namedArgs = namedArgs;
		ps.removeCacheAllExpressions = removeCacheAllExpressions;
		PrepContext ctx(&ps);
		Expression ret(p->prepareForExec(ctx,conn));
		if( reflectsChangeset )
			*reflectsChangeset = ps.hasSubQuery;
		if( constantNoResults )
			*constantNoResults = ps.constantNoResults;
		return ret;
	}
	return *this;
}

// SELECT * FROM A WHERE col=val;
// SELECT * FROM (SELECT tableoid, * FROM ONLY A UNION SELECT tableoid, * FROM ONLY B) as iq WHERE iq.col=val;
Expression Expression::transformToInheritedUnion(TableList tables, QList<RecordReturn> & rr)
{
	Expression query;
	if( p && p->type == Query_Expression )
		query = *this;
	else
		query = Query( ExpressionList(), Expression(), *this );
	((_QueryExpression*)query.p)->transformToInheritedUnion(tables,rr);
	return query;
}

Expression Expression::tableOid()
{
	return Expression(new _TableOidExpression());
}

Expression Expression::null( Field::Type castType )
{
	return Expression(new _NullExpression(castType));
}

Expression Expression::sql( const QString & sql )
{
	return Expression(new _SqlExpression(sql));
}

int Expression::placeHolderCount() const
{
	GatherPlaceHolders gph;
	if( p ) p->gatherPlaceHolders(&gph);
	return gph.placeHolders.size() - gph.setters.size();
}

QList<QVariant> Expression::placeHoldersToPositional( const QList<QVariant> & positionalArgs, const QMap<QString,QVariant> & namedArgs ) const
{
	if( !p ) return positionalArgs;
	QList<QVariant> ret;
	GatherPlaceHolders gph;
	p->gatherPlaceHolders(&gph);
	foreach( _SetPlaceHolderExpression * sphe, gph.setters )
		if( !namedArgs.contains(sphe->name) )
			const_cast<QMap<QString,QVariant> &>(namedArgs)[sphe->name] = sphe->value;
	int positionalArgsUsed = 0;
	
	for( int i = 0; i < gph.placeHolders.size(); ++i ) {
		_PlaceHolderExpression * phe = gph.placeHolders[i];
		QVariant arg;
		if( !phe->name.isEmpty() && namedArgs.contains(phe->name) )
			arg = namedArgs[phe->name];
		else if( positionalArgs.size() > positionalArgsUsed )
			arg = positionalArgs[positionalArgsUsed++];
		else
			arg = phe->defaultValue;
		ret.append(phe->prepareForCacheAccess(arg,gph.parents[i]));
	}
	return ret;
}

void Expression::placeHolderInfo( bool & canExtractValues, int & cacheAllCount )
{
	canExtractValues = true;
	cacheAllCount = 0;
	if( !p ) return;
	GatherPlaceHolders gph;
	p->gatherPlaceHolders(&gph);
	canExtractValues = gph.canExtractValues;
	bool invalidCacheAll = false;
	foreach( _PlaceHolderExpression * phe, gph.placeHolders ) {
		if( phe->cacheAll )
			cacheAllCount++;
		else if( cacheAllCount > 0 ) {
			cacheAllCount = 0;
			invalidCacheAll = true;
		}
	}
	if( invalidCacheAll ) {
		for( int i = 0; i < gph.placeHolders.size(); ++i ) {
			_PlaceHolderExpression * phe = gph.placeHolders[i];
			if( phe->cacheAll && gph.placeHolders.size() - i > cacheAllCount ) {
				LOG_1( "Clearing cacheAll flag from placeHolder " + phe->name + ".  It is out of order" );
				phe->cacheAll = false;
			}
		}
	}
}

QList<QVariant> Expression::extractPlaceHolderValues( const Record & record ) const
{
	QList<QVariant> ret;
	if( !p ) return ret;
	GatherPlaceHolders gph;
	p->gatherPlaceHolders(&gph);
	for( int i = 0; i < gph.placeHolders.size(); ++i ) {
		ret.append( gph.placeHolders[i]->extractValue(gph.parents[i],record) );
	}
	return ret;
}

Expression Expression::cache(const Interval & maxAge, bool useParentCache, bool fillParentCache)
{
	if( !p || p->type == Cache_Expression )
		return *this;
	return Expression(new _CacheExpression(p,maxAge,useParentCache,fillParentCache));
}

quint64 Expression::hash() const
{
	return p ? p->hash() : 0;
}

ExpressionList::ExpressionList(const QList<uint> & l)
{ foreach(uint i, l) *this << Expression(i); }

ExpressionList::ExpressionList(const QList<Table*> & tl)
{ foreach( Table * t, tl ) append(Expression(t)); }

ExpressionList::ExpressionList(const FieldList & fl)
{ foreach( Field * f, fl ) append(Expression::createField(f)); }

ExpressionList::ExpressionList(const QStringList & sl)
{ foreach( QString s, sl ) append(Expression::createValue(s)); }

ExpressionList::ExpressionList(const QList<QVariant> & vl)
{ foreach( QVariant v, vl ) append(Expression::createValue(v)); }

ExpressionPrivate * Expression::_create( Expression::Type type )
{
	switch( type ) {
		case Invalid_Expression:
			return 0;
		case Field_Expression:
			return new _FieldExpression();
		case Value_Expression:
			return new _ValueExpression();
		//case RecordValue_Expression:
		//	return new _RecordValueExpression();
		case And_Expression:
			return new _AndExpression();
		case Or_Expression:
			return new _OrExpression();
		case Equals_Expression:
		case NEquals_Expression:
		case IsDistinctFrom_Expression:
		case IsNotDistinctFrom_Expression:
		case Larger_Expression:
		case LargerOrEquals_Expression:
		case Less_Expression:
		case LessOrEquals_Expression:
		case BitAnd_Expression:
		case BitOr_Expression:
		case BitXor_Expression:
		case Plus_Expression:
		case Minus_Expression:
			return new _ComparisonExpression(type);
		case Not_Expression:
			return new _NotExpression();
		case IsNull_Expression:
		case IsNotNull_Expression:
			return new _IsNullExpression(type);
		case In_Expression:
		case NotIn_Expression:
		{
			ExpressionPrivate * ret = new _InExpression();
			ret->type = type;
			return ret;
		}
		case Like_Expression:
		case ILike_Expression:
		{
			ExpressionPrivate * ret = new _LikeExpression();
			ret->type = type;
			return ret;
		}
		case OrderBy_Expression:
			return new _OrderByExpression();
		case RegEx_Expression:
			return new _RegexExpression();
		case Limit_Expression:
			return new _LimitExpression();
		case Query_Expression:
			return new _QueryExpression();
		case Table_Expression:
			return new _TableExpression();
		case Union_Expression:
		case UnionAll_Expression:
		case Intersect_Expression:
		case IntersectAll_Expression:
		case Except_Expression:
		case ExceptAll_Expression:
			return new _CombinationExpression(type);
		case Alias_Expression:
			return new _AliasExpression();
		//case Values_Expression:
		case Null_Expression:
			return new _NullExpression();
		//case TableOid_Expression:
		case Sql_Expression:
			return new _SqlExpression();
		case Cast_Expression:
			return new _CastExpression();
		case PlaceHolder_Expression:
			return new _PlaceHolderExpression();
		case SetPlaceHolder_Expression:
			return new _SetPlaceHolderExpression();
		case Cache_Expression:
			return new _CacheExpression();
		case GroupBy_Expression:
			return new _GroupByExpression();
		case Coalesce_Expression:
			return new _CoalesceExpression();
		case Is_Expression:
		case IsNot_Expression:
			return new _IsExpression(type);
		case Between_Expression:
			return new _BetweenExpression();
		case Overlaps_Expression:
		case OverlapsOpen_Expression:
			return new _OverlapsExpression(type);
		case GetValue_Expression:
			return new _GetValueExpression();
		case Contains_Expression:
			return new _ContainsExpression();
		default: break;
	};
	return 0;
}

} // namespace Stone