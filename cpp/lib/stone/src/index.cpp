
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of libstone.
 *
 * libstone is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * libstone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libstone; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qatomic.h>
#include <qbitarray.h>
#include <qdatetime.h>
#include <qstringlist.h>

#include "blurqt.h"
#include "changeset.h"
#include "changesetchange.h"
#include "database.h"
#include "expressionindex.h"
#include "freezercore.h"
#include "recordimp.h"
#include "index.h"
#include "table.h"
#include "tableschema.h"
#include "record.h"

uint qHash( const QVariant & var )
{
	uint ret = 0;
	switch( var.userType() ) {
		case QVariant::Bool:
			ret = var.toBool() ? 1 : 0;
			break;
		case QVariant::Date:
			ret = qHash( var.toDate().toJulianDay() );
			break;
		case QVariant::DateTime:
			ret = var.toDateTime().toTime_t();
			break;
		case QVariant::Double:
			ret = qHash(int(var.toDouble() * 10000));
			break;
		case QVariant::Int:
			ret = qHash(var.toInt());
			break;
		case QVariant::UInt:
			ret = qHash(var.toUInt());
			break;
		case QVariant::LongLong:
			ret = qHash( var.toLongLong() );
			break;
		case QVariant::ULongLong:
			ret = qHash( var.toULongLong() );
			break;
		case QVariant::Time:
			ret = QTime(0,0).msecsTo(var.toTime()) + 86400000;
			break;
		case QVariant::String: // Drop through
			ret = qHash( var.toString() );
		default:
		{
			if( var.userType() == qMetaTypeId<Record>() ) {
				Record r = qvariant_cast<Record>(var);
				ret = qHash(int(r.key()));
				//if( r.table() )
				//	ret ^= qHash( r.table()->tableName() );
				//LOG_1( r.debug() + " " + QString::number(ret) );
			} else
				ret = qHash( var.toString() );
		}
	}
//        LOG_5( "qHash<QVariant>: returned " + QString::number(ret) );
	return ret;
}

inline uint hash_combine(uint seed, uint hash)
{
    return seed ^ (hash + 0x9e3779b9 + (seed<<6) + (seed>>2));
}

inline uint hash( const QList<QVariant> & vars )
{
	uint seed = 0;
	foreach( const QVariant & v, vars )
		seed = hash_combine(seed,qHash(v));
	return seed;
}

QString qVariantListToString( const QList<QVariant> & vars )
{
	QStringList ret;
	foreach( QVariant v, vars ) {
		ret.append( "[" + QString(v.typeName()) + " " + QString::number(qHash(v)) + "] " + v.toString() );
	}
	return ret.join(",");
}

ArgIter::ArgIter( const QList<QVariant> & args )
: mArgs(args)
, mCurrent(args)
, mSize(1)
, mPos(0)
{
	for( int i=0, end=args.size(); i<end; ++i ) {
		const QVariant & v = args[i];
		int size = 1;
		if( v.type() == QVariant::List ) {
			QList<QVariant> sub = v.toList();
			size = sub.size();
			mCurrent[i] = size == 0 ? QVariant() : sub[0];
		} else if( v.type() == QVariant::StringList ) {
			QStringList sub = v.toStringList();
			size = sub.size();
			mCurrent[i] = size == 0 ? QVariant() : QVariant(sub[0]);
		} else if( int(v.type()) == qMetaTypeId<RecordList>() ) {
			RecordList sub = v.value<RecordList>();
			size = sub.size();
			mCurrent[i] = size == 0 ? QVariant() : qVariantFromValue<RecordList>(sub[0]);
		}
		if( size > 1 ) {
			mSubListIndexes.append(i);
			mSubListSizes.append(size);
			mSize *= size;
		}
	}
}

bool ArgIter::next()
{
	mPos++;
	if( !isValid() )
		return false;
	int subPos = mPos, listIndex = mSubListIndexes.size()-1;
	for( ; listIndex >= 0; --listIndex ) {
		int size = mSubListSizes[listIndex];
		int mod = subPos % size;
		if( mod ) {
			subPos = mod;
			break;
		}
		QVariant sub = mArgs[listIndex];
		if( sub.type() == QVariant::List )
			mCurrent[listIndex] = sub.toList()[0];
		else if( sub.type() == QVariant::StringList )
			mCurrent[listIndex] = sub.toStringList()[0];
		else if( int(sub.type()) == qMetaTypeId<RecordList>() )
			mCurrent[listIndex] = qVariantFromValue<Record>(sub.value<RecordList>()[0]);
		subPos /= size;
	}
	listIndex = mSubListIndexes[listIndex];
	QVariant sub = mArgs[listIndex];
	if( sub.type() == QVariant::List )
		mCurrent[listIndex] = sub.toList()[subPos];
	else if( sub.type() == QVariant::StringList )
		mCurrent[listIndex] = sub.toStringList()[subPos];
	else if( int(sub.type()) == qMetaTypeId<RecordList>() )
		mCurrent[listIndex] = qVariantFromValue<Record>(sub.value<RecordList>()[subPos]);
	return true;
}


namespace Stone {

#ifdef _INDEX_USE_MUTEX_
#define _RLOCKER QMutexLocker rlock(&mMutex);
#define _RLOCKER_O(x) QMutexLocker rlock((x) ? &mMutex : 0);
#define _WLOCKER QMutexLocker wlock(&mMutex);
#define _WLOCKER_O(x) QMutexLocker wlock((x) ? &mMutex : 0);
#endif

#ifdef _INDEX_USE_RWLOCK_
#define _RLOCKER QReadLocker rlock(&mRWLock);
#define _RLOCKER_O(x) QReadLocker rlock((x) ? &mRWLock : 0);
#define _WLOCKER QWriteLocker wlock(&mRWLock);
#define _WLOCKER_O(x) QWriteLocker wlock((x) ? &mRWLock : 0);
#endif

Index::Index( Table * table, Flags flags, int depth, bool useCache )
: mTable( table )
, mFlags( flags )
, mDepth( depth )
, mUseCache( useCache )
, mDebug( false )
{
}

Index::~Index()
{
}


void Index::printStats()
{
	QString s = stats();
	if( !s.isEmpty() )
		LOG_1( "Index Stats (" + description() + "): " + stats() );
}

void Index::setCacheEnabled( bool cacheEnabled )
{
	if( mUseCache && !cacheEnabled ) clear();
	mUseCache = cacheEnabled;
}

void Index::setDebug( bool debug )
{
	mDebug = debug;
}

bool Index::cacheEnabled() const
{
	return mUseCache;
}

bool Index::checkMatch( const Record & /*record*/, const QList<QVariant> & /*args*/, int /*entryCount*/ )
{
	return false;
}

bool Index::checkMatch( const Record & /*record*/, const QVariant & /*args*/)
{
	return false;
}

RecordList Index::applyChangeSetHelper(ChangeSet cs, RecordList records, const QList<QVariant> & args, int entryCount)
{
	if( !cs.isValid() ) return records;
	RecordList before = records;
	Table * ourTable = table();
//	qDebug() << "Modifying index results for changeset " << cs.title() << cs.debug();
	foreach( ChangeSet::Change change, cs.changes() ) {
		if( change.type == ChangeSet::Change_Nested ) {
			ChangeSet child = change.changeSet();
			if( child.state() == ChangeSet::Committed || ChangeSet::current().isAnscestor(child) )
				records = applyChangeSetHelper(child, records, args, entryCount);
			continue;
		}
		foreach( Record r, change.records() ) {
			if( r.table() != ourTable ) continue;
			switch( change.type ) {
				case ChangeSet::Change_Insert:
					if( checkMatch(r,args,entryCount) ) {
						records += r;
					}
					break;
				case ChangeSet::Change_Update:
				{
					bool inList = records.contains(r), matches = checkMatch(r,args,entryCount);
					if( matches && !inList )
						records.append(r);
					else if( !matches && inList )
						records.remove(r);
					break;
				}
				case ChangeSet::Change_Remove:
					records.remove(r);
					break;
				default: break;
			}
		}
	}
	return records;
}

Record Index::applyChangeSetHelper(ChangeSet cs, const Record & record, const QVariant & arg)
{
	if( !cs.isValid() ) return record;
	RecordList records;
	records.append(record);
	Table * ourTable = table();
//	qDebug() << "Modifying index results for changeset " << cs.title() << cs.debug();
	foreach( ChangeSet::Change change, cs.changes() ) {
		if( change.type == ChangeSet::Change_Nested ) {
			ChangeSet child = change.changeSet();
			if( child.state() == ChangeSet::Committed || ChangeSet::current().isAnscestor(child) )
				records = applyChangeSetHelper(child, record, arg);
			continue;
		}
		foreach( Record r, change.records() ) {
			if( r.table() != ourTable ) continue;
			switch( change.type ) {
				case ChangeSet::Change_Insert:
					if( checkMatch(r,arg) ) {
						records += r;
					}
					break;
				case ChangeSet::Change_Update:
				{
					bool inList = records.contains(r), matches = checkMatch(r,arg);
					if( matches && !inList )
						records.append(r);
					else if( !matches && inList )
						records.remove(r);
					break;
				}
				case ChangeSet::Change_Remove:
					records.remove(r);
					break;
				default: break;
			}
		}
	}
	return records.size() ? records[records.size()-1] : Record();
}

RecordList Index::applyChangeSet(RecordList records, const QList<QVariant> & args, int entryCount)
{
	ChangeSet cs = ChangeSet::current();
	while( cs.parent().isValid() )
		cs = cs.parent();
	return applyChangeSetHelper(cs,records,args,entryCount);
}

Record Index::applyChangeSet(const Record & record, const QVariant & arg)
{
	ChangeSet cs = ChangeSet::current();
	while( cs.parent().isValid() )
		cs = cs.parent();
	return applyChangeSetHelper(cs,record,arg);
}

RecordList Index::recordsByIndexMulti( const QList<QVariant> & args, int lookupMode, const Interval & maxAge )
{
	QDateTime now(QDateTime::currentDateTime());
	
	RecordList ret;
	if( !(lookupMode & (UseCache|UseSelect)) ) return ret;
	
	int entrySize = depth();

	if( args.isEmpty() || (args.size() % entrySize) )
		return ret;

	int entryCount = args.size() / entrySize;

	QBitArray needSelect(entryCount,true);
	int needSelectCount = entryCount;
	
	if( mUseCache && (lookupMode & UseCache) ) {
		// Doubtful that this is cheaper than filling the list normally?
		QList<QVariant> entryArgs = QVector<QVariant>(entrySize).toList();
		int status = NoInfo;
		ChangeSet cs = ChangeSet::current();
		
		for( int entry=0; entry < entryCount; entry++ ) {
			for( int a=0; a < entrySize; a++ )
				entryArgs[a] = args[entry*entrySize + a];

			// timestamp is a return value but should be passed in with now
			QDateTime timestamp(now);
			RecordList entryRet = records( entryArgs, status, &timestamp );
			
			if( status == RecordsFound && (maxAge == 0 || Interval(timestamp,now) < maxAge) ) {
				ret += entryRet;
				needSelect[entry] = false;
				needSelectCount--;
			} else if( !(lookupMode & PartialSelect) )
				break;
		}
	}

	if( !(lookupMode & UseSelect) || needSelectCount == 0 )
		return applyChangeSet(ret, args, entryCount);

	QList<QVariant> select_args;
	//select_args.reserve(needSelectCount * entrySize);
	
	for( int entry=0; entry < entryCount; ++entry ) {
		if( (lookupMode & PartialSelect) && !needSelect[entry] ) continue;
		for( int pos=0; pos < entrySize; ++pos )
			select_args.append(args[entry*entrySize + pos]);
	}
	
	mCurrentTimestamp = QDateTime::currentDateTime();
	RecordList selected = select(select_args, lookupMode);
	
	if( lookupMode & PartialSelect )
		ret += selected;
	else
		ret = selected;

	if( mUseCache && canFillFromIncoming() ) {
		_WLOCKER
		for( int entry = 0, end = select_args.size(); entry < end; entry+=entrySize )
			setEmptyEntry( select_args.mid(entry,entrySize) );
		recordsIncoming(selected,true,false);
	}

	return applyChangeSet(ret, args, entryCount);
}

void Index::reserve(int)
{
}

/*static void printArgs(const QList<QVariant> & args)
{
	LOG_1( qVariantListToString(args) );
}*/

RecordList Index::records( const QList<QVariant> &, int &, QDateTime *, bool )
{
	return RecordList();
}

Record Index::record( const QVariant &, int &, QDateTime * )
{
	return Record();
}


Record Index::recordByIndex( const QVariant & arg, int lookupMode, const Interval & maxAge )
{
	Record ret;
	int status = NoInfo;
	
	if( 1 != depth() ) {
		LOG_1( QString("Wrong number of args, got %1, expected %2\n%3").arg(1).arg(depth()).arg(description()) );
		return ret;
	}
	
	if( holdsList() ) {
		LOG_1( "calling single result function on non-unique index\n" + description() );
		return ret;
	}
	
	if( (lookupMode & UseCache) && mUseCache ) {
		bool outofdate=false;
		if(maxAge == 0)
			ret = record( arg, status, 0 );
		else {
			QDateTime timestamp;
			ret = record( arg, status, &timestamp );
			outofdate = status == RecordsFound && Interval(timestamp,QDateTime::currentDateTime()) < maxAge;
		}
		if( status == RecordsFound ) {
			if (!outofdate)
				return applyChangeSet(ret, arg);
			if (lookupMode & UseSelect)
				clearEntries(arg);
		}
	}
	
	if (!(lookupMode & UseSelect))
		return ret;

	QList<QVariant> args;
	args.append(arg);
	RecordList results;
	if( mUseCache ) {
		
		mCurrentTimestamp = QDateTime::currentDateTime();
		results = select(args, lookupMode);
		_WLOCKER
		if( results.size() == 0 )
			setEmptyEntry(arg);
		else
		{
			if( results.size() > 1 )
				reportDuplicate(args,results);
			ret = results[0];
			clearEntries(arg,false);
			recordIncomingNode(arg,ret);
		}
	} else
		results = select(args, lookupMode);
	return applyChangeSet(results.size() ? results[0] : Record(), arg);
}

RecordList Index::recordsByIndex( const QList<QVariant> & args, int lookupMode, const Interval & maxAge )
{
	RecordList ret;
	int status = NoInfo;
	
	if( args.size() != depth() ) {
		LOG_1( QString("Wrong number of args, got %1, expected %2\n%3").arg(args.size()).arg(depth()).arg(description()) );
		return ret;
	}

	//
	// This version doesn't currently support PartialSelect
	//
	
	// printArgs(args);
	
	ArgIter it(args);
	QBitArray needSelectBits(it.size(),true);
	bool needSelect = false;
	
	if( (lookupMode & UseCache) && mUseCache ) {
		QDateTime timestamp;
		QDateTime now(QDateTime::currentDateTime());
		for( ; it.isValid(); it.next() ) {
			// timestamp is a return value but should be passed in with now
			timestamp = now;
			RecordList cur = records( it.current(), status, &timestamp );
			if( status == RecordsFound && (maxAge == 0 || Interval(timestamp,now) < maxAge) ) {
				needSelectBits[it.pos()] = false;
				ret += cur;
			} else {
				needSelect = true;
				break;
			}
		}
	} else
		needSelect = true;

	if( !(lookupMode & UseSelect) || !needSelect )
		return applyChangeSet(ret, args, 1);

	if( mUseCache ) {
		QList<QVariant> selectArgs(args);
		mCurrentTimestamp = QDateTime::currentDateTime();
		RecordList selected = select(selectArgs, lookupMode);
		if( canFillFromIncoming() ) {
			_WLOCKER
			for(it = ArgIter(args); it.isValid(); it.next())
				setEmptyEntry(it.current());
			recordsIncoming(selected,true,false);
		}
		if( (lookupMode & PartialSelect) && ret.size() )
			ret += selected;
		else
			ret = selected;
	} else
		ret = select(args, lookupMode);
	return applyChangeSet(ret, args, 1);
}

void Index::setEntries( const QList<QVariant> & args, RecordList entries )
{
	_WLOCKER
	int d = depth();
	for( int i=0, end = args.size(); i < end; i += d )
		setEmptyEntry(args.mid(i,d));
	recordsIncoming(entries,true,false);
}

Record Index::recordByIndex( const QList<QVariant> & args, int lookupMode, const Interval & maxAge )
{
	if( depth() != 1 )
		return Record( mTable );
	RecordList recs = recordsByIndex( args, lookupMode, maxAge );
	if( recs.size() )
		return recs[0];
	return Record( mTable );
}

void Index::recordsCreated( const RecordList & )
{}

RecordList Index::select( const QList<QVariant> &, int lookupMode )
{
	Q_UNUSED(lookupMode);
	return RecordList();
}

QString Index::description() const
{
	return QString();
}

QString Index::stats() const
{
	return QString();
}

QString Index::argString(const QList<QVariant> & args)
{
	QStringList argStrings;
	foreach( QVariant arg, args )
		argStrings.append(arg.toString());
	return argStrings.join(",");
}

void Index::reportDuplicate(const QList<QVariant> & args, RecordList records)
{
	LOG_1( "Duplicate records found for unique index: " + description() + " args: " + argString(args) + " records: " + records.debug() );
}

void Index::reportDuplicate(const QList<QVariant> & args, const Record & r1, const Record & r2)
{
	reportDuplicate(args, RecordList(r1) + r2);
}

HashIndex::HashIndex( Table * parent, Index::Flags flags, int depth, bool useCache )
: Index( parent, flags, depth, useCache )
, mRoot( depth ? new BranchNode : 0 )
, mHashSize( 0 )
{
}

HashIndex::~HashIndex()
{
	clear();
}

void HashIndex::recordAdded( const Record & r )
{
	if( !mUseCache ) return;
	_WLOCKER
	// Cache all created records if this is a unique index
	recordIncomingNode( extractValues(r), r, /*create=*/!holdsList() );
}

void HashIndex::recordRemoved( const Record & rec )
{
	if( !mUseCache ) return;
	recordRemovedNode( extractValues(rec), rec.imp() );
}

void HashIndex::recordUpdated( const Record & cur, const Record & old )
{
	if( !mUseCache ) return;
	
	//if( mDebug ) LOG_3( qVariantListToString( values(old) ) );
	QList<QVariant> oldVals(extractValues(old)), newVals(extractValues(cur));
	if( hash(oldVals) != hash(newVals) ) {
		_WLOCKER
		recordRemovedNode( oldVals, cur.imp() );
		recordIncomingNode( newVals, cur, /*create=*/false );
	}
}

void HashIndex::recordsIncoming( const RecordList & records, bool ci, bool needLock )
{
	if( !mUseCache || !ci ) return;
	_WLOCKER_O(needLock);
	foreach( Record r, records )
		recordIncomingNode( extractValues(r), r );
}

void HashIndex::setEmptyEntry( const QList<QVariant> & vars )
{
	if( !mUseCache || vars.size() != depth() ) return;
	
	//if( mDebug ) LOG_3( qVariantListToString( vars ) );

	//_WLOCKER
	LeafNode * ln = findLeaf(vars,CreateAlways);
	if( ln ) {
		if( holdsList() ) {
			if( ln->mRecordList )
				ln->mRecordList->clear();
			else
				ln->mRecordList = new RecordList();
		} else {
			if( ln->mRecord && ln->mRecord != (RecordImp*)0x1 )
				ln->mRecord->deref();
			ln->mRecord = (RecordImp*)0x1;
		}
	}
}

void HashIndex::setEmptyEntry( const QVariant & var )
{
	if( !mUseCache ) return;
	
	if( 1 != depth() )
		return;

	//_WLOCKER
	LeafNode * ln = (LeafNode*)((BranchNode*)mRoot)->mValues.value(var);
	if( !ln ) {
		ln = new LeafNode(mCurrentTimestamp);
		((BranchNode*)mRoot)->mValues.insert(var,ln);
	}
	if( holdsList() ) {
		if( ln->mRecordList )
			ln->mRecordList->clear();
		else
			ln->mRecordList = new RecordList();
	} else {
		if( ln->mRecord && ln->mRecord != (RecordImp*)0x1 )
			ln->mRecord->deref();
		ln->mRecord = (RecordImp*)0x1;
	}
}

void HashIndex::setFilledEntry( const QList<QVariant> & vars, bool needLock )
{
	if( !mUseCache ) return;
	if( vars.size() >= depth() )
		return;

	if( mDebug )
		LOG_3( qVariantListToString( vars ) );

	_WLOCKER_O(needLock);
	BranchNode * node = mRoot;
	for( int i = 0, last = vars.size()-1; i <= last; ++i ) {
		QVariant key = vars[i];
		if( key.userType() == qMetaTypeId<Record>() )
			key = QVariant( int(qvariant_cast<Record>(key).key()) );
		void * next = node->mValues.value(key);
		if( !next ) {
			next = (void*)(new BranchNode());
			node->mValues.insert( key, next );
		}
		node = (BranchNode*)next;
	}
	node->mFlags |= BranchNode::Filled;
	node->mFilledTimestamp = mCurrentTimestamp;
}

void HashIndex::clearEntries( const QList<QVariant> & vars, bool needLock )
{
	if( !mUseCache ) return;
	if( vars.size() > depth() )
		return;
	
	if( mDebug )
		LOG_3( qVariantListToString( vars ) );

	LeafNode * ln = 0;
	_WLOCKER_O(needLock);
	if( depth() == 0 ) {
		ln = (LeafNode*)mRoot;
		mRoot = 0;
	} else {
		BranchNode * node = mRoot;
		for( int i = 0, last = vars.size()-1; i <= last; ++i ) {
			QVariant key = vars[i];
			if( key.userType() == qMetaTypeId<Record>() )
				key = QVariant( int(qvariant_cast<Record>(key).key()) );
			void * next = node->mValues.value(key);
			if( !next ) {
				node = 0;
				break;
			}
			if( i == last ) {
				node->mValues.remove(key);
				if( i == depth() - 1 ) {
					ln = (LeafNode*)next;
				} else
					clearNode( (BranchNode*)next, depth() - vars.size() );
			}
		}
	}
	if( ln ) {
		if( holdsList() )
			delete ln->mRecordList;
		else if( ln->mRecord != (RecordImp*)0x1 )
			ln->mRecord->deref();
		delete ln;
	}
}

void HashIndex::clearEntries( const QVariant & var, bool needLock )
{
	if( !mUseCache ) return;
	if( 1 != depth() )
		return;
	
	_WLOCKER_O(needLock);
	BranchNode * node = mRoot;
	LeafNode * ln = (LeafNode*)node->mValues.value(var);
	node->mValues.remove(var);
	if( ln ) {
		if( holdsList() )
			delete ln->mRecordList;
		else if( ln->mRecord != (RecordImp*)0x1 )
			ln->mRecord->deref();
		delete ln;
	}
}

static bool isChangesetRecord( const Record & r )
{
	ChangeSet cs = r.changeSet();
	if( cs.isValid() ) {
		ChangeSetEnabler cse(cs);
		return r.isRecord();
	}
	return false;
}

RecordList HashIndex::records( const QList<QVariant> & vars, int & status, QDateTime * timestamp, bool needLock )
{
	status = NoInfo;
	
	if( vars.size() != depth() ) {
		LOG_1( QString("Got %1 args, expected %2\n%3").arg(vars.size()).arg(depth()).arg(description()) );
		return RecordList();
	}
	
	if( !mUseCache )
		return RecordList();

	if( canFillFromIncoming() ) {
		mTable->preload();
	}

	RecordList ret;
	_RLOCKER_O(needLock);
	LeafNode * ln = 0;
	if( depth() == 0 ) {
		ln = (LeafNode*)mRoot;
	} else {
		BranchNode * node = mRoot;
		for( int i = 0, last = depth()-1; i <= last; ++i ) {
			if( node->mFlags & BranchNode::Filled ) {
				status = RecordsFound;
				if( timestamp )
					*timestamp = node->mFilledTimestamp;
			}

			QVariant key = vars[i];
			if( key.userType() == qMetaTypeId<Record>() ) {
				Record r = qvariant_cast<Record>(key);
				int ikey = r.key();
				/* If we have a record committed into a changeset it's only children
				* will be found during changeset processing, not in the hash itself.
				* We don't want to perform a select for this so we treat it as being found */
				if( ikey == 0 && isChangesetRecord(r) ) {
					status = RecordsFound;
					// Leave timestamp as now
					return RecordList();
				}
				key = QVariant(ikey);
			}
			void * next = node->mValues.value(key);
			if( !next )
				break;
			if( i == last ) {
				ln = (LeafNode*)next;
				break;
			} else
				node = (BranchNode*)next;
		}
	}
	if( ln ) {
		status = RecordsFound;
		if( holdsList() )
			ret = *(ln->mRecordList);
		else if( ln->mRecord && ln->mRecord != (RecordImp*)0x1 )
			ret = RecordList( Record( ln->mRecord ) );
		if( timestamp )
			*timestamp = ln->mTimestamp;
	}

	if( mDebug ) {
		if( status == RecordsFound ) {
			LOG_3( "Found entries for args: " + qVariantListToString( vars ) );
			LOG_3( ret.debug() );
		} else
			LOG_3( "No entries found for: " + qVariantListToString( vars ) );
	}

	return ret;
}

Record HashIndex::record( const QVariant & var, int & status, QDateTime * timestamp )
{
	Record ret;
	status = NoInfo;
	
	if( 1 != depth() ) {
		LOG_1( QString("Got %1 args, expected %2\n%3").arg(1).arg(depth()).arg(description()) );
		return ret;
	}

	if( holdsList() ) {
		LOG_1( "Calling HashIndex::record on an index that holds lists\n" + description() );
		return ret;
	}
	
	if( !mUseCache )
		return ret;

	if( canFillFromIncoming() )
		mTable->preload();

	QVariant key = var;
	if( key.userType() == qMetaTypeId<Record>() ) {
		Record r = qvariant_cast<Record>(key);
		int ikey = r.key();
		/* If we have a record committed into a changeset it's only children
		 * will be found during changeset processing, not in the hash itself
		 * We don't want to perform a select for this so we treat it as being found */
		if( ikey == 0 && r.isRecord() ) {
			status = RecordsFound;
			return Record();
		}
		key = QVariant(ikey);
	}
	_RLOCKER
	BranchNode * node = mRoot;
	LeafNode * ln = (LeafNode*)node->mValues.value(key);
	if( ln ) {
		status = RecordsFound;
		if( ln->mRecord && ln->mRecord != (RecordImp*)0x1 )
			ret = Record(ln->mRecord);
		if( timestamp )
			*timestamp = ln->mTimestamp;
	} else if( node->mFlags & BranchNode::Filled ) {
		status = RecordsFound;
		if( timestamp )
			*timestamp = node->mFilledTimestamp;
	}
	
	if( mDebug ) {
		if( status == RecordsFound ) {
			LOG_3( "Found entries for args: " + var.toString() );
			LOG_3( ret.debug() );
		} else
			LOG_3( "No entries found for: " + var.toString() );
	}

	return ret;
}

// caller must hold a write lock
int HashIndex::recordRemovedNode( const QList<QVariant> & values, RecordImp * pointer )
{
	if( values.size() > depth() ) return 0;
	
	LeafNode * ln = findLeaf(values,CreateNever);
	if( ln ) {
		if( holdsList() )
			return ln->mRecordList->remove(pointer);
		else if( ln->mRecord != (RecordImp*)0x1 ) {
			ln->mRecord->deref();
			ln->mRecord = (RecordImp*)0x1;
			return 1;
		}
	}
	return 0;
}

// caller must hold a read/write lock, depending on create arg
HashIndex::LeafNode * HashIndex::findLeaf(const QList<QVariant> & values, int findNodeFlags)
{
	return (LeafNode*)findNode(values,findNodeFlags);
}

HashIndex::BranchNode * HashIndex::findBranch(const QList<QVariant> & values, int findNodeFlags)
{
	return (BranchNode*)findNode(values,findNodeFlags);
}

// caller must hold a read/write lock, depending on create arg
// caller must ensure values.size() <= depth()
void * HashIndex::findNode(const QList<QVariant> & values, int findNodeFlags)
{
	bool create = findNodeFlags == CreateAlways;
	if( depth() == 0 ) {
		if( !mRoot && create )
			mRoot = (BranchNode*)(new LeafNode(mCurrentTimestamp));
		return mRoot;
	} else {
		BranchNode * node = mRoot;
		for( int i = 0, last = values.size()-1; i <= last; ++i ) {
			QVariant key = values[i];
			if( key.userType() == qMetaTypeId<Record>() )
				key = QVariant( int(qvariant_cast<Record>(key).key()) );
			if( node->mFlags & BranchNode::Filled )
				create = create || (findNodeFlags == CreateIfFilled);
			void * next = node->mValues.value(key);
			if( !next ) {
				if( !create ) break;
				if( i == depth()-1 )
					next = new LeafNode(mCurrentTimestamp);
				else
					next = new BranchNode();
				node->mValues.insert(key,next);
			}
			if( i == last )
				return next;
			node = (BranchNode*)next;
		}
	}
	return nullptr;
}

// caller must hold a read/write lock, depending on create arg
HashIndex::LeafNode * HashIndex::findLeaf(const QVariant & value, int findNodeFlags)
{
	BranchNode * node = mRoot;
	void * next = node->mValues.value(value);
	if( !next && (findNodeFlags == CreateAlways || (node->mFlags & BranchNode::Filled && findNodeFlags == CreateIfFilled)) ) {
		next = (void*)(new LeafNode(mCurrentTimestamp));
		node->mValues.insert(value,next);
	}
	return (LeafNode*)next;
}

// Requires caller to have a write lock!
// returns false if duplicate unique!
bool HashIndex::recordIncomingNode( const QList<QVariant> & values, const Record & record, bool create )
{
	LeafNode * ln = findLeaf(values,create ? CreateAlways : CreateIfFilled);
	if( ln ) {
		if( holdsList() ) {
			if( ln->mRecordList )
				ln->mRecordList->impMerge(record.imp());
			else
				ln->mRecordList = new RecordList(record);
		} else {
			if(ln->mRecord > (RecordImp*)0x1) {
				if( ln->mRecord != record.imp() ) {
					reportDuplicate(values,ln->mRecord,record);
					return false;
				}
			} else {
				ln->mRecord = record.imp();
				ln->mRecord->ref();
			}
		}
	}
	
	if( mDebug ) {
		LOG_3( "Cached record for args: " + qVariantListToString( values ) );
		LOG_3( record.debug() );
	}

	return true;
}

// Requires caller to have a write lock!
bool HashIndex::recordIncomingNode( const QList<QVariant> & values, const RecordList & records, bool create )
{
	LeafNode * ln = findLeaf(values,create ? CreateAlways : CreateIfFilled);
	if( ln ) {
		if( holdsList() ) {
			if( !ln->mRecordList ) {
				ln->mRecordList = new RecordList(records);
				ln->mRecordList->impSort();
			} else {
				foreach( Record r, records )
					ln->mRecordList->impMerge(r.imp());
			}
		} else {
			if( !ln->mRecord || ln->mRecord == (RecordImp*)0x1 ) {
				ln->mRecord = records[0].imp();
				ln->mRecord->ref();
			} else if( ln->mRecord != records[0].imp() ) {
				reportDuplicate(values,ln->mRecord, records[0]);
				return false;
			}
		}
	}
	if( mDebug ) {
		LOG_3( "Cached records for args: " + qVariantListToString( values ) );
		LOG_3( records.debug() );
	}
	return true;
}

// Requires caller to have a write lock!
bool HashIndex::recordIncomingNode( const QVariant & value, const Record & record, bool create )
{
	LeafNode * ln = findLeaf(value,create ? CreateAlways : CreateIfFilled);
	assert(!holdsList());
	if( ln ) {
		if( !ln->mRecord || ln->mRecord == (RecordImp*)0x1 ) {
			ln->mRecord = record.imp();
			ln->mRecord->ref();
		} else if( ln->mRecord != record.imp() ) {
			reportDuplicate(QList<QVariant>() << value, ln->mRecord, record);
			return false;
		}
	}
	if( mDebug ) {
		LOG_3( "Cached record for arg: " + value.toString() );
		LOG_3( record.debug() );
	}
	return true;
}

void HashIndex::clear()
{
	_WLOCKER;
	if( mDebug )
		LOG_3( "All entries cleared" );
	
	if( depth() == 0 ) {
		LeafNode * ln = (LeafNode*)mRoot;
		if( ln ) {
			if( holdsList() && ln->mRecordList )
				delete ln->mRecordList;
			else if( ln->mRecord && ln->mRecord != (RecordImp*)0x1 )
				ln->mRecord->deref();
			delete ln;
			mRoot = 0;
		}
	} else {
		clearNode(mRoot,depth());
		mRoot->mFilledTimestamp = QDateTime();
		mRoot->mFlags = 0;
	}
}

void HashIndex::clearNode( BranchNode * node, int depthToGo )
{
	depthToGo--;
	bool isList = holdsList();
	QHashIterator<QVariant,void*> it(node->mValues);
	while(it.hasNext()) {
		it.next();
		void * next = it.value();
		if( depthToGo > 0 ) {
			clearNode( (BranchNode*)next, depthToGo );
			delete (BranchNode*)next;
		} else {
			LeafNode * ln = (LeafNode*)next;
			if( isList )
				delete ln->mRecordList;
			else if( ln->mRecord != (RecordImp*)0x1 )
				ln->mRecord->deref();
			delete ln;
		}
	}
	node->mValues.clear();
}

void HashIndex::reserve(int space)
{
	if( depth() )
		mRoot->mValues.reserve(mRoot->mValues.size() + space);
}

void HashIndex::collectStats( LeafNode * node, Stats & s ) const
{
	if( node ) {
		s.leafNodes++;
		s.records += holdsList() ? node->mRecordList->size() : (node->mRecord > (RecordImp*)0x1 ? 1 : 0);
	}
}

void HashIndex::collectStats( BranchNode * node, int depthToGo, Stats & s ) const
{
	depthToGo--;
	QHashIterator<QVariant,void*> it(node->mValues);
	while(it.hasNext()) {
		it.next();
		void * next = it.value();
		if( depthToGo > 0 )
			collectStats( (BranchNode*)next, depthToGo, s );
		else
			collectStats( (LeafNode*)next, s );
	}
}

QString HashIndex::stats() const
{
	Stats s;
	if( depth() == 0 )
		collectStats( (LeafNode*)mRoot, s );
	else
		collectStats( (BranchNode*)mRoot, depth(), s );
	if( s.leafNodes + s.records <= 1 )
		return QString();
	return QString("Branches: %1  Leafs: %2  Records: %3").arg(s.branchNodes).arg(s.leafNodes).arg(s.records);
}

SimpleIndex::SimpleIndex( Table * parent, IndexSchema * schema )
: HashIndex( parent, Index::Flags((schema->holdsList() ? Index::HoldsListFlag : Index::Flags()) | Index::FillFromIncomingFlag), schema->columns().size(), schema ? schema->useCache() : true )
, mSchema( schema )
{}

SimpleIndex::~SimpleIndex()
{}

bool SimpleIndex::checkMatch( const Record & record, const QList<QVariant> & args, int entryCount )
{
	FieldList fl = mSchema->columns();
	int entrySize = fl.size();
	for( int entry = 0; entry < entryCount; entry++ ) {
		for( ArgIter it(args.mid(entry * entrySize,entrySize)); it.isValid(); it.next() ) {
			bool matches = true;
			QList<QVariant> it_args = it.current();
			for( int i = 0; i < entrySize; i++ ) {
				if( record.getValue(fl.at(i)) != it_args.at(i) ) {
					matches = false;
					break;
				}
			}
			if( matches )
				return true;
		}
	}
	return false;
}

bool SimpleIndex::checkMatch( const Record & record, const QVariant & arg )
{
	FieldList fl = mSchema->columns();
	return record.getValue(mSchema->columns().at(0)) == arg;
}

QList<QVariant> SimpleIndex::extractValues( const Record & record ) const
{
	QList<QVariant> ret;
	FieldList cols = mSchema->columns();
	ret.reserve(cols.size());
	foreach( Field * f, cols )
		ret.append(record.getValue(f));
	return ret;
}

void SimpleIndex::recordsIncoming( const RecordList & rl, bool cacheIncoming, bool needLock )
{
	/*
	 * If cacheIncoming is set we always put the value into the cache
	 * because cacheIncoming indicates the select has no where clause
	 * so all values are available.
	 *
	 * If we aren't holding a list that means the index is holding a
	 * unique value, so we can cache any incoming value from any query.
	 *
	 * Otherwise only access to this index can be trusted to return
	 * all records for each set of indexed fields.  In this case
	 * recordsIncoming is call explicitly from the record[s]ByIndex[Multi]
	 * functions when the index is accessed.
	 */
	HashIndex::recordsIncoming(rl, !mSchema->holdsList() || cacheIncoming, needLock);
}

/* Foreign key arguments that are records that have been committed to a changeset
 * but not to the database should never result in a select, otherwise it will
 * select all records with a null fkey.  We check for this condition here. */
static bool shouldSkipSelect( const QVariant & arg )
{
	return (arg.userType() == qMetaTypeId<Record>()) && isChangesetRecord(qvariant_cast<Record>(arg));
}

/* This could probably be configurable, but we always consider foreign keys to
 * either be valid or null, not 0. This helper function checks whether we should
 * be checking for null if the Field is a foreign key */
static bool isNullFkey( const QVariant & arg )
{
	if( arg.isNull() ) return true;
	if( arg.canConvert(QVariant::Int) && arg.toInt() == 0 ) return true;
	if( arg.userType() == qMetaTypeId<Record>() && !qvariant_cast<Record>(arg).isRecord() ) return true;
	return false;
}

RecordList SimpleIndex::select( const QList<QVariant> & args, int lookupMode )
{
	int entrySize = mSchema->columns().size();

	if( args.isEmpty() || (args.size() % entrySize) )
		return RecordList();

	const bool ignoreNullFkeys = lookupMode & IgnoreNullFkeys;
	int entryCount = args.size() / entrySize;
	Expression exp;

	// printArgs( args );
	
	if( entrySize == 1 && mSchema->columns()[0]->flag( Field::ForeignKey ) ) {
		bool hasNull = false;
		Field * f = mSchema->columns()[0];
		QList<QVariant> inArgs;
		for( int entry = 0; entry < entryCount; entry++ ) {
			QVariant arg = args[entry];
			
			const bool nullFkey = isNullFkey(arg);
			// No select for changeset comitted records
			if( (nullFkey && ignoreNullFkeys) || shouldSkipSelect(arg) )
				continue;
			
			if( nullFkey )
				hasNull = true;
			else
				inArgs += args[entry];
		}
		if( inArgs.size() )
			exp = Expression::createField(f).in(inArgs);
		if( hasNull )
			exp |= Expression::createField(f).isNull();
	} else {
		for( int entry = 0; entry < entryCount; entry++ ) {
			Expression subExp;
			int column = 0;
			foreach( Field * f, mSchema->columns() ) {
				QVariant arg = args[entry * entrySize + column];
				
				/* Since this is a multi-value index, we need to skip then entire
				 * sub-select if any arg should not be selected (due to changeset)
				 * committed records */
				if( shouldSkipSelect(arg) ) {
					subExp = Expression();
					break;
				}
				
				// Should we really be converting 0 to null here?  I don't think we do that
				// anywhere else
				if( (f->flag( Field::ForeignKey ) && isNullFkey(arg)) || arg.isNull() ) {
					//if( !arg.isNull() ) LOG_5( "Converting value to null!" );
					subExp &= Expression::createField(f).isNull();
				} else {
					Expression e = Expression::createField(f);
					if( arg.userType() == qMetaTypeId<RecordList>() )
						e = e.in(qvariant_cast<RecordList>(arg));
					else if( arg.type() == QVariant::StringList )
						e = e.in(arg.toStringList());
					else if( arg.type() == QVariant::List )
						e = e.in(arg.toList());
					else
						e = e == arg;
					subExp &= e;
				}
				column++;
			}
			if( subExp.isValid() )
				exp |= subExp;
		}
	}
	if( exp.isValid() )
		return mTable->select(exp);
	return RecordList();
}

QString SimpleIndex::description() const
{
	QStringList fieldNames;
	foreach( Field * field, mSchema->columns() )
		fieldNames.append(field->name());
	return "SimpleIndex " + mSchema->name() + " on " + mSchema->table()->tableName() + " (" + fieldNames.join(",") + ") " + (mSchema->holdsList() ? "not unique" : "unique");
}

ExpressionIndex::ExpressionIndex( Table * parent, const Expression & expression )
: HashIndex( parent, HoldsListFlag, expression.placeHolderCount(), true )
, mExpression( expression )
, mNeedIncomingFilter(true)
{
	bool canExtractValues;
	mExpression.placeHolderInfo( canExtractValues, mCacheAllCount );
	if( canExtractValues )
		mFlags = Index::Flags(mFlags | FillFromIncomingFlag);
	mTable->addIndex(this);
}

ExpressionIndex::~ExpressionIndex()
{
	mTable->removeIndex(this,true);
}

/*
QList<QVariant> ExpressionIndex::toCacheOrder( const QList<QVariant> & values ) const
{
	QList<QVariant> ret;
	if( values.size() != mCacheOrder.size() ) {
		LOG_1( "Incorrect number of values" );
		return ret;
	}
	foreach( int i, mCacheOrder ) {
		ret.append(values[i]);
	}
	return ret;
}*/

RecordList ExpressionIndex::selectHelper( const QList<QVariant> & args )
{
	RecordList ret;
	if( mCacheAllCount ) {
		QList<QVariant> partialArgs = args.mid(0,args.size()-mCacheAllCount);
		if( !mCacheExpression.isValid() )
			mCacheExpression = mExpression.prepareForExec(Database::current()->connection(), args, QMap<QString,QVariant>(), 0, true, true);
		_WLOCKER
		for( ArgIter it(partialArgs); it.isValid(); it.next() )
			clearEntries(it.current(),false);
		mTable->select(mCacheExpression,args.mid(mCacheAllCount));
		int status;
		ret.clear();
		for( ArgIter it(args); it.isValid(); it.next() )
			ret += records(it.current(),status,0,/*needLock=*/false);
		for( ArgIter it(partialArgs); it.isValid(); it.next() )
			setFilledEntry(it.current(),/*needLock=*/false);
	} else
		ret = mExpression(args);
	return ret;
}

RecordList ExpressionIndex::select( const QList<QVariant> & args, int lookupMode )
{
	Q_UNUSED(lookupMode);
	RecordList ret;
	mNeedIncomingFilter = false;
	if( mFlags & FillFromIncomingFlag ) {
		ret = selectHelper( args );
	} else {
		// If we can't fill from incoming then we need to know the exact args that each
		// record is associated with, so we can't pass a recordlist/qvariantlist to any
		// of the placeholders, so we iterate through them here
		for( ArgIter it(args); it.isValid(); it.next() ) {
			ret = selectHelper(it.current());
			_WLOCKER
			clearEntries( it.current(),false );
			recordIncomingNode( it.current(), ret );
		}
	}
	mNeedIncomingFilter = true;
	return ret;
}

QList<QVariant> ExpressionIndex::extractValues( const Record & record ) const
{
	return /* toCacheOrder( */ mExpression.extractPlaceHolderValues(record); //);
}

void ExpressionIndex::recordAdded( const Record & r )
{
	if( (mFlags & FillFromIncomingFlag) )
		HashIndex::recordAdded(r);
	else
		clear();
}

void ExpressionIndex::recordRemoved( const Record & r )
{
	if( (mFlags & FillFromIncomingFlag) )
		HashIndex::recordRemoved(r);
	// We check to make sure records aren't deleted before returning
	// them from the index, so no need to remove deleted records
}

void ExpressionIndex::recordUpdated( const Record & r, const Record & old )
{
	if( (mFlags & FillFromIncomingFlag) ) {
		QList<QVariant> vals = extractValues(r), oldVals = extractValues(old);
		bool newMatchesExp = checkMatch(r,vals,1);
		bool valsChanged = vals != oldVals;
		_WLOCKER
		if( !newMatchesExp || valsChanged )
			recordRemovedNode( vals, r.imp() );
		if( newMatchesExp )
			recordIncomingNode( vals, r, !holdsList() );
	} else
		clear();
}

void ExpressionIndex::recordsIncoming( const RecordList & rl, bool ci, bool needLock )
{
	if( (mFlags & FillFromIncomingFlag) && ci ) {
		if( mNeedIncomingFilter ) {
			RecordList tmp;
			foreach( Record r, rl ) {
				if( checkMatch(r,extractValues(r),1) )
					tmp.append(r);
			}
			HashIndex::recordsIncoming( tmp, ci, needLock );
		} else
			HashIndex::recordsIncoming( rl, ci, needLock );
	}
}

bool ExpressionIndex::checkMatch( const Record & record, const QList<QVariant> & args, int entryCount )
{
	// This should probably be optimized at some point by an api change.  Right now for
	// every record that needs to be checked we are preparing the expression, when that could
	// be done a single time.
	int d = depth();
	for( int i=0; i<entryCount; ++i ) {
		if( mExpression.prepareForExec(Database::current()->connection(), args.mid(i*d,d)).matches(record) )
			return true;
	}
	return false;
}

/* The KeyIndex class only holds a reference to the records if
 * preloading is enabled( all records from table are loaded, and stay loaded), or
 * if expireKeyCache is false, so any previously loaded records stay loaded.
 */
KeyIndex::KeyIndex( Table * table, IndexSchema * )
: Index( table, Index::Flags(), 1 )
, mPrimaryKeyColumn( table->schema()->primaryKeyIndex() )
{}

KeyIndex::~KeyIndex()
{
	clear();
}

void KeyIndex::recordsCreated( const RecordList & records )
{
	// Return if nothing to cache
	if( !records.size() )
		return;

	//LOG_5( "Caching Incoming records in keyindex for table: " + mTable->tableName() + " index id: " + QString::number( (int)this ) );
	//TableSchema * schema = mTable->schema();
	_WLOCKER
	st_foreach( RecordIter, rec, records ){
		RecordImp * imp = rec.imp();
		uint key = imp->key();
		//LOG_5( "Caching key: " + QString::number( key ) );
		if( key && imp && imp->table() == mTable ){
			RecordImp * ri = mDict.value( key );
			if( !ri || ri == (RecordImp*)1 || ri->refCount() == 0 )
				mDict.insert( key, imp );
		}
	}

}

void KeyIndex::recordAdded( const Record & r )
{
	if( r.table() != mTable ) return;
	uint key = r.key();
	TableSchema * schema = mTable->schema();
	bool needRef = schema->isPreloadEnabled() || !schema->expireKeyCache();
	_WLOCKER
	RecordImp * fin = mDict.value( key );
	if( !fin || fin == (RecordImp*)1 ) {
		//qDebug() << "Key " << key << " added to keyindex";
		fin = r.imp();
		mDict.insert( key, fin );
	}
	// If fin is already in the dict, it was added by recordsCreated and does not have a reference
	// so we ref the imp here always
	if( needRef )
		fin->ref();
}

void KeyIndex::recordRemoved( const Record & r )
{
	uint key = r.key();
	TableSchema * schema = mTable->schema();
	bool needRef = schema->isPreloadEnabled() || !schema->expireKeyCache();
	_WLOCKER
	RecordImp * fin = mDict.value( key );
	if( fin && fin != (RecordImp*)1 ) {
		//qDebug() << "Key " << key << " removed from keyindex";
		mDict.insert( key, (RecordImp*)1 );
		if( needRef )
			fin->deref();
	}
}

void KeyIndex::recordUpdated( const Record&, const Record& )
{
}

void KeyIndex::recordsIncoming( const RecordList & records, bool, bool )
{
	// Return if nothing to cache
	if( !records.size() )
		return;

	//LOG_5( "Caching Incoming records in keyindex for table: " + mTable->tableName() + " index id: " + QString::number( (int)this ) );
	TableSchema * schema = mTable->schema();
	bool needRef = schema->isPreloadEnabled() || !schema->expireKeyCache();
	_WLOCKER
	if( mDict.size() < int(records.size()) )
		mDict.reserve(records.size());
	st_foreach( RecordIter, rec, records ){
		RecordImp * imp = rec.imp();
		if( imp && imp->table() == mTable ){
			uint key = imp->key();
			//LOG_5( "Caching key: " + QString::number( key ) );
			RecordImp * ri = mDict.value( key );
			if( !ri || ri == (RecordImp*)1 || ri->refCount() == 0 ) {
				mDict.insert( key, imp );
				if( needRef )
					imp->ref();
			}
		}
	}
}

Record KeyIndex::record( uint key, bool * found )
{
	//qDebug() << "Checking " << mTable->tableName() << " KeyIndex for record: " << key << " index id: " << this;

	_RLOCKER
	RecordImp * ri = mDict.value( key );

	Record ret;
	// Not found
	if( ri ) {
		// Past here we either found a valid record, or found an "empty entry"(record does not exist in db)
		if( found )
			*found = true;

		if( ri != (RecordImp*)1 )
			return Record(ri,false);
	}
	//qDebug() << mTable->tableName() << " KeyIndex::record returning " << ri << " with count " << (ri ? ri->refCount() : 0);
	return ret;
}

RecordList KeyIndex::values()
{
	RecordList ret;
	ret.reserve(mDict.size());
	for( QHash<uint, RecordImp*>::const_iterator it = mDict.begin(); it != mDict.end(); ++it )
		if( it.value() != (RecordImp*)1 ) {
			Record r(it.value());
			// Will check if the record is deleted in the current changeset
			if( r.isRecord() )
				ret += it.value();
		}
	ChangeSet cs = ChangeSet::current();
	if( cs.isValid() ) {
		RecordList added, removed;
		cs.visibleRecordsChanged( &added, 0, 0, QList<Table*>()<<mTable );
		ret += added;
	}
	return ret;
}

RecordList KeyIndex::records( const QList<QVariant> &, int & status, QDateTime *, bool needLock )
{
	Q_UNUSED(needLock);
	status = NoInfo;
	return RecordList();
}

Record KeyIndex::record( const QVariant &, int & status, QDateTime * )
{
	status = NoInfo;
	return Record();
}

void KeyIndex::setEmptyEntry( const QList<QVariant> & vars )
{
	_WLOCKER
	if( vars.size() == 1 ) {
		//qDebug() << mTable->tableName() << " KeyIndex::record setting empty entry for record " << vars[0].toUInt();
		mDict.insert( vars[0].toUInt(),(RecordImp*)1 );
	}
}

void KeyIndex::setEmptyEntry( const QVariant & var )
{
	_WLOCKER
	//qDebug() << mTable->tableName() << " KeyIndex::record setting empty entry for record " << vars[0].toUInt();
	mDict.insert( var.toUInt(),(RecordImp*)1 );
}

void KeyIndex::clear()
{
	RecordList tmp;
	{
		_WLOCKER
		for( QHash<uint, RecordImp*>::const_iterator it = mDict.begin(); it != mDict.end(); ++it )
			if( it.value() != (RecordImp*)1 ) {
				// Put it into a tmp list to deref later, to avoid mutex contention in expire
				tmp += it.value();
			}
		mDict.clear();
	}
	TableSchema * schema = mTable->schema();
	bool needRef = schema->isPreloadEnabled() || !schema->expireKeyCache();
	st_foreach( RecordIter, it, tmp )
		if( needRef )
			it.imp()->deref();
	//qDebug() << mTable->tableName() << " KeyIndex cleared";
}

bool KeyIndex::expire( uint recordKey, RecordImp * imp )
{
	_WLOCKER
	QHash<uint,RecordImp*>::iterator it = mDict.find( recordKey );
	if( it != mDict.end() ) {
		RecordImp * r = *it;
		if( r == imp ) {
			if( r->refCount() > 0 )
				return false;
			mDict.erase( it );
		}
		//qDebug() <<  "Expiring " << mTable->tableName() << " KeyIndex for record: " << QString::number(recordKey) << " index id: " << this;
	}
	return true;
}

} //namespace
