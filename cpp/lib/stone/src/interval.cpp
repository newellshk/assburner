
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of libstone.
 *
 * libstone is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * libstone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libstone; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <stdlib.h>

#include <qregexp.h>

#include "blurqt.h"
#include "interval.h"

#define MICROSECONDS_PER_SECOND 1000000
#define SECONDS_PER_MINUTE 60
#define SECONDS_PER_HOUR 3600
#define MINUTES_PER_DAY 1440
#define SECONDS_PER_DAY (MINUTES_PER_DAY * SECONDS_PER_MINUTE)
#define MICROSECONDS_PER_MINUTE (SECONDS_PER_MINUTE * qint64(MICROSECONDS_PER_SECOND))
#define MICROSECONDS_PER_DAY (qint64(SECONDS_PER_DAY) * qint64(MICROSECONDS_PER_SECOND))
#define HOURS_PER_DAY 24
#define MONTHS_PER_YEAR 12
#define MONTHS_PER_DECADE 120
#define MONTHS_PER_CENTURY 1200
#define MONTHS_PER_MILLENIUM 12000

Interval::Interval( const QDateTime & first, const QDateTime & second )
: mMicroseconds( 0 ), mDays( 0 ), mMonths( 0 )
{
	*this = addSeconds(first.secsTo(second));
}

Interval::Interval( const QDate & first, const QDate & second )
: mMicroseconds( 0 ), mDays( 0 ), mMonths( 0 )
{
	*this = addDays(first.daysTo(second));
}

Interval::Interval( int seconds, qint64 microseconds )
: mMicroseconds( 0 ), mDays( 0 ), mMonths( 0 )
{
	if (microseconds != 0  && microseconds != numeric_limits<qint64>::min())
		*this = addMicroseconds(microseconds);
	if (seconds != 0)
		*this = addSeconds(seconds);
}

Interval::Interval( int months, int days, int seconds, int microseconds )
: mMicroseconds( qint64(seconds) * MICROSECONDS_PER_SECOND + microseconds ), mDays( days ), mMonths( months )
{
}

Interval::Interval( const QString & string )
{
	*this = Interval::fromString(string);
}

Interval Interval::operator+( const Interval & other ) const
{
	if( isNull() || other.isNull() ) return Interval();
	Interval ret(*this);
	ret = ret.addMicroseconds( other.mMicroseconds );
	ret = ret.addDays( other.mDays );
	ret = ret.addMonths( other.mMonths );
	return ret;
}

Interval & Interval::operator+=( const Interval & other )
{
	*this = operator+( other );
	return *this;
}

Interval Interval::operator-( const Interval & other ) const
{
	if( isNull() || other.isNull() ) return Interval();
	Interval ret(*this);
	ret = ret.addMicroseconds( -other.mMicroseconds );
	ret = ret.addDays( -other.mDays );
	ret = ret.addMonths( -other.mMonths );
	return ret;
}

Interval & Interval::operator-=( const Interval & other )
{
	*this = operator-(other);
	return *this;
}

Interval Interval::operator*( double m ) const
{
	if( isNull() ) return Interval();
	Interval ret;
	ret = ret.addMicroseconds( qint64(mMicroseconds * m) );
	ret = ret.addDays( mDays * m );
	ret = ret.addMonths( mMonths * m );
	return ret;
}

Interval & Interval::operator*=( double m )
{
	*this = operator*(m);
	return *this;
}

Interval Interval::operator/( double d ) const
{
	if( isNull() ) return Interval();
	Interval ret;
	ret = ret.addMicroseconds( qint64(mMicroseconds / d) );
	ret = ret.addDays( mDays / d );
	ret = ret.addMonths( mMonths / d );
	return ret;
}

Interval & Interval::operator/=( double d )
{
	*this = operator/(d);
	return *this;
}

Interval Interval::abs() const
{
	if( isNull() ) return Interval();
	if( *this > Interval(0) )
		return *this;
	return this->operator*(-1.0);
}

double Interval::operator/( const Interval & other ) const
{
	if( isNull() || other.isNull() ) return numeric_limits<double>::quiet_NaN();
	return asOrder(Microseconds) / double(other.asOrder(Microseconds));
}

int Interval::compare( const Interval & i1, const Interval & i2 )
{
	qint64 cmp1(i1.asOrder(Days)), cmp2(i2.asOrder(Days));
	if( cmp1 == cmp2 ) {
		cmp1 = i1.asOrder(Microseconds);
		cmp2 = i2.asOrder(Microseconds);
	}
	if( cmp1 > cmp2 ) return 1;
	if( cmp2 > cmp1 ) return -1;
	return 0;
}

bool Interval::operator == ( const Interval & other ) const
{
	return compare( *this, other ) == 0;
}

bool Interval::operator != ( const Interval & other ) const
{
	return compare( *this, other ) != 0;
}

bool Interval::operator > ( const Interval & other ) const
{
	return compare( *this, other ) > 0;
}

bool Interval::operator < ( const Interval & other ) const
{
	return compare( *this, other ) < 0;
}

bool Interval::operator >= ( const Interval & other ) const
{
	return compare( *this, other ) >= 0;
}

bool Interval::operator <= ( const Interval & other ) const
{
	return compare( *this, other ) <= 0;
}

int Interval::microseconds() const
{ return mMicroseconds; }

int Interval::milliseconds() const
{
	return mMicroseconds / 1000;
}

qint64 Interval::seconds() const
{
	return mMicroseconds / MICROSECONDS_PER_SECOND;
}

int Interval::minutes() const
{
	return mMicroseconds / MICROSECONDS_PER_MINUTE;
}

int Interval::hours() const
{
	return seconds() / SECONDS_PER_HOUR;
}

int Interval::days() const
{
	return mDays;
}

int Interval::months() const
{
	return mMonths;
}

int Interval::years() const
{
	return mMonths / MONTHS_PER_YEAR;
}

QString aip( const QString & s, qint64 value, const QString & type, bool skipZero )
{
	if( skipZero && value == 0 ) return s;
	QString ret = s;
	if( s.size() > 0 )
		ret += " ";
	ret = ret + QString::number( value ) + type;
	if( llabs(value) > 1 ) ret += "s";
	return ret;
}

QString padString( qint64 num, int padSize = 2 )
{
	QString ret = QString::number( num );
	while( ret.size() < padSize )
		ret = "0" + ret;
	return ret;
}

QString Interval::toString() const
{ return toString( Years, Microseconds ); }

qint64 Interval::asOrder( Order order ) const
{
	qint64 months = mMonths, days = mDays, micros = mMicroseconds;

	if ( order < Months ) { days += (months / 12) * 365 + (months % 12) * 30; months = 0; }
	if ( order < Days ) { micros += days * MICROSECONDS_PER_DAY; days = 0; }
	if ( order >= Days ) { days += micros / MICROSECONDS_PER_DAY; }
	if ( order >= Months ) { months += days / 30; }

	switch( order ) {
		case Millenia:
			return months / MONTHS_PER_MILLENIUM;
		case Centuries:
			return months / MONTHS_PER_CENTURY;
		case Decades:
			return months / MONTHS_PER_DECADE;
		case Years:
			return months / 12;
		case Months:
			return months;
		case Days:
			return days;
		case Hours:
			return micros / (MICROSECONDS_PER_MINUTE * 60);
		case Minutes:
			return micros / MICROSECONDS_PER_MINUTE;
		case Seconds:
			return micros / MICROSECONDS_PER_SECOND;
		case Milliseconds:
			return micros / 1000;
		case Microseconds:
			return micros;
	}
	return 0;
}


QString Interval::toString( Order maximumOrder, Order minimumOrder, int flags ) const
{
	static const QString s_millenia(" millenia"), s_centuries(" centuries"), s_decade(" decade"), s_year(" year"), s_mon(" mon"), s_day(" day"), s_hour(" hour"), s_empty("00:00:00");
	QString ret;
	qint64 months = mMonths, days = mDays, micros = mMicroseconds;
	qint64 millis = 0, seconds = 0, hours = 0, minutes = 0, millenia = 0, centuries = 0, decades = 0;
	bool trimMax = flags & TrimMaximum;
#define TRIM_MIN ((flags & TrimMinimum) && months == 0 && days == 0 && micros == 0)
	if( maximumOrder < Seconds ) maximumOrder = Seconds;

	if( !(flags & ChopMaximum) ) {
		if ( maximumOrder < Months ) { days += (months / 12) * 365 + (months % 12) * 30; months = 0; }
		if ( maximumOrder < Days ) { micros += days * MICROSECONDS_PER_DAY; days = 0; }
	}
	switch( maximumOrder ) {
		case Millenia:
			millenia = months / MONTHS_PER_MILLENIUM;
			months %= MONTHS_PER_MILLENIUM;
			ret = aip( ret, millenia, s_millenia, trimMax );
			if( minimumOrder == Millenia || TRIM_MIN ) break;
		case Centuries:
			centuries = months / MONTHS_PER_CENTURY;
			months %= MONTHS_PER_CENTURY;
			ret = aip( ret, centuries, s_centuries, trimMax );
			if( minimumOrder == Centuries || TRIM_MIN ) break;
		case Decades:
			decades = months / MONTHS_PER_DECADE;
			months %= MONTHS_PER_DECADE;
			ret = aip( ret, decades, s_decade, trimMax );
			if( minimumOrder == Decades || TRIM_MIN ) break;
		case Years:
			ret = aip( ret, months / 12, s_year, trimMax );
			months %= MONTHS_PER_YEAR;
			if( minimumOrder == Years || TRIM_MIN ) break;
		case Months:
			if( flags & ChopMaximum ) months %= MONTHS_PER_YEAR;
			ret = aip( ret, months, s_mon, trimMax );
			months = 0;
			if( minimumOrder == Months || TRIM_MIN ) break;
		case Days:
			ret = aip( ret, days, s_day, trimMax );
			days = 0;
			if( minimumOrder == Days || TRIM_MIN ) break;
		case Hours:
			hours = micros / (MICROSECONDS_PER_MINUTE * 60);
			micros %= (MICROSECONDS_PER_MINUTE * 60);
			if( minimumOrder == Hours )
				ret = aip( ret, hours, s_hour, trimMax );
			else {
				if( !ret.isEmpty() ) ret += ' ';
				if (micros < 0)
					micros = llabs(micros);
				if (hours < 0) {
					ret += '-';
					hours = llabs(hours);
				}
				ret += padString( hours, (flags & PadHours) ? 2 : 0 ) + ':';
			}
			if( minimumOrder == Hours ) break;
		case Minutes:
			if( micros < 0 ) {
				micros = llabs(micros);
				ret += '-';
			}
			minutes = micros / MICROSECONDS_PER_MINUTE;
			micros %= MICROSECONDS_PER_MINUTE;
			ret += padString( minutes, 2 );
			// If we display minutes always display seconds even if zero
			//if( minimumOrder == Minutes || TRIM_MIN ) break;
			ret += ':';
		case Seconds:
			if( micros < 0 ) {
				micros = llabs(micros);
				ret += '-';
			}
			seconds = micros / MICROSECONDS_PER_SECOND;
			micros %= MICROSECONDS_PER_SECOND;
			ret += padString( seconds, 2 );
			if( minimumOrder == Seconds || TRIM_MIN ) break;
			ret += '.';
		case Milliseconds:
			millis = micros / 1000;
			micros %= 1000;
			ret += padString(millis, 3);
			if( minimumOrder == Milliseconds || TRIM_MIN ) break;
		case Microseconds:
			ret += padString(micros, 3);
			break;
	}
	if( ret.isEmpty() )
		ret = s_empty;
	return ret;
}

QString Interval::toDisplayString() const
{
	/*
	if( mDays == 0 && mMonths == 0 ) {
		qint64 _hours = hours(), _minutes = minutes(), _seconds = seconds();
		if( _hours > 0 && (_minutes % 60 == 0) && (_seconds % 3600 == 0) )
			return aip( "", _hours, " hour", false );
		if( _hours == 0 && (_minutes > 0) && (_seconds % 60 == 0) )
			return aip( "", _minutes, " minute", false );
		if( _hours == 0 && _minutes == 0 && microseconds() == 0 )
			return aip( "", _seconds, " second", false );
	}*/
	return toString();
}

static bool comparePlural( const QString & token, const QLatin1String & identifier )
{
	const Qt::CaseSensitivity CIS = Qt::CaseInsensitive;
	if( !token.startsWith(identifier,CIS) ) return false;
	const int identSize = strlen(identifier.latin1());
	const int tokenSize = token.size();
	return tokenSize == identSize
			||
			(tokenSize == identSize + 1 && token[tokenSize-1].toLower() == 's');
}

static bool hasFractional( double d )
{
	double integral;
	return modf(d,&integral) != 0.;
}

Interval Interval::fromString( const QString & iso, bool * valid )
{
	Interval ret;
	if( iso.isEmpty() ) return ret;
	
	const static QLatin1String millenia("millennia"), millennium("millennium"), centuries("centuries"), century("century"),
		decade("decade"), year("year"), month("month"), week("week"), day("day"), hour("hour"), minute("minute"), second("second"),
		millisecond("millisecond"), microsecond("microsecond");
		
	enum {
		Start,
		InNumber,
		InIdent,
		InDHMS,
		AfterNumber,
		Error
	};
	
	int state = Start;
	int tokenStart = 0;
	double numberDouble = 0.;
	bool v;
	QList<double> dhms;
	dhms.reserve(4);
	QString error, tmp;
	const Qt::CaseSensitivity CIS = Qt::CaseInsensitive;
	
	for( int i=0, end=iso.size(); i<=end; ++i ) {
		QChar c = i == end ? QChar() : iso.at(i);
		switch( state ) {
			case Start:
				if( c.isNumber() || c == '-' || c == '+' || c == '.' ) {
					state = InNumber;
					tokenStart = i;
					if (c == '+') tokenStart++;
				} else if ( !c.isSpace() && !c.isNull() ) {
					state = Error;
					error = QString("Unexpected token \\u%1").arg(c.unicode(),4,16);
				}
				break;
			case InNumber:
			case InDHMS:
				if( c.isSpace() || (state == InDHMS && c.isNull()) ) {
					tmp = iso.mid(tokenStart,i-tokenStart);
					numberDouble = tmp.toDouble(&v);
					if (state == InDHMS)
						dhms.push_back(numberDouble);
					if( !v ) { state = Error; error = QString("Unable to parse number from %1 at %2:%3").arg(tmp).arg(tokenStart).arg(i); break; }
					if( state == InDHMS ) {
						switch( dhms.size() ) {
							case 2:
								ret = ret.addHours( dhms[0] );
								ret = ret.addMinutes( dhms[1] );
								break;
							case 3:
								if( hasFractional(dhms[1]) ) {
									error = "Fractional not allowed";
									state = Error;
									break;
								}
								if( hasFractional(dhms[0]) ) {
									if( hasFractional(dhms[2]) ) {
										error = "Fractional not allowed(2)";
										state = Error;
										break;
									}
									ret = ret.addDays( dhms[0] ).addHours( dhms[1] ).addMinutes( dhms[2] );
								} else
									ret = ret.addHours( dhms[0] ).addMinutes( dhms[1] ).addSeconds( dhms[2] );
								break;
							case 4:
								if( hasFractional(dhms[1]) || hasFractional(dhms[2]) ) {
									error = "Fractional not allowed(3)";
									state = Error;
									break;
								}
								ret = ret.addDays( dhms[0] );
								ret = ret.addHours( dhms[1] );
								ret = ret.addMinutes( dhms[2] );
								ret = ret.addSeconds( dhms[3] );
								break;
						}
						state = Start;
					} else
						state = AfterNumber;
				} else if( c == ':' ) {
					tmp = iso.mid(tokenStart,i-tokenStart);
					numberDouble = tmp.toDouble(&v);
					if( !v ) { state = Error; error = QString("Unable to parse number from %1 at %2:%3").arg(tmp).arg(tokenStart).arg(i); break; }
					dhms.push_back(numberDouble);
					if( dhms.size() > 4 ) { state = Error; error = QString("Too many sections in D:H:M:S format"); break; }
					tokenStart = i+1;
					state = InDHMS;
				} else if( c.isNull() ) {
					state = Error;
					error = "Number without following identifier";
					break;
				}
				break;
			case AfterNumber:
				if( c.isNumber() || c.isNull() ) {
					state = Error;
					error = "Number without following identifier";
					break;
				} else if( c.isLetter() ) {
					state = InIdent;
					tokenStart = i;
				}
			case InIdent:
				if( c.isSpace() || c.isNull() ) {
					tmp = iso.mid(tokenStart,i-tokenStart);
					if( !tmp.compare(millenia,CIS) || !tmp.compare(millennium,CIS) )
						ret = ret.addMillenia( numberDouble );
					else if( !tmp.compare(centuries,CIS) || !tmp.compare(century,CIS) )
						ret = ret.addCenturies( numberDouble );
					else if( comparePlural(tmp,decade) )
						ret = ret.addYears(numberDouble * 10.);
					else if( comparePlural(tmp,year) )
						ret = ret.addYears(numberDouble);
					else if( comparePlural(tmp,month) )
						ret = ret.addMonths(numberDouble);
					else if( comparePlural(tmp,week) )
						ret = ret.addWeeks(numberDouble);
					else if( comparePlural(tmp,day) )
						ret = ret.addDays(numberDouble);
					else if( comparePlural(tmp,hour) )
						ret = ret.addHours(numberDouble);
					else if( comparePlural(tmp,minute) )
						ret = ret.addMinutes(numberDouble);
					else if( comparePlural(tmp,second) )
						ret = ret.addSeconds(numberDouble);
					else if( comparePlural(tmp,millisecond) )
						ret = ret.addMilliseconds(numberDouble);
					else if( comparePlural(tmp,microsecond) )
						ret = ret.addMicroseconds(numberDouble);
					else {
						state = Error;
						error = "Invalid identifier";
						break;
					}
					state = Start;
				}
				
		};
		if( state == Error ) {
			LOG_5( "Invalid Interval format: \"" + iso + "\" " + error + " at token " + QString::number(i) );
			break;
		}
	}
	if( valid ) *valid = (state != Error);
	return ret;
}

Interval Interval::addMillenia( double m )
{
	return addYears( m * 1000 );
}

Interval Interval::addCenturies( double c )
{
	return addYears( c * 100 );
}

Interval Interval::addYears( double y )
{
	return addMonths( y * 12.0 );
}

Interval Interval::addMonths( double m )
{
	if( isNull() ) return Interval();
	Interval ret = *this;
	int mi = int(m);
	ret.mMonths += mi;
	if( m - double(mi) != 0 )
		ret = ret.addDays( (m - double(mi)) * 30.0 );
	return ret;
}

Interval Interval::addWeeks( double w )
{
	return addDays( w * 7 );
}

Interval Interval::addDays( double d )
{
	if( isNull() ) return Interval();
	Interval ret = *this;
	int di = int(d);
	ret.mDays += di;
	//LOG_1( "Adding days " + QString::number(di) );
	if( d - double(di) != 0 )
		ret = ret.addSeconds( (d - double(di)) * double(SECONDS_PER_DAY) );
	return ret;
}

Interval Interval::addHours( double h )
{
	return addSeconds( h * SECONDS_PER_HOUR );
}

Interval Interval::addMinutes( double m )
{
	return addSeconds( m * SECONDS_PER_MINUTE );
}

Interval Interval::addSeconds( double s )
{
	return addMicroseconds( qint64(double(MICROSECONDS_PER_SECOND) * s) );
}

Interval Interval::addMilliseconds( double m )
{
	return (*this).addMicroseconds( qint64(m * 1000.0) );
}

Interval Interval::addMicroseconds( qint64 m )
{
	if( isNull() ) return Interval();
	Interval ret = *this;
	ret.mMicroseconds += m;
	if( llabs(ret.mMicroseconds) >= MICROSECONDS_PER_DAY ) {
		ret = ret.addDays( ret.mMicroseconds / MICROSECONDS_PER_DAY );
		ret.mMicroseconds %= MICROSECONDS_PER_DAY;
		//LOG_1( "Added microseconds: " + QString::number(m) );
	}
	return ret;
}

QDateTime Interval::adjust( const QDateTime & dt ) const
{
	return dt.addMonths( mMonths ).addDays( mDays ).addMSecs( mMicroseconds / 1000 );
}

QString Interval::reformat( const QString & is, Order maximumOrder, Order minimumOrder, int orderFlags, bool * success )
{
	bool valid;
	Interval i = fromString(is, &valid);
	if( success ) *success = valid;
	if( !valid ) return is;
	return i.toString( maximumOrder, minimumOrder, orderFlags );
}

