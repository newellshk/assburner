
#include <limits.h>

#include <qstringlist.h>

#include "intlist.h"

int IntList::Null = INT_MIN;

IntList::IntList()
: QList<int>() {}

IntList::IntList(const QList<int> & o)
: QList<int>(o) {}

QString IntList::toString( bool addArrayBraces )
{
	static const QString s_null("NULL");
	QStringList toJoin;
	foreach( int i, *this )
		toJoin.append( i == Null ? s_null : QString::number(i) );
	QString ret = toJoin.join(",");
	if( addArrayBraces )
		ret = "{" + ret + "}";
	return ret;
}

IntList IntList::fromString(const QString & s, bool * valid)
{
	IntList ret;
	int start = 0;
	for( int i=0, end=s.size(); i<end; ++i ) {
		if( !s[i].isDigit() ) {
			if( i+3 < end && !s.midRef(i,4).compare("null",Qt::CaseInsensitive)) {
				ret.append(Null);
				i+=3;
			}
			else if( i > start && s[i] == ',' ) {
				
#if QT_VERSION >= 0x050000
				ret.append( s.midRef(start,i-start).toInt() );
#else
				ret.append( s.mid(start,i-start).toInt() );
#endif
			} else if (!(s[i] == '{' && i == 0) && !(s[i] == '}' && i == end-1)) {
				if (valid) *valid = false;
				return IntList();
			}
			start = i+1;
		}
	}
	if (valid) *valid = true;
	return ret;
}

