/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of libstone.
 *
 * libstone is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * libstone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libstone; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qdatetime.h>
#include <qtextstream.h>
#include <qapplication.h>
#include <qiodevice.h>

#include "multilog.h"
#include "iniconfig.h"
#include "process.h"

#ifdef Q_OS_WIN
#include <io.h>
#else
#include <sys/file.h>
#endif

Multilog::Multilog( const QString & logfile, bool stdout_option, int severity, int maxfiles, unsigned int maxsize)
: mLogFileName( logfile )
, mEchoStdOut( stdout_option )
, mSeverity( severity )
, mMaxFiles( maxfiles )
, mMaxSize( maxsize)
, mLogFile( 0 )
#ifdef Q_OS_WIN
, mLogFileHandle( INVALID_HANDLE_VALUE )
#endif
, mLogStream( 0 )
{
	mLogFileInfo.setCaching( false );
	mLogFileInfo.setFile( mLogFileName );
	mLogFileDir = mLogFileInfo.dir(); //abs path of dir

	rotate( qMin(2000,(int)mMaxSize) );
}

Multilog::~Multilog()
{
	if( mLogFile ) {
		mLogFile->flush();
		mLogFile->close();
#ifdef Q_OS_WIN
		if( mLogFileHandle != INVALID_HANDLE_VALUE ) {
			CloseHandle(mLogFileHandle);
			mLogFileHandle = INVALID_HANDLE_VALUE;
		}
#endif
	}
	delete mLogFile;
	delete mLogStream;
}

bool Multilog::rotate( int buffer )
{
	mLogFileInfo.setFile( mLogFileName );
	mLogFileInfo.refresh();

	if ( mLogFileInfo.size()+buffer > mMaxSize) {
		//close logfile, rename with date-timestamp in filename
		
		QString oldFileStr = mLogFileName;
		QString newFileStr = mLogFileInfo.path() + "/" + mLogFileInfo.completeBaseName() 
				+ QDateTime::currentDateTime().toString( ".yyyy-MM-ddThh.mm.ss.log" );
		QString logFileDirFilter = mLogFileInfo.completeBaseName() + ".*.log";

		delete mLogStream;
		mLogStream = 0;
		
		delete mLogFile;
		mLogFile = 0;

#ifdef Q_OS_WIN
		if( mLogFileHandle != INVALID_HANDLE_VALUE ) {
			CloseHandle(mLogFileHandle);
			mLogFileHandle = INVALID_HANDLE_VALUE;
		}
#endif

		qWarning("Multilog::rotate: trying to rename %s to %s", qPrintable(oldFileStr), qPrintable(newFileStr));

		if ( QDir::current().rename( oldFileStr, newFileStr ) ) {
			//scan mLogFileDir, sort by datestr in filenames, nuke any old logfiles over maxfiles limit
			QFileInfoList fil = mLogFileDir.entryInfoList( QStringList() << logFileDirFilter, QDir::Files, QDir::Time );
			
			int currentFile = 0;
			foreach( QFileInfo fi, fil ) {
				QString tempLogFileStr = fi.filePath();
				currentFile++;
				if (currentFile > (mMaxFiles - 1)) { // 1st logfile is always current logfile.
					QFile tempFile(tempLogFileStr);
					if( tempFile.remove() )
						qWarning( "Multilog::rotate delete logfile (# %s, over MaxFiles limit): %s", qPrintable( QString::number(currentFile)), qPrintable(tempLogFileStr) );
					else
						qWarning( "Multilog::rotate delete logfile (# %s) failed! -- %s",  qPrintable(QString::number(currentFile)),  qPrintable(tempFile.errorString()) );
				}
			}
		}
	}

	//open logfile if not already open
	if ( !mLogFile ) {
		
		bool exclusiveOpenSuccess = false;
		mLogFile = new QFile(mLogFileName);
		if ( mLogFile->open(QIODevice::Append) )
		{
#ifdef Q_OS_WIN
#if 0
			/* Reopen the file without FILE_SHARE_WRITE to prevent other writers */
			mLogFileHandle = ReOpenFile(
								(HANDLE)_get_osfhandle(mLogFile->handle()),
								GENERIC_READ | GENERIC_WRITE,
								FILE_SHARE_READ,
								NULL );
			
			/* Someone already has this file open  */
			if( mLogFileHandle != INVALID_HANDLE_VALUE )
				exclusiveOpenSuccess = true;
#endif // 0
			exclusiveOpenSuccess = true;
#else
			// Check for another user
			if( flock(mLogFile->handle(), LOCK_EX|LOCK_NB) != -1 )
				exclusiveOpenSuccess = true;
#endif
		}
		
		/* If we fail to open the file or take the correct write-only lock
		 * then we will try a filename with the pid in it, if that also
		 * fails we will try inside the temp directory, finally we will
		 * fail.  Recursion is used for each attempt since it's possible
		 * that an existing file will need rotated out of the way */
		if( !exclusiveOpenSuccess ) {
			if( mLogFile ) {
				mLogFile->close();
				delete mLogFile;
				mLogFile = 0;
			}
			
			int nameLen = mLogFileName.size();
			QString pidName = QString::number(processID()) + ".log";
			
			/* If we already tried opening to pidName and failed, then
			 * this time we will try the temp directory */
			if( mLogFileName.endsWith(pidName) ) {
#ifdef Q_OS_WIN
				QString tmp("c:/temp/");
#else
				QString tmp("/tmp/");
#endif
				/* If we already tried opening in the temp dir, then give up and fail */
				if( mLogFileName.startsWith(tmp) )
					return false;

				mLogFileName = tmp + pidName;
				return rotate(buffer);
			}
	
			mLogFileName = mLogFileName.left(nameLen-3) + pidName;
			return rotate(buffer);
		}

		/* Should only ever get here if the file is opened successfully */
		mLogStream = new QTextStream( mLogFile );
	}

	return true;
}

void Multilog::log( const int severity, const QString & logmessage, const QString & file, const QString & /* function */ )
{
	QMutexLocker lock( &mMutex );
	//check severity, log if required
	if (severity <= mSeverity) {
		QString line = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz") + ":" + QString::number(qApp->applicationPid()) + " " + file + ":" + logmessage + "\r\n";
		
		if( !mFileFilter.isEmpty() )
			if( file.contains( mFileFilter ) )
				return;

		//if( !mFunctionFilter.isEmpty() )
		//	if( mFunctionFilter.search( file ) < 0 )
		//		return;
		rotate( line.length() ); //before output, check size of file, rotate if needed

		if( mLogStream ) {
			*mLogStream << line;
			mLogStream->flush();
		} //else
		//	qWarning( "Log stream not open!!!" );

		if( mEchoStdOut || mEchoStdErr ) {
			QByteArray utf8 = line.toUtf8();
			if( mEchoStdOut )
				fwrite( utf8.constData(), sizeof(char), utf8.size(), stdout );
			if( mEchoStdErr )
				fwrite( utf8.constData(), sizeof(char), utf8.size(), stderr );
		}

		emit logged( line );
	}
	
}

void Multilog::setStdOut( bool stdout_option )
{
	mEchoStdOut = stdout_option;
}

bool Multilog::stdOut() const
{
	return mEchoStdOut;
}

void Multilog::setStdErr( bool stderr_option )
{
	mEchoStdErr = stderr_option;
}

bool Multilog::stdErr() const
{
	return mEchoStdErr;
}

void Multilog::setLogLevel( int severity )
{
	if (mSeverity != severity && severity <= 10)
		mSeverity = severity; //set global int value
}

int Multilog::logLevel() const
{
	return mSeverity;
}

void Multilog::setMaxFiles( int maxfiles )
{
	if (mMaxFiles != maxfiles && maxfiles <= 100)
		mMaxFiles = maxfiles;
}

int Multilog::maxFiles() const
{
	return mMaxFiles;
}

void Multilog::setMaxSize( unsigned int maxsize ) // minimum size of 
{
	if (mMaxSize != maxsize && maxsize >= 2<<16)
		mMaxSize = maxsize;
}

int Multilog::maxSize() const
{
	return mMaxSize;
}

QRegExp Multilog::filesFilter() const
{
	return mFileFilter;
}

void Multilog::setFilesFilter( const QRegExp & ff )
{
	mFileFilter = ff;
}
/*
QRegExp Multilog::functionsFilter() const
{
	return mFunctionFilter;
}

void Multilog::setFunctionsFilter( const QRegExp & ff )
{
	mFunctionFilter = ff;
}
*/

