
#include "os.h"

OperatingSystem currentOS()
{
#ifdef Q_OS_LINUX
	return Linux;
#elif defined(Q_OS_MAC)
	return OSX;
#elif defined(Q_OS_LINUX)
	return Windows;
#endif
	return Invalid;
}
