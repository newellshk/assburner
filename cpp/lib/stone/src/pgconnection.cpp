
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of libstone.
 *
 * libstone is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * libstone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libstone; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qsqldriver.h>
#include <qsqlerror.h>
#include <qsqlquery.h>
#include <qregexp.h>

#include "database.h"
#include "iniconfig.h"
#include "joinedselect.h"
#include "pgconnection.h"
#include "schema.h"
#include "table.h"
#include "tableschema.h"

static const char * reservedNames [] = { "date", "time", "timestamp", "union", "unique", "table", "sum", "select", "returning", "primary", "over", "overlaps", "or", "order", "on", "only", "not", "natural", "limit", "like", "join", "into", "intersect", "in", "ilike", "group", "full", "freeze", "for", "foreign", "fetch", "except", "else", "do", "distinct", "default", "create", "constraint", "column", "collate", "check", "case", "cast", "both", "binary", "array", "all", "any", "and", "analyze", "true", "false", "to", "using", "user", "where", "when", "reserved", "offset", 0 };

static QHash<uint,QString> * sReservedNamesHash = 0;

PGConnection::PGConnection()
: QSqlDbConnection( "QPSQL7" )
, mVersionMajor( 0 )
, mVersionMinor( 0 )
, mUseMultiTableSelect( false )
{
	if( !sReservedNamesHash ) {
		sReservedNamesHash = new QHash<uint,QString>();
		addReservedNames( sReservedNamesHash, reservedNames );
	}
	mReservedNames = sReservedNamesHash;
	
	connect( mDb.driver(), SIGNAL(notification(const QString&)), SIGNAL(notification(const QString&)) );
}

void PGConnection::setOptionsFromIni( IniConfig ini )
{
	QSqlDbConnection::setOptionsFromIni( ini );
	mUseMultiTableSelect = ini.readBool( "UseMultiTableSelect", false );
}

Connection::Capabilities PGConnection::capabilities() const
{
	
	Connection::Capabilities ret = static_cast<Connection::Capabilities>(QSqlDbConnection::capabilities() | Cap_Inheritance | Cap_MultipleInsert | (mUseMultiTableSelect ? Cap_MultiTableSelect : 0)
		| Cap_TableOids | Cap_Notifications | Cap_ChangeNotifications );
	
	// First query will be to query the version, and capabilities will be called again
	// avoid infinite loop by bypassing the cap_returning check.
	static bool checkingVersion = false;
	if( !checkingVersion ) {
		checkingVersion = true;
		if( checkVersion( 8, 2 ) )
			ret = static_cast<Connection::Capabilities>(ret | Cap_Returning);
		checkingVersion = false;
	}
	
#ifndef Q_OS_WIN
	ret = static_cast<Connection::Capabilities>(ret | Cap_FakePrepareHack);
#endif
	return ret;
}

static void getVersion( PGConnection * c, int & vMaj, int & vMin )
{
	QSqlQuery q = c->exec( "SELECT version()" );
	if( q.next() ) {
		QString versionString = q.value(0).toString();
		//LOG_3( "Got Postgres Version string: " + versionString );
		QRegExp vre( "PostgreSQL (\\d+)\\.(\\d+)" );
		if( versionString.contains( vre ) ) {
			vMaj = vre.cap(1).toInt();
			vMin = vre.cap(2).toInt();
			//LOG_3( "Got Postgres Version: " + QString::number( vMaj ) + "." + QString::number( vMin ) );
		}
	}
}

bool PGConnection::checkVersion( int major, int minor ) const
{
	PGConnection * c = const_cast<PGConnection*>(this);
	// By calling this we can ensure that we don't select the version twice by having this
	// same function called from code connected to the connected signal.  The function will run
	// twice but the first call will end up doing nothing since mVersionMajor will already be set.
	c->checkConnection();
	if( !mVersionMajor ) {
		PGConnection * c = const_cast<PGConnection*>(this);
		getVersion( c, c->mVersionMajor, c->mVersionMinor );
	}
	return (mVersionMajor > major) || (mVersionMajor == major && mVersionMinor >= minor);
}

bool PGConnection::reconnect()
{
	if( QSqlDbConnection::reconnect() ) {
		//checkVersion(0,0);
		return true;
	}
	return false;
}

uint PGConnection::newPrimaryKey( TableSchema * schema )
{
	while( schema->parent() ) schema = schema->parent();
	QString sql("SELECT nextval('" + schema->tableName().toLower() + "_" + schema->primaryKey().toLower() + "_seq')" );
	QSqlQuery q = exec( sql );
	if( q.isActive() && q.next() )
		return q.value(0).toUInt();
	return 0;
}

bool pgtypeIsCompat( const QString pg_type, const bool pg_is_array, uint enumType )
{
	if( pg_type == "text" )
		return pg_is_array ? (enumType==Field::StringArray) : (enumType==Field::String);
	if( pg_type == "varchar" )
		return ( enumType==Field::String );
	if( pg_type == "int2" )
		return ( enumType==Field::UInt || enumType==Field::Int );
	if( pg_type == "int4" )
		return pg_is_array ? (enumType==Field::IntArray) : (enumType==Field::UInt || enumType==Field::Int);
	if( pg_type == "int8" )
		return ( enumType==Field::UInt || enumType==Field::Int || enumType==Field::ULongLong );
	if( pg_type == "numeric" )
		return ( enumType==Field::Double || enumType==Field::Float );
	if( pg_type == "float4" )
		return (enumType==Field::Float || enumType==Field::Double);
	if( pg_type == "float8" )
		return (enumType==Field::Double || enumType==Field::Float);
	if( pg_type == "timestamp" )
		return ( enumType==Field::DateTime );
	if( pg_type == "time" )
		return ( enumType==Field::Time );
	if( pg_type == "date" )
		return ( enumType==Field::Date );
	if( pg_type == "boolean" || pg_type == "bool" )
		return ( enumType==Field::Bool );
	if( pg_type == "bytea" )
		return ( enumType==Field::ByteArray || enumType==Field::Image );
	if( pg_type == "color" )
		return ( enumType==Field::Color );
	if( pg_type == "interval" )
		return ( enumType==Field::Interval );
	if( pg_type == "hstore" )
		return ( enumType==Field::StringMap );
	if( pg_type == "json" || pg_type == "jsonb" )
		return ( enumType==Field::Json );
	return false;
}

uint fieldTypeFromPgType( const QString & pg_type, const bool pg_is_array )
{
	if( pg_type == "text" || pg_type == "varchar" )
		return pg_is_array ? Field::StringArray : Field::String;
	if( pg_type == "int2" || pg_type == "int4" )
		return pg_is_array ? Field::IntArray : Field::Int;
	if( pg_type == "int8" )
		return Field::ULongLong;
	if( pg_type == "numeric" )
		return Field::Double;
	if( pg_type == "float4" )
		return Field::Float;
	if( pg_type == "float8" )
		return Field::Double;
	if( pg_type == "timestamp" )
		return Field::DateTime;
	if( pg_type == "time" )
		return Field::Time;
	if( pg_type == "date" )
		return Field::Date;
	if( pg_type == "boolean" || pg_type == "bool" )
		return Field::Bool;
	if( pg_type == "bytea" )
		return Field::ByteArray;
	if( pg_type == "color" )
		return Field::Color;
	if( pg_type == "interval" )
		return Field::Interval;
	if( pg_type == "hstore" )
		return Field::StringMap;
	if( pg_type == "json" || pg_type == "jsonb" )
		return Field::Json;
	return Field::Invalid;
}

QString warnAndRet( const QString & s ) { LOG_3( s ); return s + "\n"; }

bool PGConnection::tableExists( TableSchema * schema )
{
	return exec( "select * from pg_class WHERE pg_class.relname=? AND pg_class.relnamespace=2200", VarList() << schema->tableName().toLower() ).size() >= 1;
}

bool PGConnection::verifyTable( TableSchema * schema, bool createMissingColumns, QString * output )
{
	QString out;
	bool ret = true, updateDocs = false;

	if( !tableExists( schema ) ) {
		out += warnAndRet( "Table does not exist: " + schema->tableName() );
		if( output )
			*output = out;
		return false;
	}

	FieldList fl = schema->fields();
	QMap<QString, Field*> fieldMap;
	Field * missingPkey = nullptr;
	FieldList updateDesc;

	// pg_class stores all of the tables
	// relnamespace 2200 is the current public database
	// private columns have negative attnums
	QString info_query( "select att.attname, typ.typname, des.description, con.contype, att.attndims "
			"from pg_class cla "
			"inner join pg_attribute att on att.attrelid=cla.oid "
			"inner join pg_type typ on att.atttypid=typ.oid "
			"left join pg_description des on cla.oid=des.classoid AND att.attnum=des.objsubid "
			"left join pg_constraint con on con.conrelid=cla.oid and att.attnum=ANY(con.conkey) AND con.contype='p'"
			"where cla.relkind='r' AND cla.relnamespace=2200 AND att.attnum>0 AND cla.relname='" );
			
	info_query += schema->tableName().toLower() + "';";
	
	QSqlQuery q = exec( info_query );
	if( !q.isActive() )
	{
		out += warnAndRet( "Unable to select table information for table: " + schema->tableName() );
		out += warnAndRet( "Error was: " + q.lastError().text() );
		if (output) *output = out;
		return false;
	}
	
	foreach( Field * f, fl )
		if( !f->flag(Field::LocalVariable) )
			fieldMap[f->name().toLower()] = f;
	
	while( q.next() )
	{
		QString fieldName = q.value(0).toString();
		Field * fp = 0;
		QMap<QString, Field*>::Iterator fi = fieldMap.find( fieldName );
		
		if( fi == fieldMap.end() )
			continue;
		
		fp = *fi;
	
		const QString dbTypeName = q.value(1).toString();
		const bool dbIsArray = q.value(4).toBool();
		if( !pgtypeIsCompat( dbTypeName, dbIsArray, fp->type() ) )
		{
			out += warnAndRet( schema->tableName() + "." + fp->name() + "[" + fp->typeString() + "] not compatible: " + q.value(1).toString() );
			ret = false;
		}

		if( !fp->docs().isEmpty() && q.value(2).toString() != fp->docs() )
			updateDesc += fp;

		// Column marked as primary key in the schema, but not the database
		if( fp->flag(Field::PrimaryKey) && q.value(3).toString() != "p" )
			missingPkey = fp;
		
		fieldMap.remove ( fieldName );
	}

	if( !fieldMap.isEmpty() )
	{
		out += warnAndRet( "Couldn't find the following columns for " + schema->tableName() + ": " );
		QStringList cols;
		for( QMap<QString, Field*>::Iterator it = fieldMap.begin(); it != fieldMap.end(); ++it )
			cols += it.key();
		out += warnAndRet( cols.join( "," ) );
		if( createMissingColumns ) {
			out += warnAndRet( "Creating missing columns" );
			for( QMap<QString, Field*>::Iterator it = fieldMap.begin(); it != fieldMap.end(); ++it ) {
				Field * f = it.value();
				QString cc = "ALTER TABLE " + schema->tableName() + " ADD COLUMN \"" + f->name().toLower() + "\" " + f->dbTypeString() + ";";
				QSqlQuery query = exec(cc);
				if( !query.isActive() ) {
					out += warnAndRet( "Unable to create column: " + f->name() );
					out += warnAndRet( "Error was: " + query.lastError().text() );
					ret = false;
				}
			}
		}
	}
	
	if( missingPkey ) {
		QString tcn = schema->tableName() + "." + missingPkey->name();
		out += warnAndRet( "Primary Key constraint does not exist on " + tcn );
		if( createMissingColumns ) {
			QSqlQuery q = exec( "ALTER TABLE " + schema->tableName() + " ADD PRIMARY KEY (\"" + missingPkey->name().toLower() + "\")" );
			if( !q.isActive() ) {
				out += warnAndRet( "Unable to create primary key constraint on: " + tcn );
				out += warnAndRet( "Error was: " + q.lastError().text() );
				ret = false;
			}
		}
	}
	
	if( createMissingColumns && updateDocs && updateDesc.size() ) {
		out += warnAndRet( "Updating column descriptions" );
		foreach( Field * f, updateDesc ) {
			QString sql( "COMMENT ON " + schema->tableName().toLower() + "." + f->name().toLower() + " IS '" + f->docs() + "'");
			QSqlQuery query = exec( sql );
			if( !query.isActive() ) {
				out += warnAndRet( "Unable to set description: " + sql );
				ret = false;
			}
		}
	}

	if( schema->isPreloadEnabled() && !verifyChangeTrigger(schema,createMissingColumns) )
	
	if( output )
		*output = out;
	return ret;
}

bool PGConnection::createTable( TableSchema * schema, QString * output )
{
	QString out;
	bool ret = false;
	QString cre("CREATE TABLE ");
	cre += schema->tableName().toLower() + "  (";
	QStringList columns;
	FieldList fl = schema->fields();
	foreach( Field * f, fl ) {
		if( f->flag(Field::LocalVariable) ) continue;
		if( f->table() == schema ) {
			QString ct("\"" + f->name().toLower() + "\"");
			if( f->flag( Field::PrimaryKey ) ) {
				ct += " SERIAL PRIMARY KEY";
				columns.push_front( ct );
			} else {
				ct += " " + f->dbTypeString();
				columns += ct;
			}
		} else {
			// Inherited columns are implicitly created, but may
			// need constraints explicitly created
			if( f->flag( Field::PrimaryKey ) ) {
				columns += "CONSTRAINT PRIMARY KEY(\t" + f->name().toLower() + "\")";
			}
		}
	}
	cre += columns.join(",") + ")";
	if( schema->parent() )
		cre += " INHERITS (" + schema->parent()->tableName().toLower() + ")";
	cre += ";";
	out += warnAndRet( "Creating table: " + cre );
	
	QString action;
	try {
		action = "create table: " + schema->tableName();
		exec( cre );
		if( schema->isPreloadEnabled() ) {
			action = "create change trigger";
			verifyChangeTrigger(schema,true);
		}
		ret = true;
	} catch ( SqlException & e ) {
		out += warnAndRet( "Unable to " + action );
		out += warnAndRet( "Error was: " + e.error() );
		out += warnAndRet( "Sql was: " + e.sql() );
	}

	if( output )
		*output = out;
	
	return ret;
}

TableSchema * PGConnection::importTableSchema()
{
	return 0;
}

Schema * PGConnection::importDatabaseSchema()
{
	return 0;
}

RecordList PGConnection::selectOnly( Table * table, const QString & where, const QList<QVariant> & args )
{
	return selectFrom( table, "FROM ONLY " + tableQuoted(table->schema()->tableName()) + prepareWhereClause(where), args );
}

QList<RecordList> PGConnection::joinedSelect( const JoinedSelect & joined, QString where, QList<QVariant> args )
{
	Schema * schema = joined.table()->schema()->schema();
	Database * db = joined.table()->database();
	QList<RecordList> ret;
	QList<JoinCondition> joinConditions = joined.joinConditions();
	QList<Table*> tables;
	QList<bool> usingTableOid;
	QList<FieldList> fieldsPerTable;
	QList<int> pkeyPositions;
	
	tables += joined.table();
	usingTableOid += !joined.joinOnly();
	QString query( "SELECT " );
	// Generate column list
	QStringList fields;

	{
		// Add initial table
		ret += RecordList();
		int pkeyPos = -1;
		FieldList fl;
		fields += getSqlFields(joined.table()->schema(), joined.alias(), !joined.joinOnly(), &fl, &pkeyPos);
		fieldsPerTable += fl;
		pkeyPositions += pkeyPos;
	}
	
	foreach( JoinCondition jc, joinConditions ) {
		if( jc.ignoreResults )
			continue;
		tables += jc.table;
		usingTableOid += !jc.joinOnly;
		ret += RecordList();
		FieldList fl;
		int pkeyPos = -1;
		fields += getSqlFields(jc.table->schema(), jc.alias, !jc.joinOnly, &fl, &pkeyPos);
		fieldsPerTable += fl;
		pkeyPositions += pkeyPos;
		fl.clear();
	}
	
	query += fields.join(", ");
	query += " FROM ";
	if( joined.joinOnly() )
		query += "ONLY ";
	query += tableQuoted(joined.table()->schema()->tableName());
	
	// Generate join list
	int idx = 0;
	foreach( JoinCondition jc, joinConditions ) {
		switch( jc.type ) {
			case InnerJoin:
				query += " INNER";
				break;
			case LeftJoin:
				query += " LEFT";
				break;
			case RightJoin:
				query += " RIGHT";
				break;
			case OuterJoin:
				query += " OUTER";
				break;
		}
		query += " JOIN ";
		if( jc.joinOnly )
			query += "ONLY ";
		query += tableQuoted(jc.table->schema()->tableName());
		if( !jc.alias.isEmpty() && jc.alias != jc.table->schema()->tableName() )
			query += " AS " + jc.alias;

		query += " ON " + jc.condition;
		++idx;
	}
	
	query += prepareWhereClause(where) + ";";
	
	QSqlQuery sq = exec( query, args, true /*retry*/, tables[0] );
	
	while( sq.next() ) {
		int tableIndex = 0;
		int colOffset = 0;
		foreach( Table * table, tables ) {
			bool utoi = usingTableOid[tableIndex];
			Table * destTable = table;
			if( utoi ) {
				colOffset++;
				TableSchema * ts = tableByOid( sq.value(colOffset-1).toInt(), schema);
				destTable = ts ? db->tableFromSchema(ts) : table;
			}
			
			if( destTable ) {
				int pkeyPos = pkeyPositions[tableIndex];
				if( sq.value(colOffset + pkeyPos).toInt() != 0 )
					ret[tableIndex] += Record( new RecordImp( destTable, sq, colOffset, &fieldsPerTable[tableIndex] ), false );
				else
					ret[tableIndex] += Record(table);
			}
			
			colOffset += fieldsPerTable[tableIndex].size();
			
			++tableIndex;
		}
	}
	return ret;
}

QMap<Table *, RecordList> PGConnection::selectMulti( QList<Table*> tables,
	const QString & innerWhere, const QList<QVariant> & innerArgs,
	const QString & outerWhere, const QList<QVariant> & outerArgs )
{
	QMap<Table*,RecordList> ret;
	QMap<Table*,QVector<int> > positionsByTable;
	QVector<int> typesByPosition(1);
	QList<QVariant> allArgs;
	QMap<Table*,QStringList> colsByTable;
	QStringList selects;
	QString innerW(prepareWhereClause(innerWhere)), outerW(prepareWhereClause(outerWhere));

	bool colsNeedTableName = innerW.toLower().contains( "join" );

	// First position is the table position
	typesByPosition[0] = Field::Int;
	int tablePos = 0;

	foreach( Table * table, tables ) {
		TableSchema * schema = table->schema();
		FieldList fields = schema->fields();
		QVector<int> positions(fields.size());
		int pos = 1, i = 0;
		QStringList sql;
		QString tableName = schema->tableName();
		sql << QString::number(tablePos);
		foreach( Field * f, fields ) {
			if( f->flag( Field::NoDefaultSelect | Field::LocalVariable ) ) {
				positions[i++] = -1;
				continue;
			}
			while( pos < typesByPosition.size() && typesByPosition[pos] != f->type() ) {
				if( tablePos == 0 )
					sql << "NULL::" + Field::dbTypeString(Field::Type(typesByPosition[pos]));
				else
					sql << "NULL";
				pos++;
			}
			if( pos >= typesByPosition.size() ) {
				typesByPosition.resize(pos+1);
				typesByPosition[pos] = f->type();
			}
			if( colsNeedTableName )
				sql << tableName + prepareFieldName(f);
			else
				sql << prepareFieldName(f);
			positions[i++] = pos++;
		}
		tablePos++;
		colsByTable[table] = sql;
		allArgs << innerArgs;
		positionsByTable[table] = positions;
	}

	tablePos = 0;
	foreach( Table * t, tables ) {
		QStringList cols = colsByTable[t];
		while( cols.size() < typesByPosition.size() ) {
			if( tablePos == 0 )
				cols << "NULL::" + Field::dbTypeString(Field::Type(typesByPosition[cols.size()]));
			else
				cols << "NULL";
		}

		QString w(innerW);
		w.replace( tables[0]->schema()->tableName() + ".", t->schema()->tableName() + ".", Qt::CaseInsensitive);
		selects << "SELECT " + cols.join(", ") + " FROM ONLY " + t->schema()->tableName() + " " + w;
		
		tablePos++;
	}
	allArgs << outerArgs;
	
	QString select = "SELECT * FROM ( (" + selects.join(") UNION (") + ") ) AS IQ " + outerW;
	QSqlQuery sq = exec( select, allArgs, true, tables[0] );
	while( sq.next() ) {
		Table * t = tables[sq.value(0).toInt()];
		ret[t] += Record( new RecordImp( t, sq, positionsByTable[t] ), false );
	}
	return ret;
}

void PGConnection::selectFields( Table * table, RecordList records, FieldList fields )
{
	TableSchema * schema = table->schema();
	RecordList ret;
	
	QStringList fieldNames;
	foreach( Field * f, fields )
		if( !f->flag( Field::LocalVariable ) )
			fieldNames += f->name().toLower();

	QString tableName = tableQuoted(schema->tableName());
	QString sql = "SELECT " + tableName + ".\"" + fieldNames.join( "\", " + tableName + ".\"" ) + "\"" + " FROM ONLY " + tableName + " WHERE " + schema->primaryKey() + " IN (" + records.keyString() + ");";

	QSqlQuery sq = exec( sql, VarList(), true /*retry*/, table );
	RecordIter it = records.begin();
	while( sq.next() ) {
		if( it == records.end() ) break;
		int i = 0;
		Record r(*it);
		foreach( Field * f, fields )
			if( !f->flag(Field::LocalVariable) )
				r.imp()->fillColumn( f->pos(), sq.value(i++) );
	}
}

QString PGConnection::lastInsertPrimaryKey( TableSchema * tableSchema )
{
	return "currval('" + tableSchema->tableName() + "_" + tableSchema->primaryKey() + "_seq');";
}

struct ColUpdate {
	Field * field;
	// Records that need this field updated
	RecordList records;
};

bool PGConnection::update( Table * table, RecordList records, RecordList * returnValues )
{
	if( records.size() == 0 ) {
		LOG_1( "Empty recordlist");
		return true;
	}
	
	// For a single record the other version provides more readable sql
	if( records.size() == 1 ) {
		Record retVal;
		bool ret = QSqlDbConnection::update( table, records[0].imp(), returnValues ? &retVal : 0 );
		if( returnValues )
			returnValues->append(retVal);
		return ret;
	}
	
	QList<ColUpdate> colUpdates;
	TableSchema * schema = table->schema();
	FieldList fields = schema->fields();
	QString pkn = schema->primaryKey(), tn = schema->tableName();
	
	foreach( Field * f, fields ) {
		if( f->flag(Field::LocalVariable) )
			continue;
		ColUpdate cu;
		cu.field = f;
		foreach( Record r, records ) {
			if( r.imp()->isColumnModified(f->pos()) ) {
				cu.records.append(r);
			}
		}
		if( cu.records.size() )
			colUpdates.append(cu);
	}
	
	if( colUpdates.isEmpty() ) {
		LOG_1( "No modified columns" );
		return true;
	}
	
	FieldList fieldsToUpdate;
	QStringList cols, values, valueAs;
	valueAs += pkn;
	foreach( const ColUpdate & cu, colUpdates ) {
		fieldsToUpdate += cu.field;
		QString col = "\"" + cu.field->name().toLower() + "\"";
		if( cu.records.size() == records.size() ) {
			col += " = v." + col;
			if( cu.field->type() == Field::Date )
				col += "::date";
			else if( cu.field->type() == Field::DateTime )
				col += "::timestamp";
			else if( cu.field->type() == Field::Interval )
				col += "::interval";
		} else {
			col += " = CASE WHEN " + tn + "." + pkn + " IN (" + cu.records.keyString() + ") THEN v." + col + " ELSE " + tn + "." + col + " END";
		}
		cols += col;
		valueAs += "\"" + cu.field->name().toLower() + "\"";
	}
	
	typedef QPair<QString,QVariant> StrVarPair;
	QList<StrVarPair> toBind;
	int i = 0;
	foreach( Record r, records ) {
		QStringList rvals;
		rvals += QString::number(r.key());
		
		foreach( Field * f, fieldsToUpdate ) {
			if( !r.imp()->isColumnModified(f->pos()) ) {
				rvals += "NULL::" + f->dbTypeString();
				continue;
			}
			if( r.imp()->isColumnLiteral( f->pos() ) ) {
				rvals += r.imp()->getColumn(f).toString();
			} else {
				QString ph = f->placeholder(i);
				QVariant val = f->dbPrepare(r.imp()->getColumn(f));
				if( val.isNull() ) {
					rvals += "NULL::" + f->dbTypeString();
				} else {
					rvals += ph;
					toBind.append( qMakePair(ph,val) );
				}
			}
		}
		values.append( "(" + rvals.join(",") + ")" );
		++i;
	}
	
	
	QString up("UPDATE %1 SET ");
	up = up.arg( tableQuoted(schema->tableName()) );
	up += cols.join(", ") + " FROM (VALUES " + values.join(", ") + ") AS v(" + valueAs.join(", ") + ") WHERE v." + pkn + "=" + tn + "." + pkn;
	
	if( returnValues )
		up += " RETURNING " + getSqlFields( schema );
	up += ";";

	QSqlQuery q = fakePrepare( up );
	foreach( StrVarPair svp, toBind )
		q.bindValue( svp.first, svp.second );

	if( exec( q, true /*retryLostConn*/, table, /*usingFakePrepare =*/ true ) ) {
		if( returnValues ) {
			while( q.next() )
				returnValues->append( Record( new RecordImp( table, q ), false ) );
		}
		return true;
	}
	return false;
}

int PGConnection::remove( Table * table, const QString & keys, QList<int> * rowsDeleted )
{
	TableSchema * schema = table->schema();
	TableSchema * base = schema;
	// is this an inherited table? if so remove from the base class
	if( schema->inherits().size() )
		base = schema->inherits()[0];
	QString del("DELETE FROM ");
	del += tableQuoted(base->tableName());
	del += " WHERE ";
	del += base->primaryKey();
	del += " IN(" + keys + ") RETURNING " + base->primaryKey() + ";";
	QSqlQuery q = exec( del, QList<QVariant>(), true /*retryLostConn*/, table );
	if( !q.isActive() ) return -1;
	if( rowsDeleted ) {
		while( q.next() )
			rowsDeleted->append( q.value(0).toInt() );
	}
	return q.numRowsAffected();
}

bool PGConnection::createIndex( IndexSchema * schema )
{
	QString cmd;
	QStringList cols;
	foreach( Field * f, schema->columns() )
		if( f->flag(Field::LocalVariable) )
			return false;
		else
			cols += f->name();
	cmd += "CREATE INDEX " + schema->name();
	cmd += " ON TABLE " + tableQuoted(schema->table()->tableName()) + "(" + cols.join(",") + ")";
	if( !schema->databaseWhere().isEmpty() )
		cmd += " WHERE " + schema->databaseWhere();
	cmd += ";";

	return exec( cmd ).isActive();
}

void PGConnection::loadTableOids()
{
	QSqlQuery q = exec( "SELECT oid, relname FROM pg_class WHERE relkind='r' AND relnamespace=(SELECT oid FROM pg_namespace WHERE nspname='public')" );
	const int rows = q.size();
	mTablesByOid.reserve(rows);
	mOidsByTable.reserve(rows);
	while( q.next() ) {
		uint oid = q.value(0).toInt();
		QString tableName = q.value(1).toString().toLower();
		mTablesByOid[oid] = tableName;
		mOidsByTable[tableName] = oid;
	}
}

TableSchema * PGConnection::tableByOid( uint oid, Schema * schema )
{
	if( mTablesByOid.size() == 0 )
		loadTableOids();
	QHash<uint,QString>::iterator it = mTablesByOid.find( oid );
	if( it != mTablesByOid.end() )
		return schema->tableByName( it.value() );
	if( oid != 0 )
		LOG_1( "Unable to find table by oid: " + QString::number(oid) );
	return 0;
}

uint PGConnection::oidByTable( TableSchema * schema )
{
	if( mTablesByOid.size() == 0 )
		loadTableOids();
	QHash<QString,uint>::iterator it = mOidsByTable.find( schema->tableName().toLower() );
	if( it != mOidsByTable.end() )
		return it.value();
	LOG_1( "Unable to find oid for table: " + schema->tableName() );
	return 0;
}

bool PGConnection::verifyTriggerExists( TableSchema * table, const QString & triggerName )
{
	//LOG_5( "Verifying trigger " + triggerName + " for table " + table->tableName() + " with oid " + QString::number(oidByTable(table)) );
	if (mHasPreloadTriggerByOid.size() == 0) {
		QSqlQuery q = exec( "SELECT tgrelid FROM pg_trigger WHERE right(tgname,16)='_preload_trigger'");
		const int rows = q.size();
		if( rows > 0 ) {
			mHasPreloadTriggerByOid.reserve(q.size());
			while(q.next())
				mHasPreloadTriggerByOid[q.value(0).toInt()] = true;
		} else
			// Avoid reselecting each time if there are no preload triggers
			mHasPreloadTriggerByOid[0] = false;
	}
	return mHasPreloadTriggerByOid[oidByTable(table)];
}

static const char * listen_func_template = 
"CREATE OR REPLACE FUNCTION %1_preload_trigger() RETURNS TRIGGER AS $$\n"
"DECLARE\n"
"BEGIN\n"
"\tNOTIFY %2_preload_change;\n"
"\tRETURN NEW;\n"
"END;\n"
"$$ LANGUAGE 'plpgsql';";

static const char * listen_trig_template =
"CREATE TRIGGER %1_preload_trigger AFTER INSERT OR DELETE OR UPDATE ON %2 FOR EACH STATEMENT EXECUTE PROCEDURE %3_preload_trigger();";

static const char * preload_change_str = "_preload_trigger";

bool PGConnection::verifyChangeTrigger( TableSchema * table, bool create )
{
	QString tableName = table->tableName();
	QString triggerName = tableName.toLower() + QString::fromLatin1(preload_change_str);
	bool exists = verifyTriggerExists(table,triggerName);
	if( exists || !create )
		return exists;
	
	QString func = QString(listen_func_template).arg(tableName).arg(tableName);
	QString trig = QString(listen_trig_template).arg(tableName).arg(tableName).arg(tableName);
	exec(func);
	exec(trig);
	return true;
}

bool PGConnection::isConnectionError( const QSqlError & e )
{
	if( e.type() == QSqlError::ConnectionError
		|| e.databaseText().contains( "server closed the connection" )
		|| e.databaseText().contains( "Software caused connection abort" )
		|| e.databaseText().contains( "Connection timed out" )
		// Seems that connection errors sometimes give no database text, just Unable to create query
		|| ((e.databaseText() == "()" || e.databaseText().contains("could not receive data from server")) && e.driverText().contains( "Unable to create query" )) )
		return true;
	return false;
}

void PGConnection::_connected()
{
	QSqlDbConnection::_connected();
	listen( "auto_drop_stale_connections" );
}
