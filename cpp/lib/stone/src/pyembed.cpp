
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of libstone.
 *
 * libstone is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * libstone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libstone; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include "pyembed.h"

#include <qcolor.h>
#include <qdatetime.h>
#include <qmetatype.h>
#include <qimage.h>

#include "interval.h"
#include "intlist.h"
#include "tableschema.h"
#include "stringmap.h"

PythonException::PythonException()
: type(0)
, value(0)
, traceback(0)
{
	SIP_BLOCK_THREADS
	mCString = pythonExceptionTraceback(/*clearException=*/false).toUtf8();
	PyErr_Fetch(&type, &value, &traceback);
	SIP_UNBLOCK_THREADS
}

PythonException::PythonException(const PythonException & o)
: type(o.type)
, value(o.value)
, traceback(o.traceback)
{
	Py_XINCREF(type);
	Py_XINCREF(value);
	Py_XINCREF(traceback);
}

PythonException::~PythonException() throw()
{
	Py_XDECREF(type);
	Py_XDECREF(value);
	Py_XDECREF(traceback);
}

void PythonException::restore()
{
	SIP_BLOCK_THREADS
	PyErr_Restore(type, value, traceback);
	type = value = traceback = 0;
	SIP_UNBLOCK_THREADS
}

const char * PythonException::what() const throw()
{
	return mCString.constData();
}

static inline void ensurePythonInitialized()
{
	if( ! Py_IsInitialized() )
		Py_Initialize();
}

const sipAPIDef * getSipAPI()
{
	ensurePythonInitialized();

	static const sipAPIDef * api = 0;
	if( api ) return api;

	/* Import the SIP module and get it's API.
	 * libsip does not provide a symbol for accessing the sipAPIDef object
	 * it must be retrieved through sip's python module's dictionary
	 */
    SIP_BLOCK_THREADS
	PyObject * sip_sipmod = PyImport_ImportModule((char *)"sip");
	if (sip_sipmod == NULL) {
		LOG_3( "getSipAPI: Error importing sip module" );
	} else {

		PyObject * sip_capiobj = PyDict_GetItemString(PyModule_GetDict(sip_sipmod),"_C_API");
#if defined(SIP_USE_PYCAPSULE)
		if (sip_capiobj == NULL || !PyCapsule_CheckExact(sip_capiobj))
#else
		if (sip_capiobj == NULL || !PyCObject_Check(sip_capiobj))
#endif
			LOG_3( QString("getSipAPI: Unable to find _C_API object from sip modules dictionary: Got 0x%1").arg((qulonglong)sip_capiobj,0,16) );
		else
#if defined(SIP_USE_PYCAPSULE)
			api = reinterpret_cast<const sipAPIDef *>(PyCapsule_GetPointer(sip_capiobj, SIP_MODULE_NAME "._C_API"));
#else
			api = reinterpret_cast<const sipAPIDef *>(PyCObject_AsVoidPtr(sip_capiobj));
#endif
	}
    SIP_UNBLOCK_THREADS
	return api;
}
/*
sipExportedModuleDef * getSipModule( const char * name )
{
	const sipAPIDef * api = getSipAPI();
	if( !api ) return 0;

	sipExportedModuleDef * module = api->api_find_module( name );
	if( !module )
		LOG_5( "getSipModule: Unable to lookup module " + QString::fromLatin1(name) + " using api_find_module" );
	return module;
}
*/
sipTypeDef * getSipType( const char * /*module_name*/, const char * typeName )
{
	/*
	sipExportedModuleDef * module = getSipModule(module_name);
	if( !module ) return 0;

	for( int i = module->em_nrtypes - 1; i >= 0; i-- ) {
		sipTypeDef * type = module->em_types[i];
		// We could try checking type->u.td_py_type->name
		if( //strcmp( type->td_name, typeName ) == 0 ||
			 ( strcmp( sipNameFromPool( type->td_module, type->td_cname ), typeName ) == 0 ) )
			return type;
	}*/
	const sipTypeDef * type = getSipAPI()->api_find_type(typeName);
	if( type ) return const_cast<sipTypeDef*>(type);

	LOG_1( "getSipType: Unabled to find " + QString::fromLatin1(typeName) );// + " in module " + QString::fromLatin1(module_name) );
	return 0;
}

QMap<QPair<Table*,int>,CompiledPythonFragment> codeCache;

CompiledPythonFragment * getCompiledCode( Table * table, int pkey, const QString & pCode, const QString & codeName )
{
	CompiledPythonFragment * ret = 0;
	ensurePythonInitialized();
	// Create the key for looking up the code fragment in the cache
	QPair<Table*,int> codeKey = qMakePair(table,pkey);

	// Return the cached code fragment, if the code still matches
	if( codeCache.contains( codeKey ) ) {
		CompiledPythonFragment * frag = &codeCache[codeKey];
		// This should actually be very fast because of QString's
		// implicit sharing, should usually just be a pointer comparison
		if( frag->codeString == pCode )
			return frag;

		// Remove from cache if the fragment is out of date
		// TODO: This should free any references and remove the module
		// this isn't a big deal though, since code fragments won't
		// change very often during program execution
		codeCache.remove( codeKey );
	}

	if( pCode.isEmpty() )
		return 0;

	// Compile the code
	CompiledPythonFragment frag;
	frag.codeString = pCode;
	SIP_BLOCK_THREADS
	frag.code = (PyCodeObject*)Py_CompileString( pCode.toLatin1(), codeName.toLatin1(), Py_file_input );
	SIP_UNBLOCK_THREADS
	if( !frag.code )
	{
		PyErr_Print();
		LOG_5( "PathTemplate:getCompiledCode: Error Compiling Python code for: " + table->schema()->tableName() + " " + QString::number( pkey ) + " " + codeName );
		return 0;
	}

	// Load the code into a module
	// This is the only way i could figure out how to make the globals work properly
	// before i was using PyEval_EvalCode, passing the __main__ dict as the globals
	// but this wasn't working properly when calling the functions later, because
	// the import statements were lost.
	// This method works great and takes less code.
	
	// Generate a unique module name
	QString moduleName = "__" + table->schema()->tableName() + "__" + QString::number(pkey) + "__";
	// Returns a NEW ref
	SIP_BLOCK_THREADS
	PyObject * module = PyImport_ExecCodeModule(moduleName.toLatin1().data(),(PyObject*)frag.code);
	if( !module ) {
		PyErr_Print();
		LOG_3( "Unable to execute code module" );
	}

	// Save the modules dict, so we can lookup the function later
	else {
		frag.locals = PyModule_GetDict(module);
		Py_INCREF(frag.locals);
		codeCache[codeKey] = frag;
		ret = &codeCache[codeKey];
	}
	SIP_UNBLOCK_THREADS

	return ret;
}

CompiledPythonFragment * getCompiledCode( const Record & r, const QString & pCode, const QString & codeName )
{
	return getCompiledCode( r.table(), r.key(), pCode, codeName );
}

PyObject * getCompiledFunction( const char * functionName, Table * table, int pkey, const QString & pCode, const QString & codeName )
{
	CompiledPythonFragment * frag = getCompiledCode( table, pkey, pCode, codeName );
	if( !frag ) return 0;

	/// Returns a BORROWED reference
	PyObject * ret = 0;
	SIP_BLOCK_THREADS
	ret = PyDict_GetItemString( frag->locals, functionName );
	SIP_UNBLOCK_THREADS
	if( !ret ) {
		LOG_3( "getCompiledFunction: Unable to find function " + QString::fromLatin1(functionName) + " in code object" );
	}
	return ret;
}

PyObject * getCompiledFunction( const char * functionName, const Record & r, const QString & pCode, const QString & codeName )
{
	return getCompiledFunction( functionName, r.table(), r.key(), pCode, codeName );
}

static sipTypeDef * sipColorType()
{ static sipTypeDef * sColorW = 0; if( !sColorW ) sColorW = getSipType("PyQt4.QtGui", "QColor"); return sColorW; }

static sipTypeDef * sipImageType()
{ static sipTypeDef * sImageW = 0; if( !sImageW ) sImageW = getSipType("PyQt4.QtGui", "QImage"); return sImageW; }

static sipTypeDef * sipDateType()
{ static sipTypeDef * sDateW = 0; if( !sDateW ) sDateW = getSipType("PyQt4.QtCore","QDate"); return sDateW; }

static sipTypeDef * sipDateTimeType()
{ static sipTypeDef * sDateTimeW = 0; if( !sDateTimeW ) sDateTimeW = getSipType("PyQt4.QtCore","QDateTime"); return sDateTimeW; }

static sipTypeDef * sipTimeType()
{ static sipTypeDef * sTimeW = 0; if( !sTimeW ) sTimeW = getSipType("PyQt4.QtCore","QTime"); return sTimeW; }

static sipTypeDef * sipStringType()
{ static sipTypeDef * sStringW = 0; if( !sStringW ) sStringW = getSipType("PyQt4.QtCore","QString"); return sStringW; }

/*static sipTypeDef * sipQVariantType()
{ static sipTypeDef * sQVariantW = 0; if( !sQVariantW ) sQVariantW = getSipType("PyQt4.QtCore","QVariant"); return sQVariantW; }*/

static sipTypeDef * sipIntervalType()
{ static sipTypeDef * sIntervalW = 0; if( !sIntervalW ) sIntervalW = getSipType("blur.Stone", "Interval"); return sIntervalW; }

static sipTypeDef * sipQStringListType()
{ static sipTypeDef * sQStringListW = 0; if( !sQStringListW ) sQStringListW = getSipType("PyQt4.QtCore", "QStringList"); return sQStringListW; }

static sipTypeDef * sipStringMapType()
{ static sipTypeDef * sStringMapW = 0; if( !sStringMapW ) sStringMapW = getSipType("blur.Stone", "StringMap"); return sStringMapW; }

static sipTypeDef * sipIntListType()
{ static sipTypeDef * sIntListW = 0; if( !sIntListW ) sIntListW = getSipType("blur.Stone","IntList"); return sIntListW; }

static sipTypeDef * sipQVariantType()
{ static sipTypeDef * sQVariantW = 0; if( !sQVariantW ) sQVariantW = getSipType("blur.Stone","QVariant"); return sQVariantW; }

static sipTypeDef * sipQByteArrayType()
{ static sipTypeDef * sQByteArrayW = 0; if( !sQByteArrayW ) sQByteArrayW = getSipType("PyQt4.QtCore","QByteArray"); return sQByteArrayW; }

static sipTypeDef * sipRecordListType()
{ static sipTypeDef * sRecordListW = 0; if( !sRecordListW ) sRecordListW = getSipType("blur.Stone","RecordList"); return sRecordListW; }

sipTypeDef * getRecordType( const char * module, const char * className )
{
	/*
	QString hash = QString::fromLatin1(module) + "." + className;
	static QHash<QString,sipTypeDef*> sipTypeHash;
	
	QHash<QString,sipTypeDef*>::iterator it = sipTypeHash.find( hash );
	if( it != sipTypeHash.end() )
		return it.value();
	*/
	sipTypeDef * ret = getSipType(module,className);
	return ret;
}

// Returns a BORROWED reference
PyObject * getPythonClass( const char * moduleName, const char * typeName )
{
	PyObject * ret = 0;
	ensurePythonInitialized();
	// Return a NEW reference
	SIP_BLOCK_THREADS
	PyObject * module = PyImport_ImportModule( (char*)moduleName );
	if( !module ) {
		LOG_1( "getPythonRecordClass: Unable to load " + QString::fromLatin1(moduleName) + " module" );
	} else {

		// Returns a BORROWED reference
		PyObject * moduleDict = PyModule_GetDict( module );
		Py_DECREF(module);
		if( !moduleDict ) {
			LOG_1( "getPythonRecordClass: Unable to get dict for " + QString::fromLatin1(moduleName) + " module" );
		} else
			// Returns a BORROWED reference
			ret = PyDict_GetItemString( moduleDict, typeName );
	}
	SIP_UNBLOCK_THREADS
	return ret;
}

// Returns a BORROWED reference
PyObject * getPythonRecordClass()
{
	return getPythonClass( "blur.Stone", "Record" );
}

/*static PyObject * getPythonRecordListClass()
{
	return getPythonClass( "blur.Stone", "RecordList" );
}*/

PyObject * sipWrapRecord( Record * r, bool makeCopy, TableSchema * defaultType )
{
	PyObject * ret = 0;
	// First we convert to Record using sip methods
	static sipTypeDef * recordType = getRecordType( "blur.Stone", "Record" );
	sipTypeDef * type = recordType;
	if( type ) {
		if( makeCopy )
			ret = getSipAPI()->api_convert_from_new_type( new Record(*r), type, NULL );
		else {
			ret = getSipAPI()->api_convert_from_type( r, type, Py_None );
		}
	} else {
		LOG_1( "Stone.Record not found" );
		return 0;
	}

	Table * table = r->table();

	TableSchema * tableSchema = 0;
	
	if( table )
		tableSchema = table->schema();
	else if( defaultType )
		tableSchema = defaultType;
	else
		return ret;

	bool isErr = false;
	if( tableSchema ) {
		QString className = tableSchema->className();
	
		// Then we try to find the python class for the particular schema class type
		// from the desired module set using addSchemaCastModule
		// BORROWED ref
		PyObject * dict = getSchemaCastModule( tableSchema->schema() );
		if( dict ) {
			SIP_BLOCK_THREADS
			// BORROWED ref
			PyObject * klass = PyDict_GetItemString( dict, className.toLatin1().constData() );
			if( klass ) {
				PyObject * tuple = PyTuple_New(1);
				// Tuple now holds only ref to ret
				PyTuple_SET_ITEM( tuple, 0, ret );
				PyObject * result = PyObject_CallObject( klass, tuple );
				if( result ) {
					if( PyObject_IsInstance( result, klass ) == 1 ) {
						ret = result;
					} else {
						LOG_1( "Cast Ctor Result is not a subclass of " + className );
						Py_INCREF( ret );
						Py_DECREF( result );
					}
				} else {
					LOG_1( "Execution Failed, Error Was:\n" );
					PyErr_Print();
					isErr = true;
				}
				Py_DECREF( tuple );
			}
			SIP_UNBLOCK_THREADS
			if( isErr ) return 0;
		} //else LOG_1( "No cast module set for schema" );
	} else LOG_1( "Table has no schema" );
	return ret;
}

PyObject * sipWrapRecordList( RecordList * rl, bool makeCopy, TableSchema * defaultType, bool allowUpcasting )
{
	PyObject * ret = 0;
	static sipTypeDef * recordType = getRecordType( "blur.Stone", "RecordList" );
	// First we convert to Record using sip methods
	sipTypeDef * type = recordType;
	if( type ) {
		if( makeCopy )
			ret = getSipAPI()->api_convert_from_new_type( new RecordList(*rl), type, NULL );
		else
			ret = getSipAPI()->api_convert_from_type( rl, type, Py_None );
		if( !ret ) {
			PyErr_SetString(PyExc_RuntimeError,"Failed to wrap recordlist");
			return 0;
		}
	} else {
		LOG_1( "Unable to find RecordList type in blur.Stone module" );
		PyErr_SetString(PyExc_RuntimeError,"Unable to find RecordList type in blur.Stone module");
		return 0;
	}
	
	TableSchema * tableSchema = 0;
	if( allowUpcasting && !rl->isEmpty() ) { 
		Table * table = (*rl)[0].table();
		if( table ) tableSchema = table->schema();
		if( tableSchema ) {
			foreach( Record r, (*rl) ) {
				table = r.table();
				if( !table ) {
					tableSchema = 0;
					break;
				}
				TableSchema * ts = table->schema();
				if( ts != tableSchema && !tableSchema->isDescendant( ts ) ) {
					if( ts->isDescendant( tableSchema ) )
						tableSchema = ts;
					else {
						tableSchema = 0;
						break;
					}
				}
			}
		}
	}

	if( !tableSchema && defaultType )
		tableSchema = defaultType;

	if( !tableSchema ) return ret;

	QString className = tableSchema->className() + "List";
	
	// Then we try to find the python class for the particular schema class type
	// from the desired module set using addSchemaCastModule
	// BORROWED ref
	PyObject * dict = getSchemaCastModule( tableSchema->schema() );
	if( dict ) {
		bool isErr = false;
		SIP_BLOCK_THREADS
		// BORROWED ref
		PyObject * klass = PyDict_GetItemString( dict, className.toLatin1().constData() );
		if( klass ) {
			PyObject * tuple = PyTuple_New(1);
			PyTuple_SET_ITEM( tuple, 0, ret );
			PyObject * result = PyObject_CallObject( klass, tuple );
			if( result ) {
				if( PyObject_IsInstance( result, klass ) == 1 ) {
					// result is returned, the original ret is decref'ed in the tuple
					ret = result;
				} else {
					// Dont decref ret, it's owned by the tuple which is decref'ed below
					Py_DECREF( result );
				}
			} else{
				isErr = true;
			}
			Py_DECREF( tuple );
		}
		SIP_UNBLOCK_THREADS
		if( isErr )
			return 0;
	} //else LOG_1( "No cast module set for schema" );
	return ret;
}

RecordList recordListFromPyList( PyObject * a0, Table * table )
{
	SIP_SSIZE_T numItems = PySequence_Size(a0);
	RecordList ret;
	ret.reserve(numItems);
	
	sipTypeDef * qstringType = getSipType("","QString");
	PyObject * pyQStringType = (PyObject*)(qstringType->u.td_py_type);
	
	QStringList * sl = 0;
	QList<uint> * kl = 0;
	for (int i = 0; i < numItems; ++i)
	{
		PyObject * o = PySequence_GetItem(a0,i);
		if( PyString_Check(o) || PyUnicode_Check(o) || PyObject_IsInstance(o,pyQStringType) )
		{
			if( !sl )
				sl = new QStringList();
			
			int state, err = 0;
			QString * sp = (QString*)getSipAPI()->api_convert_to_type(o, qstringType, 0, 0, &state, &err);
			if( sp ) {
				(*sl) << *sp;
				getSipAPI()->api_release_type(sp, qstringType, state);
			}
			else {
				LOG_1( "unable to convert to QString: " + QString( PyString_AsString( PyObject_Repr(o) ) ) );
				(*sl) << QString();
			}
		}
		else if( PyLong_Check(o) || PyInt_Check(o) ) {
			if( !kl ) kl = new QList<uint>();
			*kl << PyInt_AsUnsignedLongMask(o);
		}
		else
			ret += sipUnwrapRecord(o);
	}
	if( sl ) {
		Index * idx = table ? table->defaultLookupIndex() : 0;
		if( !idx ) {
			LOG_1( "Strings passed to list constructor, but no default lookup index was found" );
		} else {
			RecordList rl = idx->recordsByIndex( QList<QVariant>() << QVariant(*sl) );
			if( ret.size() ) {
				RecordList rl2;
				rl2.reserve(numItems);
				int stringLookupIdx=0, recordListIdx=0;
				for (int i = 0; i < numItems; ++i) {
					PyObject * o = PyList_GET_ITEM(a0,i);
					if( PyString_Check(o) || PyUnicode_Check(o) || PyObject_IsInstance(o,pyQStringType) )
						rl2.append(rl[stringLookupIdx++]);
					else
						rl2.append(ret[recordListIdx++]);
				}
				ret = rl2;
			} else
				ret = rl;
		}
		delete sl;
	}
	if( kl && table ) {
		ret += table->records(*kl);
	}
	return ret;
}

PyObject * recordListGroupByCallable( const RecordList * rl, PyObject * callable, TableSchema * defaultType )
{
	PyObject *d = PyDict_New();

	if (!d)
		return NULL;
	
	bool error = false;
	static sipTypeDef * rl_type = getRecordType( "blur.Stone", "RecordList" );
	
	foreach( Record r, *rl ) {
		PyObject * py_r = sipWrapRecord( &r );
		
		// This should probably throw an exception
		if( !py_r )
			continue;
			
		// Tuple now holds only ref to py_r
		PyObject * tuple = PyTuple_New(1);
		PyTuple_SET_ITEM( tuple, 0, py_r );
		
		PyObject * key = PyObject_CallObject( callable, tuple );
		Py_DECREF(tuple);
		
		if( !key ) {
			error = true;
			break;
		}
		
		PyObject * entry = PyDict_GetItem( d, key );
		if( entry ) {
			if( getSipAPI()->api_can_convert_to_type(entry, rl_type, SIP_NOT_NONE | SIP_NO_CONVERTORS) ) {
				int isErr = 0;
				RecordList * rl = (RecordList*)getSipAPI()->api_convert_to_type( entry, rl_type, NULL, SIP_NOT_NONE | SIP_NO_CONVERTORS, 0, &isErr );
				if( !isErr )
					rl->append(r);
			}
		} else {
			RecordList rl(r);
			PyDict_SetItem( d, key, sipWrapRecordList( &rl, true, defaultType, /*allowUpcasting=*/ false ) );
		}
		Py_DECREF(key);
	}
	
	if( error ) {
		Py_DECREF(d);
		d = 0;
	}
	
	return d;
}

PyObject * recordListGroupByCallable( const RecordList * rl, PyObject * callable, PyObject * pyList )
{
	PyObject *d = PyDict_New();

	if (!d)
		return NULL;
	
	PyObject * listType = PyObject_Type(pyList);
	static sipTypeDef * rl_type = getRecordType( "blur.Stone", "RecordList" );

	bool error = false;
	foreach( Record r, *rl ) {
		PyObject * py_r = sipWrapRecord( &r );
		
		// This should probably throw an exception
		if( !py_r )
			continue;
			
		// Tuple now holds only ref to py_r
		PyObject * tuple = PyTuple_New(1);
		PyTuple_SET_ITEM( tuple, 0, py_r );
		
		PyObject * key = PyObject_CallObject( callable, tuple );
		Py_DECREF(tuple);
		
		if( !key ) {
			error = true;
			break;
		}

		PyObject * entry = PyDict_GetItem( d, key );
		if( !entry ) {
			entry = PyObject_CallObject(listType, 0);
			if( entry ) {
				PyDict_SetItem( d, key, entry );
				Py_DECREF(entry);
			}
		}

		if( entry && getSipAPI()->api_can_convert_to_type(entry, rl_type, SIP_NOT_NONE | SIP_NO_CONVERTORS) ) {
			int isErr = 0;
			RecordList * rl = (RecordList*)getSipAPI()->api_convert_to_type( entry, rl_type, NULL, SIP_NOT_NONE | SIP_NO_CONVERTORS, 0, &isErr );
			if( !isErr )
				rl->append(r);
		}
		Py_DECREF(key);
	}
	
	if( error ) {
		Py_DECREF(d);
		d = 0;
	}
	
	return d;
}

bool isPythonRecordInstance( PyObject * pyObject )
{
	bool ret = false;
	// Is this a class instance?
	SIP_BLOCK_THREADS
	ret = PyObject_IsInstance( pyObject, getPythonRecordClass() );
	SIP_UNBLOCK_THREADS
	return ret;
}

Record sipUnwrapRecord( PyObject * pyObject )
{
	static sipTypeDef * sipRecordType = getRecordType( "blur.Stone", "Record" );
	Record ret;
	SIP_BLOCK_THREADS
	if( getSipAPI()->api_can_convert_to_type( pyObject, sipRecordType, 0 ) ) {
		int state, err = 0;
		Record * r = (Record*)getSipAPI()->api_convert_to_type( pyObject, sipRecordType, 0, 0, &state, &err );
		if( !err )
			ret = *r;
		getSipAPI()->api_release_type( r, sipRecordType, state );
	}
	SIP_UNBLOCK_THREADS
	return ret;
	/*
	PyObject * recordClassObject = getPythonRecordClass();
	if( classObject && recordClassObject && sipRecordWrapper ) {
		SIP_BLOCK_THREADS
		if( PyClass_IsSubclass( classObject, recordClassObject ) ) {
			bool needRecordInstanceDeref = false;
			if( classObject != recordClassObject ) {
				PyObject * tuple = PyTuple_New(1);
				
				// tuple steals a ref
				Py_INCREF(pyObject);
				PyTuple_SetItem(tuple,0,pyObject);
				
				recordClassInstance = PyInstance_New( recordClassObject, tuple, 0 );
				Py_DECREF(tuple);
				needRecordInstanceDeref = true;
			} else
				recordClassInstance = pyObject;
	
			int err=0;
			if( recordClassInstance && getSipAPI()->api_can_convert_to_type( recordClassInstance, sipRecordWrapper, 0 ) ) {
				Record * r = (Record*)getSipAPI()->api_convert_to_type( recordClassInstance, sipRecordWrapper, 0, 0, 0, &err );
				ret = *r;
			}
			
			if( needRecordInstanceDeref && recordClassInstance )
				Py_DECREF( recordClassInstance );
		}
		SIP_UNBLOCK_THREADS
	}
	*/
}

template<class T> struct SipInstance
{
	T * t;
	const sipAPIDef * api;
	const sipTypeDef * td;
	int state, error;
	SipInstance(const sipAPIDef * _api, const sipTypeDef * _td, PyObject * pyObject, int flags = 0)
	: t(nullptr)
	, api(_api)
	, td(_td)
	, state(0)
	, error(0)
	{
		if (api->api_can_convert_to_type(pyObject, td, 0))
			t = (T*)api->api_convert_to_type(pyObject, td, nullptr, flags, &state, &error);
		if (error) {
			api->api_release_type(t, td, state);
			t = nullptr;
		}
	}
	~SipInstance() {
		if (t)
			api->api_release_type(t, td, state);
	}
	bool isValid() const { return t != nullptr; }
	T * operator->() { return t; }
	T operator*() { return *t; }
};

QVariant unwrapQVariant( PyObject * pyObject )
{
	if( !pyObject || pyObject == Py_None )
		return QVariant();
	
	const sipAPIDef * api = getSipAPI();

	{
		SipInstance<RecordList> sl(api, sipRecordListType(), pyObject, SIP_NO_CONVERTORS);
		if( sl.isValid() ) return qVariantFromValue( *sl );
	}
	
	/* PyQt4.QtCore.QVariant will convert these types to unwanted QVariantList/QVariantMap
	 * Instead check for conversion of these types first, then try the generic QVariant 
	 * conversion.  QStringList will accept any sequence so we only want to try these
	 * for non-strings */
	if(
			!PyString_Check(pyObject)
		&&	!PyUnicode_Check(pyObject)
		&&	!PyObject_IsInstance(pyObject, (PyObject*)sipStringType()->u.td_py_type)
		&&	!PyObject_IsInstance(pyObject, (PyObject*)sipQByteArrayType()->u.td_py_type)
	)
	{
		// StringList
		{
			SipInstance<QStringList> sl(api, sipQStringListType(), pyObject);
			if( sl.isValid() ) return *sl;
		}
		
		// StringMap
		{
			SipInstance<StringMap> sm(api, sipStringMapType(), pyObject);
			if( sm.isValid() ) return qVariantFromValue<StringMap>(*sm);
		}
		
		// IntList
		{
			SipInstance<IntList> il(api, sipIntListType(), pyObject);
			if( il.isValid() ) return qVariantFromValue<IntList>(*il);
		}
	}
	
	{
		SipInstance<QVariant> v(api, sipQVariantType(), pyObject);
		if( v.isValid() && qstrcmp(v->typeName(),"PyQt_PyObject") != 0 ) {
			return *v;
		}
	}
	
	if( PyObject_IsInstance(pyObject, getPythonRecordClass()) )
		return qVariantFromValue( sipUnwrapRecord( pyObject ) );
	
	{
		SipInstance<QColor> c(api, sipColorType(), pyObject);
		if( c.isValid() ) return *c;
	}
	
	{
		SipInstance<Interval> i(api, sipIntervalType(), pyObject);
		if( i.isValid() ) return qVariantFromValue<Interval>(*i);
	}
	
	{
		SipInstance<QImage> i(api, sipImageType(), pyObject);
		if( i.isValid() ) return *i;
	}

	LOG_1( "Unable to unwrap python object:" + pyObjectRepr( pyObject ) );
	return QVariant();
}

PyObject * wrapQVariant( const QVariant & var, bool stringAsPyString, bool noneOnFailure, int qvariantType )
{
	ensurePythonInitialized();
	PyObject * ret = 0;
	SIP_BLOCK_THREADS
	int qvt = var.userType();
	if( qvt == QVariant::Invalid )
		qvt = qvariantType;
	switch( qvt ) {
		case QVariant::Bool:
			ret = PyBool_FromLong( var.toBool() ? 1 : 0 );
			break;
		case QVariant::Color:
		{
			if( sipColorType() )
				ret = getSipAPI()->api_convert_from_new_type(new QColor(var.value<QColor>()), sipColorType(), 0);
			break;
		}
		case QVariant::Image:
		{
			if( sipImageType() )
				ret = getSipAPI()->api_convert_from_new_type(new QImage(var.value<QImage>()), sipImageType(), 0);
			break;
		}
		case QVariant::Date:
		{
			if( sipDateType() )
				ret = getSipAPI()->api_convert_from_new_type(new QDate(var.toDate()),sipDateType(),0);
			break;
		}
		case QVariant::DateTime:
		{
			if( sipDateTimeType() )
				ret = getSipAPI()->api_convert_from_new_type(new QDateTime(var.toDateTime()),sipDateTimeType(),0);
			break;
		}
		case QVariant::Double:
			ret = PyFloat_FromDouble( var.toDouble() );
			break;
		case QVariant::Int:
			ret = PyLong_FromLong(var.toInt());
			break;
		case QVariant::UInt:
			ret = PyLong_FromUnsignedLong(var.toUInt());
			break;
		case QVariant::LongLong:
			ret = PyLong_FromLongLong( var.toLongLong() );
			break;
		case QVariant::ULongLong:
			ret = PyLong_FromLongLong( var.toLongLong() );
			break;
		case QVariant::Time:
		{
			if( sipTimeType() )
				ret = getSipAPI()->api_convert_from_new_type(new QTime(var.toTime()),sipTimeType(),0);
			break;
		}
		case QVariant::String:
		{
			ret = pyUnicodeFromQString(var.toString());
			break;
		}
		case QVariant::StringList:
		{
			QStringList sl = var.toStringList();
			ret = PyList_New(0);
			foreach( QString s, sl ) {
				PyList_Append(ret, pyUnicodeFromQString(s));
			}
			break;
		}
		case QVariant::ByteArray:
		{
			QByteArray ba = var.toByteArray();
			ret = PyString_FromStringAndSize( ba.constData(), ba.size() );
			break;
		}
		default:
			break;
	}
	if( !ret ) {
		if( qvt == qMetaTypeId<Record>() ) {
			Record r = qvariant_cast<Record>(var);
			ret = sipWrapRecord( &r );
		}
		else if( qvt == qMetaTypeId<Interval>() )
			ret = getSipAPI()->api_convert_from_new_type(new Interval(qvariant_cast<Interval>(var)),sipIntervalType(),0);
		else if( qvt == qMetaTypeId<StringMap>() )
			ret = getSipAPI()->api_convert_from_new_type(new StringMap(qvariant_cast<StringMap>(var)),sipStringMapType(),0);
		else if( qvt == qMetaTypeId<IntList>() )
			ret = getSipAPI()->api_convert_from_new_type(new IntList(qvariant_cast<IntList>(var)),sipIntListType(),0);
	}
	if( !ret && noneOnFailure ) {
		ret = Py_None;
		Py_INCREF(ret);
	}
	SIP_UNBLOCK_THREADS
	return ret;
}

QString pyObjectRepr( PyObject * pyObject )
{
	QString ret;
	SIP_BLOCK_THREADS
	PyObject * repr =  PyObject_Repr( pyObject );
	if( repr ) {
		ret = unwrapQVariant( repr ).toString();
		Py_DECREF( repr );
	}
	SIP_UNBLOCK_THREADS
	return ret;
}

PyObject * variantListToTuple( const VarList & args, int qvariantType )
{
	ensurePythonInitialized();
	PyObject * tuple = 0;
	SIP_BLOCK_THREADS
	tuple = PyTuple_New(args.size());
	int i=0;
	foreach( QVariant arg, args )
		PyTuple_SET_ITEM( tuple, i++, wrapQVariant(arg,false,true,qvariantType) );
	SIP_UNBLOCK_THREADS
	return tuple;
}

PyObject * variantListToPyList( const VarList & args, int qvariantType )
{
	ensurePythonInitialized();
	PyObject * list = 0;
	SIP_BLOCK_THREADS
	list = PyList_New(args.size());
	int i=0;
	foreach( QVariant arg, args )
		PyList_SET_ITEM( list, i++, wrapQVariant(arg,false,true,qvariantType) );
	SIP_UNBLOCK_THREADS
	return list;
}

QVariant runPythonFunction( PyObject * callable, PyObject * tuple )
{
	QVariant ret;
	SIP_BLOCK_THREADS
	if( !callable || !PyCallable_Check(callable) ) {
		LOG_1( "runPythonFunction: callable is not a valid PyCallable object" );
	} else {

		PyObject * result = PyObject_CallObject( callable, tuple );
		Py_DECREF( tuple );
	
		if( result ) {
			ret = unwrapQVariant( result );
			Py_DECREF( result );
		} else {
			LOG_1( "runPythonFunction: Execution Failed, Error Was:\n" );
			PyErr_Print();
		}
	}
	SIP_UNBLOCK_THREADS
	return ret;
}

QVariant runPythonFunction( const QString & moduleName, const QString & functionName, const VarList & varArgs )
{
	QVariant ret;
	ensurePythonInitialized();
	// Returns a NEW reference
	SIP_BLOCK_THREADS
	PyObject * module = PyImport_ImportModule( moduleName.toLatin1().data() );
	if( !module ) {
		LOG_1( "Unable to load python module: " + moduleName );
	} else {

		// Returns a BORROWED reference
		PyObject * moduleDict = PyModule_GetDict( module );
		Py_DECREF( module );
		if( !moduleDict ) {
			LOG_1( "Unable to retrieve dict for module: " + moduleName );
		} else {
		
			// Returns a BORROWED reference
			PyObject * function = PyDict_GetItemString( moduleDict, functionName.toLatin1().data() );
			if( !function ) {
				LOG_1( "Unable to find function: " + functionName + " inside module " + moduleName );
			} else {
				if( !PyCallable_Check( function ) ) {
					LOG_1( moduleName + "." + functionName + " is not a callable object" );
				} else {
				
					// Returns a NEW reference
					PyObject * args = variantListToTuple( varArgs );
					// Steals the new reference from args
					ret = runPythonFunction( function, args );
				}
			}
		}
	}
	SIP_UNBLOCK_THREADS
	return ret;
}

struct SchemaCastModule {
	Schema * schema;
	PyObject * dict;
};

static QList<SchemaCastModule> schemaCastModuleList;

void addSchemaCastNamedModule( Schema * schema, const QString & moduleName )
{
	// NEW ref
	SIP_BLOCK_THREADS
	PyObject * module = PyImport_ImportModule( moduleName.toLatin1().data() );
	if( module ) {
		// BORROWED ref
		PyObject * moduleDict = PyModule_GetDict( module );
		if( moduleDict ) {
			addSchemaCastTypeDict( schema, moduleDict );
		} else LOG_1( "Unable to get dict for module: " + moduleName );
		Py_DECREF( module );
	} else LOG_1( "Unable to load module: " + moduleName );
	SIP_UNBLOCK_THREADS
}

void addSchemaCastTypeDict( Schema * schema, PyObject * dict )
{
	removeSchemaCastModule(schema);
	SchemaCastModule scm;
	scm.schema = schema;
	scm.dict = dict;
	SIP_BLOCK_THREADS
	Py_INCREF(dict);
	SIP_UNBLOCK_THREADS
	schemaCastModuleList += scm;
}

void removeSchemaCastModule( Schema * schema )
{
	for( int i = 0; i < schemaCastModuleList.size(); i++ )
		if( schemaCastModuleList[i].schema == schema ) {
			SchemaCastModule & scm = schemaCastModuleList[i];
			SIP_BLOCK_THREADS
			Py_DECREF(scm.dict);
			SIP_UNBLOCK_THREADS
			schemaCastModuleList.removeAt(i);
		}
}

PyObject * getSchemaCastModule( Schema * schema )
{
	for( int i = 0; i < schemaCastModuleList.size(); i++ ) {
		SchemaCastModule & scm = schemaCastModuleList[i];
		if( scm.schema == schema )
			return scm.dict;
	}
	return 0;
}

QString pythonExceptionTraceback( bool clearException )
{
	/*
		import traceback
		return '\n'.join(traceback.format_exc())
	*/
	QString ret;
	bool success = false;
	SIP_BLOCK_THREADS

	PyObject * type, * value, * traceback;
	/* Save the current exception */
	PyErr_Fetch(&type, &value, &traceback);
	if( type ) {
	
		PyObject * traceback_module = PyImport_ImportModule("traceback");
		if (traceback_module) {

			/* Call the traceback module's format_exception function, which returns a list */
			PyObject * traceback_list = PyObject_CallMethod(traceback_module, "format_exception", "OOO", type, value ? value : Py_None, traceback ? traceback : Py_None);
			if( traceback_list ) {

				PyObject * separator = PyUnicode_FromString("");
				if( separator ) {

					PyObject * retString = PyUnicode_Join(separator, traceback_list);
					if( retString ) {
						ret = unwrapQVariant(retString).toString();
						success = true;
						Py_DECREF(retString);

					} else
						ret = "PyUnicode_Join failed";

					Py_DECREF(separator);
				} else
					ret = "PyUnicode_FromString failed";

				Py_DECREF(traceback_list);
			} else
				ret = "Failure calling traceback.format_exception";

			Py_DECREF(traceback_module);
		} else
			ret = "Unable to load the traceback module, can't get exception text";
	} else
		ret = "pythonExceptionTraceback called, but no exception set";

	if( clearException ) {
		Py_DECREF(type);
		Py_XDECREF(value);
		Py_XDECREF(traceback);
	} else
		PyErr_Restore(type,value,traceback);

	SIP_UNBLOCK_THREADS

	// Ret is an error message if success is false
	if( !success )
		LOG_1( ret );

	return ret;
}

void printPythonStackTrace()
{
	SIP_BLOCK_THREADS
	PyObject * traceback_module = PyImport_ImportModule("traceback");
	if (traceback_module) {

		/* Call the traceback module's format_exception function, which returns a list */
		PyObject * ret = PyObject_CallMethod(traceback_module, "print_stack","");
		if( !ret ) {
			LOG_1( pythonExceptionTraceback(true) );
		}
		Py_XDECREF(ret);
		Py_DECREF(traceback_module);
	} else
		LOG_1( "Unable to load the traceback module, can't get exception text" );
	SIP_UNBLOCK_THREADS
}

QString pythonStackTrace()
{
	QString ret;
	SIP_BLOCK_THREADS
	PyObject * traceback_module = PyImport_ImportModule("traceback");
	if (traceback_module) {

		/* Call the traceback module's format_exception function, which returns a list */
		PyObject * stack = PyObject_CallMethod(traceback_module, "format_stack","");
		if( PyList_Check(stack) ) {
			PyObject * separator = PyUnicode_FromString("");
			if( separator ) {
				PyObject * retString = PyUnicode_Join(separator, stack);
				if( retString ) {
					ret = unwrapQVariant(retString).toString();
					Py_DECREF(retString);
				} else
					ret = "PyUnicode_Join failed";

				Py_DECREF(separator);
			} else
				ret = "PyUnicode_FromString failed";

			Py_DECREF(stack);
		} else {
			LOG_1( "traceback.format_stack returned unexpected value" );
		}
		Py_XDECREF(stack);
		Py_DECREF(traceback_module);
	} else
		LOG_1( "Unable to load the traceback module, can't get exception text" );
	
	SIP_UNBLOCK_THREADS
	return ret;
}

// Convert a QString to a Python Unicode object.
PyObject *pyUnicodeFromQString(const QString &qstr)
{
	const sipTypeDef * stringType = sipStringType();
	PyObject * qs = getSipAPI()->api_convert_from_new_type(new QString(qstr),stringType,0);
	
	if( !qs ) {
		PyErr_Format(PyExc_SystemError, "Failure to convert QString to Python Object\n" );
		return nullptr;
	}
	
	/* With sip v1 api for QString we'll have a wrapped QString, we can call __str__
	 * on it to get a python unicode object for all versions of sip/pyqt */
	if( PyObject_IsInstance(qs, (PyObject*)stringType->u.td_py_type) ) {
		PyObject * tmp = PyObject_CallMethod(qs, "__str__", nullptr);
		Py_DECREF(qs);
		return tmp;
	}
	
	return qs;
}

typedef struct _RecordMetaType
{
	sipWrapperType super;
	PyObject * schema;
	PyObject * modDict;
	bool built;
} RecordMetaType;

static PyObject * RecordMetaType_getattro(PyObject *o, PyObject *attr_name);
static int RecordMetaType_setattro(PyObject *o, PyObject *attr, PyObject *v);
static int RecordMetaType_init(RecordMetaType *self, PyObject *args, PyObject *kwds);
static void RecordMetaType_dealloc(RecordMetaType *self);
static PyObject * RecordMetaType_createTableType(PyObject *cls, PyObject *args);
static PyObject * RecordMetaType_build(PyObject *cls, PyObject *args);

static PyMethodDef RecordMetaType_methods[] =
{
	{"createTableType", RecordMetaType_createTableType, METH_VARARGS | METH_CLASS},
	{"build", RecordMetaType_build, METH_VARARGS},
	{0}
};

static PyTypeObject RecordMetaType_Type = {
	PyVarObject_HEAD_INIT(NULL, 0)
	"blur._Stone.RecordMetaType",  /* tp_name */
	sizeof (RecordMetaType),    /* tp_basicsize */
	0,              /* tp_itemsize */
	(destructor)RecordMetaType_dealloc,              /* tp_dealloc */
	0,              /* tp_print */
	0,              /* tp_getattr */
	0,              /* tp_setattr */
	0,              /* tp_reserved (Python v3), tp_compare (Python v2) */
	0,              /* tp_repr */
	0,              /* tp_as_number */
	0,              /* tp_as_sequence */
	0,              /* tp_as_mapping */
	0,              /* tp_hash */
	0,              /* tp_call */
	0,              /* tp_str */
	(getattrofunc)RecordMetaType_getattro,              /* tp_getattro */
	(setattrofunc)RecordMetaType_setattro,              /* tp_setattro */
	0,              /* tp_as_buffer */
	Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE | Py_TPFLAGS_HAVE_GC,  /* tp_flags */
	0,              /* tp_doc */
	0,              /* tp_traverse */
	0,              /* tp_clear */
	0,              /* tp_richcompare */
	0,              /* tp_weaklistoffset */
	0,              /* tp_iter */
	0,              /* tp_iternext */
	RecordMetaType_methods,              /* tp_methods */
	0,              /* tp_members */
	0,              /* tp_getset */
	0,              /* tp_base */
	0,              /* tp_dict */
	0,              /* tp_descr_get */
	0,              /* tp_descr_set */
	0,              /* tp_dictoffset */
	(initproc)RecordMetaType_init,              /* tp_init */
	0,              /* tp_alloc */
	0,              /* tp_new */
	0,              /* tp_free */
	0,              /* tp_is_gc */
	0,              /* tp_bases */
	0,              /* tp_mro */
	0,              /* tp_cache */
	0,              /* tp_subclasses */
	0,              /* tp_weaklist */
	0,              /* tp_del */
#if PY_VERSION_HEX >= 0x02060000
	0,              /* tp_version_tag */
#endif
#if PY_VERSION_HEX >= 0x03040000
	0,              /* tp_finalize */
#endif
};

static int RecordMetaType_init(RecordMetaType *self, PyObject *args, PyObject *kwds)
{
	self->built = false;
	/* Meta types receive the dict as the 3rd arg */
	PyObject * dict = PySequence_GetItem(args,2);
	self->schema = PyDict_GetItemString(dict,"Schema");
	Py_XINCREF(self->schema);
	self->modDict = PyDict_GetItemString(dict,"ClassDict");
	Py_XINCREF(self->modDict);
	return RecordMetaType_Type.tp_base->tp_init((PyObject*)self,args,kwds);
}

static void RecordMetaType_dealloc(RecordMetaType * rmt)
{
	Py_XDECREF(rmt->modDict);
	Py_XDECREF(rmt->schema);
	RecordMetaType_Type.tp_base->tp_dealloc((PyObject*)rmt);
}

static RecordMetaType * findGeneratedBase(RecordMetaType * rmt)
{
	if( rmt->schema ) return rmt;
	/* Subclass of a generated type, find the generated type */
	PyObject * mro = ((PyTypeObject*)rmt)->tp_mro;

	if( mro ) {
		assert(PyTuple_Check(mro));
		int n = PyTuple_GET_SIZE(mro);
		for (int i = 0; i < n; i++) {
			PyTypeObject * base = (PyTypeObject*)PyTuple_GET_ITEM(mro, i);
			if( PyType_IsSubtype(base->ob_type,&RecordMetaType_Type) && ((RecordMetaType*)base)->schema )
				return (RecordMetaType*)base;
		}
	}
	return 0;
}

const char * no_build_attrs [] = {
	"__mro__",
	"__reverse_access_fields__",
	"__class__",
	"__dict__",
	"__name__",
	"__bases__",
	"Schema",
	0
};

static bool isNoBuildAttr(const char * attr)
{
	for( const char ** nbaa = no_build_attrs; *nbaa; ++nbaa )
		if( !strcmp(*nbaa,attr) )
			return true;
	return false;
}

static PyObject * RecordMetaType_getattro(PyObject *o, PyObject *attr)
{
	RecordMetaType * rmt = findGeneratedBase((RecordMetaType*)o);
	if( rmt ) {
		const char * att_cstr = PyString_AsString(attr);
	
		if( !strcmp(att_cstr,"__built__") )
			return PyBool_FromLong(rmt->built ? 1 : 0);
		
		if( !strcmp(att_cstr,"Schema") ) {
			Py_INCREF(rmt->schema);
			return rmt->schema;
		}
		
		if( rmt->modDict && !strcmp(att_cstr,"ClassDict") ) {
			Py_INCREF(rmt->modDict);
			return rmt->modDict;
		}

		if( !rmt->built && !isNoBuildAttr(att_cstr) ) {
			//PySys_WriteStdout("Building due to attribute %s\n", att_cstr);
			PyObject * ret = PyObject_CallMethod((PyObject*)o->ob_type,"build","O",o);
			if( !ret ) return NULL;
			Py_DECREF(ret);
			ret = PyObject_GetAttr(o,attr);
			if( ret ) return ret;
		}
	}
	return RecordMetaType_Type.tp_base->tp_getattro(o,attr);
}

static int RecordMetaType_setattro(PyObject *o, PyObject *attr, PyObject *v)
{
	RecordMetaType * rmt = findGeneratedBase((RecordMetaType*)o);
	if( rmt ) {
		const char * att_cstr = PyString_AsString(attr);

		if( !strcmp(att_cstr,"__built__") ) {
			if( !PyBool_Check(v) ) {
				PyErr_Format(PyExc_TypeError,
					"__built__ name must be a bool, not '%.200s'",
					Py_TYPE(v)->tp_name);
				return -1;
			}
			RecordMetaType * rmt = (RecordMetaType*)o;
			rmt->built = v == Py_True;
			return 0;
		}
	}
	return RecordMetaType_Type.tp_base->tp_setattro(o,attr,v);
}

static void checkUpdateBases(PyObject * class_, PyObject * newBase)
{
	PyObject * oldBases = PyObject_GetAttrString(class_,"__bases__");
	if( PyTuple_GET_SIZE(oldBases) != 1 || PyTuple_GET_ITEM(oldBases,0) != newBase ) {
		PyObject * newBases = Py_BuildValue("(O)",newBase);
		PyObject_SetAttrString(class_, "__bases__", newBases);
		Py_DECREF(newBases);
	}
	Py_DECREF(oldBases);
}

static PyObject * createType( const QString & className, PyObject * modDict, PyObject * recordMeta, PyObject * parentType, PyObject * baseDict )
{
	PyObject * pyClassName = PyString_FromString(className.toLatin1().constData());
	PyObject * class_ = PyDict_GetItem(modDict,pyClassName);
	PyObject * tableType = NULL, * args = NULL;
	
	// Rearrange __bases__ on the parent type if needed
	if( class_ ) {
		checkUpdateBases(class_,parentType);
		parentType = class_;
	}
	
	// Create the actual class by calling the meta type
	args = Py_BuildValue("(O(O)O)", pyClassName, parentType, baseDict);
	tableType = PyObject_CallObject(recordMeta,args);

	// Insert class into the module dict
	PyDict_SetItem(modDict,pyClassName,tableType);

	Py_DECREF(tableType);
	Py_DECREF(args);
	Py_DECREF(pyClassName);
	
	return tableType;
}
	
PyObject * RecordMetaType_createTableType(PyObject * cls, PyObject * args)
{
	PyObject * tableSchemaObject, * modDict, * moduleName, * recordMeta, * recordListMeta, *recordType, *recordListType;
	if( !PyArg_UnpackTuple(args,"createTableType",7,7,&tableSchemaObject, &modDict, &moduleName, &recordMeta, &recordListMeta, &recordType, &recordListType) )
		return NULL;
	
	if( strcmp(tableSchemaObject->ob_type->tp_name,"TableSchema") != 0 ) {
		PyErr_Format(PyExc_TypeError, "First argument must be a TableSchema, got %s",tableSchemaObject->ob_type->tp_name );
		return 0;
	}
	
	if( !PyDict_Check(modDict) ) {
		PyErr_Format(PyExc_TypeError, "Second argument to createTableType must be a dict, got %s", modDict->ob_type->tp_name );
		return 0;
	}
	
	TableSchema * tableSchema = (TableSchema*)((sipSimpleWrapper*)tableSchemaObject)->data;
	
	PyObject * parentType = NULL;
	PyObject * parentListType = NULL;
	TableSchema * parent = tableSchema->parent();
	if( parent ) {
		QString parentName = parent->className();
		parentType = PyDict_GetItemString(modDict,parentName.toLatin1().constData());
		parentListType = PyDict_GetItemString(modDict,(parentName + "List").toLatin1().constData());
	}
	
	if( parentType == NULL )
		parentType = recordType;
	
	if( parentListType == NULL )
		parentListType = recordListType;
	
	PyObject * baseDict = PyDict_New();
	PyDict_SetItemString(baseDict,"Schema",tableSchemaObject);
	PyDict_SetItemString(baseDict,"ClassDict",modDict);
	if( moduleName != Py_None )
		PyDict_SetItemString(baseDict,"__module__",moduleName);
	
	QString className = tableSchema->className();
	
	return Py_BuildValue("(OO)",
		createType( className, modDict, recordMeta, parentType, baseDict ),
		createType( className + "List", modDict, recordListMeta, parentListType, baseDict ));
}

static bool check_do_build( PyObject * o )
{
	//PySys_WriteStdout( "Check Type: %s\n", PyString_AsString(PyObject_Repr(o)) );
	if( PyType_IsSubtype(o->ob_type,&RecordMetaType_Type) ) {
		RecordMetaType * rmt = (RecordMetaType*)o;
		//PySys_WriteStdout( "Check built: %s\n", rmt->built ? "true" : "false" );
		if( !rmt->built ) {
			rmt->built = true;
			PyObject * ret = PyObject_CallMethod( (PyObject*)o->ob_type, "do_build", "O", o );
			if( !ret ) return false;
			Py_DECREF(ret);
		}
	}
	return true;
}

static PyObject * RecordMetaType_build(PyObject *cls, PyObject *args)
{
	//PySys_WriteStdout( "Build class %s\n", ((PyTypeObject*)cls)->tp_name );
	PyObject * mro = PyObject_GetAttrString(cls,"__mro__");
	if( !mro || !PyTuple_Check(mro) ) {
		PyErr_Format(PyExc_TypeError, "Class %s has no valid __mro__\n", ((PyTypeObject*)cls)->tp_name );
		return 0;
	}
	for( int i=PyTuple_GET_SIZE(mro)-1, end=0; i>=end; --i )
		if( !check_do_build( PyTuple_GET_ITEM(mro,i) ) )
			return NULL;
	if( !check_do_build( cls ) )
		return NULL;
	return Py_None;
}


void recordmetatype_init(PyTypeObject * sipWrapperType_Type, PyObject * blurStoneModule)
{
	RecordMetaType_Type.tp_base = sipWrapperType_Type;
	
	if (PyType_Ready(&RecordMetaType_Type) < 0)
		Py_FatalError("blur.Stone: Failed to initialise RecordMetaType type");
	
	Py_INCREF(&RecordMetaType_Type);
	
	PyModule_AddObject(blurStoneModule, "RecordMetaType", (PyObject *)&RecordMetaType_Type);
}

