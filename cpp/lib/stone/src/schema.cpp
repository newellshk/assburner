
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of libstone.
 *
 * libstone is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * libstone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libstone; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qdatetime.h>
#include <qdom.h>
#include <qfile.h>
#include <QXmlStreamReader>

#include "path.h"
#include "schema.h"

namespace Stone {

Schema::~Schema()
{
	foreach( TableSchema * ts, tables() ) {
		ts->deref();
	}
}

TableSchemaList Schema::tables() const
{
	return mTablesByName.values();
}

TableSchema * Schema::tableByName( const QString & tableName ) const
{
	QMap<QString, TableSchema*>::const_iterator it = mTablesByName.find( tableName.toLower() );
	if( it != mTablesByName.end() )
		return it.value();
	return 0;
}

TableSchema * Schema::tableByClass( const QString & className ) const
{
	QMap<QString, TableSchema*>::const_iterator it = mTablesByClass.find( className );
	if( it != mTablesByClass.end() )
		return it.value();
	return 0;
}

void Schema::addTable( TableSchema * table )
{
	mTablesByName[table->tableName().toLower()] = table;
	mTablesByClass[table->className()] = table;
}

void Schema::removeTable( TableSchema * table, bool  )
{
	mTablesByName.remove( table->tableName().toLower() );
	if( mTablesByClass.remove( table->tableName() ) )
		table->deref();
}

void Schema::tableRenamed( TableSchema * table, const QString & oldName )
{
	mTablesByName.remove( oldName.toLower() );
	addTable( table );
}

void Schema::tableClassRenamed( TableSchema * table, const QString & oldName )
{
	mTablesByClass.remove( oldName );
	addTable( table );
}

//
// Loads the database table definitions from
// the 'schema' xml file
//
void parseTable( Schema * schema, QXmlStreamReader & xr, bool ignoreDocs, QList<TableSchema*> * parsed, bool verbose=false )
{
	static QLatin1String s_true("true"), s_false("false"), s_name("name"), s_className("className"), s_preload("preload"),
		s_projectPreload("projectPreload"), s_useCodeGen("useCodeGen"), s_docs("docs"), s_expireKeyCache("expireKeyCache"),
		s_parent("parent"), s_logEvents("logEvents"), s_column("column"), s_type("type"), s_fkey("fkey"), s_table("table"), 
		s_indexDeleteMode("indexDeleteMode"), s_indexName("indexName"), s_hasIndex("hasIndex"), s_indexUseCache("indexUseCache"), 
		s_pkey("pkey"), s_multi("multi"), s_methodName("methodName"), s_pluralMethodName("pluralMethodName"), s_displayName("displayName"),
		s_reverseAccess("reverseAccess"), s_defaultValue("defaultValue"), s_tableDisplayName("tableDisplayName"),
		s_noDefaultSelect("noDefaultSelect"), s_variable("variable"), s_useCache("useCache"), s_index("index"),
		s_defaultLookup("defaultLookup"), s_unique("unique"), s_reverseAccessMethodName("reverseAccessMethodName");
		
	QXmlStreamAttributes atts = xr.attributes();
	QString tableName = atts.value( s_name ).toString();
	QStringRef className = atts.value( s_className );
	
	bool newTable = false;
	TableSchema * ret = schema->tableByName( tableName );
	if( !ret ) {
		newTable = true;
		ret = new TableSchema( schema );
		ret->setTableName( tableName );
	}

	if( parsed ) *parsed << ret;
	
	if( verbose )
		LOG_5( "Database::parseTable: Created table " + ret->tableName() );

	ret->setClassName( className == tableName ? tableName : atts.value( s_className ).toString() );
	ret->setPreloadEnabled( atts.value( s_preload ) == s_true );
	ret->setProjectPreloadColumn( atts.value( s_projectPreload ).toString() );
	ret->setUseCodeGen( atts.value( s_useCodeGen ) == s_true );

	if( !ignoreDocs )
		ret->setDocs( atts.value( s_docs ).toString() );

	// On by default
	ret->setExpireKeyCache( atts.value( s_expireKeyCache ) != s_false );
	ret->setLogEvents( atts.value( s_logEvents ) != s_false );
	
	QString parent = atts.value( s_parent ).toString();
	TableSchema * parentSchema = schema->tableByClass( parent );
	if( parentSchema && parentSchema != ret->parent() )
		ret->setParent( parentSchema );

	if( verbose )
		LOG_5( "Adding new table: " + tableName + " className: " + ret->className() + (parentSchema ? (" parent: " + parentSchema->tableName()) : QString()) );
	
	// Iterator through the table's nodes
	while( xr.readNextStartElement() ) {
		// Parse a column
		if( xr.name() == s_column ) {
			atts = xr.attributes();
			Field::Type ct = Field::stringToType( atts.value( s_type ) );
			QString name = atts.value( s_name ).toString();
			if( verbose )
				LOG_5( "Found column: " + name );
			if( !name.isEmpty() ) {
				Field * f = newTable ? 0 : ret->field(name,/*silent=*/true);
				bool modify = bool(f);
				QXmlStreamAttributes fatts = xr.attributes();
				QStringRef fkeyTable, fkeyDelMode;

				bool hasIndex = atts.value(s_hasIndex) == s_true;
				bool unique = atts.value(s_unique) == s_true;
				bool useCache = atts.value(s_indexUseCache) == s_true;
				QStringRef indexName = atts.value(s_indexName);

				while( xr.readNextStartElement() ) {
					if( verbose )
						LOG_1( "Found element inside column: " + xr.name().toString() );
					if( xr.name() == s_fkey ) {
						QXmlStreamAttributes fkatts = xr.attributes();
						fkeyTable = fkatts.value(s_table);
						fkeyDelMode = fkatts.value(s_indexDeleteMode);
						
						const bool legacyIndex = fkatts.value(s_hasIndex) == s_true;
						if( legacyIndex ) {
							hasIndex = true;
							indexName = fkatts.value(s_indexName);
							useCache = fkatts.value(s_indexUseCache) == s_true;
							unique = fkatts.value(s_type) != s_multi;
						}
						
					} else {
						LOG_1( "Found unknown element: " + xr.name().toString() + " inside column definition" );
					}
					xr.skipCurrentElement();
				}

				if( !modify ) {
					if( verbose && !newTable )
						LOG_5( "Adding new column: " + tableName + "." + name );
					if( fatts.value( s_pkey ) == s_true )
						f = new Field( ret, name, Field::UInt, Field::PrimaryKey );
					else if( !fkeyTable.isNull() ) {
						f = new Field( ret, name, fkeyTable.toString(), Field::Flags((unique ? Field::Unique : Field::None) | Field::ForeignKey),
							hasIndex, Field::indexDeleteModeFromString( fkeyDelMode ) );
					} else {
						if( ct >= 0 )
							f = new Field( ret, name, ct );
						else
							LOG_1( "Couldn't find the correct type for field: " + name );
					}
				}
				if( f ) {
					f->setFlag( Field::TableDisplayName, atts.value(s_tableDisplayName) == s_true );
					f->setFlag( Field::NoDefaultSelect, atts.value(s_noDefaultSelect) == s_true );
					f->setFlag( Field::Unique, unique );
					if( !indexName.isEmpty() && hasIndex ) {
						f->setHasIndex( true, Field::indexDeleteModeFromString(fkeyDelMode) );
						f->index()->setName( indexName.toString() );
						f->index()->setUseCache( useCache );
						if( atts.value(s_defaultLookup) == s_true && f->type() == Field::String ) {
							f->setFlag( Field::DefaultLookup, true );
							f->index()->setDefaultTableLookup( true );
						}
					}

					QString methodName = atts.value(s_methodName).toString();
					if( !methodName.isEmpty() ) {
						if( verbose && modify && !f->methodName().isEmpty() && f->methodName() != methodName )
							LOG_5( QString("Changing %1.%2 methodName from %3 to %4").arg(ret->tableName()).arg(f->name()).arg(f->methodName()).arg(methodName) );
						f->setMethodName( methodName );
					}
					QString pluralMethodName = atts.value(s_pluralMethodName).toString();
					if( !pluralMethodName.isEmpty() ) {
						if( verbose && modify && !f->pluralMethodName().isEmpty() && f->pluralMethodName() != pluralMethodName )
							LOG_5( QString("Changing %1.%2 pluralMethodName from %3 to %4").arg(ret->tableName()).arg(f->name()).arg(f->pluralMethodName()).arg(pluralMethodName) );
						f->setPluralMethodName( pluralMethodName );
					}
					QString displayName = atts.value( s_displayName ).toString();
					if( !displayName.isEmpty() && f->displayName() != displayName ) {
						if( verbose && modify && !f->displayName().isEmpty()  )
							LOG_5( QString("Changing %1.%2 display name from %3 to %4").arg(ret->tableName()).arg(f->name()).arg(f->displayName()).arg(displayName) );
						f->setDisplayName( displayName );
					}
					if( f->flag(Field::ForeignKey) ) {
						bool ra = atts.value(s_reverseAccess) == s_true;
						if( verbose && modify && f->flag(Field::ReverseAccess) != ra )
							LOG_5( QString("Changing %1.%2 reverse access from %3 to %4").arg(ret->tableName()).arg(f->name()).arg(f->flag(Field::ReverseAccess)).arg(ra) );
						f->setFlag( Field::ReverseAccess, ra );
						QStringRef methodName = atts.value(s_reverseAccessMethodName);
						if (!methodName.isNull()) {
							if( verbose && modify && f->reverseAccessMethodName() != methodName )
								LOG_5( QString("Changing %1.%2 reverse access method name from %3 to %4")
									.arg(ret->tableName()).arg(f->name()).arg(f->reverseAccessMethodName()).arg(methodName.toString()) );
							f->setReverseAccessMethodName(methodName.toString());
						}
					}
					if( !ignoreDocs ) {
						QString docs = atts.value(s_docs).toString();
						if( verbose && modify && !docs.isEmpty() && f->docs() != docs )
							LOG_5( QString("Changing %1.%2 docs from %3 to %4").arg(ret->tableName()).arg(f->name()).arg(f->docs()).arg(docs) );
						f->setDocs( docs );
					}
					QString defValS = atts.value(s_defaultValue).toString();
					if( !defValS.isEmpty() ) {
						QVariant defVal = Field::variantFromString( defValS, f->type() );
						if( verbose && modify && defVal != f->defaultValue() )
							LOG_5( QString("Changing %1.%2 default value from %3 to %4").arg(ret->tableName()).arg(f->name()).arg(f->defaultValue().toString()).arg(defVal.toString()) );
						f->setDefaultValue( defVal );
					}
					
				}
			} else
				xr.skipCurrentElement();
		}
		else if( xr.name() == s_variable ) {
			atts = xr.attributes();
			Field::Type ct = Field::stringToType( atts.value( s_type ).toString() );
			QString name = atts.value( s_name ).toString();
			if( !name.isEmpty() && !ret->field( name ) )
				new Field( ret, name, ct, Field::LocalVariable );
			xr.skipCurrentElement();
		}
		// Parse an index
		else if( xr.name() == s_index ) {
			atts = xr.attributes();
			QString name = atts.value( s_name ).toString();
			if( verbose )
				LOG_5( "Creating index: " + name );
			bool multi = atts.value( s_type ) == s_multi;
			IndexSchema * idx = ret->index(name);
			FieldList newFields;
			bool existingIndex = bool(idx), useCache(atts.value(s_useCache) == s_true);
			if( !idx )
				idx = new IndexSchema( name, ret, multi );
			if( atts.value(s_defaultLookup) == s_true )
				idx->setDefaultTableLookup( true );
			while( xr.readNextStartElement() ) {
				// try to convert the node to an element.
				if( xr.name() == s_column ) {
					QXmlStreamAttributes catts = xr.attributes();
					QString colName( catts.value( s_name ).toString() );
					Field * f = ret->field( colName );
					if( f )
						newFields.append(f);
					else
						LOG_1( "ERROR: Column " + colName + " not found in table " + tableName + " for index " + name );
				} else {
					LOG_1( "Found unkown element: " + xr.name().toString() + " while parsing index element" );
				}
				xr.skipCurrentElement();
			}
			if( existingIndex ) {
				if( newFields != idx->columns() )
					LOG_1( "ERROR: Index " + name + " in table " + tableName + " redefined with different list of fields" );
			} else {
				foreach( Field * f, newFields )
					idx->addField( f );
			}
			idx->setUseCache( useCache );
		} else {
			LOG_1( "Found unknown element: " + xr.name().toString() + " while parsing table element" );
			xr.skipCurrentElement();
		}
	}
}

bool Schema::mergeXmlSchema( const QString & schema, bool isFile, bool ignoreDocs, QList<TableSchema*> * tables, bool verbose )
{
	static QLatin1String s_schemaName("schemaName"), s_table("table"), s_parent("parent"), s_name("name");
	// Tables that we have deferred processing until their
	// parent tables have been processed
	QFile file;
	QMap<QString,QList<QDomElement> > parseAfterParent;
	
	QXmlStreamReader xr;
	
	QString errorMessage;

	if( isFile ) {
		if( verbose )
			LOG_1( "Merging schema: " + schema );
		
		file.setFileName(schema);
		if ( !file.open( QIODevice::ReadOnly ) ) {
			LOG_1( "Couldn't Open File (" + schema + ")" );
			return false;
		}
		
		xr.setDevice(&file);
	} else {
		xr.addData(schema);
	}
	
	if( xr.readNextStartElement() )
	{
		{
			QStringRef sn = xr.attributes().value(s_schemaName);
			setName( sn.isEmpty() ? "CLASSES" : sn.toString() );
		}
		
		while( xr.readNextStartElement() ) {
			if( xr.name() == s_table ) {
				//qWarning( "Found table " + table.attribute( "name" ) );
				QStringRef parent = xr.attributes().value( s_parent );
				
				// If we haven't parsed the parent yet, then add it to the map
				if( !parent.isNull() && !tableByName( parent.toString() ) ) {
					QStringRef name = xr.attributes().value( s_name );
					TableSchema * ret = tableByName( name.toString() );
					if( !ret ) {
						//newTable = true;
						ret = new TableSchema( this );
						ret->setTableName( name.toString() );
					}

					if( tables ) *tables << ret;
				}
				parseTable( this, xr, ignoreDocs, tables, verbose );
			} else {
				LOG_1( "Unknown element in schema: " + xr.name().toString() );
				xr.skipCurrentElement();
			}
		}
	} else {
		LOG_1( "No Element Read!" );
	}
	
	return true;
}

Schema * Schema::createFromXmlSchema( const QString & xmlSchema, bool isFile, bool ignoreDocs, bool verbose )
{
	Schema * ret = new Schema();
	if( !ret->mergeXmlSchema( xmlSchema, isFile, ignoreDocs, 0, verbose ) ) {
		delete ret;
		return 0;
	}
	return ret;
}

bool Schema::writeXmlSchema( const QString & outputFile, QString * xmlOut, TableSchemaList tables ) const
{
	// Create the root
	QString output;
	QXmlStreamWriter xw(&output);
	
	xw.setAutoFormatting(true);
	xw.writeStartDocument();
	xw.writeStartElement( "database" );
	xw.writeAttribute("schemaName", name());
	
	if( tables.isEmpty() )
		tables = mTablesByName.values();
	
	for( int i = 0; i < tables.size(); ) {
		TableSchema * t = tables[i];
		if( t->parent() ) {
			int pi = tables.indexOf(t->parent());
			if( pi > i ) {
				tables[pi] = t;
				tables[i] = t->parent();
				continue;
			}
		}
		++i;
	}
	
	foreach( TableSchema * tbl, tables )
	{
		xw.writeStartElement("table");
		
		xw.writeAttribute( "name", tbl->tableName() );
		xw.writeAttribute( "className", tbl->className() );
		
		if( tbl->useCodeGen() )
			xw.writeAttribute( "useCodeGen", "true" );
			
		if( tbl->isPreloadEnabled() )
			xw.writeAttribute( "preload", "true" );
		
		if( !tbl->projectPreloadColumn().isEmpty() )
			xw.writeAttribute( "projectPreload", tbl->projectPreloadColumn() );
			
		if( tbl->parent() )
			xw.writeAttribute( "parent", tbl->parent()->className() );
		
		if( !tbl->docs().isEmpty() )
			xw.writeAttribute( "docs", tbl->docs() );

		xw.writeAttribute( "expireKeyCache", tbl->expireKeyCache() ? "true" : "false" );
		xw.writeAttribute( "logEvents", tbl->logEvents() ? "true" : "false" );

		IndexSchemaList fieldIndexes;
		FieldList columns = tbl->ownedFields();
		for( FieldIter col_it = columns.begin(); col_it != columns.end(); ++col_it )
		{
			Field * f = *col_it;
			
			if( f->flag(Field::LocalVariable) ) {
				xw.writeStartElement( "variable" );
				xw.writeAttribute( "name", f->name() );
				xw.writeAttribute( "type", f->typeString() );
				xw.writeEndElement();
				continue;
			}
			
			xw.writeStartElement( "column" );
			
			xw.writeAttribute( "name", f->name() );
			if( f->displayName() != f->generatedDisplayName() )
				xw.writeAttribute( "displayName", f->displayName() );
			xw.writeAttribute( "type", f->typeString() );
			xw.writeAttribute( "methodName", f->methodName() );
			if( f->pluralMethodName() != f->generatedPluralMethodName() )
				xw.writeAttribute( "pluralMethodName", f->pluralMethodName() );

			if( !f->defaultValue().isNull() ) {
				QString valString;
				if( f->type() == Field::Date )
					valString = f->defaultValue().toDate().toString( Qt::ISODate );
				else if( f->type() == Field::DateTime )
					valString = f->defaultValue().toDateTime().toString( Qt::ISODate );
				else
					valString = f->defaultValue().toString();
				xw.writeAttribute( "defaultValue", valString );
			}

			if( f->flag( Field::PrimaryKey ) )
				xw.writeAttribute( "pkey", "true" );
			if( f->flag( Field::ReverseAccess ) ) {
				xw.writeAttribute( "reverseAccess", "true" );
				if( f->reverseAccessMethodName() != f->defaultReverseAccessMethodName() )
					xw.writeAttribute( "reverseAccessMethodName", f->reverseAccessMethodName() );
			}
			if( f->flag( Field::TableDisplayName ) )
				xw.writeAttribute( "tableDisplayName", "true" );
			if( f->flag( Field::NoDefaultSelect ) )
				xw.writeAttribute( "noDefaultSelect", "true" );
			if( f->flag( Field::DefaultLookup) )
				xw.writeAttribute( "defaultLookup", "true" );
			if( f->flag(Field::Unique) )
				xw.writeAttribute( "unique", "true" );
			if( f->hasIndex() ) {
				xw.writeAttribute( "hasIndex","true" );
				xw.writeAttribute( "indexName", f->index()->name() );
				xw.writeAttribute( "indexUseCache", f->index()->useCache() ? "true" : "false" );
				fieldIndexes += f->index();
			}
			if( !f->docs().isEmpty() )
				xw.writeAttribute( "docs", f->docs() );

			if( f->flag( Field::ForeignKey ) ) {
				xw.writeStartElement( "fkey" );
				xw.writeAttribute( "table", f->foreignKey() );
				xw.writeAttribute( "indexDeleteMode", f->indexDeleteModeString() );
				xw.writeEndElement();
			}
			xw.writeEndElement();
		}
		
		IndexSchemaList idxs = tbl->indexes();
		foreach( IndexSchema * idx, idxs )
		{
			FieldList fl = idx->columns();
			
			// Skip the empty keyindex
			if( fl.isEmpty() || (idx->field() && idx->field()->flag( Field::PrimaryKey )) )
				continue;
				
			// Skip the indexes that are owned by an fkey field
			if( fieldIndexes.contains( idx ) )
				continue;
				
			xw.writeStartElement( "index" );
			
			xw.writeAttribute( "type", idx->holdsList() ? "multi" : "single" );
			xw.writeAttribute( "name", idx->name() );
			xw.writeAttribute( "useCache", idx->useCache() ? "true" : "false" );
			if( idx->defaultTableLookup() )
				xw.writeAttribute( "defaultLookup", "true" );
			
			for( FieldIter ci_it = fl.begin(); ci_it != fl.end(); ++ci_it ) {
				xw.writeStartElement( "column" );
				xw.writeAttribute( "name", (*ci_it)->name() );
				xw.writeEndElement();
			}
			xw.writeEndElement();
		}
		xw.writeEndElement();
	}

	xw.writeEndElement();
	xw.writeEndDocument();

	if( !outputFile.isEmpty() ) {
		if( !writeFullFile( outputFile, output ) ) {
			LOG_1( "Unable to save xml schema to " + outputFile );
			return false;
		}
	}
	
	
	if( xmlOut )
		*xmlOut = output;
		
	return true;
}

QString Schema::name() const
{
	return mName;
}

void Schema::setName(const QString & name)
{
	mName = name;	
}

QString Schema::diff( Schema * before, Schema * after )
{
	QString ret;
	foreach( TableSchema * before_table, before->tables() ) {
		TableSchema * after_table = after->tableByName( before_table->tableName() );
		if( !after_table ) {
			ret += "Removed Table " + before_table->className() + "\n";
			continue;
		}
		ret += before_table->diff( after_table );
	}
	foreach( TableSchema * after_table, after->tables() ) {
		TableSchema * before_table = before->tableByName( after_table->tableName() );
		if( !before_table ) {
			ret += "Added Table " + after_table->className() + "\n";
			continue;
		}
	}
	return ret;
}

} // namespace