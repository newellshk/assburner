
#include <qstringlist.h>

#include "blurqt.h"
#include "stringmap.h"

StringMap::StringMap()
: Base() {}

StringMap::StringMap(const QMap<QString,QString> & o)
: Base(o) {}

StringMap::~StringMap()
{}

StringMap StringMap::fromString( const QString & s, QString * errorMessage )
{
	enum State { BeforeKey, Key, KeyQuoted, AfterKey, ExpectGT, BeforeValue, Value, ValueQuoted, AfterValue, Error };
	const char s_quote = '"', s_escape='\\', s_equals='=', s_gt='>', s_comma=',';
	State state = BeforeKey;
	int start = 0, i = 0;
	bool escaped = false;
	QString key;
	StringMap ret;
	QChar c;
	for( int e=s.size(); i<e && (state != Error); ++i ) {
		c = s[i];
		switch(state) {
			case BeforeKey:
				if( c==s_quote ) {
					start = i+1;
					state = KeyQuoted;
				}
				else if( !c.isSpace() ) {
					start = i;
					state = Key;
				}
				break;
			case Key:
				if( c.isSpace() ) {
					key = s.mid(start,i-start);
					state = AfterKey;
				} else if( c == s_equals ) {
					key = s.mid(start,i-start);
					state = ExpectGT;
				}
				break;
			case KeyQuoted:
				if( !escaped && c==s_quote ) {
					key = s.mid(start,i-start);
					state = AfterKey;
					start = i+1;
					escaped = false;
				} else if( c == s_escape )
					escaped = !escaped;
				else
					escaped = false;
				break;
			case AfterKey:
				if( c == s_equals )
					state = ExpectGT;
				else if( !c.isSpace() ) {
					state = Error;
				}
				break;
			case ExpectGT:
				if( c == s_gt )
					state = BeforeValue;
				else
					state = Error;
				break;
			case BeforeValue:
				if( c == s_quote ) {
					start = i+1;
					state = ValueQuoted;
				}
				else if( !c.isSpace() ) {
					start = i;
					state = Value;
				}
				break;
			case Value:
				if( c.isSpace() || c == s_comma ) {
					QString val = s.mid(start,i-start);
					if( val.size() == 4 && !val.compare("null",Qt::CaseInsensitive) )
						ret.insert( key, QString() );
					else
						ret.insert( key, val );
					state = c == s_comma ? BeforeKey : AfterValue;
				}
				if( (c == 'N' || c == 'n') && s.mid(i,4).toLower() == "null" ) {
					ret.insert( key, QString() );
					state = AfterValue;
					i += 3;
					start = i+1;
				}
				break;
			case ValueQuoted:
				if( !escaped && c==s_quote ) {
					QString val = s.mid(start,i-start);
					ret.insert( key, val );
					state = AfterValue;
					start = i+1;
				} else if( c == s_escape )
					escaped = !escaped;
				else
					escaped = false;
				break;
			case AfterValue:
				if( c == s_comma ) {
					state = BeforeKey;
				} else if( !c.isSpace() ) {
					state = Error;
				}
				break;
			case Error:
				break;
		}
	}
	
	switch(state) {
		case Value:
		{
			QString val = s.mid(start,i-start);
			if( val.size() == 4 && !val.compare("null",Qt::CaseInsensitive) )
				ret.insert( key, QString() );
			else
				ret.insert( key, val );
		}
			// fallthrough
		case BeforeKey:
		case AfterValue:
			// Valid exit states
			break;
		
		case Key:
		case KeyQuoted:
		case AfterKey:
		case ExpectGT:
		case BeforeValue:
		case ValueQuoted:
			if(errorMessage) *errorMessage = "Unexpected end of string";
			break;
		case Error:
			if(errorMessage) *errorMessage = QString("Syntax error near '%1' at position %2").arg(c).arg(i);
			break;
	}
	
	return ret;
}

static QString quoteAndEscape( const QString & s, bool canBeNull )
{ 
	Q_UNUSED(canBeNull);
	static QString s_null("NULL");
	return s.isNull() ? s_null : "\"" + QString(s).replace("\"","\\\"") + "\""; }

QString StringMap::toString() const
{
	QStringList ret;
	for( StringMap::const_iterator it = begin(), e = end(); it != e; ++it )
		ret.append( quoteAndEscape(it.key(),false) + "=>" + quoteAndEscape(it.value(),true) );
	return ret.join(", ");
}

