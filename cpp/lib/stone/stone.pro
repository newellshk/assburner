
TARGET=stone
TEMPLATE=lib

include(svnrev.pri)
include($$(PRI_SHARED)/common.pri)
include($$(PRI_SHARED)/python.pri)
include($$(PRI_SHARED)/qjson.pri)

SOURCES += \
	src/blurqt.cpp \
	src/changeset.cpp \
	src/connection.cpp \
	src/dateutil.cpp \
	src/database.cpp \
	src/expression.cpp \
	src/field.cpp \
	src/freezercore.cpp \
	src/graphite.cpp \
	src/index.cpp \
	src/indexschema.cpp \
	src/iniconfig.cpp \
	src/interval.cpp \
	src/intlist.cpp \
	src/joinedselect.cpp \
	src/os.cpp \
	src/path.cpp \
	src/packetsocket.cpp \
	src/pgconnection.cpp \
	src/process.cpp \
	src/process_win.cpp \
	src/pyembed.cpp \
	src/quickdatastream.cpp \
	src/recentvalues.cpp \
	src/record.cpp \
	src/recordlist.cpp \
	src/recordimp.cpp \
	src/recordproxy.cpp \
	src/recordxml.cpp \
	src/resultset.cpp \
	src/remotelogserver.cpp \
	src/schema.cpp \
	src/stringmap.cpp \
	src/sqlerrorhandler.cpp \
	src/sqliteconnection.cpp \
	src/table.cpp \
	src/tableschema.cpp \
	src/multilog.cpp \
	src/md5_globalstuff.cpp \
	src/md5.cpp \
	src/bt.cpp

	
HEADERS += \
	include/blurqt.h \
	include/changeset.h \
	include/connection.h \
	include/dateutil.h \
	include/database.h \
	include/expression.h \
	include/expression_p.h \
	include/expressionindex.h \
	include/field.h \
	include/freezercore.h \
	include/graphite.h \
	include/graphite_p.h \
	include/index.h \
	include/indexschema.h \
	include/iniconfig.h \
	include/interval.h \
	include/intlist.h \
	include/joinedselect.h \
	include/os.h \
	include/path.h \
	include/packetsocket.h \
	include/pgconnection.h \
	include/process.h \
	include/pyembed.h \
	include/quickdatastream.h \
	include/recentvalues.h \
	include/record.h \
	include/recordimp.h \
	include/recordlist.h \
	include/recordproxy.h \
	include/recordxml.h \
	include/remotelogserver.h \
	include/resultset.h \
	include/schema.h \
	include/sqlerrorhandler.h \
	include/sqliteconnection.h \
	include/stringmap.h \
	include/table.h \
	include/tableschema.h \
	include/trigger.h \
	include/multilog.h \
	include/md5_bithelp.h \
	include/md5_globalstuff.h \ 
	include/md5.h
	
INCLUDEPATH+=include src .out

DEPENDPATH+=src include

DEFINES+=QT_USE_QSTRINGBUILDER

DEFINES+=STONE_MAKE_DLL

win32 {
	LIBS+=-lPsapi -lMpr -ladvapi32 -lshell32 -luser32 -lpdh -lUserenv -lnetapi32 -lGdi32
	LIBS+=-Lc:/IntelLib
	#QMAKE_CXXFLAGS+=/Z7
	#QMAKE_LFLAGS+=/DEBUG
}

unix {
	INCLUDEPATH += ../sip/siplib/
}

INSTALLS += target
CONFIG+=qt thread exceptions rtti
QT+=sql xml network widgets

DESTDIR=./
