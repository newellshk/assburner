
from blur.build import *

path = os.path.dirname(os.path.abspath(__file__))
rp = lambda dirName: os.path.join(path,dirName)

allTests = [
	QTestTarget("stone_tests",path,'tests.pro'),
	QTestTarget('stone_test_field',rp('field'),'field.pro')
]

Target('stone_tests',path,allTests)

allTests = [
	QTestTarget('pystone_test_intervals',rp('interval'),'interval_test.py'),
	QTestTarget('pystone_test_dtt',rp('dtt'),'dtt.py')
]

Target('pystone_tests',path,allTests)
