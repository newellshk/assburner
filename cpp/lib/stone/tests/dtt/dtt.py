
# Dynamic Table Types tests

import sys, os
from blur.Stone import *
from PyQt4.QtCore import *
from PyQt4.QtGui import QImage, QColor

app = QCoreApplication([])
initConfig('dtt.ini')
schema = Schema()
createPythonTypesFromSchema( 'schema.xml', sys.modules[__name__], schema )
Database.setCurrent( Database( schema ) )

class Tester(object):
	def __init__(self):
		self.Passes = self.Fails = 0
	
	def __del__(self):
		print('Totals: %i passed, %i fails' % (self.Passes, self.Fails))
	
	def compare( self, test, actual, expected ):
		if expected == actual:
			print 'PASS:', test
			self.Passes += 1
		else:
			print 'FAIL:', test, 'expected:', str(expected), 'actual:', str(actual)
			self.Fails += 1
	

t = Tester()
compare = t.compare

a = TestA()

val = True
a.setTestBool( val )
compare( 'Bool', a.testBool(), val )

val = -1
a.setTestInt( val )
compare( 'Int', a.testInt(), val )

val = 1
a.setTestUInt( val )
compare( 'UInt', a.testUInt(), val )

val = sys.maxint
a.setTestULongLong(val)
compare( 'ULongLong', a.testULongLong(), val )

val = 'test'
a.setTestString(val)
compare( 'String', a.testString(), val )

val = QDate( 2000, 1, 1 )
a.setTestDate(val)
compare( 'Date', a.testDate(), val )

val = QDateTime( val, QTime( 12, 0 ) )
a.setTestDateTime(val)
compare( 'DateTime', a.testDateTime(), val )

val = val.time()
a.setTestTime(val)
compare( 'Time', a.testTime(), val )

val = Interval(60)
a.setTestInterval( val )
compare( 'Interval', a.testInterval(), val )

val = QByteArray('abc')
a.setTestByteArray( val )
compare( 'ByteArray', a.testByteArray(), val )

val = QColor( 0, 100, 255 )
a.setTestColor( val )
compare( 'Color', a.testColor(), val )

val = ['one','two']
a.setTestStringArray( val )
print a.dump()
compare( 'StringArray', a.testStringArray(), val )

val = {'a':'b"\\'}
a.setTestStringMap( val )
compare( 'StringMap', a.testStringMap(), val )

val = [1,2]
a.setTestIntArray( val )
compare( 'IntArray', a.testIntArray(), val )

