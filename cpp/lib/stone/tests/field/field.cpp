
#include <QtTest/QtTest>
#include <qsqlerror.h>

#include "field.h"

class FieldTest : public QObject
{
    Q_OBJECT
private slots:
	void parseStringArray_data();
	void parseStringArray();
};

void FieldTest::parseStringArray_data()
{
	QTest::addColumn<QString>("string");
	QTest::addColumn<QStringList>("result");
	QTest::addColumn<bool>("expectValid");
	
	QTest::newRow("empty1") << "{}" << QStringList() << true;
	QTest::newRow("empty2") << "{ \n\t}" << QStringList() << true;
	QTest::newRow("empty_invalid") << "{,}" << QStringList() << false;
	QTest::newRow("one") << "{a}" << QStringList("a") << true;
	QTest::newRow("two") << "{a,b}" << (QStringList("a") << "b") << true;
	QTest::newRow("three") << "{a,bb,ccc}" << (QStringList("a") << "bb" << "ccc") << true;
	QTest::newRow("null") << "{null,\"null\"}" << (QStringList() << QString() << "null") << true;
	QTest::newRow("empty") << "{\"\",\"\"}" << (QStringList() << "" << "") << true;
	
}

void FieldTest::parseStringArray()
{
	QFETCH(QString, string);
	QFETCH(QStringList, result);
	QFETCH(bool, expectValid);
	
	bool valid;
	QCOMPARE(::parseStringArray(string,&valid), result);
	QCOMPARE(valid, expectValid);
}


QTEST_MAIN(FieldTest)
#include "field.moc"
