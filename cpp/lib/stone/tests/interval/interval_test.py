
from blur.quickinit import *

passes, fails = 0, 0

def testIntervalS( ins, out = None ):
	global passes, fails
	out = out or ins
	result = Interval.fromString(ins)[0].toString()
	if( result == out ):
		print 'PASS:', ins, '\t\t', result
		passes += 1
	else:
		print 'FAIL: ', ins, 'incorrect result was:', '"%s"' % result, 'expected:', '"%s"' % out
		fails += 1

def testInterval( ins, out ):
	return testIntervalS( ins.toString(), out )

def ifs( s ):
	return Interval.fromString(s)[0]

testIntervalS( '1 millennium', '1000 years' )
testIntervalS( '1.2 centuries', '120 years' )
testIntervalS( '1.6 decades', '16 years' )
testIntervalS( '1.5 years', '1 year 6 mons' )
testIntervalS( '13 months', '1 year 1 mon' )
testIntervalS( '1.5 months', '1 mon 15 days' )
testIntervalS( '1.61 months', '1 mon 18 days 07:12:00' )
testIntervalS( '1:1', '01:01:00' )
testIntervalS( '1.5:1:1', '1 day 13:01:00' )
testIntervalS( '-1 month -1 day -12:00:00', '-1 mon -1 day -12:00:00' )
testInterval( ifs('1 day') / 2, '12:00:00' )
testIntervalS( '.035 seconds', '00:00:00.035' )
testIntervalS( '32 milliseconds', '00:00:00.032' )

print('Totals: %i passed, %i fails' % (passes, fails))
