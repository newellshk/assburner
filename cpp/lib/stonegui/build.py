
from blur.build import *
import os

path = os.path.dirname(os.path.abspath(__file__))
sippath = os.path.join(path,'sipStonegui')

post_deps = []
# Install the libraries
if sys.platform != 'win32':
	LibInstallTarget("stoneguiinstall",path,"stonegui","/usr/lib/")

try:
	os.mkdir(sippath)
except:	pass

sipIncludes = ['../../stone/include','/usr/include/stone','/usr/include/stonegui']
sipLibs = ['stone','stonegui']
# Python module targets, both depend on classes
pc = SipTarget2("pystonegui",path,'Stonegui')
pc.pre_deps = ["stonegui","pystone:install"]
pc.ExtraIncludes += sipIncludes
pc.ExtraLibs += sipLibs
pc.ExtraLibDirs += ['../../stone']

sst = SipTarget2("pystoneguistatic",path,'stonegui',True)
sst.pre_deps = ["stonegui"]
sst.ExtraIncludes += sipIncludes

# Check for numpy support, or disable if not found
try:
	import numpy
	npi = numpy.get_include()
	pc.ExtraIncludes.append(npi)
	sst.ExtraIncludes.append(npi)
except:
	pc.ExtraSipFlags += ['-x','NumpySupport']
	sst.ExtraSipFlags += ['-x','NumpySupport']

sst = SipTarget2("pystoneguistatic",path,'Stonegui',True)
sst.pre_deps = ["stonegui"]
sst.ExtraIncludes += sipIncludes

Target = QMakeTarget("stonegui",path,"stonegui.pro",["stone"],post_deps)
StaticTarget = QMakeTarget("stoneguistatic",path,"stonegui.pro",["stone"],[],True)

rpm = RPMTarget("stoneguirpm",'libstonegui',path,'../../../rpm/spec/stonegui.spec.template','1.0')

pyrpm = RPMTargetSip('pystoneguirpm','pystonegui',path,'../../../rpm/spec/pystonegui.spec.template','1.0')

if __name__ == "__main__":
	build()