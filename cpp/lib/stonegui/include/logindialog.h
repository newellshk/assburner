 
#ifndef LOGIN_DIALOG_H
#define LOGIN_DIALOG_H

#include <qdialog.h>

#include "blurqt.h"

#include "ui_logindialogui.h"
#include "stonegui.h"

namespace Stone {
	class Connection;
};
using Stone::Connection;

class STONEGUI_EXPORT LoginDialog : public QDialog, public Ui::LoginDialogUI
{
Q_OBJECT
public:
	LoginDialog(QWidget * parent=nullptr, Connection * c=nullptr, bool showConnectionSettings=false);
	
	void accept();
	void reject();

public slots:
	void slotResize();

protected:
	Connection * mConnection;
};


#endif // LOGIN_DIALOG_H
