
#ifndef STRING_MAP_EDITOR_H
#define STRING_MAP_EDITOR_H

#include <qdialog.h>

#include "stringmap.h"

class QTreeWidget;

class StringMapEditorDialog : public QDialog
{
Q_OBJECT
public:
	StringMapEditorDialog(QWidget * parent=nullptr);
	~StringMapEditorDialog();
	
	void setStringMap( const StringMap & stringMap );
	StringMap stringMap() const;
	
protected slots:
	void showTreeWidgetMenu(const QPoint &);
	
protected:
	QTreeWidget * mTree;
};

#endif // STRING_MAP_EDITOR_H

