
#include <iostream>

#include <qpainterpath.h>
#include <limits>
#include <utility>
#include <algorithm>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

// Returns nullptr with python exception set on failure
double * dataFromNumpy( PyObject * numPyArray, int sizeCheck = -1 )
{

	if( !PyArray_Check(numPyArray) ) {
		PyErr_Format(PyExc_TypeError, "Expected a numpy array" );
		return nullptr;
	}

	PyArrayObject * array = (PyArrayObject*)numPyArray;
	
	if( PyArray_TYPE(array) != NPY_FLOAT64 ) {
		PyErr_Format(PyExc_TypeError, "array contains wrong type: %i Should be NPY_FLOAT64",  PyArray_TYPE(array) );
		return nullptr;
	}

#if NPY_API_VERSION < 0x08
	int checkFlags = NPY_C_CONTIGUOUS | NPY_ALIGNED;
#else
	int checkFlags = NPY_ARRAY_C_CONTIGUOUS | NPY_ARRAY_ALIGNED;
#endif

	if( (PyArray_FLAGS(array) & checkFlags) != checkFlags ) {
		PyErr_Format(PyExc_TypeError, "array does not have expected flags" );
		return nullptr;
	}

	if( sizeCheck >= 0 && PyArray_SIZE(array) < sizeCheck ) {
		PyErr_Format(PyExc_ValueError, "array does not have enough data, expected: %i got: %li", sizeCheck, PyArray_SIZE(array) );
		return nullptr;
	}

	return (double*)PyArray_DATA(array);
}

QPainterPath pathFromNumpyArray(PyObject * numPyArray, int offset, int count, double dayWidth, double height, double scaleMax, bool * error)
{
	QPainterPath path;
	double * data = dataFromNumpy(numPyArray,offset+count);
	if( !data ) {
		*error = true;
		return path;
	}

	double mul = height / scaleMax;

	path.moveTo(0.0, 0.0);
	int i=0;
	for( ; i < count; ++i ) {
		path.lineTo(i * dayWidth,data[i + offset] * mul);
	}
	
	path.lineTo(i * dayWidth, 0.0);

	return path;
}


static const double tiny = 0.000001;

inline double getAttrDouble( PyObject * obj, const char * attrS, bool * error )
{
	double ret = 0.0;
	if( *error ) return ret;
	PyObject * attr = PyObject_GetAttrString(obj,attrS);
	if( attr ) {
		ret = PyFloat_AsDouble(attr);
		Py_DECREF(attr);
	} else
		*error = true;
	return ret;
}

inline int getAttrInt( PyObject * obj, const char * attrS, bool * error )
{
	int ret = 0;
	if( *error ) return ret;
	PyObject * attr = PyObject_GetAttrString(obj,attrS);
	if( attr ) {
		if( PyLong_Check(attr) )
			ret = PyLong_AsLong(attr);
		else if( PyInt_Check(attr) )
			ret = PyInt_AsLong(attr);
		Py_DECREF(attr);
	} else
		*error = true;
	return ret;
}

inline double getItemDouble( PyObject * obj, PyObject * item, bool * error )
{
	if( *error ) return 0.0;
	PyObject * pd = PyObject_GetItem(obj,item);
	if(!pd) {
		*error = true;
		return 0.0;
	}
	double ret = PyFloat_AsDouble(pd);
	if( ret == -1.0 && PyErr_Occurred() )
		*error = true;
	Py_DECREF(pd);
	return ret;
}

inline void setItemDouble( PyObject * obj, PyObject * item, double d, bool * error )
{
	if( *error ) return;
	if( PyObject_SetItem(obj,item,PyFloat_FromDouble(d)) == -1 )
		*error = true;
}

//def calculateRenderUsageWorker(count, leftover, resourceSeriesData, submitSeriesData, renderUsageData, renderBacklogData):
PyObject * calculateRenderUsageWorker(int count, double leftover, PyObject * resourceSeriesData, PyObject * submitSeriesData, PyObject * renderUsageData, PyObject * renderBacklogData)
{
	//for i in xrange(count):
	for( int i=0; i < count; ++i ) {
		bool error = false;
		PyObject * p_i = PyInt_FromLong(i);
		if( !p_i ) return nullptr;
		//rt = resourceSeriesData[i]
		double rt = getItemDouble(resourceSeriesData,p_i,&error);
		double ru = leftover + getItemDouble(submitSeriesData,p_i,&error);
		if(ru > rt) {
			leftover = ru - rt;
			ru = rt;
		} else
			leftover = 0.0;
		setItemDouble(renderUsageData,p_i,ru,&error);
		setItemDouble(renderBacklogData,p_i,leftover,&error);
		Py_DECREF(p_i);
		if( error ) return nullptr;
	}
	Py_INCREF(Py_None);
	return Py_None;
}

struct Series
{
	double constant;
	int offset, size;
	double * data;
	
	Series(int _offset=0)
	: constant(0.0), offset(_offset), size(0), data(0)
	{}

	double * dataFromSeries( PyObject * series, int * size, double * constant, int sizeCheck, bool * error )
	{
		double * ret = nullptr;
		if( *error ) return ret;
		if (size)
			
		return ret;
	}

	void setup(PyObject * seriesContainer,const char * seriesName,bool *error)
	{
		if( *error ) return;
		PyObject * series = PyObject_GetAttrString(seriesContainer,seriesName);
		if( !series ) {
			*error = true;
			return;
		}
		
		if( series == Py_None ) {
			Py_DECREF(series);
			/* No error */
			return;
		}
		
		size = getAttrInt(series,"Size",error);
		if( *error ) {
			Py_DECREF(series);
			return;
		}
		
		PyObject * dataObject = PyObject_GetAttrString(series,"Data");
		if( dataObject ) {
			if( dataObject == Py_None ) {
				Py_DECREF(dataObject);
				dataObject = nullptr;
			} else {
				data = dataFromNumpy( dataObject, -1 );
				if( !data )
					*error = true;
			}
		}
		
		if( !dataObject ) {
			PyErr_Clear();
			constant = getAttrDouble(series,"Constant",error);
		}
		
		Py_XDECREF(dataObject);
		Py_DECREF(series);
	}
	
	inline double get(int i) {
		if( i < offset || i - offset >= size ) return 0.0;
		if( data ) return data[i-offset];
		return constant;
	}
	inline void set(int i, double d) {
		if( data && i >= offset && i-offset < size )
			data[i-offset] = d;
	}
	inline void inc(int i, double d) {
		if( data && i >= offset && i-offset < size )
			data[i-offset] += d;
	}
};

struct WorkloadContainer {
	WorkloadContainer() : leftover(0.0) {}
	void setup(PyObject * po,bool *error) {
		leftover = getAttrDouble(po,"leftover",error);
		submit.offset = getAttrInt(po,"submitOffset",error);
		submit.setup(po,"submitSeries",error);
		render.setup(po,"renderUsage",error);
		backlog.setup(po,"renderBacklog",error);
	}
	double leftover;
	Series submit, render, backlog;
};

struct ProjectContainer {
	ProjectContainer() : leftover(0.0), workloadCount(0), workloads(0) {}
	~ProjectContainer() {
		delete [] workloads;
	}
	bool setup( PyObject * po ) {
		bool error = false;
		leftover = getAttrDouble(po,"leftover",&error);
		weight.offset = getAttrInt(po,"weightOffset",&error);
		weight.setup(po,"weightSeries",&error);
		render.setup(po,"renderUsage",&error);
		backlog.setup(po,"renderBacklog",&error);
		if( error ) return false;
		PyObject * workloadList = PyObject_GetAttrString(po,"workloads");
		if( workloadList ) {
			workloadCount = PyList_GET_SIZE(workloadList);
			if( workloadCount > 0 ) {
				workloads = new WorkloadContainer[workloadCount];
				for( int i=0; i<workloadCount; ++i )
					workloads[i].setup(PyList_GET_ITEM(workloadList,i),&error);
			}
		} else
			return false;
		return !error;
	}
	double leftover;
	int workloadCount;
	WorkloadContainer * workloads;
	Series weight, render, backlog;
};

//def calculateProjectRenderUsageWorker( count, projects, resourceSeries ):
PyObject * calculateProjectRenderUsageWorker( int count, PyObject * pyProjects, PyObject * resourceSeries )
{
	bool error = false;

	int projectCount = PyList_GET_SIZE(pyProjects);
	ProjectContainer * projects = new ProjectContainer[projectCount];

	for( int pi=0; pi<projectCount; pi++ ) {
		if( !projects[pi].setup(PyList_GET_ITEM(pyProjects,pi)) ) {
			error = true;
			break;
		}
	}

	if( !error ) {
		for( int i = 0; i < count; ++i ) {
			// Get render time available for the date
			PyObject * p_i = PyInt_FromLong(i);
			if( !p_i ) {
				error = true;
				break;
			}

			double rt = getItemDouble(resourceSeries, p_i, &error);
			Py_DECREF(p_i);
			if( error ) break;

			double rto = rt;
				
			// Add the render time submitted to the existing backlog(also per project)
			// Add submission time to current backlogs for projects and workloads
			//	for pc in projects:
			for( int pi=0; pi<projectCount; pi++ ) {
				ProjectContainer & pc = projects[pi];
				// projectSub = 0.0
				double projectSub = 0.0;

				for( int wi=0; wi < pc.workloadCount; wi++ ) {
					WorkloadContainer & wl = pc.workloads[wi];
					double sub = wl.submit.get(i);
					projectSub += sub;
					wl.leftover += sub;
				}

				// pc.leftover += projectSub
				pc.leftover += projectSub;
				
				// Apply weighted render time to the project and it's workloads
				//if pc.weightSeries is not None and i >= pc.weightSeriesOffset:
				double weight = pc.weight.get(i);
				if( weight > tiny ) {
					double weightedRenderTime = std::min( pc.leftover, rto * weight );
					if (weightedRenderTime > tiny) {
						rt -= weightedRenderTime;
						pc.leftover -= weightedRenderTime;
						pc.render.inc(i,weightedRenderTime);
						for( int wi=0; wi < pc.workloadCount; wi++ ) {
							WorkloadContainer & wl = pc.workloads[wi];
							double wlr = std::min(weightedRenderTime, wl.leftover);
							wl.leftover -= wlr;
							wl.render.inc(i,wlr);
							weightedRenderTime -= wlr;
						}
					}
				}
			}

			// Split up existing rendertime evenly between projects
			// Go until there are less than 10 seconds of rendertime left
			//	while rt > tiny:
			while (rt > tiny) {
				//	(least, cnt) = findLeastRenderTime()
				double least = 0.0;
				int cnt = 0;

				for( int pi=0; pi<projectCount; pi++ ) {
					ProjectContainer & pc = projects[pi];
					double rt = pc.leftover;
					if (rt < tiny) {
						pc.leftover = 0.0;
						continue;
					}
					if(cnt == 0)
						least = rt;
					else
						least = std::min(least,rt);
					cnt++;
				}

				if(cnt == 0)
					break;

				if(least * cnt > rt)
					least = rt / double(cnt);

				for( int pi=0; pi<projectCount; pi++ ) {
					ProjectContainer & pc = projects[pi];
					if( pc.leftover > tiny ) {
						double rtu = std::min(least,pc.leftover);
						pc.leftover -= rtu;
						pc.render.inc(i,rtu);
						rt -= rtu;
						for( int wi=0; wi < pc.workloadCount; wi++ ) {
							WorkloadContainer & wl = pc.workloads[wi];
							if( wl.leftover > tiny ) {
								double wlru = std::min(rtu,wl.leftover);
								wl.leftover -= wlru;
								wl.render.inc(i,wlru);
								rtu -= wlru;
							}
						}
					}
				}
			}

			for( int pi=0; pi<projectCount; pi++ ) {
				ProjectContainer & pc = projects[pi];
				if (pc.leftover > tiny)
					pc.backlog.set(i,pc.leftover);
				for( int wi=0; wi < pc.workloadCount; wi++ ) {
					WorkloadContainer & wl = pc.workloads[wi];
					if( wl.leftover > tiny )
						wl.backlog.set(i,wl.leftover);
				}
			}
		}
	}

	delete [] projects;

	if( error ) return nullptr;

	Py_INCREF(Py_None);
	return Py_None;
}
 
