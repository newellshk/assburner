
#include "connection.h"

#include "logindialog.h"

LoginDialog::LoginDialog( QWidget * parent, Connection * c, bool showConnectionSettings )
: QDialog( parent )
, mConnection(c)
{
	setupUi(this);
	connect( mConnectionButton, SIGNAL(toggled(bool)), SLOT(slotResize()), Qt::QueuedConnection );
	
	QString app = QApplication::instance()->applicationName();
	if (!app.isEmpty())
		setWindowTitle( app + " Database Login" );
	
	mConnectionGroup->setShown(showConnectionSettings);
	mConnectionButton->setChecked(showConnectionSettings);
	slotResize();
	
	mUsernameEdit->setText(c->userName());
	mPasswordEdit->setText(c->password());
	mHostEdit->setText(c->host());
	mDbNameEdit->setText(c->databaseName());
	mPortSpin->setValue(c->port());
}

void LoginDialog::accept()
{
	ConnectionFactory * cf = ConnectionFactory::instance();
	cf->setCredentials( mUsernameEdit->text(), mPasswordEdit->text() );
	cf->setConnectionOptions( mHostEdit->text(), mDbNameEdit->text(), mPortSpin->value() );
	mConnection->setUserName(mUsernameEdit->text());
	mConnection->setPassword(mPasswordEdit->text());
	mConnection->setDatabaseName(mDbNameEdit->text());
	mConnection->setPort(mPortSpin->value());
	mConnection->setHost(mHostEdit->text());
	QDialog::accept();
}

void LoginDialog::reject()
{
	QApplication::instance()->quit();
	QDialog::reject();
}

void LoginDialog::slotResize()
{
	resize(width(),0);
}
