
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of libstone.
 *
 * libstone is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * libstone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libstone; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qmenu.h>
#include <qevent.h>
#include <qapplication.h>

#include "stringmap.h"
#include "table.h"

#include "recordpropvaltree.h"
#include "stringmapeditor.h"

PropValDelegate::PropValDelegate(QObject * parent)
: QStyledItemDelegate(parent)
, mButtonState(0)
{}

void PropValDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	QStyledItemDelegate::paint(painter,option,index);

	if( option.state & QStyle::State_MouseOver ) {
		if( mButtonState ) {
			QStyleOptionButton button;
			button.rect = mButtonRect;
			button.text = "...";
			if( mButtonState & QStyle::State_MouseOver )
				button.state = mButtonState | QStyle::State_Enabled;
			else
				button.state = (mButtonState | QStyle::State_Enabled) & ~(QStyle::State_Sunken);
			QApplication::style()->drawControl(QStyle::CE_PushButton, &button, painter);
		}
	}
}

bool PropValDelegate::editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option,  const QModelIndex &index)
{
	if( event->type() == QEvent::MouseMove ) {
		QMouseEvent * me = static_cast<QMouseEvent*>(event);
		bool returnTrue = false;
		if( index != mLastHoverIndex ) {
			mLastHoverIndex = index;
			QVariant editData = index.data(Qt::EditRole);
			if( editData.userType() == qMetaTypeId<StringMap>() ) {
				mButtonState = QStyle::State_Raised;
				mButtonRect = QRect(option.rect);
				mButtonRect.setLeft( mButtonRect.right() - mButtonRect.height() );
			} else
				mButtonState = 0;
			returnTrue = true;
		}
		if( mButtonState ) {
			QFlags<QStyle::StateFlag> newButtonState = mButtonState;
			if( mButtonRect.contains( me->pos() ) ) {
				newButtonState |= QStyle::State_MouseOver;
			} else
				newButtonState &= ~QStyle::State_MouseOver;
			if( newButtonState != mButtonState ) {
				mButtonState = newButtonState;
				returnTrue = true;
			//	LOG_1( QString( "mButtonRect(%1,%2,%3,%4), mouse_pos(%5,%6), mButtonState(%7)" )
			//		.arg(mButtonRect.x()).arg(mButtonRect.y()).arg(mButtonRect.width()).arg(mButtonRect.height())
			//		.arg(me->pos().x()).arg(me->pos().y())
			//		.arg(newButtonState) );
			}
		}
		if (returnTrue)
			return true;
	}
	if( event->type() == QEvent::MouseButtonPress ) {
		QMouseEvent * me = static_cast<QMouseEvent*>(event);
		if( mButtonRect.contains( me->pos() ) )
			mButtonState |= QStyle::State_Sunken | QStyle::State_On;
		LOG_TRACE;
		return true;
	}
	if( event->type() == QEvent::MouseButtonRelease ) {
		QMouseEvent * me = static_cast<QMouseEvent*>(event);
		if( mButtonRect.contains( me->pos() ) ) {
			mButtonState &= ~(QStyle::State_Sunken | QStyle::State_On);
			StringMapEditorDialog edit(qobject_cast<QWidget*>(parent()));
			edit.setStringMap( index.data(Qt::EditRole).value<StringMap>() );
			if( edit.exec() == QDialog::Accepted )
				model->setData( index, qVariantFromValue<StringMap>(edit.stringMap()) );
		}
		LOG_TRACE;
		return true;
	}
	return QStyledItemDelegate::editorEvent(event,model,option,index);
}

RecordPropValTree::RecordPropValTree( QWidget * parent )
: ExtTreeView( parent )
{
	setModel( new RecordPropValModel(this) );
	setItemDelegateForColumn(1, new PropValDelegate(this));
	viewport()->setMouseTracking(true);
	//viewport()->setAttribute(Qt::WA_Hover);
	connect( this, SIGNAL( showMenu( const QPoint &, const QModelIndex & ) ), SLOT( slotShowMenu( const QPoint &, const QModelIndex & ) ) );
}

void RecordPropValTree::setRecords( const RecordList & rl )
{
	propValModel()->setRecords(rl);
}

RecordList RecordPropValTree::records() const
{
	return propValModel()->records();
}

void RecordPropValTree::setEditable( bool editable )
{
	propValModel()->setEditable(editable);
}

bool RecordPropValTree::editable() const
{
	return propValModel()->editable();
}

void RecordPropValTree::setModel( QAbstractItemModel * model )
{
	if (qobject_cast<RecordPropValModel*>(model))
		ExtTreeView::setModel(model);
	else
		LOG_1("RecordPropValTree requires a RecordPropValModel");
}

RecordPropValModel * RecordPropValTree::propValModel() const
{
	return qobject_cast<RecordPropValModel*>(model());
}

RecordPropValTree * RecordPropValTree::showRecords( const RecordList & records, QWidget * parent, bool editable )
{
	Q_UNUSED(parent);

	RecordPropValTree * tree = new RecordPropValTree(0);
	tree->setRecords( records );
	tree->setEditable( editable );
	tree->show();
	return tree;
}

void RecordPropValTree::slotShowMenu( const QPoint & pos, const QModelIndex & index )
{
	RecordList dest = propValModel()->foreignKeyRecords( index );
	if( dest.isEmpty() ) return;

	QMenu * menu = new QMenu( this );
	QAction * go = menu->addAction( "Show " + dest[0].table()->schema()->className() );
	
	QAction * result = menu->exec( pos );
	delete menu;

	if( result == go ) {
		setRecords(dest);
	}
}
