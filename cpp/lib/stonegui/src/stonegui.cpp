
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of libstone.
 *
 * libstone is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * libstone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libstone; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qsqldatabase.h>
#include <qsqlerror.h>
#include <qtimer.h>
#include <qthread.h>

#include "database.h"
#include "connection.h"

#include "stonegui.h"
#include "logindialog.h"
#include "lostconnectiondialog.h"
#include "imagesequenceprovider.h"

static ConnectionWatcher * sWatcher = 0;

ConnectionWatcher::ConnectionWatcher()
: mDialog( 0 )
, mConnection(nullptr)
, mMutex(QMutex::Recursive)
{
	LOG_TRACE
	connect( ConnectionFactory::instance(), SIGNAL(connectionLost(Connection*)), SLOT(connectionLost(Connection*)) );
	connect( ConnectionFactory::instance(), SIGNAL(authenticate(Connection*)), SLOT(authenticate(Connection*)) );
}

void ConnectionWatcher::connectionLost(Connection * c)
{
	mMutex.lock();
	mConnection = c;
	if( QThread::currentThread() != QApplication::instance()->thread() )
		QMetaObject::invokeMethod( this, "showLostConnectionDialog", Qt::BlockingQueuedConnection );
	else
		showLostConnectionDialog();
	mConnection = nullptr;
	mMutex.unlock();
}

void ConnectionWatcher::showLostConnectionDialog()
{
	if( mDialog == 0 ) {
		mDialog = new LostConnectionDialog( mConnection, mConnection->lastErrorText() );
		mDialog->exec();
		delete mDialog;
		mDialog = 0;
	}
}

void ConnectionWatcher::connected()
{
}

void ConnectionWatcher::authenticate(Connection * c)
{
	mMutex.lock();
	mConnection = c;
	mAuthOnly = true;
	if( QThread::currentThread() != QApplication::instance()->thread() )
		QMetaObject::invokeMethod( this, "showLoginDialog", Qt::BlockingQueuedConnection );
	else
		showLoginDialog();
	mConnection = nullptr;
	mMutex.unlock();
}

void ConnectionWatcher::showLoginDialog()
{
	LoginDialog loginDialog(nullptr,mConnection);
	loginDialog.mConnectionButton->setChecked(!mAuthOnly);
	loginDialog.exec();
}

ConnectionWatcher * ConnectionWatcher::connectionWatcher()
{
	if( !sWatcher )
		sWatcher = new ConnectionWatcher();
	return sWatcher;
}


void initStoneGui( bool createConnectionWatcher )
{
	if( createConnectionWatcher )
		ConnectionWatcher::connectionWatcher();
	registerBuiltinImageSequenceProviderPlugins();
}

