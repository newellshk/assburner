
#include <qdialogbuttonbox.h>
#include <qlayout.h>
#include <qmenu.h>
#include <qtreewidget.h>

#include "stringmapeditor.h"

StringMapEditorDialog::StringMapEditorDialog(QWidget * parent)
: QDialog(parent)
{
	mTree = new QTreeWidget(this);
	mTree->setHeaderLabels( QStringList() << "Key" << "Value" );
	mTree->setContextMenuPolicy(Qt::CustomContextMenu);
	connect( mTree, SIGNAL(customContextMenuRequested(const QPoint&)), SLOT(showTreeWidgetMenu(const QPoint&)) );

	QDialogButtonBox * dbb = new QDialogButtonBox(this);
	dbb->setStandardButtons(QDialogButtonBox::Save|QDialogButtonBox::Cancel);
	connect( dbb, SIGNAL(accepted()), SLOT(accept()) );
	connect( dbb, SIGNAL(rejected()), SLOT(reject()) );

	QVBoxLayout * layout = new QVBoxLayout(this);
	layout->addWidget(mTree);
	layout->addWidget(dbb);
}

StringMapEditorDialog::~StringMapEditorDialog()
{
}

void StringMapEditorDialog::setStringMap( const StringMap & stringMap )
{
	mTree->clear();
	for( StringMap::const_iterator it = stringMap.begin(), end = stringMap.end(); it != end; ++it ) {
		QTreeWidgetItem * item = new QTreeWidgetItem(mTree);
		item->setText(0,it.key());
		item->setText(1,it.value());
		item->setFlags(item->flags() | Qt::ItemIsEditable);
	}
}

StringMap StringMapEditorDialog::stringMap() const
{
	StringMap ret;
	for( int i=0, end = mTree->topLevelItemCount(); i < end; ++i ) {
		QTreeWidgetItem * item = mTree->topLevelItem(i);
		ret[item->text(0)] = item->text(1);
	}
	return ret;
}

void StringMapEditorDialog::showTreeWidgetMenu(const QPoint & pos)
{
	QMenu * menu = new QMenu(this);
	QAction * addItem = menu->addAction( "Add Item" );
	QAction * removeItem = menu->addAction( "Remove Item" );
	QAction * result = menu->exec(mTree->viewport()->mapToGlobal(pos));
	if( result == addItem ) {
		QTreeWidgetItem * item = new QTreeWidgetItem(mTree);
		item->setFlags(item->flags() | Qt::ItemIsEditable);
		mTree->editItem(item,0);
	}
	else if( result == removeItem ) {
		delete mTree->currentItem();
	}
}
