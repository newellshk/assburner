
TARGET=stonegui
TEMPLATE=lib

include($$(PRI_SHARED)/common.pri)
include($$(PRI_SHARED)/stone.pri)

RESOURCES += stonegui.qrc

FORMS+= \
	ui/addlinkdialog.ui \
	ui/graphitedialogui.ui \
	ui/graphitesourceswidgetui.ui \
	ui/graphiteoptionswidgetui.ui \
	ui/imagesequencewidget.ui \
	ui/logindialogui.ui \
	ui/lostconnectiondialogui.ui \
	ui/modelcopydialogui.ui \
	ui/remotetailwindowui.ui

SOURCES += \
	src/actions.cpp \
	src/busywidget.cpp \
	src/extgraphicsscene.cpp \
	src/exttreewidgetitem.cpp \
	src/ffimagesequenceprovider.cpp \
	src/fieldcheckbox.cpp \
	src/fieldlineedit.cpp \
	src/fieldtextedit.cpp \
	src/fieldspinbox.cpp \
	src/filteredit.cpp \
	src/glutil.cpp \
	src/graphitedialog.cpp \
	src/graphitesource.cpp \
	src/graphitesourceswidget.cpp \
	src/graphiteoptionswidget.cpp \
	src/graphitewidget.cpp \
	src/htmlhighlighter.cpp \
	src/iconserver.cpp \
	src/imagesequenceprovider.cpp \
	src/intervaledit.cpp \
	src/logindialog.cpp \
	src/lostconnectiondialog.cpp \
	src/modelcopydialog.cpp \
	src/modelgrouper.cpp \
	src/modeliter.cpp \
	src/qvariantcmp.cpp \
	src/recordcombo.cpp \
	src/recorddelegate.cpp \
	src/recorddrag.cpp \
	src/recordlistmodel.cpp \
	src/recordlistview.cpp \
#	src/tardstyle.cpp \
	src/recentvaluesui.cpp \
	src/recordpropvalmodel.cpp \
	src/recordpropvaltree.cpp \
	src/recordsupermodel.cpp \
	src/recordtreeview.cpp \
	src/richtexteditor.cpp \
	src/stonegui.cpp \
	src/stringmapeditor.cpp \
	src/supermodel.cpp \
	src/remotetailwidget.cpp \
	src/remotetailwindow.cpp \
	src/imagesequencewidget.cpp \
	src/undotoolbutton.cpp \
	src/viewcolors.cpp

HEADERS += \
	include/actions.h \
	include/busywidget.h \
	include/extgraphicsscene.h \
	include/exttreewidgetitem.h \
	include/ffimagesequenceprovider.h \
	include/fieldcheckbox.h \
	include/fieldlineedit.h \
	include/fieldtextedit.h \
	include/fieldspinbox.h \
	include/filteredit.h \
	include/glutil.h \
	include/graphitedialog.h \
	include/graphitesource.h \
	include/graphitesourceswidget.h \
	include/graphiteoptionswidget.h \
	include/graphitewidget.h \
	include/htmlhighlighter.h \
	include/iconserver.h \
	include/imagesequenceprovider.h \
	include/intervaledit.h \
	include/logindialog.h \
	include/lostconnectiondialog.h \
	include/modelcopydialog.h \
	include/modelgrouper.h \
	include/modeliter.h \
	include/qvariantcmp.h \
	include/recentvaluesui.h \
	include/recordcombo.h \
	include/recorddelegate.h \
	include/recorddrag.h \
	include/recordlistmodel.h \
	include/recordlistview.h \
	include/recordpropvalmodel.h \
	include/recordpropvaltree.h \
	include/recordsupermodel.h \
#	include/tardstyle.h \
	include/recordtreeview.h \
	include/richtexteditor.h \
	include/stonegui.h \
	include/stringmapeditor.h \
	include/supermodel.h \
	include/remotetailwidget.h \
	include/remotetailwindow.h \
	include/imagesequencewidget.h \
	include/undotoolbutton.h \
	include/viewcolors.h

INCLUDEPATH+=include src .out

DEPENDPATH+=src include ui

macx{
	INCLUDEPATH+=/Developer/SDKs/MacOSX10.4u.sdk/usr/X11R6/include/
}

# FFmpeg support
#unix:DEFINES+=USE_FFMPEG
#win32:DEFINES+=USE_FFMPEG

contains( DEFINES, USE_FFMPEG ) {
	include(ffmpeg.pri)
}

DEFINES+=STONEGUI_MAKE_DLL

target.path=$$LIB_PREFIX

INSTALLS += target
CONFIG+=qt thread
QT+=sql xml gui network opengl

