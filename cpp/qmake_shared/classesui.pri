
# Include if building against and linking to classesui

rpm_build() {
	INCLUDEPATH += /usr/include/classesui
} else {
	INCLUDEPATH += \
		../../lib/classesui/include \
		../../lib/classesui/.out
	LIBS+=-L../../lib/classesui
}
win32 {
	LIBS += -lshell32
}

LIBS+=-lclassesui

