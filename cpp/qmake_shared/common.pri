
DESTDIR=./
MOC_DIR=.out
OBJECTS_DIR=.out
UI_DIR=.out

*clang* {
	message(Compiling for clang!)
	QMAKE_CXXFLAGS+=-std=c++11 -Wno-deprecated-register -Wno-format-security -fcolor-diagnostics
}

*g++* {
	QMAKE_CXXFLAGS+=-std=c++11
}

defineTest(rpm_build) {
	__RPM_BUILD_ROOT = $$(RPM_BUILD_ROOT)
	isEmpty (__RPM_BUILD_ROOT) {
		return (false)
	}
	return (true)
}

macx:CONFIG -= app_bundle

isEmpty(PREFIX) {
	PREFIX = $$(PREFIX)
}

isEmpty(PREFIX) {
	win32 {
		PREFIX=c:/blur/
	} else {
		PREFIX=/usr/local/
	}
}

isEmpty(LIB_PREFIX) {
	LIB_PREFIX = $$(LIB_PREFIX)
}

isEmpty(LIB_PREFIX) {
	win32 {
		LIB_PREFIX=$$PREFIX/common/
	} else {
		LIB_PREFIX=$$PREFIX/lib/
	}
}

isEmpty(BIN_PREFIX) {
	win32 {
		BIN_PREFIX=$$PREFIX
	} else {
		BIN_PREFIX=$$PREFIX/bin/
	}
}

equals(TEMPLATE,lib) {
	win32:QMAKE_CXXFLAGS += -Fdlib$${TARGET}.pdb
	target.path=$$LIB_PREFIX
}

equals(TEMPLATE,app) {
	win32:QMAKE_CXXFLAGS += -Fd$${TARGET}.pdb
	target.path=$$BIN_PREFIX
}