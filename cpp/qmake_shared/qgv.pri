
rpm_build() {
	INCLUDEPATH+=/usr/include/qgv
	LIBS+=-L/usr/lib/qgv
} else {
	win32:contains(QMAKE_TARGET.arch, x86_64) {
	} else {
		INCLUDEPATH += ../../lib/qgv/QGVCore ../../lib/qgv/QGVCore/private
		LIBS+=-L../../lib/qgv -L../../lib/qgv/QGVCore
		DEFINES += USE_GRAPHVIZ
	}
}
contains(DEFINES,USE_GRAPHVIZ){
	LIBS+=-lQGVCore
}
