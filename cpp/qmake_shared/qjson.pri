# Include if building against and linking to libqjson4

lessThan(QT_MAJOR_VERSION, 5) {

	rpm_build() {
		INCLUDEPATH+=/usr/include/qjson
		LIBS+=-L/usr/lib/qjson
	} else {
		INCLUDEPATH += ../../lib/qjson
		LIBS+=-L../../lib/qjson
	}
	LIBS+=-lqjson4

}
