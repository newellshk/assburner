
_DEBUG_ = False

import re
import sip
from PyQt4.QtCore import QVariant, QString, QStringList
from blur.Stone import *
#from trax.api.data.bases.record import RecordBase as Record
from blur.Stone import Record as _Record

class FieldMethodObject(object):
	__slots__ = ('Field')
	def __init__(self, Field):
		self.Field = Field

# Function object that calls self.Method and casts the result to the same type as the instance's class
class CastReturnMethodObject(object):
	__slots__ = ('Method')
	
	def __init__(self,Method):
		self.Method = Method
	
	def _generateDocs(self):
		return 'Auto-generated DynamicTableTypes constructor method.'

	__doc__ = property(_generateDocs)

	def __call__(self, object, *args):
		return object.__class__(self.Method(object, *args))

# Used for Record getters, function object knows the field that is being retrieved, and
# converts the results from QVariants to python types
class GetMethodObject(FieldMethodObject):
	__slots__ = ('Field')
	
	def _generateDocs(self):
		self.__doc__ = 'Auto-generated DynamicTableTypes database field getter method.\n'
		self.__doc__ += str(self.Field.docs())
		self.__doc__ += ':return %s' % str(self.Field.typeString())

	__doc__ = property(_generateDocs)

	def __call__(self, object, fail=None):
		out = Record.getPyValue(object,self.Field)
		return out if out is not None else fail

class FKGetMethodObject(FieldMethodObject):
	__slots__ = ('Field')
	
	def _generateDocs(self):
		self.__doc__ = 'Auto-generated DynamicTableTypes database foreign key field getter method.\n'
		self.__doc__ += str(self.Field.docs())
		self.__doc__ += ':return %s' % str(self.Field.typeString())
	
	__doc__ = property(_generateDocs)

	def __call__(self, object, fail=None):
		ret = Record.foreignKey(object, self.Field)
		if not ret.table():
			ret = object.ClassDict[str(self.Field.foreignKeyTable().className())](ret)
		return ret

# Used for RecordList getters.
class ListGetMethodObject(FieldMethodObject):
	__slots__ = ('Field','QVariantType')
	
	def __init__(self, Field):
		FieldMethodObject.__init__(self, Field)
		self.QVariantType = None

	def __call__(self, object):
		if not self.QVariantType:
			self.QVariantType = self.Field.qvariantType()
		return variantListToPyList(object.getValue(self.Field), self.QVariantType)

# Used for RecordList getters.
class ListFKGetMethodObject(FieldMethodObject):
	__slots__ = ('Field')

	def __call__(self, object):
		ret = object.foreignKey(self.Field)
		# An empty list will have RecordList type, instead of a typed list, so we'll return the correct type instead
		if ret.size() == 0:
			ret = object.ClassDict[str(self.Field.foreignKeyTable().className()) + "List"]()
		return ret

# Used for record setters
class SetMethodObject(FieldMethodObject):
	__slots__ = ('Field')
		
	def _generateDocs(self):
		self.__doc__ = 'Auto-generated DynamicTableTypes database field setter method.\n'
		self.__doc__ += str(self.Field.docs())

		typestr = str(self.Field.typeString())
		if (self.Field.foreignKeyTable()):
			typestr = str(self.Field.foreignKeyTable().className())

		self.__doc__ += '\n:param %s value' % (typestr)
		return self.__doc__
	
	__doc__ = property(_generateDocs)

	def __call__(self, object, value):
		cls = object.__class__
		if self.Field.flag(Field.ForeignKey):
			return cls(Record.setForeignKey(object, self.Field, value))
		return cls(Record.setValue(object, self.Field, unwrapQVariant(value)))

# Used for RecordList setters
class ListSetMethodObject(FieldMethodObject):
	__slots__ = ('Field')
	
	def __call__(self, object, value):
		if self.Field.flag(Field.ForeignKey):
			return RecordList.setForeignKey(object, self.Field, value)
		else:
			value = unwrapQVariant(value)
		return RecordList.setValue(object, self.Field, value)

# function object for RecordList subclasses' __init__ function
class ListInitMethodObject(object):
	__slots__ = ('ParentType')

	def __init__(self,ParentType):
		self.ParentType = ParentType
	
	def __call__(self, object, *args):
		if len(args):
			arg = args[0]
			if len(args) == 1 and isinstance(arg,(tuple,list)):
				arg = recordListFromPyList(arg,type(object).Schema.table())
			elif len(args) == 1 and isinstance(arg,(QStringList)):
				idx = object.table().defaultLookupIndex()
				if idx:
					arg = idx.recordsByIndex([arg])
			self.ParentType.__init__(object, arg)
		else:
			self.ParentType.__init__(object)

# Used for static Record subclass select function, casts the return 
# record list to recordlist subclass
class SelectMethodObject(object):
	def __call__(self, cls, *args):
		listType = cls.ClassDict[str(cls.Schema.className()) + 'List']
		return listType(cls.Schema.table().select(*args))
SelectMethodObjectInstance = classmethod(SelectMethodObject())

# Used for static Record subclass join function
class JoinMethodObject(object):
	def __call__(self, cls, otherCls, condition='', joinType=InnerJoin, ignoreResults=False, alias=''):
		if isinstance(otherCls, Table):
			otable = otherCls
		else:
			try:
				otable = otherCls.table()
			except:
				raise Exception("Must pass a Table instance or Record subclass for the first join argument")
		return cls.Schema.table().join(otable, condition, joinType, ignoreResults, alias)
JoinMethodObjectInstance = classmethod(JoinMethodObject())

# Static table function object
class TableMethodObject(object):
	def __get__(self,obj,cls):
		return lambda s=None: cls.Schema.table()
TableMethodObjectInstance = TableMethodObject()

class TableSchemaMethodObject(object):
	def __get__(self,obj,cls):
		return lambda s=None: cls.Schema
TableSchemaMethodObjectInstance = TableSchemaMethodObject()

def setMethodName(methodName):
	mn = str(methodName)
	if mn.startswith('is') and mn[2].isupper():
		return 'set' + mn[2:]
	else:
		return 'set' + mn[0].upper() + mn[1:]

class InitMethodObject(object):
	__slots__ = ('ParentType')
	def __init__(self,ParentType):
		self.ParentType = ParentType
		
	def __call__(self, object, *args):
		arg = None
		schema = object.__class__.Schema
		if len(args):
			argz = args[0]
			if issubclass(type(args), _Record) and argz.table():
				if _DEBUG_:
					print argz

				argSchema = argz.table().schema()
				# Check to make sure it's a subclass of this type
				if schema.isDescendant(argSchema):
					arg = argz
			elif len(args) == 1 and isinstance(argz, (unicode,str,QString)):
				idx = type(object).Schema.table().defaultLookupIndex()
				if idx:
					arg = idx.recordByIndex(argz)
			else:
				arg = argz
		if arg is None:
			arg = schema.table().emptyImp()
		if type(arg) in (int, long) and self.ParentType == Record:
			arg = schema.table().record(arg)
		self.ParentType.__init__(object, arg)

class IndexMethodObject(object):
	__slots__ = ('IndexSchema')
	def __init__(self,IndexSchema):
		self.IndexSchema = IndexSchema
	def __call__(self, cls, *args):
		qvargs = []
		lookupMode = Index.UseCache | Index.UseSelect | Index.PartialSelect
		index = self.IndexSchema.table().table().indexFromSchema(self.IndexSchema)
		cols = len(self.IndexSchema.columns())
		for i, arg in enumerate(args):
			if i < cols and arg is None:
				raise TypeError('Argument %i has unexpected type %s' % (i, type(arg).__name__))
			if i == cols and isinstance(arg, int):
				lookupMode = arg
			else:
				qvargs.append(unwrapQVariant(arg))
		vals = index.recordsByIndex(qvargs,lookupMode)
		if self.IndexSchema.holdsList():
			return cls.ClassDict[str(self.IndexSchema.table().className() + 'List')](vals)
		if len(vals):
			return cls.ClassDict[str(self.IndexSchema.table().className())](vals[0])
		return cls.ClassDict[str(self.IndexSchema.table().className())]()

class ReverseAccessMethodObject(FieldMethodObject):
	__slots__ = ('Field')
	def __call__(self, object, lookupMode=Index.UseCache|Index.UseSelect|Index.PartialSelect):
		oSchema = self.Field.table()
		vals = oSchema.table().indexFromSchema(self.Field.index()).recordsByIndex([unwrapQVariant(object)],lookupMode)
		if self.Field.flag(Field.Unique):
			return object.ClassDict[str(oSchema.className())](vals and vals[0] or None)
		return object.ClassDict[str(oSchema.className()) + 'List'](vals)

class ReverseAccessListMethodObject(FieldMethodObject):
	__slots__ = ('Field')
	def __call__(self, object,lookupMode=Index.UseCache|Index.UseSelect|Index.PartialSelect):
		oSchema = self.Field.table()
		vals = oSchema.table().indexFromSchema(self.Field.index()).recordsByIndexMulti([unwrapQVariant(object)],lookupMode)
		return object.ClassDict[str(oSchema.className()) + 'List'](vals)

class ClassDocDescriptor(object):
	def __get__(self,obj,cls):
		docs = 'Auto-generated DynamicTableTypes Table class.\n'
		docs += str(cls.Schema.docs())
		return docs
	
ClassDocDescriptorInstance = ClassDocDescriptor()

from new import instancemethod

def createTableType(tableSchema, dict, moduleName=None):
	className 		 = str(tableSchema.className())
	listClassName 	 = str(className + 'List')

	parentType = Record
	parentListType = RecordList

	if tableSchema.parent() and str(tableSchema.parent().className()) in dict:
		parentName = str(tableSchema.parent().className())
		parentType = dict[parentName]
		parentListType = dict[parentName + 'List']

	createdTableType = False
	createdListType = False

	initMethod = None
	listInitMethod = None
	
	baseDict = {'Schema':tableSchema,'__doc__':ClassDocDescriptorInstance,'ClassDict':dict}
	if moduleName is not None:
		baseDict['__module__'] = moduleName

	# Updates a dict with another dict and returns it
	dm = lambda d, m: (m and d.update(m)) or d

	if className + 'Base' in dict:
		dict[className].__bases__ = (parentType,)
		tableType = sip.wrappertype(className, (dict[className + 'Base'],), baseDict)
	elif className in dict:
		dict[className].__bases__ = (parentType,)
		tableType = sip.wrappertype(className, (dict[className],), baseDict)
	else:
		# This creates the new type, we still have to add all the methods
		initMethod = InitMethodObject(ParentType=parentType)
		tableType = sip.wrappertype(className, (parentType,), dm({'__init__': instancemethod(initMethod, None, parentType)}, baseDict))
		createdTableType = True

	if listClassName + 'Base' in dict:
		listType = sip.wrappertype(listClassName, (dict[listClassName + 'Base'],), baseDict)
	elif listClassName in dict:
		dict[listClassName].__bases__ = (parentListType,)
		listType = sip.wrappertype(listClassName, (dict[listClassName],), dm({'ClassType': tableType}, baseDict))
	else:
		# Creates the new list type
		listInitMethod = ListInitMethodObject(ParentType=parentListType)
		listType = sip.wrappertype(listClassName, (parentListType,), dm({'__init__': instancemethod(listInitMethod, None, parentListType)}, baseDict))
		createdListType = True

	if createdTableType:
		setattr(tableType, 'table', TableMethodObjectInstance)
		setattr(tableType, 'schema', TableSchemaMethodObjectInstance)
		setattr(tableType, 'select', SelectMethodObjectInstance)
		setattr(tableType, 'join', JoinMethodObjectInstance)
	
	if createdListType:
		setattr(listType, 'table', TableMethodObjectInstance)
		setattr(listType, 'schema', TableSchemaMethodObjectInstance)
		setattr(listType, 'ClassType', tableType)

		# Add cast methods
		for method in ['filter', 'sorted', 'unique', 'reversed', 'reloaded']:
			setattr(listType, method, instancemethod(CastReturnMethodObject(Method=getattr(RecordList, method)), None, listType))

	# Getters and setters
	# Add each only if they don't already exist
	for field in tableSchema.ownedFields():
		methodName = str(field.methodName())
		flags = field.flags()
		isFkey = flags & Field.ForeignKey
		isPkey = flags & Field.PrimaryKey
		
		atn = methodName
		if not hasattr(tableType, atn):
			if isFkey:
				setattr(tableType, atn, instancemethod(FKGetMethodObject(Field=field), None, tableType))
			else:
				setattr(tableType, atn, instancemethod(GetMethodObject(Field=field), None, tableType))
		
		atn = str(setMethodName(methodName))
		if not hasattr(tableType, atn):
			setattr(tableType, atn, instancemethod(SetMethodObject(Field=field), None, tableType))
		
		atn = str(field.pluralMethodName())
		if not hasattr(listType, atn):
			if isFkey:
				setattr(listType, atn, instancemethod(ListFKGetMethodObject(Field=field), None, listType))
			else:
				setattr(listType, atn, instancemethod(ListGetMethodObject(Field=field), None, listType))
		
		atn = str(setMethodName(atn))
		if not hasattr(listType, atn):
			setattr(listType, atn, instancemethod(ListSetMethodObject(Field=field), None, listType))

		if flags & Field.DefaultLookup:
			if initMethod is None:
				initMethod = InitMethodObject(ParentType=parentType)
				setattr(tableType, '__init__', instancemethod(initMethod, None, parentType))
			if listInitMethod is None:
				listInitMethod = ListInitMethodObject(ParentType=parentListType)
				setattr(listType, '__init__', instancemethod(listInitMethod, None, parentListType))
	
	# From blur.Stone
	createFieldExpressions(tableType,tableSchema)

	# Index functions
	for indexSchema in tableSchema.indexes():
		if indexSchema.field() and indexSchema.field().flag(Field.PrimaryKey): continue
		prefix = 'recordBy'
		if indexSchema.holdsList():
			prefix = 'recordsBy'
		name = str(indexSchema.name())
		name = prefix + name[0].upper() + name[1:]
		if _DEBUG_:
			print "Checking for existing index: " + name
		if not hasattr(tableType, name):
			if _DEBUG_:
				print "Creating index: " + className + "." + name
			setattr(tableType, name, classmethod(IndexMethodObject(IndexSchema=indexSchema)))

	# Put the new types in the dict
	dict[className] = tableType
	dict[listClassName] = listType

	return (tableType, listType)

def createReverseAccessors(tableSchema, typeDict, dict):
	name = str(tableSchema.className())
	name = name[0].lower() + name[1:]
	
	# For each reverse access field, we create a reverse access function
	# on the class and list class that it refers to.
	for field in tableSchema.ownedFields():
		flags = field.flags()
		if flags & Field.ReverseAccess:
			fkt = field.foreignKeyTable()
			if not fkt:
				print "{table}.{field} marked ReverseAccess, but no foreign key table was returned".format(table=tableSchema.tableName(),field=field.name())
				continue
			tableType, listType = typeDict[tableSchema]
			objectTableType, objectListType = None, None
			if fkt in typeDict:
				objectTableType, objectListType = typeDict[fkt]
			else:
				className = str(fkt.className())
				if not objectTableType in dict:
					continue
				objectTableType = dict[className]
				objectListType = dict[className + 'List']
			methodName = name
			if not flags & Field.Unique:
				methodName = str(pluralizeName(methodName))
			if _DEBUG_:
				print "Adding Reverse Accessor %s.%s" % (objectTableType.__name__, methodName)
			if not hasattr(objectTableType, methodName):
				try:
					setattr(objectTableType, methodName, instancemethod(ReverseAccessMethodObject(Field=field), None, objectTableType))
				except Exception, e:
					print e
					print "Failed to add attribute"
			elif _DEBUG_:
				print "%s already in dict" % methodName
			if _DEBUG_:
				print "Adding Reverse Accessor %s.%s" % (objectListType.__name__, methodName)
			if not hasattr(objectListType, methodName):
				try:
					setattr(objectListType, methodName, instancemethod(ReverseAccessListMethodObject(Field=field), None, objectListType))
				except Exception, e:
					print e
					print "Failed to add attribute"
			elif _DEBUG_:
				print objectListType.__name__,'already has attribute',methodName

def createTableTypes(tables, dict=None, moduleName=None):
	dict = dict or globals()
	typeDict = {}

	origtables = tables[:]

	while True:
		childrenDoLater = []
		for table in tables:
			if table.parent() and (table.parent() in tables) and table.parent() not in typeDict:
				childrenDoLater.append(table)
				continue

			if _DEBUG_:
				print "Generating ", table.className()

			typeDict[table] = createTableType(table, dict, moduleName)
		tables = childrenDoLater
		if len(tables) == 0: break

	for table in origtables:
		createReverseAccessors(table, typeDict, dict)
