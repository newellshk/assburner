
from .._Stone import *
from .expression_decorator import *

def sendWOLPacket(macaddress, ipaddr = '255.255.255.255', port=9):
	""" Switches on remote computers using WOL. """
	import socket
	macaddress = str.strip(macaddress)
	# Check macaddress format and try to compensate.
	if len(macaddress) == 12:
		pass
	elif len(macaddress) == 12 + 5:
		sep = macaddress[2]
		macaddress = macaddress.replace(sep, '')
	else:
		raise ValueError('Incorrect MAC address format: "' + macaddress + '"')
	
	pkt = ''
	for i in range(0,12,2):
		val = macaddress[i:i+2]
		pkt += chr(int(val,16))
	pkt = chr(0xFF)*6 + ''.join(pkt*16)
	
	# Broadcast it to the LAN.
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.getprotobyname('udp'))
	sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
	print "sending magic packet to %s:%s with %s"%(ipaddr, port, macaddress)
	sock.sendto(pkt, (socket.gethostbyname(ipaddr), port))
	sock.close()

# schema can be a filename or a string containing the xml if schemaIsFile=True
# Returns (schema,[tableSchemas]), where schema is the merged or created schema object
# and tableSchemas is the list of tableSchema objects that were created
def createPythonTypesFromSchema( xmlSchema, module, mergeWithSchema = None, xmlSchemaIsFile = True ):
	from blur.DynamicTableTypes import createTableTypes
	if mergeWithSchema is None:
		schema = Schema.createFromXmlSchema( xmlSchema, xmlSchemaIsFile )
		tables = schema and schema.tables()
		success = schema is not None
	else:
		schema = mergeWithSchema
		success, tables = schema.mergeXmlSchema( xmlSchema, xmlSchemaIsFile )
	if success:
		createTableTypes( tables, module.__dict__, module.__name__ )
		addSchemaCastTypeDict( schema, module.__dict__ )
	else:
		print 'Error loading', xmlSchema
	return (schema,tables)

class FieldContainer(object):
	def __init__(self,tableSchema):
		self.Table = tableSchema
	@staticmethod
	def _fieldName(field):
		if field.flag(Field.PrimaryKey):
			return 'Key'
		name = str(field.methodName())
		return name[0].upper() + name[1:]
	def _get_dynamic_attributes(self):
		return [self._fieldName(f) for f in self.Table.fields()]
	def __dir__(self):
		return list(set(dir(type(self)) + list(self.__dict__) + self._get_dynamic_attributes()))
	def __getattr__(self,attr):
		atn = 'Key'
		if attr == atn:
			field = self.Table.field(self.Table.primaryKeyIndex())
		else:
			field = self.Table.field(attr)
			if not field:
				raise AttributeError(attr)
			atn = self._fieldName(field)
			if atn != attr:
				raise AttributeError(attr)
		ret = FieldExpression(field,self.Table)
		self.__dict__[atn] = ret
		return ret

def createFieldExpressions(tableType,tableSchema):
	if not hasattr(tableType,'c') or not hasattr(tableType.c,'Table') or tableType.c.Table != tableSchema:
#		print "Creating FieldContainer for table", tableSchema.tableName()
		setattr(tableType, 'c', FieldContainer(tableSchema))


# Copy logging macros from libstone
LOG_1 = lambda msg: Log( msg, 1 )
LOG_2 = lambda msg: Log( msg, 2 )
LOG_3 = lambda msg: Log( msg, 3 )
LOG_4 = lambda msg: Log( msg, 4 )
LOG_5 = lambda msg: Log( msg, 5 )
def Record__getattr__(record,attr):
	if attr == 'c':
		return FieldContainer(schema)
	fieldName = attr
	isSetter = False
	if not isinstance(record,Record):
		raise TypeError("self is not a Record instance")
	table = record.table()
	schema = table.schema()
	if not table:
		raise AttributeError(attr)
	if fieldName.startswith('set'):
		fieldName = fieldName[3:]
		isSetter = True
	field = schema.field(fieldName)
	if not field:
		for field in schema.fields():
			fkt = field.foreignKeyTable()
			if fkt and fkt.className().toLower() == fieldName:
				break
		else:
			raise AttributeError(attr)
	if field.foreignKeyTable():
		def getter():
			r = record.foreignKey(field)
			if r.isRecord():
				return r
			return getSchemaCastModule(schema.schema())[str(field.foreignKeyTable().className())](r)
		def setter(value):
			return record.setForeignKey(field,value)
		return isSetter and setter or getter
	def getter():
		return wrapQVariant(record.getValue(field))
	def setter(value):
		return record.setValue(field,value)
	return isSetter and setter or getter

setattr(Record,'__getattr__',Record__getattr__)
