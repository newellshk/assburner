
from blur.Stone import PlaceHolder, Expression

class ExpressionDescriptor(object):
	def __init__(self,func=None,selfPlaceHolders=()):
		self.val = None
		self.func = func
		if not isinstance(selfPlaceHolders,tuple):
			selfPlaceHolders = (selfPlaceHolders,)
		self.selfPlaceHolders = selfPlaceHolders
		if self.func and hasattr(self.func,'__doc__'):
			self.__doc__ = func.__doc__
	def __call__(self, func):
		self.func = func
		if hasattr(func,'__doc__'):
			self.__doc__ = func.__doc__
		return self
	@staticmethod
	def call_with_placeholders(func,selfPlaceHolders = None):
		args = []
		argCount = func.func_code.co_argcount
		varDefaults = func.func_defaults
		varNames = func.func_code.co_varnames
		defaultStart = argCount - len(varDefaults or [])
		for i in range(argCount):
			varName = varNames[i]
			if i == 0 and not selfPlaceHolders:
				selfPlaceHolders = (varName)
			phArgs = [varName]
			if i >= defaultStart:
				phArgs.append(varDefaults[i-defaultStart])
			args.append(PlaceHolder(*phArgs))
		return (func(*args),selfPlaceHolders)
	def __get__(self, obj, cls):
		if self.val is None:
			(self.val,self.selfPlaceHolders) = ExpressionDescriptor.call_with_placeholders(self.func, self.selfPlaceHolders)
		ret = self.val
		if obj is not None:
			for phName in self.selfPlaceHolders:
				#print "Setting placeholder to self", phName
				ret = ret.setPlaceHolder(phName,obj)
		return Expression(ret)

def expression(*args,**kwargs):
	return ExpressionDescriptor(*args,**kwargs)

def expression_fn(arg):
	return ExpressionDescriptor.call_with_placeholders(args[0])[0]

__all__ = ['expression','expression_fn']

if __name__ == "__main__":
	from blur.quickinit import *
	import sys, time
	import unittest
	
	class JobSub(Job):
		
		@expression
		def project(name):
			return (Project.c.Name == name).cache()
		
		@expression
		def status(status):
			return (Job.c.Status == status).cache()
			
		@expression
		def statusAndPriority(status,priority):
			return (Test.status & (Job.c.Priority==priority)).cache(useParentCache=True,fillParentCache=True)
			
		@expression
		def exp(priority=50,status=QString('started')):
			return ((Job.c.Priority == priority) & (Job.c.Status == status)).cache()
			
		@expression
		def exp2(projectName,priority=50,status=QString('started')):
			return ((Job.c.Project == Query([Project.c.Key],Project.table(),Test.project)) & Test.exp).cache()

		@expression(selfPlaceHolders='job')
		def exp3(job):
			return (Job.c.Key == job).cache()

		@expression
		def testCacheAll():
			priority = PlaceHolder('priority',cacheAll=True)
			return ((Job.c.Status == 'ready') & (Job.c.Priority == priority)).cache()


	class Tests(unittest.TestCase):
		def setUp(self):
			Database.current().setEchoMode(Database.EchoSelect)
			
		def assertTimed(self,func,args,maxTime=None,minTime=None):
			time = QTime()
			time.start()
			func(args)
			elapsed = time.elapsed()
			if maxTime is not None and elapsed > maxTime:
				fail("Took %i ms exceeding max time of %i ms" % (elapsed, maxTime))
			if minTime is not None and elapsed < minTime:
				fail("Took %i ms, less than the min time of %i ms" % (elapsed, maxTime))
				
		def test_static(self):
			self.assertIsInstance(JobSub.project,Expression)
			self.assertIsInstance(JobSub.project('test'),RecordList)
		
		def test_maxAge(self):
			@expression
			def hostByName(hostName):
				return (Host.c.Name == hostName).cache(maxAge=Interval.fromString('1 second')[0])
			hostByName('sentinel001')
			self.assertTimed(hostByName,'sentinel001',maxTime=20)
			time.sleep(5)
			self.assertTimed(hostByName,'sentinel001',minTime=100)

		@unittest.skip("Not finished")
		def test_cacheAll(self):
			print Test.testCacheAll(50)
			print Test.testCacheAll(50)
			print Test.testCacheAll(70)
			
	unittest.main()
	sys.exit(0)
	
	def timed(exp,args,its=10000):
		print exp.toString()
		time = QTime()
		time.start()
		exp(*args)
		print time.elapsed()
		time.start()
		exp(*args)
		print time.elapsed()
		time.start()
		for i in xrange(its):
			exp(*args)
		print time.elapsed()
	
	Database.current().setEchoMode(Database.EchoSelect)
	#print Test.statusAndPriority('ready',50)
	#print Test.statusAndPriority('ready',55)
	
	
	timed( Test.exp, [50] )
	timed( Test.exp2, [QString('Goldfish_S6'),70,QString('ready')] )

	jobs = Job.select(Expression().limit(2))
	timed( Test(jobs[0]).exp3, [] )
	timed( Test.exp3, [jobs[1]] )
