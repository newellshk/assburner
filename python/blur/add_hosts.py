
from blur.quickinit import *
from optparse import OptionParser
import re, os

def initHost( hostName, location=None ):
	host = Host()
	host.setName(hostName)
	host.setOnline(1)
	host.setAllowMapping(True)
	if location:
		host.setLocation(location)
	return host
	
def createHosts( baseName, startNumber, count, services, ipBase, dryRun, macAddresses, location, ipmiIpBase, ipmiMacAddresses, chassis ):
	hosts = HostList()
	hostNumbers = {}
	print "Creating %i hosts %s%s%s" % (
		count, 
		chassis and 'chassis %s ' % chassis.name() or '',
		location and 'at location %s ' % location.name() or '',
		services and 'with services %s' %services.services().join(',') or '')
	
	digits = 3
	if baseName.endswith('0'):
		digits = 0
		while baseName.endswith('0'):
			digits += 1
			baseName = baseName[:-1]
	
	if startNumber is not None:
		for i in range(startNumber,startNumber+count):
			hostName = baseName + (('%0' + str(digits) + 'i') % i)
			hostNumbers[hostName] = i
			if not ipBase:
				print hostName
			hosts += initHost(hostName,location)
	else:
		hosts.append( initHost(baseName) )
	
	if not dryRun:
		hosts.commit()
	
	if services:
		hostServices = HostServiceList()
		for host in hosts:
			for service in services:
				hs = HostService()
				hs.setService(service)
				hs.setHost(host)
				hs.setEnabled(True)
				hostServices += hs
				
		if not dryRun:
			hostServices.commit()
	
	if ipBase:
		hostInterfaces = HostInterfaceList()
		for i, host in enumerate(hosts):
			hi = HostInterface()
			def ip(base):
				ip = base
				if startNumber is not None:
					ip += str(hostNumbers[str(host.name())])
				return ip
			hi.setIp( ip(ipBase) )
			hi.setHost( host )
			hi.setValue( 'fkeyhostinterfacetype', QVariant(2) )
			print host.name(), ip(ipBase)
			if macAddresses:
				print macAddresses[i], 
				hi.setMac( macAddresses[i] )
			hostInterfaces += hi
			if ipmiMacAddresses:
				print ip(ipmiIpBase), ipmiMacAddresses[i]
				ipmiHi = HostInterface()
				ipmiHi.setIp( ip(ipmiIpBase) )
				ipmiHi.setHost( host )
				ipmiHi.setValue( 'fkeyhostinterfacetype', QVariant(4) )
				ipmiHi.setMac( ipmiMacAddresses[i] )
				hostInterfaces += ipmiHi
			else:
				print 
		if not dryRun:
			hostInterfaces.commit()

def buildOptionsParser():
	parser = OptionParser(usage="usage: %prog [options]")
	parser.add_option( "-n", "--host-name", 		dest="hostName", 		default=None, help="Base name of the host records to be created, the host number will be appended.  Defaults to 3 digits, append 0's to indicate number of digits, ie ajax00 for 2 digits." )
	parser.add_option( "-i", "--ip-base", 			dest="ipBase", 			default=None, help="If specified, hostinterface records will be created.  The last number will match the number of the host if the --start-number is included and should be in the format 192.168.0., otherwise should be the full ip address.  If you specify 'copy', it will copy the base from the host specified with the -e option." )
	parser.add_option( "-s", "--service-list", 		dest="serviceList", 	default=None, help="Comma separated list of service names to add hostservice records." )
	parser.add_option( "-e", "--copy-from-host", 	dest="copyFromHost", 	default=None, help="Copies the services and optionally ip-base from the the specified host" )
	parser.add_option( "-b", "--start-number", 		dest="startNumber", 	default=None, 	help="The first number to create, if this is excluded a single host will be created" )
	parser.add_option( "-c", "--count", 			dest="count", 			default=1, 	help="The number of host records to create" )
	parser.add_option( "-d", "--dry-run", 			dest="dryRun", 			action="store_true", default=None, help="Print the host names but dont actually commit" )
	parser.add_option( "-m", "--mac-addresses",		dest="macAddresses",	default=None, help="File path, or comma/newline separate list of mac addresses, one per host" )
	parser.add_option( "-l", "--location", 			dest="location", 		default=None, help="Set Location of hosts, must exist in location table." )
	parser.add_option( "-C", "--chassis",			dest="chassis",			default=None, help="Set Chassis, by host name." )
	parser.add_option( "-M", "--ipmi-mac-addresses",dest="ipmiMacAddresses",default=None, help="IPMI Mac addreses." )
	parser.add_option( "-I", "--ipmi-ip-base", 		dest="ipmiIpBase", 		default=None, help="If specified, hostinterface records will be created for ipmi.  The last number will match the number of the host if the --start-number is included and should be in the format 192.168.0., otherwise should be the full ip address.  If you specify 'copy', it will copy the base from the host specified with the -e option." )
	return parser

def main():
	parser = buildOptionsParser()
	parser.parse_args()
	opts = parser.values
	
	macAddresses = None
	ipmiMacAddresses = None
	chassis = None
	location = None
	services = ServiceList()
	count = int(opts.count)
	if opts.hostName is None:
		parser.error( "hostName is required" )
	if opts.location:
		location = (Location.c.Name == opts.location).select()[0]
		if not location.isRecord():
			parser.error( "Location '%s' not found in database" % opts.location )
	def verifyMacs( macAddressesString, label='' ):
		if os.path.exists( macAddressesString ):
			macAddressesString = open( macAddressesString, 'r' ).read()
		macAddresses = re.split(r'[\n,]',macAddressesString.strip())
		if len(macAddresses) != count:
			print macAddresses, macAddressesString
			parser.error( "Number of %smac addresses %i doesn't match number of hosts to create %i" % (label, len(macAddresses), count) )
		for ma in macAddresses:
			if not re.match( r'^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$', ma, re.I ):
				parser.error( "Invalid mac address: " + ma )
		return macAddresses
	if opts.macAddresses:
		macAddresses = verifyMacs( opts.macAddresses )
	if opts.ipmiMacAddresses:
		ipmiMacAddresses = verifyMacs( opts.ipmiMacAddresses, 'ipmi ' )
	if opts.chassis:
		chassis = Host.recordByName(opts.chassis)
		if not chassis.isRecord():
			parser.error( "Chassis not found: " + chassis )
	if opts.serviceList:
		for serviceName in opts.serviceList.split(','):
			service = Service.recordByName( serviceName )
			if service.isRecord():
				services += service
	if opts.copyFromHost:
		host = Host.recordByName( opts.copyFromHost )
		if host.isRecord():
			print "Copying services from host:", opts.copyFromHost
			services += host.hostServices().services().filter("enabled",True)
			if opts.ipBase == 'copy':
				hostInterfaces = host.hostInterfaces()
				if hostInterfaces:
					stripIp = lambda ip: ip[:ip.rfind('.')+1]
					opts.ipBase = stripIp(str(hostInterfaces[0].ip()))
		else:
			print "Unable to find host: " + opts.copyFromHost
	createHosts( opts.hostName, opts.startNumber and int(opts.startNumber) or None, count, services, opts.ipBase, opts.dryRun, macAddresses, location, opts.ipmiIpBase, ipmiMacAddresses, chassis )

if __name__=="__main__":
	main()
