
from __future__ import print_function
import os
import sys
import string
import re
import shutil
import traceback
import pickle as cPickle
from .defaultdict import *

All_Targets = []
Targets = []
Config_Replacement_File = None
Args = []
Generated_Installers = {}
Generated_DebugFiles = []
Parser = None

try:
	import subprocess
except:
	import popen2

# Make some attempt to encode data properly when writing to a pipe
if sys.stdout.encoding is None:
	import codecs, locale
	sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout)

class TerminalControllerDummy:
	def render(self,text):
		return re.sub( r'\${\w+}', '', text )
try:
	from termctl import TerminalController
except:
	TerminalController = TerminalControllerDummy

term = TerminalControllerDummy()

def find_targets(name):
	ret = []
	for t in All_Targets:
		if t.name == name or re.match('^' + name + '$',t.name):
			ret.append(t)
	return ret

def find_target(name):
	return find_targets(name)[0]

def add_target(target):
	try:
		if find_target(target) == target:
			# Same target added twice
			pass
		else:
			raise ("Adding target with duplicate name " + target.name)
	except:
		All_Targets.append(target)


# Returns a tuple containing (returncode,stdout)
# Cmd can be a string or a list of args
def cmd_output(cmd,outputObject=None,shell=None):
	p = None
	outputFd = None
	pollRunVal = None
	if shell is None:
		shell = sys.platform != 'win32'
	try:
		if 'subprocess' in globals():
			p = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.STDOUT,shell=shell)
			outputFd = p.stdout
		else:
			p = popen2.Popen4(cmd)
			pollRunVal = -1
			outputFd = p.fromchild
	except Exception as e:
		print( "Error starting command: " + str(cmd) )
		raise e
	
	output = ''
	ret = 0
	def processOutput(existing, new, outputProgress):
		new = new.decode('utf-8')
		existing += new
		if outputProgress:
			outputObject.output(new)
		return existing
	while True:
		ret = p.poll()
		if ret == pollRunVal:
			output = processOutput(output,outputFd.readline(),outputObject)
		else:
			break
	
	return (ret,processOutput(output,outputFd.read(),outputObject))

# Baseclass for all targets
class Target:
	def __init__(self,name=None,dir=None,pre_deps=None,post_deps=None):
		self.name = name
		self.dir = dir
		self.pre_deps = pre_deps or []
		self.post_deps = post_deps or []
		self.built = False
		self.args = []
		self.output_in_warning = False
		# "Registers" this target
		add_target(self)
		
	def __repr__(self):
		return self.name
	
	# Returns true if this target has already been built
	# Some targets have different build steps, and can
	# return different values depending on the args
	def is_built(self):
		return self.built
	
	def apply_arg(self,arg):
		if arg is not None:
			try:
				for a in arg: self.args.append(a)
			except:
				self.args.append(arg)
		
	# Returns true if the argument list contains arg or
	# contains TARGET:arg
	def has_arg(self,arg):
		# Local args
		for a in self.args:
			if arg == a:
				return True
		# Global args
		sa = '-'+arg
		for a in Args:
			if arg == a or sa == a:
				return True
		return False
	
	# This is called if a target is selected for building, before any
	# build steps happen for any targets.  It lets a target modify
	# global args or other selected targets
	def selected(self):
		pass
	
	# Returns a string to execute using os.system to complete the target
	# Simplified method for creating simpler targets that
	# dont need to run multiple commands or study any output
	#
	# The return value of the command indicates the whether
	# the target was completed. 0 for success
	def command(self):
		return ''
	
	def output(self,output):
		for line in output.splitlines():
			if self.has_arg('verbose'):
				self.output_line(line)
			elif self.has_arg('progress'):
				self.progress_line(line)
			else:
				self.silent_line(line)

	# Override for coloring
	def output_line(self,line):
		print(line)
	
	# Override to match output that should be shown in silent mode
	def silent_line(self,line):
		pass
	
	# Override to match output that should be shown in progress mode
	def progress_line(self,line):
		if self.output_in_warning:
			if line == '':
				self.output_in_warning = False
			self.output_line(line)
		
		# Remove control code, probably related to color output
		if line.startswith(chr(27)):
			line = line[4:]
		
		match = re.match("^(g\+\+|gcc|clang)",line)
		if match:
			self.output_in_warning = False
			# Compile
			if re.search(r"\s-c\s",line):
				match = re.search(r'([\w\._-]+)\s*$',line)
				if match:
					return self.output_line("Compiling " + match.group(1))
			# Link
			else:
				match = re.search(r'-o\s+([\w\._-]+)',line)
				if match:
					return self.output_line("Linking " + match.group(1))
		
		if self.has_arg('warn') and 'warning' in line:
			self.output_in_warning = True
			return self.output_line(line)
			
		
		match = re.match(r"^\S+uic\s+(\S+)",line)
		if match:
			self.output_in_warning = True
			return self.output_line("Uic" + match.group(1))

		self.silent_line(line)

	def cmd_error(self,cmd,output):
		if not self.has_arg('verbose'):
			print( output, )
		print( term.render("${RED}Error Building Target${NORMAL}: %s, cmd was: %s" % (self.name,cmd)) )
		raise Exception()
		
	def run_cmd(self,cmd,shell=None,noThrow=False):
		if self.has_arg('verbose') or self.has_arg('show-commands'):
			print( term.render('${BLUE}Running Command${NORMAL}:'), str(cmd) )
		try:
			(ret,output) = cmd_output(cmd,self,shell)
		except:
			print( "Exception while running cmd:", cmd )
			traceback.print_exc()
			raise
		if ret and not noThrow:
			self.cmd_error(cmd,output)
		return (ret,output)
	
	# Used by run_make if other than None
	def makefile(self):
		return None
	
	def run_make(self, arg_string=''):
		make_cmd = 'make'
		if 'QMAKESPEC' in os.environ and 'msvc' in os.environ['QMAKESPEC']:
			make_cmd = 'nmake'
		makefile = self.makefile()
		if makefile is not None:
			make_cmd += ' -f %s' % makefile
		if arg_string and arg_string[0] != ' ':
			arg_string = ' ' + arg_string
		# slightly less shitty quick put sudo in front of install if not root
		if self.has_arg( 'sudo_install' ):
			if re.match( r'^\s*install', arg_string ) and os.getuid():
				make_cmd = 'sudo '+make_cmd
		return self.run_cmd(make_cmd + arg_string)

	# Central function of a Target, responsible for completing the target.
	# Raises an exception if it cannot complete the target
	def build_run(self):
		cmd = self.command()
		if cmd and len(cmd):
			self.run_cmd(cmd)
	
	# This is used to check whether the given target is buildable.
	# A target may only be buildable on certain systems, or when
	# certain requirements are met
	def is_buildable(self):
		return True
	
	# This builds all the dependencies for this target
	# If the 'skip-ext-deps' option is passed, targets
	# that are specified as string are ignored, otherwise
	# a string target is looked up in the All_Targets list
	# with the find_target function.
	def build_deps(self,deps):
		skipext = self.has_arg('skip-ext-deps')
		for d in deps:
			if isinstance(d,str) and not skipext:
				if ':' in d:
					parts = d.split(':')
					#local_args.append(d)
					d = parts[0]
				try:
					d = find_target(d)
				except:
					raise Exception("Target.build_deps: couldn't find dependancy: %s for target: %s" % (d, self.name))
			if isinstance(d,Target):
				d.build()
	
	# Builds the target using by building the deps and callign build_run
	# Skips build if name:skip exists in the args list.  Changes directories
	# to the target directory if there is one
	def build(self):
		if self.has_arg('skip') or self.is_built():
			return
		if not self.is_buildable():
			return
		self.build_deps(self.pre_deps)
		cwd = os.getcwd()
		nwd = os.path.join(cwd,self.dir)
		print( term.render("${YELLOW}Building${NORMAL}: %s\t\t%s" % (self.name, nwd)) )
		#print "Target.build: doing ", self.name
		#print "Target.build: chdir to ", nwd
		os.chdir( nwd )
		self.build_run()
		os.chdir(cwd)
		self.built = True
		self.build_deps(self.post_deps)

	QMAKE_CONFIG = None
	@classmethod
	def qmake_var(cls,var):
		if cls.QMAKE_CONFIG is None:
			(ret,out) = cmd_output('qmake -query')
			cls.QMAKE_CONFIG = {}
			for line in out.splitlines():
				parts = line.split(':')
				cls.QMAKE_CONFIG[parts[0]] = len(parts) > 1 and ':'.join(parts[1:]) or ''
		return cls.QMAKE_CONFIG[var]

	@classmethod
	def qt_major(cls):
		return int(cls.qmake_var('QT_VERSION').split('.')[0])

	def platform(self):
		plat = 'Unknown'
		if 'QMAKESPEC' in os.environ:
			plat = os.environ['QMAKESPEC']
		if self.has_arg('X86_64'):
			plat += '_64'
		return plat

	def compiler(self):
		plat = self.platform()
		if plat == 'Unknown':
			return None
		return plat[plat.index('-')+1:]
	
# Executes a static command inside dir
class StaticTarget(Target):
	def __init__(self,name,dir,cmd,pre_deps=[],post_deps=[],shell=False):
		Target.__init__(self,name,dir,pre_deps,post_deps)
		self.cmd = cmd
		self.shell = shell
		
	def command(self):
		return self.cmd

	def build_run(self):
		cmd = self.command()
		if cmd and len(cmd):
			self.run_cmd(cmd,shell=self.shell)

# Copies a single file
class CopyTarget(Target):
	def __init__(self,name,dir,src,dest):
		Target.__init__(self,name,dir)
		self.Source = src
		self.Dest = dest
		
	def build_run(self):
		shutil.copyfile(self.Source,self.Dest)

# Run configure.py, make, and optionally make install
# for building sip targets, these are python bindings
# including pyqt, pystone, pyclasses, various application
# interfaces, and whatever else is added...
class SipTarget(Target):
	def __init__(self,name,dir,static=False,platform=None,pre_deps=[]):
		Target.__init__(self,name,dir,pre_deps)
		self.Static = static
		self.Platform = platform
		self.CleanDone = False
		self.InstallDone = False
	
	def is_built(self):
		if self.has_arg('clean') and not self.CleanDone:
			return False
		if self.has_arg('install') and not self.InstallDone:
			return False
		return Target.is_built(self)
	
	def configure_command(self):
		config = "python configure.py"
		if self.Static:
			config += " -k"
		if self.Platform:
			config += " -p " + self.Platform
		if self.has_arg("debug"):
			config += " -u"
		if self.has_arg("trace"):
			config += " -r"
		return config
		
	def build_run(self):
		self.run_cmd(self.configure_command())
		if self.has_arg('clean') and not self.CleanDone:
			self.run_make('clean')
			self.CleanDone = True
			self.built = False
			self.InstallDone = False
		self.run_make()
		if self.has_arg('install') and not self.InstallDone:
			cmd = 'install'
			try:
				pos = args.index('-install-root')
				cmd += ' INSTALL_ROOT=' + args[pos+1]
			except: pass
			self.run_make(cmd)
			self.InstallDone = True

# Run configure.py, make, and optionally make install
# for building sip targets, these are python bindings
# including pyqt, pystone, pyclasses, various application
# interfaces, and whatever else is added...
class SipTarget2(Target):
	
	def __init__(self,name,dir,sipName,static=False,platform=None,pre_deps=[],parentModule='blur'):
		Target.__init__(self,name,dir,pre_deps)
		self.SipName = sipName
		self.Static = static
		self.Platform = platform
		self.CleanDone = False
		self.InstallDone = False
		self.SipConfig = None
		self.ExtraIncludes = []
		self.ExtraLibs = []
		self.ExtraLibDirs = []
		self.ExtraSipFlags = []
		self.ParentModule = parentModule
		
	def is_built(self):
		if self.has_arg('clean') and not self.CleanDone:
			return False
		if self.has_arg('install') and not self.InstallDone:
			return False
		return Target.is_built(self)

	# Example 'Xml' -> 'QtXml' or 'Qt5Xml'
	def qt_library(self,library):
		if self.qt_major() == 5:
			return 'Qt5' + library
		if sys.platform=='win32':
			return 'Qt' + library + '4'
		return 'Qt' + library
	
	def __getstate__(self):
		odict = self.__dict__.copy() # copy the dict since we change it
		odict['SipConfig'] = None
		return odict

	def sip_config(self):
		if not self.SipConfig:
			import sipconfig
			self.SipConfig = sipconfig.Configuration()
		return self.SipConfig

	def sip_flags(self):
		if self.qt_major() == 5:
			import PyQt5.QtCore
			return PyQt5.QtCore.PYQT_CONFIGURATION['sip_flags']
		else:
			import PyQt4.pyqtconfig as pyqtconfig
			return pyqtconfig.Configuration().pyqt_sip_flags
		return ''
	
	def moduleName(self):
		return self.SipName
	
	def build_dir(self):
		return 'sip' + self.moduleName()[0].upper() + self.moduleName()[1:]
	
	def build_file(self):
		return '%s.sbf' % self.SipName.lower()

	def sip_includes(self):
		ret = []
		sip_install_prefix = self.sip_config().default_sip_dir
		sp = lambda rel: os.path.normpath(sip_install_prefix + rel)
		# Need to read our own sip files from where they are installed
		if 'DESTDIR' in os.environ:
                        destSipDir = os.path.join(os.environ['DESTDIR'],sip_install_prefix.lstrip('/')) 
			ret += [destSipDir,os.path.join(destSipDir,'PyQt4')]
		if sys.version > '3':
			return ret + [ sp('/PyQt5'), sp('') ]
		return ret + [ sp('/PyQt4'), sp('') ]
	
	def sip_bin(self):
		path = self.sip_config().sip_bin
		if sys.platform == 'win32':
			path += '.exe'
		return path
	
	def sip_command(self):
		import PyQt4.pyqtconfig as pyqtconfig
		bdir = self.build_dir()
		if not os.path.exists(bdir):
			os.mkdir(bdir)
		args = [ self.sip_bin(), "-g", "-e", "-c", self.build_dir(), "-b", '%s/%s' % (bdir,self.build_file()), self.sip_flags()]
		for inc in self.sip_includes():
			args += ['-I',inc]
		args += self.ExtraSipFlags
		args.append( "sip/%s.sip" % self.SipName.lower() )
		return " ".join(args)
	
	def CustomModuleMakefileClass(self):
		import sipconfig
		isQt5 = self.qt_major() == 5
		color = self.has_arg('color')
		class StoneModuleMakefile(sipconfig.SIPModuleMakefile):
			# Override target for static builds
			def finalise(self):
				if sys.platform != 'win32':
					if color:
						cc = self.required_string('CC')
						cxx = self.required_string('CXX')
						if cc == 'clang':
							self.CFLAGS.append('-fcolor-diagnostics')
						if cxx == 'clang++':
							self.CXXFLAGS.append('-fcolor-diagnostics')
						if cc == 'gcc':
							self.CFLAGS.append('-fdiagnostics-color=always')
						if cxx == 'g++':
							self.CXXFLAGS.append('-fdiagnostics-color=always')
					self.extra_cxxflags.append( '-std=c++11' )
					if isQt5:
						self.CFLAGS.extend(self.optional_list("CFLAGS_SHLIB"))
						self.CXXFLAGS.extend(self.optional_list("CXXFLAGS_SHLIB"))
				sipconfig.SIPModuleMakefile.finalise(self)
				if self.static:
					if self._target.startswith('lib'):
						self._target = 'libpy' + self._target[3:]
					else:
						self._target = 'py' + self._target

		return StoneModuleMakefile
	
	def makefile(self):
		return 'Makefile.sip'
	
	def build_makefiles(self):
		import sipconfig
		import glob
		config = self.sip_config()
		
		makefile = self.CustomModuleMakefileClass()(
			configuration=config,
			build_file=self.build_file(),
			static=self.Static,
			debug=self.has_arg('debug'),
			install_dir=os.path.join(config.default_mod_dir,self.ParentModule),
			dir=self.build_dir()
		)
		
		installs = []
		sipfiles = []

		for s in glob.glob("sip/*.sip"):
			sipfiles.append(os.path.join("sip", os.path.basename(s)))

		installs.append([sipfiles, os.path.join(config.default_sip_dir, self.ParentModule, self.moduleName())])

		#installs.append(["stoneconfig.py", config.default_mod_dir])

		sipconfig.ParentMakefile(
			configuration=config,
			installs=installs,
			subdirs=[self.build_dir()],
			makefile=self.makefile()
		).generate()

		makefile.extra_libs = map(self.qt_library, ['Core','Gui','Sql','Xml','Network'])
		makefile.extra_libs += self.ExtraLibs
		
		qth = lambda rel: os.path.normpath(self.qmake_var('QT_INSTALL_HEADERS') + rel)
		makefile.extra_include_dirs = ['..',"../include",'../.out',qth('/QtCore/'),qth('/QtGui/'),qth('/QtOpenGL/'),qth(''),
								qth('/QtWidgets/'),qth('/QtSql/'), qth('/QtNetwork/')]
		if sys.version < '3':
			makefile.extra_include_dirs += [qth('/QtXml/')]
		makefile.extra_include_dirs += self.ExtraIncludes
		
		makefile.extra_lib_dirs += [ "..", os.path.normpath(self.qmake_var('QT_INSTALL_LIBS')) ]
		makefile.extra_lib_dirs += self.ExtraLibDirs

		# Generate the Makefile itself.
		makefile.generate()

	def build_run(self):
		gen = self.has_arg('gen')
		full = self.has_arg('full')
		build = self.has_arg('build')
		if gen or full:
			self.run_cmd(self.sip_command())
			self.build_makefiles()
		if self.has_arg('clean') and not self.CleanDone:
			self.run_make('clean')
			self.CleanDone = True
			self.built = False
			self.InstallDone = False
		if build or full:
			self.run_make()
		if self.has_arg('install') and not self.InstallDone:
			cmd = 'install'
			try:
				pos = args.index('-install-root')
				cmd += ' INSTALL_ROOT=' + args[pos+1]
			except: pass
			self.run_make(cmd)
			self.InstallDone = True

# Builds a Qt Project(.pro) using qmake and make
# Runs qmake with optional debug and console
# Runs make clean(optional), make, and make install(optional)
#
# make clean is done if args contains 'clean' or 'TARGET:clean'
# make install is done if args contains 'install' or 'TARGET:install' 
class QMakeTarget(Target):
	def __init__(self,name,dir,target=None,pre_deps=[],post_deps=[],Static=False):
		Target.__init__(self,name,dir,pre_deps,post_deps)
		self.Static = Static
		self.Target = target
		self.UicOnly = False
		self.Defines = []
		self.ConfigDone = False
		self.CleanDone = False
		self.BuildDone = False
		self.InstallDone = False
		
	def is_built(self):
		if self.UicOnly and self.built:
			return True
		if self.has_arg('clean') and not self.CleanDone:
			return False
		if self.has_arg('install') and not self.InstallDone:
			return False
		return self.ConfigDone and self.BuildDone
	
	# Returns the arguments to qmake
	# Override if you need special args
	def qmakeargs(self):
		Args = []
		if self.Static:
			Args.append("CONFIG+=staticlib")
		if self.has_arg("debug")>0:
			Args.append("CONFIG+=debug")
		else:
			Args.append("CONFIG-=debug")
		if self.has_arg("console"):
			Args.append("CONFIG+=console")
		for d in self.Defines:
			Args.append("DEFINES+=\"" + d + "\"")
		Args.append("COMPILER=" + self.compiler())
		if self.Target:
			Args.append(self.Target)
		Args.append( os.environ.get( 'BLUR_QMAKE_FLAGS', '' ) )
		return ' '.join(Args)
	
	def find_debug_files(self):
		import glob
		files = glob.glob(os.path.join(self.dir,'*.pdb'))
		for file in files:
			Generated_DebugFiles.append( os.path.join(self.dir,file) )
	
	# Runs qmake, make clean(option), make, make install(option)
	def build_run(self):
		build = self.has_arg('build')
		full = self.has_arg('full')
		config = self.has_arg('config')
		if (config or full) and not self.ConfigDone:
			cmd = "qmake " + self.qmakeargs()
			self.run_cmd(cmd)
			self.ConfigDone = True
		if self.has_arg("clean") and not self.CleanDone:
			self.run_make('clean')
			self.BuildDone = self.InstallDone = False
			self.CleanDone = True
		if self.UicOnly:
			if sys.platform=='win32':
				self.run_make("-f Makefile.release compiler_uic_make_all")
			else:
				self.run_make("compiler_uic_make_all")
			return
		if (build or full) and not self.BuildDone:
			self.run_make()
			self.find_debug_files()
			self.BuildDone = True
		if not self.InstallDone and self.has_arg('install'):
			self.run_make('install')
			self.InstallDone = True

class QTestTarget(Target):
	def __init__(self,name,dir,file,pre_deps=[],post_deps=[]):
		if file.endswith('.pro'):
			pre_deps.append(QMakeTarget(name + '_build',dir,file))
			file = file[:-4]
		Target.__init__(self,name,dir,pre_deps,post_deps)
		self.file = file

	def build_run(self):
		cmd = os.path.join(self.dir,self.file)
		if cmd.endswith('.py'):
			cmd = 'python ' + cmd
		self.run_cmd(cmd)

	def output_line(self,line):
		if re.match(r'^Totals:',line):
			line = re.sub(r'(\d+ passed)',r'${GREEN}\1${NORMAL}',line)
			print(term.render(line))
		elif re.match(r'^(?:FAIL|PASS)',line):
			print(term.render('${%s}%s${NORMAL}' % (line[:4] == 'FAIL' and 'RED' or 'GREEN', line[:4])) + line[4:])
		elif self.has_arg('verbose'):
			print(line)

	def progress_line(self,line):
		if re.match(r'^PASS',line):
			self.output_line(line)
		Target.progress_line(self,line)

	def silent_line(self,line):
		if re.match(r'^Totals:',line):
			self.output_line(line)

# Finds the makensis command and runs it on the specified file
# Nullsoft install - used to generate windows installers
class NSISTarget(Target):
	if sys.platform=='win32':
		NSIS_PATHS = ["C:/Program Files (x86)/NSIS/","C:/Program Files/NSIS/","E:/Program Files (x86)/NSIS/","E:/Program Files/NSIS/"]
		NSIS = "makensis.exe"
		CanBuild = True
	else:
		CanBuild = False
	def __init__(self,name,dir,file,pre_deps=[],makensis_extra_options=[], revdir=None):
		Target.__init__(self,name,dir,pre_deps)
		self.File = file
		self.ExtraOptions = makensis_extra_options
		self.RevDir = revdir
		if not self.RevDir:
			self.RevDir = dir
			
	# Only buildable on win32
	def is_buildable(self):
		return NSISTarget.CanBuild

	def find_nsis(self):
		for p in self.NSIS_PATHS:
			if os.access(p,os.F_OK):
				return p + "makensis.exe"
		raise Exception("Couldn't find nsis cmd, searched " + str(self.NSIS_PATHS))
		return None
	
	def makensis_options(self):
		p = self.find_nsis()
		file = os.getcwd() + "/" + self.File
		cmd_parts = [p]
		platform = self.platform()
		if platform != 'Unknown':
			cmd_parts.append( '/DPLATFORM=%s' % self.platform() )
			compiler = self.compiler()
			if compiler:
				cmd_parts.append( '/DCOMPILER=%s' % compiler )
		rev = GetRevision(self.RevDir)
		cmd_parts.append( '/DREVISION=%s' % rev )
		if self.ExtraOptions:
			cmd_parts += self.ExtraOptions
		cmd_parts.append(file)
		return cmd_parts
		
	def build_run(self):
		cmd_parts = self.makensis_options()
		(ret,output) = self.run_cmd( cmd_parts )
		outputFile = None
		for line in output.splitlines():
			outputMatch = re.match( r'Output:\s+"(.*)"', line )
			if outputMatch:
				outputFile = outputMatch.group(1)
		if outputFile is None:
			raise ("Unable to parse output file from output\n"+output)
		Generated_Installers[self.name] = outputFile
		if self.has_arg('install'):
			if self.has_arg('progress') or self.has_arg('-verbose'):
				print( term.render("${YELLOW}Installing${NORMAL}"), outputFile )
			self.run_cmd( [outputFile,'/S'] )

# Finds the subwcrev.exe program
# Part of toroisesvn used to get revision info for a file/dir
def find_wcrev():
	WCREV_PATHS = ["C:/Program Files/TortoiseSVN/bin/","C:/Program Files (x86)/TortoiseSVN/bin/","C:/blur/TortoiseSVN/bin/"]
	for p in WCREV_PATHS:
		if os.access(p,os.F_OK):
			return p + "/" + "subwcrev.exe"
	raise "Couldn't find subwcrev.exe, searched " + WCREV_PATHS
	return None

# Returns (Last committed rev, updated rev(can be range XX:XXX), local modifications bool)
def GetRevision_nocache(dir):
	lastCommRev = None
	updatedRev = None
	localMods = False
	if sys.platform == 'win32':
		wcrev = find_wcrev()
		(ret,output) = cmd_output([wcrev,dir])
		m = re.search("Last committed at revision (\d+)",output)
		if m:
			lastCommRev = m.group(1)
		m = re.search("Updated to revision (\d+)",output)
		if m:
			updatedRev = m.group(1)
		m = re.search("Mixed revision range (\d+:\d+)",output)
		if m:
			updatedRev = m.group(1)
		if re.search("Local modifications found",output):
			localMods = True
	else:
		(ret,output) = cmd_output('svnversion ' + dir)
		if 'Unversioned directory' in output:
			return (0,0,False)
		m = re.search(r"(\d+(?::\d+)?)(M?)",output)
		if m:
			updatedRev = m.group(1)
			localMods = m.group(2) == 'M'
		else:
			raise Exception("Couldn't parse valid revision(%s,%r): %s" % (updatedRev,localMods,output))
		(ret,output) = cmd_output('svnversion -c ' + dir)
		m = re.search(r"(?:\d+:)?(\d+)M?$", output)
		if m:
			lastCommRev = m.group(1)
	if lastCommRev is None or updatedRev is None:
		raise Exception("Couldn't parse valid revision(%s,%s,%r): %s" % (lastCommRev,updatedRev,localMods,output))
	return (lastCommRev, updatedRev, localMods)
	
# Gets the svn revision from the current directory
rev_cache = {}
def GetRevisionInfo(dir):
	global rev_cache
	if dir in rev_cache:
		return rev_cache[dir]
	rev = GetRevision_nocache(dir)
	if rev:
		rev_cache[dir] = rev
	return rev

def GetRevision(dir):
	return GetRevisionInfo(dir)[1]

# Takes a template file, runs subwcrev.exe on it and outputs to output
class WCRevTarget(Target):
	def __init__(self,name,dir,revdir,input,output):
		Target.__init__(self,name,dir)
		self.Input = input
		self.Output = output
		self.Revdir = revdir
	
	def build_run(self):
		if sys.platform == 'win32':
			p = find_wcrev()
			self.run_cmd( [p,self.Revdir,self.Input,self.Output,"-f"] )
		else:
			rev = GetRevision(self.Revdir)
			self.run_cmd( "cat " + self.Input + " | sed 's/\\$WCREV\\$/" + str(rev) + "/' > " + self.Output )

# Takes a template .ini file, and sets certain keys based the inputed IniConfig object
class IniConfigTarget(Target):
	def __init__(self,name,dir,template_ini,output_ini,config = None):
		Target.__init__(self,name,dir)
		self.TemplateIni = template_ini
		self.OutputIni = output_ini
		self.Config = config
		
	def build_run(self):
		replacementsBySection = DefaultDict(dict)
		if not self.Config:
			self.Config = Config_Replacement_File
		section = "Default"
		replacementsFile = None
		try:
			replacementsFile = open(self.Config)
		except:
			print ("Failure Reading %s for config replacement" % str(self.Config))
		if replacementsFile:
			for line in replacementsFile:
				matchSection = re.match("^\[(.*)\]$",line)
				if matchSection:
					section = matchSection.group(1)
					continue
				matchKV = re.match("^([^=]*)=(.*)$",line)
				if matchKV:
					replacementsBySection[section][matchKV.group(1)] = matchKV.group(2) 
		
		outLines = []
		section = "Default"
		for line in open(self.TemplateIni):
			matchSection = re.match("^\[(.*)\]$",line)
			if matchSection:
				section = matchSection.group(1)
				outLines.append(line)
				continue
			matchKV = re.match("^([^=]*)=(.*)$",line)
			if matchKV:
				key = matchKV.group(1)
				if key in replacementsBySection[section]:
					outLines.append( "%s=%s\n" % (key, replacementsBySection[section][key] ) )
					continue
			outLines.append(line)
		
		open(self.OutputIni,"w").write( ''.join(outLines) )

# Copies a file, replacing part of the name with the
# svn revisision number from revdir, which is relative
# to dir(or absolute)
class RevCopyTarget(Target):
	def __init__(self,name,dir,revdir,src,dest):
		Target.__init__(self,name,dir)
		self.RevDir = revdir
		self.Source = src
		self.Dest = dest
	
	def build_run(self):
		rev = GetRevision(self.RevDir)
		dest = re.sub("{REVSTR}",rev,self.Dest)
		shutil.copyfile(self.Source,dest)

class LibVersionTarget(Target):
	def __init__(self,name,dir,revdir,library):
		Target.__init__(self,name,dir)
		self.RevDir = revdir
		self.Library = library
		
	def build_run(self):
		if sys.platform == 'win32':
			rev = GetRevision(self.RevDir)
			shutil.copyfile(self.Library+".dll",self.Library+str(rev)+".dll")
			shutil.copyfile("lib"+self.Library+".a","lib" + self.Library+str(rev)+".a")
		else:
			return

class LibInstallTarget(Target):
	def __init__(self,name,dir,library,dest):
		Target.__init__(self,name,dir)
		self.Library = library
		self.Dest = dest

	def build_run(self):
		libname = self.Library
		dest = self.Dest
		if sys.platform == 'win32':
			libname = libname + '.dll'
		else:
			libname = 'lib' + libname + '.so'
		destpath = dest + libname
		shutil.copyfile(libname,destpath)

class RPMTarget(Target):
	CanBuildRpms = None
	"""	# This class is used to build rpms
		# It currently has 5 steps
		# 1. Create a tarball named packageName-Version-rRevision.tgz in /usr/src/redhat/SOURCES/
		# 2. Create a spec file, uses a template and replaces $VERSION$ and $WCREV$ in /usr/src/redhat/SPECS/
		# 3. Run rpmbuild on the created spec file.
		# 4. Parse the output from rpmbuild to generate a list of created rpms
		# 5(optional, depeding on install arg). Install the rpms, except for debug-info and source rpm
	"""
	def __init__(self,targetName,packageName,dir,specTemplate,version,excludeSipFiles=True):
		"""  for targetName and dir refer to Target docs
		"    packageName is the name of the .tgz package ie  packageName-Version-rRevision.tgz
		"	 specTemplate is the path to the spec template file, relative to the file the RPMTarget is
		"    	constructed in
		"    version is the Version for the rpm, the revision is taken using GetRevision on dir
		"""
		Target.__init__(self,targetName,dir)
		self.SpecTemplate = specTemplate
		self.Version = version
		self.Revision = None
		self.PackageName = packageName
		self.BuiltRPMS = []
		self.InstallDone = False
		self.BuildRoot = '/usr/src/redhat/'
		# Relative to dir/..
		self.TargetDirName = os.path.split(dir)[1]
		# Set this to transform the top-level-dir name
		# in the tar archive
		self.OutputDirName = "%s-%s" % (packageName, self.Version)
		self.Files = ['.'] 
		self.Excludes = ['.svn','.git','*.o','*.pyc','*.pyo','moc_*','ui_*','*.so*','*.a','sip_*','*.log','qrc_*','Makefile*',
				   'sip' + self.TargetDirName[0].upper() + self.TargetDirName[1:]]
		if excludeSipFiles:
			self.Excludes += [ 'sip' ]
		try:
			if os.path.exists('/etc/redhat-release'):
				rhr = open('/etc/redhat-release','r').read()
				match = re.search( r'(\w+) release ([\d\.]+)', rhr)
				if match:
					prod = match.group(1)
					ver = float(match.group(2))
					if prod == 'Fedora':
						self.BuildRoot = '~/rpmbuild/'
					elif ver >= 6.0:
						self.BuildRoot = '/root/rpmbuild/'
		except: pass
			
	# Only buildable on linux, this should probably check for the existance
	# of rpmbuild and other required commands.
	def is_buildable(self):
		if RPMTarget.CanBuildRpms == None:
			if sys.platform == 'win32':
				RPMTarget.CanBuildRpms = False
				return False
			RPMTarget.CanBuildRpms = True
		return RPMTarget.CanBuildRpms
	
	def is_built(self):
		if self.has_arg('install') and not self.InstallDone:
			return False
		return Target.is_built(self)
	
	def build_run(self):
		if not self.built:
			self.Revision = GetRevision(self.dir)
			tarball = self.BuildRoot + 'SOURCES/%s-%s-r%s.tgz' % (self.PackageName,self.Version,self.Revision)
			specDest = self.BuildRoot + 'SPECS/%s.spec' % self.PackageName
			
			if os.path.exists(tarball):
				os.remove(tarball)
			
			tarOptions=''
			if self.OutputDirName:
				tarOptions += " --show-transformed-names --transform='s,^(\\./)?,%s/,Sx'" % self.OutputDirName
			
			tarOptions += ' -czf %s' % tarball
			
			tar_cmd = 'tar %s %s' % (tarOptions, ' '.join(map(lambda e: "--exclude='%s'" % e, self.Excludes) + ["'%s'" % f for f in self.Files]))

			if self.run_cmd(tar_cmd)[0]:
				raise "Unable to create rpm tarball"
			
			if self.run_cmd('cat %s | sed "s/\\$WCREV\\\\$/%s/" | sed "s/\\$VERSION\\\\$/%s/" > %s' % (self.SpecTemplate,self.Revision,self.Version,specDest) )[0]:
				raise "Unable to process spec file template."
			
			# Out of tree rpm builds need to find the shared qmake .pri files
			# This is where they are installed
			if not 'PRI_SHARED' in os.environ:
				os.environ['PRI_SHARED'] = '/usr/share/stone/qmake_shared'
			
			ret,output = self.run_cmd('rpmbuild -ba %s' % specDest)
			for line in output.splitlines():
				res = re.match('Wrote: (.+)$',line)
				if res:
					self.BuiltRPMS.append(res.group(1))
					print( line )

		if self.has_arg('install') and not self.InstallDone:
			for rpm in self.BuiltRPMS:
				if not '/SRPMS/' in rpm and not '-debuginfo-' in rpm:
					cmd = 'sudo rpm -Uv %s%s' % (self.has_arg('force_install') and '--force ' or '', rpm)
					(ret,output) = self.run_cmd(cmd,noThrow=True)
					if ret:
						if 'is already installed' in output:
							print ("Skipping rpm installation, %s already installed" % rpm)
						else:
							self.cmd_error( cmd, output )
			self.InstallDone = True

class RPMTargetSip(RPMTarget):
	def __init__(self,*args,**kwargs):
		RPMTarget.__init__(self,*args,**kwargs)
		dirName = self.Files[0]
		self.Excludes.remove('sip')
		self.Files = map(lambda p: os.path.join(dirName,p),['build.py','__init__.py','sip'])

class UploadTarget(Target):
	DefaultHost = 'hartigan'
	DefaultDest = ''
	def __init__(self,name,dir,files,host,dest):
		Target.__init__(self,name,dir)
		if files is None:
			self.files = []
		else:
			try:
				iter(files)
				self.files = files
			except:
				self.files = [files]
		self.host = host
		self.dest = dest
	
	def build_run(self):
		for file in self.files:
			self.run_cmd(['scp',file,"%s:%s" % (self.host,self.dest)])

class ClassGenTarget(Target):
	def __init__(self,name,dir,schema, pre_deps = [], post_deps = []):
		Target.__init__(self,name,dir,pre_deps,post_deps)
		self.Schema = schema
		
	def get_command(self):
		# Here is the static command for running classmaker to generate the classes
		classmakercmd = 'classmaker'
		if sys.platform == 'win32':
			classmakercmd = 'classmaker.exe'

		# Run using cmd in path, unless we are inside the tree
		if os.path.isfile(os.path.join(self.dir,'../../apps/classmaker/',classmakercmd)):
			if sys.platform != 'win32':
				classmakercmd = './' + classmakercmd
			classmakercmd = 'cd ../../apps/classmaker && ' + classmakercmd
		return classmakercmd
		
	def build_run(self):
		outputMode = "-o"
		if self.has_arg("sip_only"):
			outputMode = "--output=sip"
		elif self.has_arg("cpp_only"):
			outputMode = "--output=cpp,ngschema"
		self.run_cmd( self.get_command() + " -s " + self.Schema + (" %s " % outputMode) + self.dir, shell=True )
	
argv = sys.argv

def build():
	global All_Targets
	global Targets
	global Args
	global Config_Replacement_File

	Short_To_Long = dict(
		c='clean',
		d='debug',
		i='install',
		I='sudo_install',
		v='verbose',
		r='resume',
		s='skip-ext-deps',
		C='color',
		p='progress',
		w='warn'
	)

	Environ_Options = dict(
		m="MAKEFLAGS",
		q="BLUR_QMAKE_FLAGS",
	)

	if Parser is None:
		import getopt
		options, Args = getopt.gnu_getopt(argv[1:], "cdiIvrsCpwm:q:n:")
		for opt,arg in options:
			o = opt[1:]
			if o in Short_To_Long:
				Args.append( Short_To_Long[o] )
			elif o in Environ_Options:
				var = Environ_Options[o]
				os.environ[var] = os.environ.get(var,"")+" "+arg

	# Targets
	build_resume_path = os.path.join(os.path.abspath(os.getcwd()),'build_resume.cache')
	if Args.count('resume'):
		try:
			pf = open( build_resume_path, 'rb' )
			All_Targets = cPickle.load(pf)
			Targets = cPickle.load(pf)
			Args = cPickle.load(pf)
		except:
			traceback.print_exc()
			print( "Unable to load build resume cache." )
			return
		
	if Args.count('-config-replacement-file'):
		try:
			idx = args.index('-config-replacement-file')
			Config_Replacement_File = Args[idx+1]
			del Args[idx+1]
			del Args[idx]
		except:
			print( "Unable to load config replacement file" )
	
	# Gather the targets
	for a in argv[1:]:
		targetName = a
		args = []
		
		# Parse args if there are any
		try:
			col_idx = targetName.index(':')
			targetName = a[:col_idx]
			args = a[col_idx+1:].split(',')
		except: pass
		
		# Find the targets, continue to next arg if none found
		try:
			targets = find_targets(targetName)
		except: continue
		
		# Remove the arg from the global arg list if it applies to specified targets
		if len(targets) > 1 and a in Args:
			Args.remove(a)
		
		for target in targets:
			target.apply_arg(args)
			if not target in Targets:
				Targets.append(target)
			# Has an argument
			try:
				Args.remove(target.name)
			except: pass

	if argv.count('all'):
		Targets = All_Targets
		Args.remove('all')
		
	for target in Targets:
		target.selected()

	if sys.platform == 'win32':
		if 'QMAKESPEC' in os.environ and 'win32-msvc' in os.environ['QMAKESPEC']:
			if 'FrameworkDir64' in os.environ or ('FrameworkDir' in os.environ and os.environ['FrameworkDir'].endswith('Framework64')):
				Args.append('X86_64')
	
	if 'color' in Args:
		global term
		term = TerminalController()
	
	# These Options are passed to the module build scripts
	print( term.render('${YELLOW}Starting Build${NORMAL}') )
	print( "Targets:", Targets, "Args:", Args )

	for t in Targets:
		#print "Entering Target.build(args) on target ", t.name
		#print "Target pre_deps are: ", t.pre_deps
		#print "Target post_deps are: ", t.post_deps
		#print "\n [build process starts here] \n"
		try:
			t.build()
		except Exception as e:
			if not e.__class__ == KeyboardInterrupt:
				traceback.print_exc()
			print( "Writing build resume file to",build_resume_path )
			pf = open(build_resume_path,'wb')
			cPickle.dump(All_Targets,pf,-1)
			cPickle.dump(Targets,pf,-1)
			cPickle.dump(Args,pf,-1)
			pf.close()
			print( "Exiting" )
			sys.exit(1)
	
	if '--write-installers-file' in Args:
		f = open('new_installers.pickle','wb')
		cPickle.dump(Generated_Installers,f)
		f.close()
		
