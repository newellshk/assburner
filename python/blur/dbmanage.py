
from PyQt4.QtCore import *
from PyQt4.QtSql import *
from blur.Stone import *
from blur.Classes import *
import blur
import sys, time, os
from optparse import OptionParser

dryRun = False

def install_sql_files( path ):
	print "Executing all sql files in", os.path.abspath(path)
	for sqlFile in QDir( os.path.abspath(path) ).entryList( ['*.sql'], QDir.Files ):
		if '_remove' in sqlFile or 'vacuum' in sqlFile: continue
		print "Executing %s" % sqlFile
		if os.system( 'psql -h %s -p %i %s %s < sql/renderfarm/%s' % (connection.host(), connection.port(), connection.databaseName(), connection.userName(), sqlFile) )!= 0:
			raise Exception("Error executing sql file %s" % sqlFile)

required_data_tables = ['JobType','Service','Software','SoftwareStatus']

def dump_table_data( path ):
	args = {
		'host' : connection.host(),
		'port' : connection.port(),
		'userName' : connection.userName(),
		'tables' : ' '.join(map( lambda x: '-t %s' % x, required_data_tables )),
		'database' : connection.databaseName(),
		'outputPath' : path }
	cmd = 'pg_dump -D -a -h %(host)s -p %(port)i -U %(userName)s %(tables)s %(database)s > %(outputPath)s' % args
	
	print cmd
	if not dryRun:
		os.system( cmd )
	
	
def buildOptionsParser():
	parser = OptionParser(usage="usage: %prog [options] COMMAND")
	parser.add_option( "-d", "--dry-run", 			dest="dryRun", 			action="store_true", default=None, help="Print the host names but dont actually commit" )
	parser.add_option( "-r", "--dump-required-data", 		dest="dumpRequiredData", 		default=None, help="Dump the contents of required tables to FILE.  Used to create a .sql file that can be used to kickstart a new installation." )
	parser.add_option( "-u", "--upgrade", action="store_true", dest = "upgrade", 			default=None, help="Upgrades the database by creating or updating the schema, and loading all the sql files that contain the triggers and functions" )
	return parser
	
if __name__=="__main__":
	app = QCoreApplication(sys.argv)

	initConfig( "/etc/db.ini", "/var/log/ab/manager.log" )
	initStone( sys.argv[1:] )

	blurqt_loader()

	Database.current().setEchoMode( Database.EchoUpdate | Database.EchoDelete )
	connection = Database.current().connection()

	parser = buildOptionsParser()
	parser.parse_args()
	opts = parser.values

	dryRun = opts.dryRun
	
	if not connection.reconnect():
		sys.exit( 1 )

	if opts.upgrade:
		Database.current().createTables()
		install_sql_files( os.path.join( __file__, "../../../sql/renderfarm/" ) )

	if opts.dumpRequiredData:
		dump_table_data( opts.dumpRequiredData )