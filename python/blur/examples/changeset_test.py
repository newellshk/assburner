
from blur.quickinit import *

cs = ChangeSet.create()
cse = ChangeSetEnabler(cs)

if True:
	lost = User()
	lost.setName( 'test' )
	# Here lost falls out of scope, the new record is lost because commit isn't called

if True:
	saved = User()
	saved.setName( 'test' )
	saved.setPassword( 'pass' )
	# This doesn't commit the record to the database, only to the changeset.
	saved.commit()

if True:
	me = User.recordByUserName('test')
	me.setPassword('new_pass')
	# New password committed to the changeset
	me.commit()

# Regardless of where we get the record from(existing reference, index, or select), the new value will be present
print User.recordByUserName('test').password() # Prints 'new_pass'

print ChangeSet.current() == cs # True

del cse

print ChangeSet.current() == cs # True

print ChangeSet.current().isValid()

print User.recordByUserName('test').password() # Prints 'old_pass'

cs.commit()

print User.recordByUserName('test').password() # Prints 'new_pass', cs isn't active but changes committed to the db are now visible everywhere

cs.revert() # Does nothing, changes are already committed to the db.
cs.undo() # Rolls back the changes.  Changes are still held by the changeset and can be modified and committed again
