

from PyQt4.QtGui import *
from blur.Stonegui import *
import sys


class MyGraphicsView(QGraphicsView):
	def __init__(self,parent=None):
		QGraphicsView.__init__(self,parent)
		self.Scene = ExtGraphicsScene(self)
		self.setScene(self.Scene)
		self.Scene.prepareToolTip.connect(self.prepareToolTip)
		item = QGraphicsRectItem()
		self.Scene.addItem(item)
		item.setPos( 0, 0 )
		item.setRect(0,0,100,100)

	def prepareToolTip(self,item):
		print "Preparing tooltip"
		item.setToolTip( "Tool Tip" )
	
	
def main():
	app = QApplication(sys.argv)
	view = MyGraphicsView()
	view.show()
	app.exec_()
	

if __name__=="__main__":
	main()
