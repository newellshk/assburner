
from PyQt4.QtGui import *
from PyQt4.QtCore import *

# This example shows how to update the contents of a QTextDocument
# It updates the contents of one cell in a table every second

class Window(QWidget):
	def __init__(self):
		QWidget.__init__(self)
		lay = QHBoxLayout(self)
		self.textEdit = QTextEdit(self)
		lay.addWidget(self.textEdit)
		self.curVal = 1
		self.setupText()
		self.timer = QTimer(self)
		self.timer.timeout.connect(self.updateText)
		self.timer.start(1000)
		
	def setupText(self):
		tc = QTextCursor(self.textEdit.document())
		tableFormat = QTextTableFormat()
		tableFormat.setBorderStyle( QTextFrameFormat.BorderStyle_Solid )
		tableFormat.setBorder(1)
#		tableFormat.setMargin(0)
		tableFormat.setCellSpacing(-1)
		tableFormat.setCellPadding(5)
		self.table = tc.insertTable(1,2,tableFormat)
		tc.insertText('Test')
		tc.movePosition(QTextCursor.NextCell)
		tc.insertText(str(self.curVal))
		
	def updateText(self):
		tc = QTextCursor(self.table)
		# Move to cell 2, row 1
		tc.movePosition(QTextCursor.NextCell)
		# Select the text for deletion
		tc.movePosition(QTextCursor.EndOfBlock,QTextCursor.KeepAnchor)
		tc.removeSelectedText()
		self.curVal+=1
		tc.insertText(str(self.curVal))
		
app = QApplication([])

win = Window()
win.show()
app.exec_()
