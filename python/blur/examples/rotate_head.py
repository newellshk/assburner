#!/usr/bin/env python

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import sys

app = QApplication(sys.argv)

def createRotateImage( img, steps ):
	ret = QImage(img.width(), img.height()*steps, QImage.Format_ARGB32)
	ret.fill( QColor(0,0,0,0).rgba() )
	p = QPainter(ret)
	p.setRenderHint(QPainter.SmoothPixmapTransform,  True)
	for i in range(steps):
		p.resetTransform()
		p.translate(img.width()/2,(i + 0.5) * img.height())
		p.rotate(i * 360 / steps)
		p.drawImage(QPoint(-img.width()/2,-img.height()/2),img)
	return ret

class BusyIcon(QWidget):
	def __init__(self,pixmap, parent=None, loopTime=1.6):
		QWidget.__init__(self,parent)
		self.Pixmap = pixmap
		self.Steps = pixmap.height() / pixmap.width()
		self.Step = 0
		self.Timer = QTimer(self)
		self.connect(self.Timer, SIGNAL('timeout()'), self.step )
		self.resize(pixmap.width(),pixmap.width())
		self.LoopTime = loopTime
		self.start()
		
	def start(self):
		self.Timer.start(self.LoopTime * 1000.0 / self.Steps)
		
	def stop(self):
		self.Timer.stop()
		self.Step = 0
		self.update()
		
	def step(self):
		self.Step = (self.Step + 1) % self.Steps
		self.update()
		
	def paintEvent(self,pe):
		p = QPainter(self)
		w = self.Pixmap.width()
		p.drawPixmap(QPoint(0,0),self.Pixmap,QRect(0,w * self.Step,w,w))

img = createRotateImage( QImage("../../../cpp/apps/resin/images/head2.png"), 32 )
img.save( "../../../cpp/apps/assfreezer/images/rotating_head.png", "png" )

bi = BusyIcon(QPixmap.fromImage(img))
bi.show()
bi.start()
app.exec_()
