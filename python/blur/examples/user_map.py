
from blur.Stone import *
from blur.Classes import *
from PyQt4.QtCore import Qt
import sys

def getSavedMappingsFilePath():
	if sys.platform == 'win32':
		return "c:/temp/saved_mappings.xml"
	return '/tmp/saved_mappings.xml'

def backupMappings(mappings):
	mappings = MappingList(RecordList(mappings))
	success, errorMessage = RecordXmlSaver.toFile( mappings + mappings.hosts() + mappings.mappingTypes(), getSavedMappingsFilePath() )
	if not success:
		print "Error while backing up mappings for %s: %s" % (getUserName(), errorMessage)
	return success

def restoreMappings():
	records, errorMessage = RecordXmlLoader.fromFile( getSavedMappingsFilePath() )
	if errorMessage:
		raise Exception("Error restoring backup mappings: ", errorMessage)
		
	# We have to index the backed up hosts and mappingTypes by their primary keys, so that mapping.host() and mapping.mappingType()
	# works without accessing the db
	# This could be done automatically in RecordXmlLoader, but I would need to think about possible side effects
	Host.table().keyIndex().recordsIncoming(HostList(records))
	MappingType.table().keyIndex().recordsIncoming(MappingTypeList(records))
	return records
	
def mapDrivesForCurrentUser(forceUnmountOfExisting):
	errors = {}
	mappings = None
	# Need to keep mapping fkeys in memory when restoring from backups
	records = None
	
	if Database.current().connection().checkConnection():
		mappingByDrive = {}
		# Mappings from lowest priority to highest
		for gm in User.currentUser().userGroups().groups().groupMappings().sorted('priority',Qt.AscendingOrder):
			mapping = gm.mapping()
			mappingByDrive[mapping.mount()] = mapping
		mappings = mappingByDrive.values()
		backupMappings(mappings)
	else:
		# If we can't connect to the database, use backed up mappings
		print "Database connection failed, restoring backed up mappings"
		records = restoreMappings()
		mappings = MappingList(records)
	
	for mapping in mappings:
		Log( "Mapping \\\\%s\\%s to %s:" % (mapping.host().name(), mapping.share(), mapping.mount()) )
		success, errorMessage = mapping.map(forceUnmountOfExisting)
		if not success:
			errors[mapping.mount()] = errorMessage
	
	return errors

if __name__=="__main__":
	from blur.quickinit import *
	mapDrivesForCurrentUser(False)
