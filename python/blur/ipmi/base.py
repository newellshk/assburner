
Start = 1
Stop = 2
Reboot = 4
# If a support action, returns a dict with various info about each blade
ChassisBladeInfo = 32

class IPMIFactory(object):
	Factories = []
	
	# Reimplement for concrete classes
	def _construct(self, manufacturer, model):
		return IPMIClass()
	
	@classmethod
	def constructClass(cls, manufacturer, model):
		for f in cls.Factories:
			c = f._construct(manufacturer, model)
			if c: return c
		return None

class IPMIClass(object):
	def __new__(cls, manufacturer = None, model = None):
		ret = None
		if cls == IPMIClass:
			ret = IPMIFactory.constructClass(manufacturer, model)
		if not ret:
			ret = super(IPMIClass,cls).__new__(cls,manufacturer,model)
		return ret
	
	def __init__(self, manufacturer, model):
		self.Manufacturer = manufacturer
		self.Model = model
		
	def isValid(self):
		return self.isHost() or self.isChassis()
	
	def isHost(self):
		return False

	def isChassis(self):
		return False

	def hasCap(self,cap):
		return bool(self.caps() & cap)

	def caps(self):
		return 0

	def ipmi(self, ip, chassisIP=None, chassisSlot=None):
		return IPMI(self, ip, chassisIP)

	def chassis(self):
		return IPMIClass()

class IPMI(object):
	def __init__(self, ipmiClass, ip, chassisIP=None, chassisSlot=None):
		self.IPMIClass = ipmiClass
		self.IP = ip
		self.ChassisIP = chassisIP
		self.ChassisSlot = chassisSlot

	def isValid(self):
		return self.IPMIClass.isValid()
	
	def ipmiClass(self):
		return self.IPMIClass
	
	def hasCap(self,cap):
		return self.IPMIClass.hasCap(cap)

	def caps(self):
		return self.IPMIClass.caps()

	def chassis(self):
		return self.IPMIClass.chassis().ipmi(self.ChassisIP)

	def action(self,action,args=None):
		pass

	def start(self):
		return self.action(Start)
	
	def stop(self):
		return self.action(Stop)
	
	def reboot(self):
		return self.action(Reboot)

	def chassisBladeInfo(self):
		return self.action(ChassisBladeInfo)
