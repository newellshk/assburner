
import base as ipmi
import subprocess
import re, os

user = 'Administrator'
password = 'openn.ipmii'

class HpFactory(ipmi.IPMIFactory):
	def _construct(self,manufacturer,model):
		if manufacturer == 'HP':
			if model == 'ProLiant BL460c G6':
				return HpHostClass(manufacturer,model)
			elif model == 'BladeSystem c7000 Enclosure':
				return HpChassisClass(manufacturer,model)
		return None

ipmi.IPMIFactory.Factories.append(HpFactory())

class HpHostClass(ipmi.IPMIClass):
	def caps(self):
		return ipmi.Start | ipmi.Stop | ipmi.Reboot
	
	def isHost(self):
		return True
	
	def chassis(self):
		return HpChassisClass()
	
	def ipmi(self, ip, chassisIP=None, chassisSlot=None):
		return HpHostIPMI(self, ip, chassisIP, chassisSlot)
	
class HpChassisClass(ipmi.IPMIClass):
	def isChassis(self):
		return True
	
	def ipmi(self, ip, chassisIP=None, chassisSlot=None):
		return HpChassisIPMI(self, ip, ip)

def devNull():
	try:
		# py3k
		return subprocess.DEVNULL
	except AttributeError:
		return open(os.devnull, 'wb')

class HpIPMIBase(ipmi.IPMI):
	def _sshCmd(self,cmd):
		args = dict( user = user, password = password, ip = self.ChassisIP, slot = self.ChassisSlot )
		return subprocess.check_output( ('sshpass -p %(password)s ssh -o StrictHostKeyChecking=no %(user)s@%(ip)s ' + cmd) % args, shell=True, stderr=devNull() )
	
class HpHostIPMI(HpIPMIBase):
	def caps(self):
		if self.ChassisIP is None or self.ChassisSlot is None:
			return None
		return ipmi.IPMI.caps(self)
	
	def action(self,action,args=None):
		actionMap = {
			ipmi.Start : ('poweron server %(slot)s','Powering on blade %(slot)s.'),
			ipmi.Stop : ('poweroff server %(slot)s','Blade %(slot)s performing a graceful shutdown.'),
			ipmi.Reboot : ('reboot server %(slot)s','Rebooting Blade %(slot)s') }
		if self.ChassisIP is not None and self.ChassisSlot is not None and action in actionMap:
			cmd, result = actionMap[action]
			output = self._sshCmd(cmd)
			return output.strip() == (result % dict(slot=self.ChassisSlot))
		return False

class HpChassisIPMI(HpIPMIBase):
	def caps(self):
		return ipmi.ChassisBladeInfo
	
	def action(self,action,args=None):
		if action == ipmi.ChassisBladeInfo:
			ret = {}
			blade = {}
			bladeNum = re.compile( r'Server Blade #(\d+) Information:' )
			mac = re.compile( r'(?:NIC|Port) (\d): (.{17})' )
			ipmiIp = re.compile( r'IP Address: (.+)$' )
			ipmiMac = re.compile( r'MAC Address: (.{17})' )
			for l in self._sshCmd( r'show server info all').splitlines():
				mo = bladeNum.search(l)
				if mo:
					blade['chassisSlot'] = int(mo.group(1))
					continue
				mo = mac.search(l)
				if mo:
					blade['mac%s' % mo.group(1)] = mo.group(2)
					continue
				mo = ipmiIp.search(l)
				if mo:
					blade['ipmiIp'] = mo.group(1)
					continue
				mo = ipmiMac.search(l)
				if mo:
					blade['ipmiMac'] = mo.group(1)
					ret[blade['chassisSlot']] = blade
					blade = {}
			return ret
