
import os
import subprocess
import base as ipmi

# Put into a config class
user = 'ADMIN'
password = 'ADMIN'
smcPath = '/usr/lib/smcipmi/SMCIPMITool.jar'

class SMCFactory(ipmi.IPMIFactory):
	Models = ('B8DTT','B9DRT','SBI-7227R-T2','X8DTT-H')
	def _construct(self,manufacturer,model):
		if manufacturer == 'Supermicro' and model in self.Models:
			return SMCHostClass(manufacturer,model)
		return None

ipmi.IPMIFactory.Factories.append(SMCFactory())

class SMCHostClass(ipmi.IPMIClass):
	def caps(self):
		return ipmi.Start | ipmi.Stop | ipmi.Reboot
	
	def isHost(self):
		return True
	
	def chassis(self):
		return SMCChassisClass()
	
	def ipmi(self, ip, chassisIP=None, chassisSlot=None):
		return SMCHostIPMI(self, ip, chassisIP, chassisSlot)
	
class SMCChassisClass(ipmi.IPMIClass):
	def isChassis(self):
		return True
	
	def ipmi(self, ip, chassisIP=None, chassisSlot=None):
		return SMCChassisIPMI(self, ip, ip)

def devNull():
	try:
		# py3k
		return subprocess.DEVNULL
	except AttributeError:
		return open(os.devnull, 'wb')

class SMCHostIPMI(ipmi.IPMI):
	def smcAction(self,action):
		args = dict( smcPath= smcPath, ip = self.IP, user = user, password = password, action = action )
		text = subprocess.check_output( 'java -jar %(smcPath)s %(ip)s %(user)s %(password)s ipmi power %(action)s 1>&2 &' % args, stderr=devNull(), shell=True )
		print text
		return text.strip() == 'Done'

	def action(self,action,args=None):
		if action == ipmi.Start:
			self.smcAction('up')
		elif action == ipmi.Reboot:
			self.smcAction('reset')
		elif action == ipmi.Stop:
			self.smcAction('down')

class SMCChassisIPMI(ipmi.IPMI):
	def caps(self):
		return ipmi.ChassisBladeInfo

	def action(self,action,args=None):
		if action == ipmi.ChassisBladeInfo:
			ret = {}
			blade = {}
