
from blur.quickinit import *

def usage():
	print "Usage:\nvray_translate.py INPUT_SCENE_FILE OUTPUT_DIRECTORY"
	
if __name__=="__main__":
	if len(sys.argv) != 3:
		usage()
		sys.exit(1)
	inputScene = sys.argv[1]
	outputDir = sys.argv[2]
	success, errorMessage = Mapping.translateVrayScene(inputScene, outputDir)
	if not success:
		print errorMessage
		sys.exit(2)
	print "Scene successfully translated"
