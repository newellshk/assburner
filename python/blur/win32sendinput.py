

from ctypes import Structure, Union, pointer, sizeof, POINTER, windll
from ctypes import c_ulong, c_ushort, c_short, c_long
from win32api import GetSystemMetrics

PUL = POINTER(c_ulong)
_SendInput = windll.user32.SendInput

# START SENDINPUT TYPE DECLARATIONS

# http://msdn.microsoft.com/en-us/library/ms646271.aspx
class _KeyBdInput(Structure):
    _fields_ = [("wVk", c_ushort),
             ("wScan", c_ushort),
             ("dwFlags", c_ulong),
             ("time", c_ulong),
             ("dwExtraInfo", PUL)]

KEYEVENTF_EXTENDEDKEY = 0x01
KEYEVENTF_KEYUP = 0x02
KEYEVENTF_SCANCODE = 0x08
KEYEVENTF_UNICODE = 0x04

# http://msdn.microsoft.com/en-us/library/ms646269.aspx
class _HardwareInput(Structure):
    _fields_ = [("uMsg", c_ulong),
             ("wParamL", c_short),
             ("wParamH", c_ushort)]

MOUSEEVENTF_ABSOLUTE = 0x8000
MOUSEEVENTF_MOVE = 0x0001
MOUSEEVENTF_LEFTDOWN = 0x0002
MOUSEEVENTF_LEFTUP = 0x0004
MOUSEEVENTF_RIGHTDOWN = 0x0008
MOUSEEVENTF_RIGHTUP = 0x0010
MOUSEEVENTF_MIDDLEDOWN = 0x0020
MOUSEEVENTF_MIDDLEUP = 0x0040
# http://msdn.microsoft.com/en-us/library/ms646273.aspx
class _MouseInput(Structure):
    _fields_ = [("dx", c_long),
             ("dy", c_long),
             ("mouseData", c_ulong),
             ("dwFlags", c_ulong),
             ("time",c_ulong),
             ("dwExtraInfo", PUL)]

# http://msdn.microsoft.com/en-us/library/ms646270.aspx
class Input_I(Union):
    _fields_ = [("ki", _KeyBdInput),
              ("mi", _MouseInput),
              ("hi", _HardwareInput)]

class Input(Structure):
    _fields_ = [("type", c_ulong),
             ("ii", Input_I)]

class POINT(Structure):
    _fields_ = [("x", c_ulong),
             ("y", c_ulong)]

INPUT_MOUSE = 0x0
INPUT_KEYBOARD = 0x1
INPUT_HARDWARE = 0x2

def KeyBdInput(*args):
	_in = Input_I()
	_in.ki = _KeyBdInput(*args)
	return Input(INPUT_KEYBOARD,_in)

def MouseInput(*args):
	_in = Input_I()
	_in.mi = _MouseInput(*args)
	return Input(INPUT_MOUSE,_in)
	
def HardwareInput(*args):
	_in = Input_I()
	_in.hi = _HardwareInput(*args)
	return Input(INPUT_HARDWARE,_in)

# http://msdn.microsoft.com/en-us/library/dd375731.aspx
VK_RETURN = 0x0D
VK_ESCAPE = 0x1B
VK_TAB = 0x09


def InputArray( *argc ):
	cnt = len(argc)
	cls = Input * cnt
	return cls(*argc)

def SendInput( *inputs ):
	ia = InputArray( *inputs )
	return _SendInput( len(inputs), pointer(ia), sizeof(ia[0]) )

# END SENDINPUT TYPE DECLARATIONS

SM_CXVIRTUALSCREEN = 78
SM_CYVIRTUALSCREEN = 79

def simulateMouseMove(x, y, relative = False):
	# Convert the coordinates to a value between 0-65535(from upperleft to lower right of virtual screen size)
	if not relative:
		vw = GetSystemMetrics(SM_CXVIRTUALSCREEN)
		vh = GetSystemMetrics(SM_CYVIRTUALSCREEN)
		x = x*65535/vw
		y = y*65535/vh
	extra = c_ulong(0)
	evt = MOUSEEVENTF_MOVE
	if not relative:
		evt = evt | MOUSEEVENTF_ABSOLUTE
	move = MouseInput(x,y,0,evt,0,pointer(extra))
	SendInput(move)

def simulateMouseClick():
	extra = c_ulong(0)
	click = MouseInput(0, 0, 0, MOUSEEVENTF_LEFTDOWN, 0, pointer(extra))
	release = MouseInput(0, 0, 0, MOUSEEVENTF_LEFTUP, 0, pointer(extra))
	SendInput(click,release)
	
def simulateKeyPress( vcode = VK_RETURN ):
	extra = c_ulong(0)
	keydown = KeyBdInput(vcode, 0, 0, 0, pointer(extra))
	keyup = KeyBdInput(vcode, 0, KEYEVENTF_KEYUP, 0, pointer(extra))
	SendInput( keydown, keyup )

if __name__ == "__main__":
	import win32ui

	hwnd = win32ui.FindWindow(None, "Some Dialog Box")
	hwnd.SetForegroundWindow()
	simulateKeyPress()
	
	