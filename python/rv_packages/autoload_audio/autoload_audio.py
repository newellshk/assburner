
import re
import operator
import os
import sys
from pprint import pprint, pformat
import platform

if platform.architecture()[0] == '32bit':
	sys.path.append(r'C:\Python26\Lib\site-packages')
else:
	sys.path.append(r'C:\Python26_64\Lib\site-packages')

import rv.rvtypes
import rv.commands 
import rv.extra_commands
import rv.runtime
from pymu import MuSymbol


def createMode():
	return PyAutoLoadAudio()


class PyAutoLoadAudio(rv.rvtypes.MinorMode):
	
	re_fp = re.compile(r'^Q:/(?P<project>\w+)/Comp/(?:[^/]+/)*(?P<sequence>Sc\d\d\d)_(?P<shot>S\d\d\d\d\.\d\d)_.+?(?P<ext>\.[a-zA-Z0-9]{2,4})$') 
	audio_template = 'Q:/%(project)s/Audio/Breakout/%(sequence)s/%(sequence)s_%(shot)s.wav'
	
	re_fps = [
		re.compile(r'^[A-Z]:/(?P<project>\w+)/(?:[^/]+/)*(?P<sequence>Sc\d{3})/(?P<shot>S\d{4}(?:\.\d\d)?)/.*?$', re.I),
		re.compile(r'^[A-Z]:/(?P<project>\w+)/(?:[^/]+/)*(?P<sequence>Sc\d{3})_(?P<shot>S\d{4}(?:\.\d\d)?).*?$', re.I),
	]
	
	def __init__(self):
		rv.rvtypes.MinorMode.__init__(self)
		self._autoload_audio = False
		self.readPrefs()
		
		self.init(
			'py-rvblur-autoloadaudio-mode',
			[('new-source', self.loadAudio, 'load audio')],
			None,
			[('File', 
				[('Options', 
					[('Auto-load Audio', self.toggleAutoLoadAudio, None, self.autoLoadAudioState)]
				)]
			)]
		)
		
	def loadAudio(self, event=None):
		if not self._autoload_audio:
			return

		contents = event.contents()
		nodename, rvsource, filepath = contents.split(';;')
	
		m = None
		for regex in self.re_fps:
			m = regex.match(filepath)
			if m:
				break
		
		if not m:
			return
				
		audio_path = self.audio_template % m.groupdict()
		if not os.path.isfile(audio_path):
			return

#		rv.commands.addToSource(audio_path, 'autoloadaudio')	
	
		#NOTE: MuSymbol only maps one of the function signatures, to use the
		# other signatures, we need to eval raw mu code (kinda ugly, but it 
		# there doesn't appear to be any other way to do it)
		rv.runtime.eval('commands.addToSource("%s", "%s", "%s")' % (nodename, audio_path, 'autoloadaudio'), ['commands'])	
		
	def updateAllSources(self, event=None):
		if not self._autoload_audio:
			return
		
	def readPrefs(self):
		self._autoload_audio = rv.commands.readSettings('autoload_audio', 'autoloadAudio', False)
	
	def writePrefs(self):
		rv.commands.writeSettings('autoload_audio', 'autoloadAudio', self._autoload_audio)
						
	def toggleAutoLoadAudio(self, event):
		self._autoload_audio = not self._autoload_audio
		self.writePrefs()		
		self.updateAllSources(event)			
	
	def autoLoadAudioState(self):
		if self._autoload_audio:
			return rv.commands.CheckedMenuState
		return rv.commands.UncheckedMenuState
						


