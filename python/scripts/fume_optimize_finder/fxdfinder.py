

import sys
import os
from optparse import OptionParser
import pprint
import subprocess
from StringIO import StringIO
import pickle
import datetime


cache_fp = os.path.join(os.path.normpath(os.path.dirname(__file__)), 'cache.txt')
exe_path = os.path.join(os.path.normpath(os.path.dirname(__file__)), 'fxdreader.exe')


def cache_fxd_list():
	fxd_files = []
	roots = ["X:/_library/QC/FumeFx", "X:/_library/_Master/FumeFx"]
	for r in roots:
		r = os.path.normpath(r)
		fx_dirs = [os.path.join(r, d) for d in os.listdir(r)  if os.path.isdir(os.path.join(r, d))]
		cache_dirs = [os.path.join(d, 'cache') for d in fx_dirs if os.path.isdir(os.path.join(d, 'cache'))]
		cnt = len(cache_dirs)
		for i, d in enumerate(cache_dirs):
			print 'Looking for fxds (%s of %s): %s' % (i, cnt, d)
			fxd = None
			for f in os.listdir(d):
				fp = os.path.join(d, f)
				if fp.endswith('.fxd'):
					fxd = fp
					break
			if fxd:
				fxd_files.append(fxd)
	f = open(cache_fp, 'w')
	pickle.dump(fxd_files, f)
	f.close()


def main():
	if not os.path.isfile(cache_fp):
		cache_fxd_list()

	f = open(cache_fp, 'r')
	fxd_files = pickle.load(f)
	f.close()

	opt_files = []
	cnt = len(fxd_files)
	for i, f in enumerate(fxd_files):
		cmd = '%s %s' % (exe_path, f)
		print cmd
		pid = subprocess.Popen([cmd], shell=True, stdout=subprocess.PIPE)
		(cout, cerr) = pid.communicate()
		rval = cout
		print 'Reading fxd (%s of %s)[%s]: %s' % (i, cnt, rval, f)
		if '1' in rval:
			opt_files.append(f)

	dt = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
	results_fp = os.path.join(os.path.normpath(os.path.dirname(__file__)), 'results_%s.txt' % dt)
	f = open(results_fp, 'w')
	pprint.pprint(opt_files, f)
	f.close()





if __name__ == '__main__':
	main()
