#!/usr/bin/python
from commands import getstatusoutput
from platform import node
from socket import socket, AF_INET, SOCK_STREAM
from sys import argv, exit
from time import sleep, time
import os

try:
    from blur.daemonize import createDaemon
except ImportError:
    createDaemon = lambda: sys.stderr.write( "Could not load blur.daemonize, running in the foreground\n" )

DELAY = 30
CARBON_SERVER = 'graphite.blur.com'
CARBON_PORT = 2003

class Carbon:
    def __init__(self, hostname, port):
        self.s = socket(AF_INET, SOCK_STREAM)
        self.hostname = hostname
        self.port = int(port)
        self.connect()
    def connect(self):
        try:
            self.s.connect((self.hostname, self.port))
        except IOError, e:
            print "connect: ", e
            return
    def disconnect(self): self.s.close()
    def send(self, data):
        try:
            self.s.sendall(data + "\n")
        except:
            self.connect()
            self.s.sendall(data + "\n")

class Host:
    def __init__(self):
        self.historical = {}
    def get_all(self):
        data = []
        functions = dir(self)
        for function in functions:
            if not function.startswith("fetch_"): continue
            for metric in eval("self.%s()" % (function)):
                data.append(metric)
        return data

    def read_file(self, filename):
        file_handle = open(filename)
        contents = file_handle.readlines()
        file_handle.close()
        return contents

    def delta_analyzer(self, measurements, data, now):
        result = []
        for line in data:
            print line
            for measurement, loc in measurements.iteritems():
                metric_name = "%s.%s" % (line[0], measurement)
                try:
                    value = line[loc]
                except: continue
                if self.historical.has_key(metric_name):
                    current = value
                    delta = int(value) - int(self.historical[metric_name][1])
                    timedelta = time() - self.historical[metric_name][0]
                    self.historical[metric_name] = (time(), current)
                    if timedelta < 1:
                        continue
                    value = int( delta / timedelta )
                    if value > 0:
                        result.append("%s %d %d" % (metric_name, value, now))
                else:
                    self.historical[metric_name] = (time(), value)
        return result

    def fetch_cpustats(self):
        data = []
        now = int(time())

        fields = "user nice sys idle wait irq soft_irq steal guest guest_nice".split()
        measurements = {}
        for i in range(len(fields)):
            measurements[fields[i]] = i+1

        try:
            raw_data = self.read_file("/proc/stat")
        except:
            return data

        for line in raw_data:
            if not line.find("cpu "):
                values = line.split()
                data.append(values)
                break

        return self.delta_analyzer( measurements, data, now )

    def fetch_loadavg(self):
        data = []
        now = int(time())
        (loadavg_1, loadavg_5, loadavg_15) = self.read_file("/proc/loadavg")[0].strip().split()[:3]
        data.append("load.1min %s %d" % (loadavg_1,now))
        data.append("load.5min %s %d" % (loadavg_5,now))
        data.append("load.15min %s %d" % (loadavg_15,now))
        return data

    def fetch_network_io(self):
        measurements = {"rx_bytes": 1, "rx_packets": 2, "rx_errors": 3, "rx_dropped": 4, "tx_bytes": 9, "tx_packets": 10, "tx_errors": 11, "tx_dropped": 12}
        now = int(time())
        raw_data = self.read_file("/proc/net/dev")
        prepared_data = []
        for line in raw_data[2:]:
            (interface, values) = line.strip().split(":")
            values = values.split()
            if interface == "lo": continue
            values.insert(0, "network." + interface)
            prepared_data.append(values)
        return self.delta_analyzer(measurements, prepared_data, now)

    def fetch_diskstats(self):
        # From kernel-source/Documentation/iostats.txt
        metrics = dict(
            reads_completed  =    1,
            reads_merged     =    2,
            sectors_read     =    3,
            time_reading     =    4,
            writes_completed =    5,
            writes_merged    =    6,
            sectors_written  =    7,
            time_writing     =    8,
            # iops is not cumulative
            current_iops     = None,
            time_total       =   10,
            time_weighted    =   11,
        )

        now = int(time())
        data = []

        try:
            raw_data = self.read_file('/proc/diskstats')
        except:
            return data

        iops = []
        for line in raw_data:
            values = line.split()[2:]
            name = values[0]

            # ignore partitions and loop devices
            if 'loop' in name or ( 'sd' in name and not name.isalpha() ):
                continue

            values[0] = 'disk.'+name
            data.append(values)
            iops.append( "%s.current_iops %s %d" % ( values[0], values[9], now ) )

        deltas = self.delta_analyzer( metrics, data, now )
        return deltas + iops

    def fetch_dmcache_stat(self):
        metrics = dict(
            used         = None,
            read_hits    =    8,
            read_misses  =    9,
            write_hits   =   10,
            write_misses =   11,
        )

        now = int(time())
        data = []
        static = []

        # Find the device
        for mapping in os.listdir( '/dev/mapper' ):
            if not mapping.endswith( '-cached' ):
                continue

            try:
                (status, raw_data) = getstatusoutput( 'dmsetup status ' + mapping )
            except:
                return data

            values = raw_data.split()

            dev = os.path.basename( os.readlink( '/dev/mapper/'+mapping ) )
            key = 'disk.%s.cache' % dev
            values.insert( 0, key )
            data.append( values )

            used,total = values[7].split('/')
            used_perc =  100.0 * int(used) / int(total) 
            static.append( '%s.used %d %d' % ( key, used_perc, now ) )

        deltas = self.delta_analyzer( metrics, data, now )
        return static + deltas


    def fetch_memory_usage(self):
        metrics = {"MemFree": "memory_free", "Buffers": "buffers", "Cached": "cached", "SwapFree": "swap_free", "Slab": "slab"}
        data = []
        now = int(time())
        raw_data = self.read_file("/proc/meminfo")
        for line in raw_data:
            metric, i = line.split(":")
            value = int(i.strip().strip(" kB")) * 1024
            if metric in metrics.keys():
                data.append("memory.%s %d %d" % (metrics[metric], value, now))
        return data

    def fetch_smb_statistics(self):
        measurements = {0: "clients", 2: "file_locks"}
        data = []
        now = int(time())
        this_node = None
        (status, raw_data) = getstatusoutput("/usr/bin/ctdb status")
        if status != 0: return data
        for line in raw_data.split("\n"):
            if line.find("THIS NODE") > 0:
                # pnn:0 10.168.22.101    OK (THIS NODE)
                this_node = line.split()[0].split(":")[1]
        if this_node is None: return
        (status, raw_data) = getstatusoutput("/usr/bin/smbstatus")
        if status != 0: return data
        for i, block in enumerate(raw_data.split("\n\n")):
            if i not in measurements.keys(): continue
            raw_data = block.split("\n")
            this_node_count = [line.startswith(this_node + ":") for line in raw_data].count(True)
            data.append("smb.%s %d %d" % (measurements[i], this_node_count, now))
        return data

    def fetch_lustre_mds_data(self):
        measurements = {"open": 2, "close": 3, "link": 4, "unlink": 5, "mkdir": 6, "rmdir": 7, "rename": 8, "getxattr": 9, "setattr": 18, "getattr": 19}
        data = []
        now = int(time())
        try:
            raw_data = self.read_file("/proc/fs/lustre/mds/sparta-MDT0000/stats")
        except:
            return data
        prepared_data = ["lustre.sparta-mdt"]
        [prepared_data.append(float(line.split()[1])) for line in raw_data]
        return self.delta_analyzer(measurements, [prepared_data], now)

def main():
    if '--daemonize' in argv:
        createDaemon()

    host = Host()
    hostname = node().split('.')[0]

    graphite = Carbon(CARBON_SERVER, CARBON_PORT);

    while True:
        data = host.get_all()
        for datum in data:
            metric = "system.%s.%s" % (hostname, datum)
            if "-debug" in argv: print metric
            graphite.send(metric)
        sleep(DELAY)

if __name__ == '__main__':
    main()
