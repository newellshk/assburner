#!/lib/init/init-d-script-python
### BEGIN INIT INFO
# Provides:          graphite-feed
# Required-Start:    $remote_fs $syslog $network
# Required-Stop:     $remote_fs $syslog $network
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Controls graphite
# Description:       This file should be used to construct scripts to be
#                    placed in /etc/init.d.  This example start a
#                    single forking daemon capable of writing a pid
#                    file.  To get other behavoirs, implemend
#                    do_start(), do_stop() or other functions to
#                    override the defaults in /lib/init/init-d-script.
### END INIT INFO

# Author: Matthew Newell <newellm@blur.com>
#
# Please remove the "Author" lines above and replace them
# with your own name if you copy and modify this script.

DESC="Collects runtime statistics and feeds them to a graphite server"
DAEMON=/usr/local/bin/graphite-feed.py
NAME=graphite-feed
DAEMON_ARGS=--daemonize
