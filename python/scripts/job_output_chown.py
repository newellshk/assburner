#!/usr/bin/python

from PyQt4.QtCore import *
from blur.Stone import *
from blur.Classes import *
import sys, os, re, traceback, time

if sys.argv.count('-daemonize'):
	from blur.daemonize import createDaemon
	createDaemon(pidFilePath='/var/run/ab_job_output_chown.pid')

# First Create a Qt Application
app = QCoreApplication(sys.argv)

# Load database config
if sys.platform=='win32':
	initConfig("c:\\blur\\resin\\resin.ini")
else:
	initConfig("/etc/job_output_chown.ini", "/var/log/ab/job_output_chown.log")
	# Read values from db.ini, but dont overwrite values from reclaim_tasks.ini
	# This allows db.ini defaults to work even if reclaim_tasks.ini is non-existent
	config().readFromFile( "/etc/db.ini", False )

initStone(sys.argv)
blurqt_loader()

Database.current().setEchoMode( Database.EchoUpdate | Database.EchoInsert | Database.EchoDelete )

VERBOSE = True
exit_after_one_job = sys.argv.count('-exit-after-one-job') > 0

hardCodedDrivePathMapping = \
	{ 	'T' : '/mnt/FX/',
		'G' : '/mnt/animation/',
		'S' : '/mnt/renderOutput/',
		'Q' : '/mnt/compOutput/',
		'V' : '/mnt/design/broadcast/',
		'I' : '/mnt/ftpRoot/',
		'K' : '/mnt/production/',
		'H' : '/mnt/user',
		'X' : '/mnt/FX',
		'W' : '/mnt/FX' }

hardCodedDriveHostMapping = \
	{ 	'T' : 'thing',
		'G' : 'thor',
		'S' : 'ironman',
		'Q' : 'cougar',
		'V' : 'goat',
		'I' : 'klump',
		'K' : 'snake',
		'H' : 'snake',
		'X' : 'hulk',
		'W' : 'beast' }

def translatePath( path ):
	driveLetter = str(path[0]).upper()
	if not driveLetter in hardCodedDrivePathMapping:
		return ('','',False,'Drive Letter not found: ' + driveLetter)
	retPath = (hardCodedDrivePathMapping[driveLetter] + path[2:].replace('\\','/')).replace('//','/')
	retHost = hardCodedDriveHostMapping[driveLetter]
	if VERBOSE: print ("Path %s translated to %s:%s" % (path, retHost, retPath))
	return ( retHost, retPath, True, '' )

def isImagePath(path):
	return len(path) > 0

def checkJob(job):
	Log( "Checking job %i %s" % (job.key(), job.name()) )
	if job.jobStatus().tasksDone() == 0:
		Log( "Skipping job, no tasks were completed" )
	elif isImagePath(job.outputPath()):
		outPath = job.outputPath()
		Log( "Changing owner to " + job.user().name() + " for files matching " + outPath )
		(host, localPath, success, errorText) = translatePath( outPath )
		if success:
			cmd = 'ssh %s "find \'%s\' -name \'%s*.%s\' -type f -user magma -maxdepth 1 -print0  | xargs -0 chown %s.render"' % (host, Path(localPath).dirPath(), QFileInfo(localPath).completeBaseName().replace(QRegExp("\\d+$"),""), QFileInfo(localPath).suffix(), job.user().name())
			Log( cmd )
			if os.system( cmd ) == 0:
				Log( "Success" )
			else:
				Log( "Cmd failed" )
		else:
			Log( "Failure translating path %s, error was %s" % (outPath, errorText) )
	else:
		Log( "Skipping job, has no output set" )
	Database.current().exec_( "UPDATE Job SET chownchecked=true WHERE keyjob=?", [QVariant(job.key())] )
	return True
	
def checkJobs():
	Log( "Checking for jobs" )
	for job in Job.select( "status IN ('done','deleted') AND chownchecked=false" ):
		checkJob(job)
		if exit_after_one_job:
			return;
	
def run():
	while True:
		checkJobs()
		if exit_after_one_job:
			return
		time.sleep(30)

if False:
	Database.current().setEchoMode(Database.EchoSelect|Database.EchoInsert|Database.EchoUpdate|Database.EchoDelete)
	
	@expression
	def driveLetterMappings(driveLetter):
		#driveLetter = PlaceHolder('driveLetter',cacheAll=True)
		return (Mapping.c.Mount == driveLetter).cache()
		
	def setupMapping(mapping,driveLetter,mountPath,host=None):
		if host:
			mapping.setHost(host)
		mapping.setMount(driveLetter)
		mapping.setMountPath(mountPath)
		mapping.commit()
	
	for driveLetter, host in hardCodedDriveHostMapping.iteritems():
		mountPath = hardCodedDrivePathMapping[driveLetter]
		driveLetter = driveLetter.lower()
		mappings = driveLetterMappings(driveLetter)
		if mappings:
			for mapping in mappings:
				setupMapping(mapping,driveLetter,mountPath)
		else:
			setupMapping(Mapping(),driveLetter,mountPath,Host.recordByName(host))
	sys.exit(0)

if __name__ == "__main__":
	run()

	