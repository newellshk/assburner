from PyQt4.QtCore import *
from PyQt4.QtSql import *
from blur.Stone import *
from blur.Classes import *
import blur
import sys, time, bisect, os, random, re, subprocess
from math import ceil, floor, log10, pow
from blur.defaultdict import DefaultDict
from exceptions import Exception
import traceback

if sys.argv.count('-daemonize'):
	from blur.daemonize import createDaemon
	createDaemon(pidFilePath='/var/run/ab_loaded_dice.pid')

pid = os.getpid()
sync_id = random.randint(1,2000000000)

app = QCoreApplication(sys.argv)

initConfig( "/etc/db.ini", "/var/log/ab/loaded_dice.log" )
# Read values from db.ini, but dont overwrite values from manager.ini
# This allows db.ini defaults to work even if manager.ini is non-existent
config().readFromFile( "/etc/db.ini", False )

initStone(sys.argv)

blur.RedirectOutputToLog()

# Load all the database tables
blurqt_loader()

# Don't log the periodic changes to assburnerAutoPacketTarget
Config.schema().removeTrigger( blur.Classes.eventLogTrigger )

VERBOSE_DEBUG = sys.argv.count('-verbose') or False
DEBUG_LICENSES = sys.argv.count('-debug-licenses') or False

sendGraphiteStats = True

if sys.argv.count('-no-graphite'):
	sendGraphiteStats = False

if VERBOSE_DEBUG:
	Database.current().setEchoMode( Database.EchoUpdate | Database.EchoDelete ) #| Database.EchoSelect )

# Connect to the database
Database.current().connection().reconnect()

# Automatically setup the AB_manager service record
# the hostservice record for our host, and enable it
# if no other hosts are enabled for this service
service = Service.ensureServiceExists('AB_loaded_dice')
hostService = service.byHost(Host.currentHost(),True)
hostService.enableUnique()

def createLocalDbConnection():
	# TODO: Configurable
	conn = Connection.create('QPSQL7')
	conn.setPort(5432)
	conn.setHost('127.0.0.1')
	conn.setDatabaseName('dice')
	conn.setUserName('dice')
	return conn

_localDb = False
def localDb():
	global _localDb
	if _localDb:
		return _localDb
	schemaFile = os.path.join( os.path.dirname(os.path.abspath(__file__)),'loaded_dice.schema')
	schema = createPythonTypesFromSchema( schemaFile, sys.modules[__name__] )[0]
	if not schema:
		raise Exception("Failed to load local schema at: " + schemaFile)
	conn = createLocalDbConnection()
	_localDb = Database(schema, conn)
	if not conn.checkConnection():
		raise Exception( "Unable to connect to local database at: " + conn.connectString() )
	_localDb.createTables()

	SyncedFile.SyncNotQueued = 0
	SyncedFile.SyncQueued = 1
	SyncedFile.SyncStarted = 2
	SyncedFile.SyncCompleted = 3
	SyncedFile.SyncErrored = 4
	SyncedFile.SyncDeleted = 5
	
	SyncedJob.SyncStarted = 1
	SyncedJob.SyncCompleted = 2
	SyncedJob.SyncErrored = 3
	return _localDb

def path_insensitive(path):
	if path == '' or os.path.exists(path):
		return path

	base = os.path.basename(path)  # may be a directory or a file
	dirname = os.path.dirname(path)

	suffix = ''
	if not base:  # dir ends with a slash?
		if len(dirname) < len(path):
			suffix = path[:len(path) - len(dirname)]

		base = os.path.basename(dirname)
		dirname = os.path.dirname(dirname)

	if not os.path.exists(dirname):
		dirname = path_insensitive(dirname)
		if not dirname:
			return

	# at this point, the directory exists but not the file
	try:  # we are expecting dirname to be a directory, but it could be a file
		files = os.listdir(dirname)
	except OSError:
		return

	baselow = base.lower()
	try:
		basefinal = next(fl for fl in files if fl.lower() == baselow)
	except StopIteration:
		return

	if basefinal:
		return os.path.join(dirname, basefinal) + suffix

# TODO: Use Mapping.translate
def transformMapPath(mapPath):
	# Only sync G: paths for now
	if mapPath[0].lower() != 'g':
		return None
	return path_insensitive('/mnt/animation' + mapPath[2:].strip())

# Finds the nth occurrence of a substring
# findNth('a,b,c,defg',',',3) returns 5
def findNth(input, find, nth):
	pos = 0
	for i in range(nth):
		pos = input.find(find,pos+1)
	return pos

class SyncConnection(QObject):
	def __init__(self,jobSyncer):
		QObject.__init__(self,None)
		self.JobSyncer = jobSyncer
		self.setupProcess()
		self.FilesInFlight = 0
		self.OutputRE = re.compile( r'.(.).{9}\s(.+)\s(\d+)' )
		
	def setupProcess(self):
		p = self.Process = QProcess(self)
		p.setProcessChannelMode(QProcess.MergedChannels)
		p.finished.connect(self.processFinished)
		p.readyRead.connect(self.processOutput)
		
# rsync output 
#cd+++++++++ mnt/animation/FSB/01_Shots/Sc001/S0020.00/SceneAssembly/point_cache/read 0
#<f+++++++++ mnt/animation/FSB/01_Shots/Sc001/S0020.00/SceneAssembly/point_cache/read/P_Whip_Mesh_X_WhipChain_Pc-Sd0_.pc2 309262123

		# TODO: Configurable
		p.start('rsync --files-from=- --no-blocking-io --out-format="%i %f %b" --outbuf=L / dice01:/',QProcess.ReadWrite | QProcess.Unbuffered)
		
	def addFile(self,path,syncedFile):
		# TODO: Configurable
		if self.Process.state() != QProcess.NotRunning and self.FilesInFlight >= 200:
			self.complete()
			self.setupProcess()
		path = path + '\n'
		self.Process.write(path)
		print 'Queued File:', path
		self.FilesInFlight += 1
		self.processOutput()
	
	def processOutput(self):
		while self.Process.canReadLine():
			line = unicode(self.Process.readLine())
			print 'Read line:', line
			# Check for errors
			if line.startswith('rsync'):
				self.JobSyncer.syncError(line)
			else:
				m = self.OutputRE.match(line)
				if not m:
					raise Exception('Unable to parse rsync output: ' + line)
				pathType, path, bytes = m.groups()
				if pathType == 'f':
					path = '/' + path
					print 'Synced file:', path
					self.FilesInFlight -= 1
					self.JobSyncer.fileSynced(path)
				
	def processFinished(self, exitCode,exitStatus):
		print "Process exited"
		if exitStatus != QProcess.NormalExit or exitCode != 0:
			self.JobSyncer.syncError('Process exited with code: %i' % exitCode)
		
	def complete(self):
		# EOF to rsync
		self.Process.waitForBytesWritten(50)
		self.Process.closeWriteChannel()
		while self.Process.state() != QProcess.NotRunning:
			self.Process.waitForReadyRead(50)
			self.processOutput()
		self.processOutput()

class JobSyncer(QThread):
	def __init__(self,jobLocation):
		QThread.__init__(self)
		
		jobLocation.setSyncId(sync_id)
		jobLocation.setSyncState(JobLocation.SyncStarted)
		jobLocation.setColumnLiteral(JobLocation.c.SyncStartTime,'now()')
		jobLocation.commit()
		self.JobLocation = jobLocation

		self.Job = jobLocation.job()
		
		existing = (SyncedJob.c.Key == self.Job.key())()
		if existing:
			self.SyncedJob = existing[0]
		else:
			self.SyncedJob = SyncedJob()
			self.SyncedJob.setValue(SyncedJob.c.Key, self.Job.key())
		self.SyncedJob.setSyncState(SyncedJob.SyncStarted)
		self.SyncedJob.commit()

		# List of records where another thread is already syncing
		# the file and we'll need to wait when we are done syncing
		# our files
		self.SyncedFileWaitList = SyncedFileList()
	
		self.QueuedFileMap = {}
		
	def getSyncedFile(self,mapPath,stat):
		while True:
			sfl = (SyncedFile.c.Path == mapPath)()
			if sfl:
				sf = sfl[0]
				break
			else:
				try:
					sf = SyncedFile()
					sf.setPath(mapPath)
					sf.setSize(stat.st_size)
					sf.setMtime(QDateTime.fromTime_t(int(stat.st_mtime)))
					sf.setColumnLiteral(sf.c.SyncDateTime,'now()')
					sf.commit()
					break
				except SqlException:
					continue
		return sf
	
	def getSyncedJobFile(self,syncedJob,syncedFile):
		existing = ((SyncedJobFile.c.SyncedJob == syncedJob) & (SyncedJobFile.c.SyncedFile == syncedFile))()
		if existing:
			return existing[0]
		return SyncedJobFile().setSyncedJob(syncedJob).setSyncedFile(syncedFile)
	
	def syncError(self,message):
		print 'Sync Error:', message
		self.SyncedJob.setSyncState(SyncedJob.SyncErrored).commit()
		self.JobLocation.setSyncState(JobLocation.SyncErrored).commit()
		
	# TODO: Standard format or plugin enabled
	def readMapList(self):
		statsPath = str(self.Job.fileName())
		if not statsPath:
			return
		if statsPath[1] == ':':
			statsPath = statsPath[2:]
		if not statsPath.endswith('.max'):
			raise Exception('Only max files are supported currently')
		statsPath = '/mnt/ab_spool/' + statsPath[:-4] + '.txt'
		if not os.path.exists(statsPath):
			raise Exception('Unable to find stats file at:' + statsPath)
		with open(statsPath) as statsFile:
			for l in statsFile:
				if not l.startswith('map,') and not l.startswith('max,') and not l.startswith('PC,'):
					continue
				mapPath = l[findNth(l,',',3)+1:] # Everything after the third comma
				mapPath = transformMapPath(mapPath)
				# transformMapPath returns None for non-synced paths
				if mapPath is not None:
					yield mapPath

	def fileSynced(self,path):
		sf = self.QueuedFileMap[path]
		sf.setSyncState(SyncedFile.SyncCompleted)
		sf.commit()
		del self.QueuedFileMap[path]

	def queueFile(self,path,syncedFile):
		syncedFile.setSyncState(SyncedFile.SyncStarted)
		syncedFile.setSyncId(sync_id)
		syncedFile.commit()
		self.QueuedFileMap[path] = syncedFile
		self.SyncConn.addFile(path,syncedFile)

	def run(self):
		Database.setCurrent(blurDb())
		Database.setCurrent(localDb(),False)
		localDb().setConnection(createLocalDbConnection())
		
		try:
			self.SyncConn = SyncConnection(self)
			jl = self.JobLocation
			sj = self.SyncedJob
			
			syncBytes = 0
			for mapPath in self.readMapList():
				
				# Check to see if we got a status change
				if sj.syncState() != SyncedJob.SyncStarted:
					break

				# Canceled
				if jl.syncState() != JobLocation.SyncStarted:
					return
				
				stat = os.stat(mapPath)
				mapSize = stat.st_size
				
				syncedFile = self.getSyncedFile(mapPath,stat)
				
				sjf = self.getSyncedJobFile(sj,syncedFile)
				sjf.commit()

				if syncedFile.syncState() == SyncedFile.SyncStarted:
					self.SyncedFileWaitList.append(syncedFile)
					continue
				elif syncedFile.syncState() == SyncedFile.SyncCompleted and QDateTime.fromTime_t(int(stat.st_mtime)) == syncedFile.mtime():
					continue

				self.queueFile(mapPath,syncedFile)

				syncBytes += mapSize
				# Update sync progress every 100MB
				if (syncBytes / 1048576) - jl.syncMegabytes() > 100:
					jl.setSyncMegabytes(syncBytes / 1048576).commit()
				
			while self.SyncedFileWaitList:
				exp = (SyncedFile.c.SyncState == SyncedFile.SyncStarted) & (SyncedFile.c.SyncId == sync_id)
				self.SyncedFileWaitList, toProcess = self.SyncedFileWaitList.reloaded().part(exp)
				for syncedFile in toProcess.filter(SyncedFile.c.SyncState != SyncedFile.SyncCompleted):
					# File has been returned, sync it ourselves
					self.queueFile( unicode(syncedFile.path()), syncedFile )
				
			self.SyncConn.complete()
				
			jl.setSyncMegabytes(syncBytes / 1048576)
			jl.setColumnLiteral(JobLocation.c.SyncEndTime,'now()')
			jl.setSyncState(JobLocation.SyncCompleted)
			jl.commit()
			
			sj.setSyncState(SyncedJob.SyncCompleted)
			sj.commit()
			
		except:
			import traceback
			traceback.print_exc()
			jl.setSyncState(JobLocation.SyncErrored).commit()
			sj.setSyncState(SyncedJob.SyncErrored).commit()
			sfl = SyncedFileList(self.QueuedFileMap.values())
			sfl.setSyncStates(SyncedFile.SyncErrored)
			sfl.commit()
			
			# Restart daemon just in case
			raise


class Cleaner(QThread):
	def __init__(self,parent=None):
		QThread.__init__(self,parent)
	
	def cleanFile(self,jobFile):
		path = unicode(jobFile.path())
		# TODO: Configurable
		if system('ssh dice01 "rm %s"' % path) != 0:
			print "Unable to delete file at: ", path
			return 0
		return os.stat(path).st_size
	
	def calculateNeededSpace(self):
		try:
			# TODO: Configurable
			line = subprocess.check_output('ssh dice01 "df /mnt/animation"',shell=True).splitlines()[1]
			device, blocks, usedblocks, freeblocks, percent_used, mountpoint = re.split('\s+',line)
			percent_used = float(percent_used[:-1])
			if percent_used > 95:
				return (95 - percent_used) * blocks * 1024
		except:
			traceback.print_exc()
		return 0
	
	def checkDeleted(self):
		toDelete = (JobLocation.c.SyncState == JobLocation.SyncCompleted) && (JobLocation.c.Job.in_(Job.c.Status.in_(['done','deleted','archived'])))
		for jl in toDelete:
			# Foreign key relationship will delete SyncedJobFile records
			SyncedJob(jl.getValue(JobLocation.c.Key).toInt()).remove()
	
	def run(self):
		runCount = 0
		while True:
			runCount += 1
			
			# Every half hour check for deleted jobs
			if runCount >= 30:
				checkDeleted()
			
			# Run every minute TODO: Configurable
			self.sleep(60)

			sizeNeeded = self.calculateNeededSpace()
			if sizeNeeded <= 0:
				continue
			
			jobFiles = (JobFile.c.UsageCount == 0).orderBy(JobFile.c.LastUsageDateTime,Expression.AscendingOrder).limit(50)()
			for jobFile in jobFiles:
				sizeNeeded -= self.cleanFile(jobFile)
				if sizeNeeded < 0:
					break

def loop():
	# Find all jobs that need synced including ones that were already
	# started but the daemon has died for some reason
	exp = JobLocation.c.SyncState == JobLocation.SyncNotStarted
	exp |= (JobLocation.c.SyncState == JobLocation.SyncStarted) & (JobLocation.c.SyncId != sync_id)

	syncPool = []
	cleaner = Cleaner()
	cleaner.start()
	
	while True:
		for jl in exp():
			syncer = JobSyncer(jl)
			syncer.start()
			syncPool.append(syncer)

		service.pulse()
		
		# Drop references to finished syncers
		syncPool = [syncer for syncer in syncPool if syncer.isRunning()]
		
		time.sleep(10)

def init():
	Database.setCurrent( localDb(), False )

if __name__ == "__main__":
	init()
	loop()
