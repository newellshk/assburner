
import manager
import reaper
from blur.quickinit import *

def dbe(sql,args = []):
	return Database.current().exec_(sql,args)

def clearTables(tables):
	for table in tables:
		dbe("DELETE FROM " + table)

def clearDatabase():
	clearTables( ['Job','JobStatus','JobTask','JobService','Service','Host','HostService','License'] )

def createHost(name,services,initialStatus='ready'):
	host = Host().setName(name).commit()
	for service in services:
		HostService().setHost(host).setService(service).setEnabled(True).commit()
	host.hostStatus().setSlaveStatus(initialStatus).commit()
	return host

def createService(name,license=License()):
	return Service().setService(name).setEnabled(True).setLicense(license).commit()

def createJobTasks(job,frameList):
	ret = JobTaskList()
	for frame in expandNumberList(frameList)[0]:
		ret += JobTask().setJob(job).setFrameNumber(frame).setStatus('new')
	ret.commit()
	return ret

def createBatchJob(name,priority,services=[],frameList='1'):
	job = JobBatch().setName(name).setStatus('submit').setPriority(priority).setCmd('sleep 500').setJobType(JobType.recordByName('Batch')).commit()
	createJobTasks(job,frameList)
	for service in services:
		JobService().setJob(job).setService(service).commit()
	job.setStatus('verify').commit()
	job.setStatus('ready').commit()
	return job
	
# We should split up assburner so that we can have pyassburner and pass
# a host record to the slave, then we can test actual assburner code
def simulateAssburner():
	dbe("UPDATE HostStatus SET slavestatus='ready',fkeyjob=NULL WHERE slavestatus='starting'")

# Unassigns 'assigned' tasks from lower priority job to make available for higher priority job
def testUnassignment():
	print "\n\nTesting Unassignment\n\n"
	clearDatabase()
	
	# Setup Service with license
	assburner = createService('Assburner')
	service = createService('Test')
	host = createHost('Host',[assburner,service])
	job1 = createBatchJob('Test1',50,[assburner])
	manager.run_loop()
	
	# Job1 should get assigned to host
	assert( host.hostStatus().reload().slaveStatus() == 'assigned' )
	assert( job1.jobTasks()[0].host() == host )
	
	job2 = createBatchJob('Test2',1,[service])
	manager.run_loop()
	simulateAssburner()
	
	assert( job1.jobTasks()[0].status() == 'new' )
	manager.run_loop()
	assert( job2.jobTasks()[0].host() == host )

# Test that we don't unassign for a job that has no licenses available
def testLicenseUnassignment():
	print "\n\nTesting License Unassignment\n\n"
	clearDatabase()
	
	# Setup Service with license
	lic = License().setTotal(0).commit()
	assburner = createService('Assburner')
	service = createService('Test',lic)
	host = createHost('Host',[service,assburner])
	job1 = createBatchJob('Test1',50)
	manager.run_loop()
	
	# Job1 should get assigned to host
	assert( host.hostStatus().reload().slaveStatus() == 'assigned' )
	assert( job1.jobTasks()[0].host() == host )
	
	# Create higher priority job that current has no licenses available
	job2 = createBatchJob('Test2',1,[service])
	# Make sure nothing gets unassigned since it has not licenses
	manager.run_loop()
	assert( job1.jobTasks()[0].host() == host )
	
	# Give it a license and test that job Test1 gets unassigned
	lic.setTotal(1).commit()
	manager.run_loop()
	simulateAssburner()
	assert( job1.jobTasks()[0].status() == 'new' )
	manager.run_loop()
	assert( job2.jobTasks()[0].host() == host )
	
	# Create new high priority job with same license needs
	job3 = createBatchJob('Test3',1,[service])
	# Change current assigned jobs priority
	job2.setPriority(40).commit()
	manager.run_loop()
	simulateAssburner()
	Database.current().setEchoMode(Database.EchoSelect)
	print lic.reload().dump()
	Database.current().setEchoMode(0)
	assert( job2.jobTasks()[0].status() == 'new' )
	manager.run_loop()
	assert( job3.jobTasks()[0].host() == host )
	
def testReassignment():
	print "\n\nTesting Reassignment\n\n"
	clearDatabase()
	
	# Setup Service with license
	assburner = createService('Assburner')
	service = createService('Test')
	host = createHost('Host1',[assburner,service])
	host2 = createHost('Host2',[assburner,service])
	host3 = createHost('Host3',[assburner,service])
	job1 = createBatchJob('Test1',50,[assburner],'1-2')
	manager.run_loop()
	
	# Job1 should get assigned two hosts
	for jt in job1.jobTasks():
		print( jt.status() == 'assigned' )
	
	# Job2 will get all tasks assigned to the remaining ready host
	job2 = createBatchJob('Test2',1,[service],'1-3')
	manager.run_loop()
	simulateAssburner()
	reaper.run_loop()
	assert( job2.jobStatus().reload().hostsOnJob() == 1 )
	
	# Create the condition that two tasks for one host will take too long
	# One loop through the manager will free up a host, but not yet assign it
	# to job2
	jt = job2.jobTasks()[0].setStatus( "done" ).setStartedts( QDateTime.currentDateTime().addSecs( -60 * 61 ) ).setEndedts( QDateTime.currentDateTime() ).commit()
	reaper.run_loop()
	manager.run_loop()
	simulateAssburner()
	
	# Now job2 should get a task freed then assigned to the freed host
	reaper.run_loop()
	manager.run_loop()
	simulateAssburner()
	reaper.run_loop()
	assert( job2.jobStatus().reload().hostsOnJob() == 2 )
	assert( job1.jobStatus().reload().hostsOnJob() == 1 )

if __name__=='__main__':
	manager.VERBOSE_DEBUG=True
	# Disable throttling
	manager.throttler.assignRate=9999999
	manager.throttler.assignLeft=9999999
	testUnassignment()
	testLicenseUnassignment()
	testReassignment()