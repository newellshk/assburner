#!/usr/bin/python

from PyQt4.QtCore import *
from PyQt4.QtSql import *
from blur.Stone import *
from blur.Classes import *
import blur.email, blur.jabber
import sys, time, re
import traceback
from optparse import OptionParser

app = None
VERBOSE_DEBUG = False

def findMatchingDeliveryPrefs( notifDest ):
	notif = notifDest.notification()
	prefs = NotificationUserPref.select( "fkeyuser=? AND fkeynotificationevent=? AND enabled=true", [QVariant(notifDest.user().key()), QVariant(notifDest.notification().notificationEvent().key())] )
	if prefs.count() <= 1:
		return prefs
	# Do not re-send meeting creation notification if it's already sent and it's repeated events
	# Only send notification before meeting happens
	if notif.isRecord() and notif.parent().isRecord():
		return prefs.filter(NotificationUserPref.c.DeliverBefore==True)
	return prefs

def roundTimestamp( timestamp, offsetType, deliverBefore ):
	if offsetType >= 1: # Hour
		offset = -(timestamp.time().minute() * 60 + timestamp.time().second() )
		if not deliverBefore and offset != 0:
			offset = 3600 + offset
		timestamp = timestamp.addSecs( offset )
	if offsetType >= 2: # Day
		offset = -(60 * 60 * timestamp.time().hour())
		if not deliverBefore and offset != 0:
			offset = 3600 * 24 + offset
		timestamp = timestamp.addSecs( offset )
	if offsetType >= 3: # Week
		offset = (1-timestamp.date().dayOfWeek())
		if not deliverBefore and offset != 0:
			offset = 7 + offset
		timestamp = timestamp.addDays( offset )
	return timestamp

def getDeliveryTime( notif, pref ):
	time = None
	if pref.deliveryTrigger() == 1: # Created Time
		time = notif.created()
	else:
		time = notif.occursAt()
		if not time.isValid():
			time = notif.created()
	# Shouldn't happen but lets handle it gracefully
	if not time.isValid():
		time = QDateTime.currentDateTime()
	print "Trigger time:", time.toString()
	time = roundTimestamp( time, pref.offsetType(), pref.deliverBefore() )
	print "Rounded time:", time.toString()
	offset = pref.offset()
	if pref.deliverBefore():
		offset *= -1
	print "Offset: ", offset.toString()
	time = offset.adjust( time )
	print "Adjusted time:", time.toString()
	return time

def compile_history( notif ):
	history = []
	prev = Notification.recordByUpdatedNotification( notif )
	updateCreatedTime = notif.created()
	while prev.isRecord():
		history.append(updateCreatedTime.toString() + ": " + prev.prev.updateText())
		updateCreatedTime = prev.created()
		prev = Notification.recordByUpdatedNotification( prev )
	if history:
		return '\n'.join( ['History:\n'] + history )
	return ''

def notify_jabber( dest ):
	notif = dest.notification()
	recip = dest.destination()

	body = '[%s - %s Notification] %s\n' % (notif.component(),notif.event(),notif.subject())
	if ( notif.brief() ):
		body += notif.brief()
	else:
		body += notif.message()

	blur.jabber.send( 'thepipe@jabber.blur.com/Notifier','thePipe',str(recip), body )

def notify_email( dest ):
	# Grab the recipients and notification
	recip = dest.destination()
	notif = dest.notification()

	# Grab the html template
	html = Config.recordByName( 'notification_email_template' ).value()

	# Grab the component/event notification
	subject = notif.subject()
	brief = notif.brief()
	body = str( notif.message() )
	body.replace( '\n', '<br/>' )

	mail_subject = '[%s - %s Notification] %s' % (notif.component(),notif.event(),subject)

	# Grab additional information
	extra_info = []
	if ( notif.occursAt().isValid() ):
		extra_info.append( '<hr><b>Occurs At: </b> %s' % notif.occursAt().toString( 'MM.dd.yyyy @ h:mm ap' ) )

	users = NotificationDestination.select( 'fkeynotification = %i' % notif.key() ).users().unique()

	if users:
		extra_info.append( '<hr><b>Notification sent to:</b><ul>' )
		for user in users:
			extra_info.append( '<li>%s</li>' % user.displayName() )
		extra_info.append( '</ul>' )

	body += '\n'.join( extra_info )

	mail_body = str(html) % { 'subject': subject, 'brief': brief, 'body': body, 'history' : compile_history( notif ) }

	blur.email.send( sender = 'thePipe@blur.com', recipients = [str(recip)], subject = mail_subject, body = mail_body, html = True )

def fill_default_destination( dest ):
	meth = dest.notificationMethod().name()
	recip = ''
	if meth == 'Email':
		recip = dest.user().email()
		if not '@' in recip:
			recip += '@blur.com'
	elif meth == 'Jabber':
		recip = dest.user().jid()
		if not '@' in recip:
			recip += '@jabber.blur.com'
	Log( "User's %s notification has no destination, setting to %s" % (meth,recip) )
	dest.setDestination( recip )
	return dest

def get_routes( notif, user ):
	sql = '(eventMatch IS NULL or eventMatch ~ ?) AND (componentMatch IS NULL OR componentMatch ~ ?)'
	#'AND (subjectMatch IS NULL OR subjectMatch ~ ?) ' + 'AND (messageMatch IS NULL OR messageMatch ~ ?)')
	args = [QVariant(notif.event()), QVariant(notif.component())]
	#, QVariant(notif.subject()), QVariant(notif.message())]
	if user.isRecord():
		sql += ' AND (fkeyUser=?) AND (matchdestination=true) '
		args.append( QVariant(user.key()) )
	else:
		sql += ' AND (matchdestination=false) '
	return NotificationRoute.select( sql, args ).sorted( 'priority' )

def isDestinationUniqueAndValid(dest):
	method = dest.notificationMethod()
	if method.isRecord() and not dest.destination().isEmpty():
		return NotificationDestination.select( 'fkeyNotification=? AND fkeyNotificationMethod=? AND destination=? AND fkeynotificationuserpref=? AND routed IS NOT NULL',
				[QVariant(dest.notification().key()), QVariant(method.key()), QVariant(dest.destination()), QVariant(dest.notificationUserPref().key()) ] ).isEmpty()
	return False

class RouteAction:
	def __init__(self, parts):
		self.Action = parts[0]
		self.Method = parts[1]
		try: # These parts are not required
			self.Dest = parts[2]
			self.User = parts[3]
		except: pass

	def applyToNotification(self,notif):
		if self.Action == 'add':
			method = NotificationMethod.recordByName( self.Method )
			user = User()
			if hasattr(self,'User'):
				user = User.recordByUserName( self.User )
			if method.isRecord() and (user.isRecord() or (hasattr(self,'Dest') and self.Dest)):
				nd = NotificationDestination()
				nd.setNotification( notif )
				nd.setUser( user )
				nd.setNotificationMethod( method )
				if hasattr( self, 'Dest' ):
					nd.setDestination( self.Dest )
				return nd
		return None

	def applyToDestination(self,dest,needsDefault):
		if self.Action == 'add':
			newDest = self.applyToNotification(dest.notification())
			# Route adds a new destination, set as owned by same user
			if not newDest.user().isRecord() and dest.user().isRecord():
				newDest.setUser( dest.user() )

			return (self.applyToNotification(dest.notification()),dest)
		elif needsDefault and self.Action == 'default':
			method = NotificationMethod.recordByName( self.Method )
			if method.isRecord():
				dest.setNotificationMethod( method )
				if hasattr( self,'Dest' ): # self.Dest may not exist
					dest.setDestination( self.Dest )
				return (None,dest)
		return (None,dest)

def parseActionString( actionString ):
	actions = []
	sections = actionString.split( ',' )
	for s in sections:
		parts = s.split(':')
		# Action:Method is required
		if len(parts) >= 2:
			actions.append( RouteAction( parts ) )
	return actions

def route_notification( notif ):
	Log( "Routing Notification: %i" % notif.key() )
	# Apply NotificationRoute matches
	routes = get_routes( notif, User() )
	newDests = NotificationDestinationList()
	for route in routes:
		actions = parseActionString( route.actions() )
		for action in actions:
			res = action.applyToNotification( notif )
			if res:
				newDests += res
	newDests.commit()
	notif.setColumnLiteral( 'routed', 'NOW()' )
	notif.commit()

def route_destination( dest ):
	Log( "Routing Destination: %i" % dest.key() )
	template = dest.copy()
	newDests = NotificationDestinationList()
	# Apply NotificationUserPref matches
	prefs = findMatchingDeliveryPrefs( dest )
	for pref in prefs:
		dest.setNotificationUserPref( pref )
		delTime = getDeliveryTime( dest.notification(), pref )
		print delTime
		dest.setScheduled( delTime )
		dest.setNotificationMethod( pref.notificationMethod() )
		newDests += dest
		dest = template
	if newDests.isEmpty():
		newDests += dest
	for dest in newDests:
		if dest.user().isRecord():
			if not dest.notificationMethod().isRecord():
				Log( "User Notification has no method, setting to Email" )
				dest.setNotificationMethod( NotificationMethod.recordByName( 'Email' ) )
			if dest.destination().isEmpty():
				dest = fill_default_destination( dest )
		if isDestinationUniqueAndValid( dest ):
			dest.setColumnLiteral( 'routed', 'NOW()' )
			dest.commit()
		else:
			Log( "Removing Destination because it is invalid or a duplicate: %i" % dest.key() )
			dest.remove()

def buildOptionsParser():
	parser = OptionParser(usage="usage: %prog [options]")
	parser.add_option( "-t", "--run-tests", 			dest="runTests", 		action="store_true", default=False, help="Run the builtin tests, then exit" )
	parser.add_option( "-d", "--daemonize", 			dest="daemonize", 		action="store_true", default=True, help="Forks the process as a daemon" )
	parser.add_option( "-n", "--no-daemonize", 			dest="daemonize", 		action="store_false", help="Disables daemon forking" )
	parser.add_option( "-v", "--verbose", 				dest="verbose",			action="store_true", default=False, help="Enables verbose output for debugging" )
	return parser

def initialize(redirectOutputToLog=True,ensureDbConnection=True):
	global app
	app = QCoreApplication(sys.argv)
	initConfig( "/etc/notifier.ini", "/var/log/ab/notifier.log" )
	# Read values from db.ini, but dont overwrite values from notifier.ini
	# This allows db.ini defaults to work even if notifier.ini is non-existent
	config().readFromFile( "/etc/db.ini", False )

	if redirectOutputToLog:
		blur.RedirectOutputToLog()

	blurqt_loader()

	if VERBOSE_DEBUG:
		Database.current().setEchoMode( Database.EchoUpdate | Database.EchoDelete )# | Database.EchoSelect )

	if ensureDbConnection:
		Database.current().connection().reconnect()

def runTests():
	initialize(False,False)
	timestamp = QDateTime.currentDateTime()
	print "Testing rounding of timestamp: " + timestamp.toString()
	print "Rounded to next hour: " + roundTimestamp(timestamp,1,False).toString()
	print "Rounded to next day: " + roundTimestamp(timestamp,2,False).toString()
	print "Rounded to next week: " + roundTimestamp(timestamp,3,False).toString()
	print "Rounded to previous hour: " + roundTimestamp(timestamp,1,True).toString()
	print "Rounded to previous day: " + roundTimestamp(timestamp,2,True).toString()
	print "Rounded to previous week: " + roundTimestamp(timestamp,3,True).toString()

def notifier(daemonize=False):

	if daemonize:
		from blur.daemonize import createDaemon
		print "Forking deamon"
		createDaemon(pidFilePath='/var/run/notifier.pid')

	initialize()

	n = Notification.create( 'Notifier', 'Starting', 'Notifier is starting' )
	#newellm = User.recordByUserName( 'newellm' )
	#n.sendTo( newellm )#, 'Jabber' )
	#n.sendTo( newellm, 'Email' )

	# A dict, to map method names to their sending functions
	methods = {
		'Jabber' : notify_jabber,
		'Email' : notify_email }

	service = Service.ensureServiceExists('Notifier')

	# Loop, Routing and Delivering the notifications
	while True:
		service.pulse()

		if service.enabled():
			Log( "Routing Notifications" )
			# Route the notifications and destinations
			for table, route_method in [ (Notification,route_notification), (NotificationDestination,route_destination) ]:
				to_route = table.select( "routed IS NULL" )
				for notif in to_route:
					route_method(notif)

			# Send to the routed destinations
			to_send = NotificationDestination.select( "delivered IS NULL AND routed IS NOT NULL AND (scheduled IS NULL or scheduled < (now() - '10 seconds'::interval))" )

			# Sending Notifications
			for dest in to_send:
				try:
					print "Sending dest ", dest.key()
					methods[str(dest.notificationMethod().name())]( dest )
				except:
					Log( 'Couldnt find notification method for destination: %i' % dest.key() )
					traceback.print_exc()
				dest.setColumnLiteral( 'delivered', 'NOW()' )
				dest.commit()

		time.sleep(8)

if __name__ == "__main__":
	opt_parser = buildOptionsParser()
	opt_parser.parse_args()
	opts = opt_parser.values

	VERBOSE_DEBUG = opts.verbose
	if opts.runTests:
		runTests()
	else:
		notifier(opts.daemonize)
