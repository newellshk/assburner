#!/lib/init/init-d-script-python
### BEGIN INIT INFO
# Provides:          notifier
# Required-Start:    $remote_fs $syslog $network
# Required-Stop:     $remote_fs $syslog $network
# Default-Start:     2 3 4 5
# Default-Stop:
# Short-Description: Notifier daemon
# Description:       
#                    
### END INIT INFO
# -*- coding: utf-8 -*-
# Debian init.d script for notifier
# Copyright © 2014 Matt Newell <newellm@blur.com>

DAEMON=/usr/bin/notifier.py
DAEMON_ARGS=--daemonize
NAME=notifier
DESC="notifier"

