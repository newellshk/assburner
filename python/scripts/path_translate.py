#!/usr/bin/python
import sys
import os
import subprocess
import ntpath
import posixpath
from trax.api.data import Mapping

#Passing path from clipboard
pathInput=  subprocess.check_output( ['xclip','-selection', 'clipboard', '-o'], stdin=subprocess.PIPE)

#print pathInput
#simple check of the path is either win or linux and translate accordingly
if "\\" in pathInput:
    pathOutput = Mapping.translatePath(pathInput)
else:
    pathOutput = Mapping.translatePath(pathInput,ntpath)
#print pathOutput
#passes Output to clipbaord
p = subprocess.Popen( ['xclip', '-selection', 'clipboard'], stdin=subprocess.PIPE)
p.communicate(input=pathOutput)
