#!/usr/bin/python

from PyQt4.QtCore import *
from PyQt4.QtSql import *
import blur.Stone
from blur.Stone import *
from blur.Classes import *
import blur.email, blur.jabber
import sys, time, re, os
from math import ceil
import traceback

try:
	import popen2
except: pass

if sys.argv.count('-daemonize'):
	from blur.daemonize import createDaemon
	createDaemon(pidFilePath='/var/run/ab_reaper.pid')

app = None
VERBOSE_DEBUG = False
sendGraphiteStats = True

if sys.argv.count('-no-graphite'):
	sendGraphiteStats = False
	
# Holds PID of checker:Job
newJobCheckers = {}

# If we get an unexpected exception while checking a job, send one email
# to notify IT of the problem
exceptionEmailSent = False

def startup():
	global app
	global errorStep
	if QCoreApplication.instance():
		app = QCoreApplication.instance()
	else:
		app = QCoreApplication(sys.argv)

	initConfig( "/etc/reaper.ini", "/var/log/ab/reaper.log" )
	# Read values from db.ini, but dont overwrite values from reaper.ini
	# This allows db.ini defaults to work even if reaper.ini is non-existent
	blur.Stone.config().readFromFile( "/etc/db.ini", False )

	blur.Stone.initStone( sys.argv )

	blur.RedirectOutputToLog()

	blurqt_loader()

	if VERBOSE_DEBUG:
		Database.current().setEchoMode( Database.EchoUpdate | Database.EchoDelete | Database.EchoInsert ) #| Database.EchoSelect )
	
	Database.current().connection().reconnect()

	config.update()
	
	# Make sure that people get notified at most once per error

# Stores/Updates Config vars needed for reaper
class ReaperConfig:
	def update(self):
		self.managerDriveLetter = str(Config.getString('managerDriveLetter')).lower()
		self.spoolDir = Config.getString('managerSpoolDir')
		self.requiredSpoolSpacePercent = Config.getFloat('assburnerRequiredSpoolSpacePercent',5.0)
		self.totalFailureThreshold = Config.getInt('assburnerTotalFailureErrorThreshold',30)
		self.permissableMapDrives = Config.getString('assburnerPermissableMapDrives','G:,V:')
		self.assburnerForkJobVerify = Config.getBool('assburnerForkJobVerify',False)
		self.assburnerThreadedJobVerify = Config.getBool('assburnerThreadedJobVerify',True)
		self.jabberDomain = Config.getString('jabberDomain','jabber.blur.com')
		self.jabberSystemUser = Config.getString('jabberSystemUser','thepipe')
		self.jabberSystemPassword = Config.getString('jabberSystemPassword','thePipe')
		self.jabberSystemResource = Config.getString('jabberSystemResource','Reaper')
		self.errorStep = min(1,Config.getInt('assburnerErrorStep',1))
		
config = ReaperConfig()

def execReturnStdout(cmd):
	p = popen2.Popen3(cmd)
	while True:
		try:
			p.wait()
			break
		except OSError:
			print 'EINTR during popen2.wait, ignoring.'
			pass
	return p.fromchild.read()

def getLocalFilePath(job):
	if config.spoolDir != "":
		baseName = QFileInfo(job.fileName()).fileName()
		userName = job.user().name()
		userDir = config.spoolDir + "/" + userName + "/"
		filePath = userDir + baseName
		return str(filePath)
	else: return str(job.fileName())
	

def cleanupJob(job):
	print "cleanupJob called on %s keyJob: %i" % (job.name(), job.key())
	if job.user().isRecord():
		filePath = getLocalFilePath(job)
		filePathExtra = re.sub('(\.zip|\.max)','.txt',filePath)

		if config.spoolDir != "":
			QFile.remove(filePath)
			QFile.remove(filePathExtra)
	
	job.setCleaned(True)
	job.commit()

def diskFree(path):
	if len(path) < 1:
		path = '/'
	dfcmd = '/bin/df'
	inp = []
	for line in execReturnStdout('%s %s 2>&1' % (dfcmd, path)).splitlines():
		line = line.strip()
		if line.find('Filesystem') >= 0:
			continue
		inp.append(line)
	parts = re.split('\s+',' '.join(inp))
	device, blocks, usedblocks, freeblocks, percent_used, mountpoint = parts
	percent_used = int(re.sub('\%','',percent_used))
	return (blocks,usedblocks,freeblocks,percent_used)


# Usage: checkSpoolSpace( SpoolDir, RequiredPercent )
# Returns: 1 for sufficient space, else 0
def checkSpoolSpace(requiredPercent):
	(blocks,usedblocks,freeblocks,percent_used) = diskFree(config.spoolDir)
	if (100 - percent_used) >= requiredPercent: return True
	print "checkSpoolSpace found required percent free disk space on %s insufficient (%i %% used)!" % (config.spoolDir, percent_used)
	return False

def cleanupJobs():
	if VERBOSE_DEBUG: print "cleanupJobs called, retrieving cleanable jobs"
	jobsCleaned = 0
	for job in retrieveCleanable():
		print "Found cleanable Job: %s %i" % (job.name(), job.key() )
		cleanupJob(job)
		jobsCleaned+=1
	if VERBOSE_DEBUG: print "Job::cleanupJobs done, returning,", jobsCleaned
	return jobsCleaned

def ensureSpoolSpace(requiredPercent):
	triedCleanup = False
	while not checkSpoolSpace( requiredPercent ):
		print "ensureSpoolSpace -- checkSpool indicates not enough free space, attemping to free space!"
		if not triedCleanup:
			triedCleanup = True
			if cleanupJobs() > 0:
				continue
		jobs = JobMax.select("status='done' ORDER BY endedts asc limit 1")
		#only auto-deleting MaxXXX and MaxScript jobs for now, some batch jobs will be really old and ppl will want them saved 
		#no matter what disk space they use, and they usually don't use any
		if not jobs.isEmpty():
			job = jobs[0]
			print "Setting Job %s to deleted and removing files to free disk space" % (job.name())
			job.setStatus('deleted')
			cleanupJob(job)
			job.setCleaned(True)
			job.commit()
			notifylist = ''# "it:e"
			if job.user().isRecord():
				notifylist += job.user().name() + ":je"   
                        if notifylist:
 		        	notifySend( notifyList = notifylist, subject = 'Job auto-deleted: ' + job.name(),
						body = "Your old job was automatically deleted to free up space on the Assburner job spool. Job: " + job.name() )
		else:
			print "Failed to ensure proper spool space, no more done jobs to delete and cleanup."
			return False
	return True


class NewJobChecker:
	def __init__(self,job):
		self.Job = job

	def checkSubmissionFinished(self):
		if not self.Job.user().isRecord():
			print "Job missing fkeyUsr"
			return False

		fileName = str(self.Job.fileName())
		if fileName and len(fileName):
			if config.spoolDir != "":
				if self.Job.uploadedFile() and not config.managerDriveLetter in fileName.lower():
					print "Job submission not finished, still has local fileName %s" % (self.Job.fileName())
					return False

			if self.Job.checkFileMd5():
				filePath = getLocalFilePath(self.Job)
				filePathExtra = re.sub('(\.zip|\.max)','.txt',filePath)
			
				if not QFile.exists( filePath ):
					print "Job file not found:", filePath
					return False
			
				if filePath in execReturnStdout( 'lsof %s 2>&1' % (filePath) ):
					print "Job file still open:", filePath
					return False
			
				#Should this be a regular check causing a submission failure???
				if config.spoolDir != "":
					if not str(self.Job.key()) in filePath:
						print "Job file without job key:", filePath
						return False
		return True
	
	def recordFailure(self, subject, body, notifyList="it:e"):
		print "Job check failed with error: " + subject
		je = JobError()
		je.setJob( self.Job )
		je.setMessage( subject + '\n' + body )
		je.setColumnLiteral( "lastOccurrence", "NOW()" )
		je.commit()
		self.Job.setVerifyError(je)
		self.Job.setStatus('deleted')
		cleanupJob(self.Job)
		notifySend( notifyList = notifyList, subject = subject, body = body )
		self.Job.commit()
		
	def checkNewJob(self,job):
		# make sure the job file exists
		print 'Checking new job %i, %s' % ( job.key(), job.name() )
		
		fx_drive_limits = {}
		for service in Service.c.Service.regexSearch('^FX_Limit')():
			fx_drive_limits[str(service.service()[-2]).lower()] = [False,service]
		
		pageFileUsageBytes = None
		fileName = str(job.fileName())
		if fileName and len(fileName):
			filePathExtra = ''
				
			if job.checkFileMd5():
				filePath = getLocalFilePath(job)
				filePathExtra = re.sub('(\.zip|\.max)','.txt',filePath)
			
				md5sum = execReturnStdout('md5sum ' + filePath)[:32].lower()
		
				if md5sum != str(job.fileMd5sum()).lower():
                                        host = job.host()
					subject = 'Corrupt jobfile from host ' + host.name()
					message = "The following host just uploaded a corrupt job file to the Assburner spool.\nHost: %s\nJob: %s\nJobID: %i\n\n" % (host.name(), job.name(), job.key())
					message += "Job file %s md5sum doesnt match the database local md5sum: %s db md5sum: %s" % (filePath, md5sum, job.fileMd5sum())
					self.Job.setStatus( 'corrupt' )
					self.recordFailure( subject, message )
					return False
			
				job.setFileSize( min(QFileInfo( filePath ).size(),2**31 - 251*1024*1024) )
		
			# automatically suspend job if assets use UNC paths or if there are 
			# any mapped drives used besides G:\ (ie, job uses assets from K: or S:)
			badpaths, badpaths2 = [], []
			if QFile.exists(filePathExtra):
				file = open(filePathExtra,'r')
				
				try:
					# BLUR SPECIFIC HACK
					# Get mappings for G, should be 0 or 1
					gMappings = job.jobType().jobTypeMappings().mappings().filter( "mount", QRegExp("g",Qt.CaseInsensitive) )
					# If we have a G mapping and it's a mirror(uses hostmapping instead of fkeyhost for multiple mirrors)
					usingGMirror = (gMappings.size() == 1 and not gMappings[0].host().isRecord())
					validGPathRegs = []
					if usingGMirror:
						validGPathRegs.append( QRegExp( "/_master/", Qt.CaseInsensitive ) )
						validGPathRegs.append( QRegExp( "/maps/", Qt.CaseInsensitive ) )
						validGPathRegs.append( QRegExp( "/Texture/" ) )
						validGPathRegs.append( QRegExp( "/(?:050_)maps/", Qt.CaseInsensitive ) )
						validGPathRegs.append( QRegExp( "/read/" ) )
				
									#rsync -W --numeric-ids -av --progress --stats --timeout=900 --delete 
									#--dry-run --include '*/' --exclude '*.[Pp][Ss][Dd]' --exclude 'temp/' 
									#--include '*.rps' --include '*_MASTER.max' --include '*.flw' --include 
									#'**/_MASTER/**' --include' **/_Master/**' --include 
									#'**/[Mm][Aa][Pp][Ss]/**' --include '**/050_maps/ **' --include 
									#'**/Texture/**' --include '**/read/*pc2' --include '**/read/*tmc' 
									#--include '**/read/*mdd' --include '**/Mesh*bin' --exclude '*' 
				except:
					print "Exception while setting up regexs for valid g maps mirror path tests"
					traceback.print_exc()
				
				for line in file:
					line = line.strip()
					if re.match('\#',line): continue
					if re.match('mem',line):
						try:
							parts = line.split(',')
							if parts[1] == 'max_PagefileUsage':
								# Strip the tailing 'L'
								pageFileUsageBytes = int(parts[2][:-1])
						except:
							print "Exception while processing mem entry in the max meta file"
							traceback.print_exc()
					if re.match('(map|pc|file)',line):
						try:
							# Format per line is
							# filetype,refcount,size,path
							# Because path can contain commas, we set maxsplit to 3, to avoid splitting path
							filetype, refcount, size, path = line.split(',',3)
							if re.match('(\\\\|\/\/)',path,re.I):
								badpaths.append(path)
							# Create a RE from the paths, in the format (G:|V:)
							mapDriveRe = '(%s)' % ('|'.join( [str(s) for s in config.permissableMapDrives.split(',') ] ) )
							if re.match('([a-z]\:)',path,re.I):
								if not re.match(mapDriveRe,path,re.I) and not re.match( 'c:[\\\\/]+max', path, re.I ) and not re.search('[\\\\/]forest pack pro[\\\\/]distmaps[\\\\/]',path,re.I):
									badpaths.append(path)
								mapDrive = path[0].lower()
								if mapDrive in fx_drive_limits:
									fx_drive_limits[mapDrive][0] = True
								
							# BLUR SPECIFIC HACK
							# If we are using a G: mirror, check if maps are in proper directories
							try:
								if usingGMirror and re.match('[gG]\:',path):
									valid = False
									for re_ in validGPathRegs:
										if len(path) and QString(path).contains( re_ ):
											valid = True
											break
									if not valid:
										badpaths2.append(path)
							except:
								print "Exception while testing for valid g mirror paths"
								traceback.print_exc()
						except:
							print "Invalid Line Format in Stats File:", line
			
			if badpaths2:
				subject = 'Job contains bad maps, pc2 or xref paths'
				print subject, job.key(), job.name()
				body = "Job: %s\n" % job.name()
				body += "Job ID: %i\n" % job.key()
				body += "User: %s\n" % job.user().displayName()
				body += "This job contains maps, pointcache or xrefs that\n"
				body += "point to locations that can not be reached by render nodes. Please\n"
				body += "make sure your maps are stored with the project on %s.\n"
				body += "Jobs that use maps via UNC paths such as \\\\thor or \\\\snake,"
				body += "or through drive letters other than %s will NOT render on the farm.\n\n"
				body += "List of invalid paths:\n"
				body = body % (config.permissableMapDrives, config.permissableMapDrives)
				for path in badpaths2:
					body += path + "\n"
					print path
				notifySend( notifyList = "newellm:j", subject = subject, body = body )
				
			if badpaths:
				subject = 'Job contains bad maps, pc2 or xref paths'
				print subject, job.key(), job.name()
				body = "Job: %s\n" % job.name()
				body += "Job ID: %i\n" % job.key()
				body += "User: %s\n" % job.user().displayName()
				body += "This job contains maps, pointcache or xrefs that\n"
				body += "point to locations that can not be reached by render nodes. Please\n"
				body += "make sure your maps are stored with the project on %s.\n"
				body += "Jobs that use maps via UNC paths such as \\\\thor or \\\\snake,"
				body += "or through drive letters other than %s will NOT render on the farm.\n\n"
				body += "List of invalid paths:\n"
				body = body % (config.permissableMapDrives, config.permissableMapDrives)
				for path in badpaths:
					body += path + "\n"
					print path
				
				notifyList = "it:e,%s:je" % (job.user().name())
				self.recordFailure( subject, body, notifyList )
				
				return False
		
		if job.table().schema().field( 'frameStart' ) and job.table().schema().field( 'frameEnd' ):
			minMaxFrames = Database.current().exec_('SELECT min(jobtask), max(jobtask), avg(jobtask) from JobTask WHERE fkeyjob=%i' % job.key())
				

			if minMaxFrames.next():
				midGuess = minMaxFrames.value(2).toDouble()[0]
				job.setValue( 'frameStart', minMaxFrames.value(0) )
				job.setValue( 'frameEnd', minMaxFrames.value(1) )
				job.setMinTaskNumber( minMaxFrames.value(0).toInt()[0] )
				job.setMaxTaskNumber( minMaxFrames.value(1).toInt()[0] )
		
				# Get the closest numbered task
				midFrame = Database.current().exec_('SELECT jobtask, abs(%g-jobtask) as distance from jobtask where fkeyjob=%i order by distance asc limit 1' % (midGuess, job.key()) )
				if midFrame.next():
					job.setMidTaskNumber( midFrame.value(0).toInt()[0] )
		
		# Copy JobType specific resolution information to the generic fields
		for fieldName in ('flag_w','resolutionX','width'):
			if job.table().schema().field( fieldName ):
				tmpWidth = job.getValue( fieldName ).toInt()[0]
				if tmpWidth and not job.frameWidth():
					job.setFrameWidth( tmpWidth )
					
		for fieldName in ('flag_h','resolutionY','height'):
			if job.table().schema().field( fieldName ):
				tmpHeight = job.getValue( fieldName ).toInt()[0]
				if tmpHeight and not job.frameHeight():
					job.setFrameHeight( tmpHeight )

		project = job.project()

		# Update jobstat's resolution information if we can get it from the job
		print "Updating JobStat"
		stat = job.jobStat()
		stat.setFrameWidth( job.frameWidth() )
		stat.setFrameHeight( job.frameHeight() )
		stat.setName( job.name() )
		stat.setElement( job.element() )
		stat.setProject( project )
		stat.setUser( job.user() )
		stat.setOutputPath( job.outputPath() )

		if project.isRecord():
			# Used by assfreezer for project filtering
			project.setLastJobSubmitted(QDate.currentDate()).commit()

			# See if we can find a corrosponding project resolution record
			if job.frameWidth() and job.frameHeight():
				prc = ProjectResolution.c
				prl = ProjectResolution.select( (prc.Height == job.frameHeight()) & (prc.Width == job.frameWidth()) & (prc.Project==project) )
				if prl.size() > 1:
					prl = prl.filter( prc.PrimaryOutput == True )
				if prl.size() == 1:
					stat.setProjectResolution( prl[0] )
		
		stat.commit()
		
		# In case there was no record before now
		job.setJobStat( stat )

		# Job update trigger will automatically set status to holding
		# if there are unfinished dependencies
		status = 'ready'
		if job.status() == 'verify-suspended':
			status = 'suspended'
		job.setStatus(status)
		
		services = job.jobServices().services()
		serviceNames = services.services()

		def isPowerOfTwo(n):
			return n and ((n-1)&n) == 0
		
		# Check for multi-os masks and include os-specific services
		# This allows simple submission setup where a single service can 
		# be passed and allows that service to be executed on multiple
		# operating systems, each with it's own service/software setup
		for service in services:
			if service.os() and not isPowerOfTwo(service.os):
				osServices = Service.select(Expression(service.service() + "%").like(Service.c.Service) & (Service.c.Os & service.os()))
				toCommit = JobServiceList()
				for osService in osServices:
					toCommit += JobService().setService(osService).setJob(job)
				toCommit.commit()

		################################################################
		##  BLUR SPECIFIC SERVICE HACKS
		################################################################
		
		# Per FX-Drive license counting
		for drive, (used, service) in fx_drive_limits.iteritems():
			if used and service not in services:
				JobService().setService(service).setJob(job).commit()

		# Dont apply project weighting for PCPreview or Fusion jobs
		if serviceNames.contains( 'PCPreview' ) or serviceNames.filter( QRegExp( '^Fusion' ) ).count():
			job.setApplyProjectWeight( False )

		# Hack for bug in max where long frame ranges cause memory corruption
		if job.jobType().name().startsWith('Max') and job.jobStatus().tasksCount() > 1000:
			job.setPacketType('continuous')
	
		################################################################
		##  END BLUR SPECIFIC SERVICE HACKS
		################################################################
			
		# Ensure that the job has at least one service
		if services.isEmpty():
			js = JobService()
			js.setService( Service( 'Assburner' ) )
			js.setJob( job )
			js.commit()
			
		# Use past renders to estimate resources
		if not job.outputPath().isEmpty():
			stats = (JobStat.c.OutputPath==job.outputPath()).orderBy(JobStat.c.Submitted,Expression.Descending).limit(1)()
			if stats.size() == 1 and stats[0].averageMemory():
				# Kilobytes
				job.setEstimatedMemory( stats[0].averageMemory() )
				job.setStats( job.stats() + ";memoryEstimateType=JobStat" )
				
			# Check to see if we need a writeable X: mapping
			if job.outputPath().startsWith( "X", Qt.CaseInsensitive ):
				JobMapping().setJob(job).setMapping(Mapping(13)).commit()
				
		if job.estimatedMemory() == 0 and pageFileUsageBytes is not None:
			# pageFileUsageBytes is the process size when the job was submitted. Estimated memory is in kilobytes
			job.setEstimatedMemory( int(pageFileUsageBytes / 1024.0) )
			job.setStats( job.stats() + ";memoryEstimateType=SubmitTimePageFileUsage" )
		job.commit()
		
		print "New Job %s is ready" % ( job.name() )
		return True

class JobCheckerThread(QThread):
	def __init__(self,job,db):
		QThread.__init__(self)
		self.db = db
		self.job = job
		import blur.Stone
		# Create a new connection to the database for our thread(thread local)
		self.Conn = Connection.createFromIni( blur.Stone.config(), "Database" )
		
	def run(self):
		self.db.setConnection( self.Conn )
		self.Conn.reconnect()
		# Share the same db with the rest of the process
		Database.setCurrent( self.db )
		checker = NewJobChecker(self.job)
		try:
			while self.job.status().startsWith('verify') and not checker.checkSubmissionFinished():
				QThread.sleep(1)
				self.job.reload()
			if self.job.status().startsWith('verify'):
				checker.checkNewJob(self.job)
		except:
			import traceback
			traceback.print_exc()
		Database.cleanupThread( True )
		
def getHostCount(job):
	q = Database.current().exec_("SELECT count(distinct(fkeyhost)) FROM JobAssignment WHERE fkeyjob=%i AND fkeyjobassignmentstatus BETWEEN 1 AND 3" % (job.key()) )
	if q.next():
		return q.value(0).toInt()[0]
	return 0

def getErrorCount(job):
	q = Database.current().exec_("SELECT sum(count) FROM JobError WHERE cleared=false AND fkeyJob=%i" % (job.key()) )
	if q.next():
		return q.value(0).toInt()[0]
	return 0

def getTimeoutCount(job):
	q = Database.current().exec_("SELECT count(*) FROM JobError WHERE timeout=true AND cleared=false AND fkeyjob=%i GROUP BY fkeyhost" % job.key() )
	if q.next():
		return q.size()
	return 0

# Returns a tuple with (loadTimeouts, taskTimeouts)
def getTimeoutCountByType(job):
	ret = {False:0,True:0}
	load, task = 0, 0
	q = Database.current().exec_( "SELECT count(*), hosthistory.fkeyjobtask is null from joberror inner join hosthistory on hosthistory.fkeyjob=%i AND fkeyjoberror=keyjoberror where joberror.fkeyjob=%i and timeout=true AND joberror.cleared=false group by hosthistory.fkeyjobtask is null;" % (job.key(),job.key()) )
	while q.next():
		ret[q.value(1).toBool()] = q.value(0).toInt()[0]
	return (ret[True],ret[False])

def getAvgTaskTime(job):
	doneAvg, doneCount = 0, 0
	q = Database.current().exec_("SELECT count(*), AVG( EXTRACT(epoch FROM (endedts - startedts)) )::int FROM JobTask WHERE fkeyjob=%i AND startedts IS NOT NULL and endedts IS NOT NULL AND endedts > startedts AND status='done'" % (job.key()))
	if q.next():
		doneCount = q.value(0).toInt()[0]
		doneAvg = q.value(1).toInt()[0]
	q = Database.current().exec_("SELECT count(*), AVG( EXTRACT(epoch FROM (now() - startedts)) )::int FROM JobTask WHERE fkeyjob=%i AND startedts IS NOT NULL and EXTRACT(epoch FROM (now() - startedts)) > %i AND status='busy'" % (job.key(), doneAvg) )
	if q.next():
		busyCount = q.value(0).toInt()[0]
		if( busyCount > 0 ):
			busyAvg = q.value(1).toInt()[0]
			totalCount = busyCount + doneCount
			return (doneAvg * doneCount + busyAvg * busyCount) / totalCount
	return doneAvg
	
# Returns a tuple, (int averageMemory, int maxMemory), both in Kb
def getAvgMaxMemory(job):
	q = Database.current().exec_("SELECT greatest(0, AVG( greatest(0, memory)) ), max(memory) FROM JobTask WHERE fkeyjob=%i AND coalesce(memory,0) > 0" % (job.key()))
	if q.next():
		return (int(q.value(0).toDouble()[0]), int(q.value(1).toInt()[0]))
	return (0,0)

# Returns a tuple, (max host tasks, execution time for busy task[Interval])
def getMaxHostTasks(job):
	q = Database.current().exec_("SELECT count(*), sum( CASE status WHEN 'busy' THEN now() - startedts ELSE '0'::interval END ) as busy_time FROM JobTask WHERE fkeyjob=%i and status in ('assigned','busy') group by fkeyhost order by count desc, busy_time asc limit 1" % job.key() )
	if q.next():
		return (q.value(0).toInt()[0], Interval.fromString(q.value(1).toString())[0])
	return (0,Interval())

def computeJobHealth(job):
	q = Database.current().exec_("SELECT * FROM job_compute_health(%i)" % job.key())
	if q.next():
		return q.value(0).toDouble()[0]
	return 0.0

def retrieveReapable():
	jobList = Job.select("status IN ('verify','verify-suspended','ready','started')")
	if not jobList.isEmpty():
		JobStatus.select("fkeyjob IN ("+jobList.keyString()+")")
	return jobList

def retrieveCleanable():
	return Job.select("status IN ('deleted','archived') AND (cleaned IS NULL OR cleaned=0)")

def run_loop(resetJobTaskCounts):
	# Remove checker from dict if the process has exited, so
	# we can recheck the job if it failed
	if config.assburnerForkJobVerify:
		try:
			for pid in newJobCheckers.keys():
				result = os.waitpid(pid,os.WNOHANG)
				if result and result[0] == pid:
					del newJobCheckers[pid]
					print 'Job Checker Child Process: %i Exited with code: %i' % result
		except:
			print "Exception During newJobCheckers waitpid checking."
			traceback.print_exc()

	elif config.assburnerThreadedJobVerify:
		for thread in newJobCheckers.keys():
			if thread.isFinished():
				del newJobCheckers[thread]
	
	config.update()
	if config.spoolDir != "":
		ensureSpoolSpace(config.requiredSpoolSpacePercent)

	userHosts, userJobs, userErrorsAvg, userTaskTimeAvg, userTaskTimeMax = {}, {}, {}, {}, {}
	jobStatus, projectHosts, projectJobs = {}, {}, {}
	activeTotal, taskTimeAvg, taskTimeMax = 0, 0, 0

	for job in retrieveReapable():
		if VERBOSE_DEBUG: print "Checking Job %i %s" % (job.key(), job.name())
		try:
			status = str(job.status())
			try:	jobStatus[status] += 1
			except:	jobStatus[status] = 1
			
			suspend = False
			suspendTitle = ''
			suspendMsg = ''
			
			if status.startswith('verify'):
				if config.assburnerForkJobVerify:
					if not job in newJobCheckers.values():
						if VERBOSE_DEBUG: print "Forking verify thread for %s" % (job.name())
						pid = os.fork()
						Database.current().connection().reconnect()
						if pid == 0:
							checkNewJob(job)
							os._exit(0)
						else:
							newJobCheckers[pid] = job
				elif config.assburnerThreadedJobVerify:
					if VERBOSE_DEBUG: print "Creating new thread for verifying job %s" % job.name()
					if not job in newJobCheckers.values():
						thread = JobCheckerThread(job,Database.current())
						newJobCheckers[thread] = job
						thread.start()
				else:
					checkNewJob(job)
				continue

			if resetJobTaskCounts:
				Database.current().exec_( "SELECT update_job_task_counts(%i)" % job.key() )
			
			js = job.jobStatus()
			unassigned  = js.tasksUnassigned()
			done        = js.tasksDone()
			tasks       = js.tasksCount()
			cancelled   = js.tasksCancelled()
			assigned    = js.tasksAssigned()
			busy 	    = js.tasksBusy()
			suspended 	= js.tasksSuspended()
			
			hostCount = getHostCount(job)
			js.setHostsOnJob(hostCount)
			if hostCount:
				activeTotal += 1

			avgTaskTime = getAvgTaskTime(job)
			js.setTasksAverageTime(avgTaskTime)

			errorCount = getErrorCount(job)
			js.setErrorCount(errorCount)

			# Suspend a job if it only has suspended tasks left
			if unassigned == 0 and busy == 0 and assigned == 0 and suspended > 0:
				suspend = True

			loadTimeouts, taskTimeouts = getTimeoutCountByType(job)
	#		timeoutCount = getTimeoutCount(job)

			# 5 or more task timeouts and at least 25% of the time
			if taskTimeouts >= 5 and taskTimeouts > done / 3.0:
				# Increase max task time automatically
				if done > 0 and avgTaskTime > .75 * job.maxTaskTime():
					job.setMaxTaskTime( int(avgTaskTime * 2.0) )
					# Clear current timeouts
					Database.current().exec_("UPDATE JobError SET cleared=true WHERE timeout=true AND fkeyjob=%i" % job.key())
				else:
					suspend = True
					suspendMsg = 'Job %s has been suspended.  The job has timed out during task execution %i hosts.' % (job.name(),taskTimeouts)
					suspendTitle = 'Job %s has been suspended(Task timeout limit reached).' % job.name()
			
			# 5 or more load timeouts and 
			if loadTimeouts >= 5:
				# at least 33% of the time
				if loadTimeouts > (busy + done) / 2.0:
					suspend = True
					suspendMsg = 'Job %s has been suspended.  The job has timed out while loading on %i hosts.' % (job.name(),loadTimeouts)
					suspendTitle = 'Job %s has been suspended(Load timeout limit reached).' % job.name()
			
			avgMemory, maxMemory = getAvgMaxMemory(job)
			js.setAverageMemory(avgMemory)
			
			if not js.memoryOverageNotified() and maxMemory > 64 * 1024 * 1024:
				maxTaskTime = job.maxTaskTime()
				maxTaskTime *= 5
				notifySend(job.user().name() + ":je",
					'Job %i %s is using over 64Gb, maxTaskTime automatically adjusted to %s' % (job.key(), job.name(), Interval(maxTaskTime).toString()),
					'Job exceeding available memory' )
				job.setMaxTaskTime(maxTaskTime)
				js.setMemoryOverageNotified(True)
				
			maxHostTasks, busyTime = getMaxHostTasks(job)
			js.setMaxHostTasks(maxHostTasks)
			
			js.setHealth(computeJobHealth(job))
			
			if unassigned == 0:
				js.setEstimatedCompletionTime( (Interval(avgTaskTime) * maxHostTasks - busyTime).adjust(QDateTime.currentDateTime()) )
			else:
				js.setEstimatedCompletionTime( QDateTime() )

			# If job is erroring with no success, suspend it
			if done == 0 and errorCount > config.totalFailureThreshold:
				suspend = True
				suspendMsg = 'Job %s has been suspended.  The job has produced %i errors with no tasks completing.' % (job.name(),errorCount)
				suspendTitle = 'Job %s suspended.' % job.name()
				
			if suspend:
				print "Suspending job %i %s " % (job.key(),job.name()), suspendMsg
				Job.updateJobStatuses( JobList(job), "suspended", False, True )
				if suspendMsg:
					notifyList = str(job.notifyOnError())
					# If they don't have notifications on for job errors, then
					# notify them via email and jabber
					if len(notifyList) == 0:
						notifyList = job.user().name() + ':je'
					# Notify me for now.
					# notifyList += ',newellm:j'
					notifySend(notifyList, suspendMsg, suspendTitle )
				continue
			
			user = job.user()
			try:	userJobs[user] += 1
			except:	userJobs[user] = 1
			
			try:	userHosts[user] += hostCount
			except:	userHosts[user] = hostCount
			
			try:	userErrorsAvg[user] += (errorCount - userErrorsAvg[user]) / userJobs[user]
			except: userErrorsAvg[user] = errorCount
			
			try:	userTaskTimeAvg[user] += (avgTaskTime - userTaskTimeAvg[user]) / userJobs[user]
			except: userTaskTimeAvg[user] = avgTaskTime
			
			taskTimeAvg += avgTaskTime
			
			fkeyProject = job.getValue("fkeyproject").toInt()[0]
			if fkeyProject > 0:
				try:	projectHosts[fkeyProject] += hostCount
				except: projectHosts[fkeyProject] = hostCount
				try: 	projectJobs[fkeyProject] += 1
				except: projectJobs[fkeyProject] = 1

			# send error notifications
			lastErrorCount = js.lastNotifiedErrorCount()
			
			# If some or all of the errors were cleared
			if errorCount < lastErrorCount:
				js.setLastNotifiedErrorCount( errorCount )
				
			# Notify about errors if needed
			if job.notifyOnError() and errorCount - lastErrorCount > config.errorStep:
				notifyOnErrorSend(job,errorCount)
				js.setLastNotifiedErrorCount( errorCount )
			
			# now we set the status started unless it's new
			if busy + assigned > 0:
				if job.status() == 'ready':
					job.setStatus('started')
				if job.startedts().isNull():
					job.setColumnLiteral( "startedts", "now()" )
			
			# done if no more tasks
			if done + cancelled >= tasks:
				job.setColumnLiteral("endedts","now()")
				if job.notifyOnComplete():
					notifyOnCompleteSend(job)
					
				if job.deleteOnComplete():
					if job.jobType().name() == 'Batch':
						job.setStatus( 'archived' )
					else:
						job.setStatus( 'deleted' )
				else:
					job.setStatus( 'done' )
				job.commit()
				
			job.commit()
			js.commit()
		except:
			global exceptionEmailSent
			if not exceptionEmailSent:
				exceptionText = traceback.format_exc()
				blur.email.send(sender = 'thePipe@blur.com', recipients = ["it@blur.com"], subject = "Unexpected exception in the reaper", body = exceptionText )
				exceptionEmailSent = True
		# End of job check loop

	try:
		if sendGraphiteStats:
			if VERBOSE_DEBUG: print "Writing Graphite stats"
			
			for user, jobCount in userJobs.iteritems():
				name = user.name()
				graphiteRecord( "users.%s.renderfarm.errors_average" % name, userErrorsAvg[user] )
				graphiteRecord( "users.%s.renderfarm.jobs_active" % name, jobCount )
				graphiteRecord( "users.%s.renderfarm.hosts" % name, userHosts[user] )
				graphiteRecord( "users.%s.renderfarm.task_time_average" % name, userTaskTimeAvg[user] )
		
			for projectKey, jobCount in projectJobs.iteritems():
				project = Project(projectKey)
				name = project.name()
				
				graphiteRecord( "projects.%s.renderfarm.jobCount" % name, jobCount )
				graphiteRecord( "projects.%s.renderfarm.hosts" % name, projectHosts[projectKey] )
				
			for status in ['started','ready','verify','verify-suspended']:
				if not status in jobStatus:
					jobStatus[status] = 0

			graphiteRecord( "renderfarm.jobs_active", activeTotal )
			graphiteRecord( "renderfarm.jobs_started", jobStatus['started'] )
			graphiteRecord( "renderfarm.jobs_ready", jobStatus['ready'] )
			graphiteRecord( "renderfarm.jobs_new", jobStatus['verify'] + jobStatus['verify-suspended'] )
			if activeTotal:
				graphiteRecord( "renderfarm.average_task_time", taskTimeAvg / activeTotal )
		
	except:
		print "Error recording graphite stats"
		traceback.print_exc()
		
	cleanupJobs()
	

def reaper():
	startup()
	print "Reaper is starting up"
	service = Service.ensureServiceExists('AB_Reaper')
	notifySend( notifyList = 'newellm:j', subject = 'Reaper Starting', body = "Reaper is (re)starting" )
	
	periodicJobTaskCountsReset = 0
	
	while True:
		resetJobTaskCounts = (periodicJobTaskCountsReset == 0)
		# Reset every 10 times through the loop, about 30 seconds
		periodicJobTaskCountsReset = (periodicJobTaskCountsReset + 1) % 10
		
		service.pulse()
		run_loop(resetJobTaskCounts)

		if VERBOSE_DEBUG: print "Sleeping for 3 seconds"
		time.sleep(3)

def notifyOnCompleteSend(job):
	if VERBOSE_DEBUG:
		print 'notifyOnCompleteSend(): Job %s is complete.' % (job.name())
	msg = 'Job %s is complete.' % (job.name())
	notifySend( notifyList = job.notifyOnComplete(), subject = msg, body = msg )

def notifyOnErrorSend(job,errorCount):
	if VERBOSE_DEBUG:
		print 'notifyOnErrorSend(): Job %s has errors.' % (job.name())
	msg = 'Job %s has %i errors.' % (job.name(),errorCount)
	notifySend( notifyList = job.notifyOnError(), body = msg, subject = msg, noEmail = True )

def notifySend( notifyList, body, subject, noEmail = False ):
	for notify in str(notifyList).split(','):
		try:  # Incorrectly formatted notify entries are skipped
			recipient, method = notify.split(':')
			if 'e' in method and not noEmail:
				try:
					blur.email.send(sender = 'thePipe@blur.com', recipients = [recipient], subject = subject, body = body )
				except:
					traceback.print_exc()
			if 'j' in method:
				sender = '%s@%s/%s' % (config.jabberSystemUser, config.jabberDomain, config.jabberSystemResource)
				recipient += '@' + config.jabberDomain
				if VERBOSE_DEBUG:
					print 'JABBER: %s %s %s %s\n' % (sender, config.jabberSystemPassword, recipient, body) 
				#blur.jabber.send(sender, str(config.jabberSystemPassword), str(recipient), str(body) )
		except:
			traceback.print_exc()

if __name__ == "__main__":
	reaper()
