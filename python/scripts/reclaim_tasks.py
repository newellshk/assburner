#!/usr/bin/python

from PyQt4.QtCore import *
from blur.Stone import *
from blur.Classes import *
import sys, os, re, traceback, time

if sys.argv.count('-daemonize'):
	from blur.daemonize import createDaemon
	createDaemon(pidFilePath='/var/run/ab_reclaim_tasks.pid')

# First Create a Qt Application
app = QCoreApplication(sys.argv)

# Load database config
if sys.platform=='win32':
	initConfig("c:\\blur\\resin\\resin.ini")
else:
	initConfig("/etc/reclaim_tasks.ini", "/var/log/ab/reclaim_tasks.log")
	# Read values from db.ini, but dont overwrite values from reclaim_tasks.ini
	# This allows db.ini defaults to work even if reclaim_tasks.ini is non-existent
	config().readFromFile( "/etc/db.ini", False )

initStone(sys.argv)
blurqt_loader()

blur.RedirectOutputToLog()

#Database.current().setEchoMode( Database.EchoUpdate | Database.EchoInsert | Database.EchoDelete )

class RTConfig:
	def __init__(self):
		pass
	def update(self):
		self.pulse_period = Config.getInt('assburnerPulsePeriod', 600) # default 10 minutes
		self.loop_time = int(Config.getInt('assburnerLoopTime') / 1000.0 + .999)
		
		# It can take up to assburnerPulsePeriod + assburnerLoopTime to update the pulse
		# If a host is swamped by a job and running low on memory pulses can become delayed,
		# allow an extra 10 minutes before reporting a host as no-pulse
		# Windows can be really bad about delivering a pulse when the host is swapping heavily
		# default value bumped to 10 minutes, can probably be set to 1 or 2 minutes if machines
		# are never swapping and/or you are running jobs on decent OS's
		self.pulse_period += self.loop_time + Config.getInt('assburnerReclaimTasksPulseGracePeriod', 600)
		
# Returns true if the host responds to a ping
def ping(hostName):
	pingcmd = "ping -c 3 %s 2>&1 > /dev/null" % hostName
	return os.system(pingcmd) == 0
	
# check for dead pulse/hung status
# 	any host that hasn't given pulse or changed status in 10 minutes, 
# 	reset host and reclaim job frames and send note to it@

total_hosts = 0

# Automatically setup the AB_reclaim_tasks service record
# the hostservice record for our host, and enable it
# if no other hosts are enabled for this service
service = Service.ensureServiceExists('AB_reclaim_tasks')
hostService = service.byHost(Host.currentHost(),True)
hostService.enableUnique()

config = RTConfig()
lastPingSweepTime = QDateTime.currentDateTime().addDays(-1)

def dbTime():
	q = Database.current().exec_("SELECT now();")
	if q.next():
		return q.value(0).toDateTime()
	LOG_5( "Database Time select failed" )
	return QDateTime.currentDateTime()

def reportClientUpdateError(host, status, assignments):
	for assignment in assignments:
		taskAssignments = assignment.jobTaskAssignments()
		tasks = taskAssignments.jobTasks()

		# Return the task
		tasks.setStatus( "new" )
		tasks.setColumnLiteral( "endedts", "now()" )
		tasks.commit()
		
		# Insert JobError record
		error = JobError()
		error.setHost(host)
		error.setJob(job)
		error.setFrames(','.join(tasks.frameNumbers()))
		error.setColumnLiteral( "lastOccurrence", "now()" )
		error.setMessage( "Client Update timed out.  Machine likely in limbo." )
		error.setCount(1)
		#error.setJobCommandHistory(jch)
		error.commit()

		errorStatus = JobAssignmentStatus("error")
		taskAssignments.setJobError(error)
		taskAssignments.setJobAssignmentStatus( errorStatus )
		taskAssignments.setColumnLiteral( JobTaskAssignment.c.Ended, "now()" )
		taskAssignments.commit()
		
		assignment = taskAssignment.jobAssignment()
		assignment.setJobError( error )
		assignment.setJobAssignmentStatus( errorStatus )
		assignment.setColumnLiteral( JobAssignment.c.Ended, "now()" )
		#assignment.setJobCommandHistory(jch)
		assignment.commit()
			
		task = tasks[0]

kickHostsRE = QRegExp('^eater|blade|bolo')

while True:
	hostService.pulse()
	service.reload()
	if service.enabled() and hostService.enabled():
		config.update()
		
		#waywardTasks = JobTask.select( "WHERE fkeyhost IS NOT NULL AND status IN ('assigned','busy') AND fkeyhost NOT IN (SELECT fkeyhost FROM hoststatus WHERE slavestatus IN ('assigned','copy','busy','client-update','client-update-offline','starting'))")
		
		#waywardTasksByJob = waywardTasks.groupedBy("fkeyjob")
		#for jobKey, tasks in waywardTasksByJob.iteritems():
			#job = Job(int(jobKey))
			#tasks = JobTaskList(tasks)
			#msg = "Returning %i tasks from job %s" % (tasks.size(),job.name())
			#for task in tasks:
				#msg += "\tKey %i, Task Number %i, Host %s" % (task.key(), task.frameNumber(), task.host().name())
			#print msg
			#Notification.create('reclaim_tasks','hung frames', ('Job %s has %i hung frames' % (job.name(), tasks.size())), msg)
			#tasks.setStatuses('new')
			#if job.packetType() != 'preassigned':
				#tasks.setHosts( Host() )
			#tasks.commit()
		
		selectStartTime = dbTime()
		q = Database.current().exec_("SELECT * FROM get_wayward_hosts_2('%s'::interval,'%s'::interval)" % (Interval(config.pulse_period).toString(), Interval(config.loop_time).toString()))
		while q.next():
			fkeyhost = q.value(0).toInt()[0]
			reason = q.value(1).toInt()[0] # 1 - Stale status, 2 - Stale pulse, 3 - Assigned to non-running job
			reason_text = {
				1: "failed to act on requested status",
				2: "was not pulsing",
				3: "is assigned to an invalid job" }

			host = Host(fkeyhost).reload()
			status = host.hostStatus().reload()
			assignments = host.activeAssignments()
			jobs = assignments.jobs()
			
			# Double check to see if the problem host is already up-to-date
			if reason in (1,3) and status.lastStatusChange() > selectStartTime:
				continue
			elif reason == 2 and status.slavePulse() > selectStartTime:
				continue
			
			# Special case client-updates that have timed out
			if reason==1 and status.slaveStatus().startsWith('client-update'):
				reportClientUpdateError(host, status, jobs)
			
			title = '%s %s' % (host.name(), reason_text[reason])
			
			lastPulseInterval = Interval(status.slavePulse(),selectStartTime)
			if reason == 3:
				nextStatus = 'starting'
			else:
				nextStatus = ping(host.name()) and 'no-pulse' or 'no-ping'
			
			# Dont notify when an offline host stops pulsing.  This is a nasty side affect of a windows
			# bug, where it doesn't properly shutdown applications running in their own logon session
			# Also we now ignore no-ping hosts since we use nagios to monitor the overall number of no-ping'ers
			# and this avoids lots of emails when a breaker trips, network outage, etc.
			if nextStatus != 'no-ping' and not (reason == 2 and status.slaveStatus() == 'offline' and nextStatus == 'no-pulse'):
				msg = QStringList()
				msg << title
				msg << ("Host: " + host.name())
				msg << ("Assburner Version:" + host.abVersion())
				msg << ("keyHost: " + str(host.key()))
				msg << ("Status: " + status.slaveStatus())
				msg << ("Time since status change: " + Interval(status.lastStatusChange(),selectStartTime).toString())
				msg << ("Time since pulse: " + lastPulseInterval.toString())

				for job in jobs:
					msg << ("Job: " + str(job.key()) + ", " + job.name())
					msg << ("Job Type: " + job.jobType().name())
					msg << ("Job Status: " + job.status())
					
				msg << ""
				msg << ("Setting status: " + nextStatus)
				
				n = Notification.create( 'reclaim_tasks', 'wayward host', '%s %s' % (host.name(), reason_text[reason]), msg.join("\n") )
			
			status.returnSlaveFrames(nextStatus)
			
			total_hosts += 1
		
		Log("total_hosts: %i" % total_hosts)

		for jobAssignment in JobAssignment.select( """
			(fkeyJobAssignmentStatus=1 AND created < now() - '5 minutes'::interval)
			OR
			(fkeyJobAssignmentStatus BETWEEN 1 AND 3 AND 
			(     fkeyJob NOT IN (SELECT keyJob FROM Job WHERE status IN ('ready','started'))
			  OR  fkeyHost NOT IN (SELECT fkeyHost FROM HostStatus WHERE slaveStatus IN ('assigned','busy')) )) """ )
			
			job = jobAssignment.job()
			host = jobAssignment.host()
			
			n = Notification.create( 'reclaim_tasks', 'stale assignment', '%s assigned to %s' % (job.name(), host.name()),
				'keyJobAssignment: %i Job Assignment Status: %s<br>' % (jobAssignment.key(), jobAssignment.jobAssignmentStatus().name()) +
				'keyJob: %i Job Name: %s Job Status: %s<br>' % (job.key(), job.name(), job.status()) +
				'keyHost: %i Host Name: %s Host Status: %s Host Ab Version: %s<br>' % (host.key(), host.name(), host.hostStatus().slaveStatus(), host.abVersion()) )
			
			Database.current().exec_( "SELECT cancel_job_assignment(%i)" % jobAssignment.key() )
	
		for hs in HostStatus.select( 
				  HostStatus.c.SlaveStatus.in_(['no-pulse','no-ping']) 
				& HostStatus.c.Host.in_(Host.c.Online==1)
				& HostStatus.c.Host.in_(HostService.c.Service.in_(['Assburner']) & (HostService.c.Enabled==True)) ):
			host = hs.host()
			hostName = hs.host().name()
			# print "Pinging host", hostName
			nextStatus = ping(hostName) and 'no-pulse' or 'no-ping'
			if kickHostsRE.indexIn(hostName) == 0:
				if nextStatus == 'no-pulse':
					print "Rebooting host", hostName
					host.reboot()
				else:
					print "Starting host", hostName
					host.start()
			else:
				# Ensure it's status hasn't already changed
				if nextStatus != hs.slaveStatus() and str(hs.reload().slaveStatus()) in ('no-pulse','no-ping',''):
					print "Updating %s slave status from %s to %s" % (hostName, hs.slaveStatus(), nextStatus)
					hs.setSlaveStatus(nextStatus).commit()
	

	# Run every 15 min
	time.sleep(60 * 10)

