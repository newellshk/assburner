#!/usr/bin/python

from PyQt4.QtCore import *
from PyQt4.QtSql import *
from blur.Stone import *
from blur.Classes import *
import blur.email, blur.jabber
import sys, time, re
from math import ceil
from blur.defaultdict import DefaultDict

try:
	import popen2
except: pass

if sys.argv.count('-daemonize'):
	from blur.daemonize import createDaemon
	createDaemon(pidFilePath='/var/run/ab_render_host_checker.pid')

app = QCoreApplication(sys.argv)

initConfig( "/etc/render_host_checker.ini", "/var/log/ab/render_host_checker.log" )
# Read values from db.ini, but dont overwrite values from render_host_checker.ini
# This allows db.ini defaults to work even if render_host_checker.ini is non-existent
config().readFromFile( "/etc/db.ini", False )

initStone(sys.argv)

blur.RedirectOutputToLog()

blurqt_loader()

VERBOSE_DEBUG = True

if VERBOSE_DEBUG:
	Database.current().setEchoMode( Database.EchoUpdate | Database.EchoDelete  | Database.EchoSelect )

Database.current().connection().reconnect()

class HostStat:
	def __init__(self):
		self.errorTime = 0
		self.successTime = 0
		self.errorCount = 0
		self.errorJobCount = 0
		self.successCount = 0
		self.successJobCount = 0

def get_error_string(jobError):
	dt = QDateTime()
	dt.setTime_t(jobError.errorTime())
	return unicode(dt.toString() + '\n' + jobError.message())

def find_fucked_hosts( interval ):
	statsByHostAndJobType = {}
	ret = []
	ret_errors = []
	q = Database.current().exec_(('SELECT hosthistory.fkeyhost, job.fkeyjobtype, count(distinct(hosthistory.fkeyjob)) as jobcount, extract(epoch from sum(duration)), count(*),' +
				" ((fkeyjobtask is not null and success is null) or (fkeyjobtask is null and hosthistory.status='busy' and nextstatus!='busy')) as error" +
				" FROM hosthistory INNER JOIN Job ON hosthistory.fkeyjob=Job.keyjob " +
				" WHERE duration IS NOT NULL AND nextstatus IS NOT NULL AND (success=true OR fkeyjoberror IS NOT NULL) AND hosthistory.status='busy' AND datetime > now() - '%s'::interval " +
				" GROUP BY hosthistory.fkeyhost, job.fkeyjobtype, error;") % interval.toString() )
	while q.next():
		fkeyhost = q.value(0).toInt()[0]
		fkeyjobtype = q.value(1).toInt()[0]
		jobCount = q.value(2).toInt()[0]
		duration = q.value(3).toInt()[0]
		count = q.value(4).toInt()[0]
		error = q.value(5).toBool()
		if not (fkeyhost,fkeyjobtype) in statsByHostAndJobType:
			statsByHostAndJobType[(fkeyhost,fkeyjobtype)] = HostStat()
		if error:
			statsByHostAndJobType[(fkeyhost,fkeyjobtype)].errorTime = duration
			statsByHostAndJobType[(fkeyhost,fkeyjobtype)].errorCount = count
			statsByHostAndJobType[(fkeyhost,fkeyjobtype)].errorJobCount = jobCount
		else:
			statsByHostAndJobType[(fkeyhost,fkeyjobtype)].successTime = duration
			statsByHostAndJobType[(fkeyhost,fkeyjobtype)].successCount = count
			statsByHostAndJobType[(fkeyhost,fkeyjobtype)].successJobCount = jobCount
	
	for hostJobTypeTuple, stats in statsByHostAndJobType.iteritems():
		(fkeyhost, fkeyjobtype) = hostJobTypeTuple
		errorTimeRatio = stats.errorTime / float(max(1,stats.successTime + stats.errorTime))
		errorCountRatio = stats.errorCount / float(max(1,stats.successCount + stats.errorCount))
		#keeps a couple large error times(could be exceeded max task minutes) from triggering false positives in most cases
		if (errorTimeRatio > .75 or errorCountRatio > .5) and (stats.errorCount > 1 or stats.errorTime > 60) and (stats.errorJobCount > 1):
			h = Host(fkeyhost)
			msg = "<tr><td>%s</td><td>%s</td><td>%i</td><td>%i</td><td>%i</td><td>%i</td><td>%i%%</td><td>%i%%</td></tr>" % (h.name(), JobType(fkeyjobtype).name(), stats.errorTime / 60, stats.errorCount, stats.successTime / 60, stats.successCount, int(errorTimeRatio*100), int(errorCountRatio*100))
			print msg
			print errorTimeRatio, errorCountRatio 
			ret.append(msg)
			errors = JobError.select("WHERE fkeyhost=%i AND fkeyjob IN (SELECT keyjob FROM job WHERE fkeyjobtype=%i) order by errortime desc limit 10" % (fkeyhost,fkeyjobtype))
			ret_errors.append( unicode(h.name() + '\n' + u'\n'.join([get_error_string(e) for e in errors])) )
	return (ret, ret_errors)

def runOnce(config):
	(fucked_hosts_messages, fucked_hosts_errors) = find_fucked_hosts( Interval.fromString('%i hours' % config.loopPeriod)[0] )
	
	if fucked_hosts_messages:
		msg = u"""
	Here are the crispy hosts for the last 12 hours
	A host is considered crispy if it's error time % is larger than 75%
	or it's error count % is larger than 50%
	
	<table border="1">
	<tr>
	<th>Host</th>
	<th>JobType</th>
	<th>Error Time</th>
	<th>Error Count</th>
	<th>Success Time</th>
	<th>Success Count</th>
	<th>Error Time %</th>
	<th>Error Count %</th>
	<tr>
	"""
		msg += u''.join(fucked_hosts_messages) + u"</table>\n"
		msg += u'<pre>\n\n' + '\n\n'.join(fucked_hosts_errors) + u"\n</pre>"
		Log(msg)
		Notification.create('render_host_checker','crispy host report', 'Crispy Hosts Report', msg )
		#blur.email.send( sender = 'thePipe@blur.com', ['it@blur.com'], 'Crispy Hosts Report', msg)
	else:
		Log('No crispy hosts at this time')

def hostHistoryIntervalAdjusted(hostHistory, dateStartTime, dateEndTime):
	start = hostHistory.dateTime()
	end = hostHistory.duration().adjust(start)
	if hostHistory.getValue('duration').isNull():
		end = dateEndTime
	elif end > dateEndTime:
		end = dateEndTime
	if start < dateStartTime:
		start = dateStartTime
	return Interval(start,end)

def aggregateDailyStats(date):
	dateStartTime = QDateTime(date)
	dateEndTime = dateStartTime.addDays(1)
	history = HostHistory.select( 'WHERE (duration IS NULL OR dateTime + duration > ?) and dateTime < ?', [QVariant(dateStartTime),QVariant(dateEndTime)] )
	historyByHost = history.groupedBy('fkeyhost')
	for hostKey, hostHistory in historyByHost.iteritems():
		byStatus = DefaultDict(Interval)
		host = Host(int(hostKey))
		complete = False
		taskErrors = 0
		loadErrors = 0
		tasksDone = 0
		taskErrorTime = Interval()
		loadErrorTime = Interval()
		loadTime = Interval()
		busyTime = Interval()
		for hh in hostHistory:
			adjInt = hostHistoryIntervalAdjusted(hh,dateStartTime,dateEndTime)
			byStatus[str(hh.status())] += adjInt
			hasTask = not hh.getValue('fkeyjobtask').isNull()
			hasError = not hh.getValue('fkeyjoberror').isNull()
			if hasTask and hh.success():
				tasksDone += 1
			if hasError:
				if hasTask:
					taskErrors += 1
					taskErrorTime += adjInt
				else:
					loadErrors += 1
					loadErrorTime += adjInt
			if hh.status() == "busy":
				if hasTask:
					busyTime += adjInt
				else:
					loadTime += adjInt
			complete = not hh.getValue("duration").isNull()
		hds = HostDailyStat()
		hds.setComplete(complete)
		hds.setHost(host)
		hds.setDate(date)
		hds.setTaskErrors(taskErrors)
		hds.setLoadErrors(loadErrors)
		hds.setBusyErrorTime(taskErrorTime)
		hds.setLoadErrorTime(loadErrorTime)
		hds.setReadyTime(byStatus["ready"])
		hds.setAssignedTime(byStatus["assigned"])
		hds.setCopyTime(byStatus["copy"])
		hds.setLoadTime(loadTime)
		hds.setBusyTime(busyTime)
		hds.setOfflineTime(byStatus["offline"] + byStatus["no-ping"] + byStatus["no-pulse"])
		hds.setTasksDone(tasksDone)
		hds.commit()
		
class RHCConfig:
	def __init__(self):
		self.lastDailyStatDate = None
		
	def update(self):
		self.loopPeriod = Config.getInt('renderHostCheckerLoopPeriodHours',12)
		if self.lastDailyStatDate is None:
			lastDailyStatDateString = Config.getString('renderHostCheckerLastDailyStatDate')
			if lastDailyStatDateString.size():
				self.lastDailyStatDate = QDate.fromString( lastDailyStatDateString )
			else:
				self.lastDailyStatDate = QDate()
	
	def setLastDailyStatDate(self,date):
		cfg = Config.recordByName( 'renderHostCheckerLastDailyStatDate' );
		cfg.setConfig( 'renderHostCheckerLastDailyStatDate' ).setValue( date.toString() ).commit()
		self.lastDailyStatDate = date
		
def main():
	config = RHCConfig()
	lastRun = QDateTime()
	service = Service.ensureServiceExists('AB_render_host_checker')
	hostService = service.byHost(Host.currentHost(),True)
	hostService.enableUnique()
	lastDailyStatDay = QDate()
	while True:
		service.reload()
		hostService.pulse()
		print config
		if service.enabled() and hostService.enabled():
			dt = QDateTime.currentDateTime()
			if lastRun.isNull() or lastRun.secsTo( QDateTime.currentDateTime() ) / (60 * 60) > config.loopPeriod:
				lastRun = QDateTime.currentDateTime()
				config.update()
				runOnce(config)
			# Aggregate daily stats for previous day at 5am, most long running tasks will be finished by then
			yesterday = dt.date().addDays(-1)
			if dt.time().hour() > 5 and not config.lastDailyStatDate.isValid() or config.lastDailyStatDate < yesterday:
				aggregateDailyStats( yesterday )
				config.setLastDailyStatDate( yesterday )
		
		time.sleep(60)

if __name__ == "__main__":
	main()

