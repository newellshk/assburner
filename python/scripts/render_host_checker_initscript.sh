#!/lib/init/init-d-script-python
### BEGIN INIT INFO
# Provides:          ab_render_host_checker
# Required-Start:    $remote_fs $syslog $network
# Required-Stop:     $remote_fs $syslog $network
# Default-Start:     2 3 4 5
# Default-Stop:
# Short-Description: Assburner render host checker daemon
# Description:       
#                    
### END INIT INFO
# -*- coding: utf-8 -*-
# Debian init.d script for assburner render host checker
# Copyright © 2014 Matt Newell <newellm@blur.com>

DAEMON=/usr/bin/render_host_checker.py
DAEMON_ARGS=-daemonize
NAME=ab_render_host_checker
DESC="assburner render host checker"

