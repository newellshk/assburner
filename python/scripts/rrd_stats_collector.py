#!/usr/bin/python

from PyQt4.QtCore import *
from PyQt4.QtSql import *
from blur.Stone import *
from blur.Classes import *
import blur.email, blur.jabber
import sys, time, re, os
from math import ceil
import traceback

try:
	import popen2
except: pass

if sys.argv.count('-daemonize'):
	from blur.daemonize import createDaemon
	createDaemon(pidFilePath='/var/run/rrd_stats_collector.pid')

app = QCoreApplication(sys.argv)

initConfig( "/etc/rrd_stats_collector.ini", "/var/log/ab/rrd_stats_collector.log" )
# Read values from db.ini, but dont overwrite values from rrd_stats_collector.ini
# This allows db.ini defaults to work even if rrd_stats_collector.ini is non-existent
config().readFromFile( "/etc/db.ini", False )

blur.RedirectOutputToLog()

blurqt_loader()

VERBOSE_DEBUG = False

if VERBOSE_DEBUG:
	Database.instance().setEchoMode( Database.EchoUpdate | Database.EchoDelete )# | Database.EchoSelect )

Database.current().connection().reconnect()

db = Database.current()

def dbTime():
	q = Database.current().exec_("SELECT now();")
	if q.next():
		return q.value(0).toDateTime()
	Log( "Database Time select failed" )
	return QDateTime.currentDateTime()

# Writes the stats, setting the timestamp to be now - howFarBackInterval
def writeStats( stats, howFarBackInterval ):
	total, non_idle, non_error = stats

	for category, val_dict in { 'total' : total, 'active' : non_idle, 'active_non_error' : non_error }.iteritems():
		for k in val_dict:
			graphiteRecord( "renderfarm.status_percentages.%s.%s" % (category,k), val_dict[k] * 100.0, (howFarBackInterval * -1).adjust(QDateTime.currentDateTime()) )

# Selects the stats for a certain duration of time, starting now - howFarBackInterval
def updateStats( durationInterval, howFarBackInterval ):
	print durationInterval.toString(), howFarBackInterval.toString()
	dateRange = "(now() - '%(howFarBack)s'::interval - '%(duration)s'::interval)::timestamp, (now() - '%(howFarBack)s'::interval)::timestamp" % \
		{'howFarBack' : str(howFarBackInterval.toString()), 'duration' : str(durationInterval.toString())}
	q = db.exec_( "SELECT * FROM hosthistory_status_percentages( 'hosthistory_timespan_duration_adjusted( %s, '''', '''' )' );" % dateRange.replace("'", "''") )

	total = {}
	non_idle = {}
	non_error = {}
	while q.next():
		status = q.value(0).toString()
		perc = q.value(4).toDouble()[0]
		non_idle_perc = q.value(5).toDouble()[0]
		non_error_perc = q.value(6).toDouble()[0]
		if status == 'busy':
			q2 = db.exec_("SELECT * FROM renderfarm_summary( %s );" % dateRange )
			q2_interval = lambda i: Interval.fromString(q2.value(i).toString())[0]
			total_time = q2_interval(5)
			load_time = q2_interval(2)
			load_error_time = q2_interval(3)
			cancelled_time = q2_interval(8)
			task_time = q2_interval(10)
			error_task_time = q2_interval(12)
			
			busy = task_time / total_time
			total['busy'] = busy * perc
			non_idle['busy'] = busy * non_idle_perc
			non_error['busy'] = busy * non_error_perc
			
			busy_loading = load_time / total_time
			total['busy_loading'] = busy_loading * perc
			non_idle['busy_loading'] = busy_loading * non_idle_perc
			non_error['busy_loading'] = busy_loading * non_error_perc
			
			busy_loading_errored = load_error_time / total_time
			total['busy_loading_errored'] = busy_loading_errored * perc
			non_idle['busy_loading_errored'] = busy_loading_errored * non_idle_perc
			
			busy_errored = error_task_time / total_time
			total['busy_errored'] = busy_errored * perc
			non_idle['busy_errored'] = busy_errored * non_idle_perc
			
			cancelled = cancelled_time / total_time
			total['cancelled'] = cancelled * perc
			non_idle['cancelled'] = cancelled * non_idle_perc
		else:
			loading = q.value(1).toBool()
			errored = q.value(2).toBool()
			if loading:
				status += '_loading'
			if errored:
				status += '_errored'
			if not q.value(4).isNull():
				total[status] = perc
			if not q.value(5).isNull():
				non_idle[status] = non_idle_perc
			if not q.value(6).isNull():
				non_error[status] = non_error_perc
	return (total,non_idle,non_error)

def collectActiveJobTypeCounts():
	ret = {}
	q = db.exec_( "select jobtype, count(*) from job inner join jobtype on jobtype.keyjobtype=job.fkeyjobtype inner join jobstatus on jobstatus.fkeyjob=job.keyjob where jobstatus.hostsonjob > 0 and job.status in ('ready','started','coasting') group by jobtype" )
	
	while q.next():
		ret[q.value(0).toString()] = q.value(1).toInt()[0]
	return ret

def writeActiveJobTypeCounts(activeJobTypeCounts):
	for jobType, activeCount in activeJobTypeCounts.iteritems():
		if activeCount > 0:
			graphiteRecord( "renderfarm.jobtype_usage_counts.%s" % jobType.replace('.',','), activeCount )

def collectActiveServiceCounts():
	ret = {}
	q = db.exec_( "select service, sum(jobstatus.hostsonjob) from job inner join jobservice on jobservice.fkeyjob=job.keyjob inner join service on jobservice.fkeyservice=service.keyservice inner join jobstatus on jobstatus.fkeyjob=job.keyjob where jobstatus.hostsonjob > 0 and job.status in ('ready','started','coasting') group by service" )
	
	while q.next():
		ret[q.value(0).toString()] = q.value(1).toInt()[0]
	return ret
	
def writeActiveServiceCounts(activeServiceCounts):
	for serviceName, activeCount in activeServiceCounts.iteritems():
		if activeCount > 0:
			graphiteRecord( "renderfarm.service_usage_counts.%s" % serviceName.replace('.',','), activeCount )

def collectHostStatusCounts():
	ret = {}
	q = db.exec_( "select slavestatus, count(*) from hoststatus group by slavestatus" )
	
	while q.next():
		ret[q.value(0).toString()] = q.value(1).toInt()[0]
	return ret

def writeHostStatusCounts(hostStatusCounts):
	for status, count in hostStatusCounts.iteritems():
		graphiteRecord( "renderfarm.host_status_counts.%s" % status, count )

def main():
	dt = None
	statsByPriorInterval = {}
	# We write out the stats multiple times to account for the fact that we cannot tell if a 'busy' status is successful
	# or errored until after it is complete.  So we write out stats right away, again after 2 hours, and again after a day
	duration = Interval.fromString('5 minutes')[0]
	priorIntervals = (Interval.fromString('0 seconds')[0], Interval.fromString('2 hours')[0], Interval.fromString('24 hours')[0])

	# Service : Number of hosts with a job that requires the service
	activeServiceCounts = {}
	activeJobTypeCounts = {}
	hostStatusCounts = {}
	
	while True:
		# Collect hosthistory status percentage stats every 5 minutes
		if dt is None or Interval(dt,QDateTime.currentDateTime()) > duration:
			skew = Interval(dbTime(),QDateTime.currentDateTime())
			dt = QDateTime.currentDateTime()
			for pi in priorIntervals:
				statsByPriorInterval[pi] = updateStats( duration, pi )
			
			activeServiceCounts = collectActiveServiceCounts()
			activeJobTypeCounts = collectActiveJobTypeCounts()
			hostStatusCounts = collectHostStatusCounts()
			
		# Write the stats every 15 seconds
		for pi, stats in statsByPriorInterval.iteritems():
			writeStats( stats, pi + skew )
		
		writeActiveServiceCounts(activeServiceCounts)
		writeActiveJobTypeCounts(activeJobTypeCounts)
		writeHostStatusCounts(hostStatusCounts)
		
		time.sleep( 15 )

if __name__ == "__main__":
	main()
