"""Imports all necessary Trax DB records to Shotgun DB.

This script will read the Trax DB and create or update the equivalent
record in Shotgun.

So far, this importer supports:

[Trax object] -> [Shotgun object]
=================================
Employee      -> HumanUser
Project       -> Project
Asset         -> Asset
Shot          -> Shot
ShotGroup     -> Sequence
Delivery      -> Task
Department    -> Department
Department    -> PipelineStep (created manually, linked via script, though)
Note          -> Note


Planned to support:

[Trax object] -> [Shotgun object]
=================================
AssetType     -> AssetType (custom entity)
ElementDep    -> Task.dependencies





"""

import os
import logging
import pprint
from HTMLParser import HTMLParser

import blurdev
import trax.api
from shotgun_api3 import Shotgun
from shotgun_api3.shotgun import Fault


base_url = 'https://blur.shotgunstudio.com'
script_name = 'migration_importer'
api_key = '67cddb6c62f904a3420f77706ffab425be592ba6'
api_ver = '1.0'
sg = Shotgun(base_url, script_name, api_key)

# If this is set to True, the script will attempt to update existing
# records in shotgun.  If set to False, it will only create new records.
_UPDATE = False


logging.basicConfig(datefmt='%I:%M:%S', format='[%(asctime)s] %(message)s', level=logging.DEBUG)


class JunkParser(HTMLParser):
	"""
	Parses the junk html that Qt generates as part of its RichTextWidget
	that has found its way into trax.  Exports plain text.
	
	"""

	recording = False
	data = []

	def handle_starttag(self, tag, attributes):
		if tag == 'p':
			self.recording = True

	def handle_data(self, data):
		if self.recording:
			self.data.append(data)

	def handle_endtag(self, tag):
		if tag == 'p':
			self.recording = False

	def plain_text(self):
		return '\n'.join(self.data)




def get_trax_projects():
	projectstatus = trax.api.data.ProjectStatus.recordByName('Production')
	return trax.api.projects("fkeyprojectstatus = %s" % projectstatus.key())

def get_project_assets(project):
	return trax.api.data.Asset.select("fkeyproject = %s" % project.key())

def get_project_shots(project):
	return trax.api.data.Shot.select("fkeyproject = %s" % project.key())

def get_project_deliveries(project):
	return trax.api.data.Delivery.select("fkeyproject = %s" % project.key())

def get_project_notes(project):
	return trax.api.data.Note.select("fkeyproject = %s" % project.key())




# Generic Create or Update
def create_or_update(trax_obj, sg_cache, entity_type, filter_list, update_dict, field_list=None, image=None):
	# Checks the local cache first to see if this record has already been
	# created or updated.
	sg_obj = sg_cache.get(trax_obj, None)
	if sg_obj is not None:
		return sg_obj
	logging.debug('Creating %s - %s' % (entity_type, trax_obj.displayName()))

	# The full fieldlist isn't required, since none of the current functions
	# are actually using any of the object data besides id.  Less fields in
	# the fieldlist means the query will execute faster.
	if field_list is None:
		field_list = update_dict.keys()
	t_field_list = ['id']
	if 'image' in field_list:
		t_field_list.append('image')

	# Look up the object to see if it exists, and create it or update it
	# accordingly
	sg_obj = sg.find_one(entity_type, filter_list, t_field_list)
	sg_obj_created = False
	if not sg_obj:
		sg_obj_created = True
		try:
			sg_obj = sg.create(entity_type, update_dict)
		except Fault:
			logging.debug('Fault--------------------------')
			return None
	else:
		if _UPDATE:
			sg_obj_updated = sg.update(entity_type, sg_obj['id'], update_dict)
			# The create and update commands only return the fields that were
			# created or updated, and so they won't include the image field, which
			# we need, so we need to update the original lookup dict
			sg_obj.update(sg_obj_updated)

	# Add the object to the cache so it can be accessed again without having
	# to do another lookup.
	sg_cache[trax_obj] = sg_obj

	# If the object was created/updated and doesn't currently have an image
	# thumbnail, and trax has provided an image thumbnail filepath, upload 
	# that image to shotgun for this object.
	if sg_obj_created and sg_obj and not sg_obj.get('image', None) and image:
		image = os.path.normpath(image)
		if os.path.isfile(image):
			sg.upload_thumbnail(entity_type, sg_obj['id'], image)

			#TODO: update the sg_obj dict with the new uploaded image path.
			# Nothing is using this information right now, so it doesn't
			# matter.			
	return sg_obj


# Pipeline Step
_sg_step_cache = {}
def create_or_update_Step(department, parent):
	# We don't actually create pipeline steps programatically, they are
	# already created in shotgun, we just map them here.
	if isinstance(parent, (trax.api.data.Shot, trax.api.data.ShotAsset)):
		pipeline = 'Shot'
	elif isinstance(parent, trax.api.data.Asset):
		pipeline = 'Asset'
	else:
		return None
	d = {
		'Pre-Production': None,
		'Concept Design': 'Art',
		'Motion Capture': 'Mocap',
		'Layout': 'Layout',
		'Character Modeling': 'Model',
		'Rigging': 'Rigging',
		'Facial Rigging': 'Rigging',
		'Animation': 'Animation',
		'Simulation': 'FX',
		'Cloth': 'Cloth',
		'Hair': 'Hair',
		'Modeling': 'Modeling',
		'Scene Assembly': 'Scene Assembly',
		'FX': 'FX',
		'Audio Production': 'Audio',
		'Pipeline': None,
		'Motion Graphics': 'Art',
		'Production': None,
		'IT': None,
		'Human Resources': None,
		'Crowd': 'Crowd',
	}
	step_name = d[str(department.name())]
	if step_name is None:
		return None
	key = (pipeline, step_name)
	sg_obj = _sg_step_cache.get(key, None)
	if sg_obj is not None:
		return sg_obj

	filter_list = [['code', 'is', step_name], ['entity_type', 'is', pipeline]]
	sg_obj = sg.find_one('Step', filter_list, ['id'])
	_sg_step_cache[key] = sg_obj
	return sg_obj


# Notes
_sg_note_cache = {}
def create_or_update_Note(note):
#	logging.debug('NOTE: %s, %s, %s, %s' % (note.project().isRecord(), note.parent().isRecord(), type(note.parent()), type(note.parent().parent())))
	if not note.project().isRecord():
		return None
	if not note.element().isRecord():
		return None

	# Shotgun links to both the Entity, and the Task
	parent = note.element()
	task = None
	if isinstance(parent, trax.api.data.Delivery):
		task = parent
		parent = parent.parent()

	if isinstance(parent, trax.api.data.Asset):
		sg_parent = create_or_update_Asset(parent)
	elif isinstance(parent, trax.api.data.Shot):
		sg_parent = create_or_update_Shot(parent)
	elif isinstance(parent, trax.api.data.ShotGroup):
		sg_parent = create_or_update_Sequence(parent)
	else:
		return None

	if not sg_parent:
		return None



	try:
		content = str(note.text())
		subject = str(note.subject())
		if '<html>' in content:
			jp = JunkParser()
			jp.feed(content)
			content = jp.plain_text()
		if '<html>' in subject:
			jp = JunkParser()
			jp.feed(subject)
			subject = jp.plain_text()
	except Exception:
		return None

	filter_list = [['project', 'is', create_or_update_Project(note.project())],
				   ['note_links', 'is', sg_parent],
				   ['content', 'is', content]
				  ]


	if note.closed() or note.studioClosed():
		sg_status = 'clsd'
	else:
		sg_status = 'opn'

	update_dict = {
		'project': create_or_update_Project(note.project()),
		'note_links': [sg_parent],
		'subject': subject,
		'content': content,
		'sg_status_list': sg_status,
	}

	if task:
		update_dict['tasks'] = [create_or_update_Task(task)]

	emp = trax.api.data.Employee(note.assignedBy().key())
	if emp.isRecord():
		update_dict['user'] = create_or_update_HumanUser(emp)

	emp = trax.api.data.Employee(note.assignedTo().key())
	if emp.isRecord():
		update_dict['addressings_to'] = [create_or_update_HumanUser(emp)]

	field_list = update_dict.keys()
	return create_or_update(note, _sg_note_cache, "Note", filter_list, update_dict, field_list)

# Shots
_sg_shot_cache = {}
def create_or_update_Shot(shot):
	# It's very helpful in shotgun if the shot names are unique per show.
	# Instead of S0001.00, I've included the Sequence name so it's unique,
	# Sc001_S0001.00
	shot_name = '%s_%s' % (str(shot.shotGroup().name()), str(shot.name()))
	filter_list = [['code', 'is', shot_name],
				   ['project', 'is', create_or_update_Project(shot.project())],
				   ['sg_sequence', 'is', create_or_update_Sequence(shot.shotGroup())]]
	update_dict = {
		'project': create_or_update_Project(shot.project()),
		'code': shot_name,
		'sg_sequence': create_or_update_Sequence(shot.shotGroup()),
	}
	if str(shot.assetType().name()) == 'LayoutShot':
		update_dict['sg_shot_type'] = 'Layout'
	else:
		update_dict['sg_shot_type'] = 'Production'

	field_list = update_dict.keys() + ['image']
	image = None
	ls = shot.latestSample()
	if ls.isRecord():
		image = str(ls.absoluteFilePath())
	return create_or_update(shot, _sg_shot_cache, "Shot", filter_list, update_dict, field_list, image)

# ShotGroup
_sg_sequence_cache = {}
def create_or_update_Sequence(shotgroup):
	filter_list = [['code', 'is', str(shotgroup.name())],
				   ['project', 'is', create_or_update_Project(shotgroup.project())]]
	update_dict = {
		'project': create_or_update_Project(shotgroup.project()),
		'code': str(shotgroup.name()),
	}
	field_list = update_dict.keys() + ['image']
	return create_or_update(shotgroup, _sg_sequence_cache, "Sequence", filter_list, update_dict, field_list)

# Assets
_sg_asset_cache = {}
def create_or_update_Asset(asset):
	filter_list = [['code', 'is', str(asset.name())],
				   ['project', 'is', create_or_update_Project(asset.project())]]
	update_dict = {
		'project': create_or_update_Project(asset.project()),
		'code': str(asset.name()),
	}
	if str(asset.assetType().name()) in ['Character', 'Prop', 'Environment', 'Vehicle']:
		update_dict['sg_asset_type'] = str(asset.assetType().name())
	field_list = update_dict.keys() + ['image']
	image = None
	ls = asset.latestSample()
	if ls.isRecord():
		image = str(ls.absoluteFilePath())
	return create_or_update(asset, _sg_asset_cache, "Asset", filter_list, update_dict, field_list, image)

# Projects
_sg_project_cache = {}
def create_or_update_Project(project):
	filter_list = [['name', 'is', str(project.name())]]
	update_dict = {
		'name': str(project.name()),
		'sg_status': 'Active',
		'sg_type': 'Game Cinematic'
	}
	field_list = update_dict.keys() + ['image']
	image = None
	ls = project.latestSample()
	if ls.isRecord():
		image = str(ls.absoluteFilePath())
	return create_or_update(project, _sg_project_cache, "Project", filter_list, update_dict, field_list, image)

# HumanUsers
_sg_humanuser_cache = {}
def create_or_update_HumanUser(employee):
	filter_list = [['login', 'is', str(employee.username())]]
	update_dict = {
		'firstname': str(employee.firstName()),
		'lastname': str(employee.lastName()),
		'login': str(employee.username()),
		'email': str(employee.email()),
	}
	field_list = update_dict.keys() + ['image']
	if employee.primaryRole().department().isRecord():
		update_dict['department'] = create_or_update_Department(employee.primaryRole().department())
	image = None
	if employee.iconFile():
		image = str(employee.iconFile())
	return create_or_update(employee, _sg_humanuser_cache, "HumanUser", filter_list, update_dict, field_list, image)

# Departments
_sg_department_cache = {}
def create_or_update_Department(department):
	filter_list = [['name', 'is', str(department.name())]]
	update_dict = {
		'name': str(department.name()),
		'code': str(department.shortName()),
	}
	return create_or_update(department, _sg_department_cache, "Department", filter_list, update_dict)

# Tasks
_sg_task_cache = {}
def create_or_update_Task(delivery):
	parent = delivery.parent()
	if not parent.isRecord():
		return None
	if isinstance(parent, trax.api.data.Shot):
		sg_parent = create_or_update_Shot(parent)
	elif isinstance(parent, trax.api.data.Asset):
		sg_parent = create_or_update_Asset(parent)
	else:
		return None
	filter_list = [['content', 'is', str(delivery.name())],
				   ['project', 'is', create_or_update_Project(delivery.project())],
				   ['entity', 'is', sg_parent]]
	update_dict = {
		'content': str(delivery.name()),
		'project': create_or_update_Project(delivery.project()),
		'entity': sg_parent,
	}
	# dict to translate from trax delivery statuses to shotgun task statuses.
	status = str(delivery.deliveryStatus().name())
	status_d = {
		'New': 'wtg',
		'Assigned': 'wtg',
		'Scheduled': 'wtg',
		'Waiting Approval': 'hld',
		'Passed QC': 'hld',
		'Internal Approved': 'fin',
		'Waiting on Client': 'hld',
		'Client Approved': 'fin',
		'Revising with Notes': 'hld',
		'Client Rejected': 'hld',
		'On Hold': 'hld',
		'In Progress': 'ip',
		'Ready to Begin': 'rdy',
		'Dept Sup Approved': 'fin',
		'CG Sup Approved': 'fin',
		'Director Approved': 'fin',
		'Client App With Notes': 'hld',
		'Qc in Progress': 'hld',
		'Passed Art QC': 'hld',
		'Submit to QC': 'hld',
		'Finaled': 'fin',
		'Waiting on SA': 'hld',
		'Rendering': 'hld',
	}
	sg_status = status_d.get(status, 'wtg')
	if delivery.final():
		sg_status = 'fin'
	update_dict['sg_status_list'] = sg_status
	if delivery.dateStart().isValid():
		update_dict['start_date'] = delivery.dateStart().toPyDate()
	if delivery.dateComplete().isValid():
		update_dict['due_date'] = delivery.dateComplete().toPyDate()
	dept = delivery.deliveryType().department()
	if dept.isRecord():
		step = create_or_update_Step(dept, delivery.parent())
		if step:
			update_dict['step'] = step

	field_list = update_dict.keys()
	return create_or_update(delivery, _sg_task_cache, "Task", filter_list, update_dict, field_list)



def create_or_update_Departments(departments):
	sg_depts = []
	for dept in departments:
		sg_depts.append(create_or_update_Department(dept))
	return sg_depts

def create_or_update_HumanUsers(employees):
	sg_emps = []
	for employee in employees:
		sg_emps.append(create_or_update_HumanUser(employee))
	return sg_emps

def create_or_update_Projects(projects):
	sg_objs = []
	for project in projects:
		sg_objs.append(create_or_update_Project(project))
	return sg_objs

def create_or_update_Assets(assets):
	sg_objs = []
	for asset in assets:
		sg_objs.append(create_or_update_Asset(asset))
	return sg_objs

def create_or_update_Shots(shots):
	sg_objs = []
	for shot in shots:
		sg_objs.append(create_or_update_Shot(shot))
	return sg_objs

def create_or_update_Tasks(tasks):
	sg_objs = []
	for task in tasks:
		sg_objs.append(create_or_update_Task(task))
	return sg_objs

def create_or_update_Notes(notes):
	sg_objs = []
	for note in notes:
		sg_objs.append(create_or_update_Note(note))
	return sg_objs

def import_all():
#	employees = trax.api.activeEmployees(stub_=False)
#	create_or_update_HumanUsers(employees)


#	project = trax.api.findProject('project_TEST')
#	projects = [project]
	projects = get_trax_projects()
	for project in projects:
#		create_or_update_Project(project)

#		assets = get_project_assets(project)
#		create_or_update_Assets(assets)

#		shots = get_project_shots(project)
#		create_or_update_Shots(shots)

#		deliveries = get_project_deliveries(project)
#		create_or_update_Tasks(deliveries)

		notes = get_project_notes(project)
#		logging.debug("LEN %s" % len(notes))
		create_or_update_Notes(notes)


def main():
	import_all()


if __name__ == '__main__':
	main()
