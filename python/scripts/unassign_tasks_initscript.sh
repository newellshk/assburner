#!/lib/init/init-d-script-python
### BEGIN INIT INFO
# Provides:          ab_manager
# Required-Start:    $remote_fs $syslog $network
# Required-Stop:     $remote_fs $syslog $network
# Default-Start:     2 3 4 5
# Default-Stop:
# Short-Description: Assburner manager daemon
# Description:       
#                    
### END INIT INFO
# -*- coding: utf-8 -*-
# Debian init.d script for assburner manager
# Copyright © 2014 Matt Newell <newellm@blur.com>

DAEMON=/usr/bin/manager.py
DAEMON_ARGS=
NAME=ab_manager
DESC="assburner manager"

