#!/bin/sh

# TODO 
#  make this script not so repetitive/gay
#  only rebuild sub-rpms where SVNREV in particular
#   subdirectory has changed since last run
#  test for Qt4 RPMs being installed
#  make sure that qmake is in $PATH
#  etc, etc -- lots of room for improvement

WORKINGCOPYDIR="$1"

if [ -f "$WORKINGCOPYDIR/rpm_build.sh" ]; then
	echo "cool, $WORKINGCOPYDIR appears to be a valid svn checkout of blur repo"
else 
	echo "usage: $0 [path to working copy directory aka blur IT svn co]"
	exit -1
fi

ARCH=`uname -i`
if [ $ARCH == "x86_64" ] 
then 
	export QTDIR="/usr/lib64/qt4"
else 
	export QTDIR="/usr/lib/qt4"
fi

echo "removing any old blur RPMs"
for rpm in "notifier manager reaper unassign_tasks energy_saver" blur-perl stoneperlqt pyclasses pystone "libclasses-devel libclasses" "libstone libstone-devel" PyQt4 sip blur-python 
do
	echo "rpm -e $rpm"
	rpm -e $rpm
done

echo "svn update to the latest revision of the repository.."
cd $WORKINGCOPYDIR
svn up
if [ $? != 0 ] 
then
	echo "subversion update failed! (return code $?)"
	exit -1
fi

echo "running $WORKINGCOPYDIR/rpm_build.sh"
$WORKINGCOPYDIR/rpm_build.sh
if [ $? != 0 ]
then
	echo "$WORKINGCOPYDIR/rpm_build.sh failed! (return code $?)"
	exit -1
fi

echo "grabbing BUILDVERSION from svnversion"
BUILDVERSION=`svnversion $WORKINGCOPYDIR`

echo "attempting to build RPMs from blur codebase revision $BUILDVERSION"

echo "building blur-python (rpm_build_blur-python.sh)"
../rpm_build_blur-python.sh
RETVAL=$?
if [ $RETVAL != 0 ]
then 
	echo "rpm_build_blur-python.sh failed! (return code $RETVAL)"
	exit -1
else 
	rpm -Uvh /usr/src/redhat/RPMS/$ARCH/blur-python-1.0-$BUILDVERSION.$ARCH.rpm --nomd5
	RETVAL=$?
	if [ $RETVAL != 0 ]
	then
		echo "rpm install of blur-python-1.0-$BUILDVERSION.$ARCH.rpm failed! (return value $RETVAL)"
		exit -1
	fi
fi

echo "building sip (rpm_build_sip.sh)"
../rpm_build_sip.sh
RETVAL=$?
if [ $RETVAL != 0 ]
then 
	echo "rpm_build_sip.sh failed! (return code $RETVAL)"
	exit -1
else 
	rpm -Uvh /usr/src/redhat/RPMS/$ARCH/sip-1.0-$BUILDVERSION.$ARCH.rpm --nomd5
	RETVAL=$?
	if [ $RETVAL != 0 ]
	then
		echo "rpm install of sip-1.0-$BUILDVERSION.$ARCH.rpm failed! (return value $RETVAL)"
		exit -1
	fi
fi

echo "building PyQT (rpm_build_pyqt.sh)"
../rpm_build_pyqt.sh
RETVAL=$?
if [ $RETVAL != 0 ]
then 
	echo "rpm_build_pyqt.sh failed! (return code $RETVAL)"
	exit -1
else 
	rpm -Uvh /usr/src/redhat/RPMS/$ARCH/PyQt4-1.0-$BUILDVERSION.$ARCH.rpm --nomd5
	RETVAL=$?
	if [ $RETVAL != 0 ]
	then
		echo "rpm install of PyQt4-1.0-$BUILDVERSION.$ARCH.rpm failed! (return value $RETVAL)"
		exit -1
	fi
fi

echo "building libstone, libstone-devel (rpm_build_stone.sh)"
../rpm_build_stone.sh
RETVAL=$?
if [ $RETVAL != 0 ]
then
	echo "rpm_build_stone.sh failed! (return code $RETVAL)"
	exit -1
else
	rpm -Uvh /usr/src/redhat/RPMS/$ARCH/libstone-1.0-$BUILDVERSION.$ARCH.rpm /usr/src/redhat/RPMS/$ARCH/libstone-devel-1.0-$BUILDVERSION.$ARCH.rpm
	RETVAL=$?
	if [ $RETVAL != 0 ]
	then
		echo "rpm install of libstone/libstone-devel-1.0-$BUILDVERSION.$ARCH.rpm failed! (return value $RETVAL)"
		exit -1
	fi
	
fi

echo "building libclass, libclasses-devel (rpm_build_classes.sh)"
../rpm_build_classes.sh
RETVAL=$?
if [ $RETVAL != 0 ]
then
	echo "rpm_build_classes.sh failed! (return code $RETVAL)"
	exit -1
else
	rpm -Uvh /usr/src/redhat/RPMS/$ARCH/libclasses-1.0-$BUILDVERSION.$ARCH.rpm /usr/src/redhat/RPMS/$ARCH/libclasses-devel-1.0-$BUILDVERSION.$ARCH.rpm
	RETVAL=$?
	if [ $RETVAL != 0 ]
	then
		echo "rpm install of libclasses/libclasses-devel-1.0-$BUILDVERSION.$ARCH.rpm failed! (return value $RETVAL)"
		exit -1
	fi
	
fi

echo "building perlqt (rpm_build_perlqt.sh)"
../rpm_build_perlqt.sh
RETVAL=$?
if [ $RETVAL != 0 ]
then
	echo "rpm_build_perlqt.sh failed! (return code $RETVAL)"
	exit -1
else
	rpm -Uvh /usr/src/redhat/RPMS/$ARCH/stoneperlqt-1.0-$BUILDVERSION.$ARCH.rpm
	RETVAL=$?
	if [ $RETVAL != 0 ]
        then
                echo "rpm install of stoneperlqt-1.0-$BUILDVERSION.$ARCH.rpm failed! (return value $RETVAL)"
		exit -1
	fi
fi

echo "running rpm_build_pystone.sh"
../rpm_build_pystone.sh
RETVAL=$?
if [ $RETVAL != 0 ]
then	
	echo "rpm_build_pystone.sh failed! (return code $RETVAL)"
	exit -1
else
	rpm -Uvh /usr/src/redhat/RPMS/$ARCH/pystone-1.0-$BUILDVERSION.$ARCH.rpm
	RETVAL=$?
	if [ $RETVAL != 0 ]
	then
		echo "rpm install of pystone-1.0-$BUILDVERSION.$ARCH.rpm failed! (return code $RETVAL)"
		exit -1
	fi
fi

echo "running rpm_build_pyclasses.sh"
../rpm_build_pyclasses.sh
RETVAL=$?
if [ $RETVAL != 0 ]
then	
	echo "rpm_build_pyclasses.sh failed! (return code $RETVAL)"
	exit -1
else
	rpm -Uvh /usr/src/redhat/RPMS/$ARCH/pyclasses-1.0-$BUILDVERSION.$ARCH.rpm
	RETVAL=$?
	if [ $RETVAL != 0 ]
	then
		echo "rpm install of pyclasses-1.0-$BUILDVERSION.$ARCH.rpm failed! (return code $RETVAL)"
		exit -1
	fi
fi

echo "running rpm_build_blur-perl.sh"
../rpm_build_blur-perl.sh
RETVAL=$?
if [ $RETVAL != 0 ]
then
	echo "rpm_build_blur-perl.sh failed! (return code $?)"
	exit -1
else
	rpm -Uvh /usr/src/redhat/RPMS/$ARCH/blur-perl-1.0-$BUILDVERSION.$ARCH.rpm --nodeps
	RETVAL=$?
	if [ $RETVAL != 0 ]
	then
		echo "rpm install of blur-perl-1.0-$BUILDVERSION.$ARCH.rpm failed! (return code $RETVAL)"
		exit -1
	fi
fi

echo "running rpm_build_rum.sh"
../rpm_build_rum.sh
RETVAL=$?
if [ $RETVAL != 0 ]
then
	echo "rpm_build_rum.sh failed! (return code $?)"
	exit -1
else
	rpm -Uvh /usr/src/redhat/RPMS/$ARCH/RUM-1.0-$BUILDVERSION.$ARCH.rpm --nodeps
	RETVAL=$?
	if [ $RETVAL != 0 ]
	then
		echo "rpm install of RUM-1.0-$BUILDVERSION.$ARCH.rpm failed! (return code $RETVAL)"
		exit -1
	fi
fi

echo "running rpm_build_reaper.sh"
../rpm_build_reaper.sh
RETVAL=$?
if [ $RETVAL != 0 ]
then
	echo "rpm_build_reaper.sh failed! (return code $?)"
	exit -1
else
	rpm -Uvh /usr/src/redhat/RPMS/$ARCH/reaper-1.0-$BUILDVERSION.$ARCH.rpm --nodeps
	RETVAL=$?
	if [ $RETVAL != 0 ]
	then
		echo "rpm install of reaper-1.0-$BUILDVERSION.$ARCH.rpm failed! (return code $RETVAL)"
		exit -1
	fi
fi

echo "running rpm_build_manager.sh"
../rpm_build_manager.sh
RETVAL=$?
if [ $RETVAL != 0 ]
then
	echo "rpm_build_manager.sh failed! (return code $?)"
	exit -1
else
	rpm -Uvh /usr/src/redhat/RPMS/$ARCH/manager-1.0-$BUILDVERSION.$ARCH.rpm --nodeps
	RETVAL=$?
	if [ $RETVAL != 0 ]
	then
		echo "rpm install of manager-1.0-$BUILDVERSION.$ARCH.rpm failed! (return code $RETVAL)"
		exit -1
	fi
fi

echo "running rpm_build_unassign_tasks.sh"
../rpm_build_unassign_tasks.sh
RETVAL=$?
if [ $RETVAL != 0 ]
then
	echo "rpm_build_unassign_tasks.sh failed! (return code $?)"
	exit -1
else
	rpm -Uvh /usr/src/redhat/RPMS/$ARCH/unassign_tasks-1.0-$BUILDVERSION.$ARCH.rpm --nodeps
	RETVAL=$?
	if [ $RETVAL != 0 ]
	then
		echo "rpm install of unassign_tasks-1.0-$BUILDVERSION.$ARCH.rpm failed! (return code $RETVAL)"
		exit -1
	fi
fi

echo "running rpm_build_reclaim_tasks.sh"
../rpm_build_reclaim_tasks.sh
RETVAL=$?
if [ $RETVAL != 0 ]
then
        echo "rpm_build_reclaim_tasks.sh failed! (return code $?)"
        exit -1
else
        rpm -Uvh /usr/src/redhat/RPMS/$ARCH/reclaim_tasks-1.0-$BUILDVERSION.$ARCH.rpm --nodeps
        RETVAL=$?
        if [ $RETVAL != 0 ]
        then
                echo "rpm install of reclaim_tasks-1.0-$BUILDVERSION.$ARCH.rpm failed! (return code $RETVAL)"
                exit -1
        fi
fi


echo "running rpm_build_energy_saver.sh"
../rpm_build_energy_saver.sh
RETVAL=$?
if [ $RETVAL != 0 ]
then
	echo "rpm_build_energy_saver.sh failed! (return code $?)"
	exit -1
else
	rpm -Uvh /usr/src/redhat/RPMS/$ARCH/energy_saver-1.0-$BUILDVERSION.$ARCH.rpm --nodeps
	RETVAL=$?
	if [ $RETVAL != 0 ]
	then
		echo "rpm install of energy_saver-1.0-$BUILDVERSION.$ARCH.rpm failed! (return code $RETVAL)"
		exit -1
	fi
fi

echo "running rpm_build_notifier.sh"
../rpm_build_notifier.sh
RETVAL=$?
if [ $RETVAL != 0 ]
then
	echo "rpm_build_notifier.sh failed! (return code $?)"
	exit -1
else
	rpm -Uvh /usr/src/redhat/RPMS/$ARCH/notifier-1.0-$BUILDVERSION.$ARCH.rpm --nodeps
	RETVAL=$?
	if [ $RETVAL != 0 ]
	then
		echo "rpm install of notifier-1.0-$BUILDVERSION.$ARCH.rpm failed! (return code $RETVAL)"
		exit -1
	fi
fi

echo "done RPM build. Everything appears to have built correctly."
