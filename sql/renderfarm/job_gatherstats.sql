
DROP FUNCTION IF EXISTS Job_GatherStats(int);
DROP TYPE IF EXISTS Job_GatherStats_Result;

CREATE TYPE Job_GatherStats_Result AS (mintasktime interval, avgtasktime interval, maxtasktime interval, totaltasktime interval, taskcount int,
	minloadtime interval, avgloadtime interval, maxloadtime interval, totalloadtime interval, loadcount int,
	minerrortime interval, avgerrortime interval, maxerrortime interval, totalerrortime interval, errorcount int,
	mincopytime interval, avgcopytime interval, maxcopytime interval, totalcopytime interval, copycount int, totaltime interval);

CREATE OR REPLACE FUNCTION Job_GatherStats(_keyjob int) RETURNS Job_GatherStats_Result AS $$
DECLARE
	ret Job_GatherStats_Result;
BEGIN
	SELECT INTO ret.mintasktime, ret.avgtasktime, ret.maxtasktime, ret.totaltasktime, ret.taskcount
				min(duration) as mintasktime,
				avg(duration) as avgtasktime,
				max(duration) as maxtasktime,
				sum(duration) as totaltasktime,
				count(*) as taskcount
			FROM (SELECT ended-started as duration FROM JobTaskAssignment
				WHERE
				fkeyjobassignment IN (SELECT keyJobAssignment FROM JobAssignment WHERE fkeyJob=_keyjob)
				AND fkeyjobassignmentstatus=4) as iq;
		-- Load Times
	SELECT INTO ret.minloadtime, ret.avgloadtime, ret.maxloadtime, ret.totalloadtime, ret.loadcount
				min(duration) as minloadtime,
				avg(duration) as avgloadtime,
				max(duration) as maxloadtime,
				sum(duration) as totalloadtime,
				count(*) as loadcount
			FROM (
				SELECT firsttaskstarted-started as duration FROM JobAssignment
				WHERE
					fkeyjob=_keyjob
					AND firsttaskstarted is not null) as iq;
		-- Error Times
	SELECT INTO ret.minerrortime, ret.avgerrortime, ret.maxerrortime, ret.totalerrortime, ret.errorcount
				min(duration) as minerrortime,
				avg(duration) as avgerrortime,
				max(duration) as maxerrortime,
				sum(duration) as totalerrortime,
				count(*) as errorcount
			FROM (
				SELECT error_time as duration FROM jobassignment_summaries(_keyjob) WHERE error_time > '0'::interval) as iq;
	SELECT INTO ret.mincopytime, ret.avgcopytime, ret.maxcopytime, ret.totalcopytime, ret.copycount
				min(duration) as mincopytime,
				avg(duration) as avgcopytime,
				max(duration) as maxcopytime,
				sum(duration) as totalcopytime,
				count(*) as copycount
			FROM (
				SELECT started-created as duration FROM JobAssignment
			WHERE
				fkeyjob=_keyjob AND started-created IS NOT NULL) as iq;
	ret.totaltime := coalesce(ret.totaltasktime,'0 minutes'::interval) + coalesce(ret.totalloadtime,'0 minutes'::interval) + coalesce(ret.totalerrortime,'0 minutes'::interval);
	RETURN ret;
END;
$$ LANGUAGE 'plpgsql';