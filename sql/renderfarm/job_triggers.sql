
CREATE OR REPLACE FUNCTION Job_Update() RETURNS trigger AS $$
DECLARE
BEGIN

	IF OLD.status IS DISTINCT FROM NEW.status AND NEW.status IN ('started','ready') AND job_has_unfinished_deps(NEW.keyjob) THEN
		NEW.status := 'holding';
	END IF;

	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION Job_Update_After() RETURNS trigger AS $$
DECLARE
	stat jobstat;
	restartTasks boolean := false;
	cancelAssignments boolean := false;
BEGIN
	IF OLD.status != NEW.status THEN
		IF NEW.status IN ('done','deleted','archived') THEN
			PERFORM job_check_start_dependents(NEW.keyjob);
		ELSE
			PERFORM job_check_hold_dependents(NEW.keyjob);
		END IF;
	END IF;

	IF (NEW.fkeyjobstat IS NOT NULL) THEN
		IF (NEW.status='started' AND OLD.status='ready') THEN
			UPDATE jobstat SET started=NOW() WHERE keyjobstat=NEW.fkeyjobstat AND started IS NULL;
		END IF;
		IF (NEW.status='ready' AND OLD.status='done') THEN
			WITH stat_insert(keyjobstat) AS (
				INSERT INTO jobstat (fkeyelement, fkeyproject, taskCount, fkeyusr, started, fkeyjobtype)
					SELECT stat.fkeyelement, stat.fkeyproject, stat.taskCount, stat.fkeyusr, NOW(), stat.fkeyjobtype 
					FROM jobstat WHERE keyjobstat=NEW.fkeyjobstat
				RETURNING keyJobStat
			)
			SELECT INTO NEW.fkeyjobstat keyJobStat FROM stat_insert;
		END IF;
		IF (NEW.status IN ('done','deleted') AND OLD.status IN ('ready','started','suspended') ) THEN
			UPDATE jobstat
			SET
				ended=NOW(),
				errorcount=iq.errorcount,
				totaltasktime=iq.totaltasktime, mintasktime=iq.mintasktime, maxtasktime=iq.maxtasktime, avgtasktime=iq.avgtasktime,
				totalloadtime=iq.totalloadtime, minloadtime=iq.minloadtime, maxloadtime=iq.maxloadtime, avgloadtime=iq.avgloadtime,
				totalerrortime=iq.totalerrortime, minerrortime=iq.minerrortime, maxerrortime=iq.maxerrortime, avgerrortime=iq.avgerrortime,
				totalcopytime=iq.totalcopytime, mincopytime=iq.mincopytime, maxcopytime=iq.maxcopytime, avgcopytime=iq.avgcopytime
			FROM (select * FROM Job_GatherStats(NEW.keyjob)) as iq
			WHERE keyjobstat=NEW.fkeyjobstat;
			UPDATE jobstat
			SET
				taskcount=iq.taskscount,
				taskscompleted=iq.tasksdone,
				averagememory=iq.averagememory,
				fkeyproject=NEW.fkeyproject,
				fkeyelement=NEW.fkeyelement,
				fkeyusr=NEW.fkeyusr,
				outputpath=NEW.outputpath,
				started=NEW.startedts,
				ended=NEW.endedts,
				fkeyjobtype=NEW.fkeyjobtype
			FROM (select jobstatus.taskscount, jobstatus.tasksdone, jobstatus.averagememory FROM jobstatus WHERE jobstatus.fkeyjob=NEW.keyjob) as iq
			WHERE keyjobstat=NEW.fkeyjobstat;
		END IF;
	END IF;

	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';


DROP TRIGGER IF EXISTS Job_Update ON job;
DROP TRIGGER IF EXISTS JobBatch_Update ON jobbatch;
DROP TRIGGER IF EXISTS JobClientUpdate_Update ON jobclientupdate;
DROP TRIGGER IF EXISTS JobMax_Update ON jobmax;
DROP TRIGGER IF EXISTS JobMax7_Update ON jobmax7;
DROP TRIGGER IF EXISTS JobMax8_Update ON jobmax8;
DROP TRIGGER IF EXISTS JobMax9_Update ON jobmax9;
DROP TRIGGER IF EXISTS JobMax10_Update ON jobmax10;
DROP TRIGGER IF EXISTS JobMax2009_Update ON jobmax2009;
DROP TRIGGER IF EXISTS JobMax2010_Update ON jobmax2010;
DROP TRIGGER IF EXISTS JobMaxScript_Update ON jobmaxscript;
DROP TRIGGER IF EXISTS JobXSI_Update ON jobxsi;
DROP TRIGGER IF EXISTS JobXSIScript_Update on jobxsiscript;
DROP TRIGGER IF EXISTS JobCinema4d_Update ON jobcinema4d;
DROP TRIGGER IF EXISTS JobAfterEffects_Update ON jobaftereffects;
DROP TRIGGER IF EXISTS JobAfterEffects7_Update ON jobaftereffects7;
DROP TRIGGER IF EXISTS JobAfterEffects8_Update ON jobaftereffects8;
DROP TRIGGER IF EXISTS JobAfterEffects9_Update ON jobaftereffects9;
DROP TRIGGER IF EXISTS JobAfterEffects10_Update ON jobaftereffects10;
DROP TRIGGER IF EXISTS JobMaya7_Update ON jobmaya7;
DROP TRIGGER IF EXISTS JobMaya85_Update ON jobmaya85;
DROP TRIGGER IF EXISTS JobMaya2008_Update ON jobmaya2008;
DROP TRIGGER IF EXISTS JobMaya2009_Update ON jobmaya2009;
DROP TRIGGER IF EXISTS JobMaya2011_Update ON jobmaya2011;
DROP TRIGGER IF EXISTS JobMaya2012_Update ON jobmaya2012;
DROP TRIGGER IF EXISTS JobMentalRay85_Update ON jobmentalray85;
DROP TRIGGER IF EXISTS JobMentalRay2011_Update ON jobmentalray2011;
DROP TRIGGER IF EXISTS JobMentalRay2012_Update ON jobmentalray2012;
DROP TRIGGER IF EXISTS JobShake_Update ON jobshake;
DROP TRIGGER IF EXISTS JobSync_Update ON jobsync;
DROP TRIGGER IF EXISTS JobFumeFxSim_Update ON jobfumefxsim;
DROP TRIGGER IF EXISTS JobFusion_Update ON jobfusion;
DROP TRIGGER IF EXISTS JobFusionVideoMaker_Update ON jobfusionvideomaker;
DROP TRIGGER IF EXISTS JobNuke_Update ON jobnuke;
DROP TRIGGER IF EXISTS JobNuke51_Update ON jobnuke51;
DROP TRIGGER IF EXISTS JobNuke6_Update ON jobnuke6;
DROP TRIGGER IF EXISTS Job3Delight_Update ON job3delight;
DROP TRIGGER IF EXISTS JobRealFlow_Update ON jobrealflow;
DROP TRIGGER IF EXISTS JobProxy_Update ON jobproxy;
DROP TRIGGER IF EXISTS JobHoudini_Update ON jobhoudini;
DROP TRIGGER IF EXISTS JobHoudiniScript_Update ON jobhoudiniscript;
DROP TRIGGER IF EXISTS JobMantra_Update ON jobmantra;
DROP TRIGGER IF EXISTS JobVray_Update ON jobvray;
DROP TRIGGER IF EXISTS JobVraySpawner_Update ON jobvrayspawner;

CREATE TRIGGER Job_Update BEFORE UPDATE ON job FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobBatch_Update BEFORE UPDATE ON jobbatch FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobClientUpdate_Update BEFORE UPDATE ON jobclientupdate FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobMax_Update BEFORE UPDATE ON jobmax FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobMaxScript_Update BEFORE UPDATE ON JobMaxScript FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobXSI_Update BEFORE UPDATE ON jobxsi FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobXSIScript_Update BEFORE UPDATE ON jobxsiscript FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobCinema4d_Update BEFORE UPDATE ON JobCinema4d FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobAfterEffects_Update BEFORE UPDATE ON JobAfterEffects FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobAfterEffects10_Update BEFORE UPDATE ON JobAfterEffects10 FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobMaya_Update BEFORE UPDATE ON JobMaya2012 FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobShake_Update BEFORE UPDATE ON JobShake FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobSync_Update BEFORE UPDATE ON JobSync FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobFumeFxSim_Update BEFORE UPDATE ON JobFumeFxSim FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobFusion_Update BEFORE UPDATE ON JobFusion FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobFusionVideoMaker_Update BEFORE UPDATE ON JobFusionVideoMaker FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobNuke_Update BEFORE UPDATE ON JobNuke FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobRealFlow_Update BEFORE UPDATE ON JobRealFlow FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobProxy_Update BEFORE UPDATE ON JobProxy FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobHoudini_Update BEFORE UPDATE ON JobHoudini FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobHoudiniScript_Update BEFORE UPDATE ON JobHoudiniScript FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobMantra_Update BEFORE UPDATE ON JobMantra FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobVray_Update BEFORE UPDATE ON JobVray FOR EACH ROW EXECUTE PROCEDURE Job_Update();
CREATE TRIGGER JobVraySpawner_Update BEFORE UPDATE ON JobVraySpawner FOR EACH ROW EXECUTE PROCEDURE Job_Update();

DROP TRIGGER IF EXISTS Job_Update_After ON job;
DROP TRIGGER IF EXISTS JobBatch_Update_After ON jobbatch;
DROP TRIGGER IF EXISTS JobClientUpdate_Update_After ON jobclientupdate;
DROP TRIGGER IF EXISTS JobMax_Update_After ON jobmax;
DROP TRIGGER IF EXISTS JobMax7_Update_After ON jobmax7;
DROP TRIGGER IF EXISTS JobMax8_Update_After ON jobmax8;
DROP TRIGGER IF EXISTS JobMax9_Update_After ON jobmax9;
DROP TRIGGER IF EXISTS JobMax10_Update_After ON jobmax10;
DROP TRIGGER IF EXISTS JobMax2009_Update_After ON jobmax2009;
DROP TRIGGER IF EXISTS JobMax2010_Update_After ON jobmax2010;
DROP TRIGGER IF EXISTS JobMaxScript_Update_After ON jobmaxscript;
DROP TRIGGER IF EXISTS JobXSI_Update_After ON jobxsi;
DROP TRIGGER IF EXISTS JobXSIScript_Update_After on jobxsiscript;
DROP TRIGGER IF EXISTS JobCinema4d_Update_After ON jobcinema4d;
DROP TRIGGER IF EXISTS JobAfterEffects_Update_After ON jobaftereffects;
DROP TRIGGER IF EXISTS JobAfterEffects7_Update_After ON jobaftereffects7;
DROP TRIGGER IF EXISTS JobAfterEffects8_Update_After ON jobaftereffects8;
DROP TRIGGER IF EXISTS JobAfterEffects9_Update_After ON jobaftereffects9;
DROP TRIGGER IF EXISTS JobAfterEffects10_Update_After ON jobaftereffects10;
DROP TRIGGER IF EXISTS JobMaya7_Update_After ON jobmaya7;
DROP TRIGGER IF EXISTS JobMaya85_Update_After ON jobmaya85;
DROP TRIGGER IF EXISTS JobMaya2008_Update_After ON jobmaya2008;
DROP TRIGGER IF EXISTS JobMaya2009_Update_After ON jobmaya2009;
DROP TRIGGER IF EXISTS JobMaya2011_Update_After ON jobmaya2011;
DROP TRIGGER IF EXISTS JobMaya2012_Update_After ON jobmaya2012;
DROP TRIGGER IF EXISTS JobMentalRay85_Update_After ON jobmentalray85;
DROP TRIGGER IF EXISTS JobMentalRay2011_Update_After ON jobmentalray2011;
DROP TRIGGER IF EXISTS JobMentalRay2012_Update_After ON jobmentalray2012;
DROP TRIGGER IF EXISTS JobShake_Update_After ON jobshake;
DROP TRIGGER IF EXISTS JobSync_Update_After ON jobsync;
DROP TRIGGER IF EXISTS JobFumeFxSim_Update_After ON jobfumefxsim;
DROP TRIGGER IF EXISTS JobFusion_Update_After ON jobfusion;
DROP TRIGGER IF EXISTS JobFusionVideoMaker_Update_After ON jobfusionvideomaker;
DROP TRIGGER IF EXISTS JobNuke_Update_After ON jobnuke;
DROP TRIGGER IF EXISTS JobNuke51_Update_After ON jobnuke51;
DROP TRIGGER IF EXISTS JobNuke6_Update_After ON jobnuke6;
DROP TRIGGER IF EXISTS Job3Delight_Update_After ON job3delight;
DROP TRIGGER IF EXISTS JobRealFlow_Update_After ON jobrealflow;
DROP TRIGGER IF EXISTS JobProxy_Update_After ON jobproxy;
DROP TRIGGER IF EXISTS JobHoudini_Update_After ON jobhoudini;
DROP TRIGGER IF EXISTS JobHoudiniScript_Update_After ON jobhoudiniscript;
DROP TRIGGER IF EXISTS JobMantra_Update_After ON jobmantra;
DROP TRIGGER IF EXISTS JobVray_Update_After ON jobvray;
DROP TRIGGER IF EXISTS JobVraySpawner_Update_After ON jobvrayspawner;

CREATE TRIGGER Job_Update_After AFTER UPDATE ON job FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobBatch_Update_After AFTER UPDATE ON jobbatch FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobClientUpdate_Update_After AFTER UPDATE ON jobclientupdate FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobMax_Update_After AFTER UPDATE ON jobmax FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobMaxScript_Update_After AFTER UPDATE ON JobMaxScript FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobXSI_Update_After AFTER UPDATE ON jobxsi FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobXSIScript_Update_After AFTER UPDATE ON jobxsiscript FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobCinema4d_Update_After AFTER UPDATE ON JobCinema4d FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobAfterEffects_Update_After AFTER UPDATE ON JobAfterEffects FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobAfterEffects10_Update_After AFTER UPDATE ON JobAfterEffects10 FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobShake_Update_After AFTER UPDATE ON JobShake FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobSync_Update_After AFTER UPDATE ON JobSync FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobFumeFxSim_Update_After AFTER UPDATE ON JobFumeFxSim FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobFusion_Update_After AFTER UPDATE ON JobFusion FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobFusionVideoMaker_Update_After AFTER UPDATE ON JobFusionVideoMaker FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobNuke_Update_After AFTER UPDATE ON JobNuke FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobRealFlow_Update_After AFTER UPDATE ON JobRealFlow FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobProxy_Update_After AFTER UPDATE ON JobProxy FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobHoudini_Update_After AFTER UPDATE ON JobHoudini FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobHoudiniScript_Update_After AFTER UPDATE ON JobHoudiniScript FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobMantra_Update_After AFTER UPDATE ON JobMantra FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobVray_Update_After AFTER UPDATE ON JobVray FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();
CREATE TRIGGER JobVraySpawner_Update_After AFTER UPDATE ON JobVraySpawner FOR EACH ROW EXECUTE PROCEDURE Job_Update_After();

-- INSERT triggers
CREATE OR REPLACE FUNCTION Job_Insert()
  RETURNS "trigger" AS
$BODY$
BEGIN
	INSERT INTO jobstat (name,fkeyelement,fkeyproject,fkeyusr,fkeyjobtype)
		VALUES (NEW.job, NEW.fkeyelement, NEW.fkeyproject, NEW.fkeyusr, NEW.fkeyjobtype) RETURNING keyjobstat INTO NEW.fkeyjobstat;
	INSERT INTO jobstatus (fkeyjob) VALUES (NEW.keyjob);
RETURN NEW;
END;
$BODY$
  LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS Job_Insert ON job;
DROP TRIGGER IF EXISTS JobBatch_Insert ON jobbatch;
DROP TRIGGER IF EXISTS JobClientUpdate_Insert ON jobclientupdate;
DROP TRIGGER IF EXISTS JobMax_Insert ON jobmax;
DROP TRIGGER IF EXISTS JobMax7_Insert ON jobmax7;
DROP TRIGGER IF EXISTS JobMax8_Insert ON jobmax8;
DROP TRIGGER IF EXISTS JobMax9_Insert ON jobmax9;
DROP TRIGGER IF EXISTS JobMax10_Insert ON jobmax10;
DROP TRIGGER IF EXISTS JobMax2009_Insert ON jobmax2009;
DROP TRIGGER IF EXISTS JobMax2010_Insert ON jobmax2010;
DROP TRIGGER IF EXISTS JobMaxScript_Insert ON jobmaxscript;
DROP TRIGGER IF EXISTS JobXSI_Insert ON jobxsi;
DROP TRIGGER IF EXISTS JobXSIScript_Insert ON jobxsiscript;
DROP TRIGGER IF EXISTS JobCinema4d_Insert ON jobcinema4d;
DROP TRIGGER IF EXISTS JobAfterEffects_Insert ON jobaftereffects;
DROP TRIGGER IF EXISTS JobAfterEffects7_Insert ON jobaftereffects7;
DROP TRIGGER IF EXISTS JobAfterEffects8_Insert ON jobaftereffects8;
DROP TRIGGER IF EXISTS JobAfterEffects9_Insert ON jobaftereffects9;
DROP TRIGGER IF EXISTS JobAfterEffects10_Insert ON jobaftereffects10;
DROP TRIGGER IF EXISTS JobMaya7_Insert ON jobmaya7;
DROP TRIGGER IF EXISTS JobMaya85_Insert ON jobmaya85;
DROP TRIGGER IF EXISTS JobMaya2008_Insert ON jobmaya2008;
DROP TRIGGER IF EXISTS JobMaya2009_Insert ON jobmaya2009;
DROP TRIGGER IF EXISTS JobMaya2011_Insert ON jobmaya2011;
DROP TRIGGER IF EXISTS JobMaya2012_Insert ON jobmaya2012;
DROP TRIGGER IF EXISTS JobMentalRay85_Insert ON jobmentalray85;
DROP TRIGGER IF EXISTS JobMentalRay2011_Insert ON jobmentalray2011;
DROP TRIGGER IF EXISTS JobMentalRay2012_Insert ON jobmentalray2012;
DROP TRIGGER IF EXISTS JobShake_Insert ON jobshake;
DROP TRIGGER IF EXISTS JobSync_Insert ON jobsync;
DROP TRIGGER IF EXISTS JobFumeFxSim_Insert ON jobfumefxsim;
DROP TRIGGER IF EXISTS JobFusion_Insert ON jobfusion;
DROP TRIGGER IF EXISTS JobFusionVideoMaker_Insert ON jobfusionvideomaker;
DROP TRIGGER IF EXISTS JobNuke_Insert ON jobnuke;
DROP TRIGGER IF EXISTS JobNuke51_Insert ON jobnuke51;
DROP TRIGGER IF EXISTS JobNuke6_Insert ON jobnuke6;
DROP TRIGGER IF EXISTS Job3Delight_Insert ON job3delight;
DROP TRIGGER IF EXISTS JobRealFlow_Insert ON jobrealflow;
DROP TRIGGER IF EXISTS JobProxy_Insert ON jobproxy;
DROP TRIGGER IF EXISTS JobHoudini_Insert ON jobhoudini;
DROP TRIGGER IF EXISTS JobHoudiniScript_Insert ON jobhoudiniscript;
DROP TRIGGER IF EXISTS JobMantra_Insert ON jobmantra;
DROP TRIGGER IF EXISTS JobVray_Insert ON jobvray;
DROP TRIGGER IF EXISTS JobVraySpawner_Insert ON jobvrayspawner;

CREATE TRIGGER Job_Insert AFTER INSERT ON job FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobBatch_Insert AFTER INSERT ON jobbatch FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobClientUpdate_Insert AFTER INSERT ON jobclientupdate FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobMax_Insert AFTER INSERT ON jobmax FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobMaxScript_Insert AFTER INSERT ON jobmaxscript FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobXSI_Insert AFTER INSERT ON jobxsi FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobXSIScript_Insert AFTER INSERT ON jobxsiscript FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobCinema4d_Insert AFTER INSERT ON JobCinema4d FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobAfterEffects_Insert AFTER INSERT ON JobAfterEffects FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobAfterEffects10_Insert AFTER INSERT ON JobAfterEffects10 FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobMaya2012_Insert AFTER INSERT ON JobMaya2012 FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobShake_Insert AFTER INSERT ON JobShake FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobSync_Insert AFTER INSERT ON JobSync FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobFumeFxSim_Insert AFTER INSERT ON JobFumeFxSim FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobFusion_Insert AFTER INSERT ON JobFusion FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobFusionVideoMaker_Insert AFTER INSERT ON JobFusionVideoMaker FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobNuke_Insert AFTER INSERT ON JobNuke FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobRealFlow_Insert AFTER INSERT ON JobRealFlow FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobProxy_Insert AFTER INSERT ON JobProxy FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobHoudini_Insert AFTER INSERT ON JobHoudini FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobHoudiniScript_Insert AFTER INSERT ON JobHoudiniScript FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobMantra_Insert AFTER INSERT ON JobMantra FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobVray_Insert AFTER INSERT ON JobVray FOR EACH ROW EXECUTE PROCEDURE Job_Insert();
CREATE TRIGGER JobVraySpawner_Insert AFTER INSERT ON JobVraySpawner FOR EACH ROW EXECUTE PROCEDURE Job_Insert();

-- DELETE triggers
CREATE OR REPLACE FUNCTION Job_Delete()
  RETURNS "trigger" AS
$BODY$
BEGIN
	DELETE FROM JobDep WHERE fkeyjob=OLD.keyjob OR fkeydep=OLD.keyjob;
RETURN OLD;
END;
$BODY$
  LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS Job_Delete ON job;
DROP TRIGGER IF EXISTS JobBatch_Delete ON jobbatch;
DROP TRIGGER IF EXISTS JobClientUpdate_Delete ON jobclientupdate;
DROP TRIGGER IF EXISTS JobMax_Delete ON jobmax;
DROP TRIGGER IF EXISTS JobMax7_Delete ON jobmax7;
DROP TRIGGER IF EXISTS JobMax8_Delete ON jobmax8;
DROP TRIGGER IF EXISTS JobMax9_Delete ON jobmax9;
DROP TRIGGER IF EXISTS JobMax10_Delete ON jobmax10;
DROP TRIGGER IF EXISTS JobMax2009_Delete ON jobmax2009;
DROP TRIGGER IF EXISTS JobMax2010_Delete ON jobmax2010;
DROP TRIGGER IF EXISTS JobMaxScript_Delete ON jobmaxscript;
DROP TRIGGER IF EXISTS JobXSI_Delete ON jobxsi;
DROP TRIGGER IF EXISTS JobXSIScript_Delete ON jobxsiscript;
DROP TRIGGER IF EXISTS JobCinema4d_Delete ON jobcinema4d;
DROP TRIGGER IF EXISTS JobAfterEffects_Delete ON jobaftereffects;
DROP TRIGGER IF EXISTS JobAfterEffects7_Delete ON jobaftereffects7;
DROP TRIGGER IF EXISTS JobAfterEffects8_Delete ON jobaftereffects8;
DROP TRIGGER IF EXISTS JobAfterEffects9_Delete ON jobaftereffects9;
DROP TRIGGER IF EXISTS JobAfterEffects10_Delete ON jobaftereffects10;
DROP TRIGGER IF EXISTS JobMaya7_Delete ON jobmaya7;
DROP TRIGGER IF EXISTS JobMaya85_Delete ON jobmaya85;
DROP TRIGGER IF EXISTS JobMaya2008_Delete ON jobmaya2008;
DROP TRIGGER IF EXISTS JobMaya2009_Delete ON jobmaya2009;
DROP TRIGGER IF EXISTS JobMaya2011_Delete ON jobmaya2011;
DROP TRIGGER IF EXISTS JobMaya2012_Delete ON jobmaya2012;
DROP TRIGGER IF EXISTS JobMentalRay85_Delete ON jobmentalray85;
DROP TRIGGER IF EXISTS JobMentalRay2011_Delete ON jobmentalray2011;
DROP TRIGGER IF EXISTS JobMentalRay2012_Delete ON jobmentalray2012;
DROP TRIGGER IF EXISTS JobShake_Delete ON jobshake;
DROP TRIGGER IF EXISTS JobSync_Delete ON jobsync;
DROP TRIGGER IF EXISTS JobFumeFxSim_Delete ON jobfumefxsim;
DROP TRIGGER IF EXISTS JobFusion_Delete ON jobfusion;
DROP TRIGGER IF EXISTS JobFusionVideoMaker_Delete ON jobfusionvideomaker;
DROP TRIGGER IF EXISTS JobNuke_Delete ON jobnuke;
DROP TRIGGER IF EXISTS JobNuke51_Delete ON jobnuke51;
DROP TRIGGER IF EXISTS JobNuke6_Delete ON jobnuke6;
DROP TRIGGER IF EXISTS Job3Delight_Delete ON job3delight;
DROP TRIGGER IF EXISTS JobRealFlow_Delete ON jobrealflow;
DROP TRIGGER IF EXISTS JobProxy_Delete ON jobproxy;
DROP TRIGGER IF EXISTS JobHoudini_Delete ON jobhoudini;
DROP TRIGGER IF EXISTS JobHoudiniScript_Delete ON jobhoudiniscript;
DROP TRIGGER IF EXISTS JobMantra_Delete ON jobmantra;
DROP TRIGGER IF EXISTS JobVray_Delete ON jobvray;
DROP TRIGGER IF EXISTS JobVraySpawner_Delete ON jobvrayspawner;

CREATE TRIGGER Job_Delete AFTER DELETE ON job FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobBatch_Delete AFTER DELETE ON jobbatch FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobClientUpdate_Delete AFTER DELETE ON jobclientupdate FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobMax_Delete AFTER DELETE ON jobmax FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobMaxScript_Delete AFTER DELETE ON jobmaxscript FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobXSI_Delete AFTER DELETE ON jobxsi FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobXSIScript_Delete AFTER DELETE ON jobxsiscript FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobCinema4d_Delete AFTER DELETE ON JobCinema4d FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobAfterEffects_Delete AFTER DELETE ON JobAfterEffects FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobAfterEffects10_Delete AFTER DELETE ON JobAfterEffects10 FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobMaya_Delete AFTER DELETE ON JobMaya2012 FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobShake_Delete AFTER DELETE ON JobShake FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobSync_Delete AFTER DELETE ON JobSync FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobFumeFxSim_Delete AFTER DELETE ON JobFumeFxSim FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobFusion_Delete AFTER DELETE ON JobFusion FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobFusionVideoMaker_Delete AFTER DELETE ON JobFusionVideoMaker FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobNuke_Delete AFTER DELETE ON JobNuke FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobRealFlow_Delete AFTER DELETE ON JobRealFlow FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobProxy_Delete AFTER DELETE ON JobProxy FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobHoudini_Delete AFTER DELETE ON JobHoudini FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobHoudiniScript_Delete AFTER DELETE ON JobHoudiniScript FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobMantra_Delete AFTER DELETE ON JobMantra FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobVray_Delete AFTER DELETE ON JobVray FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
CREATE TRIGGER JobVraySpawner_Delete AFTER DELETE ON JobVraySpawner FOR EACH ROW EXECUTE PROCEDURE Job_Delete();
