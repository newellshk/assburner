
CREATE OR REPLACE FUNCTION create_job_assignment( job_key int, host_key int, jobtask_keys int [] ) RETURNS INT AS $$
DECLARE
	_jobAssignment JobAssignment;
BEGIN
	
	-- Create the JobAssignment
	INSERT INTO JobAssignment (fkeyjob, fkeyhost, fkeyjobassignmentstatus)
		VALUES (job_key, host_key, 1) 
		RETURNING * INTO _jobAssignment;

	-- Insert the JobTaskAssignments, and update the corrosponding jobtasks
	WITH task_assignments(fkeyjobtask, keyjobtaskassignment) AS (
		INSERT INTO JobTaskAssignment (fkeyjobassignment, fkeyjobassignmentstatus, fkeyjobtask)
			SELECT _jobAssignment.keyJobAssignment, 1, unnest(jobtask_keys)
			RETURNING fkeyjobtask, keyjobtaskassignment
	)
	UPDATE JobTask SET fkeyJobTaskAssignment=task_assignments.keyjobtaskassignment, status='assigned', fkeyhost=host_key
		FROM task_assignments WHERE keyJobTask=task_assignments.fkeyjobtask;

	-- Make this check, or the list of statuses to check, optional, for 'offline' assignment
	PERFORM 1 FROM HostStatus WHERE slaveStatus IN ('assigned','busy') AND fkeyhost=host_key;

	-- if not, raise an exception so the previous inserts/updates are rolled back
	IF NOT FOUND THEN
		RAISE EXCEPTION 'Host no longer ready for assignment';
	END IF;

	PERFORM 1 FROM Job WHERE status IN ('ready','started') AND keyJob=job_key;
	
	IF NOT FOUND THEN
		RAISE EXCEPTION 'Job no longer ready for assignment';
	END IF;
	
	RETURN _jobAssignment.keyJobAssignment;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION cancel_jobs_assignments( job_keys INT[], reset_all_tasks boolean ) RETURNS VOID AS $$
DECLARE
	cancelledStatusKey int;
	jobassignment_keys int[];
	reset_task_statuses text[];
BEGIN
	cancelledStatusKey := 6;
	-- SELECT INTO cancelledStatusKey keyjobassignmentstatus FROM jobassignmentstatus WHERE status='cancelled';

	IF reset_all_tasks THEN
		reset_task_statuses := ARRAY['new','assigned','busy','done','suspended'];
	ELSE
		reset_task_statuses := ARRAY['new','assigned','busy'];
	END IF;

	UPDATE JobTask SET status='new', startedts=NULL, endedts=NULL, memory=0, fkeyjobcommandhistory=NULL, fkeyjobtaskassignment=NULL
		WHERE fkeyjob=ANY(job_keys) AND status=ANY(reset_task_statuses);

	-- Gather all active job assignments
	SELECT INTO jobassignment_keys array_agg(keyJobAssignment) FROM
		JobAssignment WHERE fkeyJob=ANY(job_keys) AND fkeyJobAssignmentStatus IN (1,2,3);

	UPDATE JobTaskAssignment
		SET fkeyjobassignmentstatus=cancelledStatusKey
		WHERE fkeyjobassignment=ANY(jobassignment_keys)
		AND fkeyjobassignmentstatus IN (1,2,3);
	
	UPDATE JobAssignment
		SET fkeyjobassignmentstatus=cancelledStatusKey, tasksReturned=true
		WHERE keyJobAssignment=ANY(jobassignment_keys);

END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION cancel_job_assignment( _keyjobassignment int ) RETURNS VOID AS $$
DECLARE
	cancelledStatusKey int;
	assignment jobassignment;
BEGIN
	cancelledStatusKey := 6;

	UPDATE JobTask
		SET status='new',
			startedts=NULL, endedts=NULL, memory=0, fkeyjobcommandhistory=NULL, fkeyjobtaskassignment=NULL
		WHERE fkeyjobtaskassignment IN 
			(SELECT keyjobtaskassignment FROM jobtaskassignment WHERE fkeyjobassignment=_keyjobassignment)
		AND status IN ('assigned','busy');

	UPDATE JobTaskAssignment
		SET fkeyjobassignmentstatus=cancelledStatusKey 
		WHERE fkeyjobassignment=_keyjobassignment 
		AND fkeyjobassignmentstatus IN (1,2,3); --(SELECT keyjobassignmentstatus from jobassignmentstatus WHERE status IN ('ready','copy','busy'));

	-- Dont set it to cancelled if it is already cancelled or error
	UPDATE JobAssignment 
		SET fkeyjobassignmentstatus=cancelledStatusKey, tasksReturned=true
		WHERE keyjobassignment=_keyjobassignment 
		AND fkeyjobassignmentstatus IN (1,2,3); -- (SELECT keyjobassignmentstatus from jobassignmentstatus WHERE status IN ('ready','copy','busy'));

END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION assignment_status_count( fkeyjobassignmentstatus int ) RETURNS INT AS $$
DECLARE
	status jobassignmentstatus;
BEGIN
	IF fkeyjobassignmentstatus >= 0 AND fkeyjobassignmentstatus <= 3 THEN
		RETURN 1;
	END IF;
--	SELECT INTO status * FROM jobassignmentstatus WHERE keyjobassignmentstatus=fkeyjobassignmentstatus;
--	IF status.status IN ('ready','copy','busy') THEN
--		return 1;
--	END IF;
--	IF status.status IN ('done','cancelled','error') THEN
--		RETURN 0;
--	END IF;
	RETURN 0;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION jobassignment_insert() RETURNS trigger AS $$
DECLARE
	newstatusval int := 0;
BEGIN
	newstatusval := assignment_status_count(NEW.fkeyjobassignmentstatus);
	IF newstatusval > 0 THEN
		UPDATE HostStatus SET activeassignmentCount=coalesce(activeassignmentCount,0)+newstatusval,lastAssignmentChange=NOW() WHERE HostStatus.fkeyhost=NEW.fkeyhost;
	END IF;
	
	-- If we are starting at 'new','ready', or 'busy', we need to update license counts for the services
	-- TODO, uncomment when we switch from HostStatus triggers
--	IF newstatusval > 0 THEN
--		PERFORM updateJobLicenseCounts( NEW.fkeyjob, 1 );
--	END IF;
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION jobassignment_update() RETURNS trigger AS $$
DECLARE
	currentjob job;
	oldstatusval int := 0;
	newstatusval int := 0;
	assignmentcountchange int :=0;
	totaltime interval := '0'::interval;
BEGIN
	IF NEW.fkeyhost != OLD.fkeyhost THEN
		-- TODO: Raise some kind of error
		RAISE NOTICE 'JobAssignment.fkeyhost is immutable';
	END IF;


	IF OLD.started IS NULL AND NEW.started IS NOT NULL AND NEW.started > NOW() THEN
		NEW.started := NOW();
	END IF;

	oldstatusval := assignment_status_count(OLD.fkeyjobassignmentstatus);
	newstatusval := assignment_status_count(NEW.fkeyjobassignmentstatus);

	-- Update ended automatically
	IF oldstatusval > newstatusval AND NEW.started IS NOT NULL THEN
		NEW.ended := NOW();
	END IF;

	-- Allow explicit update of .ended even if the job is already
	-- done/cancelled/errored, so that assburner can account for 
	-- all time actually spent on the assignment.
	-- For example when the last task in an assignment is done 
	-- the assignment will automatically go to done, then later
	-- assburner will update it again when cleanup is finished,
	-- which we do want to include
	IF NEW.fkeyJobAssignmentStatus >= 4 AND NEW.started IS NOT NULL AND NEW.ended IS NOT NULL THEN
		totaltime = NEW.ended - NEW.started;
		-- Successful assignment, consider all time success time
		IF NEW.fkeyJobAssignmentStatus=4 THEN
			NEW.successtime = totaltime;
		-- Errored assignment, consider load time + done task time as successful if at least one task completed, else all is error time
		ELSIF NEW.fkeyJobAssignmentStatus=5 THEN
			IF NEW.donetasktime > '0'::interval THEN
				NEW.errortime = NEW.ended - NEW.firsttaskstarted - NEW.donetasktime;
				NEW.successtime = totaltime - NEW.errortime;
			ELSE
				NEW.errortime = totaltime;
				NEW.successtime = '0'::interval;
			END IF;
		-- Canceled assignment, similar to error time but canceled, not errored
		ELSIF NEW.fkeyJobAssignmentStatus=6 THEN
			IF NEW.donetasktime > '0'::interval THEN
				NEW.canceltime = NEW.ended - NEW.firsttaskstarted - NEW.donetasktime;
				NEW.successtime = totaltime - NEW.canceltime;
			ELSE
				NEW.canceltime = totaltime;
				NEW.successtime = '0'::interval;
			END IF;
		END IF;
	END IF;

	-- Update the number of active assignments for the host
	IF newstatusval != oldstatusval THEN
		UPDATE HostStatus SET activeAssignmentCount=coalesce(activeAssignmentCount,0)+(newstatusval-oldstatusval), lastAssignmentChange=NOW() WHERE HostStatus.fkeyhost=NEW.fkeyhost;
	END IF;
	
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION jobassignment_after_update() RETURNS trigger AS $$
DECLARE
BEGIN

	-- When assignment is complete(done, error, canceled), update JobStatus
	IF NEW.fkeyJobAssignmentStatus != OLD.fkeyJobAssignmentStatus AND NEW.fkeyJobAssignmentStatus > 3 THEN
	DECLARE
		elapsed_new interval;
		loadtime_new interval;
	BEGIN
		IF NOT NEW.tasksReturned THEN
			UPDATE JobTask
				SET status='new' 
				WHERE
					fkeyjobtaskassignment IN 
					(SELECT keyjobtaskassignment FROM jobtaskassignment WHERE fkeyjobassignment=NEW.keyjobassignment)
				AND status IN ('assigned','busy');

			UPDATE JobTaskAssignment
				SET fkeyjobassignmentstatus=6 -- cancelled
				WHERE fkeyjobassignment=NEW.keyjobassignment
				AND fkeyjobassignmentstatus IN (1,2,3); --(SELECT keyjobassignmentstatus from jobassignmentstatus WHERE status IN ('ready','copy','busy'));

			UPDATE JobAssignment SET tasksReturned=true WHERE keyJobAssignment=NEW.keyJobAssignment;
		END IF;

		elapsed_new = CASE WHEN NEW.started IS NOT NULL THEN coalesce(NEW.ended,now()) - NEW.started ELSE '0'::interval END;
		loadtime_new = CASE WHEN NEW.firsttaskstarted IS NOT NULL THEN NEW.firsttaskstarted - NEW.started ELSE '0'::interval END;

		UPDATE JobStatus SET
			totaltime = totaltime + elapsed_new,
			loadtime = loadtime + loadtime_new,
			successtime = successtime + NEW.successtime,
			donetasktime = donetasktime + NEW.donetasktime,
			canceltime = canceltime + NEW.canceltime,
			errortime = errortime + NEW.errortime
		WHERE fkeyJob=NEW.fkeyJob;
	END;
	END IF;
	
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION jobassignment_delete() RETURNS trigger AS $$
DECLARE
	oldstatusval int := 0;
BEGIN
	oldstatusval := assignment_status_count(OLD.fkeyjobassignmentstatus);
	IF oldstatusval > 0 THEN
		-- PERFORM updateJobLicenseCounts( OLD.fkeyjob, -1 );
	END IF;
	IF oldstatusval > 0 THEN
		UPDATE HostStatus SET activeAssignmentCount=coalesce(activeAssignmentCount,0)-oldstatusval, lastAssignmentChange=NOW() WHERE HostStatus.fkeyhost=OLD.fkeyhost;
	END IF;
	RETURN OLD;
END;
$$ LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS jobassignment_insert_trigger ON jobassignment;
CREATE TRIGGER jobassignment_insert_trigger
BEFORE INSERT
ON jobassignment
FOR EACH ROW
EXECUTE PROCEDURE jobassignment_insert();

DROP TRIGGER IF EXISTS jobassignment_update_trigger ON jobassignment;
CREATE TRIGGER jobassignment_update_trigger
BEFORE UPDATE
ON jobassignment
FOR EACH ROW
EXECUTE PROCEDURE jobassignment_update();

DROP TRIGGER IF EXISTS jobassignment_after_update_trigger ON jobassignment;
CREATE TRIGGER jobassignment_after_update_trigger
AFTER UPDATE
ON jobassignment
FOR EACH ROW
EXECUTE PROCEDURE jobassignment_after_update();

DROP TRIGGER IF EXISTS jobassignment_delete_trigger ON jobassignment;
CREATE TRIGGER jobassignment_delete_trigger
BEFORE DELETE
ON jobassignment
FOR EACH ROW
EXECUTE PROCEDURE jobassignment_delete();
