
CREATE OR REPLACE FUNCTION jobtaskassignment_update() RETURNS trigger AS $$
DECLARE
BEGIN
	IF NEW.fkeyjobassignmentstatus BETWEEN 4 AND 6 AND NEW.started IS NOT NULL AND NEW.ended IS NULL THEN
		NEW.ended := NOW();
	END IF;
	IF NEW.started < OLD.started OR (NEW.started IS NOT NULL AND OLD.started IS NULL AND OLD.fkeyJobAssignmentStatus BETWEEN 5 AND 6) THEN
		RAISE EXCEPTION 'Why is started getting set here???';
	END IF;
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS jobtaskassignment_update_trigger ON jobtaskassignment;

CREATE TRIGGER jobtaskassignment_update_trigger
	BEFORE UPDATE
	ON jobtaskassignment
	FOR EACH ROW
	EXECUTE PROCEDURE jobtaskassignment_update();

CREATE OR REPLACE FUNCTION jobtaskassignment_after_update() RETURNS trigger AS $$
DECLARE
BEGIN
	PERFORM update_jobassignment_summary_fields(NEW.fkeyJobAssignment);
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS jobtaskassignment_after_update_trigger ON jobtaskassignment;

CREATE TRIGGER jobtaskassignment_after_update_trigger
	AFTER UPDATE
	ON jobtaskassignment
	FOR EACH ROW
	EXECUTE PROCEDURE jobtaskassignment_after_update();
