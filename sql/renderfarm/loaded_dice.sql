

CREATE OR REPLACE FUNCTION after_insert_syncedjobfile() RETURNS trigger AS $$
BEGIN
	UPDATE SyncedFile SET usageCount=usageCount+1 WHERE keySyncedFile=NEW.fkeySyncedFile;
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION after_delete_syncedjobfile() RETURNS trigger AS $$
BEGIN
	UPDATE SyncedFile SET usageCount=usageCount-1 WHERE keySyncedFile=OLD.fkeySyncedFile;
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';


DROP TRIGGER IF EXISTS after_insert_syncedjobfile ON syncedjobfile;
CREATE TRIGGER after_insert_syncedjobfile
AFTER INSERT ON syncedjobfile
FOR EACH ROW EXECUTE PROCEDURE after_insert_syncedjobfile();

DROP TRIGGER IF EXISTS after_delete_syncedjobfile ON syncedjobfile;
CREATE TRIGGER after_delete_syncedjobfile
AFTER DELETE ON syncedjobfile
FOR EACH ROW EXECUTE PROCEDURE after_delete_syncedjobfile();
